<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PostTags
 *
 * @mixin \Eloquent
 */
class PostTags extends Model
{

    protected $table = 'post_tags';

}
