<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\GalleryImage
 *
 * @mixin \Eloquent
 */
class GalleryImage extends Model
{

    protected $table = 'image_gallery';

}
