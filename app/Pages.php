<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pages
 *
 * @mixin \Eloquent
 */
class Pages extends Model
{

    protected $table = 'pages';

}
