<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use App\PackagesStats;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\UpdateSources::class,
        \App\Console\Commands\ExpirePosts::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $path = storage_path('app/expire.txt');
        $schedule->command('inspire')
                 ->hourly();

        $schedule->call(function () {
            $stats = PackagesStats::all();
            foreach ($stats as $stat) {
                if ($stat->days_left == 30 || $stat->days_left == 14 || $stat->days_left == 7 || $stat->days_left == 1) {
                    Log::debug($stat->user->name);
                    Log::debug($stat->user->email);
                    Log::debug($stat->days_left);
                    $email = $stat->user->email;
                    $subject = 'Package Expiry | ItsMyCall';
                    Mail::send(
                        'emails.expiry',
                        array(
                            'name' => $stat->user->name,
                            'email' => $stat->user->email,
                            'subject' => $stat->package->package_name,
                            'days_left' => $stat->days_left,
                            'balance' => $stat->user->credits,
        
                        ),
                        function ($message) use ($email, $subject) {
                            $message->to($email)->subject($subject);
                        }
                    );
                }
                
            }

        })->daily();
    }
}
