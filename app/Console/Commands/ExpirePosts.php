<?php

namespace App\Console\Commands;

use App\CronJobs;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Posts;

class ExpirePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire-posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update jobs listed more than 30 day to expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $what = "";
        $cron_started_on = Carbon::now('Australia/Melbourne');;
        $what .= "<h2>Cron Job Started On " . $cron_started_on . " </h2> <br>";
        $posts = Posts::where('expired_at', null)->get();
        $what .= "<h4>Checking jobs (" . sizeof($posts) . ") </h4><br><br>";
        $expiredJobs = [];

        if (empty($posts)) {
            $what .= "<h4>Found expired jobs</h4>";
        }

        foreach ($posts as $post) {
            /** @var Carbon $expired_at */
            $post->expired_at = $post->created_at->addDay(Posts::EXPIRY_DAYS);
//            $expiredJobs[] = [
//                'id' => $post->id,
//                'title' => $post->title,
//                'company' => $post->company
//            ];
            $what .= "<h4>Found expired jobs: {$post->id} {$post->company} {$post->title}</h4>";
            $post->save();
        }

        $cron_completed_on = Carbon::now();

        $what .= "<h2>Cron Job Completed On " . $cron_completed_on . " </h2> <br><br>";

        $cron = new CronJobs();
        $cron->cron_started_on = $cron_started_on;
        $cron->cron_completed_on = $cron_completed_on;
        $cron->what = $what;
        $cron->result = 1;
        $cron->save();
    }
}
