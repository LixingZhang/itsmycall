<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\PostUpgrades
 *
 * @property-read \App\Upgrades $upgrade
 * @property-read \App\Posts $job
 * @property-read \App\Users $user
 * @mixin \Eloquent
 */
class PostUpgrades extends Model
{

    protected $table = 'posts_upgrades';

    /**
     * Get the Upgrade owns
     */
    public function upgrade()
    {
        return $this->belongsTo(Upgrades::class, 'upgrade_id');
    }
    
    public function job()
    {
        return $this->belongsTo(Posts::class, 'post_id');
    }
    
     public function user()
    {
        return $this->belongsTo(Users::class);
    }
    
    public function lists()
    {
        return $this->hasMany(PostUpgradesLists::class);
    }

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'job_id', 'post_id');
    }

    public function isNewsLetterUpgrade()
    {
        return $this->upgrade()->getResults()->name === Upgrades::UPGRADE_NEWSLETTER;
    }

    public function isFacebookUpgrade()
    {
        return $this->upgrade()->getResults()->name === Upgrades::UPGRADE_FACEBOOK;
    }

    public function getStatus()
    {
        $lists = $this->lists()->getResults();
        $notActionedCount = 0;
        $inProgressCount = 0;
        $completeCount = 0;

        foreach ($lists as $list) {
            switch ($list->post_status) {
                case PostUpgradesLists::STATUS_NOT_ACTIONED:
                    $notActionedCount++;
                    break;
                case PostUpgradesLists::STATUS_IN_PROGRESS:
                    $inProgressCount++;
                    break;
                case PostUpgradesLists::STATUS_COMPLETED:
                    $completeCount++;
                    break;
                default:
                    break;
            }
        }

        if ($lists->count() === $completeCount) {
            return '<lable class="label label-success label-sm">' . PostUpgradesLists::$status[PostUpgradesLists::STATUS_COMPLETED] . '</label>';
        }

        if ($notActionedCount > 0) {
            return '<lable class="label label-warning label-sm">' . PostUpgradesLists::$status[PostUpgradesLists::STATUS_NOT_ACTIONED] . '</label>';
        }

        return '<lable class="label label-info label-sm">' . PostUpgradesLists::$status[PostUpgradesLists::STATUS_IN_PROGRESS] . '</label>';
    }

    public function getRemainingPosts() {
        $lists = $this->lists()->getResults();
        $notActionedCount = 0;
        $inProgressCount = 0;
        $completeCount = 0;
        
        foreach ($lists as $list) {
            switch ($list->post_status) {
                case PostUpgradesLists::STATUS_NOT_ACTIONED:
                    $notActionedCount++;
                    break;
                case PostUpgradesLists::STATUS_IN_PROGRESS:
                    $inProgressCount++;
                    break;
                case PostUpgradesLists::STATUS_COMPLETED:
                    $completeCount++;
                    break;
                default:
                    break;
            }
        }

        if ($this->count >= $completeCount) {
            return $this->count - $completeCount;
        }

        return $inProgressCount + $notActionedCount;
    }

    public function getTotalViews()
    {
        $lists = $this->lists()->getResults();
        $totalViews = 0;

        foreach($lists as $list) {
            $totalViews += $list->post_views;
        }

        return $totalViews;
    }

    public function getTotalClicks()
    {
        $lists = $this->lists()->getResults();
        $totalClicks = 0;

        foreach ($lists as $list) {
            $totalClicks += $list->post_clicks;
        }

        return $totalClicks;
    }

    public function getTotalCost()
    {
        $lists = $this->lists()->getResults();
        $totalCost = 0;

        foreach ($lists as $list) {
            $totalCost += $list->post_cost;
        }

        return $totalCost;
    }

    public function getPostsDates()
    {
        $lists = $this->lists()->getResults();
        $dates = [];

        foreach ($lists as $list) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $list->post_datetime);
            $dates[] = '<span class="label label-success">' . $date->format('d M Y') . '</span>';
        }

        return implode('<br />', $dates);
    }

    public function getPostEditions()
    {
        $lists = $this->lists()->getResults();
        $editions = [];
        foreach ($lists as $list) {
            if (!$list->post_edition) {
                continue;
            }

            $editions[] = '<span class="label label-info">' . $list->post_edition . '</span>';
        }

        return implode('<br />', $editions);
    }

    public function getActualPrice()
    {
        $transaction = $this->transaction()->getResults();

        if (!empty($transaction->coupon_used)) {
            $coupon = Coupons::where('coupon_code', $transaction->coupon_used)->get()->first();

            if ($coupon) {
                return $this->upgrade()->getResults()->price;
            }
        }

        return $this->upgrade()->getResults()->price;
    }

    public static function getNewsLetterEdition()
    {
        $lists = PostUpgradesLists::all();

        $currentEdition = 0;

        foreach ($lists as $list) {
            if ((int) $list->post_edition && $list->post_edition > $currentEdition) {
                $currentEdition = $list->post_edition;
            }
        }

        return $currentEdition ? $currentEdition + 1: null;
    }

    public function isCreditsUsed()
    {
        $transaction = $this->transaction()->getResults();

        if ($transaction) {
            return $transaction->payment === 'Credits' ? 'Yes' : 'No';
        }

        return 'nil';
    }

    public function getPostComments()
    {
        $lists = $this->lists()->getResults();
        $comments = [];
        foreach ($lists as $list) {
            if (empty($list->post_comment)) {
                continue;
            }

            $comments[] = '<span class="label label-info">' . $list->post_comment  . '</span>';
        }

        return implode('<br />', $comments);
    }

    public function getInternalComments()
    {
        $lists = $this->lists()->getResults();
        $comments = [];
        foreach ($lists as $list) {
            if (empty($list->internal_comment)) {
                continue;
            }

            $comments[] = '<span class="label label-info">' . $list->internal_comment . '</span>';
        }

        return implode('<br />', $comments);
    }
}
