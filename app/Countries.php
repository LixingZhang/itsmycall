<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Countries
 *
 * @mixin \Eloquent
 */
class Countries extends Model
{


    protected $table = 'countries';

}
