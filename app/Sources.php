<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sources
 *
 * @mixin \Eloquent
 */
class Sources extends Model
{

    protected $table = 'sources';

}
