<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Upgrades
 *
 * @mixin \Eloquent
 */
class Upgrades extends Model
{
    const UPGRADE_NEWSLETTER = 'Feature in The Pulse newsletter';
    const UPGRADE_FACEBOOK = 'Facebook Post';
    const UPGRADE_LINKEDIN = 'LinkedIn Post';

    protected $table = 'upgrades';

}
