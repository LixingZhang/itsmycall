<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SubCategories
 *
 * @mixin \Eloquent
 */
class SubCategories extends Model
{

    const SCROLL_TYPE_PAGINATION = "pagination";
    const SCROLL_TYPE_SCROLL = "infinite_scroll";

    protected $table = 'sub_categories';

    public static function getSubCategoryById($id)
    {
        $subCategory = SubCategories::find($id);
        if ($subCategory) {
            return $subCategory->title;
        }

        return $id;
    }

}
