<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PostLikes
 *
 * @mixin \Eloquent
 */
class PostLikes extends Model
{

    protected $table = 'post_likes';

}
