<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Applicants
 *
 * @mixin \Eloquent
 */
class Applicants extends Model
{


    protected $table = 'applicants';

    public function user()
    {
        return $this->belongsTo(Users::class, 'author_id');
    }

    public function job()
    {
        return $this->hasOne(Posts::class, 'job_id');
    }

}
