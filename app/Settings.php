<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Settings
 *
 * @mixin \Eloquent
 */
class Settings extends Model
{

    const CATEGORY_CUSTOM_CSS = "custom_css";
    const CATEGORY_CUSTOM_JS = "custom_js";
    const CATEGORY_LIVE_JS = "live_js";
    const CATEGORY_SOCIAL = "social";
    const CATEGORY_CARDFEE = "card";
    const CATEGORY_COMMENTS = "comments";
    const CATEGORY_SEO = "seo";
    const CATEGORY_GENERAL = "general";
    const CATEGORY_OLD_NEWS = "old_news";
    const CATEGORY_WELCOME = "welcome";
    const CATEGORY_ADVERTISING_TABS = "advertising_tabs";
    const CATEGORY_ICONS_INFO = "icons_info";

    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_CHECK = 'check';

    protected $table = 'settings';

}
