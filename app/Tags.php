<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tags
 *
 * @mixin \Eloquent
 */
class Tags extends Model
{

    protected $table = 'tags';

}
