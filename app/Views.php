<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Views
 *
 * @mixin \Eloquent
 */
class Views extends Model
{


    protected $table = 'views';

}
