<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Permissions
 *
 * @mixin \Eloquent
 */
class Permissions extends Model
{

    protected $table = 'permissions';

}
