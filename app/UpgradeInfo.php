<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UpgradeInfo
 *
 * @property-read \App\Users $user
 * @property-read \App\Upgrades $upgrade
 * @property-read \App\Posts $job
 * @mixin \Eloquent
 */
class UpgradeInfo extends Model
{

    protected $table = 'upgrade_info';

     /*
     * @var bool
     */
    public $timestamps = false;


    /**
     * Get the User owns
     */
    public function user()
    {
        return $this->belongsTo(Users::class);
    }

    /**
     * Get the Package owns
     */
    public function upgrade()
    {
        return $this->belongsTo(Upgrades::class);
    }
    
    public function job()
    {
        return $this->belongsTo(Posts::class);
    }

}
