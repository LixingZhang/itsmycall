<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * App\Coupons
 *
 * @mixin \Eloquent
 */
class Coupons extends Model
{

    const TYPE_INDEX_HEADER = "index_header";
    const TYPE_INDEX_FOOTER = "index_footer";
    const TYPE_SIDEBAR = "sidebar";
    const TYPE_ABOVE_POST = "above_post";
    const TYPE_BELOW_POST = "below_post";
    const TYPE_BETWEEN_CATEGORY_INDEX = "between_category_index";
    const TYPE_BETWEEN_SUBCATEGORY_INDEX = "between_sub_category_index";
    const TYPE_BETWEEN_AUTHOR_INDEX = "between_author_index";
    const TYPE_BETWEEN_TAG_INDEX = "between_tag_index";
    const TYPE_BETWEEN_SEARCH_INDEX = "between_search_index";
    const TYPE_ABOVE_PAGE = "above_page";
    const TYPE_BELOW_PAGE = "below_page";

    const STATUS_VALID = 'Valid';
    const STATUS_EXPIRED = 'Expired';

    protected $table = 'coupons';

    public function getStatus()
    {
        if ($this->date_begin <= Carbon::now() && $this->date_end >= Carbon::now())
        {
            return static::STATUS_VALID;
        }

        return  static::STATUS_EXPIRED;
    }

    public function scopeActive($query) {
        return $query->where('is_active', true);
    }

    public function getUsedCount()
    {
        $count = DB::table('transactions')
            ->select(DB::raw('count(*) as count'))
            ->where('coupon_used', $this->coupon_code)->first();

        return $count->count;
    }

}
