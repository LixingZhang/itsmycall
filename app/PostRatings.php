<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PostRatings
 *
 * @mixin \Eloquent
 */
class PostRatings extends Model
{

    protected $table = 'post_ratings';

}
