<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
/**
 * App\PackagesStats
 *
 * @property-read \App\Users $user
 * @property-read \App\Packages $package
 * @mixin \Eloquent
 */
class PackagesStats extends Model
{

    protected $table = 'packages_stats';
    // protected $dates = ['date_purchase'];
     /*
     * @var bool
     */
    public $timestamps = false;

    const STATUS_PLEASE_INVOICE = 'please_invoice';
    const STATUS_AWAITING_PAYMENT = 'awaiting_payment';
    const STATUS_PAID = 'paid';

    static $status = [
        self::STATUS_PLEASE_INVOICE => 'Please Invoice',
        self::STATUS_AWAITING_PAYMENT => 'Awaiting Payment',
        self::STATUS_PAID => 'Paid'
    ];

    /**
     * Get the User owns
     */
    public function user()
    {
        return $this->belongsTo(Users::class);
    }

    /**
     * Get the Package owns
     */
    public function package()
    {
        return $this->belongsTo(Packages::class);
    }

    public function getExpiryDateAttribute($value)
    {
        return Carbon::parse($this->date_purchase)->addMonths(6);
    }

     public function getDaysLeftAttribute($value)
    {
        $dt = Carbon::parse($this->date_purchase);
        $days = Carbon::now()->diffInDays($dt->copy()->addMonths(6));
        if ($days < 0 ) $days = 0;
        return $days;
    }

}
