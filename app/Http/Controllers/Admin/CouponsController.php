<?php

namespace App\Http\Controllers\Admin;

use App\Coupons;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use Illuminate\Http\Request;
use Input;
use Session;
use DB;

class CouponsController extends Controller {

    function __construct() {
        $this->middleware('has_permission:ad_sections.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:ad_sections.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
        $this->middleware('has_permission:ad_sections.delete', ['only' => ['delete']]);
    }

    public function create() {
        //echo "hello";
        $referrals = DB::table('referralcode')->where('exclusive','partial')->get();
        return view('admin.coupons.create',compact('referrals'));
    }

    public function store(Request $request) {
        

//        if (!Input::has('discount_rate') | !Input::has('coupon_code') | !Input::has('name') | !Input::has('date_begin') | !Input::has('date_end')) {
//            Session::flash('error_msg', "All fields are required");
//            return redirect()->back()->withInput(Input::all());
//        }
        $this->validate($request, [
            'coupon_code' => 'required|unique:coupons,coupon_code',
            ]);
        $ad = new Coupons();
        $ad->description = Input::get('description');
        if (empty(Input::get('job_discount_rate'))) {
          $ad->job_discount_rate = 0;
        }else{
          $ad->job_discount_rate = Input::get('job_discount_rate');
        }
        if (empty(Input::get('packages_discount_rate'))) {
          $ad->packages_discount_rate = 0;
        }else{
          $ad->packages_discount_rate = Input::get('packages_discount_rate');
        }
        if (empty(Input::get('boost_discount_rate'))) {
         $ad->boost_discount_rate = 0;
        }else{
         $ad->boost_discount_rate = Input::get('boost_discount_rate');
        }
        if (empty(Input::get('question_discount_rate'))) {
         $ad->question_discount_rate = 0;
        }else{
         $ad->question_discount_rate = Input::get('question_discount_rate');
        }
        $ad->coupon_code = Input::get('coupon_code');
        $ad->name = Input::get('name');
        $ad->date_begin = Input::get('date_begin');
        $ad->date_end = Input::get('date_end');
        if(Input::get('exclusive') == 'partial'){
          $ad->exclusive = Input::get('exclusive');
          $ad->referral_code = Input::get('referral');
        }else{
          $ad->referral_code = NULL;
          $ad->exclusive = Input::get('exclusive');
        }
        if (empty(Input::get('is_active'))) {
         $ad->is_active = 0;
        }else{
         $ad->is_active = 1;
        }
        $ad->save();

        Session::flash('success_msg', 'Coupon Created');
        return redirect()->to('/admin/coupons/all');
    }

    public function edit($id = 0) {

        $ad = Coupons::where('id', $id)->first();

        if (!empty($ad)) {
            $referrals = DB::table('referralcode')->where('exclusive','partial')->get();
            return view('admin.coupons.edit', compact('referrals','ad'));
        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/coupons/all');
        }
    }

    public function update(Request $request) {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Coupons::where('id', Input::get('id'))->get()) > 0) {
            $ad = Coupons::where('id', Input::get('id'))->first();

            $ad->description = Input::get('description');
            if (empty(Input::get('job_discount_rate'))) {
              $ad->job_discount_rate = 0;
            }else{
              $ad->job_discount_rate = Input::get('job_discount_rate');
            }
            if (empty(Input::get('packages_discount_rate'))) {
              $ad->packages_discount_rate = 0;
            }else{
              $ad->packages_discount_rate = Input::get('packages_discount_rate');
            }
            if (empty(Input::get('boost_discount_rate'))) {
             $ad->boost_discount_rate = 0;
            }else{
             $ad->boost_discount_rate = Input::get('boost_discount_rate');
            }
            if (empty(Input::get('question_discount_rate'))) {
             $ad->question_discount_rate = 0;
            }else{
             $ad->question_discount_rate = Input::get('question_discount_rate');
            }
            $ad->coupon_code = Input::get('coupon_code');
            $ad->name = Input::get('name');
            $ad->date_begin = Input::get('date_begin');
            $ad->date_end = Input::get('date_end');
            if(Input::get('exclusive') == 'partial'){
              $ad->exclusive = Input::get('exclusive');
              $ad->referral_code = Input::get('referral');
            }else{
              $ad->exclusive = Input::get('exclusive');
              $ad->referral_code = NULL;
            }
            if (empty(Input::get('is_active'))) {
             $ad->is_active = 0;
            }else{
             $ad->is_active = 1;
            }
            $ad->save();

            Session::flash('success_msg', 'Coupon Updated');
            return redirect()->to('/admin/coupons/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/coupons/all');
        }
    }

    public function all() {

        $ads = Coupons::all();

        return view('admin.coupons.all', ['ads' => $ads]);
    }

    public function delete($id = 0) {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $ad = Coupons::where('id', $id)->first();

        if (!empty($ad)) {

            Coupons::where('id', $id)->delete();

            Session::flash('success_msg', trans('Coupon deleted Successfully'));
            return redirect()->to('/admin/coupons/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/coupons/all');
        }
    }

}
