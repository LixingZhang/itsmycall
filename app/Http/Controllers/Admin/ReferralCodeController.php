<?php

namespace App\Http\Controllers\Admin;

use App\ReferralCode;
use App\Upgrades;
use DB;
use App\PostsPackages;
use App\Packages;
use App\Transactions;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Utils;
use Input;
use Session;

class ReferralCodeController extends Controller {

    function __construct() {
        $this->middleware('has_permission:ad_sections.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:ad_sections.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
        $this->middleware('has_permission:ad_sections.delete', ['only' => ['delete']]);
    }

    public function create() {
        $packages = PostsPackages::all();
        $upgrades = Upgrades::where('status',1)->get();
        $questions = DB::table('questionspricing')->get();
        return view('admin.referralcode.create' , compact('packages','upgrades','questions'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'referral_code' => 'required|unique:referralcode,referral_code',
        ]);
        $ad = new ReferralCode();
        $ad->description = Input::get('description');
        if (Input::get('credit_amount') > 0) {
               $ad->credit_amount = Input::get('credit_amount');
            }
        else{
               $ad->credit_amount = 0;
            }
        if (Input::get('discount_rate') > 0) {
               $ad->discount_rate = Input::get('discount_rate');
            }
        else{
               $ad->discount_rate = 0;
            }
        $ad->referral_code = Input::get('referral_code');
        $ad->name = Input::get('name');
        $ad->date_begin = Input::get('date_begin');
        $ad->date_end = Input::get('date_end');
        $ad->campaign_end = Input::get('campaign_end');
        if (Input::get('credit_amount') > 0) {
            $dt = Carbon::createFromFormat('Y-m-d',Input::get('date_begin'));
            $bonus_end = $dt->addMonth(6);
            $ad->bonus_end = $bonus_end->toDateString();
        }
        $ad->exclusive = Input::get('exclusive');
        $ad->referral_alert  = Input::get('referral_alert');
        $ad->save();
        if ($request->questionid) {
            $referralid = DB::table('referralcode')->orderBy('id','DESC')->first();
            DB::table('referral_questions')->insert(
            ['id' => $request->questionid,'description' => $request->questiondescription,'price' => $request->questionprice,'referral_id' => $referralid->id,'oprice' => $request->originalpricequestion,'title' => $request->questiontitle]
        );
        }
        if ($request->upgradeid) {
        $referralid = DB::table('referralcode')->orderBy('id','DESC')->first();
        for ($i=0; $i < count($request->upgradeid); $i++) { 
           $id = $request->upgradeid[$i];
           $price = $request->upgradeprice[$i];
           $description = Input::get('upgradedescription')[$i];
           $type = $request->upgradetype[$i];
           $name = $request->upgradename[$i];
           $status = $request->upgradestatus[$i];
           $boost_number = $request->boost_number[$i];
            DB::table('referralupgrades')->insert(
            ['id' => $id,'description' => $description,'price' => $price,'type' => $type,'referral_id' => $referralid->id,'name' => $name,'status' => $status,'boost_number' => $boost_number]
        );
        }
        }
        if ($request->packageid) {
        $referralid = DB::table('referralcode')->orderBy('id','DESC')->first();
        for ($i=0; $i < count($request->packageid); $i++) { 
           $id = $request->packageid[$i];
           $price = $request->packageprice[$i];
           $description = Input::get('packagedescription')[$i];
           $title = $request->packagetitle[$i];
           $name = $request->packagename[$i];
            DB::table('referralpackages')->insert(
            ['id' => $id,'description' => $description,'price' => $price,'title' => $title,'referral_id' => $referralid->id,'name' => $name]
        );
        }
        }
        Session::flash('success_msg', 'Registration Code Created');
        return redirect()->to('/admin/referralcode/all');
    }

    public function edit($id = 0) {

        $ad = ReferralCode::where('id', $id)->first();
        $packages = DB::table('referralpackages')->where('referral_id',$id)->get();
        $upgrades = DB::table('referralupgrades')->where('referral_id',$id)->get();
        $questions = DB::table('referral_questions')->where('referral_id',$id)->get();
        if (!empty($ad)) {

            return view('admin.referralcode.edit', ['ad' => $ad],compact('packages','upgrades','questions'));
        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/referralcode/all');
        }
    }

    public function update(Request $request) {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }
        if (Input::has('id') && sizeof(ReferralCode::where('id', Input::get('id'))->get()) > 0) {
            $ad = ReferralCode::where('id', Input::get('id'))->first();

            $ad->description = Input::get('description');
            if (Input::get('discount_rate') > 0) {
               $ad->discount_rate = Input::get('discount_rate');
            }
            else{
               $ad->discount_rate = 0;
            }
            if (Input::get('credit_amount') > 0) {
               $ad->credit_amount = Input::get('credit_amount');
            }
            else{
                $ad->credit_amount = 0;
                }
            $ad->referral_code = Input::get('referral_code');
            $ad->name = Input::get('name');
            $ad->date_begin = Input::get('date_begin');
            $ad->campaign_end = Input::get('campaign_end');
            if (Input::get('credit_amount') > 0) {
                $dt = Carbon::createFromFormat('Y-m-d',Input::get('date_begin'));
                $bonus_end = $dt->addMonth(6);
                $ad->bonus_end = $bonus_end->toDateString();
            }
            $ad->date_end = Input::get('date_end');
            $ad->exclusive = Input::get('exclusive');
            $ad->referral_alert  = Input::get('referral_alert');
            $ad->save();
            if ($request->questionidtable) {
                DB::table('referral_questions')->where('idtable',$request->questionidtable)->update(
                ['description' => $request->questiondescription,'price' => $request->questionprice,'oprice' => $request->originalpricequestion,'title' => $request->questiontitle]
            );
            }
            for ($key=0; $key < count($request->packagetable); $key++) {
                   $value = $request->packagetable[$key]; 
                   $id = $request->packageid[$key];
                   $price = $request->packageprice[$key];
                   $description = Input::get('packagedescription')[$key];
                   $title = $request->packagetitle[$key];
                   $name = $request->packagename[$key];
                    DB::table('referralpackages')->where('idtable', $value)->update(
                    ['id' => $id,'description' => $description,'price' => $price,'title' => $title,'name' => $name]
                    );
            }
            for ($key=0; $key < count($request->upgradetable); $key++) { 
                           $value = $request->upgradetable[$key];
                           $id = $request->upgradeid[$key];
                           $price = $request->upgradeprice[$key];
                           $description = Input::get('upgradedescription')[$key];
                           $type = $request->upgradetype[$key];
                           $name = $request->upgradename[$key];
                           $status = $request->upgradestatus[$key];
                           $boost_number = $request->boost_number[$key];
                            DB::table('referralupgrades')->where('idtable', $value)->update(
                            ['id' => $id,'description' => $description,'price' => $price,'type' => $type,'name' => $name,'status' => $status,'boost_number' => $boost_number]
                        );
            }
            Session::flash('success_msg', 'Registration Code Updated');
            return redirect()->to('/admin/referralcode/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/referralcode/all');
        }
    }

    public function all() {

        $ads = ReferralCode::all();
        $packages = Packages::all();
        $joblevels = PostsPackages::all();
        $boosts = Upgrades::where('status',1)->get();
        $trans = Transactions::all();
        $sum = 0;
        foreach ($trans as $tran) {
            $sum = $sum+$tran->welcome_credits;
        }
        return view('admin.referralcode.all', compact('ads','packages','joblevels','boosts','sum','questions'));
    }

    public function delete($id = 0) {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $ad = ReferralCode::where('id', $id)->first();

        if (!empty($ad)) {

            ReferralCode::where('id', $id)->delete();

            Session::flash('success_msg', trans('messages.ad_deleted_success'));
            return redirect()->to('/admin/referralcode/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/referralcode/all');
        }
    }

}
