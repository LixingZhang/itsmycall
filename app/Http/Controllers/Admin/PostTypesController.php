<?php

namespace App\Http\Controllers\Admin;

use App\PostsPackages;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use Input;
use Session;

class PostTypesController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:ad_sections.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:ad_sections.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
        $this->middleware('has_permission:ad_sections.delete', ['only' => ['delete']]);
    }

    public function create()
    {

        return view('admin.post_types.create');
    }

    public function store()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('price') | !Input::has('package_name') | !Input::has('jobs_included')) {
            Session::flash('error_msg', "All fields are required");
            return redirect()->back()->withInput(Input::all());
        }
		$ad = new PostsPackages();
        $ad->package_name = Input::get('name');
            $ad->jobs_included = Input::get('description');
            $ad->price = Input::get('price');
            $ad->type = Input::get('type');
        $ad->save();

        Session::flash('success_msg', 'Post Type Created');
        return redirect()->to('/admin/post_types/all');

    }

    public function edit($id = 0)
    {

        $ad = PostsPackages::where('id', $id)->first();

        if (!empty($ad)) {

            return view('admin.post_types.edit', ['ad' => $ad]);

        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/post_types/all');

        }

    }

    public function update()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(PostsPackages::where('id', Input::get('id'))->get()) > 0) {


            $ad = PostsPackages::where('id', Input::get('id'))->first();

            $ad->title = Input::get('title');
            $ad->name = Input::get('name');
            $ad->xero_code = Input::get('xero_code');
            $ad->price = Input::get('price');
            $ad->description = Input::get('description');
            $ad->save();

            Session::flash('success_msg', 'Post Type Updated');
            return redirect()->to('/admin/post_types/all');

        } else {
            Session::flash('error_msg', 'Not Found');
            return redirect()->to('/admin/post_types/all');
        }

    }

    public function all()
    {

        $ads = PostsPackages::all();

        return view('admin.post_types.all', ['ads' => $ads]);
    }

    public function delete($id = 0)
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $ad = Ads::where('id', $id)->first();

        if (!empty($ad)) {

            Ads::where('id', $id)->delete();

            Session::flash('success_msg', trans('messages.ad_deleted_success'));
            return redirect()->to('/admin/post_types/all');

        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/post_types/all');
        }
    }

}