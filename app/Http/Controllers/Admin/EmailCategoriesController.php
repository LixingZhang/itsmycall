<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use DB;
use Input;
use Session;
use URL;

class EmailCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = DB::table('emailcategories')
                    ->get();
     return view('admin.email.all', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.email.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('category_name') | !Input::has('description') ) {
            Session::flash('error_msg', "All fields are required");
            return redirect()->back()->withInput(Input::all());
        }
        else
        $description = Input::get('description');
        $name = Input::get('category_name');
        DB::table('emailcategories')->insert([
                ['description' => $description , 'name' => $name ]
            ]);

        Session::flash('success_msg', 'Email Category Created');
        return redirect()->to('/admin/email/all');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $categories = DB::table('emailcategories')
                     ->where('id', '=', $id)
                    ->get();
        if (!empty($categories)) {

            return view('admin.email.edit', ['categories' => $categories]);

        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/email/all');

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('category_name') | !Input::has('description') ) {
            Session::flash('error_msg', "All fields are required");
            return redirect()->back()->withInput(Input::all());
        }
        else
        $description = Input::get('description');
        $name = Input::get('category_name');
        $id = Input::get('id');
        DB::table('emailcategories')
                ->where('id', $id)
                ->update(['description' => $description , 'name' => $name]);
        Session::flash('success_msg', 'Email Category Edited');
        return redirect()->to('/admin/email/all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }


        if (!empty($id)) {

        DB::table('emailcategories')->where('id', '=', $id)->delete();

            Session::flash('success_msg', 'Email Catgeory Deleted Successfully');
            return redirect()->back();

        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/email/all');
        }
    }
}
