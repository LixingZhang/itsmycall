<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Posts;
use App\Settings;
use App\Testimonials;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Input;
use Session;
use StdClass;

class SettingsController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:settings.view', ['only' => ['all']]);
        $this->middleware('has_permission:settings.general', ['only' => ['updateGeneral']]);
        $this->middleware('has_permission:settings.seo', ['only' => ['updateSEO']]);
        $this->middleware('has_permission:settings.comments', ['only' => ['updateComments']]);
        $this->middleware('has_permission:settings.socials', ['only' => ['updateSocial']]);
        $this->middleware('has_permission:settings.custom_js', ['only' => ['edit', 'updateCustomJS']]);
        $this->middleware('has_permission:settings.custom_css', ['only' => ['create', 'updateCustomCSS']]);
    }

    public function all()
    {

        $data = [];

        $data[Settings::CATEGORY_GENERAL] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $data[Settings::CATEGORY_SEO] = Utils::getSettings(Settings::CATEGORY_SEO);
        $data[Settings::CATEGORY_CARDFEE] = Utils::getSettings(Settings::CATEGORY_CARDFEE);
        $data[Settings::CATEGORY_SOCIAL] = Utils::getSettings(Settings::CATEGORY_SOCIAL);
        $data[Settings::CATEGORY_CUSTOM_JS] = Utils::getSettings(Settings::CATEGORY_CUSTOM_JS);
        
        $data[Settings::CATEGORY_CUSTOM_CSS] = Utils::getSettings(Settings::CATEGORY_CUSTOM_CSS);
        $data[Settings::CATEGORY_WELCOME] = Utils::getSettings(Settings::CATEGORY_WELCOME);


        $data['testimonials'] = Testimonials::orderBy('order')->get();
        return view('admin.settings.all', $data);
    }

    public function updateCustomCSS()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_CUSTOM_CSS,
            Settings::CATEGORY_CUSTOM_CSS,
            Input::get('custom_css'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function iconsinfo()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'business_info_img',
            Input::hasFile('business_info_img') ? Utils::imageUpload(Input::file('business_info_img'), 'images') : Input::get('business_info_img_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'agreement_info_img',
            Input::hasFile('agreement_info_img') ? Utils::imageUpload(Input::file('agreement_info_img'), 'images') : Input::get('agreement_info_img_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'home_info_img',
            Input::hasFile('home_info_img') ? Utils::imageUpload(Input::file('home_info_img'), 'images') : Input::get('home_info_img_value'),
            Settings::TYPE_STRING
        );




        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'standard_info_img',
            Input::hasFile('standard_info_img') ? Utils::imageUpload(Input::file('standard_info_img'), 'images') : Input::get('standard_info_img_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'exten_info_img',
            Input::hasFile('exten_info_img') ? Utils::imageUpload(Input::file('exten_info_img'), 'images') : Input::get('exten_info_img_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'parent_info_img',
            Input::hasFile('parent_info_img') ? Utils::imageUpload(Input::file('parent_info_img'), 'images') : Input::get('parent_info_img_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'after_info_img',
            Input::hasFile('after_info_img') ? Utils::imageUpload(Input::file('after_info_img'), 'images') : Input::get('after_info_img_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'night_info_img',
            Input::hasFile('night_info_img') ? Utils::imageUpload(Input::file('night_info_img'), 'images') : Input::get('night_info_img_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'rotno_info_img',
            Input::hasFile('rotno_info_img') ? Utils::imageUpload(Input::file('rotno_info_img'), 'images') : Input::get('rotno_info_img_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'rotyes_info_img',
            Input::hasFile('rotyes_info_img') ? Utils::imageUpload(Input::file('rotyes_info_img'), 'images') : Input::get('rotyes_info_img_value'),
            Settings::TYPE_STRING
        );

        

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'business_info',
            Input::get('business_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'home_info',
            Input::get('home_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'agreement_info',
            Input::get('agreement_info'),
            Settings::TYPE_TEXT
        );



        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'standard_info',
            Input::get('standard_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'exten_info',
            Input::get('exten_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'parent_info',
            Input::get('parent_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'after_info',
            Input::get('after_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'night_info',
            Input::get('night_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'rotno_info',
            Input::get('rotno_info'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ICONS_INFO,
            'rotyes_info',
            Input::get('rotyes_info'),
            Settings::TYPE_TEXT
        );


        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function advetisingtabs()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_company_logo',
            Input::get('tab_company_logo'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_company_name',
            Input::get('tab_company_name'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_title',
            Input::get('tab_title'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_preview',
            Input::get('tab_preview'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_main',
            Input::get('tab_main'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_selling',
            Input::get('tab_selling'),
            Settings::TYPE_TEXT
        );


        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_video',
            Input::get('tab_video'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_photos',
            Input::get('tab_photos'),
            Settings::TYPE_TEXT
        );


        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_location',
            Input::get('tab_location'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_state',
            Input::get('tab_state'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_category',
            Input::get('tab_category'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_subcategory',
            Input::get('tab_subcategory'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_salary',
            Input::get('tab_salary'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_estsalary',
            Input::get('tab_estsalary'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_hrsalinfo',
            Input::get('tab_hrsalinfo'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_annualsalinfo',
            Input::get('tab_annualsalinfo'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_incentive',
            Input::get('tab_incentive'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_freq',
            Input::get('tab_freq'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_showsal',
            Input::get('tab_showsal'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_emptype',
            Input::get('tab_emptype'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_empstatus',
            Input::get('tab_empstatus'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_workloc',
            Input::get('tab_workloc'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_shiftguide',
            Input::get('tab_shiftguide'),
            Settings::TYPE_TEXT
        );



        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_applic',
            Input::get('tab_applic'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_secemail',
            Input::get('tab_secemail'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_ADVERTISING_TABS,
            'tab_url',
            Input::get('tab_url'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function blog_text()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'blog_text',
            Input::get('blog_text'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', 'Blog Page updated Successfully');
        return redirect()->to('/admin/pages/blogs');
    }

    public function updateCustomJS()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_CUSTOM_JS,
            Settings::CATEGORY_CUSTOM_JS,
            Input::get('custom_js'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

     public function updateLiveJS()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_LIVE_JS,
            Settings::CATEGORY_LIVE_JS,
            Input::get('live_js'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function update_auscontact()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'aus_contact_discount',
            Input::get('aus_contact_discount'),
            Settings::TYPE_STRING
        );

        Session::flash('success_msg', 'Auscontact commission updated Successfully!');
        return redirect()->to('/admin/transactions/auscontact');
    }

    public function updateSocial()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'fb_page_url',
            Input::get('fb_page_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'twitter_url',
            Input::get('twitter_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'twitter_handle',
            Input::get('twitter_handle'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'linkedin_page_url',
            Input::get('linkedin_page_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'skype_username',
            Input::get('skype_username'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'youtube_channel_url',
            Input::get('youtube_channel_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'vimeo_channel_url',
            Input::get('vimeo_channel_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'addthis_js',
            Input::get('addthis_js'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'sharethis_js',
            Input::get('sharethis_js'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'sharethis_span_tags',
            Input::get('sharethis_span_tags'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'facebook_box_js',
            Input::get('facebook_box_js'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'twitter_box_js',
            Input::get('twitter_box_js'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'show_sharing',
            Input::has('show_sharing') ? 1 : 0,
            Settings::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SOCIAL,
            'show_big_sharing',
            Input::has('show_big_sharing') ? 1 : 0,
            Settings::TYPE_CHECK
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateSEO()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'seo_keywords',
            Input::get('seo_keywords'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'seo_description',
            Input::get('seo_description'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'google_verify',
            Input::get('google_verify'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'bing_verify',
            Input::get('bing_verify'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'og_image',
            Input::hasFile('og_image') ? Utils::imageUpload(Input::file('og_image'), 'images') : Input::get('og_image_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'og_url',
            Input::get('og_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'og_title',
            Input::get('og_title'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'og_description',
            Input::get('og_description'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'fb_app_id',
            Input::get('fb_app_id'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'default_job_image',
            Input::hasFile('default_job_image') ? Utils::imageUpload(Input::file('default_job_image'), 'images') : Input::get('default_job_image_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'tweet_text',
            Input::get('tweet_text'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateGeneral()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'site_url',
            Input::get('site_url'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'invoice_terms',
            Input::get('invoice_terms'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'invoice_second_warning',
            Input::get('invoice_second_warning'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'invoice_second_warning_text',
            Input::get('invoice_second_warning_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'invoice_first_warning',
            Input::get('invoice_first_warning'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'invoice_first_warning_text',
            Input::get('invoice_first_warning_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'facebook_pixel_tracking_code',
            Input::get('newfacebook'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'site_title',
            Input::get('site_title'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'max_text',
            Input::get('max_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'connect_program',
            Input::get('connect_program'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'stan_text',
            Input::get('stan_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'aus_contact_exclusive',
            Input::get('aus_contact_exclusive'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'analytics_code',
            Input::get('analytics_code'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'mailchimp_form',
            Input::get('mailchimp_form'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'logo_76',
            Input::hasFile('logo_76') ? Utils::imageUpload(Input::file('logo_76'), 'images') : Input::get('logo_76_value'),
            Settings::TYPE_STRING
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'logo_120',
            Input::hasFile('logo_120') ? Utils::imageUpload(Input::file('logo_120'), 'images') : Input::get('logo_120_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'logo_152',
            Input::hasFile('logo_152') ? Utils::imageUpload(Input::file('logo_152'), 'images') : Input::get('logo_152_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'favicon',
            Input::hasFile('favicon') ? Utils::imageUpload(Input::file('favicon'), 'images') : Input::get('favicon_value'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'site_post_as_titles',
            Input::has('site_post_as_titles') ? 1 : 0,
            Settings::TYPE_CHECK
        );


        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'timezone',
            Input::get('timezone'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'locale',
            Input::get('locale'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'front_page_text',
            Input::get('front_page_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'front_page_logo_text',
            Input::get('front_page_logo_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'register_user',
            Input::get('register_user'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'register_poster',
            Input::get('register_poster'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'register_poster_conversion',
            Input::get('register_poster_conversion'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'register_user_conversion',
            Input::get('register_user_conversion'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'general_conversion',
            Input::get('general_conversion'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'after_register_user_conversion',
            Input::get('after_register_user_conversion'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'after_register_poster_conversion',
            Input::get('after_register_poster_conversion'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
          Settings::CATEGORY_GENERAL,
          'registration_text_user',
          Input::get('registration_text_user'),
          Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
          Settings::CATEGORY_GENERAL,
          'registration_text_poster',
          Input::get('registration_text_poster'),
          Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'posts_purchase_text',
            Input::get('posts_purchase_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'posts_question_text',
            Input::get('posts_question_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'posts_select_question_text',
            Input::get('posts_select_question_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'posts_boosts_text',
            Input::get('posts_boosts_text'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'front_page_video',
            Input::get('front_page_video'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'generate_sitemap',
            Input::has('generate_sitemap') ? 1 : 0,
            Settings::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'card_fee',
            Input::get('card_fee'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'generate_rss_feeds',
            Input::has('generate_rss_feeds') ? 1 : 0,
            Settings::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'include_sources',
            Input::has('include_sources') ? 1 : 0,
            Settings::TYPE_CHECK
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }


    public function updateGeneralTerms()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'terms',
            Input::get('terms'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateGeneralAdTerms()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'adterms',
            Input::get('adterms'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }


    public function updateAbout()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'about',
            Input::get('about'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'about_seo_description',
            Input::get('about-seodescription'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'about_seo_keywords',
            Input::get('about-seokeywords'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'about_page_title',
            Input::get('about_page_title'),
            Settings::TYPE_STRING
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }
    public function homestatus()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'homestatus',
            Input::get('homestatus'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'homestatuson',
            Input::get('homestatuson'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function development()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'status_development',
            Input::get('status_development'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'development_message',
            Input::get('development_message'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function loginpage()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'default_login',
            Input::get('default_login'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'default_login_page_title',
            Input::get('default_login_page_title'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'default_login-seokeywords',
            Input::get('default_login-seokeywords'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'default_login-seodescription',
            Input::get('default_login-seodescription'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'default_login_google',
            Input::get('default_login_google'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'default_login_facebook',
            Input::get('default_login_facebook'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login',
            Input::get('seeker_login'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_page_title',
            Input::get('seeker_login_page_title'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_seokeywords',
            Input::get('seeker_login_seokeywords'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_seodescription',
            Input::get('seeker_login_seodescription'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_google',
            Input::get('seeker_login_google'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_facebook',
            Input::get('seeker_login_facebook'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login',
            Input::get('advertiser_login'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_page_title',
            Input::get('advertiser_login_page_title'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_seokeywords',
            Input::get('advertiser_login_seokeywords'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_seodescription',
            Input::get('advertiser_login_seodescription'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_google',
            Input::get('advertiser_login_google'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_facebook',
            Input::get('advertiser_login_facebook'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_verify',
            Input::get('seeker_login_verify'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_verify_page_title',
            Input::get('seeker_login_verify_page_title'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_verify_seokeywords',
            Input::get('seeker_login_verify_seokeywords'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_verify_seodescription',
            Input::get('seeker_login_verify_seodescription'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_verify_google',
            Input::get('seeker_login_verify_google'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'seeker_login_verify_facebook',
            Input::get('seeker_login_verify_facebook'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_verify',
            Input::get('advertiser_login_verify'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_verify_page_title',
            Input::get('advertiser_login_verify_page_title'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_verify_seokeywords',
            Input::get('advertiser_login_verify_seokeywords'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_verify_seodescription',
            Input::get('advertiser_login_verify_seodescription'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_verify_google',
            Input::get('advertiser_login_verify_google'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'advertiser_login_verify_facebook',
            Input::get('advertiser_login_verify_facebook'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

        public function updatefaq()
    {   

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'faq',
            Input::get('faq'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'faq_seo_description',
            Input::get('faq-seodescription'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'faq_seo_keywords',
            Input::get('faq-seokeywords'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'faq_page_title',
            Input::get('faq_page_title'),
            Settings::TYPE_STRING
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

            public function updatequality(Request $request)
    {   
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'quality',
            Input::get('quality'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'quality_seo_description',
            Input::get('quality-seodescription'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'quality_seo_keywords',
            Input::get('quality-seokeywords'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'quality_page_title',
            Input::get('quality_page_title'),
            Settings::TYPE_STRING
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

      public function updateconnect()
    {   

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'connect',
            Input::get('connect'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'connect_seo_description',
            Input::get('connect-seodescription'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'connect_seo_keywords',
            Input::get('connect-seokeywords'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'connect_page_title',
            Input::get('connect_page_title'),
            Settings::TYPE_STRING
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

     public function updateReferral()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'referral',
            Input::get('referral'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

     public function updateWelcome()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'google_adwards_tracking_code',
            Input::get('welcome-google-adwards-tracking-code'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'facebook_pixel_tracking_code',
            Input::get('welcome-facebook-pixel-tracking-code'),
            Settings::TYPE_TEXT
        );


        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'google_adwards_tracking_code-download',
            Input::get('welcome-google-adwards-tracking-code-download'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'facebook_pixel_tracking_code-download',
            Input::get('welcome-facebook-pixel-tracking-code-download'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'google_adwards_tracking_code-register',
            Input::get('welcome-google-adwards-tracking-code-register'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'facebook_pixel_tracking_code-register',
            Input::get('welcome-facebook-pixel-tracking-code-register'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_one',
            Input::get('welcome-section-one'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_two',
            Input::get('welcome-section-two'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_three',
            Input::get('welcome-section-three'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_four',
            Input::get('welcome-section-four'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_five',
            Input::get('welcome-section-five'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_six',
            Input::get('welcome-section-six'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_seven',
            Input::get('welcome-section-seven'),
            Settings::TYPE_TEXT
        );
        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'section_eight',
            Input::get('welcome-section-eight'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', 'Welcome Page updated Successfully');
        return redirect()->to('/admin/pages');
    }

    public function updateContact()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'contactus',
            Input::get('contactus'),
            Settings::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'contact_seo_description',
            Input::get('contact-seodescription'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'contact_seo_keywords',
            Input::get('contact-seokeywords'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_SEO,
            'contact_page_title',
            Input::get('contact_page_title'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'contact_recaptcha_site_key',
            Input::get('contact-recaptcha-site-key'),
            Settings::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'contact_recaptcha_secret_key',
            Input::get('contact-recaptcha-secret-key'),
            Settings::TYPE_STRING
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateGeneralPrivacy()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'privacy',
            Input::get('privacy'),
            Settings::TYPE_TEXT
        );


        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }


    public function updateGeneralPrivacyCollection()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }


        Utils::setOrCreateSettings(
            Settings::CATEGORY_GENERAL,
            'privacycollection',
            Input::get('privacycollection'),
            Settings::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateTestimonials(Request $request)
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Settings::CATEGORY_WELCOME,
            'testimonial_interval',
            Input::get('testimonial-interval'),
            Settings::TYPE_TEXT
        );

        $requestData = collect($request->only('testimonial-ids', 'testimonial-images', 'testimonial-html', 'testimonial-orders'));
        if (!is_array($requestData['testimonial-ids']) ||
            !is_array($requestData['testimonial-images']) ||
            !is_array($requestData['testimonial-html']) ||
            !is_array($requestData['testimonial-orders'])) {
            return redirect()->back()->withInput(Input::all());
        }

        $requestData->transpose()->each(function ($testimonialData) {
            $id = (int) $testimonialData[0];
            $image = $testimonialData[1];
            $html = $testimonialData[2];
            $order = (int) $testimonialData[3];

            if (empty($html) || empty($order)) {
                return redirect()->back();
            }

            if ($id) {
                $testimonial = Testimonials::find($id);
                if (!isset($testimonial)) {
                    return;
                }
            } else {
                $testimonial = new Testimonials;
            }

            if ($image) {
                $filename = uniqid() . "_file." . $image->guessClientExtension();
                $image->move(public_path() . '/uploads/images', $filename);
                $testimonial->image = $filename;
            }
            $testimonial->html = $html;
            $testimonial->order = $order;
            $testimonial->save();
        });

        Session::flash('success_msg', 'Testimonial Updated Successfully');
        return redirect()->to('/admin/pages');
    }

    public function deleteTestimonial(Request $request)
    {
        $id = $request->input('id');
        $count = Testimonials::destroy($id);
        if ($count !== 1) {
            return response()->json(['status' => 'failed'], 404);
        }
        return response()->json(['status' => 'success'], 200);
    }
}
