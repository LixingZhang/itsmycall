<?php

namespace App\Http\Controllers\Admin;

use App\Posts;
use App\Transactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Users;
use App\Packages;
use Input;
use Session;
use DB;

class TransactionsController extends Controller {

    function __construct() {
        $this->middleware('has_permission:ad_sections.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:ad_sections.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
        $this->middleware('has_permission:ad_sections.delete', ['only' => ['delete']]);
    }

    public function create() {
        //echo "hello";
        return view('admin.coupons.create');
    }

    public function store() {
        

//        if (!Input::has('discount_rate') | !Input::has('coupon_code') | !Input::has('name') | !Input::has('date_begin') | !Input::has('date_end')) {
//            Session::flash('error_msg', "All fields are required");
//            return redirect()->back()->withInput(Input::all());
//        }
        $ad = new Coupons();
        $ad->description = Input::get('description');
        $ad->discount_rate = Input::get('discount_rate');
        $ad->coupon_code = Input::get('coupon_code');
        $ad->name = Input::get('name');
        $ad->date_begin = Input::get('date_begin');
        $ad->date_end = Input::get('date_end');
        $ad->exclusive = Input::get('exclusive');
        $ad->save();

        Session::flash('success_msg', 'Coupon Created');
        return redirect()->to('/admin/coupons/all');
    }

    public function edit($id = 0) {

        $ad = Coupons::where('id', $id)->first();

        if (!empty($ad)) {

            return view('admin.coupons.edit', ['ad' => $ad]);
        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/coupons/all');
        }
    }

    public function credit($id) {
        $trans_bonus = Transactions::where('user_id',$id)->where('reason','Bonus')->get();
        $trans_refund = Transactions::where('user_id',$id)->where('reason','Refund')->get();
        $trans_transfer = Transactions::where('user_id',$id)->where('reason','Transfer')->get();
        $trans_invoice = Transactions::where('user_id',$id)->where('reason','Invoice payment')->get();
        $sum1 = 0;
        $sum2 = 0;
        $sum3 = 0;
        $sum4 = 0;
        foreach ($trans_bonus as $tran) {
          $sum1= $sum1+$tran->creditsgiven;
        }
        $result['bonus'] = $sum1;
        foreach ($trans_refund as $tran1) {
          $sum2= $sum2+$tran1->creditsgiven;
        }
        $result['refund'] = $sum2;
        foreach ($trans_transfer as $tran2) {
          $sum3= $sum3+$tran2->creditsgiven;
        }
        $result['transfer'] = $sum3;
        foreach ($trans_invoice as $tran3) {
          $sum4= $sum4+$tran3->creditsgiven;
        }
        $result['invoice'] = $sum4;
        return json_encode($result);
    }

    public function welcome($id) {
        $trans = Transactions::where('user_id',$id)->get();
        $sum = 0;
        foreach ($trans as $tran) {
          $sum = $sum+$tran->welcome_credits;
        }
        return $sum;
    }
    public function creditreason($id) {
        $trans = Transactions::where('reason',$id)->get();
        $sum = 0;
        foreach ($trans as $tran) {
          $sum = $sum+$tran->creditsgiven;
        }
        return $sum;
    }

    public function update() {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Coupons::where('id', Input::get('id'))->get()) > 0) {


            $ad = Coupons::where('id', Input::get('id'))->first();

            $ad->description = Input::get('description');
            $ad->discount_rate = Input::get('discount_rate');
            $ad->coupon_code = Input::get('coupon_code');
            $ad->name = Input::get('name');
            $ad->date_begin = Input::get('date_begin');
            $ad->date_end = Input::get('date_end');
            $ad->exclusive = Input::get('exclusive');
            $ad->save();

            Session::flash('success_msg', 'Package Updated');
            return redirect()->to('/admin/coupons/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/coupons/all');
        }
    }

    public function all() {

        $transactions = Transactions::all()->sortByDesc('created_at');
        $packages = Packages::all();
        $posts = Posts::all();
        $users = Users::all();
        $trans = Transactions::all();
        $sum = 0;
        foreach ($trans as $tran) {
            $sum = $sum+$tran->welcome_credits;
        }
        $sum2 = 0;
        foreach ($trans as $tran) {
            $sum2 = $sum2+$tran->creditsgiven;
        }
        $remainbonus = 0;
        foreach ($users as $user) {
            if ($user->bonusremaining) {
                $remainbonus = $remainbonus+$user->bonusremaining;
            }
        }
        $welcomeremain = 0;
        foreach ($users as $user) {
            if ($user->welcome_remaining) {
                $welcomeremain = $welcomeremain+$user->welcome_remaining;
            }
        }
            $this->data['transactions'] = $transactions;
            $this->data['posts'] = $posts;
            $this->data['packages'] = $packages;
            $this->data['users'] = $users;
            $this->data['sum'] = $sum;
            $this->data['sum2'] = $sum2;
            $this->data['remainbonus'] = $remainbonus;
            $this->data['welcomeremain'] = $welcomeremain;
        return view('admin.transactions.all', $this->data);
    }

    public function auscontact() {
        $packages = Packages::all();
        $posts = Posts::all();
        $users = Users::where('aus_contact_member_no','!=','')->get();
        $commission = \App\Settings::where('column_key','aus_contact_discount')->first()->value_string;
        $user_array = [];
        foreach ($users as $key) {
            $user_array[] = $key->id;
        }
        $transactions = Transactions::whereIn('user_id', $user_array)->orderBy('id','DESC')->get();
        $trans_jobs = Transactions::whereIn('user_id', $user_array)->where('listing_type','!=','')->orderBy('id','DESC')->count();
        $trans_package = Transactions::whereIn('user_id', $user_array)->where('package','!=','')->orderBy('id','DESC')->count();  
        $aus_posts = DB::table('posts')->whereIn('author_id', $user_array)->get();
        $post_array = [];
        foreach ($aus_posts as $key) {
            $post_array[] = $key->id;
        }
        $upgrades = DB::table('posts_upgrades')->whereIn('post_id', $post_array)->where('is_purchased',1)->get();
        $revenue_boost = 0;
        foreach ($upgrades as $upgrade) {
            $revenue_boost = $revenue_boost+$upgrade->count;
        }
        $transaction_revenue = DB::table('transactions')->whereIn('user_id', $user_array)->where('payment','!=','Credits')->where('payment','!=','')->get();
        $total_revenue = 0;
        foreach ($transaction_revenue as $transaction) {
            $total_revenue = $total_revenue+$transaction->totalnogst;
        }
        $trans = Transactions::where('auscontact','!=','')->get();
            $this->data['transactions'] = $transactions;
            $this->data['posts'] = $posts;
            $this->data['packages'] = $packages;
            $this->data['users'] = $users;
            $this->data['total_revenue'] = $total_revenue;
            $this->data['revenue_boost'] = $revenue_boost;
            $this->data['trans_jobs'] = $trans_jobs;
            $this->data['trans_package'] = $trans_package;
            $this->data['commission'] = $commission;
        return view('admin.transactions.auscontact', $this->data);
    }

    public function search(Request $request) {
        if (strtotime($request->date_begin) > strtotime($request->date_end)){
            Session::flash('error_msg', 'Date End field must be greater than Date Start field');
            return redirect()->back()->withInput(Input::all());
        }

        $v = \Validator::make($request->all(), [
            'date_begin' => 'required',
            'date_end' => 'required',

        ]);
        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput(Input::all());
            //return ["status" => "1", "error" => Utils::messages($v)];
        }
        $date_begin = $request->date_begin;
        $date_fin = $request->date_end;
        $packages = Packages::all();
        $posts = Posts::all();
        $users = Users::where('aus_contact_member_no','!=','')->get();
        $commission = \App\Settings::where('column_key','aus_contact_discount')->first()->value_string;
        $user_array = [];
        foreach ($users as $key) {
            $user_array[] = $key->id;
        }
        $transactions = Transactions::where('created_at','>=',$date_begin)->where('created_at','<=',$request->date_end)->whereIn('user_id', $user_array)->orderBy('id','DESC')->get();
        $trans_jobs = Transactions::where('created_at','>=',$date_begin)->where('created_at','<=',$request->date_end)->whereIn('user_id', $user_array)->where('listing_type','!=','')->orderBy('id','DESC')->count();
        $trans_package = Transactions::where('created_at','>=',$date_begin)->where('created_at','<=',$request->date_end)->whereIn('user_id', $user_array)->where('package','!=','')->orderBy('id','DESC')->count();  
        $aus_posts = DB::table('posts')->whereIn('author_id', $user_array)->get();
        $post_array = [];
        foreach ($aus_posts as $key) {
            $post_array[] = $key->id;
        }
        $upgrades = DB::table('posts_upgrades')->where('created_at','>=',$date_begin)->where('created_at','<=',$request->date_end)->whereIn('post_id', $post_array)->where('is_purchased',1)->get();
        $revenue_boost = 0;
        foreach ($upgrades as $upgrade) {
            $revenue_boost = $revenue_boost+$upgrade->count;
        }
        $transaction_revenue = DB::table('transactions')->where('created_at','>=',$date_begin)->where('created_at','<=',$request->date_end)->whereIn('user_id', $user_array)->where('payment','!=','Credits')->where('payment','!=','')->get();
        $total_revenue = 0;
        foreach ($transaction_revenue as $transaction) {
            $total_revenue = $total_revenue+$transaction->totalnogst;
        }
        $trans = Transactions::where('auscontact','!=','')->get();
            $this->data['transactions'] = $transactions;
            $this->data['posts'] = $posts;
            $this->data['packages'] = $packages;
            $this->data['users'] = $users;
            $this->data['total_revenue'] = $total_revenue;
            $this->data['revenue_boost'] = $revenue_boost;
            $this->data['trans_jobs'] = $trans_jobs;
            $this->data['trans_package'] = $trans_package;
            $this->data['commission'] = $commission;
            $this->data['date_begin'] = $date_begin;
            $this->data['date_fin'] = $date_fin;
        return view('admin.transactions.auscontactsearch', $this->data);
    }

    public function byuser($id) {

        $transactions = Transactions::where('user_id',$id)->orderBy('created_at','desc')->get();
        $user = Users::find($id);
        $packages = Packages::all();
        $posts = Posts::all();
        $users = Users::all();
        $trans = Transactions::all();
            $this->data['transactions'] = $transactions;
            $this->data['posts'] = $posts;
            $this->data['packages'] = $packages;
            $this->data['users'] = $users;
            $this->data['user'] = $user;
        return view('admin.transactions.byuser', $this->data);
    }

    public function bypost($id) {

        $transactions = Transactions::where('job_id',$id)->orderBy('created_at','desc')->get();
        $post = Posts::find($id);
        $packages = Packages::all();
        $posts = Posts::all();
        $users = Users::all();
        $trans = Transactions::all();
            $this->data['transactions'] = $transactions;
            $this->data['posts'] = $posts;
            $this->data['packages'] = $packages;
            $this->data['users'] = $users;
            $this->data['post'] = $post;
        return view('admin.transactions.bypost', $this->data);
    }

    public function delete($id = 0) {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $ad = Coupons::where('id', $id)->first();

        if (!empty($ad)) {

            Coupons::where('id', $id)->delete();

            Session::flash('success_msg', trans('messages.ad_deleted_success'));
            return redirect()->to('/admin/coupons/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/coupons/all');
        }
    }

}
