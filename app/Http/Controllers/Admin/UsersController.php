<?php

namespace App\Http\Controllers\Admin;

use App\Applicants;
use App\ApplicantsStatus;
use App\Groups;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Users;
use App\PackagesStats;
use App\Posts;
use App\ReferralCode;
use App\PostUpgrades;
use App\Subscription;
use App\UsersGroups;
use Carbon\Carbon;
use App\Transactions;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Input;
use Session;
use URL;

class UsersController extends Controller
{
    function __construct()
    {
        $this->middleware('has_permission:users.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:users.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:users.view', ['only' => ['all']]);
        $this->middleware('has_permission:users.delete', ['only' => ['delete']]);
    }

    public function create()
    {
        $countries = DB::table('countries')->get();
        $groups = DB::table('groups')->get();

        return view('admin.users.create', ['countries' => $countries, 'groups' => $groups]);
    }

    public function inboundimagescreate()
    {   
        
        return view('admin.users.inboundimageCreate');
    }

    public function inboundimagescreatevid(Request $request)
    {     
        DB::table('inboundimages')->insert([
                ['video' => $request->url ]
                    ]);
        return redirect()->to('/admin/users/inboundimages');
    }

    public function inboundimagessave(Request $request)
    {   
        if($request->image){
            $file = $request->image;
            $folder = 'inboundimg';
                if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/' . $folder . '/', $name);
                $imgpath = '/uploads/' . $folder . '/' . $name;
                }
            }
         DB::table('inboundimages')->insert([
                ['image' => $imgpath, 'url' => $request->url ]
                    ]);
        return redirect()->to('/admin/users/inboundimages');
    }

    public function inboundimages()
    {
        $images = DB::table('inboundimages')->get();

        return view('admin.users.inboundimages', ['images' => $images]);
    }

    public function inboundimagesdelete($id)
    {
        DB::table('inboundimages')->where('id', $id)->delete();

        return redirect()->to('/admin/users/inboundimages');
    }

    public function addcredits()
    {
        $users = DB::table('users')->where('activated','1')->where('deleted',0)->get();
        $activities = DB::table('creditshistory')->get();
        return view('admin.users.addcredits',compact('users','activities'));
    }

    public function creditshistory()
    {
        $activities = DB::table('creditshistory')->get();
        return view('admin.users.credithistory',compact('users','activities'));
    }

    public function transferbalance()
    {
        $users = DB::table('users_groups')
            ->join('users', 'users_groups.user_id', '=', 'users.id')
            ->where('users_groups.group_id','!=',2)
            ->where('users_groups.group_id','!=',1)
            ->where('users.deleted','0')
            ->where('users.activated','1')
            ->get();
        return view('admin.users.transferbalance',compact('users'));
    }

    public function transferhistory()
    {
        $transfers = DB::table('transfers')->get();
        return view('admin.users.transferhistory',compact('transfers'));
    }

    public function transferbalancestore(Request $request)
    {
        if($request->to && $request->from){
            $user = Users::find($request->to);
            $transfer = Users::find($request->from)->credits;
            $user_from = Users::find($request->from);
            $remainingcredits = $user_from->credits;
            Users::where('id',$user->id)->update(['credits' => $user->credits+$remainingcredits] );
            $trans = new Transactions();
            $trans->reason = 'Transfer';
            $trans->creditsgiven = $remainingcredits;
            $trans->creditsin = $user->credits;
            $trans->creditsremaing = $user->credits+$remainingcredits;
            $trans->user_id = $user->id;
            $trans->save();

            $user->bonusremaining = $remainingcredits+$user->bonusremaining;
            $user->save();
            DB::table('creditshistory')->insert([
                    ['reason' => 'Transfer', 'user_id' => $user->id, 'credits' => $remainingcredits, 'date' => Carbon::now(), 'description' => 'Transferred from ('.$user_from->id.')'.$user_from->email]
                ]);
            DB::table('transfers')->insert([
                ['fromemail' => $user_from->email, 'toemail' => $user->email, 'fromname' => $user_from->name, 'toname' => $user->name, 'credits' => $transfer, 'date' => Carbon::now(), 'description' => $request->description, 'trans_id' => $trans->id, 'from_id' => $request->from, 'to_id' => $request->to]
            ]);
            $posts = DB::table('posts')->where('author_id',$request->from)->get();
                    foreach ($posts as $post) {
                       $post_delete = DB::table('posts')->where('id',$post->id)->get();
                       DB::table('posts')
                        ->where('id', $post->id)
                        ->update(['status' => 'closed_withdrawn']);
                       DB::table('savedjobs')->where('job_id',$post->id)->delete();
                    };
            DB::table('users')
                ->where('id', $request->from)
                ->update(['deleted' => 1,'deleted_at' => Carbon::now(),'credits' => 0]);
            DB::table('package_info')->where('user_id',$request->from)->delete();
            DB::table('transactions')->where('user_id',$request->from)->delete();
            DB::table('packages_stats')->where('user_id',$request->from)->delete();
            DB::table('applicant_questions')->where('user_id',$request->from)->delete();
            DB::table('activitylog')->insert([
                ['description' => 'Account Closed and transferred to '.$user->email, 'user_id' => $request->from, 'created_at' => Carbon::now(), 'admin' => 1]
            ]);
            Session::flash('success_msg', trans('Credit Amount: $'.number_format($transfer, 2, '.', '').' transfered to user: '.$user->email.' from user: '.$user_from->email));
        }
        return redirect()->to('/admin/users/all');
    }

    public function addcreditsstore(Request $request)
    {
        $this->validate($request, [
            'credits' => 'required|numeric|min:1',
        ]);
        $user = Users::find($request->user);
        $remainingbonus = $user->bonusremaining+$request->credits;
        Users::where('id',$request->user)->update(['credits' => $request->credits+$user->credits] );
        $trans =  new Transactions();
        $trans->reason = $request->reason;
        $trans->creditsgiven = $request->credits;
        $trans->creditsin = $user->credits;
        $trans->creditsremaing = $user->credits+$request->credits;
        $trans->user_id = $request->user;
        $trans->save();

        $user->bonusremaining = $remainingbonus;
        $user->save();
        DB::table('creditshistory')->insert([
                ['reason' => $request->reason, 'user_id' => $request->user, 'credits' => $request->credits, 'date' => Carbon::now(), 'description' => $request->description]
            ]);
        Session::flash('success_msg', trans('Credit Amount: $'.$request->credits.' added to User: '.$user->email));
        return redirect()->to('/admin/users/credits');
    }

    public function addcreditsstoreuser(Request $request)
    {
        $this->validate($request, [
            'credits' => 'required|numeric|min:1',
        ]);
        $user = Users::find($request->user);
        $remainingbonus = $user->bonusremaining+$request->credits;
        Users::where('id',$request->user)->update(['credits' => $request->credits+$user->credits] );
        $trans =  new Transactions();
        $trans->reason = $request->reason;
        $trans->creditsgiven = $request->credits;
        $trans->creditsin = $user->credits;
        $trans->creditsremaing = $user->credits+$request->credits;
        $trans->user_id = $request->user;
        $trans->save();

        $user->bonusremaining = $remainingbonus;
        $user->save();
        DB::table('creditshistory')->insert([
                ['reason' => $request->reason, 'user_id' => $request->user, 'credits' => $request->credits, 'date' => Carbon::now(), 'description' => $request->description]
            ]);
        Session::flash('success_msg', trans('Credit Amount: $'.$request->credits.' added to User: '.$user->email));
        return redirect()->to('/admin/users/edit/'.$user->id);
    }

    public function store()
    {
        if(!Utils::hasWriteAccess()){
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $v = \Validator::make([
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
            'type' => Input::get('type'),
        ], [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'type' => 'required',
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::except('avatar'));
        }

        $user = new Users();
        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->avatar = Input::hasFile('avatar') ? Utils::imageUpload(Input::file('avatar')) : '';
        $user->birthday = Input::get('dob');
        $user->bio = Input::get('bio');
        $user->gender = Input::get('gender');
        $user->mobile_no = Input::get('mobile_no');
        $user->fb_url = Input::get('fb_url');
        $user->fb_page_url = Input::get('fb_page_url');
        $user->website_url = Input::get('website_url');
        $user->twitter_url = Input::get('twitter_url');
        $user->google_plus_url = Input::get('google_plus_url');
        $user->country = Input::get('country');
        $user->activated = Input::has('activate');
        $user->advertiser_type = Input::get('advertiser_type');
	    $user->credits = floatval(Input::get('credits'));
		
        if (Input::has('activate')) {
            $user->activated_at = Carbon::now();
        }

        $user->save();
        DB::table('activitylog')->insert([
                    ['description' => 'Created Account', 'user_id' => $user->id, 'created_at' => Carbon::now(), 'admin' => 1]
                ]);
        $type = Input::get('type', 1);

        $users_group = new UsersGroups();
        $users_group->group_id = $type;
        $users_group->user_id = $user->id;
        $users_group->save();

        Session::flash('success_msg', trans('messages.user_created_success'));
        return redirect()->to('/admin/users/all');

    }

    public function edit($id)
    {

        if (!is_null($id) && sizeof(Users::where('id', $id)->get()) > 0) {

            $user = Users::where('id', $id)->first();
            $countries = DB::table('countries')->get();
            $groups = DB::table('groups')->get();
            $activities = DB::table('activitylog')->where('user_id',$id)->get();
            $user->group = UsersGroups::where('user_id', $user->id)->first();
            $histories = DB::table('creditshistory')->where('user_id',$id)->get();

            $referral = ReferralCode::find($user->ref_id);
            $transaction_change = Transactions::where('user_id',$id)->where('welcome_credits','>',0)->get();
                $welcomecred = 0;
                if ($transaction_change) {
                    foreach ($transaction_change as $key) {
                    $welcomecred = $welcomecred + $key->welcome_credits;
                    }
                }


            $trans_bonus = Transactions::where('user_id',$id)->where('reason','Bonus')->get();
            $trans_refund = Transactions::where('user_id',$id)->where('reason','Refund')->get();
            $trans_transfer = Transactions::where('user_id',$id)->where('reason','Transfer')->get();
            $trans_invoice = Transactions::where('user_id',$id)->where('reason','Invoice payment')->get();
            $bonuscred_bonus = 0;
            $bonuscred_refund = 0;
            $bonuscred_transfer = 0;
            $bonuscred_invoice = 0;
            foreach ($trans_bonus as $tran) {
              $bonuscred_bonus= $bonuscred_bonus+$tran->creditsgiven;
            }
            foreach ($trans_refund as $tran1) {
              $bonuscred_refund= $bonuscred_refund+$tran1->creditsgiven;
            }
            foreach ($trans_transfer as $tran2) {
              $bonuscred_transfer= $bonuscred_transfer+$tran2->creditsgiven;
            }
            foreach ($trans_invoice as $tran3) {
              $bonuscred_invoice= $bonuscred_invoice+$tran3->creditsgiven;
            }
            return view('admin.users.edit', ['user' => $user, 'countries' => $countries, 'groups' => $groups, 'activities' => $activities, 'histories' => $histories, 'welcomecred' => $welcomecred, 'referral' => $referral, 'bonuscred_bonus' => $bonuscred_bonus, 'bonuscred_refund' => $bonuscred_refund, 'bonuscred_transfer' => $bonuscred_transfer, 'bonuscred_invoice' => $bonuscred_invoice]);

        } else {
            Session::flash('error_msg', trans('messages.user_not_found'));
            return redirect()->to('/admin/users/all');
        }

    }

    public function update()
    {
        if(!Utils::hasWriteAccess()){
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Users::where('id', Input::get('id'))->get()) > 0) {

            if (sizeof(Users::where('email', Input::get('email'))->where('id', '!=', Input::get('id'))->get()) > 0) {
                Session::flash('error_msg', 'Email already exists');
                return redirect()->back()->withInput(Input::all());
            }

            $data = [
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'type' => Input::get('type'),
            ];

            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'type' => 'required',
            ];

            if(strlen(Input::get('password')) > 0){
                $data['password'] = Input::get('password');
                $data['password_confirmation'] = Input::get('password_confirmation');

                $rules['password'] = 'required|confirmed';
                $rules['password_confirmation'] = 'required';
            }

            $v = \Validator::make($data, $rules);

            if ($v->fails()) {
                Session::flash('error_msg', Utils::messages($v));
                return redirect()->back()->withInput(Input::except('avatar'));
            }

            $user = Users::where('id', Input::get('id'))->first();
            DB::table('activitylog')->insert([
                    ['description' => 'Edited Profile', 'user_id' => Input::get('id'), 'created_at' => Carbon::now(), 'admin' => 1]
                ]);
            $user->name = Input::get('name');
            $user->invoice_terms = Input::get('invoice_term');
            $user->slug = Str::slug(Input::get('name'));
            $user->email = Input::get('email');

            if(strlen(Input::get('password')) > 0) {
                $user->password = Hash::make(Input::get('password'));
            }

            if(Input::hasFile('avatar')){
                $file = Input::file('avatar');
                $folder = 'images';
                    if ($file->isValid()) {
                    $name = $file->getClientOriginalName();
                    $file->move(public_path() . '/uploads/' . $folder . '/', $name);
                    $imgpath = URL::to('/uploads/' . $folder . '/' . $name);
                    $user->avatar = $imgpath;
                    }
            }
            $user->birthday = Input::get('dob');
            $user->bio = Input::get('bio');
            $user->gender = Input::get('gender');
            $user->mobile_no = Input::get('mobile_no');
            $user->fb_url = Input::get('fb_url');
            $user->fb_page_url = Input::get('fb_page_url');
            $user->website_url = Input::get('website_url');
            $user->twitter_url = Input::get('twitter_url');
            $user->google_plus_url = Input::get('google_plus_url');
            $user->country = Input::get('country');
            $user->activated = Input::has('activate');
            $user->deleted = Input::has('deleted');
            $user->billing_business = Input::get('billing_business');
            $user->billing_number = Input::get('billing_number');
            $user->billing_abn = Input::get('billing_abn');
            $user->billing_address = Input::get('billing_address');
            $user->billing_email = Input::get('billing_email');
            $user->advertiser_type = Input::get('advertiser_type');
            
            if (Input::has('activate')) {
                $user->activated_at = Carbon::now();
            }

            $user->save();

            $group = UsersGroups::where('user_id',$user->id)->first();

            $type = Input::get('type', 1);

            if($group->id != $type ) {

                UsersGroups::where('id',$group->id)->delete();

                $users_group = new UsersGroups();
                $users_group->group_id = $type;
                $users_group->user_id = $user->id;
                $users_group->save();
            }

            Session::flash('success_msg', trans('messages.user_updated_success'));
            return redirect()->to('/admin/users/all');
        } else {
            Session::flash('error_msg', trans('messages.user_not_found'));
            return redirect()->to('/admin/users/all');
        }

    }

    public function all()
    {
        $data = DB::table('users')
          ->whereNotNull('ref_id')
          ->where('ref_id', '!=', ' ')
          ->select('ref_id', DB::raw('COUNT(ref_id) AS occurrences'))
          ->groupBy('ref_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $users = Users::all()->sortByDesc('created_at');
        $transactions = Transactions::all();
        $message = "-";
        foreach ($users as $user) {
            $user_group = UsersGroups::where('user_id', $user->id)->first();
            $user->type = Groups::where('id', $user_group->group_id)->first();
        }
        return view('admin.users.all', ['users' => $users, 'message' => $message , 'transactions' => $transactions]);
    }

    public function inbound()
    {
        $users = Users::whereIn('profile_type', Users::$inboundProgram)->get()->sortByDesc('updated_at');
        $message = "-";

        return view('admin.users.inbound', compact('users', 'message'));
    }
    
    public function withreferral($id)
    {
        $coup = ReferralCode::where('id', $id)->get()->first();
        $message = "Posts with referral code " . $coup->referral_code . " used.";
        $users = Users::where('ref_id', $id)->get();

        foreach ($users as $user) {
            $user_group = UsersGroups::where('user_id', $user->id)->first();
            $user->type = Groups::where('id', $user_group->group_id)->first();
        }

        return view('admin.users.all', ['users' => $users, 'message' => $message]);
        
    }

    public function user($id)
    {
        $user = Users::find($id);
        $message = "-";
        $user_group = UsersGroups::where('user_id', $id)->first();
        // dd($user);
        $user->type = Groups::where('id', $user_group->group_id)->first();

        return view('admin.users.all', ['users' => [$user], 'message' => $message]);
    }

    public function auscontact()
    {
        
        $message = "Auscontact Members";
        $users = Users::all()->sortByDesc('created_at');
        $finalposts = array();

        foreach ($users as $user) {
            $user_group = UsersGroups::where('user_id', $user->id)->first();
            $user->type = Groups::where('id', $user_group->group_id)->first();

            if ($user->aus_contact_member_no != '') {
                // dd($user);
                array_push($finalposts,$user);
            }
                
        }

        return view('admin.users.ac', ['users' => $finalposts, 'message' => $message]);
        
    }

    public function applicants()
    {
        $message = "Job Applicants";
        $applicants = Applicants::all()->sortByDesc('date_created');

        return view('admin.users.applicants', compact('applicants', 'message'));
    }

    public function applicantsStatus()
    {
        $message = 'Job Applicants Status';
        $applicantsStatus = ApplicantsStatus::all();

        return view('admin.users.applicantsStatus', compact('applicantsStatus', 'message'));
    }

    public function appliedstatus()
    {
        $message = 'Customer Applied Job Status';
        $applicantsStatus = DB::table('applied_status')->get();

        return view('admin.users.applicantsStatusCustomer', compact('applicantsStatus', 'message'));
    }
    public function appliedstatusdelete($id)
    {
        $message = 'Customer Applied Job Status';
        $applicantsStatus = DB::table('applied_status')->where('id', $id)->delete();

        return redirect()->back();
    }
    public function appliedstatusCreate(Request $request)
    {   
        $message = 'Applied Job Status';
        if ($request->isMethod('POST')) {
            $this->validate($request, [
            'order' => 'required|unique:applied_status,order',
            ]);
            DB::table('applied_status')->insert(
                ['status' => Input::get('status'),'order' => Input::get('order')]
            );
            return redirect('/admin/users/applied_status');
        }

        return view('admin.users.applicantsStatusCreateCustomer', compact( 'message'));
    }
    public function appliedstatusview()
    {
        return view('admin.users.applicantsStatusCreateCustomer', compact( 'message'));
    }
    public function applicantsStatusEditCustomer($id)
    {   
        $message = 'Applied Job Status';
        $applicantsStatus = DB::table('applied_status')->where('id',$id)->first();
        return view('admin.users.applicantsStatusEditCustomer', compact('applicantsStatus', 'message'));
    }
    public function applicantsStatusUpdateCustomer(Request $request)
    {  
        $message = 'Applied Job Status';
        DB::table('applied_status')
            ->where('id', $request->id)
            ->update(['status' => $request->status,'order' => $request->order]);
        return redirect('/admin/users/applied_status');
    }
    public function applicantsStatusEdit(Request $request, $id)
    {
        $message = 'Job Applicants Status';
        $applicantsStatus = ApplicantsStatus::find($id);

        if ($request->isMethod('POST')) {
            $applicantsStatus->status = Input::get('status');
            $applicantsStatus->deleted = Input::get('deleted');
            $applicantsStatus->order = Input::get('order');
            $applicantsStatus->save();
            return redirect('/admin/users/applicants_status');
        }


        return view('admin.users.applicantsStatusEdit', compact('applicantsStatus', 'message'));
    }

    public function applicantsStatusDelete()
    {
        $message = 'Job Applicants Status';
        $applicantsStatus = ApplicantsStatus::all();

        return view('admin.users.applicantsStatus', compact('applicantsStatus', 'message'));
    }

    public function applicantsStatusView()
    {   
        $message = 'Job Applicants Status';

        return view('admin.users.applicantsStatusCreate', compact( 'message'));
    }

    public function applicantsStatusCreate(Request $request)
    {
        $message = 'Job Applicants Status';
        if ($request->isMethod('POST')) {
            $this->validate($request, [
            'order' => 'required|unique:applicants_statuses,order',
            ]);
            $applicantsStatus = new ApplicantsStatus();
            $applicantsStatus->status = Input::get('status');
            $applicantsStatus->deleted = Input::get('deleted');
            $applicantsStatus->order = Input::get('order');
            $applicantsStatus->save();
            return redirect('/admin/users/applicants_status');
        }

        return view('admin.users.applicantsStatusCreate', compact( 'message'));
    }

    public function deleteApplicant($id)
    {
        Applicants::destroy($id);
        Session::flash('success_msg', 'Applicant Deleted');

        return redirect("/admin/users/applicants");
    }

    public function delete($id)
    {
        if(!Utils::hasWriteAccess()){
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!is_null($id) && sizeof(Users::where('id', $id)->get()) > 0) {
            Subscription::where('user_id', $id)->delete();    
            Posts::where('author_id', $id)->delete();  
            PackagesStats::where('user_id', $id)->delete();  
            PostUpgrades::where('post_id', $id)->delete();  
            Users::where('id', $id)->delete();
            

            Session::flash('success_msg', trans('messages.user_deleted_success'));
            return redirect()->to('/admin/users/all');

        } else {
            Session::flash('error_msg', trans('messages.user_not_found'));
            return redirect()->to('/admin/users/all');
        }
    }

}