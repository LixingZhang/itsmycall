<?php

namespace App\Http\Controllers\Admin;

use App\PackagesStats;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Input;
use DB;
use Session;

class PackagesStatsController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
    }

    public function all()
    {
        $ads = PackagesStats::all()->sortByDesc('date_purchase');

        return view('admin.packages_stats.all', ['ads' => $ads]);
    }

    public function allnew()
    {   
        $ads = DB::table('package_info')->where('payment','Invoice')->orderBy('purchase_date','DESC')->get();
        foreach ($ads as $ad) {
            if ($ad->invoice_terms) {
                $start = new \Carbon\Carbon($ad->purchase_date);
                $date = $start->addDays($ad->invoice_terms);
                if ($date < Carbon::now() && $ad->status == 'Awaiting Payment') {
                    $user = DB::table('package_info')->where('id',$ad->id)->first();
                    $posts = DB::table('posts')->where('author_id',$user->user_id)->get();
                    DB::table('package_info')->where('id',$ad->id)->update(['status' => 'Overdue']);
                    foreach ($posts as $post) {
                       DB::table('posts')->where('id',$post->id)->update(['invoice_status' =>  $post->status,'status' => 'remove']);
                    }
                }
            }else{
                $start = new \Carbon\Carbon($ad->purchase_date);
                $check_term = \App\Users::find($ad->user_id);
                $current_term = \App\Settings::where('column_key','invoice_terms')->first();
                if($check_term && $check_term->invoice_terms){
                    $date = $start->addDays($check_term->invoice_terms);
                }elseif($current_term && $current_term->value_string){
                    $date = $start->addDays($current_term->value_string);
                }else{
                    $date = $start->addDays(7);
                }
                if ($date < Carbon::now() && $ad->status == 'Awaiting Payment') {
                    $user = DB::table('package_info')->where('id',$ad->id)->first();
                    $posts = DB::table('posts')->where('author_id',$user->user_id)->get();
                      DB::table('package_info')->where('id',$ad->id)->update(['status' => 'Overdue']);
                    foreach ($posts as $post) {
                      DB::table('posts')->where('id',$post->id)->update(['invoice_status' =>  $post->status,'status' => 'remove']);
                    }
                }
            }
        }
        $valuepacks = DB::table('package_info')->orderBy('purchase_date','DESC')->get();
        return view('admin.packages_stats.package_info', ['ads' => $valuepacks]);
    }

    public function setstatus(Request $request,$id)
    {   
        if (strlen($request->xero) > 0 || strlen($request->payment_time) > 0) {
            $request->status = 'Paid';
        }
        DB::table('package_info')->where('id',$id)->update(['status' => $request->status,'invoice_terms' => $request->invoice,'payment_time' => $request->payment_time,'xero' => $request->xero]);
        if ($request->status == 'Overdue') {
            $user = DB::table('package_info')->where('id',$id)->first();
            $posts = DB::table('posts')->where('author_id',$user->user_id)->get();
            foreach ($posts as $post) {
               DB::table('posts')->where('id',$post->id)->update(['invoice_status' =>  $post->status,'status' => 'remove']);
            }
        }
        if($request->status == 'Paid'){
            $user = DB::table('package_info')->where('id',$id)->first();
            $posts = DB::table('posts')->where('author_id',$user->user_id)->get();
            foreach ($posts as $post) {
              if($post->invoice_status != '')
               DB::table('posts')->where('id',$post->id)->update(['invoice_status' =>  NULL,'status' => $post->invoice_status]);
              }
          }
        Session::flash('success_msg', 'Package Info Updated');
        return redirect()->to('/admin/packages_stats/packageinfo');
    }

     public function editpackage($id)
    {   
        return view('admin.packages_stats.edit_package_info', ['ad' => DB::table('package_info')->where('id',$id)->first()]);
    }

     public function edit($id = 0) {

        $ad = PackagesStats::where('id', $id)->first();

        if (!empty($ad)) {

            return view('admin.packages_stats.edit', ['ad' => $ad]);
        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/packages_stats/all');
        }
    }

    public function delete($id = 0) {
        $package = PackagesStats::find($id);
        $package->deleted = true;
        $package->save();
        Session::flash('error_msg', 'Package Stats Deleted');
        return redirect()->to('/admin/packages_stats/all');
    }

    public function update() {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(PackagesStats::where('id', Input::get('id'))->get()) > 0) {

            $ad = PackagesStats::where('id', Input::get('id'))->first();

           
            $ad->date_purchase = Input::get('date_purchase');
            $ad->status = Input::get('status');
            $ad->deleted = Input::get('deleted');
            $ad->save();

            Session::flash('success_msg', 'Package Updated');
            return redirect()->to('/admin/packages_stats/all');
        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/packages_stats/all');
        }
    }


}