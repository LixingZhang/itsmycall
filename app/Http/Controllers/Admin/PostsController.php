<?php

namespace App\Http\Controllers\Admin;

use App\Applicants;
use App\Categories;
use App\PostUpgradesLists;
use App\Coupons;
use App\Settings;
use App\Countries;
use App\GalleryImage;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Packages;
use App\Posts;
use App\PostsPackages;
use App\PostTags;
use App\PostUpgrades;
use App\ReferralCode;
use Illuminate\Support\Facades\Auth;
use App\Sources;
use App\SubCategories;
use App\Tags;
use App\Transactions;
use App\Upgrades;
use App\Users;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Input;
use Session;

class PostsController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:posts.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:posts.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:posts.view', ['only' => ['all']]);
        $this->middleware('has_permission:posts.delete', ['only' => ['delete']]);
    }

    public function jobsearch()
    {
        $presets = DB::table('searchpresets')->get();
        return view('admin.posts.jobsearch',compact('presets'));
    }

    public function jobsearchdelete($id)
    {
        DB::table('searchpresets')->where('id',$id)->delete();
        Session::flash('success_msg', 'Job Search Preset deleted Successfully!');
        return redirect()->back();
    }

    public function jobsearchsave()
    {
        $users = DB::table('users')->get();
        if(Input::get('searchour') == 'yes'){
            if (Input::get('hrsalstart') >= Input::get('hrsalend')) {
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Salary Max value must be greater than Salary Min value.');
                return redirect()->back();
            }
        }else if(Input::get('salstart') != '' &&  Input::get('salend') != '' && Input::get('salstart') >= Input::get('salend')){
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Salary Max value must be greater than Salary Min value.');
                return redirect()->back();
            }
        if (!empty(Input::all())) {
            DB::table('searchpresets')
            ->insert(['title' => Input::get('title'),'country' => Input::get('country'),'state' => Input::get('state'),'category' => Input::get('category'),'subcategory' => Input::get('subcategory'),'salstart' => Input::get('salstart'),'salend' => Input::get('salend'),'hrsalstart' => Input::get('hrsalstart'),'hrsalend' => Input::get('hrsalend'),'work_from_home' => Input::get('work_from_home'),'parentsoption' => Input::get('parentsoption'),'work_from_home' => Input::get('work_from_home'),'incentivestructure1' => Input::get('incentivestructure1'),'location' => Input::get('joblocation'),'searchour' => Input::get('searchour'),'lat' => Input::get('lat'),'lng' => Input::get('lng'),'sort' => Input::get('sort'),'pay_cycle' => Input::get('pay_cycle'),'advertiser_type' => Input::get('advertiser_type'),'employment_type' => json_encode(Input::get('employment_type')),'employment_term' => json_encode(Input::get('employment_term')),'status' => Input::get('is_active'),'pagetitle' => Input::get('page_title'),'heading' => Input::get('searchheading'),'url' => Input::get('url'),'seodescription' => Input::get('seodescription'),'seokeywords' => Input::get('seokeywords'),'facebook_pixel' => Input::get('facebook-pixel-tracking-code'),'google_adwards' => Input::get('google-adwards-tracking-code'),'html_notes' => Input::get('html_notes')]);
        }

        Session::flash('success_msg', 'Job Search Preset created Successfully!');
        return redirect()->to('admin/posts/jobsearch');
    }

    public function jobsearchcreate()
    {
        $recent_jobs = Posts::active()->orderBy('id', 'desc')->paginate(10);

        $categories = Categories::get();

        $countries = Countries::get();

        $companies = Posts::groupby(['company'])->limit(20)->get();

        $states = Posts::groupby(['state'])->limit(20)->get();

        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);


        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();

        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::active()->where('category', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::active()->where('subcategory', $subcat->id)->count();
            }
        }


        $this->data['recent_jobs'] = $recent_jobs;
        $this->data['categories'] = $categories;
        $this->data['countries'] = $countries;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['states'] = $states;
        return view('admin.posts.jobsearchcreate', $this->data);
    }

    public function jobsearchedit($id)
    {
        $recent_jobs = Posts::active()->orderBy('id', 'desc')->paginate(10);

        $preset = DB::table('searchpresets')->where('id',$id)->first();

        $categories = Categories::get();

        $countries = Countries::get();

        $companies = Posts::groupby(['company'])->limit(20)->get();

        $states = Posts::groupby(['state'])->limit(20)->get();

        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);


        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();

        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::active()->where('category', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::active()->where('subcategory', $subcat->id)->count();
            }
        }


        $this->data['recent_jobs'] = $recent_jobs;
        $this->data['categories'] = $categories;
        $this->data['countries'] = $countries;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['states'] = $states;
        $this->data['preset'] = $preset;
        return view('admin.posts.jobsearchedit', $this->data);
    }

    public function jobsearchupdate($id)
    {
        $users = DB::table('users')->get();
        if(Input::get('searchour') == 'yes'){
            if (Input::get('hrsalstart') >= Input::get('hrsalend')) {
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Salary Max value must be greater than Salary Min value.');
                return redirect()->back();
            }
        }else if(Input::get('salstart') != '' &&  Input::get('salend') != '' && Input::get('salstart') >= Input::get('salend')){
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Salary Max value must be greater than Salary Min value.');
                return redirect()->back();
            }
        if (!empty(Input::all())) {
            DB::table('searchpresets')
            ->where('id',$id)
            ->update(['title' => Input::get('title'),'country' => Input::get('country'),'state' => Input::get('state'),'category' => Input::get('category'),'subcategory' => Input::get('subcategory'),'salstart' => Input::get('salstart'),'salend' => Input::get('salend'),'hrsalstart' => Input::get('hrsalstart'),'hrsalend' => Input::get('hrsalend'),'work_from_home' => Input::get('work_from_home'),'parentsoption' => Input::get('parentsoption'),'work_from_home' => Input::get('work_from_home'),'incentivestructure1' => Input::get('incentivestructure1'),'location' => Input::get('joblocation'),'searchour' => Input::get('searchour'),'lat' => Input::get('lat'),'lng' => Input::get('lng'),'sort' => Input::get('sort'),'pay_cycle' => Input::get('pay_cycle'),'advertiser_type' => Input::get('advertiser_type'),'employment_type' => json_encode(Input::get('employment_type')),'employment_term' => json_encode(Input::get('employment_term')),'status' => Input::get('is_active'),'pagetitle' => Input::get('page_title'),'heading' => Input::get('searchheading'),'url' => Input::get('url'),'seodescription' => Input::get('seodescription'),'seokeywords' => Input::get('seokeywords'),'facebook_pixel' => Input::get('facebook-pixel-tracking-code'),'google_adwards' => Input::get('google-adwards-tracking-code'),'html_notes' => Input::get('html_notes')]);
        }

        Session::flash('success_msg', 'Job Search Preset Edited Successfully!');
        return redirect()->to('admin/posts/jobsearch');
    }

    public function questions_pricing(Request $request)
    {  
        DB::table('questionspricing')->insert(
            ['price' => $request->price, 'description' => $request->description, 'title' => $request->title]
        );

        Session::flash('success_msg', 'Screening Question Pricing created Successfully!');
        return redirect()->to('admin/posts/questions_pricing/all');
    }

    public function errors()
    {
        $errors = DB::table('errors')->get();
        return view('admin.statistics.errors',compact('errors'));
    }

    public function errorsedit($id)
    {
        $error = DB::table('errors')->where('id',$id)->first();
        return view('admin.statistics.errorsedit',compact('error'));
    }

    public function errorscreate()
    {
        return view('admin.statistics.errorscreate');
    }

    public function errorsstore(Request $request)
    {
       DB::table('errors')->insert(
            ['name' => $request->title, 'description' => $request->description, 'pagetitle' => $request->page_title]
        );

        Session::flash('success_msg', 'Error Created Successfully!');
        return redirect()->to('admin/posts/errorsall');
    }

    public function errorsupdate(Request $request)
    {
       DB::table('errors')->where('id',$request->id)->update(
            ['name' => $request->title, 'description' => $request->description, 'pagetitle' => $request->page_title]
        );

        Session::flash('success_msg', 'Error updated Successfully!');
        return redirect()->to('admin/posts/errorsall');
    }

    public function errorsdelete($id)
    {
       DB::table('errors')->where('id',$id)->delete();

        Session::flash('error_msg', 'Error deleted Successfully!');
        return redirect()->to('admin/posts/errorsall');
    }

    public function questions_pricing_update(Request $request)
    {  
        DB::table('questionspricing')->where('id',$request->id)->update(
            ['price' => $request->price, 'description' => $request->description, 'title' => $request->title]
        );

        Session::flash('success_msg', 'Screening Question Pricing updated Successfully!');
        return redirect()->to('admin/posts/questions_pricing/all');
    }

    public function questions_pricing_delete($id)
    {  
        DB::table('questionspricing')->where('id',$id)->delete();

        Session::flash('success_msg', 'Screening Question Pricing deleted Successfully!');
        return redirect()->to('admin/posts/questions_pricing/all');
    }

    public function questions_pricing_view()
    {  
        return view('admin.posts.question_pricing_create');
    }

    public function questions_pricing_all()
    {  
        $questions = DB::table('questionspricing')->get();

        return view('admin.posts.questionspricing',compact('questions'));
    }

    public function questions_pricing_edit($id)
    {  
        $questions = DB::table('questionspricing')->where('id',$id)->first();

        return view('admin.posts.question_pricing_edit',compact('questions'));
    }

    public function addquestion(Request $request)
    {  
        DB::table('applicant_questions')->insert(
            ['question' => $request->question, 'user_id' => $request->user_id]
        );
        $id = DB::table('applicant_questions')->where('user_id',Auth::user()->id)->orderBy('id','DESC')->first();
        return $id->id;
    }
    public function removequestion(Request $request)
    {   DB::table('applicant_questions')->where('id',$request->id)->delete();
        return 'removed question';
    }
    public function edit_question(Request $request)
    {   DB::table('applicant_questions')->where('id',$request->id)->update(['question' => $request->question]);;
        return 'edited question';
    }
    public function create()
    {
        $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);

        $users = DB::table('users')
                    ->select('users.id', 'users.name', 'users.company')
                    ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
                    ->join('groups', 'groups.id', '=', 'users_groups.group_id')
                    ->where('groups.name', 'company_admin')->get();

        return view('admin.posts.create', [
            'categories' => Categories::all(),
            'posts_packages' => PostsPackages::orderBy('price', 'desc')->get(),
            'packages' => Packages::orderBy('price', 'desc')->get(),
            'upgrades' => Upgrades::orderBy('price', 'desc')->get(),
            'admins' => $admins,
            'users' => $users
        ]);
    }

    public function jobPost()
    {
        $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);

        return view('job_post', ['categories' => Categories::all(), 'admins' => $admins]);
    }

    public function store(Request $request)
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $v = \Validator::make($request->all(), [
            'title' => 'required',
            'featured_image' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'author_id' => 'required',
            'company' => 'required',
            'country' => 'required',
            'state' => 'required',
            'category_id' => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'incentivestructure' => 'required',
            'paycycle' => 'required',
            'employment_type' => 'required',
            'employment_term' => 'required',
            'status' => 'required',
            'expired_at' => 'date_format:"Y-m-d H:i:s"',
            'package' => 'required|integer|exists:posts_packages,id',
//            'additional_upgrades.*.upgrade_id' => 'integer|exists:upgrades,id',
//            'additional_upgrades.*.upgrade_count' => 'integer',
        ] + (!!Input::get('application_method') ? ['email_or_link' => 'required|url'] : [])
            + (Input::get('salarytype') === 'annual' ? ['salary' => 'required'] : ['hrsalary' => 'required']),
        [
            'featured_image.required' => 'Company logo is required'
        ]
        );

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }

        $post_item = new Posts();
        $post_item->paid = Input::get('paid');
        $post_item->title = Input::get('title');
        $post_item->description = Input::get('description');
        $post_item->short_description = Input::get('short_description');
        $post_item->author_id = Input::get('author_id');
        $post_item->company = Input::get('company');
        $post_item->slug = Str::slug(Input::get('title'))."-".Carbon::now()->timestamp;
        $post_item->country = Str::slug(Input::get('country'));
        $post_item->state = Str::slug(Input::get('state'));
        $post_item->category_id = Input::get('sub_category');
        $post_item->category = Str::slug(Input::get('category'));
        $post_item->subcategory = Str::slug(Input::get('sub_category'));
        //salary_fields
        $post_item->salary = str_replace( ',', '', Input::get('salary'));;
        $post_item->hrsalary = Input::get('hrsalary');
        $post_item->showsalary = Input::get('showsalary');
        $post_item->salarytype = Str::slug(Input::get('salarytype'));
        $post_item->incentivestructure = Str::slug(Input::get('incentivestructure'));
        $post_item->paycycle = Str::slug(Input::get('paycycle'));

        //location fields
        $post_item->joblocation = Input::get('joblocation');
        $post_item->postcode = Input::get('postcode');
        $post_item->lat = Input::get('lat');
        $post_item->lng = Input::get('lng');
        $post_item->state = Input::get('state');
        $post_item->showsalary = Input::get('showsalary');
        $post_item->country = Str::slug(Input::get('country'));

        $post_item->employment_type = Input::get('employment_type');
        $post_item->employment_term = Input::get('employment_term');
        $post_item->person_email = Input::get('person_email');
        $post_item->email_or_link = Input::get('email_or_link');
        $post_item->selling1 = Input::get('selling1');
        $post_item->selling2 = Input::get('selling2');
        $post_item->selling3 = Input::get('selling3');
        $post_item->work_from_home = Input::get('work_from_home');
        $post_item->parentsoption = Input::get('parentsfilter');

        if (!Input::get('application_method')) {
            $post_item->email_or_link = '';
        } else {
            $post_item->person_email = '';
        }

        $post_item->video_link = Input::get('video_link');
        $post_item->experience = Str::slug(Input::get('experience'));
        $post_item->status = Input::get('status');
        $post_item->featured_image = Utils::imageUpload(Input::file('featured_image'), 'images');
        if (Input::hasFile('job_image')) {
            $post_item->job_image = Utils::imageUpload(Input::file('job_image'), 'images');
        }
        $post_item->posts_packages = Input::get('package');
        if ($post_item->posts_packages == 1) {
            DB::table('activitylog')->insert([
                ['description' => 'Featured Job Purchase - '.Input::get('title'), 'user_id' => $post_item->author_id, 'created_at' => Carbon::now(),'admin' => 1]
            ]);
        }
        if ($post_item->posts_packages == 2) {
            DB::table('activitylog')->insert([
                ['description' => 'Premium Job Purchase - '.Input::get('title'), 'user_id' => $post_item->author_id, 'created_at' => Carbon::now(),'admin' => 1]
            ]);
        }
        if ($post_item->posts_packages == 3) {
            DB::table('activitylog')->insert([
                ['description' => 'Standard Job Purchase - '.Input::get('title'), 'user_id' => $post_item->author_id, 'created_at' => Carbon::now(),'admin' => 1]
            ]);
        }
        $post_item->expired_at = !empty(Input::get('expired_at')) ? Carbon::createFromFormat('Y-m-d H:i:s', Input::get('expired_at')) : Carbon::now()->addDays(Posts::EXPIRY_DAYS);
        $post_item->save();

//        $upgrades = Input::get('additional_upgrades');
//
//        if(is_array($upgrades)) {
//            foreach ($upgrades as $upgrade) {
//                $upgrade_id = array_get($upgrade, 'upgrade_id');
//                $upgrade_count = array_get($upgrade, 'upgrade_count');
//                if ($upgrade_id == 0 OR $upgrade_count == 0) continue;
//                $posts_upgrades = new PostUpgrades();
//                $posts_upgrades->post_id = $post_item->id;
//                $posts_upgrades->upgrade_id = $upgrade_id;
//                $posts_upgrades->count = $upgrade_count;
//                $s = $posts_upgrades->save();
//            }
//        }

        Session::flash('success_msg', trans('messages.post_created_success'));
        return redirect()->back();

    }

    public function edit($id)
    {

        if (!is_null($id) && sizeof(Posts::where('id', $id)->get()) > 0) {

            $post = Posts::where('id', $id)->first();

            $post->sub_category = SubCategories::where('id', $post->subcategory)->first();
            $post->parent_category = Categories::where('id', $post->sub_category->parent_id)->first();

            $post->gallery = GalleryImage::where('post_id', $post->id)->get();
            $postsimages = DB::table('postsimages')->where('post_id',$id)->get();
            $post_tags = PostTags::where('post_id', $post->id)->lists('tag_id');

            if (sizeof($post_tags) > 0) {
                $post->tags = Tags::whereIn('id', $post_tags)->lists('title')->toArray();
            } else {
                $post->tags = [];
            }
            $admin_questions = DB::table('applicant_questions')->where('user_id','1')->get();
            $user_questions = DB::table('applicant_questions')->where('user_id',$post->author_id)->get();
            if ($post->type == Posts::TYPE_SOURCE)
                $post->source = Sources::where('id', $post->source_id)->first();

            $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);
            $ads = [];
            $check = PostUpgrades::where('post_id',$id)->get();
            foreach ($check as $ad) {
                $job = Posts::where('id', $ad->post_id);
                if (!$job) {
                    continue;
                }
                $ad->job  = $job;
                $ads[]    = $ad;
                $lists = PostUpgradesLists::where('post_upgrades_id', $ad->id)->get();
                $count = $ad->count - $lists->count();
                $boost = \App\PostUpgrades::find($ad->id);
                if ($count > 0 && $boost->actioned == '') {
                    for ($i = 0; $i < $count; $i++) {
                        $list = new PostUpgradesLists();
                        $list->post_upgrades_id = $ad->id;
                        $list->post_datetime = Carbon::now();
                        $list->post_views = 0;
                        $list->post_clicks = 0;
                        $list->post_cost = 0;
                        if($boost->upgrade_price){
                            $list->boost_cost = $boost->upgrade_price/$boost->count;
                        }else{
                            $boostprice = \App\Upgrades::find($boost->upgrade_id);
                            $list->boost_cost = $boostprice->price/$boost->count;

                        }
                        $list->post_id = $ad->post_id;
                        $list->boost_id = $boost->upgrade_id;
                        $list->post_status = PostUpgradesLists::STATUS_NOT_ACTIONED;
                        $list->save();
                    }
                $boost->actioned = 1;
                $boost->save();
                }
            }
            $postsUpgradeLists = PostUpgradesLists::where('post_id', $id)->get();
            return view('admin.posts.edit', [
                'post' => $post,
                'categories' => Categories::all(),
                'subcategories' => SubCategories::get(),
                'admins' => $admins,
                'postimages' => $postsimages,
                'posts_packages' => PostsPackages::orderBy('price', 'desc')->get(),
                'packages' => Packages::orderBy('price', 'desc')->get(),
                'upgrades' => Upgrades::orderBy('price', 'desc')->get(),
                'admin_questions' => $admin_questions,
                'user_questions' => $user_questions,
                'postsUpgradeLists' => $postsUpgradeLists,
                'id' => $id,
            ]);

        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));
            return redirect()->to('/admin/posts/all');
        }

    }

    public function update(Request $request)
    {   
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Posts::where('id', Input::get('id'))->get()) > 0) {


//            $v = \Validator::make(['title' => Input::get('title'),
//                'description' => Input::get('description'),
//                'company' => Input::get('company'),
//                'country' => Input::get('country'),
//                'state' => Input::get('state'),
//                'category' => Input::get('category'),
//                'sub_category' => Input::get('sub_category'),
//                'salary' => Input::get('salary'),
//                'file' => Input::file('featured_image'),
//                'experience' => Input::get('experience'),
//                'status' => Input::get('status'),
//                'package' => Input::get('package'),
//                'additional_upgrades' => Input::get('additional_upgrades'),
//            ], [
//                'title' => 'required',
//                'description' => 'required',
//                'file' => 'required|image',
//                'company' => 'required',
//                'country' => 'required',
//                'state' => 'required',
//                'category' => 'required',
//                'sub_category' => 'required',
//                'salary' => 'required',
//                'experience' => 'required',
//                'status' => 'required',
//                'package' => 'required|integer|exists:posts_packages,id',
//                'additional_upgrades.*.upgrade_id' => 'integer|exists:upgrades,id',
//                'additional_upgrades.*.upgrade_count' => 'integer',
//            ]);

//            if ($v->fails()) {
//                Session::flash('error_msg', Utils::messages($v));
//                return redirect()->back()->withInput(Input::all());
//            }
                    $v = \Validator::make($request->all(), [
                        'title' => 'required',
                        'description' => 'required',
                        'company' => 'required',
                        // 'country'      => 'required',
                        'state' => 'required',
                        // 'category_id'  => 'required',
                        'category' => 'required',
                        'sub_category' => 'required',
                        'status' => 'required',
                        'employment_type' => 'required',
                        'employment_term' => 'required',
                        'parentsoption' => 'required',
                        'work_from_home' => 'required',

                    ], [
                        'work_from_home.required' => 'Please select a Job venue',
                        'parentsoption.required' => 'Please select the shift times closest to your requirements'
                    ]);
                    if (Input::get('application_method') == 1 &&  Input::get('email_or_link') == ''){
                        Session::flash('error_msg', 'Please enter the URL in which your applicants will be redirected to');
                        return redirect()->back()->withInput(Input::all());
                    }
                    if ($v->fails()) {
                        Session::flash('error_msg', Utils::messages($v));

                        return redirect()->back()->withInput(Input::all());
                        //return ["status" => "1", "error" => Utils::messages($v)];
                    }
            $id = Input::get('id');
            $post = Posts::where('id', $id)->get()->first();
                DB::table('changelog')->insert([
                        ['author_id' => $post->author_id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'status' => $post->status, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'old_post' => 1 , 'expired_at' => $post->expired_at, 'admin_change' => 1 , 'posts_packages' => $post->posts_packages, 'updated_at' => Carbon::now(), 'questions' => $post->questions, 'screening_activation' => $post->selectquestions]
                        ]);
                    
                if (!Input::get('application_method')) {
                    $question = NULL;
                        if ($request->question) {
                           $question = json_encode($request->question);
                        }
                    DB::table('changelog')->insert([
                        ['author_id' => $post->author_id ,'post_id' => $id ,'title' => Input::get('title') , 'description' => Input::get('description') , 'short_description' => Input::get('shortdescription') , 'company' => Input::get('company') , 'category' => Input::get('category')  , 'subcategory' => Input::get('sub_category')  , 'joblocation' => Input::get('joblocation') , 'state' => Input::get('state') , 'salary' => str_replace( ',', '', Input::get('salary'))  , 'hrsalary' => Input::get('hrsalary')   , 'showsalary' => Input::get('showsalary') , 'salarytype' => Input::get('salarytype') , 'incentivestructure' => Input::get('incentivestructure'), 'paycycle' => Str::slug(Input::get('paycycle')), 'status' => Input::get('status'), 'employment_type' => Input::get('employment_type'), 'employment_term' => Input::get('employment_term'), 'person_email' => Input::get('person_email'), 'email_or_link' => '', 'selling1' => Input::get('selling1'), 'selling2' => Input::get('selling2'), 'selling3' => Input::get('selling3'), 'work_from_home' => Input::get('work_from_home'), 'parentsoption' => Input::get('parentsoption'), 'expired_at' => $post->expired_at , 'admin_change' => 1  , 'posts_packages' => Input::get('package'), 'questions' => $question, 'updated_at' => Carbon::now(), 'screening_activation' => Input::get('activate_screening')]
                        ]);
                } else {
                    DB::table('changelog')->insert([
                        ['author_id' => $post->author_id ,'post_id' => $id ,'title' => Input::get('title') , 'description' => Input::get('description') , 'short_description' => Input::get('shortdescription') , 'company' => Input::get('company') , 'category' => Input::get('category')  , 'subcategory' => Input::get('sub_category')  , 'joblocation' => Input::get('joblocation')  , 'state' => Input::get('state') , 'salary' => str_replace( ',', '', Input::get('salary'))  , 'hrsalary' => Input::get('hrsalary')   , 'showsalary' => Input::get('showsalary') , 'salarytype' => Input::get('salarytype') , 'incentivestructure' => Input::get('incentivestructure'), 'paycycle' => Str::slug(Input::get('paycycle')), 'status' => Input::get('status'), 'employment_type' => Input::get('employment_type'), 'employment_term' => Input::get('employment_term'), 'person_email' => '', 'email_or_link' => Input::get('email_or_link'), 'selling1' => Input::get('selling1'), 'selling2' => Input::get('selling2'), 'selling3' => Input::get('selling3'), 'work_from_home' => Input::get('work_from_home'), 'parentsoption' => Input::get('parentsoption'), 'expired_at' => $post->expired_at , 'admin_change' => 1 , 'posts_packages' => Input::get('package'), 'updated_at' => Carbon::now()]
                        ]);
                }
            $post_item = Posts::where('id', $id)->get()->first();
            $post_item->title = Input::get('title');
            $post_item->description = Input::get('description');
            $post_item->short_description = Input::get('shortdescription');
            $post_item->company = Input::get('company');
            //$post_item->slug = Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp .  "-" . Str::slug(Input::get('category'));
            if (Input::hasFile('job_photos')) {
                $files = $request->job_photos;
                foreach ($files as $file) {
                    $timestamp = uniqid();
                    $ext = $file->guessClientExtension();
                    $name = $timestamp . "_file." . $ext;
                    $folder = 'images';
                    if (is_null($folder)) {

                        // move uploaded file from temp to uploads directory
                        if ($file->move(public_path() . '/uploads/', $name)) {
                            return URL::to('/uploads/' . $name);
                        }
                    } else {


                        if (!\File::exists(public_path() . '/uploads/' . $folder)) {
                            \File::makeDirectory(public_path() . '/uploads/' . $folder);
                        }

                        // move uploaded file from temp to uploads directory
                        if ($file->move(public_path() . '/uploads/' . $folder . '/', $name)) {
                            DB::table('postsimages')->insert([
                                ['post_id' => $id , 'jobimages' => '/uploads/' . $folder . '/' . $name ]
                            ]);
                        }
                    }

                }
            }

            $post_item->category_id = Input::get('category');
            $post_item->category = Input::get('category');
            $post_item->subcategory = Input::get('sub_category');
            $scree_purch = Transactions::where('job_id',$post->id)->where('free_upgrade','!=','')->get();
            if($post->selectquestions == NULL && Input::get('activate_screening') == 1 && count($scree_purch) == 0){
                $tran_old = Transactions::where('user_id',$post->author_id)->orderBy('id','DESC')->first();
                if($tran_old){
                    $user = Users::find($post->author_id);
                    $tran = new Transactions();
                    $quest = DB::table('questionspricing')->first();
                    $tran = new Transactions();
                    $tran->user_id = $post->author_id;
                    $tran->creditsin = $user->credits;
                    $tran->job_id = $id;
                    $tran->creditsremaing = $user->credits;
                    $tran->welcome_remaining = $tran_old->welcome_remaining;
                    $tran->bonusremaining = $tran_old->bonusremaining;
                    $tran->bonus_used = $tran_old->bonus_used;
                    $tran->welcome_used = $tran_old->welcome_used;
                    $tran->free_upgrade = 'Free Screening Questions';
                    $tran->free_upgrade_price = $quest->price;
                    $tran->save();
                }
            }
            $post_item->selectquestions = Input::get('activate_screening');
            //salary_fields
            $post_item->salary = Input::get('salary');
            $post_item->hrsalary = Input::get('hrsalary');
            $post_item->showsalary = Input::get('showsalary');
            $post_item->salarytype = Str::slug(Input::get('salarytype'));
            $post_item->incentivestructure = Input::get('incentivestructure');
            $post_item->paycycle = Str::slug(Input::get('paycycle'));

            //location fields
            $post_item->joblocation = Input::get('joblocation');
            $post_item->postcode = Input::get('postcode');
            $post_item->lat = Input::get('lat');
            $post_item->lng = Input::get('lng');
            $post_item->state = Input::get('state');
            $post_item->country = Str::slug(Input::get('country'));
            $question = NULL;
            if ($request->question) {
                   $question = json_encode($request->question);
                }
            $post_item->questions = $question;
            // $post_item->experience = Str::slug(Input::get('experience'));
            $post_item->status = Input::get('status');
            $post_item->employment_type = Input::get('employment_type');
            $post_item->employment_term = Input::get('employment_term');
            $post_item->person_email = Input::get('person_email');
            $post_item->email_or_link = Input::get('email_or_link');
            $post_item->selling1 = Input::get('selling1');
            $post_item->selling2 = Input::get('selling2');
            $post_item->selling3 = Input::get('selling3');
            $post_item->work_from_home = Input::get('work_from_home');
            $post_item->parentsoption = Input::get('parentsoption');
            $post_item->video_link = Input::get('video_link');
            $post_item->paid = Input::get('paid');
            $post_item->posts_packages = Input::get('package');
            if($post->posts_packages != Input::get('package')){
                $level = PostsPackages::find(Input::get('package'));
                $tran_old = Transactions::where('user_id',$post->author_id)->orderBy('id','DESC')->first();
                if($tran_old){
                    $user = Users::find($post->author_id);
                    $tran = new Transactions();
                    $tran->free_upgrade_package = 'Free Job Ad Upgrade';
                    $tran->user_id = $post->author_id;
                    $tran->job_id = $id;
                    $tran->creditsin = $user->credits;
                    $tran->creditsremaing = $user->credits;
                    $tran->welcome_remaining = $tran_old->welcome_remaining;
                    $tran->bonusremaining = $tran_old->bonusremaining;
                    $tran->bonus_used = $tran_old->bonus_used;
                    $tran->welcome_used = $tran_old->welcome_used;
                    $tran->free_upgrade_price = $level->title ." - $".number_format($level->price, 2, '.', '');            
                    $tran->save();
                }
            }
            $post_item->expired_at = !empty(Input::get('expired_at')) ? Carbon::createFromFormat(
                'Y-m-d H:i:s',
                Input::get('expired_at')
            ) : Carbon::now()->addDays(Posts::EXPIRY_DAYS);
            if (Input::hasFile('featured_image')) {
                $post_item->featured_image = Utils::imageUpload(Input::file('featured_image'), 'images');
            }

            if (Input::hasFile('job_image')) {
                $post_item->job_image = Utils::imageUpload(Input::file('job_image'), 'images');
            }
            $post_item->save();

            Session::flash('success_msg', trans('messages.post_updated_success'));

            return redirect()->to('/admin/posts/edit/'.$id);

        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));
            return redirect()->to('/admin/posts/all');
        }

    }

    public function all()
    {

        $posts = Posts::all()->sortByDesc('created_at');
        
        $message = "";

        foreach ($posts as $post) {

            $post->sub_category = SubCategories::where('id', $post->subcategory)->get()->first();
            $post->category = Categories::where('id', $post->category_id)->get()->first();
            $post->user = Users::where('id', $post->author_id)->get()->first();
            $apps_count = Applicants::where('job_id', $post->id)->count();
            $post->apps_count = $apps_count;
            if ($post->type == Posts::TYPE_SOURCE) {
                $post->source = Sources::where('id', $post->source_id)->first();
            }
        }

        return view('admin.posts.all', ['posts' => $posts, 'message' => $message]);
    }

    public function changelog()
    {
        $posts = DB::table('changelog')->orderBy('id','Desc')->get();
        $message = "";
        return view('admin.posts.changelog', ['posts' => $posts, 'message' => $message]);
    }

    public function userchangelog($id)
    {
        $posts = DB::table('changelog')->where('author_id',$id)->orderBy('id','Desc')->get();
        $user = Users::find($id);
        $message = "";
        return view('admin.posts.changeloguser', ['posts' => $posts,'user' => $user, 'message' => $message]);
    }

    public function postchangelog($id)
    {
        $posts = DB::table('changelog')->where('post_id',$id)->orderBy('id','Desc')->get();
        $post = Posts::find($id);
        $message = "";
        return view('admin.posts.changelogpost', ['posts' => $posts,'post' => $post, 'message' => $message]);
    }

    public function questions()
    {   
        $questions = DB::table('applicant_questions')->orderBy('id','Desc')->get();
        return view('admin.posts.questions',compact('questions'));
    }

    public function questionscreate()
    {
        return view('admin.posts.create_question');
    }

    public function questionsedit($id)
    {
        $question = DB::table('applicant_questions')->where('id',$id)->first();
        return view('admin.posts.edit_question',compact('question'));
    }

    public function questionssave(Request $request)
    {   
        if ($request->isMethod('POST')) {
            $this->validate($request, [
            'order' => 'required|unique:applicant_questions,order',
            ]);
            DB::table('applicant_questions')->insert(
                ['question' => Input::get('question'),'order' => Input::get('order'),'user_id' => Auth::user()->id]
            );
            Session::flash('success_msg', trans('Question Created Successfully!'));
            return redirect('/admin/posts/questions');
        }
    }

    public function questionsupdate(Request $request,$id)
    {   
        if ($request->isMethod('POST')) {
           DB::table('applicant_questions')
            ->where('id', $id)
            ->update(
                ['question' => Input::get('question'),'order' => Input::get('order')]
            );
            Session::flash('success_msg', trans('Question Updated Successfully!'));
            return redirect('/admin/posts/questions');
        }
    }
    public function questionsdelete($id)
    {   
            DB::table('applicant_questions')->where('id', $id)->delete();
            Session::flash('success_msg', trans('Question Deleted Successfully!'));
            return redirect('/admin/posts/questions');
    }
    
    public function withcoupon($id)
    {
        $coup = Coupons::where('id', $id)->first();
        $message = "Posts with coupon " . $coup->coupon_code . " used.";

        $transactions = Transactions::where('coupon_used', $coup->coupon_code)->get();
        $postIds = [];
        $posts = [];
        foreach ($transactions as $transaction) {
            $postIds[] = $transaction->job_id;
        }

        if (!empty($postIds)) {
            $posts = Posts::whereIn('id', $postIds)->get()->sortByDesc('created_at');
            foreach ($posts as $post) {
                $post->sub_category = SubCategories::where('id', $post->subcategory)->get()->first();
                $post->category = Categories::where('id', $post->category_id)->get()->first();
                $post->user = Users::where('id', $post->author_id)->get()->first();
                $apps_count = Applicants::where('job_id', $post->id)->count();
                $post->apps_count = $apps_count;
                if ($post->type == Posts::TYPE_SOURCE) {
                    $post->source = Sources::where('id', $post->source_id)->first();
                }
            }
        }

        return view('admin.posts.all', ['posts' => $posts, 'message' => $message]);
    }
    
    
    public function withreferral($id)
    {
        
        $posts = Posts::all()->sortByDesc('created_at');
        //var_dump($posts);
        $coup = ReferralCode::where('id', $id)->get()->first();
        $message = "Posts with referral code " . $coup->referral_code . " used.";
        $finalposts = ''; 
        
        foreach ($posts as $post) {
            $post->sub_category = SubCategories::where('id', $post->subcategory)->get()->first();
            $post->category = Categories::where('id', $post->category_id)->get()->first();
            $apps_count = Applicants::where('job_id', $post->id)->count();
            $post->apps_count = $apps_count;
            if ($post->type == Posts::TYPE_SOURCE) {
                $post->source = Sources::where('id', $post->source_id)->first();
            }
            $post->user = Users::where('id', $post->author_id)->get()->first();
            if ($post->user->ref_id == $id) {
                //echo $user->ref_id;
                //var_dump($post);
                $finalposts[] = $post;                
            }
        }
        //var_dump($finalposts);
        $posts = $finalposts;
        
        return view('admin.posts.all', ['posts' => $posts, 'message' => $message]);
    }
    
    public function auscontact()
    {
        $posts = Posts::all()->sortByDesc('created_at');
        //var_dump($posts);
        $message = "Posts made with Auscontact affiliation";
        $finalposts = array(); 
        foreach ($posts as $post) {
            $post->sub_category = SubCategories::where('id', $post->subcategory)->get()->first();
            $post->category = Categories::where('id', $post->category_id)->get()->first();
            if ($post->type == Posts::TYPE_SOURCE) {
                $post->source = Sources::where('id', $post->source_id)->first();
            }
            $post->user = Users::where('id', $post->author_id)->get()->first();
            $apps_count = Applicants::where('job_id', $post->id)->count();
            $post->apps_count = $apps_count;
            if ($post->user->aus_contact_member_no != '') {
                //echo $user->ref_id;
                //var_dump($post);
                // $finalposts[] = $post;  
                array_push($finalposts,$post);              
            }
        }
        //var_dump($finalposts);
        $posts = $finalposts;
        
        return view('admin.posts.all', ['posts' => $posts, 'message' => $message]);
    }
    
    public function unpaid()
    {
        $users = DB::table('package_info')->where('status','!=','Paid')->get();
        $userpass = [];
        foreach ($users as $user) {
            $userpass[]= $user->user_id;
        }
        $posts = Posts::whereIn('author_id', $userpass)->get()->sortByDesc('created_at');
        foreach ($posts as $post) {

            $post->sub_category = SubCategories::where('id', $post->subcategory)->get()->first();
            $post->category = Categories::where('id', $post->category_id)->get()->first();
            $post->user = Users::where('id', $post->author_id)->get()->first();
            $apps_count = Applicants::where('job_id', $post->id)->count();
            $post->apps_count = $apps_count;
            if ($post->type == Posts::TYPE_SOURCE) {
                $post->source = Sources::where('id', $post->source_id)->first();
            }
        }
        $message = "";

        return view('admin.posts.unpaid', ['posts' => $posts, 'message' => $message]);
    }

    public function delete($id)
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!is_null($id) && sizeof(Posts::where('id', $id)->get()) > 0) {

            Posts::where('id', $id)->delete();
            Applicants::where('job_id', $id)->delete();
            \DB::table('savedjobs')->where('job_id', $id)->delete();
            \DB::table('posts_upgrade_lists')->where('post_id', $id)->delete();
            \DB::table('posts_upgrades')->where('post_id', $id)->delete();
            \DB::table('changelog')->where('post_id', $id)->delete();

            Session::flash('success_msg', trans('messages.post_deleted_success'));
            return redirect()->to('/admin/posts/all');

        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));
            return redirect()->to('/admin/posts/all');
        }
    }

    public function deleteimage($id)
    {   DB::table('postsimages')->where('id', $id)->delete();
        Session::flash('success_msg', 'Post Image Deleted Succesfully!');
        return redirect()->back();
    }

    public function createUpgrade(Request $request, $id)
    {   
        if (!$id) {
            return redirect('/admin/posts');
        }

        $upgrades = Upgrades::where('status','1')->get();

        if ($request->isMethod('POST')) {
            $postsUpgrade = new PostUpgrades();
            $postsUpgrade->post_id = $id;
            $postsUpgrade->upgrade_id = Input::get('upgrade_id');
            $postsUpgrade->count = Input::get('count');
            $postsUpgrade->is_purchased = 0;
            $postsUpgrade->save();
            $post = \App\Posts::find($id);
            $tran_old = Transactions::where('user_id',$post->author_id)->orderBy('id','DESC')->first();
                if($tran_old){
                    $upgrade = \App\Upgrades::find(Input::get('upgrade_id'));
                    $user = Users::find($post->author_id);
                    $tran = new Transactions();
                    $tran->free_upgrade_boost = 'Free Boost';
                    $tran->user_id = $post->author_id;
                    $tran->job_id = $id;
                    $tran->creditsin = $user->credits;
                    $tran->creditsremaing = $user->credits;
                    $tran->welcome_remaining = $tran_old->welcome_remaining;
                    $tran->bonusremaining = $tran_old->bonusremaining;
                    $tran->bonus_used = $tran_old->bonus_used;
                    $tran->welcome_used = $tran_old->welcome_used;
                    $tran->free_upgrade_price = Input::get('count')." x ".$upgrade->name ." - $".number_format($upgrade->price, 2, '.', '');            
                    $tran->save();
                }
            Session::flash('success_msg', 'Boost Created');

            return redirect('/admin/posts');
        }

        return view('admin.posts.create-upgrade', compact('id', 'upgrades'));
    }

}