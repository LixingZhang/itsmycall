<?php

namespace App\Http\Controllers\Admin;

use App\Upgrades;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use DB;
use Input;
use Session;
use URL;

class UpgradesController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:ad_sections.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:ad_sections.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
        $this->middleware('has_permission:ad_sections.delete', ['only' => ['delete']]);
    }

    public function create()
    {

        return view('admin.upgrades.create');
    }

    public function store()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('price') | !Input::has('package_name') | !Input::has('description') | !Input::has('type')) {
            Session::flash('error_msg', "All fields are required");
            return redirect()->back()->withInput(Input::all());
        }
		$ad = new Upgrades();
			$ad->name = Input::get('package_name');
            $ad->description = Input::get('description');
            $ad->type = Input::get('type');
            $ad->boost_number = Input::get('boost_number');
            $ad->price = Input::get('price');
            $ad->actual_price = Input::get('price');
            $ad->status = 1;
			$ad->save();

        Session::flash('success_msg', 'Boost Created');
        return redirect()->to('/admin/upgrades/all');

    }

    public function edit($id = 0)
    {

        $ad = Upgrades::where('id', $id)->first();
        $images = DB::table('upgradeimages')
                    ->where('upgrade_id', '=', $id)
                    ->get();
        if (!empty($ad)) {

            return view('admin.upgrades.edit', ['ad' => $ad],compact('images'));

        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/upgrades/all');

        }

    }

    public function deleteimage($id)
    {
        DB::table('upgradeimages')->where('id', '=', $id)->delete();
        Session::flash('success_msg', 'Image Deleted');
        return redirect()->back();
    }

    public function update()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Upgrades::where('id', Input::get('id'))->get()) > 0) {

            $ad = Upgrades::where('id', Input::get('id'))->first();
            $ad->name = Input::get('name');
            $ad->price = Input::get('price');
            $ad->actual_price = Input::get('price');
            $ad->boost_number = Input::get('boost_number');
            $ad->type = Input::get('type');
            $ad->description = Input::get('description');
            $ad->status = Input::get('status');
            $ad->save();
            if(Input::hasFile('usrimg')){
            $files =Input::file('usrimg');
            $folder = 'images';
            foreach ($files as $file) {
                if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/' . $folder . '/', $name);
                $imgpath = URL::to('/uploads/' . $folder . '/' . $name);
                DB::table('upgradeimages')->insert([
                ['upgrade_id' => $ad->id , 'image' => $imgpath ]
            ]);
                }
            }}


            Session::flash('success_msg', 'Boost Updated');
            return redirect()->to('/admin/upgrades/all');

        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/upgrades/all');
        }

    }

    public function all()
    {

        $ads = Upgrades::all();

        return view('admin.upgrades.all', ['ads' => $ads]);
    }

    public function delete($id = 0)
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $ad = Upgrades::where('id', $id)->first();

        if (!empty($ad)) {

            Upgrades::where('id', $id)->delete();

            Session::flash('success_msg', trans('messages.ad_deleted_success'));
            return redirect()->to('/admin/upgrades/all');

        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/upgrades/all');
        }
    }

}