<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Settings;
use App\Pages;
use App\Testimonials;
use App\Users;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use DB;
use Input;
use URL;
use Session;
use Carbon\Carbon;

class PagesController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:pages.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:pages.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:pages.view', ['only' => ['all']]);
        $this->middleware('has_permission:pages.delete', ['only' => ['delete']]);
    }

    public function blogtext()
    {
        return view('admin.pages.blogtext');
    }

    public function blogs()
    {
        $blogs = DB::table('blogs')->get();
        return view('admin.pages.blogs',compact('blogs'));
    }

    public function blogscreate()
    {
        $categories = DB::table('blogcategories')->get();
        return view('admin.pages.blogscreate',compact('categories'));
    }

    public function blogsdelete($id)
    {
        DB::table('blogs')->where('id',$id)->delete();
        Session::flash('success_msg', 'Blog Deleted Successfully');
        return redirect()->to('/admin/pages/blogs');
    }

    public function blogsedit($id)
    {
        $categories = DB::table('blogcategories')->get();
        $blog = DB::table('blogs')->where('id',$id)->first();
        $category = DB::table('blogsubcategories')->where('parent_category',$blog->parent_category)->get();
        return view('admin.pages.blogsedit',compact('blog','categories','category'));
    }

    public function blogsupdate($id)
    {
        $file = Input::file('image');
        $folder = 'images';
            if(Input::file('image')){
                if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/' . $folder . '/', $name);
                $imgpath = URL::to('/uploads/' . $folder . '/' . $name);
                }
                DB::table('blogs')->where('id',$id)->update(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp . "-" . Str::slug(Input::get('sub_category')),'parent_category' => Input::get('parent_category'),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description'),'sub_category' => Input::get('sub_category'),'preview' => Input::get('preview'),'page' => Input::get('description'),'page_title' => Input::get('page_title'),'page_title' => Input::get('page_title'),'image' => $imgpath,'updated_at' => Carbon::now()]);
            }else{
                DB::table('blogs')->where('id',$id)->update(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp . "-" . Str::slug(Input::get('sub_category')),'parent_category' => Input::get('parent_category'),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description'),'sub_category' => Input::get('sub_category'),'preview' => Input::get('preview'),'page' => Input::get('description'),'page_title' => Input::get('page_title'),'page_title' => Input::get('page_title'),'updated_at' => Carbon::now(),'share' => Input::get('share')]);
            }
        Session::flash('success_msg', 'Blog Edited Successfully');
        return redirect()->to('/admin/pages/blogs');
    }

    public function categories()
    {
        $categories = DB::table('blogcategories')->get();
        return view('admin.pages.blogcategory',compact('categories'));
    }

    public function blogsstore()
    {
        $file = Input::file('image');
        $folder = 'images';
            if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/' . $folder . '/', $name);
                $imgpath = URL::to('/uploads/' . $folder . '/' . $name);
                }
        DB::table('blogs')->insert(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp . "-" . Str::slug(Input::get('sub_category')),'parent_category' => Input::get('parent_category'),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description'),'sub_category' => Input::get('sub_category'),'preview' => Input::get('preview'),'page' => Input::get('description'),'page_title' => Input::get('page_title'),'page_title' => Input::get('page_title'),'image' => $imgpath,'created_at' => Carbon::now(),'share' => Input::get('share')]);
        Session::flash('success_msg', 'Blog created Successfully');
        return redirect()->to('/admin/pages/blogs');
    }

    public function subcategories()
    {
        $subcategories = DB::table('blogsubcategories')->get();
        return view('admin.pages.blogsubcategory',compact('subcategories'));
    }

    public function subcategoriesdelete($id)
    {
        DB::table('blogsubcategories')->where('id',$id)->delete();
        Session::flash('success_msg', 'Sub Category Deleted Successfully');
        return redirect()->to('/admin/pages/blogs/subcategories');
    }

    public function subcategoriesedit($id)
    {
        $parent_categories = DB::table('blogcategories')->get();
        $category = DB::table('blogsubcategories')->where('id',$id)->first();
        return view('admin.pages.blogsubcategoryedit',compact('category','parent_categories'));
    }

    public function subcategoriesupdate($id)
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('title')) {
            Session::flash('error_msg', trans('messages.category_title_required'));
            return redirect()->back()->withInput(Input::all());
        }

        DB::table('blogsubcategories')->where('id',$id)->update(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')),'parent_category' => Input::get('parent_category'),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description')]);

        Session::flash('success_msg', 'Sub Category Edited Successfully');
        return redirect()->to('/admin/pages/blogs/subcategories');

    }

    public function subcategoriescreate()
    {
        $parent_categories = DB::table('blogcategories')->get();
        return view('admin.pages.blogsubcategorycreate',compact('parent_categories'));
    }

    public function subcategoriesstore()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('title')) {
            Session::flash('error_msg', trans('messages.category_title_required'));
            return redirect()->back()->withInput(Input::all());
        }

        DB::table('blogsubcategories')->insert(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')),'parent_category' => Input::get('parent_category'),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description')]);

        Session::flash('success_msg', 'Sub Category created Successfully');
        return redirect()->to('/admin/pages/blogs/subcategories');

    }

    public function categoriesdelete($id)
    {
        DB::table('blogcategories')->where('id',$id)->delete();
        Session::flash('success_msg', 'Category Deleted Successfully');
        return redirect()->to('/admin/pages/blogs/categories');
    }

    public function categoriesedit($id)
    {
        $category = DB::table('blogcategories')->where('id',$id)->first();
        return view('admin.pages.blogcategoryedit',compact('category'));
    }

    public function categoriesupdate($id)
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('title')) {
            Session::flash('error_msg', trans('messages.category_title_required'));
            return redirect()->back()->withInput(Input::all());
        }

        DB::table('blogcategories')->where('id',$id)->update(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description')]);

        Session::flash('success_msg', 'Category Edited Successfully');
        return redirect()->to('/admin/pages/blogs/categories');

    }

    public function categoriescreate()
    {

        return view('admin.pages.blogcategorycreate');
    }

    public function categoriesstore()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('title')) {
            Session::flash('error_msg', trans('messages.category_title_required'));
            return redirect()->back()->withInput(Input::all());
        }

        DB::table('blogcategories')->insert(['title' => Input::get('title'),'slug' => Str::slug(Input::get('title')),'seo_keywords' => Input::get('seo_keywords'),'seo_description' => Input::get('seo_description')]);

        Session::flash('success_msg', trans('messages.category_created_success'));
        return redirect()->to('/admin/pages/blogs/categories');

    }


    public function create()
    {
        $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);
        $testimonials = Testimonials::orderBy('order')->get();

        return view('admin.pages.create', ['categories' => Categories::all(), 'admins' => $admins, 'testimonials' => $testimonials]);
    }

     public function testimonial()
    {
        $data[Settings::CATEGORY_WELCOME] = Utils::getSettings(Settings::CATEGORY_WELCOME);
        $data['testimonials'] = Testimonials::orderBy('order')->get();

        return view('admin.pages.testimonial', $data);
    }

    public function welcome()
    {
        $data[Settings::CATEGORY_WELCOME] = Utils::getSettings(Settings::CATEGORY_WELCOME);
        $data['testimonials'] = Testimonials::orderBy('order')->get();

        return view('admin.pages.welcome', $data);
    }

    public function store(Request $request)
    {
        if(!Utils::hasWriteAccess()){
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $this->validate($request, [
            'url' => 'required|unique:campaign_pages,url',
        ]);
        $testimonial = json_encode($request->testimonial);
        DB::table('campaign_pages')->insert(
            ['url' => Input::get('url'),'section1' => Input::get('section1'),'section2' => Input::get('section2'),'section3' => Input::get('section3'),'section4' => Input::get('section4'),'section5' => Input::get('section'),'section6' => Input::get('section6'),'section7' => Input::get('section7'),'section8' => Input::get('section8'),'status' => Input::get('is_active'),'testimonial' => $testimonial,'facebook' => Input::get('welcome-facebook-pixel-tracking-code'),'google' => Input::get('welcome-google-adwards-tracking-code'),'page_title' => Input::get('page_title'),'seodescription' => Input::get('seodescription'),'seokeywords' => Input::get('seokeywords'),'google_download' => Input::get('google_download'),'facebook_download' => Input::get('facebook_download'),'google_register' => Input::get('google_register'),'facebook_register' => Input::get('facebook_register'),'video' => Input::get('video'),'autoplay' => Input::get('autoplay')]
            );


        Session::flash('success_msg', trans('Campaign Page created Successfully'));
        return redirect()->to('/admin/pages/all');

    }

    public function duplicate($id)
    {
        $page = DB::table('campaign_pages')->where('id',$id)->first();
        DB::table('campaign_pages')->insert(
            ['url' => $page->url,'section1' => $page->section1,'section2' => $page->section2,'section3' => $page->section3,'section4' => $page->section4,'section5' => $page->section5,'section6' => $page->section6,'section7' => $page->section7,'section8' => $page->section8,'status' => $page->status,'testimonial' => $page->testimonial,'facebook' => $page->facebook,'google' => $page->google,'page_title' => $page->page_title,'seodescription' => $page->seodescription,'seokeywords' => $page->seokeywords,'google_download' => $page->google_download,'facebook_download' => $page->facebook_download,'google_register' => $page->google_register,'facebook_register' => $page->facebook_register,'video' => $page->video,'autoplay' => $page->autoplay]
            );


        Session::flash('success_msg', trans('Campaign Page (Id: '. $page->id .') Duplicated Successfully'));
        return redirect()->to('/admin/pages/all');

    }

    public function edit($id)
    {

        $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);
        $testimonials = Testimonials::orderBy('order')->get();

            $page = DB::table('campaign_pages')->where('id', $id)->first();
            return view('admin.pages.edit', ['page' => $page,'admins' => $admins, 'testimonials' => $testimonials]);

    }

    public function update(Request $request)
    {   
        if(!Utils::hasWriteAccess()){
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }
            $testimonial = json_encode($request->testimonial);
            DB::table('campaign_pages')
            ->where('id', $request->id)
            ->update(['url' => Input::get('url'),'section1' => Input::get('section1'),'section2' => Input::get('section2'),'section3' => Input::get('section3'),'section4' => Input::get('section4'),'section5' => Input::get('section5'),'section6' => Input::get('section6'),'section7' => Input::get('section7'),'section8' => Input::get('section8'),'status' => Input::get('is_active'),'testimonial' => $testimonial,'facebook' => Input::get('welcome-facebook-pixel-tracking-code'),'google' => Input::get('welcome-google-adwards-tracking-code'),'page_title' => Input::get('page_title'),'seodescription' => Input::get('seodescription'),'seokeywords' => Input::get('seokeywords'),'google_download' => Input::get('google_download'),'facebook_download' => Input::get('facebook_download'),'google_register' => Input::get('google_register'),'facebook_register' => Input::get('facebook_register'),'video' => Input::get('video'),'autoplay' => Input::get('autoplay')]);

            Session::flash('success_msg', trans('messages.page_updated_success'));
            return redirect()->to('/admin/pages/all');

    }

    public function all()
    {

        $pages = DB::table('campaign_pages')->get();

        return view('admin.pages.all', ['pages' => $pages]);
    }

    public function delete($id)
    {
        if(!Utils::hasWriteAccess()){
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!is_null($id)) {
            DB::table('campaign_pages')
            ->where('id', $id)
            ->delete();

            Session::flash('success_msg', trans('messages.page_deleted_success'));
            return redirect()->to('/admin/pages/all');

        } else {
            Session::flash('error_msg', trans('messages.page_not_found'));
            return redirect()->to('/admin/pages/all');
        }
    }

}