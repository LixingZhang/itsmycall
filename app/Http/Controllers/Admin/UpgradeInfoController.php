<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\Utils;
use App\PostUpgradesLists;
use App\PostUpgrades;
use App\Upgrades;
use App\Transactions;
use App\Posts;
use App\Users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Input;
use Session;
use DB;
use Illuminate\Http\Request;

class UpgradeInfoController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
    }

    public function boosts()
    {
        $posts = [];
        foreach (PostUpgrades::all()->sortByDesc('created_at') as $ad) {
            $post = Posts::where('id', $ad->post_id)->first();
            if (!$post) {
                continue;
            }
            $posts[] = $ad->post_id;

            $lists = PostUpgradesLists::where('post_upgrades_id', $ad->id)->get();
            $count = $ad->count - $lists->count();
            $boost = \App\PostUpgrades::find($ad->id);
            if ($count > 0 && $boost->actioned == '') {
                for ($i = 0; $i < $count; $i++) {
                    $list = new PostUpgradesLists();
                    $list->post_upgrades_id = $ad->id;
                    $list->post_datetime = Carbon::now();
                    $list->post_views = 0;
                    $list->post_clicks = 0;
                    $list->post_cost = 0;
                    if($boost->upgrade_price){
                        $list->boost_cost = $boost->upgrade_price/$boost->count;
                    }else{
                        $boostprice = \App\Upgrades::find($boost->upgrade_id);
                        $list->boost_cost = $boostprice->price/$boost->count;

                    }
                    $list->post_id = $ad->post_id;
                    $list->boost_id = $boost->upgrade_id;
                    $list->post_status = PostUpgradesLists::STATUS_NOT_ACTIONED;
                    $list->save();
                }
            $boost->actioned = 1;
            $boost->save();
            }
        }
        $postsall = DB::table('posts')
            ->whereIn('id',$posts)
            ->get();
        return view('admin.booststat.all', ['posts' => $postsall]);
    }

    public function boostslist($id) {
        if (!$id) {
            return redirect('/admin/boost_manage/all');
        }

        $postsUpgradeLists = PostUpgradesLists::where('post_id', $id)->get();
        if (!$postsUpgradeLists) {
            return redirect('/admin/boost_manage/all');
        }

        return view('admin.booststat.list', compact('postsUpgradeLists', 'id'));
    }

    public function boostedit(Request $request, $id) {
        /** @var PostUpgradesLists $postsUpgradeList */
        $postsUpgradeList = PostUpgradesLists::find($id);
        if(!$postsUpgradeList) {
            return redirect('/admin/boost_manage/all');
        }

        if ($request->isMethod('POST')) {
            $postsUpgradeList->post_datetime = Input::get('post_datetime') ? Carbon::createFromFormat(config('app.datetime_format.php'), Input::get('post_datetime')) : Carbon::now();
            $postsUpgradeList->post_status = Input::get('post_status');
            $postsUpgradeList->post_views = Input::get('post_views');
            $postsUpgradeList->post_clicks = Input::get('post_clicks');
            $postsUpgradeList->post_cost = Input::get('post_cost');
            $postsUpgradeList->post_edition = Input::get('post_edition') == 'The Pulse Newsletter' ? Input::get('post_edition') . ' #' . Input::get('pulse_edition') : Input::get('post_edition');
            $postsUpgradeList->post_comment = Input::get('post_comment');
            $postsUpgradeList->internal_comment = Input::get('internal_comment');
            if (Input::hasFile('boost_image')) {
                $postsUpgradeList->boost_image = Utils::imageUpload(Input::file('boost_image'), 'images');
            }
            $postsUpgradeList->save();
            Session::flash('success_msg', 'Boost Updated');

            return redirect("/admin/boost_manage/list/{$postsUpgradeList->post_id}");
        }

        return view('admin.booststat.edit-list', compact('postsUpgradeList'));
    }

    public function createUpgrade(Request $request, $id)
    {   
        if (!$id) {
            return redirect('/admin/boost_manage/all');
        }

        $upgrades = Upgrades::where('status','1')->get();

        if ($request->isMethod('POST')) {
            $postsUpgrade = new PostUpgrades();
            $postsUpgrade->post_id = $id;
            $postsUpgrade->upgrade_id = Input::get('upgrade_id');
            $postsUpgrade->count = Input::get('count');
            $postsUpgrade->is_purchased = 0;
            $postsUpgrade->save();
            $post = \App\Posts::find($id);
            $tran_old = Transactions::where('user_id',$post->author_id)->orderBy('id','DESC')->first();
                if($tran_old){
                    $upgrade = \App\Upgrades::find(Input::get('upgrade_id'));
                    $user = Users::find($post->author_id);
                    $tran = new Transactions();
                    $tran->free_upgrade_boost = 'Free Boost';
                    $tran->user_id = $post->author_id;
                    $tran->job_id = $id;
                    $tran->creditsin = $user->credits;
                    $tran->creditsremaing = $user->credits;
                    $tran->welcome_remaining = $tran_old->welcome_remaining;
                    $tran->bonusremaining = $tran_old->bonusremaining;
                    $tran->bonus_used = $tran_old->bonus_used;
                    $tran->welcome_used = $tran_old->welcome_used;
                    $tran->free_upgrade_price = Input::get('count')." x ".$upgrade->name ." - $".number_format($upgrade->price, 2, '.', '');            
                    $tran->save();
                }
            Session::flash('success_msg', 'Boost Created');

            return redirect('/admin/boost_manage/all');
        }

        return view('admin.booststat.add', compact('id', 'upgrades'));
    }

    public function boostdelete($id,$jobid)
    {   
        PostUpgradesLists::destroy($id);
        Session::flash('success_msg', 'Boost Deleted');

        return redirect("/admin/boost_manage/list/{$jobid}");
    }
}