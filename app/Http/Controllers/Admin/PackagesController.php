<?php

namespace App\Http\Controllers\Admin;

use App\Packages;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use Input;
use DB;
use Session;

class PackagesController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:ad_sections.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:ad_sections.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:ad_sections.view', ['only' => ['all']]);
        $this->middleware('has_permission:ad_sections.delete', ['only' => ['delete']]);
    }

    public function create()
    {

        return view('admin.packages.create');
    }

    public function store()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('price') | !Input::has('package_name')) {
            Session::flash('error_msg', "All fields are required");
            return redirect()->back()->withInput(Input::all());
        }
		$ad = new Packages();
        $ad->package_name = Input::get('package_name');
        $ad->status = 0; 
            $ad->price = Input::get('price');
            if (Input::get('discount_percent') > 0) {
                $ad->discount_percent = Input::get('discount_percent');
            }else{
                $ad->discount_percent = 0;
            }
			$ad->description = Input::get('description');
        $ad->save();

        Session::flash('success_msg', 'Package Created');
        return redirect()->to('/admin/packages/all');

    }

    public function edit($id = 0)
    {

        $ad = Packages::where('id', $id)->first();

        if (!empty($ad)) {

            return view('admin.packages.edit', ['ad' => $ad]);

        } else {

            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/packages/all');

        }

    }

    public function update()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Packages::where('id', Input::get('id'))->get()) > 0) {

            $ad = Packages::where('id', Input::get('id'))->first();
            $ad->package_name = Input::get('package_name');
            $ad->price = Input::get('price');
            if(Input::get('status')){ 
                $ad->status = Input::get('status'); 
            }else{
                $ad->status = 0; 
            }
            if (Input::get('discount_percent') > 0) {
                $ad->discount_percent = Input::get('discount_percent');
            }else{
                $ad->discount_percent = 0;
            }
            $ad->description = Input::get('description');
            $ad->save();

            Session::flash('success_msg', 'Package Updated');
            return redirect()->to('/admin/packages/all');

        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/packages/all');
        }

    }

    public function all()
    {

        $ads = Packages::where('status',0)->get();

        return view('admin.packages.all', ['ads' => $ads]);
    }

    public function delete($id = 0)
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $ad = Packages::where('id', $id)->first();

        if (!empty($ad)) {

            DB::table('packages')->where('id', $id)->update(
                ['status' => 1]
            );;

            Session::flash('success_msg', 'Value Pack Deleted!');
            return redirect()->to('/admin/packages/all');

        } else {
            Session::flash('error_msg', trans('messages.ad_not_found'));
            return redirect()->to('/admin/packages/all');
        }
    }

}