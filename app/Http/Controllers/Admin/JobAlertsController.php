<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use Input;
use Session;
use App\Users;
use App\Subscription;
use App\SubCategories;
use App\Categories;

class JobAlertsController extends Controller
{

    function __construct()
    {
        $this->middleware('has_permission:posts.add', ['only' => ['create', 'store']]);
        $this->middleware('has_permission:posts.edit', ['only' => ['edit', 'update']]);
        $this->middleware('has_permission:posts.view', ['only' => ['all']]);
        $this->middleware('has_permission:posts.delete', ['only' => ['delete']]);
    }

    public function all()
    {

        $job_alerts = Subscription::all();

        foreach ($job_alerts as $job_alert) {
            $job_alert->employment_term = str_replace('"', "", $job_alert->employment_term);
            $job_alert->employment_term = str_replace('[', "", $job_alert->employment_term);
            $job_alert->employment_term = str_replace(']', "", $job_alert->employment_term);
            $job_alert->employment_term = str_replace(',', ", ", $job_alert->employment_term);
            $job_alert->incentivestructure = str_replace('"', "", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace('[', "", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace(']', "", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace('"', "", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace(',', ", ", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace('fixed', "Base Salary including Superannuation", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace('basecommbonus', "Base Salary, Superannuation and performance related bonuses", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace('basecomm', "Base Salary, Superannuation and commissions", $job_alert->incentivestructure);
            $job_alert->incentivestructure = str_replace('commonly', "Commissions only", $job_alert->incentivestructure);
            $job_alert->sub_category_id = str_replace('"', "", $job_alert->sub_category_id);
            $job_alert->sub_category_id = str_replace('[', "", $job_alert->sub_category_id);
            $job_alert->sub_category_id = str_replace(']', "", $job_alert->sub_category_id);
            $job_alert->sub_category_id = explode(",",$job_alert->sub_category_id);
            $job_alert->sub_category_id = SubCategories::whereIn('id', $job_alert->sub_category_id)->get();
            $job_alert->category_id = Categories::where('id', $job_alert->category_id)->get()->first();
            $job_alert->user_id = Users::where('id', $job_alert->user_id)->get()->first();
        }

        return view('admin.job_alerts.all', ['job_alerts' => $job_alerts]);
    }

    public function delete($id = 0)
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $job_alert = Subscription::where('id', $id)->first();

        if (!empty($job_alert)) {

            Subscription::where('id', $id)->delete();

            Session::flash('success_msg', "Job alert deleted.");
            return redirect()->to('/admin/job_alerts');

        } else {
            Session::flash('error_msg', 'Job alert not found.');
            return redirect()->to('/admin/job_alerts');
        }
    }

}