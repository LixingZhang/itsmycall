<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Posts;
use App\Sources;
use App\Subscription;
use App\Users;
use File;
use Input;
use Session;
use SplFileInfo;
use URL;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{

    public function updateApplication()
    {
        \Artisan::call('cache:clear');
        \Artisan::call('migrate',['--force'=>'yes']);

        Session::flash('success_msg', 'Application successfully updated');

        return redirect()->back();
    }

    public function giveMeWriteAccess()
    {
        Session::put('GIVE-ME-WRITE-ACCESS', true);
        return 'Done dana done now u have write access';
    }

    public function removeWriteAccess()
    {
        Session::forget('GIVE-ME-WRITE-ACCESS');
        return 'Nice to meet you , see you later ba bye';
    }

    public function reports()
    {
        $continue = false;
        $date_begin = false;
        $date_fin = false;
        return view('admin.reports', compact('continue','date_begin','date_fin'));
    }

    public function getreports(Request $request)
    {   
        if ($request->date_begin > $request->date_end){
            Session::flash('error_msg', 'Date End field must be greater than Date Start field');
            return redirect()->back()->withInput(Input::all());
        }

        $v = \Validator::make($request->all(), [
            'date_begin' => 'required',
            'date_end' => 'required',

        ]);
        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput(Input::all());
            //return ["status" => "1", "error" => Utils::messages($v)];
        }
        $date_begin = Carbon::createFromFormat('Y-m-d h:i:s', $request->date_begin.' 00:00:00')->toDateTimeString();
        $date_end = Carbon::createFromFormat('Y-m-d h:i:s', $request->date_end.' 00:00:00')->toDateTimeString();
        $date_begin = $request->date_begin;
        $date_fin = $request->date_end;
        $continue = true;   
        $posts_count = Posts::where('created_at','>',$date_begin)->where('created_at','<',$request->date_end)->count();
        $sources_count = Sources::count();
        $users_count = Users::count();
        $active_jobs = Posts::where('created_at','>',$date_begin)->where('created_at','<',$request->date_end)->where('status','active')->where('expired_at','>',Carbon::now())->count();
        $active_alerts = DB::table('subscriptions')
                        ->where('created_at', '>', $date_begin)
                        ->where('created_at', '<', $date_end)
                        ->count();
        $active_seekers = DB::table('users')
            ->join('users_groups', 'users.id', '=', 'users_groups.user_id')
            ->where('users_groups.group_id','2')
            ->where('users.deleted','0')
            ->where('users.created_at','>',$date_begin)
            ->where('users.created_at','<',$date_end)
            ->count();
        $active_facebook = DB::table('posts_upgrades')
            ->join('posts_upgrade_lists', 'posts_upgrades.id', '=', 'posts_upgrade_lists.post_upgrades_id')
            ->where('posts_upgrades.upgrade_id','2')
            ->where('posts_upgrade_lists.post_status','in_progress')
            ->where('posts_upgrades.created_at','>',$date_begin)
            ->where('posts_upgrades.created_at','<',$date_end)
            ->count();
        $active_pulse = DB::table('posts_upgrades')
            ->join('posts_upgrade_lists', 'posts_upgrades.id', '=', 'posts_upgrade_lists.post_upgrades_id')
            ->where('posts_upgrades.upgrade_id','1')
            ->where('posts_upgrade_lists.post_status','in_progress')
            ->where('posts_upgrades.created_at','>',$date_begin)
            ->where('posts_upgrades.created_at','<',$date_end)
            ->count();
        $active_linked = DB::table('posts_upgrades')
            ->join('posts_upgrade_lists', 'posts_upgrades.id', '=', 'posts_upgrade_lists.post_upgrades_id')
            ->where('posts_upgrades.upgrade_id','4')
            ->where('posts_upgrade_lists.post_status','in_progress')
            ->where('posts_upgrades.created_at','>',$date_begin)
            ->where('posts_upgrades.created_at','<',$date_end)
            ->count();

        $total_job = DB::table('posts')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->count();
        $total_job_advertiser = DB::table('users_groups')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('group_id',3)->count();
        $total_standard = DB::table('users')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('profile_type','standard')->count();
        $total_maximum = DB::table('users')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('profile_type','maximum')->count();
        $total_job_seekers = DB::table('users_groups')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('group_id',2)->count();

        $posts = DB::table('posts')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->get();
        $total_job_views = 0;
        foreach ($posts as $post) {
            $total_job_views = $total_job_views + $post->views;
        }
        $total_job_applicants = DB::table('applicants')->where('date_created','>',$date_begin)->where('date_created','<',$date_end)->count();
        $total_job_filled =  DB::table('posts')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->count();

        $top5ref = DB::table('users')
          ->whereNotNull('ref_id')
          ->where('ref_id', '!=', ' ')
          ->where('created_at','>',$date_begin)
          ->where('created_at','<',$date_end)
          ->select('ref_id', DB::raw('COUNT(ref_id) AS occurrences'))
          ->groupBy('ref_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $top5applicants = DB::table('applicants')
          ->where('date_created','>',$date_begin)
          ->where('date_created','<',$date_end)
          ->where('job_id','>',0)
          ->select('job_id', DB::raw('COUNT(job_id) AS occurrences'))
          ->groupBy('job_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $top5advertiser = DB::table('posts')
          ->where('created_at','>',$date_begin)
          ->where('created_at','<',$date_end)
          ->select('author_id', DB::raw('COUNT(author_id) AS occurrences'))
          ->groupBy('author_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $top5views = DB::table('posts')
          ->where('created_at','>',$date_begin)
          ->where('created_at','<',$date_end)
          ->orderBy('views', 'DESC')
          ->limit(5)
          ->get();
        $revenue_jobs = DB::table('transactions')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('listing_type','!=','')->count();
        $upgrades = DB::table('posts_upgrades')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('is_purchased',1)->get();
        $revenue_boost = 0;
        foreach ($upgrades as $upgrade) {
            $revenue_boost = $revenue_boost+$upgrade->count;
        }
        $revenue_packages = DB::table('packages_stats')->where('date_purchase','>',$date_begin)->where('date_purchase','<',$date_end)->count();
        $transactions = DB::table('transactions')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->get();
        $transaction_revenue = DB::table('transactions')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('payment','!=','Credits')->where('payment','!=','')->get();
        $total_revenue = 0;
        foreach ($transaction_revenue as $transaction) {
            $total_revenue = $total_revenue+$transaction->totalnogst;
        }
        $welcome_revenue = 0;
        foreach ($transactions as $tran) {
            $welcome_revenue = $welcome_revenue+$tran->welcome_credits;
        }
        $trans1 = DB::table('transactions')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('reason','Refund')->get();
        $refund_revenue = 0;
        foreach ($trans1 as $tran) {
            $refund_revenue = $refund_revenue+$tran->creditsgiven;
        }
        $trans2 = DB::table('transactions')->where('created_at','>',$date_begin)->where('created_at','<',$date_end)->where('reason','Bonus')->get();
        $bonus_revenue = 0;
        foreach ($trans2 as $tran) {
            $bonus_revenue = $bonus_revenue+$tran->creditsgiven;
        }
        return view('admin.reports', compact('users_count','posts_count','sources_count','active_jobs','active_alerts','active_seekers','active_facebook','active_pulse','active_linked','total_job','total_job_advertiser','total_standard','total_maximum','total_job_seekers','total_job_views','total_job_applicants','total_job_filled','top5ref','top5views','top5applicants','top5advertiser','revenue_jobs','revenue_boost','revenue_packages','total_revenue','welcome_revenue','refund_revenue','bonus_revenue','continue','date_begin','date_fin'));
    }

    public function index()
    {
        $ads = DB::table('package_info')->where('payment','Invoice')->orderBy('purchase_date','DESC')->get();
        foreach ($ads as $ad) {
            if ($ad->invoice_terms) {
                $start = new \Carbon\Carbon($ad->purchase_date);
                $date = $start->addDays($ad->invoice_terms);
                if ($date < Carbon::now() && $ad->status == 'Awaiting Payment') {
                    $user = DB::table('package_info')->where('id',$ad->id)->first();
                    $posts = DB::table('posts')->where('author_id',$user->user_id)->get();
                    DB::table('package_info')->where('id',$ad->id)->update(['status' => 'Overdue']);
                    foreach ($posts as $post) {
                       DB::table('posts')->where('id',$post->id)->update(['invoice_status' =>  $post->status,'status' => 'remove']);
                    }
                }
            }else{
                $start = new \Carbon\Carbon($ad->purchase_date);
                $check_term = \App\Users::find($ad->user_id);
                $current_term = \App\Settings::where('column_key','invoice_terms')->first();
                if($check_term && $check_term->invoice_terms){
                    $date = $start->addDays($check_term->invoice_terms);
                }elseif($current_term && $current_term->value_string){
                    $date = $start->addDays($current_term->value_string);
                }else{
                    $date = $start->addDays(7);
                }
                if ($date < Carbon::now() && $ad->status == 'Awaiting Payment') {
                    $user = DB::table('package_info')->where('id',$ad->id)->first();
                    $posts = DB::table('posts')->where('author_id',$user->user_id)->get();
                      DB::table('package_info')->where('id',$ad->id)->update(['status' => 'Overdue']);
                    foreach ($posts as $post) {
                      DB::table('posts')->where('id',$post->id)->update(['invoice_status' =>  $post->status,'status' => 'remove']);
                    }
                }
            }
        }
        $posts_count = Posts::count();
        $sources_count = Sources::count();
        $users_count = Users::count();
        $active_jobs = Posts::where('status','active')->where('expired_at','>',Carbon::now())->count();
        $active_alerts = Subscription::where('active','1')->count();
        $active_seekers = DB::table('users')
            ->join('users_groups', 'users.id', '=', 'users_groups.user_id')
            ->where('users_groups.group_id','2')
            ->where('users.deleted','0')
            ->count();
        $active_standard = DB::table('users')->where('profile_type','standard')->where('deleted','0')->count();
        $active_maximum = DB::table('users')->where('profile_type','maximum')->where('deleted','0')->count();
        $active_auscontact = DB::table('users')->whereNotNull('aus_contact_member_no')->where('deleted','0')->count();
        $active_advertisers = DB::table('users')
            ->join('users_groups', 'users.id', '=', 'users_groups.user_id')
            ->where('users_groups.group_id','3')
            ->where('users.deleted','0')
            ->count();
        $active_facebook = DB::table('posts_upgrade_lists')
            ->where('boost_id','2')
            ->where('post_status','in_progress')
            ->count();
        $active_pulse = DB::table('posts_upgrade_lists')
            ->where('boost_id','1')
            ->where('post_status','in_progress')
            ->count();
        $active_linked = DB::table('posts_upgrade_lists')
            ->where('boost_id','4')
            ->where('post_status','in_progress')
            ->count();
        $pending_facebook = DB::table('posts_upgrade_lists')
            ->where('boost_id','2')
            ->where('post_status','not_actioned')
            ->count();
        $pending_pulse = DB::table('posts_upgrade_lists')
            ->where('boost_id','1')
            ->where('post_status','not_actioned')
            ->count();
        $pending_linked = DB::table('posts_upgrade_lists')
            ->where('boost_id','4')
            ->where('post_status','not_actioned')
            ->count();
        $completed_facebook = DB::table('posts_upgrade_lists')
            ->where('boost_id','2')
            ->where('post_status','completed')
            ->count();
        $completed_pulse = DB::table('posts_upgrade_lists')
            ->where('boost_id','1')
            ->where('post_status','completed')
            ->count();
        $completed_linked = DB::table('posts_upgrade_lists')
            ->where('boost_id','4')
            ->where('post_status','completed')
            ->count();

        $total_job = DB::table('posts')->count();
        $total_job_advertiser = DB::table('users_groups')->where('group_id',3)->count();
        $total_standard = DB::table('users')->where('profile_type','standard')->count();
        $total_maximum = DB::table('users')->where('profile_type','maximum')->count();
        $total_job_seekers = DB::table('users_groups')->where('group_id',2)->count();
        $total_auscontact = DB::table('users')->whereNotNull('aus_contact_member_no')->count();

        $posts = DB::table('posts')->get();
        $total_job_views = 0;
        foreach ($posts as $post) {
            $total_job_views = $total_job_views + $post->views;
        }
        $total_job_applicants = DB::table('applicants')->count();
        $total_job_filled = 0;
        $filled_jobs = DB::table('posts')->get();
        foreach ($filled_jobs as $job) {
            if(DB::table('applicants')->where('job_id',$job->id)->count() > 0){
                $total_job_filled = $total_job_filled+1;
            }
        }

        $top5ref = DB::table('users')
          ->whereNotNull('ref_id')
          ->where('ref_id', '!=', ' ')
          ->select('ref_id', DB::raw('COUNT(ref_id) AS occurrences'))
          ->groupBy('ref_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $top5applicants = DB::table('applicants')
          ->where('job_id','>',0)
          ->select('job_id', DB::raw('COUNT(job_id) AS occurrences'))
          ->groupBy('job_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $top5advertiser = DB::table('posts')
          ->select('author_id', DB::raw('COUNT(author_id) AS occurrences'))
          ->groupBy('author_id')
          ->orderBy('occurrences', 'DESC')
          ->limit(5)
          ->get();
        $top5views = DB::table('posts')
          ->orderBy('views', 'DESC')
          ->limit(5)
          ->get();
        $revenue_jobs = DB::table('transactions')->where('listing_type','!=','')->count();
        $revenue_screening = DB::table('transactions')->where('questions','!=','')->count();
        $upgrades = DB::table('posts_upgrades')->where('is_purchased',1)->get();
        $revenue_boost = 0;
        foreach ($upgrades as $upgrade) {
            $revenue_boost = $revenue_boost+$upgrade->count;
        }
        $revenue_packages = DB::table('package_info')->count();
        $transactions = DB::table('transactions')->get();
        $transaction_revenue = DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->get();
        $total_revenue = 0;
        foreach ($transaction_revenue as $transaction) {
            $total_revenue = $total_revenue+$transaction->totalnogst;
        }
        $welcome_revenue = 0;
        foreach ($transactions as $tran) {
            $welcome_revenue = $welcome_revenue+$tran->welcome_credits;
        }
        $bonus_revenue = 0;
        foreach ($transactions as $tran) {
            $bonus_revenue = $bonus_revenue+$tran->creditsgiven;
        }
        return view('admin.index', compact('users_count','posts_count','sources_count','active_jobs','active_alerts','active_seekers','active_facebook','active_pulse','active_linked','total_job','total_job_advertiser','total_standard','total_maximum','total_job_seekers','total_job_views','total_job_applicants','total_job_filled','top5ref','top5views','top5applicants','top5advertiser','revenue_jobs','revenue_boost','revenue_packages','total_revenue','welcome_revenue','bonus_revenue','active_advertisers','pending_pulse','pending_facebook','pending_linked','active_maximum','active_standard','total_auscontact','active_auscontact','completed_facebook','completed_pulse','completed_linked','revenue_screening'));
    }

    public function handleRedactorUploads()
    {
        $filename = Utils::imageUpload(Input::file('file'), 'images');
        Log::info('Showing user profile for user: '.$filename);
        return response()->json($data = array(
            'filelink' => $filename
        ), 200); 
    }

    public function uploadimgredactor()
    {
        if (Input::hasFile('file')) {
            $file = Input::file('file');
            $timestamp = uniqid();
            $ext = $file->guessClientExtension();
            $name = $timestamp . "_file." . $ext;
            $folder = 'images';
            if (is_null($folder)) {

                // move uploaded file from temp to uploads directory
                if ($file->move(public_path() . '/uploads/', $name)) {
                    return URL::to('/uploads/' . $name);
                }
            } else {


                if (!\File::exists(public_path() . '/uploads/' . $folder)) {
                    \File::makeDirectory(public_path() . '/uploads/' . $folder);
                }

                // move uploaded file from temp to uploads directory
                if ($file->move(public_path() . '/uploads/' . $folder . '/', $name)) {
                    return response()->json(
                        $data = array(
                            'filelink' => URL::to('/uploads/' . $folder . '/' . $name),
                        ),
                        200
                    );
                }
            }
        }

        return false;
    }

    public function redactorImages()
    {

        $arr = [];
        $allFiles = File::allFiles(public_path() . '/uploads/images/');

        foreach ($allFiles as $file) {
            $file = new SplFileInfo($file);
            $arr[] = ["thumb" => URL::to('/') . '/uploads/images/' . $file->getFilename(), "image" => URL::to('/') . '/uploads/images/' . $file->getFilename(), "title" => $file->getFilename()];
        }
        
        return $arr;
    }

}