<?php namespace App\Http\Controllers;

use App\Libraries\Utils;
use App\Users;
use Auth;
use Carbon\Carbon;
use App\Transactions;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\UsersGroups;
use Input;
use Mail;
use Session;
use DB;
use URL;
use Validator;
use Socialite;

class AuthController extends Controller
{

    protected $loginPath = '/login';

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

    }

    public function getLogin()
    {
        if (Input::has('setup')) {
            \Artisan::call('cache:clear');
            \Artisan::call('migrate', ['--force' => 'yes']);
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = \App\Settings::where('column_key','default_login-seodescription')->first();
        $this->data['settings_social'] = Utils::getSettings("social");

        $this->data['google_adwards_tracking_code'] = \App\Settings::where('column_key','default_login_google')->first();
        $this->data['facebook_pixel_tracking_code'] = \App\Settings::where('column_key','default_login_facebook')->first();
        $this->data['page_title'] = \App\Settings::where('column_key','default_login_page_title')->first()->value_txt;
        $this->data['seo_keywords'] = \App\Settings::where('column_key','default_login-seokeywords')->first();

        return view('login', $this->data);
    }

    public function getLoginseeker()
    {
        if (Input::has('setup')) {
            \Artisan::call('cache:clear');
            \Artisan::call('migrate', ['--force' => 'yes']);
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = \App\Settings::where('column_key','seeker_login_seodescription')->first();
        $this->data['settings_social'] = Utils::getSettings("social");

        $this->data['google_adwards_tracking_code'] = \App\Settings::where('column_key','seeker_login_google')->first();
        $this->data['facebook_pixel_tracking_code'] = \App\Settings::where('column_key','seeker_login_facebook')->first();
        $this->data['page_title'] = \App\Settings::where('column_key','seeker_login_page_title')->first()->value_txt;
        $this->data['seo_keywords'] = \App\Settings::where('column_key','seeker_login_seokeywords')->first();

        return view('login_seeker', $this->data);
    }

    public function getLoginseekerverify()
    {
        if (Input::has('setup')) {
            \Artisan::call('cache:clear');
            \Artisan::call('migrate', ['--force' => 'yes']);
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = \App\Settings::where('column_key','seeker_login_verify_seodescription')->first();
        $this->data['settings_social'] = Utils::getSettings("social");

        $this->data['google_adwards_tracking_code'] = \App\Settings::where('column_key','seeker_login_verify_google')->first();
        $this->data['facebook_pixel_tracking_code'] = \App\Settings::where('column_key','seeker_login_verify_facebook')->first();
        $this->data['page_title'] = \App\Settings::where('column_key','seeker_login_verify_page_title')->first()->value_txt;
        $this->data['seo_keywords'] = \App\Settings::where('column_key','seeker_login_verify_seokeywords')->first();

        return view('login_seeker_verify', $this->data);
    }

    public function getLoginposterverify()
    {
        if (Input::has('setup')) {
            \Artisan::call('cache:clear');
            \Artisan::call('migrate', ['--force' => 'yes']);
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = \App\Settings::where('column_key','advertiser_login_seodescription')->first();
        $this->data['settings_social'] = Utils::getSettings("social");

        $this->data['google_adwards_tracking_code'] = \App\Settings::where('column_key','advertiser_login_google')->first();
        $this->data['facebook_pixel_tracking_code'] = \App\Settings::where('column_key','advertiser_login_facebook')->first();
        $this->data['page_title'] = \App\Settings::where('column_key','advertiser_login_page_title')->first()->value_txt;
        $this->data['seo_keywords'] = \App\Settings::where('column_key','advertiser_login_seokeywords')->first();

        return view('login_advertiser_verify', $this->data);
    }

    public function getLoginposter()
    {
        if (Input::has('setup')) {
            \Artisan::call('cache:clear');
            \Artisan::call('migrate', ['--force' => 'yes']);
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");

        $this->data['google_adwards_tracking_code'] = \App\Settings::where('column_key','advertiser_login_verify_google')->first();
        $this->data['facebook_pixel_tracking_code'] = \App\Settings::where('column_key','advertiser_login_verify_facebook')->first();
        $this->data['page_title'] = \App\Settings::where('column_key','advertiser_login_verify_page_title')->first()->value_txt;
        $this->data['seo_keywords'] = \App\Settings::where('column_key','advertiser_login_verify_seokeywords')->first();

        return view('login_advertiser', $this->data);
    }
	
	
	public function getLoginAlert()
    {
        if (Input::has('setup')) {
            \Artisan::call('cache:clear');
            \Artisan::call('migrate', ['--force' => 'yes']);
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['message'] = '';
        $this->data['settings_social'] = Utils::getSettings("social");
        return view('login', $this->data);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback(Request $request,$provider)
    {   if($provider == 'google' || $provider == 'facebook' || $provider == 'linkedin'){
            if (!$request->has('code') || $request->has('denied')) {
                Session::flash('error_msg', 'Allow access to your profile to continue login!');
                return redirect('/login');
            }
        }elseif($request->has('denied')) {
            Session::flash('error_msg', 'Allow access to your profile to continue login!');
            return redirect('/login');
        }
        $user_profile = Socialite::driver($provider)->user();
        $emailverify = Users::where('email',$user_profile->email)->first();

        if (!empty($emailverify)) {
            $activated = Users::where('email',$user_profile->email)->first();

            $development_mode = \App\Settings::where('column_key','status_development')->first();
            $development_mode_status = \App\Settings::where('column_key','development_message')->first();
            if (!Utils::isAdmin($activated->id) && $development_mode && $development_mode->value_txt == 1) {
                Session::flash('error_msg', $development_mode_status->value_txt);
                return redirect('/login');
            }

            if (!empty($activated) && $activated->deleted) {
                Session::flash('error_msg', 'Sorry, that account has been deleted');
                return redirect('/login');
            }
            if(!empty($activated) && $activated->activated==1) {
                $check = DB::table('referralcode')->where('id',$activated->ref_id)->first();
                $date = Carbon::now()->toDateString();
                $user = Users::find($activated->id);

                if ($user) {
                    $user->lastlogin = Carbon::now();
                    $user->save();
                }

                if(count($check) > 0 && $check->bonus_end >= $date && $check->credit_amount > 0  && empty($user->firstlogin)){
                    Users::where('id',$user->id)->update(['credits' => $check->credit_amount+$user->credits,'welcome_remaining' => $check->credit_amount] );
                    $trans =  new Transactions();
                    $trans->welcome_credits = $check->credit_amount;
                    $trans->creditsin = $user->credits;
                    $trans->creditsremaing = $user->credits+$check->credit_amount;
                    $trans->user_id = $user->id;
                    $trans->save();
                }

                Auth::login($user, true);

                if (Utils::isCustomer($user->id)) {

                    return redirect()->intended('/customer');

                }elseif (Utils::isPoster($user->id)) {

                    return redirect()->intended('/poster');

                }elseif (Utils::isCadmin($user->id)) {

                    return redirect()->intended('/poster');

                }else {

                    return redirect()->intended('/admin');

                }
            }

            Session::flash('error_msg', trans('messages.invalid_login_try_again'));
            return redirect('/login');
        }else{
            $user = $this->RegisterSeeker($user_profile, $provider);
            Auth::login($user, true);
            if (Utils::isCustomer($user->id)) {

                return redirect()->intended('/customer');

            }elseif (Utils::isPoster($user->id)) {

                return redirect()->intended('/poster');

            }elseif (Utils::isCadmin($user->id)) {

                return redirect()->intended('/poster');

            }else {

                return redirect()->intended('/admin');

            }
        }

    }

    public function RegisterSeeker($user_profile,$provider)
    {   
        $customer_group = DB::table('groups')->where('name', 'customer')->first();

        $user_save = new Users();
        $name = $user_profile->getName();
        $user_save->name = $name;
        $fullname = $this->split_name($user_profile->getName());
        $user_save->slug = Str::slug($name);
        $user_save->first_name = $fullname[0];
        $user_save->last_name = $fullname[1];
        $user_save->email = $user_profile->email;
        $user_save->activated = 1;
        if(!empty($user_profile->getAvatar())){
            $user_save->avatar = $user_profile->getAvatar();
        }else{
            $user_save->avatar = "/if_profile.png";
        }
        $user_save->save();
        $user = Users::find($user_save->id);
        $user_group = new UsersGroups();
        $user_group->user_id = $user->id;
        $user_group->group_id = $customer_group->id;
        $user_group->save();
        return $user;
    }
	
    public function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }
	

    public function postLogin(Request $request)
    {

        $v = Validator::make(
            ['email' => $request->input('email'), 'password' => $request->input('password')],
            ['email' => 'required|email', 'password' => 'required']
        );
        $emailverify = Users::where('email',$request->input('email'))->first();
        if (empty($emailverify)) {
           Session::flash('error_msg', 'Sorry, the email address is not recognised');
            return redirect()->back()->withInput($request->only('email', 'password'));
        }
        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput($request->only('email', 'remember'));
        }

        $activated = Users::where('email',$request->input('email'))->first();
        if ($activated->deleted) {
            Session::flash('error_msg', 'Sorry, that account has been deleted');
            return redirect()->back()->withInput($request->only('email', 'password'));
        }
        $development_mode = \App\Settings::where('column_key','status_development')->first();
        $development_mode_status = \App\Settings::where('column_key','development_message')->first();
        if (!Utils::isAdmin($activated->id) && $development_mode && $development_mode->value_txt == 1) {
            Session::flash('error_msg', $development_mode_status->value_txt);
            return redirect()->back()->withInput($request->only('email', 'password'));
        }

        $credentials = $request->only('email', 'password');

        if(!empty($activated) && $activated->activated==1) {

            if ($this->auth->attempt($credentials, $request->has('remember'))) {
                $check = DB::table('referralcode')->where('id',Auth::user()->ref_id)->first();
                $date = Carbon::now()->toDateString();
                $user = Users::find(Auth::user()->id);
                if (Auth::user()->id) {
                  $user = Users::find(Auth::user()->id);
                  $user->lastlogin = Carbon::now();
                  $user->save();
                }
                if(count($check) > 0 && $check->bonus_end >= $date && $check->credit_amount > 0  && empty(Auth::user()->firstlogin)){
                    Users::where('id',Auth::user()->id)->update(['credits' => $check->credit_amount+$user->credits,'welcome_remaining' => $check->credit_amount] );
                    $trans =  new Transactions();
                    $trans->welcome_credits = $check->credit_amount;
                    $trans->creditsin = $user->credits;
                    $trans->creditsremaing = $user->credits+$check->credit_amount;
                    $trans->user_id = Auth::user()->id;
                    $trans->save();
                }
                if (Utils::isCustomer(Auth::user()->id)) {
                    return redirect()->intended('/customer');
                } elseif (Utils::isPoster(Auth::user()->id)) {
                    return redirect()->intended('/poster');
                }
				elseif (Utils::isCadmin(Auth::user()->id)) {
                    return redirect()->intended('/poster');
                }
				else {
                    return redirect()->intended('/admin');
                }


            }
        }

        Session::flash('error_msg', trans('messages.invalid_login_try_again'));

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function getForgotPassword()
    {
        return view("admin.forgot-password");
    }

    public function getRegister()
    {
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");

        return view('register', $this->data);
    }
	 public function getRegisterUser()
    {
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");
        $development_mode = \App\Settings::where('column_key','status_development')->first();
        $development_mode_status = \App\Settings::where('column_key','development_message')->first();
        if ($development_mode && $development_mode->value_txt == 1) {
            Session::flash('error_msg', $development_mode_status->value_txt);
            return redirect()->to('/login');
        }

        return view('register-user', $this->data);
    }
	
	

	 public function getRegisterPoster()
    {
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");
        $development_mode = \App\Settings::where('column_key','status_development')->first();
        $development_mode_status = \App\Settings::where('column_key','development_message')->first();
        if ($development_mode && $development_mode->value_txt == 1) {
            Session::flash('error_msg', $development_mode_status->value_txt);
            return redirect()->to('/login');
        }

        return view('register-poster', $this->data);
    }
	
	
	

    public function postForgotPassword()
    {
        $email = Input::get("email");

        $user = Users::where('email', $email)->first();

        if (sizeof($user) <= 0) {
            Session::flash("error_msg", trans('messages.account_not_found_with_email'));
            return redirect()->back();
        } else {
            $reset_code = Utils::generateResetCode();
            $user->reset_password_code = $reset_code;
            $user->reset_requested_on = \Carbon\Carbon::now();
            $user->save();

            $email_arr = ['name' => $user->name,
                'reset_url' => URL::to('/') . "/reset_password/" . $user->email . "/" . $user->reset_password_code,
                'email' => $user->email];

            Mail::send('emails.reset_password', $email_arr, function ($message) use ($user) {
                $message->to($user->email, $user->name)->subject(trans('messages.rss_reset_password'));
            });

            Session::flash('success_msg', trans('messages.click_link_to_reset_password'));
            return redirect()->to('/login');
        }

    }

    public function getReset($email, $code)
    {

        if (strlen($email) <= 0 || strlen($code) <= 0) {
            Session::flash("error_msg", trans('messages.invalid_request_please_reset_password'));
            return redirect()->to('/forgot-password');
        }

        //Check code and email
        $user = Users::where('email', $email)->where('reset_password_code', $code)->first();

        if (sizeof($user) <= 0) {
            Session::flash("error_msg", trans('messages.invalid_request_please_reset_password'));
            return redirect()->to('/forgot-password');
        } else {
            //check for 24 hrs for token
            $reset_requested_on = \Carbon\Carbon::createFromFormat('Y-m-d G:i:s', $user->reset_requested_on);
            $present_day = \Carbon\Carbon::now();

            if ($reset_requested_on->addDay() > $present_day) {
                //Show new password view
                return view('admin.reset-password', ['email' => $email, 'code' => $code]);
            } else {
                Session::flash("error_msg", trans('messages.your_password_token_expired'));
                return redirect()->to('/forgot-password');
            }
        }
    }

    public function postReset()
    {

        $password = Input::get('password', '');
        $password_confirmation = Input::get('password_confirmation', '');

        if ($password == $password_confirmation) {

            $validate_reset = Users::where('email', Input::get('email', ''))->where('reset_password_code', Input::get('code', ''))->first();

            if (sizeof($validate_reset) > 0) {
                $user = Users::where('email', Input::get('email'))->first();
                $user->password = \Hash::make($password);
                $user->save();

                Session::flash('success_msg', trans('messages.your_password_changed_success'));
                return redirect()->to('/login');
            } else {
                Session::flash('error_msg', trans('messages.an_invalid_password_entered'));
                return redirect()->back();
            }
        } else {
            Session::flash('error_msg', trans('messages.both_new_confirm_must_be_same'));
            return redirect()->back();
        }
    }

    public function getLogout()
    {
        $this->auth->logout();

        return redirect('/login');
    }


}
