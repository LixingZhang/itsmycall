<?php

namespace App\Http\Controllers\Poster;

use App\Categories;
use App\SubCategories;
use App\PostsPackages;
use App\Packages;
use App\Upgrades;
use App\UpgradeInfo;
use App\PostUpgrades;
use App\PostUpgradesLists;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Posts;
use App\CompanyLogos;
use App\PackagesStats;
use App\Users;
use App\Applicants;
use App\Transactions;
use App\Views;
use App\Coupons;
use App\ApplicantsStatus;
use App\PastEmail;
use App\ReferralCode;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Input;
use Session;
use Hash;
use DB;
use URL;
use Config;
use XeroPrivate;
use DateTime;
use Twitter;

class PostsController extends Controller
{

    public function addquestion(Request $request)
    {  
        DB::table('applicant_questions')->insert(
            ['question' => $request->question, 'user_id' => Auth::user()->id]
        );
        $id = DB::table('applicant_questions')->where('user_id',Auth::user()->id)->orderBy('id','DESC')->first();
        return $id->id;
    }
    public function changejobstatus(Request $request,$id)
    {  
        $post = Posts::where('id', $id)->get()->first();
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'old_post' => 1 , 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => $post->updated_at, 'questions' => $post->questions, 'status' => $post->status ]
                ]);
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => Carbon::now(), 'questions' => $post->questions, 'status' => $request->status]
                ]);
        $post = Posts::find($id);
        $post->status = $request->status;
        $post->save();
        \Session::flash('success_msg', 'Job Status Updated.');

        return redirect()->to('/poster');
    }
    public function changejobstatusinactive(Request $request,$id)
    {  
        $post = Posts::where('id', $id)->get()->first();
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'old_post' => 1 , 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => $post->updated_at, 'questions' => $post->questions, 'status' => $post->status ]
                ]);
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => Carbon::now(), 'questions' => $post->questions, 'status' => $request->status]
                ]);
        $post = Posts::find($id);
        $post->status = $request->status;
        $post->save();
        \Session::flash('success_msg', 'Job Status Updated.');

        return redirect()->to('/poster/inactive');
    }
    public function changejobstatusexpired(Request $request,$id)
    {  
        $post = Posts::where('id', $id)->get()->first();
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'old_post' => 1 , 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => $post->updated_at, 'questions' => $post->questions, 'status' => $post->status ]
                ]);
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => Carbon::now(), 'questions' => $post->questions, 'status' => $request->status]
                ]);
        $post = Posts::find($id);
        $post->status = $request->status;
        $post->save();
        \Session::flash('success_msg', 'Job Status Updated.');

        return redirect()->to('/poster/expired');
    }
    public function removequestion(Request $request)
    {   DB::table('applicant_questions')->where('id',$request->id)->delete();
        return 'removed question';
    }
    public function edit_question(Request $request)
    {   DB::table('applicant_questions')->where('id',$request->id)->update(['question' => $request->question]);;
        return 'edited question';
    }
    public function jobPost()
    {   
            $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
            if (count($invoice_cleared) > 0) {
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> New jobs cannot be advertised until all outstanding invoices are paid.');
                return redirect()->back();
            }   
            $key = Config::get('app.key');
            $pass = bcrypt(Auth::user()->password);
            $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);
            $this->data['user'] = Auth::user();
            $disc = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
            $coupon = Coupons::active()->where('id', Auth::user()->coupon_id)->get()->first();
            $img = DB::table('upgradeimages')
                        ->get();
            if (isset($coupon)) {
                $coupon_code = $coupon->coupon_code;
            } else {
                $coupon_code='';
            }
            $this->data['settings'] = Utils::getSettings("general");
            //var_dump($disc->description);
            $date = Carbon::now()->toDateString();
            if (!$disc) {
                $disc1 = 0;
                $discname = "";
                $discex = "no";
            } 
            elseif($disc->date_end <= $date){
                $disc1 = 0;
                $discname = "";
                $discex = "no";
            }
            else {
                $disc1 = $disc->discount_rate;
                $discex = $disc->exclusive;
                //var_dump($discex);
                //$discname = $disc;
            }
            if (Auth::user()->ref_id) {
                $date = Carbon::now()->toDateString();
                $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                    $posts_packages = DB::table('referralpackages')->where('referral_id',Auth::user()->ref_id)->get();
                    $this->data['posts_packages'] = $posts_packages;
                    $upgrades = DB::table('referralupgrades')->where('referral_id',Auth::user()->ref_id)->get();
                    $this->data['upgrades'] = $upgrades;
                    $price_questions = DB::table('referral_questions')->where('referral_id',Auth::user()->ref_id)->first();
                    $this->data['price_questions'] = $price_questions;
                }
                else{
                    $posts_packages = PostsPackages::orderBy('price', 'desc')->get();
                    $this->data['posts_packages'] = $posts_packages;
                    $upgrades = Upgrades::orderBy('id', 'asc')->get();
                    $this->data['upgrades'] = $upgrades;
                    $price_questions = DB::table('questionspricing')->first();
                    $this->data['price_questions'] = $price_questions;
                }

            }
            else{
                $posts_packages = PostsPackages::orderBy('price', 'desc')->get();
                $this->data['posts_packages'] = $posts_packages;
                $upgrades = Upgrades::orderBy('id', 'asc')->get();
                $this->data['upgrades'] = $upgrades;
                $price_questions = DB::table('questionspricing')->first();
                $this->data['price_questions'] = $price_questions;

            }       
                $check = DB::table('referralcode')->where('id',Auth::user()->ref_id)->first();
                $date = Carbon::now()->toDateString();
                $user = Users::find(Auth::user()->id);
                $remainingwelcome = Auth::user()->welcome_remaining;
                $welcomecred = 0;
                if (count($check) > 0 && $check->bonus_end < $date && Auth::user()->welcome_expired == NULL && $check->credit_amount > 0) {
                        if (Auth::user()->credits >= $remainingwelcome) {
                           Users::where('id',Auth::user()->id)->update(['credits' => Auth::user()->credits-$remainingwelcome] );
                        }
                        Users::where('id',Auth::user()->id)->update(['welcome_expired' => 1,'welcome_remaining' => 0] );
                        Session::flash('info_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Your Welcome Bonus credits have expired. <a href="/poster/buy_credits">Buy more Credits</a>');
                        return redirect()->to('/poster');
                }
                $package_info = DB::table('package_info')->where('user_id',Auth::user()->id)->where('expiry_date','<',Carbon::now())->whereNull('expired')->where('remaining','>',0)->orderBy('purchase_date','DESC')->get();
                if (count($package_info) > 0) {
                    foreach ($package_info as $package) {
                            if (Auth::user()->credits >= $package->remaining) {
                                Users::where('id',Auth::user()->id)->update(['credits' => Auth::user()->credits-$package->remaining] );
                            }
                            DB::table('package_info')
                                ->where('id', $package->id)
                                ->update(['remaining' => 0, 'expired' => $package->remaining]);
                    }
                    Session::flash('info_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Your Value Pack credits have expired. <a href="/poster/buy_credits">Buy more Credits</a>');
                    return redirect()->to('/poster');
                }
                $admin_questions = DB::table('applicant_questions')->where('user_id','1')->get();
                $user_questions = DB::table('applicant_questions')->where('user_id',Auth::user()->id)->get();
        return view(
            'job_post',
            [
                'categories' => Categories::all(),
                'companylogos' => CompanyLogos::where('user_id', Auth::id())->get(),
                'packages' => Packages::orderBy('price', 'asc')->where('status',0)->get(),
                'rf09871280971' => $disc1, // discount
                'rf09871280971EXC' => $discex, // discount
                //'rf09871280971#NAME' => $discname, // discount
                'admins' => $admins,
                'coupon_code' => $coupon_code,
                'images' => $img,
                'admin_questions' => $admin_questions,
                'user_questions' => $user_questions,
            ],
            $this->data
        );
    }

   public function transactiondetails()
    {

        $ads = [];
        $transactions = Transactions::where('user_id',Auth::id())->orderBy('created_at','desc')->get();
        $jobs = DB::table('posts')
                      ->select('posts.*')
                      ->join('users', 'posts.author_id', '=', 'users.id')
                      ->where('users.deleted', false)
                      ->where('posts.status', Posts::STATUS_POSITION_ACTIVE)
                      ->where('posts.expired_at', '>=', Carbon::now());
        $packages = Packages::all();
        $posts = Posts::all();
            $this->data['transactions'] = $transactions;
            $this->data['posts'] = $posts;
            $this->data['packages'] = $packages;
            $this->data['jobs'] = $jobs;
         return view('transactiondetails', $this->data);
    }

    public function applicantactive($id)
    { 
       $applicant = Applicants::find($id);
    if ($applicant->still_active == 0 || $applicant->still_active == NULL) {  
       DB::table('applicants')->where('id',$id)->update(['still_active' => 1]);
        }
    if ($applicant->still_active == 1) {
       DB::table('applicants')->where('id',$id)->update(['still_active' => NULL]);
       }
       return redirect()->back();
    }

    public function valuedetails($id)
    {   
        $ads = DB::table('package_info')->where('user_id',$id)->orderBy('purchase_date','DESC')->get();
            $this->data['ads'] = $ads;
         return view('valuepackdetails', $this->data);
    }


    public function boostdetails($id)
    {

        $ads = [];
        $check = PostUpgrades::where('post_id',$id)->get();
        foreach ($check as $ad) {
            $job = Posts::where('id', $ad->post_id);
            if (!$job) {
                continue;
            }
            $ad->job  = $job;
            $ads[]    = $ad;
            $this->data['ads'] = $ads;
            $lists = PostUpgradesLists::where('post_upgrades_id', $ad->id)->get();
            $count = $ad->count - $lists->count();
            $boost = \App\PostUpgrades::find($ad->id);
            if ($count > 0 && $boost->actioned == '') {
                for ($i = 0; $i < $count; $i++) {
                    $list = new PostUpgradesLists();
                    $list->post_upgrades_id = $ad->id;
                    $list->post_datetime = Carbon::now();
                    $list->post_views = 0;
                    $list->post_clicks = 0;
                    $list->post_cost = 0;
                    if($boost->upgrade_price){
                        $list->boost_cost = $boost->upgrade_price/$boost->count;
                    }else{
                        $boostprice = \App\Upgrades::find($boost->upgrade_id);
                        $list->boost_cost = $boostprice->price/$boost->count;

                    }
                    $list->post_id = $ad->post_id;
                    $list->boost_id = $boost->upgrade_id;
                    $list->post_status = PostUpgradesLists::STATUS_NOT_ACTIONED;
                    $list->save();
                }
            $boost->actioned = 1;
            $boost->save();
            }
        }
            $jobname = Posts::find($id);

            $boosts = PostUpgradesLists::where('post_id',$id)->get();
            $this->data['jobname'] = $jobname;
            $this->data['boosts'] = $boosts;
         return view('boostdetails', $this->data);
    }

    public function editPoster()
    {
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");
        $user = Auth::user();

        $this->data['user'] = Auth::user();

        return view('edit-poster', $this->data);
    }

    public function editStore(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'first-name' => 'required',
            'last-name' => 'required',
            'state' => 'required',
            'advertiser_type' => 'required',
            'phone' => 'required|regex:/^[\+\d\s]*$/',
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");
        $user = Auth::user();
        DB::table('activitylog')->insert([
            ['description' => 'Edited Profile', 'user_id' => $user->id, 'created_at' => Carbon::now()]
        ]);
        if(Input::hasFile('avatar')){
                $file = Input::file('avatar');
                $folder = 'images';
                    if ($file->isValid()) {
                    $name = $file->getClientOriginalName();
                    $unique = uniqid();
                    $file->move(public_path() . '/uploads/' . $folder . '/', $unique . $name);
                    $imgpath = URL::to('/uploads/' . $folder . '/' . $unique . $name);
                    $user->avatar = $imgpath;
                    }
            }
        $user->name = Input::get('first-name') . " " . Input::get('last-name');
        $user->first_name = Input::get('first-name');
        $user->last_name = Input::get('last-name');
        $user->company = Input::get('company');
        $user->state = Input::get('state');
        $user->phone = Input::get('phone');
        $user->billing_business = Input::get('billing_business');
        $user->billing_number = Input::get('billing_number');
        $user->billing_abn = Input::get('billing_abn');
        //$user->billing_address = Input::get('billing_address');
        $user->advertiser_type = Input::get('advertiser_type');
        \Session::flash('success_msg', 'Profile Updated.');
        $user->save();

        return redirect()->to('/poster');
    }

    public function editPost(Request $request)
    {   
        $this->data['settings_custom_css'] = Utils::getSettings("custom_css");
        $this->data['settings_custom_js'] = Utils::getSettings("custom_js");
        $this->data['settings_general'] = Utils::getSettings("general");
        $this->data['settings_seo'] = Utils::getSettings("seo");
        $this->data['settings_social'] = Utils::getSettings("social");
        $v = \Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'company' => 'required',
            // 'country'      => 'required',
            'state' => 'required',
            // 'category_id'  => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'employment_type' => 'required',
            'employment_term' => 'required',
            'parentsoption' => 'required',
            'work_from_home' => 'required',

        ], [
            'work_from_home.required' => 'Please select a Job venue',
            'parentsoption.required' => 'Please select the shift times closest to your requirements'
        ]);
        if (Input::get('application_method') == 1 &&  Input::get('email_or_link') == ''){
            Session::flash('error_msg', 'Please enter the URL in which your applicants will be redirected to');
            return redirect()->back()->withInput(Input::all());
        }
        if (Input::get('application_method') == 1 &&  Input::get('email_or_link') == ''){
            Session::flash('error_msg', 'Please enter the URL in which your applicants will be redirected to');
            return redirect()->back()->withInput(Input::all());
        }
        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput(Input::all());
            //return ["status" => "1", "error" => Utils::messages($v)];
        }

        $id = Input::get('id');
        $post = Posts::where('id', $id)->get()->first();
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => $post->title , 'description' => $post->description , 'short_description' => $post->short_description , 'company' => $post->company , 'category' => $post->category  , 'subcategory' => $post->subcategory  , 'joblocation' => $post->joblocation  ,'state' => $post->state  , 'salary' => $post->salary  , 'hrsalary' => $post->hrsalary  , 'showsalary' => $post->showsalary , 'salarytype' => $post->salarytype , 'incentivestructure' => $post->incentivestructure, 'paycycle' => $post->paycycle, 'employment_type' => $post->employment_type, 'employment_term' => $post->employment_term, 'person_email' => $post->person_email, 'email_or_link' => $post->email_or_link, 'selling1' => $post->selling1, 'selling2' => $post->selling2, 'selling3' => $post->selling3, 'work_from_home' => $post->work_from_home, 'parentsoption' => $post->parentsoption, 'old_post' => 1 , 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages, 'updated_at' => $post->updated_at, 'questions' => $post->questions, 'status' => $post->status, 'screening_activation' => $post->selectquestions ]
                ]);
            
        if (!Input::get('application_method')) {
            $question = NULL;
            if ($request->question != null) {
               $question = json_encode($request->question);
            }
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => Input::get('title') , 'description' => Input::get('description') , 'short_description' => Input::get('shortdescription') , 'company' => Input::get('company') , 'category' => Input::get('category')  , 'subcategory' => Input::get('sub_category')  , 'joblocation' => Input::get('joblocation') , 'state' => Input::get('state') , 'salary' => str_replace( ',', '', Input::get('salary'))  , 'hrsalary' => Input::get('hrsalary')   , 'showsalary' => Input::get('showsalary') , 'salarytype' => Input::get('salarytype') , 'incentivestructure' => Input::get('incentivestructure'), 'paycycle' => Str::slug(Input::get('paycycle')), 'employment_type' => Input::get('employment_type'), 'employment_term' => Input::get('employment_term'), 'person_email' => Input::get('person_email'), 'email_or_link' => '', 'selling1' => Input::get('selling1'), 'selling2' => Input::get('selling2'), 'selling3' => Input::get('selling3'), 'work_from_home' => Input::get('work_from_home'), 'parentsoption' => Input::get('parentsoption'), 'expired_at' => $post->expired_at  , 'posts_packages' => $post->posts_packages , 'updated_at' => Carbon::now() , 'questions' => $question, 'status' => $post->status, 'screening_activation' => $post->selectquestions]
                ]);
        } else {
            DB::table('changelog')->insert([
                ['author_id' => Auth::user()->id ,'post_id' => $id ,'title' => Input::get('title') , 'description' => Input::get('description') , 'short_description' => Input::get('shortdescription') , 'company' => Input::get('company') , 'category' => Input::get('category')  , 'subcategory' => Input::get('sub_category')  , 'joblocation' => Input::get('joblocation')  , 'state' => Input::get('state') , 'salary' => str_replace( ',', '', Input::get('salary'))  , 'hrsalary' => Input::get('hrsalary')   , 'showsalary' => Input::get('showsalary') , 'salarytype' => Input::get('salarytype') , 'incentivestructure' => Input::get('incentivestructure'), 'paycycle' => Str::slug(Input::get('paycycle')), 'employment_type' => Input::get('employment_type'), 'employment_term' => Input::get('employment_term'), 'person_email' => '', 'email_or_link' => Input::get('email_or_link'), 'selling1' => Input::get('selling1'), 'selling2' => Input::get('selling2'), 'selling3' => Input::get('selling3'), 'work_from_home' => Input::get('work_from_home'), 'parentsoption' => Input::get('parentsoption'), 'expired_at' => $post->expired_at, 'posts_packages' => $post->posts_packages , 'updated_at' => Carbon::now() , 'status' => $post->status]
                ]);
        }
        $post_item = Posts::where('id', $id)->get()->first();
        $post_item->title = Input::get('title');
        $post_item->description = Input::get('description');
        $post_item->short_description = Input::get('shortdescription');
        $post_item->company = Input::get('company');
        //$post_item->slug = Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp .  "-" . Str::slug(Input::get('category'));

        $post_item->category_id = Input::get('category');
        $post_item->category = Input::get('category');
        $post_item->subcategory = Input::get('sub_category');

        //salary_fields 372
        $post_item->showsalary = Input::get('showsalary');
        $post_item->salarytype = Str::slug(Input::get('salarytype'));
        $post_item->incentivestructure = Input::get('incentivestructure');
        $post_item->paycycle = Str::slug(Input::get('paycycle'));
        if($post_item->salarytype == 'annual'){
            $post_item->salary = str_replace( ',', '', Input::get('salary'));
            $post_item->annual_ask_select = Input::get('annual_ask_select');
            $post_item->hourly_ask_select = 'no';
            if(Input::get('annual_ask_select') == 'yes'){
                $post_item->hrsalary = Input::get('annual_ask_salary_enter');
            }else{
                $post_item->hrsalary = 0; 
            }
        }else{
            $post_item->hrsalary = Input::get('hrsalary');
            $post_item->hourly_ask_select = Input::get('hourly_ask_select');
            $post_item->annual_ask_select = 'no';
            if(Input::get('hourly_ask_select') == 'yes'){
                $post_item->salary = Input::get('hourly_ask_salary_enter');
            }else{
                $post_item->salary = 0; 
            }
        }

        //location fields
        $post_item->joblocation = Input::get('joblocation');
        $post_item->postcode = Input::get('postcode');
        $post_item->lat = Input::get('lat');
        $post_item->lng = Input::get('lng');
        $post_item->state = Input::get('state');
        $post_item->country = Str::slug(Input::get('country'));
        $question = NULL;
        if ($request->question != null) {
               $question = json_encode($request->question);
            }
        $post_item->questions = $question;
        // $post_item->experience = Str::slug(Input::get('experience'));
        $post_item->employment_type = Input::get('employment_type');
        $post_item->employment_term = Input::get('employment_term');
        $post_item->person_email = Input::get('person_email');
        $post_item->email_or_link = Input::get('email_or_link');
        $post_item->selling1 = Input::get('selling1');
        $post_item->selling2 = Input::get('selling2');
        $post_item->selling3 = Input::get('selling3');
        $post_item->work_from_home = Input::get('work_from_home');
        $post_item->parentsoption = Input::get('parentsoption');
        $post_item->featured_image = Input::get('featured_image_use');
        if(!empty(Input::get('featured_image'))) {
            $company_logos = new CompanyLogos();
            $company_logos->file = Input::get('featured_image');
            $company_logos->user_id = \Auth::id();
            $company_logos->save();
        }
        if(!empty(Input::get('featured_image_delete'))) {
            $delete_logos = explode(", ",Input::get('featured_image_delete')); 
            CompanyLogos::destroy($delete_logos);
        }
        
        if (!Input::get('application_method')) {
            $post_item->email_or_link = '';
        } else {
            $post_item->person_email = '';
        }
        $post_item->video_link = Input::get('video_link');
        /*if (Input::has('featured_image_use')) {
            $post_item->featured_image = Utils::imageUpload(Input::file('featured_image'), 'images');
        }*/
        $post_item->save();
        if(!empty(Input::get('multipleimg'))) {
           $spaces = explode(",",Input::get('multipleimg'));
            $files = array_filter($spaces,function($value) { return $value !== ' '; });
            foreach ($files as $file) {
                DB::table('postsimages')->insert([
                ['post_id' => $id , 'jobimages' => $file ]
                ]);
            }
        }


        Session::flash('success_msg', 'Job Updated');
        

        return redirect()->to('/poster');
    }

    public function updateApplicant()
    {
        //echo "cool";
        $id = Input::get('appid');
        $status = Input::get('status');
        $app = Applicants::where('id', $id)->get()->first();
        $app->status = $status;
        $app->save();

        //echo "cool";
    }

    public function buyCredits()
    {   
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
            if (count($invoice_cleared) > 0) {
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Value Pack cannot be purchased until all outstanding invoices are paid.');
                return redirect()->back();
            }   
        $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);
        $this->data['user'] = Auth::user();
        $disc = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
        $coupon = Coupons::active()->where('id', Auth::user()->coupon_id)->get()->first();
        $img = DB::table('upgradeimages')
                    ->get();
        if (isset($coupon)) {
            $coupon_code = $coupon->coupon_code;
        } else {
            $coupon_code='';
        }
        $this->data['settings'] = Utils::getSettings("general");
        //var_dump($disc->description);
        $date = Carbon::now()->toDateString();
        if (!$disc) {
            $disc1 = 0;
            $discname = "";
            $discex = "no";
        } 
        elseif($disc->date_end <= $date){
            $disc1 = 0;
            $discname = "";
            $discex = "no";
        }
        else {
            $disc1 = $disc->discount_rate;
            $discex = $disc->exclusive;
            //var_dump($discex);
            //$discname = $disc;
        }

        return view(
            'buy_credits',
            [
                'categories' => Categories::all(),
                'companylogos' => CompanyLogos::where('user_id', Auth::id())->get(),
                'posts_packages' => PostsPackages::orderBy('price', 'desc')->get(),
                'packages' => Packages::orderBy('price', 'asc')->where('status',0)->get(),
                'upgrades' => Upgrades::orderBy('price', 'asc')->get(),
                'rf09871280971' => $disc1, // discount
                'rf09871280971EXC' => $discex, // discount
                //'rf09871280971#NAME' => $discname, // discount
                'admins' => $admins,
                'coupon_code' => $coupon_code,
                'images' => $img,
            ],
            $this->data
        );
    }

    public function listJobs()
    {
        $auth_user = Auth::user();
        $check = DB::table('referralcode')->where('id',Auth::user()->ref_id)->first();
        $date = Carbon::now()->toDateString();
        $user = Users::find(Auth::user()->id);
        if(Auth::user()->ref_id && empty(Auth::user()->firstlogin)){
            $user = Users::find(Auth::user()->id);
            $user->firstlogin = 1;
            $user->save();
        }
        $jobs = Posts::active()
                     ->where("author_id", $auth_user->id)
                     ->orderBy('id', 'desc')
                     ->paginate(10);
        $chartdata = '';
        foreach ($jobs as $job) {
            //$apps = Applicants::where('job_id', $job->id)->get();
            $apps = DB::table('applicants')->select(
                DB::raw('DATE(date_created) as date'),
                DB::raw('count(*) as applicants')
            )->where('job_id', $job->id)->groupBy('date')->get();
            $views = DB::table('views')->select(
                DB::raw('DATE(date_viewed) as date'),
                DB::raw('count(*) as views')
            )->where('job_id', $job->id)->groupBy('date')->get();
            //$views = DB::table('views')->select(DB::raw('DATE(date_viewed) as date), DB::raw('count(*) as views'))->groupBy('date')->get();
            //$views = Views::where('job_id', $job->id)->get();
            $count = 0;
            $data = false;
            //var_dump($views);
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $plans = PostsPackages::all();
        $upgrades = PostUpgrades::all();
        $upgradetypes = Upgrades::all();
        $upgradelist = PostUpgradesLists::all();
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = '';
        $this->data['jobs'] = $jobs;
        $this->data['option'] = '';
        $this->data['plans'] = $plans;
        $this->data['search'] = '';
        $this->data['upgrades'] = $upgrades;
        $this->data['upgradetypes'] = $upgradetypes;
        $this->data['upgradelist'] = $upgradelist;

        return view('account-poster', $this->data);
    }

    public function sortactive($id)
    {

        $auth_user = Auth::user();
        $check = DB::table('referralcode')->where('id',Auth::user()->ref_id)->first();
        $date = Carbon::now()->toDateString();
        $user = Users::find(Auth::user()->id);
        if(Auth::user()->ref_id && empty(Auth::user()->firstlogin)){
            $user = Users::find(Auth::user()->id);
            $user->firstlogin = 1;
            $user->save();
        }
        if($id == 'latest'){
            $jobs = Posts::active()
                     ->where("author_id", $auth_user->id)
                     ->orderBy('id', 'desc')
                     ->paginate(10);
        }
        if($id == 'oldest'){
            $jobs = Posts::active()
                     ->where("author_id", $auth_user->id)
                     ->orderBy('id', 'asc')
                     ->paginate(10);
        }
        if($id == 'views'){
            $jobs = Posts::active()
                     ->where("author_id", $auth_user->id)
                     ->orderBy('views', 'desc')
                     ->paginate(10);
        }
        if($id == 'urlclicks'){
            $jobs = Posts::active()
                     ->where("author_id", $auth_user->id)
                     ->where("email_or_link",'!=', '')
                     ->orderBy('out_count', 'desc')
                     ->paginate(10);
        }
        if($id == 'applicants'){
            $postupdates = Posts::all();
            foreach ($postupdates as $key) {
                $count = Applicants::where('job_id',$key->id)->count();
                DB::table('posts')
                ->where('id', $key->id)
                ->update(['applicant_count' => $count]);
            }
            $jobs = Posts::active()
                     ->where("author_id", $auth_user->id)
                     ->where('email_or_link','')
                     ->orderBy('applicant_count', 'desc')
                     ->paginate(10);
        }

        $chartdata = '';
        $filtermessage = 'message';
        foreach ($jobs as $job) {
            //$apps = Applicants::where('job_id', $job->id)->get();
            $apps = DB::table('applicants')->select(
                DB::raw('DATE(date_created) as date'),
                DB::raw('count(*) as applicants')
            )->where('job_id', $job->id)->groupBy('date')->get();
            $views = DB::table('views')->select(
                DB::raw('DATE(date_viewed) as date'),
                DB::raw('count(*) as views')
            )->where('job_id', $job->id)->groupBy('date')->get();
            //$views = DB::table('views')->select(DB::raw('DATE(date_viewed) as date), DB::raw('count(*) as views'))->groupBy('date')->get();
            //$views = Views::where('job_id', $job->id)->get();
            $count = 0;
            $data = false;
            //var_dump($views);
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $plans = PostsPackages::all();
        $upgrades = PostUpgrades::all();
        $upgradetypes = Upgrades::all();
        $upgradelist = PostUpgradesLists::all();
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = $filtermessage;
        $this->data['option'] = $id;
        $this->data['jobs'] = $jobs;
        $this->data['plans'] = $plans;
        $this->data['search'] = '';
        $this->data['upgrades'] = $upgrades;
        $this->data['upgradetypes'] = $upgradetypes;
        $this->data['upgradelist'] = $upgradelist;

        return view('account-poster', $this->data);
    }

    public function activesearch(Request $request)
    {

        $auth_user = Auth::user();
        $check = DB::table('referralcode')->where('id',Auth::user()->ref_id)->first();
        $date = Carbon::now()->toDateString();
        $user = Users::find(Auth::user()->id);
        if(Auth::user()->ref_id && empty(Auth::user()->firstlogin)){
            $user = Users::find(Auth::user()->id);
            $user->firstlogin = 1;
            $user->save();
        }
        $jobs = Posts::active()
        ->where("author_id", $auth_user->id)
        ->where('title','like','%'.$request->search.'%')
        ->orderBy('id','desc')
        ->paginate(10);
        $chartdata = '';
        $filtermessage = 'message';
        foreach ($jobs as $job) {
            //$apps = Applicants::where('job_id', $job->id)->get();
            $apps = DB::table('applicants')->select(
                DB::raw('DATE(date_created) as date'),
                DB::raw('count(*) as applicants')
            )->where('job_id', $job->id)->groupBy('date')->get();
            $views = DB::table('views')->select(
                DB::raw('DATE(date_viewed) as date'),
                DB::raw('count(*) as views')
            )->where('job_id', $job->id)->groupBy('date')->get();
            //$views = DB::table('views')->select(DB::raw('DATE(date_viewed) as date), DB::raw('count(*) as views'))->groupBy('date')->get();
            //$views = Views::where('job_id', $job->id)->get();
            $count = 0;
            $data = false;
            //var_dump($views);
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $plans = PostsPackages::all();
        $upgrades = PostUpgrades::all();
        $upgradetypes = Upgrades::all();
        $upgradelist = PostUpgradesLists::all();
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = $filtermessage;
        $this->data['option'] = '';
        $this->data['search'] = $request->search;
        $this->data['jobs'] = $jobs;
        $this->data['plans'] = $plans;
        $this->data['upgrades'] = $upgrades;
        $this->data['upgradetypes'] = $upgradetypes;
        $this->data['upgradelist'] = $upgradelist;

        return view('account-poster', $this->data);
    }
    public function sortinactive($id)
    {
        $auth_user = Auth::user();
        if($id == 'latest'){
            $jobs = Posts::where('status','!=','active')
                     ->where("author_id", $auth_user->id)
                     ->where('expired_at', '>=', Carbon::now())
                     ->orderBy('id', 'desc')
                     ->paginate(10);
        }
        if($id == 'oldest'){
            $jobs = Posts::where('status','!=','active')
                     ->where("author_id", $auth_user->id)
                     ->where('expired_at', '>=', Carbon::now())
                     ->orderBy('id', 'asc')
                     ->paginate(10);
        }
        if($id == 'views'){
            $jobs = Posts::where('status','!=','active')
                     ->where("author_id", $auth_user->id)
                     ->where('expired_at', '>=', Carbon::now())
                     ->orderBy('views', 'desc')
                     ->paginate(10);
        }
        if($id == 'urlclicks'){
            $jobs = Posts::where('status','!=','active')
                     ->where("author_id", $auth_user->id)
                     ->where('expired_at', '>=', Carbon::now())
                     ->where("email_or_link",'!=', '')
                     ->orderBy('out_count', 'desc')
                     ->paginate(10);
        }
        if($id == 'applicants'){
            $postupdates = Posts::all();
            foreach ($postupdates as $key) {
                $count = Applicants::where('job_id',$key->id)->count();
                DB::table('posts')
                ->where('id', $key->id)
                ->update(['applicant_count' => $count]);
            }
            $jobs = Posts::where('status','!=','active')
                     ->where("author_id", $auth_user->id)
                     ->where('email_or_link','')
                     ->where('expired_at', '>=', Carbon::now())
                     ->orderBy('applicant_count', 'desc')
                     ->paginate(10);
        }
        $chartdata = '';
        foreach ($jobs as $job) {
            //$apps = Applicants::where('job_id', $job->id)->get();
            $apps = DB::table('applicants')->select(
                DB::raw('DATE(date_created) as date'),
                DB::raw('count(*) as applicants')
            )->where('job_id', $job->id)->groupBy('date')->get();
            $views = DB::table('views')->select(
                DB::raw('DATE(date_viewed) as date'),
                DB::raw('count(*) as views')
            )->where('job_id', $job->id)->groupBy('date')->get();
            //$views = DB::table('views')->select(DB::raw('DATE(date_viewed) as date), DB::raw('count(*) as views'))->groupBy('date')->get();
            //$views = Views::where('job_id', $job->id)->get();
            $count = 0;
            $data = false;
            //var_dump($views);
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = 'message';
        $this->data['search'] = '';
        $this->data['option'] = $id;
        $this->data['jobs'] = $jobs;

        return view('account-poster-inactive', $this->data);
    }
    public function listJobsInactive()
    {
        $auth_user = Auth::user();

        $jobs = Posts::where('status','!=','active')
                     ->where("author_id", $auth_user->id)
                     ->where('expired_at', '>=', Carbon::now())
                     ->orderBy('id', 'desc')
                     ->paginate(10);
        $chartdata = '';
        foreach ($jobs as $job) {
            //$apps = Applicants::where('job_id', $job->id)->get();
            $apps = DB::table('applicants')->select(
                DB::raw('DATE(date_created) as date'),
                DB::raw('count(*) as applicants')
            )->where('job_id', $job->id)->groupBy('date')->get();
            $views = DB::table('views')->select(
                DB::raw('DATE(date_viewed) as date'),
                DB::raw('count(*) as views')
            )->where('job_id', $job->id)->groupBy('date')->get();
            //$views = DB::table('views')->select(DB::raw('DATE(date_viewed) as date), DB::raw('count(*) as views'))->groupBy('date')->get();
            //$views = Views::where('job_id', $job->id)->get();
            $count = 0;
            $data = false;
            //var_dump($views);
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = '';
        $this->data['option'] = '';
        $this->data['search'] = '';
        $this->data['jobs'] = $jobs;

        return view('account-poster-inactive', $this->data);
    }

    public function inactivesearch(Request $request)
    {
        $auth_user = Auth::user();

        $jobs = Posts::where('status', '!=' ,'active')
                     ->where("author_id", $auth_user->id)
                     ->where('expired_at', '>=', Carbon::now())
                    ->where('title','like','%'.$request->search.'%')
                     ->orderBy('id', 'desc')
                     ->paginate(10);
        $chartdata = '';
        foreach ($jobs as $job) {
            //$apps = Applicants::where('job_id', $job->id)->get();
            $apps = DB::table('applicants')->select(
                DB::raw('DATE(date_created) as date'),
                DB::raw('count(*) as applicants')
            )->where('job_id', $job->id)->groupBy('date')->get();
            $views = DB::table('views')->select(
                DB::raw('DATE(date_viewed) as date'),
                DB::raw('count(*) as views')
            )->where('job_id', $job->id)->groupBy('date')->get();
            //$views = DB::table('views')->select(DB::raw('DATE(date_viewed) as date), DB::raw('count(*) as views'))->groupBy('date')->get();
            //$views = Views::where('job_id', $job->id)->get();
            $count = 0;
            $data = false;
            //var_dump($views);
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = 'sqsqwsq';
        $this->data['option'] = '';
        $this->data['search'] = $request->search;
        $this->data['jobs'] = $jobs;

        return view('account-poster-inactive', $this->data);
    }

    public function sortexpired($id) {
        $auth_user = Auth::user();
        if($id == 'latest'){
            $jobs = Posts::where("author_id", $auth_user->id)
                     ->where('expired_at', '<', Carbon::now())
                     ->orderBy('id', 'desc')
                     ->paginate(10);
        }
        if($id == 'oldest'){
            $jobs = Posts::where("author_id", $auth_user->id)
                     ->where('expired_at', '<', Carbon::now())
                     ->orderBy('id', 'asc')
                     ->paginate(10);
        }
        if($id == 'views'){
            $jobs = Posts::where("author_id", $auth_user->id)
                     ->where('expired_at', '<', Carbon::now())
                     ->orderBy('views', 'desc')
                     ->paginate(10);
        }
        if($id == 'urlclicks'){
            $jobs = Posts::where("author_id", $auth_user->id)
                     ->where('expired_at', '<', Carbon::now())
                     ->where("email_or_link",'!=', '')
                     ->orderBy('out_count', 'desc')
                     ->paginate(10);
        }
        if($id == 'applicants'){
            $postupdates = Posts::all();
            foreach ($postupdates as $key) {
                $count = Applicants::where('job_id',$key->id)->count();
                DB::table('posts')
                ->where('id', $key->id)
                ->update(['applicant_count' => $count]);
            }
            $jobs = Posts::where("author_id", $auth_user->id)
                     ->where('email_or_link','')
                     ->where('expired_at', '<', Carbon::now())
                     ->orderBy('applicant_count', 'desc')
                     ->paginate(10);
        }
        $chartdata = '';
        foreach ($jobs as $job) {
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = 'sdfsdsdf';
        $this->data['option'] = $id;
        $this->data['search'] = '';
        $this->data['jobs'] = $jobs;

        return view('account-poster-expired', $this->data);
    }


    public function expiredsearch(Request $request) {
        $auth_user = Auth::user();
        $jobs = Posts::where('title','like','%'.$request->search.'%')
            ->where("author_id", $auth_user->id)
            ->where('expired_at', '<', Carbon::now())
            ->orderBy('id', 'desc')
            ->paginate(10);
        $chartdata = '';
        foreach ($jobs as $job) {
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = '2ws2ws2ws2ws';
        $this->data['option'] = '';
        $this->data['search'] = $request->search;
        $this->data['jobs'] = $jobs;

        return view('account-poster-expired', $this->data);
    }
    public function listJobsExpired() {
        $auth_user = Auth::user();

        $jobs = Posts::where("author_id", $auth_user->id)
            ->where('expired_at', '<', Carbon::now())
            ->orderBy('id', 'desc')
            ->paginate(10);
        $chartdata = '';
        foreach ($jobs as $job) {
            $apps_count = Applicants::where('job_id', $job->id)->count();
            $views_count = Views::where('job_id', $job->id)->count();
            $job->views_count = $views_count;
            $job->apps_count = $apps_count;
        }
        $this->data['chartdata'] = $chartdata;
        $this->data['filtermessage'] = '';
        $this->data['option'] = '';
        $this->data['search'] = '';
        $this->data['jobs'] = $jobs;

        return view('account-poster-expired', $this->data);
    }


    public function changePassword()
    {
        return view('change_password', $this->data);
    }

    public function getApplicants()
    {
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
        if (count($invoice_cleared) > 0) {
            Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> As per our Terms and Conditions. You cannot View Applicants until you clear all your Incoices.');
            return redirect()->back();
        }  
        $applicants = DB::table('applicants')->orderBy('date_created','asc')->where('author_id', Auth::User()->id)->paginate(10);
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();
        foreach ($applicants as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $this->data['applicants'] = $applicants;
        $this->data['option'] = '';

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }
        return view('applicantshub', $this->data);
    }
        public function getjobfilter($id)
    { 
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
        if (count($invoice_cleared) > 0) {
            Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> As per our Terms and Conditions. You cannot View Applicants until you clear all your Incoices.');
            return redirect()->back();
        } 
        $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->where('job_id',$id)->orderBy('date_created','desc')->paginate(10);
        $fill = DB::table('applicants')->where('author_id', Auth::User()->id)->get();
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();

        foreach ($fill as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $this->data['applicants'] = $applicants;
        $this->data['id'] = $id;
        $this->data['option'] = '';

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }
        return view('applicants', $this->data);
    }

    public function filterstillactive($id)
    {   
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
        if (count($invoice_cleared) > 0) {
            Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> As per our Terms and Conditions. You cannot View Applicants until you clear all your Incoices.');
            return redirect()->back();
        } 
        if ($id == 'no') {
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('still_active',1)->paginate(10);
        }else{
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->whereNULL('still_active')->paginate(10);
        }
        $fill = DB::table('applicants')->where('author_id', Auth::User()->id)->get();
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();

        foreach ($fill as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $this->data['applicants'] = $applicants;
        $this->data['id'] = '';
        $this->data['option'] = '';
        $this->data['filter'] = $id;

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }
        return view('applicantshub', $this->data);
    }

    public function filterstatus($filter,$status)
    {   
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
        if (count($invoice_cleared) > 0) {
            Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> As per our Terms and Conditions. You cannot View Applicants until you clear all your Incoices.');
            return redirect()->back();
        } 
        if ($filter == 'no' && $status != 'all') {
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('still_active',1)->where('status',$status)->paginate(10);
        }elseif($filter == 'yes' && $status != 'all'){
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->whereNULL('still_active')->where('status',$status)->paginate(10);
        }elseif($status == 'all' && $filter == 'yes'){
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->whereNULL('still_active')->paginate(10);
        }elseif($status == 'all' && $filter == 'no'){
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('still_active',1)->paginate(10);
        }elseif($status == 'all' && $filter == 'all'){
            $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->paginate(10);
        }else{
          $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('status',$status)->paginate(10);  
        }
        $fill = DB::table('applicants')->where('author_id', Auth::User()->id)->get();
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();

        foreach ($fill as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $this->data['applicants'] = $applicants;
        $this->data['id'] = '';
        $this->data['option'] = '';
        $this->data['filter'] = $filter;
        $this->data['status'] = $status;

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }
        return view('applicantshub', $this->data);
    }

    public function filterjobstillactive($id,$filter)
    {   
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
        if (count($invoice_cleared) > 0) {
            Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> As per our Terms and Conditions. You cannot View Applicants until you clear all your Incoices.');
            return redirect()->back();
        } 
        if ($filter == 'no') {
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('still_active',1)->paginate(10);
        }else{
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->whereNULL('still_active')->paginate(10);
        }
        $fill = DB::table('applicants')->where('author_id', Auth::User()->id)->get();
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();

        foreach ($fill as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $this->data['applicants'] = $applicants;
        $this->data['id'] = $id;
        $this->data['option'] = '';
        $this->data['filter'] = $filter;

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }
        return view('applicants', $this->data);
    }

    public function filterstatusjob($id,$filter,$status)
    {   
        $invoice_cleared = DB::table('package_info')->where('user_id',Auth::user()->id)->where('payment','Invoice')->where('status','Overdue')->get();
        if (count($invoice_cleared) > 0) {
            Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> As per our Terms and Conditions. You cannot View Applicants until you clear all your Incoices.');
            return redirect()->back();
        } 
        if ($filter == 'no' && $status != 'all') {
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('still_active',1)->where('status',$status)->paginate(10);
        }elseif($filter == 'yes' && $status != 'all'){
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->whereNULL('still_active')->where('status',$status)->paginate(10);
        }elseif($status == 'all' && $filter == 'yes'){
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->whereNULL('still_active')->paginate(10);
        }elseif($status == 'all' && $filter == 'no'){
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('still_active',1)->paginate(10);
        }elseif($status == 'all' && $filter == 'all'){
            $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->paginate(10);
        }else{
          $applicants = DB::table('applicants')->where('job_id', $id)->where('author_id', Auth::User()->id)->orderBy('date_created','desc')->where('status',$status)->paginate(10);  
        }
        $fill = DB::table('applicants')->where('author_id', Auth::User()->id)->get();
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();

        foreach ($fill as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $this->data['applicants'] = $applicants;
        $this->data['id'] = $id;
        $this->data['option'] = '';
        $this->data['filter'] = $filter;
        $this->data['status'] = $status;

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }
        return view('applicants', $this->data);
    }

    public function deleteimage($id)
    {
        DB::table('postsimages')->where('id', '=', $id)->delete();
        Session::flash('success_msg', 'Image Deleted');
        return redirect()->back();
    }

    public function edit($id)
    {   
        if (!is_null($id) && sizeof(Posts::where('id', $id)->get()) > 0) {
            $this->data['categories'] = Categories::all();
            $this->data['subcategories'] = SubCategories::get();
            $this->data['post'] = Posts::where('id', $id)->first();
            $this->data['companylogos'] = CompanyLogos::where('user_id', Auth::id())->get();
            $this->data['uploadfiles'] = DB::table('postsimages')
                    ->where('post_id', '=', $id)
                    ->get();
            $uploads = count($this->data['uploadfiles']);
            $number = 5 - $uploads;
            $this->data['numberofimages'] = $number;
            $admin_questions = DB::table('applicant_questions')->where('user_id','1')->get();
            $user_questions = DB::table('applicant_questions')->where('user_id',Auth::user()->id)->get();
            $this->data['admin_questions'] = $admin_questions;
            $this->data['user_questions'] = $user_questions;
            if (Auth::User()->id != $this->data['post']->author_id || $this->data['post']->expired_at < Carbon::now()) {
                Session::flash('error_msg', 'You do not have access to this post');

                return redirect()->to('/poster');
            }

            return view('edit_job', $this->data);
        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));

            return redirect()->to('/poster');
        }
    }

    public function getApplicantsJob($id)
    {
        $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->paginate(10);
        $categories = DB::table('emailcategories')->get();
        $this->data['categories'] = $categories;
        $jobsId = array();
        foreach ($applicants as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);
        $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->where('job_id', $id)->paginate(10);
        $this->data['applicants'] = $applicants;
        $this->data['id'] = $id;

        foreach ($this->data['applicants'] as $job) {

            $job->date_created = Carbon::parse($job->date_created);
        }

        return view('applicants', $this->data);
    }

    public function updatePassword()
    {
        $v = \Validator::make(
            [
                'password' => Input::get('password'),
                'old_password' => Input::get('old_password'),
                'password_confirmation' => Input::get('password_confirmation'),
            ],
            [
                'password' => 'required|min:6|confirmed',
                'old_password' => 'required',
                'password_confirmation' => 'required',
            ]
        );

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput(Input::all());
        }

        $old_password = Input::get('old_password');

        if (\Auth::validate(['password' => $old_password, 'email' => \Auth::user()->email])) {
            Users::where('id', \Auth::id())->update(['password' => \Hash::make(Input::get('password'))]);
            Session::flash('success_msg', trans('passwords.reset'));

            return redirect()->back();
        } else {
            Session::flash('error_msg', trans('passwords.password_old'));

            return redirect()->back();
        }
    }

    public function uploadimg()
    {
        if (Input::hasFile('file')) {
            $file = Input::file('file');
            $timestamp = uniqid();
            $ext = $file->guessClientExtension();
            $name = $timestamp . "_file." . $ext;
            $folder = 'images';
            if (is_null($folder)) {

                // move uploaded file from temp to uploads directory
                if ($file->move(public_path() . '/uploads/', $name)) {
                    return URL::to('/uploads/' . $name);
                }
            } else {


                if (!\File::exists(public_path() . '/uploads/' . $folder)) {
                    \File::makeDirectory(public_path() . '/uploads/' . $folder);
                }

                // move uploaded file from temp to uploads directory
                if ($file->move(public_path() . '/uploads/' . $folder . '/', $name)) {
                    return URL::to('/uploads/' . $folder . '/' . $name);
                }
            }
        }

        return false;
    }

        public function uploadfiles(Request $request)
    {  
        if ($request->file) {
            $files = $request->file;
            foreach ($files as $file) {
                $timestamp = uniqid();
                $ext = $file->guessClientExtension();
                $name = $timestamp . "_file." . $ext;
                $folder = 'images';
                if (is_null($folder)) {

                    // move uploaded file from temp to uploads directory
                    if ($file->move(public_path() . '/uploads/', $name)) {
                        return URL::to('/uploads/' . $name);
                    }
                } else {


                    if (!\File::exists(public_path() . '/uploads/' . $folder)) {
                        \File::makeDirectory(public_path() . '/uploads/' . $folder);
                    }

                    // move uploaded file from temp to uploads directory
                    if ($file->move(public_path() . '/uploads/' . $folder . '/', $name)) {
                        return URL::to('/uploads/' . $folder . '/' . $name);
                    }
                }

            }
        }

        return false;
    }

    public function uploadimgredactor()
    {
        if (Input::hasFile('file')) {
            $file = Input::file('file');
            $timestamp = uniqid();
            $ext = $file->guessClientExtension();
            $name = $timestamp . "_file." . $ext;
            $folder = 'images';
            if (is_null($folder)) {

                // move uploaded file from temp to uploads directory
                if ($file->move(public_path() . '/uploads/', $name)) {
                    return URL::to('/uploads/' . $name);
                }
            } else {


                if (!\File::exists(public_path() . '/uploads/' . $folder)) {
                    \File::makeDirectory(public_path() . '/uploads/' . $folder);
                }

                // move uploaded file from temp to uploads directory
                if ($file->move(public_path() . '/uploads/' . $folder . '/', $name)) {
                    return response()->json(
                        $data = array(
                            'filelink' => URL::to('/uploads/' . $folder . '/' . $name),
                        ),
                        200
                    );
                }
            }
        }

        return false;
    }

    public function saveJob(Request $request)
    {
        ////echo dd($request);
        //billing fields
        $billing_name = Input::get('billing_name');
        $billing_company = Input::get('billing_company');
        $billing_phone = Input::get('billing_phone');
        $abn = Input::get('abn');
        $billing_address = Input::get('billing_address');
        $billing_email = Input::get('billing_email');
        $billing_ref = Input::get('billing_ref');
        $billing_city = Input::get('billing_city');
        $billing_state = $request->billing_state;
        $billing_zipcode = Input::get('billing_zipcode');
        $billing_house = Input::get('billing_house');
        $billing_street = Input::get('billing_street');
        if ($request->summ > 0) {
        if($billing_ref){
            $ref_verify = \App\Transactions::where('user_id',Auth::user()->id)->where('billing_reference',$billing_ref)->get();
            if (count($ref_verify) > 0) {
                $error = 'Please update Your Billing Reference description to something unique about this job (it will help reconcile your invoices!)';
                Session::flash('error_msg', $error);

                // return redirect()->back()->withInput(Input::all());
                return ["status" => "1", "error" => $error];
            }
        }
        $v = \Validator::make(
            [
                'billing_name' => Input::get('billing_name'),
                'billing_phone' => Input::get('billing_phone'),
                'billing_email' => Input::get('billing_email'),
                'billing_address' => Input::get('billing_address'),
                'billing_city' => Input::get('billing_city'),
                'billing_state' => Input::get('billing_state'),
                'billing_zipcode' => Input::get('billing_zipcode'),
                'billing_company' => Input::get('billing_company'),
                'billing_street_address' => Input::get('billing_house'),
                'billing_street_number' => Input::get('billing_street'),
                'billing_ref' => Input::get('billing_ref'),
            ],
            [
                'billing_name' => 'required',
                'billing_phone' => 'required|regex:/^[\+\d\s]*$/',
                'billing_email' => 'required',
                'billing_address' => 'required',
                'billing_city' => 'required',
                'billing_state' => 'required',
                'billing_zipcode' => 'required',
                'billing_company' => 'required',
                'billing_street_number' => 'required',
                'billing_street_address' => 'required',
                'billing_ref' => 'required',
            ], 
            [
                'billing_ref.required' => 'Please write a short unique billing reference to help you reconcile this purchase on your invoice e.g. Sales job 123',
            ]);


        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            // return redirect()->back()->withInput(Input::all());
            return ["status" => "1", "error" => Utils::messages($v)];
        }
        }
        $auth_user = Auth::user();
        Users::where('id', $auth_user->id)->update(
            [
                'billing_business' => $billing_name,
                'billing_number' => $billing_phone,
                'billing_abn' => $abn,
                'billing_address' => $billing_address,
                'billing_ref' => $billing_ref,
                'billing_email' => $billing_email,
                'billing_postal_code' => Input::get('postal_code'),
                'billing_route' => Input::get('route'),
                'billing_city' => $billing_city,
                'billing_zipcode' => $billing_zipcode,
                'billing_state' => $billing_state,
                'billing_street' => $billing_street,
                'billing_house' => $billing_house,
                'company' => $billing_company,
                'billing_administrative_area' => Input::get('administrative_area_level_1'),
            ]
        );


        // ToDo if success payment
        $saveStatus = $this->store($request);

        // return redirect()->back();
        return json_encode($saveStatus);
    }

        public function savecredit(Request $request)
    {  
        ////echo dd($request);
        //billing fields
        $billing_name = Input::get('billing_name');
        $billing_company = Input::get('billing_company');
        $billing_phone = Input::get('billing_phone');
        $abn = Input::get('abn');
        $billing_address = Input::get('billing_address');
        $billing_email = Input::get('billing_email');
        $billing_ref = Input::get('billing_ref');
        $billing_city = Input::get('billing_city');
        $billing_state = $request->billing_state;
        $billing_zipcode = Input::get('billing_zipcode');
        $billing_house = Input::get('billing_house');
        $billing_street = Input::get('billing_street');
        if ($request->summ > 0) {
            if($billing_ref){
                $ref_verify = \App\Transactions::where('user_id',Auth::user()->id)->where('billing_reference',$billing_ref)->get();
                if (count($ref_verify) > 0) {
                    $error = 'Please update Your Billing Reference description to something unique about this job (it will help reconcile your invoices!)';
                    Session::flash('error_msg', $error);

                    // return redirect()->back()->withInput(Input::all());
                    return ["status" => "1", "error" => $error];
                }
            }
        $v = \Validator::make(
            [
                'billing_name' => Input::get('billing_name'),
                'billing_phone' => Input::get('billing_phone'),
                'billing_email' => Input::get('billing_email'),
                'billing_address' => Input::get('billing_address'),
                'billing_city' => Input::get('billing_city'),
                'billing_state' => Input::get('billing_state'),
                'billing_zipcode' => Input::get('billing_zipcode'),
                'billing_company' => Input::get('billing_company'),
                'billing_street_address' => Input::get('billing_house'),
                'billing_street_number' => Input::get('billing_street'),
                'billing_ref' => Input::get('billing_ref'),
            ],
            [
                'billing_name' => 'required',
                'billing_phone' => 'required|regex:/^[\+\d\s]*$/',
                'billing_email' => 'required',
                'billing_address' => 'required',
                'billing_city' => 'required',
                'billing_state' => 'required',
                'billing_zipcode' => 'required',
                'billing_company' => 'required',
                'billing_street_address' => 'required',
                'billing_street_number' => 'required',
                'billing_ref' => 'required',
            ], 
            [
                'billing_ref.required' => 'Please write a short unique billing reference to help you reconcile this purchase on your invoice e.g. Sales job 123',
            ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            // return redirect()->back()->withInput(Input::all());
            return ["status" => "1", "error" => Utils::messages($v)];
        }
        }
        $auth_user = Auth::user();
        Users::where('id', $auth_user->id)->update(
            [
                'billing_business' => $billing_name,
                'billing_number' => $billing_phone,
                'billing_abn' => $abn,
                'billing_address' => $billing_address,
                'billing_ref' => $billing_ref,
                'billing_email' => $billing_email,
                'billing_postal_code' => Input::get('postal_code'),
                'billing_route' => Input::get('route'),
                'billing_city' => $billing_city,
                'billing_zipcode' => $billing_zipcode,
                'billing_state' => $billing_state,
                'billing_street' => $billing_street,
                'billing_house' => $billing_house,
                'company' => $billing_company,
                'billing_administrative_area' => Input::get('administrative_area_level_1'),
            ]
        );


        // ToDo if success payment
        $saveStatus = $this->storecredits($request);
        // return redirect()->back();
        return json_encode($saveStatus);
    }

        public function storecredits(Request $request)
    {     
        //$distance = $this->getDistance(Input::get('lat'), Input::get('lng'), "-27.512", "153.03300000000002");
        //dd($distance);
        //How is enable ???
        // if (!Utils::hasWriteAccess()) {
        //     Session::flash('error_msg', trans('messages.preview_mode_error'));
        //     return redirect()->back()->withInput(Input::all());
        // }
        // //echo "Testing in progress. Posts will not work. - Mark.";
        $billing_name = Input::get('billing_name');
        $billing_phone = Input::get('billing_phone');
        $abn = Input::get('abn');
        $billing_address = Input::get('billing_address');
        $billing_email = Input::get('billing_email');
        $billing_ref = Input::get('billing_ref');
        $billing_abn = Input::get('billing_abn');
        $coupon_id = Input::get('NhibunsniiMNSK992ID'); // change to coupon ID
        setlocale(LC_MONETARY, 'en_US.UTF-8');
        // $coupon_used = Coupons::where('id', $coupon_id)->first();

        //// die();;
        $transaction = '';
        $creditcalc = 0;
        $trans = new Transactions();
        $auth_user = Auth::user();
        $user = Users::where('id', $auth_user->id)->first();
        $usecredits = Input::get('uuuucMNNMNmmd');
        $transaction = "";
        // Cost of Packages
        $package_cost = 0;
        $package_id = Input::get('packages_stats1');
        if ($package_id > 0) {
            //var_dump(Input::all());
            $package = Packages::where('id', $package_id)->first();
            $package_cost_no_bonus = $package->price;
            $package_cost = $package->price;
            $creditcalc = $package->price + $package->price * $package->discount_percent / 100;
            $usecredits = NULL;
            $trans->package = $package->package_name . ": " . money_format('%.2n',$package_cost_no_bonus);
            $trans->package_discount = $package->discount_percent;
            $trans->package_price = $package->price;
            DB::table('activitylog')->insert([
                ['description' => 'Value Pack Purchase - '.$package->package_name, 'user_id' => Auth::user()->id, 'created_at' => Carbon::now()]
            ]);
        }

        $user = Users::where('id', $auth_user->id)->first();
        $trans->user_id = $user;
        // ADD IN GST

        $fintot = Input::get('summ');
        $trans->totalpassedin = $fintot;
        $trans->user_id = $user->id;

        $listing_total = 0;
        $credit_fee = 0;
        $remaining = 0;
        $GST_remaining = 0;
        $items = $package_cost;


        $listing_total = $package_cost;
        $credits_to_inserted = 0;
        if ($package_cost > 0) {
            //$listing_total = $upgrade_cost + $package_cost + $listing_type->price;
            $listing_total = $package_cost;
            $credits_to_inserted = $creditcalc;
            $trans->creditsremaing = $credits_to_inserted + Auth::user()->credits;
            //Users::where('id', $auth_user->id)->update(['credits' => $credits_to_inserted] );
        }
        if ($credits_to_inserted != 0) {
            Users::where('id',Auth::user()->id)->update(['credits' => $credits_to_inserted+Auth::user()->credits] );
        }
        //$user->credits = $credits_to_inserted;


        $coupon_discount = Input::get('NhibunsniiMNSK992'); // change to coupon ID
        $coupon_exc = Input::get('ZKONONOSDN3223NSK992');
        $coupon_discount_amount = false;
        $aus_contact_amount = false;
        $ref_discount_amount = false;
        $aus_contact_discount = $user->aus_contact_member_no;
        $ref_discount = $user->ref_id;
        $date = Carbon::now()->toDateString();
        $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
        if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
           $disc = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
           $disc1 = $disc->discount_rate;
        }
        else{
            $disc = NULL;
            $disc1 = 0;
        }

        $settings = Utils::getSettings("general");

        ////echo " AFTER" . $listing_total . " HELLO";


        $listing_total_no_disc = $listing_total;
        ////echo $listing_total;
        $ref_discount_amount = 0;

        if ($disc) {
            ////echo "hi";
            $disc1 = $disc->discount_rate;
            $discex = $disc->exclusive;
            $discname = $disc->referral_code;

            ////echo $ref_discount_amount;

            if ($discex == 'yes') {
                $ref_discount_amount = $listing_total_no_disc * $disc1 / 100;
                $listing_total = $listing_total_no_disc - $ref_discount_amount;
                $trans->ref_id = Auth::user()->ref_id;
                $trans->ref_used = $discname;
                $trans->refdisc = $ref_discount_amount;
                $trans->ausc = 0;
                $trans->coupon_used = '';
                $coupon_discount = 0;
                $xero_aus_discount = 0;
                $xero_ref_discount = $disc1;
                $xero_coupon_discount = 0;
            } else {
                $ref_discount_amount = $listing_total * $disc1 / 100;
                $trans->ref_id = Auth::user()->ref_id;
                $listing_total = $listing_total - $ref_discount_amount;
                $trans->ref_id = Auth::user()->ref_id;
                $trans->ref_used = $discname;
                $trans->refdisc = $ref_discount_amount;
            }
            ////echo $listing_total . " HELLO";
        }

        if ($coupon_discount != 0) {
            $coupon_used = Coupons::where('id', $coupon_id)->first();

            if ($coupon_exc == 'yes') {
                ////echo "hi";
                $coupon_discount_amount = $listing_total_no_disc * $coupon_discount / 100;
                $listing_total = $listing_total_no_disc - $coupon_discount_amount;
                ////echo $coupon_discount_amount;
                $trans->ref_id = '';
                $trans->ref_used = '';
                $trans->ausc = 0;
                $trans->coupon_used = $coupon_id;
                $trans->coupon_disc = $coupon_discount_amount;
                $trans->coupon_used = $coupon_used->coupon_code;
                $ref_discount_amount = 0;
                $aus_contact_amount = 0;
                $xero_aus_discount = 0;
                $xero_ref_discount = 0;
                $xero_coupon_discount = $coupon_discount;
            } else {
                ////echo "hi1";
                $coupon_discount_amount = $listing_total * $coupon_discount / 100;
                $listing_total = $listing_total - $coupon_discount_amount;
                $trans->coupon_used = $coupon_id;
                $trans->coupon_disc = $coupon_discount_amount;
                $trans->coupon_used = $coupon_used->coupon_code;
            }
        }
        ////echo $aus_contact_amount;
        $listing_total_gst = $listing_total * 10 / 100;
        $trans->gst_paid = $listing_total_gst;
        $trans->totalnogst = $listing_total;
        $listing_total_with_gst = $listing_total + $listing_total_gst;
        $listing_total = $listing_total_with_gst;
        $listing_total = round($listing_total, 2);



        $credits_less_than = false;
        $trans->creditsin = $user->credits;
        $trans->packageid = $package_id;
        $trans->billing_reference = Input::get('billing_ref');

        ////echo $remaining;
        ////echo $credits_less_than;
        //// die();;
        //var_dump($remaining);
        $settings = Utils::getSettings("general");
        ////echo "HELLO";
        ////echo $listing_total;
        ////echo $listing_total;
        if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
            $credit_fee = $listing_total * $settings->card_fee / 100;
            ////echo $credit_fee;
            $listing_total = $listing_total + $credit_fee;
            $trans->payment = "Credit Card";
            $trans->creditfee = $credit_fee;

        } else if (Input::get('paymentopt') == 'eft') {
            $trans->payment = "Invoice";
        }

        ////echo $listing_total;
        $listing_total = round($listing_total, 2);
        //var_dump($listing_total);

        $summ = $listing_total;

        $trans->total = $summ;
        $trans->save();
        $user = Users::where('id', $auth_user->id)->first();
        $fixed_term = \App\Settings::where('column_key','invoice_terms')->first();
        if(Auth::user()->invoice_terms){
            if ($package_id > 0) {
                DB::table('package_info')->insert(
                    ['user_id' => Auth::user()->id, 'price' => $trans->total, 'creditfee' => $trans->creditfee, 'gstpaid' => $trans->gst_paid,'name' => $trans->package,'purchase_date' => Carbon::now(),'expiry_date' => Carbon::now()->addMonth(6),'payment' => $trans->payment,'invoice_terms' => Auth::user()->invoice_terms,'remaining' => $trans->package_price+(($trans->package_price*$trans->package_discount)/100),'creditsgiven' => $trans->package_price+(($trans->package_price*$trans->package_discount)/100)]
                );
            }
        }else{
            if ($package_id > 0) {
                DB::table('package_info')->insert(
                    ['user_id' => Auth::user()->id, 'price' => $trans->total, 'creditfee' => $trans->creditfee, 'gstpaid' => $trans->gst_paid,'name' => $trans->package,'purchase_date' => Carbon::now(),'expiry_date' => Carbon::now()->addMonth(6),'payment' => $trans->payment,'invoice_terms' => $fixed_term->value_string,'remaining' => $trans->package_price+(($trans->package_price*$trans->package_discount)/100),'creditsgiven' => $trans->package_price+(($trans->package_price*$trans->package_discount)/100)]
                );
            }
        }
/*        $usecredits = Input::get('uuuucMNNMNmmd');
        $incoming_credits = $user->credits;

        if ($creditcalc != 0) {
            $creditcalc = $creditcalc - $summ;
            Users::where('id', $auth_user->id)->update(['credits' => $creditcalc] );
        }

        if ($usecredits == "yes") {
            $credits = $creditcalc;
            $credits = $credits - $summ;
            if ($credits < 0) {
                $credits = 0;
            }
            $user->credits = $credits;
        }

        if ($incoming_credits > 0 ) {
            $credits = $credits - $summ;
            $user->credits = $credits;
            //Users::where('id', $auth_user->id)->update(['credits' => $credits] );
        }*/

        $user->save();
        if (Input::get('adssdar32rfdsfdsf') == "R" && Input::get('paymentopt') == 'creditc') { //R == Ok
                    $token     = Input::get('stripeToken');

            if ($trans->coupon_used !== null) {
                $stripe_coupon_disc = $trans->coupon_disc;
                if ($stripe_coupon_disc > 0) {
                  $trans->coupon_disc = number_format($trans->coupon_disc, 2, '.', '');
                  $stripe_coupon = ", Coupon Code (" . $trans->coupon_used . ") Discount: -$" . number_format($stripe_coupon_disc, 2, '.', '');
                }else{
                $stripe_coupon = null;
                }
            } else {
                $stripe_coupon = null;
            }
            if ($trans->ref_used !== null) {
                $stripe_ref_disc = $trans->refdisc;
                if ($stripe_ref_disc > 0) {
                   $stripe_ref = ", Registration Code (" . $trans->ref_used . ") Discount: -$" . number_format($stripe_ref_disc, 2, '.', '');
                } else{
                   $stripe_ref = null;
                }
            } else {
                $stripe_ref = null;
            }
            $trans->gst_paid = number_format($trans->gst_paid, 2, '.', '');
            $stripe_gst = ", GST(10%): $" . $trans->gst_paid;
            $trans->creditfee = number_format($trans->creditfee, 2, '.', '');
            $stripe_creditfee = ", Credit Card Fee: $" . $trans->creditfee;
            $invoicenum_package_info = DB::table('package_info')->where('user_id',$auth_user->id)->orderBy('id','DESC')->first();
            if($invoicenum_package_info){
                $stripe_reference = 'Reference Number: (DevInvoice@ItsMyCall-ValuePack-' .$invoicenum_package_info->id. '-' .$trans->id.')';
            }else{
                $stripe_reference = null;
            }
            $stripe_description = $stripe_reference . ", " . $trans->package . $stripe_gst . $stripe_creditfee . $stripe_ref . $stripe_coupon . " . For a detailed list of your transactions please refer to your 'My Transactions' link in your account. You will receive a copy of your tax invoice within 24 hours.";
                    $payStatus = $auth_user->charge(
                        round($summ * 100),
                        [ // TODO: 100 replace with total amount paid
                            'source'        => $token,
                            'currency'      => 'aud',
                            'description'   =>  $stripe_description,
                            'receipt_email' => Input::get('billing_email'),
                            'expand' => array('balance_transaction'), // TODO: Input::get('billing_email')
                        ]
                    );
                }
        if (Input::get('adssdar32rfdsfdsf') == "R") {

            $xero = app('XeroPrivate');
            $invoice = app('XeroInvoice');
            $contact = app('XeroContact');
            $payment = new \XeroPHP\Models\Accounting\Payment();
            $address = app('\XeroPHP\Models\Accounting\Address');
            $phone = app('\XeroPHP\Models\Accounting\Phone');
            $lines;

            // Set up the invoice contact
            $contact->setAccountNumber('DevInvoice@ItsMyCall-User-'.Auth::user()->id);
            $contact->setContactStatus('ACTIVE');
            $contact->setName(Input::get('billing_company'));
            $contact->setFirstName($billing_name);
            $contact->setLastName(Input::get('billing_last_name'));
            $contact->setEmailAddress($billing_email);
            $contact->setDefaultCurrency('AUD');
            $contact->setTaxNumber(Input::get('abn'));
            //$contact->addPhone($billing_phone);
            $route = Input::get('route');
            $street = Input::get('street_number');
            $city = Input::get('locality');
            $state = Input::get('administrative_area_level_1');
            $pcode = Input::get('postal_code');
            $addressline1 = $street . " " . $route;
            $address->setAddressLine1($addressline1);
            $address->setCity($city);
            $address->setRegion($state);
            $address->setPostalCode($pcode);
            $address->setCountry('Australia');
            $address->setAddressType('POBOX');
            $contact->addAddress($address);

            $phone->setPhoneNumber(Input::get('billing_phone'));
            $phone->setPhoneType('DEFAULT');
            //dd($phone);
            $contact->addPhone($phone);
            // Assign the contact to the invoice
            $invoice->setContact($contact);
            $invoice->setType('ACCREC');
            $fixed_term = \App\Settings::where('column_key','invoice_terms')->first();
            if (Auth::user()->invoice_terms) {
               $dateInstance = Carbon::now()->addDays(Auth::user()->invoice_terms);
            }elseif($fixed_term && $fixed_term->value_string){
               $dateInstance = Carbon::now()->addDays($fixed_term->value_string);
            }else{
               $dateInstance = Carbon::now()->addDays(7); 
            }
            $invoice->setDate(Carbon::now());
            if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
                $theme = XeroPrivate::loadByGUID('Accounting\\BrandingTheme', 'CreditCardPayment');
                $invoice->setBrandingThemeID($theme->BrandingThemeID);
                $invoice->setDueDate(Carbon::now());
            }else{
                $theme = XeroPrivate::loadByGUID('Accounting\\BrandingTheme', 'InvoicePurchase');
                $invoice->setBrandingThemeID($theme->BrandingThemeID);
                $invoice->setDueDate($dateInstance);
            }
            $invoicenum_package_info = DB::table('package_info')->where('user_id',$auth_user->id)->orderBy('id','DESC')->first();
            if($trans->payment == 'Invoice'){
                    $fixed_term = \App\Settings::where('column_key','invoice_terms')->first();
                    if(Auth::user()->invoice_terms){
                        $this->adminEmailInvoice($invoicenum_package_info,Auth::user()->invoice_terms);
                    }elseif($fixed_term && $fixed_term->value_string){
                        $this->adminEmailInvoice($invoicenum_package_info,$fixed_term->value_string);
                    }else{
                        $this->adminEmailInvoice($invoicenum_package_info,7);
                    }
            }
            $invoice->setInvoiceNumber('DevInvoice@ItsMyCall-ValuePack-' .$invoicenum_package_info->id. '-' .$trans->id);

            if ($billing_ref) {
                $invoice->setReference($billing_ref);
            } else {
                $now = Carbon::now();
                $date_invoice = \Carbon\Carbon::parse($now)->format('d M Y');
                 $invoice->setReference('Value Pack-' .$invoicenum_package_info->id. ' purchased on ' . $date_invoice);
            }

            $invoice->setCurrencyCode('AUD');
            $invoice->setStatus('AUTHORISED');
            $nuline = app("XeroInvoiceLine");
            //Add the post to the invoice
            if ($package_cost > 0) {
                $nuline1 = app("XeroInvoiceLine");
                $package = Packages::where('id', Input::get('packages_stats1'))->first();
                $nuline1->setDescription($package->package_name);
                $nuline1->setQuantity(1);
                if($disc1 > 0){
                    $packagediscount_ref = $package->price-($package->price*$disc1/100);
                    $nuline1->setUnitAmount($packagediscount_ref);
                }else{
                    $nuline1->setUnitAmount($package->price);
                }
                if($coupon_discount > 0){
                    $nuline1->setDiscountRate($coupon_discount);
                }else{
                    $nuline1->setDiscountRate(0);
                }
                $nuline1->setTaxType('OUTPUT');
                $package_detail = Input::get('packages_stats1');
                if ($package_detail == '1') {
                    $nuline1->setAccountCode('230');
                }
                if ($package_detail == '2') {
                    $nuline1->setAccountCode('233');
                }
                if ($package_detail == '3') {
                    $nuline1->setAccountCode('236');
                }
                if ($package_detail == '4') {
                    $nuline1->setAccountCode('239');
                }
                $invoice->addLineItem($nuline1);
            }

            if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription("Credit Card Fee");
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount($credit_fee);
                $nuline2->setAccountCode('404');
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }

            if (Input::get('paymentopt') != 'creditc') {
                $nuline2 = app("XeroInvoiceLine");
                //$invoice->addLineItem($nuline3);
                $nuline2->setDescription("Invoice Method");
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount(0);
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }

            if ($coupon_discount != 0) {
                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription("The Coupon Code '" . $coupon_used->coupon_code ."' discount (displayed in the Disc % column) has been applied and is deducted from the Unit Price column.");
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount(0);
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }
            
            if ($trans->ref_used !== null) {
                    $invoice_ref_disc = $trans->refdisc;
                    if ($invoice_ref_disc > 0) {
                       $invoice_ref = "Please note your registration Code '" . $trans->ref_used . "' discount(s) have been automatically applied and are displayed in the Unit Price column. For this purchase your registration code has saved you an additional $" . number_format($invoice_ref_disc, 2, '.', '') . " off our standard prices!";
                    } else{
                       $invoice_ref = null;
                    }
                } else {
                    $invoice_ref = null;
                }
            if ($invoice_ref) {
                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription($invoice_ref);
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount(0);
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }

//            if($credits_less_than) {
//                $nuline2 = app("XeroInvoiceLine");
//                $invoice->addLineItem($nuline2);
//                $nuline2->setDescription("Credit Card Fee");
//                $nuline2->setQuantity(1);
//                $nuline2->setUnitAmount($credits_less_than);
//                $nuline1->setDiscountRate(100);
//                $invoice->addLineItem($nuline2);
//            }



            //$invoice->setTotalDiscount('50');
            //var_dump($invoice);
            $xero->save($invoice);
            if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
                $invoiceinfo = XeroPrivate::loadByGUID('Accounting\\Invoice', $invoice->InvoiceID);
                $contactinfo = XeroPrivate::loadByGUID('Accounting\\Account', 'EFTPOS');
                $payment->setInvoice($invoiceinfo);
                $payment->setAccount($contactinfo);
                $payment->setDate(Carbon::now());
                $payment->setIsReconciled(0);
                $payment->setAmount($summ);
                $xero->save($payment);
            }
        }


        //$this->sendEmail($post_item);
        //// die();;
        Session::flash('success_msg', 'Successfully purchased Value Pack');

         return ["status" => "200"];
        //return redirect()->to('/poster');
    }

    public function store(Request $request)
    {   
        //$distance = $this->getDistance(Input::get('lat'), Input::get('lng'), "-27.512", "153.03300000000002");
        //dd($distance);
        //How is enable ???
        // if (!Utils::hasWriteAccess()) {
        //     Session::flash('error_msg', trans('messages.preview_mode_error'));
        //     return redirect()->back()->withInput(Input::all());
        // }
        $v = \Validator::make(
            [
                'title' => Input::get('title'),
                'description' => Input::get('description'),
                'company' => Input::get('company'),
                'country' => Input::get('country'),
                'state' => Input::get('state'),
                'category_id' => Input::get('category_id'),
                'category' => Input::get('category'),
                'sub_category' => Input::get('sub_category'),
                'salary' => Input::get('salary'),
                'file' => Input::file('featured_image'),
                'experience' => Input::get('experience'),
            ],
            [
                'title' => 'required',
                'description' => 'required',
                'file' => 'required|image',
                'company' => 'required',
                'country' => 'required',
                'state' => 'required',
                'category_id' => 'required',
                'category' => 'required',
                'sub_category' => 'required',
                'salary' => 'required',
                'experience' => 'required',
            ]
        );

        $v = \Validator::make(
            [
                'title' => Input::get('title'),
                'description' => Input::get('description'),
                'company' => Input::get('company'),
                'state' => Input::get('state'),
                // 'category_id'  => Input::get('category_id'),
                'category' => Input::get('category'),
                'sub_category' => Input::get('sub_category'),
                'status' => Input::get('status'),
                'email_or_link' => Input::get('email_or_link')
            ],
            [
                'title' => 'required',
                'description' => 'required',
                'company' => 'required',
                // 'country'      => 'required',
                'state' => 'required',
                // 'category_id'  => 'required',
                'category' => 'required',
                'sub_category' => 'required',
            ] + (!!Input::get('application_method') ? ['email_or_link' => 'required|url'] : [])
        );

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));

            // return redirect()->back()->withInput(Input::all());
            return ["status" => "1", "error" => Utils::messages($v)];
        }
         setlocale(LC_MONETARY, 'en_US.UTF-8');
        // //echo "Testing in progress. Posts will not work. - Mark.";
        $billing_name = Input::get('billing_name');
        $billing_phone = Input::get('billing_phone');
        $abn = Input::get('abn');
        $billing_address = Input::get('billing_address');
        $billing_email = Input::get('billing_email');
        $billing_ref = Input::get('billing_ref');
        $billing_abn = Input::get('billing_abn');
        $coupon_id = Input::get('NhibunsniiMNSK992ID'); // change to coupon ID

        // $coupon_used = Coupons::where('id', $coupon_id)->first();
        $post_item = new Posts();
        $post_item->coupon_used = $coupon_id;
        $post_item->parentsoption = Input::get('parents_filter');
        $post_item->title = Input::get('title');
        $post_item->author_id = \Auth::id();
        $post_item->description = Input::get('description');
        $post_item->short_description = Input::get('short_description');
        $post_item->company = Input::get('company');
        $post_item->slug = Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp . "-" . Str::slug(
                Input::get('category')
            );

        $post_item->category_id = Input::get('category');
        $post_item->category = Str::slug(Input::get('category'));
        $post_item->subcategory = Str::slug(Input::get('sub_category'));

        //salary_fields
        $post_item->salarytype = Str::slug(Input::get('salarytype'));
        $post_item->incentivestructure = Str::slug(Input::get('incentivestructure'));
        $post_item->paycycle = Str::slug(Input::get('paycycle'));
        if($post_item->salarytype == 'annual'){
            $post_item->salary = str_replace( ',', '', Input::get('salary'));
            $post_item->annual_ask_select = $request->annual_ask_select_pass;
            $post_item->hourly_ask_select = 'no';
            if($request->annual_ask_select_pass == 'yes'){
                $post_item->hrsalary = Input::get('annual_ask_salary_enter');
            }else{
                $post_item->hrsalary = 0; 
            }
        }else{
            $post_item->hrsalary = Input::get('hrsalary');
            $post_item->hourly_ask_select = $request->hourly_ask_select_pass;
            $post_item->annual_ask_select = 'no';
            if($request->hourly_ask_select_pass == 'yes'){
                $post_item->salary = str_replace( ',', '', Input::get('hourly_ask_salary_enter'));
            }else{
                $post_item->salary = 0; 
            }
        }
        //location fields
        $post_item->joblocation = Input::get('joblocation');
        $post_item->postcode = Input::get('postcode');
        $post_item->lat = Input::get('lat');
        $post_item->lng = Input::get('lng');
        $post_item->state = Input::get('state');
        $post_item->showsalary = Input::get('showsalary');
        $post_item->country = Str::slug(Input::get('country'));

        // $post_item->experience = Str::slug(Input::get('experience'));
        $post_item->status = Input::get('status');
        $post_item->employment_type = Input::get('employment_type');
        $post_item->employment_term = Input::get('employment_term');
        $post_item->person_email = Input::get('person_email');
        $post_item->email_or_link = Input::get('email_or_link');
        $post_item->selling1 = Input::get('selling1');
        $post_item->selling2 = Input::get('selling2');
        $post_item->selling3 = Input::get('selling3');
        $post_item->work_from_home = Input::get('work_from_home');
        $post_item->expired_at = Carbon::now()->addDays(Posts::EXPIRY_DAYS);
        $post_item->selectquestions = Input::get('questionspricing');

        if (!Input::get('application_method')) {
            $post_item->email_or_link = '';
        } else {
            $post_item->person_email = '';
        }
        $post_item->posts_packages = Input::get('posts_packages');
        if($post_item->posts_packages == 1) {
            $post_item->video_link = Input::get('video_link');
        }
        if ($post_item->posts_packages == 1) {
            DB::table('activitylog')->insert([
                ['description' => 'Featured Job Purchase - '.Input::get('title'), 'user_id' => Auth::user()->id, 'created_at' => Carbon::now()]
            ]);
        }
        if ($post_item->posts_packages == 2) {
            DB::table('activitylog')->insert([
                ['description' => 'Premium Job Purchase - '.Input::get('title'), 'user_id' => Auth::user()->id, 'created_at' => Carbon::now()]
            ]);
        }
        if ($post_item->posts_packages == 3) {
            DB::table('activitylog')->insert([
                ['description' => 'Standard Job Purchase - '.Input::get('title'), 'user_id' => Auth::user()->id, 'created_at' => Carbon::now()]
            ]);
        }
        $post_item->featured_image = Input::get('featured_image_use');
        if(!empty(Input::get('featured_image'))) {
            $company_logos = new CompanyLogos();
            $company_logos->file = Input::get('featured_image');
            $company_logos->user_id = \Auth::id();
            $company_logos->save();
        }
        if ($request->question != null) {
            $questions = json_encode($request->question);
            $post_item->questions = $questions;
        }
        if(!empty(Input::get('featured_image_delete'))) {
            $delete_logos = explode(", ",Input::get('featured_image_delete')); 
            CompanyLogos::destroy($delete_logos);
        }

        if (Input::get('paymentopt') == 'creditc') {
            $post_item->paid = "yes";
        } else {
            $post_item->paid = "no";
        }
        $post_item->save();
        if(count(Input::get('multipleimg')) > 0 && $post_item->posts_packages == 1) {

           $files = explode(",",Input::get('multipleimg'));

            foreach ($files as $file) {
                if (strlen($file) > 10) {
                DB::table('postsimages')->insert([
                ['post_id' => $post_item->id , 'jobimages' => $file ]
                ]);
                  }
            }
        }
        $this->sendEmail($post_item,$post_item->paycycle);
        $this->postTweet($post_item);
        //// die();;
        $transaction = '';
        $creditcalc = 0;
        $trans = new Transactions();
        $trans->job_id = $post_item->id;
        $auth_user = Auth::user();
        $user = Users::where('id', $auth_user->id)->first();
        $usecredits = Input::get('uuuucMNNMNmmd');
        $coupon_used = false;
        $job_discount_rate = Input::get('job_discount_rate'); // change to coupon ID
        $packages_discount_rate = Input::get('packages_discount_rate');
        $boost_discount_rate = Input::get('boost_discount_rate');
        $question_discount_rate = Input::get('question_discount_rate');
        $coupon_exc = Input::get('ZKONONOSDN3223NSK992');
        $transaction .= "";
        // Cost of Packages
        $package_cost = 0;
        $package_id = Input::get('packages_stats1');
        if ($package_id > 0) {
            //var_dump(Input::all());
            $package = Packages::where('id', $package_id)->first();
            $package_cost_no_bonus = $package->price;
            $package_cost = $package->price;
            $creditcalc = $package->price + $package->price * $package->discount_percent / 100;
            $usecredits = "yes";
            $trans->package = $package->package_name . ": " . money_format('%.2n',$package_cost_no_bonus);
            $trans->package_id = $package_id;
            $trans->package_discount = $package->discount_percent;
            $trans->package_price = $package->price;
            DB::table('activitylog')->insert([
                ['description' => 'Value Pack Purchase - '.$package->package_name, 'user_id' => Auth::user()->id, 'created_at' => Carbon::now()]
            ]);
        }


        // Cost of Upgrades
        $upgrades = json_decode(Input::get('posts_upgrades'));
        $upgrade_cost = 0;
        $upgradecost = 0;
        $upgradecostcoupon = 0;
        $uppiescoupon = '';
        $uppies = '';
        if (is_array($upgrades)) {
            $upgrade_item = new UpgradeInfo();
            foreach ($upgrades as $key => $value) {
                if ($key == 0 OR $value == 0) {
                    continue;
                }
                $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                ////echo "SUPhello";
                ////echo "SUPhello";
                if (Auth::user()->ref_id) {
                    $date = Carbon::now()->toDateString();
                    $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                    if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                        $up = DB::table('referralupgrades')->where('referral_id',Auth::user()->ref_id)->where('id', $key)->first();
                        $upgrade_cost = $upgrade_cost + $up->price * $value;
                        $upgrade_price = $up->price * $value;
                        $uppies .= $value."x ".$up->name.": ".money_format('%.2n',$up->price)."<br><br>";
                        $upgradecostinfo1 =  $up->price * $value;
                        $upc = Upgrades::where('id', $key)->first();
                        $upgradecost = $upgradecost + $upc->price * $value;
                        $upgradecostcoupon = $upgradecostcoupon + $upc->actual_price * $value;
                        $upgradecostinfo2 = $upc->actual_price * $value;
                        $uppiescoupon .= $value."x ".$up->name.": ".money_format('%.2n',$upc->actual_price)."<br><br>";
                    }
                    else{
                        $referralviewed = Users::find(Auth::user()->id);
                        $referralviewed->referral_viewed = 1;
                        $referralviewed->save();
                        $up = Upgrades::where('id', $key)->first();
                        $upgrade_cost = $upgrade_cost + $up->price * $value;
                        $upgradecostinfo1 = $up->price * $value;
                        $upgradecostcoupon = $upgradecostcoupon + $up->actual_price * $value;
                        $upgradecostinfo2 = $up->actual_price * $value;
                        $upgrade_price = $up->price * $value;
                        $uppies .= $value."x ".$up->name.": ".money_format('%.2n',$up->price)."<br><br>";
                        $uppiescoupon .= $value."x ".$up->name.": ".money_format('%.2n',$up->actual_price)."<br><br>";
                            }
                }
                else{

                    $up = Upgrades::where('id', $key)->first();
                    $upgrade_cost = $upgrade_cost + $up->price * $value;
                    $upgradecostinfo1 = $up->price * $value;
                    $upgradecostinfo2 = $up->actual_price * $value;
                    $upgradecostcoupon = $upgradecostcoupon + $up->actual_price * $value;
                    $upgrade_price = $up->price * $value;
                    $uppies .= $value."x ".$up->name.": ".money_format('%.2n',$up->price)."<br><br>";
                    $uppiescoupon .= $value."x ".$up->name.": ".money_format('%.2n',$up->actual_price)."<br><br>";
                }
            $coupon_discount = Input::get('boost_discount_rate'); // change to coupon ID
            $coupon_exc = Input::get('ZKONONOSDN3223NSK992');
            $date = Carbon::now()->toDateString();
            $disc = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
            if ($coupon_discount != 0) {
                    if ($coupon_exc == 'yes') {
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $calculatedisc = $upgradecostinfo2 * ($coupon_discount/100);
                        $posts_upgrades->upgrade_price = $upgradecostinfo2-$calculatedisc;
                        $posts_upgrades->save();
                    }
                    elseif(count($referralcode) > 0 && $referralcode->date_end < $date){
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $calculatedisc = $upgradecostinfo2 * ($coupon_discount/100);
                        $posts_upgrades->upgrade_price = $upgradecostinfo2-$calculatedisc;
                        $posts_upgrades->save();
                    }
                    elseif(empty($referralcode)){
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $calculatedisc = $upgradecostinfo2 * ($coupon_discount/100);
                        $posts_upgrades->upgrade_price = $upgradecostinfo2-$calculatedisc;
                        $posts_upgrades->save();
                    }
                    elseif($coupon_exc == 'no' && count($referralcode) > 0 && $referralcode->date_end >= $date){
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $calculatedisc = $upgradecostinfo1-($upgradecostinfo1 * ($coupon_discount/100));
                        $posts_upgrades->upgrade_price = $calculatedisc;
                        $posts_upgrades->save();
                    }
                    else{
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $posts_upgrades->save();
                    }
                }
                elseif(count($referralcode) > 0 && $referralcode->date_end >= $date){
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $posts_upgrades->upgrade_price = $upgradecostinfo1;
                        $posts_upgrades->save();
                }
                else{
                        $posts_upgrades = new PostUpgrades();
                        $posts_upgrades->post_id = $post_item->id;
                        $posts_upgrades->upgrade_id = $key;
                        $posts_upgrades->count = $value;
                        $posts_upgrades->is_purchased = true;
                        $posts_upgrades->upgrade_price = $upgradecostinfo1;
                        $posts_upgrades->save();

                }
            }
            $date = Carbon::now()->toDateString();
            $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
            $coupon_discount = Input::get('boost_discount_rate'); // change to coupon ID
            $coupon_exc = Input::get('ZKONONOSDN3223NSK992');

            if ($coupon_discount != 0) {
                if ($coupon_exc == 'yes') {
                    $trans->upgrades = $uppiescoupon;
                    $upgrade_cost = $upgradecostcoupon;
                }
                elseif(empty(Auth::user()->ref_id)){
                    $trans->upgrades = $uppiescoupon;
                    $upgrade_cost = $upgradecostcoupon;
                }
                elseif (count($referralcode) > 0 && $referralcode->date_end < $date) {
                    $trans->upgrades = $uppiescoupon;
                    $upgrade_cost = $upgradecostcoupon;
                }
                else{
                     $trans->upgrades = $uppies;
                }
            }
            else{
                 $trans->upgrades = $uppies;
            }
        }

        $user = Users::where('id', $auth_user->id)->first();
        // ADD IN GST

        $fintot = Input::get('summ');
        $trans->totalpassedin = $fintot;
        $trans->user_id = $user->id;
        $trans->billing_reference = Input::get('billing_ref');
        $date = Carbon::now()->toDateString();
        $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
        $question_cost = 0;
        $question_cost_original = 0;
        if ($request->questionspricing) {
            // Cost of Questions
                if (Auth::user()->ref_id) {
                    if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                        $listing_question = DB::table('referral_questions')->where('referral_id',Auth::user()->ref_id)->first();
                        $question_cost = $listing_question->price;
                        $listing_question_exc = DB::table('questionspricing')->first();
                        $question_cost_original = $listing_question_exc->price;
                        $trans->questions = $listing_question->title .": ".money_format('%.2n',$listing_question->price);            
                    }
                    else{
                        $listing_question =  DB::table('questionspricing')->first();
                        $question_cost = $listing_question->price;
                        $question_cost_original = $listing_question->price;
                        $trans->questions = $listing_question->title .": ".money_format('%.2n',$listing_question->price);                   
                        }
                }
                else{
                    $listing_question =  DB::table('questionspricing')->first();
                    $question_cost = $listing_question->price;
                    $question_cost_original = $listing_question->price;
                    $trans->questions = $listing_question->title .": ".money_format('%.2n',$listing_question->price);
                }
        }

        // Cost of Listing Packages
                if (Auth::user()->ref_id) {
                    if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                        $listing_type = DB::table('referralpackages')->where('referral_id',Auth::user()->ref_id)->where('id', $post_item->posts_packages)->first();
                        $listing_type_exc = PostsPackages::where('id', $post_item->posts_packages)->first();
                        $trans->listing_type = $listing_type->title .": ".money_format('%.2n',$listing_type->price);            
                    }
                    else{
                        $referralviewed = Users::find(Auth::user()->id);
                        $referralviewed->referral_viewed = 1;
                        $referralviewed->save();
                        $listing_type = PostsPackages::where('id', $post_item->posts_packages)->first();
                        $trans->listing_type = $listing_type->title .": ".money_format('%.2n',$listing_type->price);            
                            }
                }
                else{
                    $listing_type = PostsPackages::where('id', $post_item->posts_packages)->first();
                    $trans->listing_type = $listing_type->title .": ".money_format('%.2n',$listing_type->price);
                }
            if ($job_discount_rate != 0) {
                if ($coupon_exc == 'yes' && Auth::user()->ref_id && count($referralcode) > 0 && $referralcode->date_end >= $date) {
                   $listing_type = $listing_type_exc;
                }
            }
            if (Auth::user()->ref_id) {
                    $date = Carbon::now()->toDateString();
                    $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                    if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                            $listing_type_save = PostsPackages::where('id', $post_item->posts_packages)->first();
                            $listing_total_no_discup = $listing_type_save->price+$upgradecost+$question_cost_original;
                            $trans->refdiscup = $listing_total_no_discup-($upgrade_cost + $listing_type->price + $question_cost);        
                    }
                }
        $listing_total = 0;
        $partial_credits = 0;
        $credit_fee = 0;
        $remaining = 0;
        $GST_remaining = 0;
        $coupon_discount_amount = false;
        $aus_contact_amount = false;
        $ref_discount_amount = false;
        $credits_to_inserted = 0;
        $coupon_discount_package = 0;
        $coupon_discount_boost = 0;
        $coupon_discount_job = 0;
        $coupon_discount_question = 0;
        $aus_contact_discount = $user->aus_contact_member_no;
        ////echo $user->credits;
        $ref_discount_amount = 0;
        $ref_discount_amount = false;
        $ref_discount = $user->ref_id;
        $date = Carbon::now()->toDateString();
        $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
        if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
           $disc = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
           $disc1 = $disc->discount_rate;
        }
        else{
            $disc = NULL;
            $disc1 = 0;
        }

        $settings = Utils::getSettings("general");


        ////echo " AFTER" . $listing_total . " HELLO";

        if ($disc) {
            ////echo "hi";
            $disc1 = $disc->discount_rate;
            $discex = $disc->exclusive;
            $discname = $disc->referral_code;
            ////echo $ref_discount_amount;
            if ($discex == 'yes') {
                if ($package_id > 0) {
                   $ref_discount_amount = $package_cost * $disc1 / 100;
                }else{
                    $ref_discount_amount = 0;
                }
                $package_cost = $package_cost - $ref_discount_amount;
                $trans->ref_id = Auth::user()->ref_id;
                $trans->ref_used = $discname;
                $trans->refdisc = $ref_discount_amount;
                $trans->ausc = 0;
                $trans->coupon_used = '';
                $coupon_discount = 0;
            } else {
                if ($package_id > 0) {
                  $ref_discount_amount = $package_cost * $disc1 / 100;
                }else{
                   $ref_discount_amount = 0;
                }
                $trans->ref_id = Auth::user()->ref_id;
                $package_cost = $package_cost - $ref_discount_amount;
                $trans->ref_used = $discname;
                $trans->refdisc = $ref_discount_amount;
            }
            ////echo $listing_total . " HELLO";
        }
        if ($job_discount_rate != 0 || $boost_discount_rate != 0 || $packages_discount_rate != 0 || $question_discount_rate != 0) {
            $coupon_used = Coupons::where('id', $coupon_id)->first();

            if ($coupon_exc == 'yes') {
                ////echo "hi";
                if ($package_cost > 0) {
                        //$listing_total = $upgrade_cost + $package_cost + $listing_type->price;
                        $jobcost=$listing_type->price;
                        if ($coupon_used->boost_discount_rate) {
                           $coupon_discount_boost=$upgrade_cost*($coupon_used->boost_discount_rate/100);
                           $upgrade_cost=$upgrade_cost-$coupon_discount_boost;
                        }else{
                            $coupon_discount_boost = 0;
                        }
                        if ($coupon_used->question_discount_rate) {
                           $coupon_discount_question=$question_cost*($coupon_used->question_discount_rate/100);
                           $question_cost=$question_cost-$coupon_discount_question;
                        }else{
                            $coupon_discount_question = 0;
                        }
                        if ($coupon_used->job_discount_rate) {
                           $coupon_discount_job=$listing_type->price*($coupon_used->job_discount_rate/100);
                           $jobcost=$jobcost-$coupon_discount_job;
                        }else{
                            $coupon_discount_job = 0;
                        }
                        $items = $upgrade_cost + $jobcost + $question_cost;
                        $listing_total = $items;
                        $listing_total = $package_cost;
                        $listing_total_no_disc = $listing_total;
                        if ($coupon_used->packages_discount_rate) {
                           $coupon_discount_package=$listing_total_no_disc*($coupon_used->packages_discount_rate/100);
                        }else{
                            $coupon_discount_package = 0;
                        }
                        $listing_total = $listing_total_no_disc - $coupon_discount_package;
                        $credits_to_inserted = $creditcalc - $items;
                        $trans->creditsremaing = $credits_to_inserted;
                        $coupon_discount_amount = $coupon_discount_package;
                        $coupon_discount_amount_up = $coupon_discount_job+$coupon_discount_boost+$coupon_discount_question;
                        $trans->coupon_disc = $coupon_discount_amount;
                        $trans->coupon_disc_up = $coupon_discount_amount_up;
                        //Users::where('id', $auth_user->id)->update(['credits' => $credits_to_inserted] );
                    }
                else{   $listing_total_no_disc = $upgrade_cost+$listing_type->price;
                        $jobcost=$listing_type->price;
                        if ($coupon_used->boost_discount_rate) {
                           $coupon_discount_boost=$upgrade_cost*($coupon_used->boost_discount_rate/100);
                           $upgrade_cost=$upgrade_cost-$coupon_discount_boost;
                        }else{
                            $coupon_discount_boost = 0;
                        }
                        if ($coupon_used->question_discount_rate) {
                           $coupon_discount_question=$question_cost*($coupon_used->question_discount_rate/100);
                           $question_cost=$question_cost-$coupon_discount_question;
                        }else{
                            $coupon_discount_question = 0;
                        }
                        if ($coupon_used->job_discount_rate) {
                           $coupon_discount_job=$listing_type->price*($coupon_used->job_discount_rate/100);
                           $jobcost=$jobcost-$coupon_discount_job;
                        }else{
                            $coupon_discount_job = 0;
                        }
                        $items = $upgrade_cost + $jobcost + $question_cost;
                        $listing_total = $items;
                        $coupon_discount_amount = $coupon_discount_job+$coupon_discount_boost+$coupon_discount_question;
                        $trans->coupon_disc = $coupon_discount_amount;
                }
                ////echo $coupon_discount_amount;
                $trans->ref_id = '';
                $trans->ref_used = '';
                $trans->refdisc = 0;
                $trans->refdiscup = 0;
                $trans->ausc = 0;
                $trans->coupon_used = $coupon_id;
                $coupon_discount_amount = $coupon_discount_boost + $coupon_discount_job +$coupon_discount_package+$coupon_discount_question;
                $trans->coupon_disc = $coupon_discount_amount;
                $trans->coupon_used = $coupon_used->coupon_code;
                $ref_discount_amount = 0;
                $aus_contact_amount = 0;
                $xero_aus_discount = 0;
                $xero_ref_discount = 0;
            } else {
                ////echo "hi1";
                if ($package_cost > 0) {
                        //$listing_total = $upgrade_cost + $package_cost + $listing_type->price;
                        $jobcost=$listing_type->price;
                        if ($coupon_used->boost_discount_rate) {
                           $coupon_discount_boost=$upgrade_cost*($coupon_used->boost_discount_rate/100);
                           $upgrade_cost=$upgrade_cost-$coupon_discount_boost;
                        }else{
                            $coupon_discount_boost = 0;
                        }
                        if ($coupon_used->question_discount_rate) {
                           $coupon_discount_question=$question_cost*($coupon_used->question_discount_rate/100);
                           $question_cost=$question_cost-$coupon_discount_question;
                        }else{
                            $coupon_discount_question = 0;
                        }
                        if ($coupon_used->job_discount_rate) {
                           $coupon_discount_job=$listing_type->price*($coupon_used->job_discount_rate/100);
                           $jobcost=$jobcost-$coupon_discount_job;
                        }else{
                            $coupon_discount_job = 0;
                        }
                        $items = $upgrade_cost + $jobcost + $question_cost;
                        $listing_total = $items;
                        $listing_total = $package_cost;
                        $listing_total_no_disc = $listing_total;
                        if ($coupon_used->packages_discount_rate) {
                           $coupon_discount_package=$listing_total_no_disc*($coupon_used->packages_discount_rate/100);
                        }else{
                            $coupon_discount_package = 0;
                        }
                        $listing_total = $listing_total_no_disc - $coupon_discount_package;
                        $credits_to_inserted = $creditcalc - $items;
                        $trans->creditsremaing = $credits_to_inserted;
                        $coupon_discount_amount = $coupon_discount_package;
                        $coupon_discount_amount_up = $coupon_discount_job+$coupon_discount_boost+$coupon_discount_question;
                        $trans->coupon_disc = $coupon_discount_amount;
                        $trans->coupon_disc_up = $coupon_discount_amount_up;
                        //Users::where('id', $auth_user->id)->update(['credits' => $credits_to_inserted] );
                    }
                else{   $listing_total_no_disc = $upgrade_cost+$listing_type->price;
                        $jobcost=$listing_type->price;
                        if ($coupon_used->boost_discount_rate) {
                           $coupon_discount_boost=$upgrade_cost*($coupon_used->boost_discount_rate/100);
                           $upgrade_cost=$upgrade_cost-$coupon_discount_boost;
                        }else{
                            $coupon_discount_boost = 0;
                        }
                        if ($coupon_used->question_discount_rate) {
                           $coupon_discount_question=$question_cost*($coupon_used->question_discount_rate/100);
                           $question_cost=$question_cost-$coupon_discount_question;
                        }else{
                            $coupon_discount_question = 0;
                        }
                        if ($coupon_used->job_discount_rate) {
                           $coupon_discount_job=$listing_type->price*($coupon_used->job_discount_rate/100);
                           $jobcost=$jobcost-$coupon_discount_job;
                        }else{
                            $coupon_discount_job = 0;
                        }
                        $items = $upgrade_cost + $jobcost + $question_cost;
                        $listing_total = $items;
                        $coupon_discount_amount = $coupon_discount_job+$coupon_discount_boost+$coupon_discount_question;
                        $trans->coupon_disc = $coupon_discount_amount;
                }
                $trans->coupon_used = $coupon_used->coupon_code;
            }
        }
        else{
            $items = $upgrade_cost + $listing_type->price + $question_cost;
            $listing_total = $items;
            if ($package_cost > 0) {
            //$listing_total = $upgrade_cost + $package_cost + $listing_type->price;
            $listing_total = $package_cost;
            $credits_to_inserted = $creditcalc - $items;
            $trans->creditsremaing = $credits_to_inserted;
            //Users::where('id', $auth_user->id)->update(['credits' => $credits_to_inserted] );
            }
            $listing_total_no_disc = $listing_total;
        }
        if ($usecredits == "yes") {
            //var_dump($listing_total_with_gst - $user->credits);
            $trans->creditsused = "yes";
            $trans->payment = "Credits";
            $transaction .= "Starting Credits: " . $user->credits . "<br>";

            if ($package_id > 0) {

            }else {
                $trans_user = Auth::user();
                $remainingwelcome = Auth::user()->welcome_remaining;
                $remainingbonus = Auth::user()->bonusremaining;
                        $welcome_shift = 0;
                        $welcome_shift = $listing_total;
                        if ($remainingwelcome > 0) {
                            if ($remainingwelcome >= $listing_total) {
                                $trans_user->welcome_remaining = $remainingwelcome-$listing_total;
                                $welcome_shift = 0;
                            }else{
                                $trans_user->welcome_remaining = 0;
                                $welcome_shift = $welcome_shift-$remainingwelcome;
                            }
                        }
                        if ($remainingbonus > 0) {
                           if ($remainingbonus >= $welcome_shift) {
                                $trans_user->bonusremaining = $remainingbonus-$welcome_shift;
                                $welcome_shift = 0;
                            }else{
                                $trans_user->bonusremaining = 0;
                                $welcome_shift = $welcome_shift-$remainingbonus;
                            }
                        }
                        $package_info = DB::table('package_info')->where('user_id',Auth::user()->id)->where('expiry_date','>=',Carbon::now())->where('remaining','>',0)->orderBy('remaining','asc')->get();
                        if (count($package_info) > 0) {
                            $trans_user->remaining_shift = 0;
                            $trans_user->remaining_shift = $welcome_shift;
                           foreach ($package_info as $package) {
                              if ($package->remaining >= $trans_user->remaining_shift) {
                                    $remainingpackage = $package->remaining-$trans_user->remaining_shift;
                                    DB::table('package_info')
                                            ->where('id', $package->id)
                                            ->update(['remaining' => $remainingpackage]);
                                    $trans_user->remaining_shift = 0;
                                }else{
                                    DB::table('package_info')
                                        ->where('id', $package->id)
                                        ->update(['remaining' => 0]);
                                    $package_shift = $trans_user->remaining_shift-$package->remaining;
                                    $trans_user->remaining_shift = $package_shift;
                                }
                           }
                        }
                $trans_user->save();
            }
            if ($user->credits >= $listing_total) {
                $remaining = $user->credits - $listing_total;
                $listing_total_gst = 0;
                $trans->gst_paid = $listing_total_gst;
                $trans->totalnogst = $listing_total;
                $listing_total_with_gst = $listing_total + $listing_total_gst;
                $listing_total = $listing_total_with_gst;
                $listing_total = round($listing_total, 2);
                ////echo 'hi';
                if ($remaining < 0) {
                    $remaining = 0;
                }
                $trans->creditsremaing = $remaining;
                Users::where('id', $auth_user->id)->update(['credits' => $remaining]);
            } else {
                if ($package_cost > 0) {
                    $listing_total_gst = $listing_total * 10 / 100;
                    $trans->gst_paid = $listing_total_gst;
                    $trans->totalnogst = $listing_total;
                    $listing_total_with_gst = $listing_total + $listing_total_gst;
                    $listing_total = $listing_total_with_gst;
                    $listing_total = round($listing_total, 2);
                    $remaining = $credits_to_inserted;
                    //$listing_total = $package_cost_no_bonus;
                    ////echo 'hi1---';
                } else {
                    $listing_total = $listing_total - $user->credits;
                    $partial_credits = $user->credits;
                    $listing_total_gst = $listing_total * 10 / 100;
                    $trans->gst_paid = $listing_total_gst;
                    if ($user->credits > 0 ) {
                       $trans->partial_credits = 1;
                    }
                    $trans->totalnogst = $listing_total;
                    $listing_total_with_gst = $listing_total + $listing_total_gst;
                    $listing_total = $listing_total_with_gst;
                    $listing_total = round($listing_total, 2);
                    $remaining = $user->credits - $listing_total;
                    $credits_less_than = $remaining;
                    $remaining = 0;

                    ////echo 'hi2---';
                }

                ////echo $remaining;
                if ($remaining < 0) {
                    $remaining = 0;
                }
                $trans->creditsremaing = $remaining;
                Users::where('id', $auth_user->id)->update(['credits' => $remaining]);
            }

        }else{
            $listing_total_gst = $listing_total * 10 / 100;
            $trans->gst_paid = $listing_total_gst;
            $trans->totalnogst = $listing_total;
            $listing_total_with_gst = $listing_total + $listing_total_gst;
            $listing_total = $listing_total_with_gst;
            $listing_total = round($listing_total, 2);
        }
        $credits_less_than = false;
        $trans->creditsin = $user->credits;
        ////echo $remaining;
        ////echo $credits_less_than;
        //// die();;
        //var_dump($remaining);
        $settings = Utils::getSettings("general");
        ////echo "HELLO";
        ////echo $listing_total;
        ////echo $listing_total;
        if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
            $credit_fee = $listing_total * $settings->card_fee / 100;
            ////echo $credit_fee;
            $listing_total = $listing_total + $credit_fee;
            $trans->payment = "Credit Card";
            $trans->creditfee .= $credit_fee;

        } else if (Input::get('paymentopt') == 'eft') {
            $trans->payment = "Invoice";
        }
        ////echo $listing_total;
        $listing_total = round($listing_total, 2);
        //var_dump($listing_total);
        $summ = $listing_total;
        $trans->total = $summ;
        $trans->save();
        $user = Users::where('id', $auth_user->id)->first();
        $fixed_term = \App\Settings::where('column_key','invoice_terms')->first();
        if(Auth::user()->invoice_terms){
            if ($package_id > 0) {
                DB::table('package_info')->insert(
                    ['user_id' => Auth::user()->id, 'price' => $trans->total, 'creditfee' => $trans->creditfee, 'gstpaid' => $trans->gst_paid,'name' => $trans->package,'purchase_date' => Carbon::now(),'expiry_date' => Carbon::now()->addMonth(6),'payment' => $trans->payment,'invoice_terms' => Auth::user()->invoice_terms,'remaining' => $trans->creditsremaing,'creditsgiven' => $package->price + $package->price * $package->discount_percent / 100]
                );
            }
        }else{
           if ($package_id > 0) {
                DB::table('package_info')->insert(
                    ['user_id' => Auth::user()->id, 'price' => $trans->total, 'creditfee' => $trans->creditfee, 'gstpaid' => $trans->gst_paid,'name' => $trans->package,'purchase_date' => Carbon::now(),'expiry_date' => Carbon::now()->addMonth(6),'payment' => $trans->payment,'invoice_terms' => $fixed_term->value_string,'remaining' => $trans->creditsremaing,'creditsgiven' => $package->price + $package->price * $package->discount_percent / 100]
                );
            } 
        }
/*        $usecredits = Input::get('uuuucMNNMNmmd');
        $incoming_credits = $user->credits;

        if ($creditcalc != 0) {
            $creditcalc = $creditcalc - $summ;
            Users::where('id', $auth_user->id)->update(['credits' => $creditcalc] );
        }

        if ($usecredits == "yes") {
            $credits = $creditcalc;
            $credits = $credits - $summ;
            if ($credits < 0) {
                $credits = 0;
            }
            $user->credits = $credits;
        }

        if ($incoming_credits > 0 ) {
            $credits = $credits - $summ;
            $user->credits = $credits;
            //Users::where('id', $auth_user->id)->update(['credits' => $credits] );
        }*/

        $user->save();
        if (Input::get('adssdar32rfdsfdsf') == "R" && Input::get('paymentopt') == 'creditc') { //R == Ok
            $token     = Input::get('stripeToken');
            $trans->gst_paid = number_format($trans->gst_paid, 2, '.', '');
            $stripe_gst = ", GST(10%): $" . $trans->gst_paid;
            $trans->creditfee = number_format($trans->creditfee, 2, '.', '');
            $stripe_creditfee = ", Credit Card Fee: $" . $trans->creditfee;

            if ($package_cost == 0) {
                if ($trans->coupon_used !== null) {
                $stripe_coupon_disc = $trans->coupon_disc;
                    if ($stripe_coupon_disc > 0) {
                      $trans->coupon_disc = number_format($trans->coupon_disc, 2, '.', '');
                      $stripe_coupon = ", Coupon Code (" . $trans->coupon_used . ") Discount: -$" . number_format($stripe_coupon_disc, 2, '.', '');
                    }else{
                    $stripe_coupon = null;
                    }
                } else {
                    $stripe_coupon = null;
                }
                if ($trans->ref_used !== null) {
                    $stripe_ref_disc = $trans->refdiscup;
                    if ($stripe_ref_disc > 0) {
                       $stripe_ref = ", Registration Code (" . $trans->ref_used . ") Discount: -$" . number_format($stripe_ref_disc, 2, '.', '');
                    } else{
                       $stripe_ref = null;
                    }
                } else {
                    $stripe_ref = null;
                }
                $upgrade_explode = explode("<br><br>", $trans->upgrades);
                foreach ($upgrade_explode as $key => $value) {
                    if ($value == "") {
                        unset($upgrade_explode[$key]);
                    }
                }
                if ($trans->partial_credits !== null) {
                    $stripe_credits = $partial_credits;
                    if ($stripe_credits > 0) {
                       $stripe_credits = ", Credits Used: -$" . number_format($stripe_credits, 2, '.', '');
                    } else{
                       $stripe_credits = null;
                    }
                } else {
                    $stripe_credits = null;
                }
                $upgrade_strip = implode(", ", $upgrade_explode);
                $stripe_reference = 'Reference Number: (DevInvoice@ItsMyCall-Job-' .$post_item->id. '-' .$trans->id.')';
                $stripe_description = $stripe_reference . ", " . $trans->listing_type . ", " . $upgrade_strip . ", " . $trans->questions . $stripe_gst . $stripe_creditfee . $stripe_credits . $stripe_ref . $stripe_coupon . " For a detailed list of your transactions please refer to your 'My Transactions' link in your account. You will receive a copy of your tax invoice within 24 hours.";
            } else {
                if ($trans->coupon_used !== null) {
                $stripe_coupon_disc = $trans->coupon_disc;
                    if ($stripe_coupon_disc > 0) {
                      $trans->coupon_disc = number_format($trans->coupon_disc, 2, '.', '');
                      $stripe_coupon = ", Coupon Code (" . $trans->coupon_used . ") Discount: -$" . number_format($stripe_coupon_disc, 2, '.', '');
                    }else{
                    $stripe_coupon = null;
                    }
                } else {
                    $stripe_coupon = null;
                }
                if ($trans->ref_used !== null) {
                    $stripe_ref_disc = $trans->refdisc;
                    if ($stripe_ref_disc > 0) {
                       $stripe_ref = ", Registration Code (" . $trans->ref_used . ") Discount: -$" . number_format($stripe_ref_disc, 2, '.', '');
                    } else{
                       $stripe_ref = null;
                    }
                } else {
                    $stripe_ref = null;
                }
                $invoicenum_package_info = DB::table('package_info')->where('user_id',$auth_user->id)->orderBy('id','DESC')->first();
                if($invoicenum_package_info){
                    $stripe_reference = 'Reference Number: (DevInvoice@ItsMyCall-ValuePack-' .$invoicenum_package_info->id. '-' .$trans->id.')';
                }else{
                    $stripe_reference = null;
                }
                $stripe_description = $stripe_reference . ", " . $trans->package . $stripe_gst . $stripe_creditfee . $stripe_ref . $stripe_coupon . ". For a detailed list of your transactions please refer to your 'My Transactions' link in your account.";
            }      
            $payStatus = $auth_user->charge(
                round($summ * 100),
                [ // TODO: 100 replace with total amount paid
                    'source'        => $token,
                    'currency'      => 'aud',
                    'description'   =>  $stripe_description,
                    'receipt_email' => Input::get('billing_email'),
                    'expand' => array('balance_transaction'),
                     // TODO: Input::get('billing_email')
                ]
            );
        }
        if (Input::get('adssdar32rfdsfdsf') == "R") {

            $xero = app('XeroPrivate');
            $invoice = app('XeroInvoice');
            $contact = app('XeroContact');
            $payment = new \XeroPHP\Models\Accounting\Payment();
            $address = app('\XeroPHP\Models\Accounting\Address');
            $phone = app('\XeroPHP\Models\Accounting\Phone');
            $lines;
            // Set up the invoice contact
            $contact->setAccountNumber('DevInvoice@ItsMyCall-User-'.Auth::user()->id);
            $contact->setContactStatus('ACTIVE');
            $contact->setName(Input::get('billing_company'));
            $contact->setFirstName($billing_name);
            $contact->setLastName(Input::get('billing_last_name'));
            $contact->setEmailAddress($billing_email);
            $contact->setDefaultCurrency('AUD');
            $contact->setTaxNumber(Input::get('abn'));
            //$contact->addPhone($billing_phone);
            $route = Input::get('route');
            $street = Input::get('street_number');
            $city = Input::get('locality');
            $state = Input::get('administrative_area_level_1');
            $pcode = Input::get('postal_code');
            $addressline1 = $street . " " . $route;
            $address->setAddressLine1($addressline1);
            $address->setCity($city);
            $address->setRegion($state);
            $address->setPostalCode($pcode);
            $address->setCountry('Australia');
            $address->setAddressType('POBOX');
            $contact->addAddress($address);

            $phone->setPhoneNumber(Input::get('billing_phone'));
            $phone->setPhoneType('DEFAULT');
            //dd($phone);
            $contact->addPhone($phone);
            // Assign the contact to the invoice
            $invoice->setContact($contact);

            $invoice->setType('ACCREC');

            $fixed_term = \App\Settings::where('column_key','invoice_terms')->first();
            if (Auth::user()->invoice_terms) {
               $dateInstance = Carbon::now()->addDays(Auth::user()->invoice_terms);
            }elseif($fixed_term && $fixed_term->value_string){
               $dateInstance = Carbon::now()->addDays($fixed_term->value_string);
            }else{
               $dateInstance = Carbon::now()->addDays(7); 
            }
            $invoice->setDate(Carbon::now());
            if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
                $theme = XeroPrivate::loadByGUID('Accounting\\BrandingTheme', 'CreditCardPayment');
                $invoice->setBrandingThemeID($theme->BrandingThemeID);
                $invoice->setDueDate(Carbon::now());
            }else{
                $theme = XeroPrivate::loadByGUID('Accounting\\BrandingTheme', 'InvoicePurchase');
                $invoice->setBrandingThemeID($theme->BrandingThemeID);
                $invoice->setDueDate($dateInstance);
            }
            if ($package_id > 0) {
               $invoicenum_package_info = DB::table('package_info')->where('user_id',$auth_user->id)->orderBy('id','DESC')->first();
                $invoice->setInvoiceNumber('DevInvoice@ItsMyCall-ValuePack-' .$invoicenum_package_info->id. '-' .$trans->id);
                if($trans->payment == 'Invoice'){
                    $fixed_term = \App\Settings::where('column_key','invoice_terms')->first();
                    if(Auth::user()->invoice_terms){
                        $this->adminEmailInvoice($invoicenum_package_info,Auth::user()->invoice_terms);
                    }elseif($fixed_term && $fixed_term->value_string){
                        $this->adminEmailInvoice($invoicenum_package_info,$fixed_term->value_string);
                    }else{
                        $this->adminEmailInvoice($invoicenum_package_info,7);
                    }
                }
            }else{
                $invoice->setInvoiceNumber('DevInvoice@ItsMyCall-Job-' .$post_item->id. '-' .$trans->id);
            }

            if ($billing_ref) {
                $invoice->setReference($billing_ref);
            } else {
                $now = Carbon::now();
                $date_invoice = \Carbon\Carbon::parse($now)->format('d M Y');
                $invoice->setReference('Job-' .$post_item->id. ' listed on ' . $date_invoice);
            }

            $invoice->setCurrencyCode('AUD');
            $invoice->setStatus('AUTHORISED');
            $nuline = app("XeroInvoiceLine");
            //Add the post to the invoice
            if ($package_cost > 0) {
                $nuline1 = app("XeroInvoiceLine");
                $package = Packages::where('id', Input::get('packages_stats1'))->first();
                $nuline1->setDescription($package->package_name);
                $nuline1->setQuantity(1);
                if($disc1 > 0){
                    $packagediscount_ref = $package->price-($package->price*$disc1/100);
                    $nuline1->setUnitAmount($packagediscount_ref);
                }else{
                    $nuline1->setUnitAmount($package->price);
                }
                if($packages_discount_rate > 0){
                    $nuline1->setDiscountRate($packages_discount_rate);
                }else{
                    $nuline1->setDiscountRate(0);
                }
                $nuline1->setTaxType('OUTPUT');
                $package_detail = Input::get('packages_stats1');
                if ($package_detail == '1') {
                    $nuline1->setAccountCode('230');
                }
                if ($package_detail == '2') {
                    $nuline1->setAccountCode('233');
                }
                if ($package_detail == '3') {
                    $nuline1->setAccountCode('236');
                }
                if ($package_detail == '4') {
                    $nuline1->setAccountCode('239');
                }
                $invoice->addLineItem($nuline1);
            }
            if ($trans->partial_credits !== null) {
                    $invoice_credits = $partial_credits;
                    if ($invoice_credits > 0) {
                       $invoice_credits = "Credits Used: -$" . number_format($invoice_credits, 2, '.', '');
                    } else{
                       $invoice_credits = null;
                    }
            } else {
                    $invoice_credits = null;
            }
            if ($package_cost == 0) {
                if (Auth::user()->ref_id) {
                    $date = Carbon::now()->toDateString();
                    $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                    if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                        $listing_type = DB::table('referralpackages')->where('referral_id',Auth::user()->ref_id)->where('id', $post_item->posts_packages)->first();
                        $type = $listing_type;
                    }
                    else{
                       $listing_type = PostsPackages::where('id', $post_item->posts_packages)->first();
                       $type = $listing_type;
                    }
                }else{
                    $listing_type = PostsPackages::where('id', $post_item->posts_packages)->first();
                    $type = $listing_type;
                }
                $nuline->setDescription($type->title);
                $nuline->setQuantity(1);
                if($job_discount_rate > 0){
                    $nuline->setDiscountRate($job_discount_rate);
                }else{
                    $nuline->setDiscountRate(0);
                }
                $nuline->setUnitAmount($type->price);
                if ($invoice_credits != null) {
                    $nuline->setTaxType('BASEXCLUDED');
                }else{
                    $nuline->setTaxType('OUTPUT');
                }
                $listing_detail = Input::get('posts_packages');
                if ($listing_detail == '1') {
                    $nuline->setAccountCode('210');
                }
                if ($listing_detail == '2') {
                    $nuline->setAccountCode('211');
                }
                if ($listing_detail == '3') {
                    $nuline->setAccountCode('212');
                }
                $invoice->addLineItem($nuline);

                $upgrades = json_decode(Input::get('posts_upgrades'));
                if (is_array($upgrades)) {
                    foreach ($upgrades as $key => $value) {
                        if ($key == 0 OR $value == 0)
                            continue;
                        $nuline2 = app("XeroInvoiceLine");
                        //$invoice->addLineItem($nuline2);
                        if (Auth::user()->ref_id) {
                            $date = Carbon::now()->toDateString();
                            $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                            if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                                $upgrade = DB::table('referralupgrades')->where('referral_id',Auth::user()->ref_id)->where('id', $key)->first();
                            }
                            else{
                               $upgrade = Upgrades::where('id', $key)->first();
                            }
                        }else{
                            $upgrade = Upgrades::where('id', $key)->first();
                        }

                        //var_dump($upgrade);
                        $nuline2->setDescription($upgrade->name);
                        $nuline2->setQuantity($value);
                        if ($boost_discount_rate != 0) {
                            if ($coupon_exc == 'yes') {
                                $nuline2->setUnitAmount($upgrade->actual_price);
                            }
                            elseif(empty(Auth::user()->ref_id)){
                                $nuline2->setUnitAmount($upgrade->actual_price);
                            }
                            elseif (count($referralcode) > 0 && $referralcode->date_end < $date) {
                                $nuline2->setUnitAmount($upgrade->actual_price);
                            }
                            else{
                                 $nuline2->setUnitAmount($upgrade->price);
                            }
                        }
                        else{
                             $nuline2->setUnitAmount($upgrade->price);
                        }
                        if($boost_discount_rate > 0){
                            $nuline2->setDiscountRate($boost_discount_rate);
                        }else{
                            $nuline2->setDiscountRate(0);
                        }
                        if ($invoice_credits != null) {
                            $nuline2->setTaxType('BASEXCLUDED');
                        }else{
                            $nuline2->setTaxType('OUTPUT');
                        }
                        if ($key == '1') {
                            $nuline2->setAccountCode('252');
                        }
                        if ($key == '2') {
                            $nuline2->setAccountCode('256');
                        }
                        if ($key == '4') {
                            $nuline2->setAccountCode('254');
                        }
                        $invoice->addLineItem($nuline2);
                    }
                }

                if ($trans->questions) {
                        $nuline3 = app("XeroInvoiceLine");
                        //$invoice->addLineItem($nuline2);
                        if (Auth::user()->ref_id) {
                            $date = Carbon::now()->toDateString();
                            $referralcode = ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                            if (count($referralcode) > 0 && $referralcode->date_end >= $date) {
                                $question_inv = DB::table('referral_questions')->where('referral_id',Auth::user()->ref_id)->first();
                            }
                            else{
                               $question_inv = DB::table('questionspricing')->first();
                            }
                        }else{
                            $question_inv = DB::table('questionspricing')->first();
                        }

                        //var_dump($upgrade);
                        $nuline3->setDescription($question_inv->title);
                        $nuline3->setQuantity(1);
                        if ($question_discount_rate != 0) {
                            if ($coupon_exc == 'yes') {
                                $nuline3->setUnitAmount($question_inv->price);
                            }
                            elseif(empty(Auth::user()->ref_id)){
                                $nuline3->setUnitAmount($question_inv->price);
                            }
                            elseif (count($referralcode) > 0 && $referralcode->date_end < $date) {
                                $nuline3->setUnitAmount($question_inv->price);
                            }
                            else{
                                 $nuline3->setUnitAmount($question_inv->price);
                            }
                        }
                        else{
                             $nuline3->setUnitAmount($question_inv->price);
                        }
                        if($question_discount_rate > 0){
                            $nuline3->setDiscountRate($question_discount_rate);
                        }else{
                            $nuline3->setDiscountRate(0);
                        }
                        if ($invoice_credits != null) {
                            $nuline3->setTaxType('BASEXCLUDED');
                        }else{
                            $nuline3->setTaxType('OUTPUT');
                        }
                        $nuline3->setAccountCode('240');
                        $invoice->addLineItem($nuline3);
                }
            }
            if ($invoice_credits != null) {
                $credits_go = -$partial_credits;
                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription($invoice_credits);
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount($credits_go);
                $nuline2->setTaxType('BASEXCLUDED');
                $nuline2->setAccountCode('242');
                $invoice->addLineItem($nuline2);

                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription("GST");
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount($trans->gst_paid);
                $nuline2->setAccountCode('820');
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }
            if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription("Credit Card Fee");
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount($credit_fee);
                $nuline2->setAccountCode('404');
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }

            if (Input::get('paymentopt') != 'creditc') {
                $nuline2 = app("XeroInvoiceLine");
                //$invoice->addLineItem($nuline3);
                $nuline2->setDescription("Invoice Method");
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount(0);
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }

            if ($package_cost > 0) {
                if ($packages_discount_rate > 0) {
                    $nuline2 = app("XeroInvoiceLine");
                    $nuline2->setDescription("The Coupon Code '" . $coupon_used->coupon_code ."' discount (displayed in the Disc % column) has been applied and is deducted from the Unit Price column.");
                    $nuline2->setQuantity(1);
                    $nuline2->setUnitAmount(0);
                    $nuline2->setTaxType('BASEXCLUDED');
                    $invoice->addLineItem($nuline2);
                }
            }else{
                if ($job_discount_rate > 0 || $boost_discount_rate > 0 || $question_discount_rate > 0) {
                    $nuline2 = app("XeroInvoiceLine");
                    $nuline2->setDescription("The Coupon Code '" . $coupon_used->coupon_code ."' discount (displayed in the Disc % column) has been applied and is deducted from the Unit Price column.");
                    $nuline2->setQuantity(1);
                    $nuline2->setUnitAmount(0);
                    $nuline2->setTaxType('BASEXCLUDED');
                    $invoice->addLineItem($nuline2);
                }
            }

            if ($package_cost == 0) {
                    if ($trans->ref_used !== null) {
                            $invoice_ref_disc = $trans->refdiscup;
                            if ($invoice_ref_disc > 0) {
                               $invoice_ref = "Please note your registration Code '" . $trans->ref_used . "' discount(s) have been automatically applied and are displayed in the Unit Price column. For this purchase your registration code has saved you an additional $" . number_format($invoice_ref_disc, 2, '.', '') . " off our standard prices!";
                            } else{
                               $invoice_ref = null;
                            }
                    } else {
                            $invoice_ref = null;
                    }
            }else{
                if ($trans->ref_used !== null) {
                    $invoice_ref_disc = $trans->refdisc;
                    if ($invoice_ref_disc > 0) {
                       $invoice_ref = "Please note your registration Code '" . $trans->ref_used . "' discount(s) have been automatically applied and are displayed in the Unit Price column. For this purchase your registration code has saved you an additional $" . number_format($invoice_ref_disc, 2, '.', '') . " off our standard prices!";
                    } else{
                       $invoice_ref = null;
                    }
                } else {
                    $invoice_ref = null;
                }
            }
            if ($invoice_ref) {
                $nuline2 = app("XeroInvoiceLine");
                $nuline2->setDescription($invoice_ref);
                $nuline2->setQuantity(1);
                $nuline2->setUnitAmount(0);
                $nuline2->setTaxType('BASEXCLUDED');
                $invoice->addLineItem($nuline2);
            }
//            if($credits_less_than) {
//                $nuline2 = app("XeroInvoiceLine");
//                $invoice->addLineItem($nuline2);
//                $nuline2->setDescription("Credit Card Fee");
//                $nuline2->setQuantity(1);
//                $nuline2->setUnitAmount($credits_less_than);
//                $nuline1->setDiscountRate(100);
//                $invoice->addLineItem($nuline2);
//            }



            //$invoice->setTotalDiscount('50');
            //var_dump($invoice);

            $xero->save($invoice);
            if (Input::get('paymentopt') == 'creditc' && Input::get('adssdar32rfdsfdsf') == "R") {
                $invoiceinfo = XeroPrivate::loadByGUID('Accounting\\Invoice', $invoice->InvoiceID);
                $contactinfo = XeroPrivate::loadByGUID('Accounting\\Account', 'EFTPOS');
                $payment->setInvoice($invoiceinfo);
                $payment->setAccount($contactinfo);
                $payment->setDate(Carbon::now());
                $payment->setIsReconciled(0);
                $payment->setAmount($summ);
                $xero->save($payment);
            }
        }


        //$this->sendEmail($post_item);
        //// die();;
        Session::flash('success_msg', 'Your new Job Advertisement has been published');
        $this->adminEmail($post_item);
        // return redirect()->back();
        return ["status" => "200"];
        //return redirect()->to('/poster');
    }

    private function sendEmail(Posts $post,$paycycle)
    {   
        //ToDo pg_select(connection, table_name, assoc_array) by wage min-max
        $subscriptions = DB::table('subscriptions')
                           ->where('category_id', $post->category)
                           ->where('active', 1)
                           ->get();
        foreach ($subscriptions as $subscription) {
            $user = DB::table('users')
                      ->where('id', $subscription->user_id)
                      ->where('deleted', 0)
                      ->first();

            if (!$user) {
                continue;
            }

            if ($post->lng != '' && $subscription->lng != '') {
                $distance = $this->getDistance($post->lat, $post->lng, $subscription->lat, $subscription->lng);
               // dd($distance);
            } else {
                $distance = 10000;
            }
//            //echo $distance .  " <br>";
//            //echo $subscription->radius;
            $salary_condition = false;
            $category_condition = false;
            $pay_cycle_condition = false;
            $emp_type_condition = false;
            $emp_term_condition = false;
            $parentsoption_go = false;
            $work_from_home_go = false;
            $incentivestructure_condition = false;
            $distance_condition = false;
            $advertiser_type_condition = false;

            if($subscription->hourly_end == "100+") {
                $subscription->hourly_end = "9999";
            }

            if ($subscription->salary_type == "yes") {
                if ($post->hrsalary >= $subscription->hourly_start AND $post->hrsalary <= $subscription->hourly_end) {
                    $salary_condition = true;
                }
            } else {
                if ($subscription->wage_min > 0 AND $subscription->wage_max > 0) {
                    if ($post->salary >= $subscription->wage_min AND $post->salary <= $subscription->wage_max) {
                        $salary_condition = true;
                    }
                } elseif ($subscription->wage_min == 0 AND $subscription->wage_max == 0) {
                    $salary_condition = true;
                }
            }
            
            if (empty($subscription->sub_category_id)) {
                $category_condition = true;
            } elseif (in_array($post->subcategory, json_decode($subscription->sub_category_id, true))) {
                $category_condition = true;
            }

            if ($subscription->pay_cycle) {
                $jsonString3 = $subscription->pay_cycle;
                $keys3 = json_decode($jsonString3);
                if (count($keys3) == 3) {
                   $pay_cycle_condition = true;
                }else{
                  foreach ($keys3 as $key) {
                   if ($key == $paycycle) {
                        $pay_cycle_condition = true;
                    }
                  }
                }
            }
            
            //dd($subscription->employment_type);
            //dd($post->employment_type);

            if ($subscription->employment_type == "") {
                $emp_type_condition = true;
            } elseif ($subscription->employment_type == $post->employment_type) {
                $emp_type_condition = true;
            }

            if (empty($subscription->employment_term)) {
                $emp_term_condition = true;
            } elseif (in_array($post->employment_term, json_decode($subscription->employment_term, true))) {
                $emp_term_condition = true;
            }

            if (empty($subscription->incentivestructure)) {
                $incentivestructure_condition = true;
            } elseif (in_array($post->incentivestructure, json_decode($subscription->incentivestructure, true))) {
                $incentivestructure_condition = true;
            }

            if ($subscription->radius > $distance) {
                $distance_condition = true;
            }

            if (empty($subscription->advertiser_type)) {
                $advertiser_type_condition = true;
            } elseif ($subscription->advertiser_type === $post->getAdvertiserType()) {
                $advertiser_type_condition = true;
            }

            if ($subscription->parentsoption) {
                $jsonString2 = $subscription->parentsoption;
                $keys2 = json_decode($jsonString2);
                foreach ($keys2 as $key) {
                   if ($key == $post->parentsoption) {
                        $parentsoption_go = true;
                    }
                }
            }

            if ($subscription->work_from_home) {
                $jsonString = $subscription->work_from_home;
                $keys = json_decode($jsonString);
                foreach ($keys as $key) {
                  if($key == $post->work_from_home){
                    $work_from_home_go = true;
                  }
                }
            }
            if ($salary_condition == true && $category_condition == true && $pay_cycle_condition == true && $emp_type_condition == true && $emp_term_condition == true && $incentivestructure_condition == true && $distance_condition == true && $work_from_home_go == true && $parentsoption_go == true && $advertiser_type_condition == true) {
                
                $email = $user->email;
                $subject = 'New Job Alert on ItsMyCall';
                \Mail::send(
                    'emails.post',
                    array(
                        'name' => $user->name,
                        'email' => $user->email,
                        'subject' => $post->title,
                        'description' => $post->description,
                        'company' => $post->company,
                        'slug' => $post->slug,
                    ),
                    function ($message) use ($email, $subject) {
                        $message->to($email)->subject($subject);
                    }
                );
            }
        }

        return true;
    }

    public function postTweet(Posts $post)
    {   
        $state = $post->state;

        if($state == 'Australian Capital Territory'){
            $consumer_key = "lWcIVw7C6snMhczkfrlQzTxfV";
            $consumer_secret = "HxaCPr91B8FBbpMgrFeKQT1OCOX8bXEKk8ZOvKfwTCfETwkPe2";
            $access_token = "947781146439442432-6hBAXsU4ecbz2XAqm2yjiUaX0O9crMf";
            $access_token_secret = "YKbCovjRJp9esqEBnzXOdYzW8Iy36c5OCLJtgaW3DSapd";
        }else if($state == 'New South Wales'){
            $consumer_key = "arxmI3dMIjuX3JNSJP6waYhyd";
            $consumer_secret = "pejkEJc9FOGNwsuujwzSGSsAn2HxO8D18IFXkKq0fccqgA3I81";
            $access_token = "873372329497116672-lXuw5QpZqrZIfdEzNikp763EHxr3uEu";
            $access_token_secret = "Hz9tgdSlL5ZxcmnZCJytLT3aKXr94VG6at1MNvhAGv9o4";
        }else if($state == 'Northern Territory'){
            $consumer_key = "6sGvMkFVBYyR9zEoau4XE4cLM";
            $consumer_secret = "apGYEs5HBRwXkXJBKjTYhJ8fmnTTgXAFLGg7epvbIDx7bkCQW7";
            $access_token = "947779115473440768-5J0qIL2he7U3Tbtov7SoqhzGLCse1Qp";
            $access_token_secret = "AVlJ8U0lkU5rvTYGRBsB2mVRzc4yIPnIRE17WBdmAS9ns";
        }else if($state == 'Queensland'){
            $consumer_key = "DQ0fOZ52fJmGxrzCZFF1kB4LL";
            $consumer_secret = "1bb2aTUxANcZLYuHNcxKlBS9RwEDSslG441Xvb4BujkU3TKZYj";
            $access_token = "873373079019200512-FFYPCLrHPO0AOz7gYE6M4W9TK98hJ89";
            $access_token_secret = "hFOGBz9XjhPkD92qLpewVHbVgZPtSrmfC79DMmX056aTG";
        }else if($state == 'South Australia'){
            $consumer_key = "Iwnb3R1Q9TehQCTSvhAi31Kva";
            $consumer_secret = "uwNCPlckMi5shoD4a6MVqpodlqWyhcIbbwwkFSiIT7gOl8f6g3";
            $access_token = "873373649868210176-FqMng6i9K67WkuoscRymLkHV2Dpvkv8";
            $access_token_secret = "g58hr9rKPVXUqNppZi9Hqf2JIrlQp2oT0GsI7JmCncPid";
        }else if($state == 'Tasmania'){
            $consumer_key = "R7gD71ya1CdAIQT0xJwkKhol3";
            $consumer_secret = "4woNFg3MJv9Ux6xLnvPH5VxBExKTL3Nf6hGYfEmMO87z9p34pD";
            $access_token = "873374734741786625-ejajGu2HbzJQsHQAT88VORajjbBJRmn";
            $access_token_secret = "BdC8pjgluEWFFTl9WWBqpH8VQ40mKwxmTsx23R3wUNSLD";
        }else if($state == 'Victoria'){
            $consumer_key = "aBmpxXMI0Wu11tiIRKHplyoOZ";
            $consumer_secret = "3rY988cFIIpbLj5yyXcC5kFE30FusE3XdJg2s8ND6KjQ6TgGlr";
            $access_token = "873370969380864000-DarxR7GTHXqIQ2WswmKCjhS5s3cA4Kd";
            $access_token_secret = "eUC1iJmr7FYfv004n0pr1Fy6sw7bGHmBOI5XpXpIpAML1";
        }else if($state == 'Western Australia'){
            $consumer_key = "GeXpeSg3F9rXFEVOBudyQ3Zqr";
            $consumer_secret = "1R0QgGAFBwHvLMvI58X8G2shyf8DMo44364yVLuqGGXobqVXSh";
            $access_token = "873374290116136960-kqosk6EOW5upa67aHvJDGSjddj9I9g0";
            $access_token_secret = "4rjgy7JisUOojUAuP3nvoouvLgRrm9nmqfTrKGTrzuTM6";
        }

        // set the new configurations
        Twitter::reconfig([
            "consumer_key" => $consumer_key,
            "consumer_secret"  => $consumer_secret,
            "token" => $access_token,
            "secret" => $access_token_secret,
        ]);
        
        $tweet_text = \App\Settings::where('column_key','tweet_text')->first();

        if (!empty($tweet_text)) {
          $tweet = $tweet_text->value_txt;
        }else{
          $tweet = "New Job is posted on ItsMyCall";
        }

        $url = "http://dev.itsmycall.com.au/".$post->slug;
        $status = substr($tweet, 0, 119).' '.$url;

        Twitter::postTweet(['status' => $status]);
    }

    public function adminEmail($post) {
        $email = env('NEW_JOB_EMAIL');

        \Mail::send(
            'emails.admin_email',
            array(
                'email' => $email,
                'subject' => $post->title,
                'description' => $post->description,
                'company' => $post->company,
                'job_level' => $post->package()->getResults()->name,
                'upgrades' => $post->upgrades()->getResults(),
                'slug' => $post->slug,
            ),
            function ($message) use ($email, $post) {
                $message->to($email)->subject('New Job Posted - ' . $post->title);
            }
        );
    }

    public function adminEmailInvoice($packageinfo,$invoiceterm) {
        $email = env('NEW_JOB_EMAIL');

        \Mail::send(
            'emails.admin_email_invoice',
            array(
                'email' => $email,
                'subject' => 'New Invoice needs Actioning',
                'packageinfo' => $packageinfo,
                'invoiceterm' => $invoiceterm,
            ),
            function ($message) use ($email, $packageinfo, $invoiceterm) {
                $message->to($email)->subject('New Invoice needs Actioning');
            }
        );
    }

    public function applicantmail(Request $request) {
        if (!empty($request->descriptionedit)) {
            $descriptionedit = $request->descriptionedit;
        }
        else{
            $descriptionedit = $request->feedback;
        }
        $arr = explode(' ',trim($request->applicantname));
        $name = $arr[0];
        $post = Posts::find($request->post);
        $category = DB::table('emailcategories')
                 ->where('id','=',$request->category)
                 ->get();
        foreach ($category as $key) {
                   $details = $key->description;
                   $detailsname = $key->name;
                }
        $email = $request->applicantemail;
        \Mail::send(
            'emails.applicantmailsend',
            array(
                'email' => $email,
                'name' => $name,
                'subject' =>'Update on your application for ' . $post->title,
                'description' => $details,
                'feedback' => $descriptionedit,
                'company' => $post->company,
                'slug' => $post->slug,
                'detail' => $detailsname,
            ),
            function ($message) use ($email, $post) {
                $message->to($email)->subject('Update on your application for ' . $post->title);
            }
        );
        $pastmail = new PastEmail;
        $pastmail->email = $request->applicantemail;
        $pastmail->applicant_id = $request->applicant;
        $pastmail->post_id = $request->post;
        $pastmail->author_id = $post->author_id;
        $pastmail->category_id = $request->category;
        $pastmail->feedback = $descriptionedit;
        $pastmail->save();
        Session::flash('success_msg', 'Email sent to '. $request->applicantemail);
        return redirect("/poster/applicants");
    }
    public function applicantmailjob(Request $request) {
        if (!empty($request->descriptionedit)) {
            $descriptionedit = $request->descriptionedit;
        }
        else{
            $descriptionedit = $request->feedback;
        }
        $arr = explode(' ',trim($request->applicantname));
        $name = $arr[0];
        $post = Posts::find($request->post);
        $category = DB::table('emailcategories')
                 ->where('id','=',$request->category)
                 ->get();
        foreach ($category as $key) {
                   $details = $key->description;
                   $detailsname = $key->name;
                }
        $email = $request->applicantemail;
        \Mail::send(
            'emails.applicantmailsend',
            array(
                'email' => $email,
                'name' => $name,
                'subject' =>'Update on your application for ' . $post->title,
                'description' => $details,
                'feedback' => $descriptionedit,
                'company' => $post->company,
                'slug' => $post->slug,
                'detail' => $detailsname,
            ),
            function ($message) use ($email, $post) {
                $message->to($email)->subject('Update on your application for ' . $post->title);
            }
        );
        $pastmail = new PastEmail;
        $pastmail->email = $request->applicantemail;
        $pastmail->applicant_id = $request->applicant;
        $pastmail->post_id = $request->post;
        $pastmail->author_id = $post->author_id;
        $pastmail->category_id = $request->category;
        $pastmail->feedback = $descriptionedit;
        $pastmail->save();
        Session::flash('success_msg', 'Email sent to '. $request->applicantemail);
        return redirect("/poster/applicants/".$request->post);
    }
     public function previewmail(Request $request) {
        $arr = explode(' ',trim($request->applicantname));
        $name = $arr[0];
        $post = Posts::find($request->post);
        $category = DB::table('emailcategories')
                 ->where('id','=',$request->category)
                 ->get();
        foreach ($category as $key) {
                   $details = $key->description;
                   $detailsname = $key->name;
                   $category_id = $key->id;
                };
        $this->data['email'] = $request->applicantemail;
        $this->data['name'] = $name;
        $this->data['subject'] = 'Update on your application for ' . $post->title;
        $this->data['description'] = $details;
        $this->data['feedback'] = nl2br($request->description);
        $this->data['company'] = $post->company;
        $this->data['slug'] = $post->slug;
        $this->data['detail'] = $detailsname;
        $this->data['applicant_id'] = $request->applicant;
        $this->data['post_id'] = $request->post;
        $this->data['author_id'] = $request->author;
        $this->data['category_id'] = $category_id;
        return view('/emails/applicantmail' , $this->data);
    }
     public function previewmailjob(Request $request) {
        $arr = explode(' ',trim($request->applicantname));
        $name = $arr[0];
        $post = Posts::find($request->post);
        $category = DB::table('emailcategories')
                 ->where('id','=',$request->category)
                 ->get();
        foreach ($category as $key) {
                   $details = $key->description;
                   $detailsname = $key->name;
                   $category_id = $key->id;
                };
        $this->data['email'] = $request->applicantemail;
        $this->data['name'] = $name;
        $this->data['subject'] = 'Update on your application for ' . $post->title;
        $this->data['description'] = $details;
        $this->data['feedback'] = nl2br($request->description);
        $this->data['company'] = $post->company;
        $this->data['slug'] = $post->slug;
        $this->data['detail'] = $detailsname;
        $this->data['applicant_id'] = $request->applicant;
        $this->data['post_id'] = $request->post;
        $this->data['author_id'] = $request->author;
        $this->data['category_id'] = $category_id;
        return view('/emails/applicantmailjob' , $this->data);
    }
    public function resendmailpreview($id) {
        $emails = DB::table('past_emails')
                 ->where('id','=',$id)
                 ->get();
        foreach ($emails as $email) {
                   $address = $email->email;
                   $feedback = $email->feedback;
                   $author_id = $email->author_id;
                   $post_id = $email->post_id;
                   $category_id = $email->category_id;
                   $applicant_id = $email->applicant_id;
                }
        $post = Posts::find($post_id);
        $categories = DB::table('emailcategories')
                 ->where('id','=',$category_id)
                 ->get();
        foreach ($categories as $category) {
            $categoryname = $category->name;
            $description = $category->description;
        }
        $applicants = DB::table('applicants')
                 ->where('id','=',$applicant_id)
                 ->get();
        foreach ($applicants as $applicant) {
            $arr = explode(' ',trim($applicant->name));
            $name = $arr[0];
            $applicant = $applicant->id;
        };
        $this->data['email'] = $address;
        $this->data['name'] = $name;
        $this->data['subject'] = 'Update on your application for ' . $post->title;
        $this->data['description'] = $description;
        $this->data['feedback'] = $feedback;
        $this->data['company'] = $post->company;
        $this->data['slug'] = $post->slug;
        $this->data['detail'] = $categoryname;
        $this->data['applicant_id'] = $applicant;
        $this->data['id'] = $id;
        $this->data['post_id'] = $post_id;
        $this->data['author_id'] = $author_id;
        $this->data['category_id'] = $category_id;
        return view('/emails/applicantresendmail',$this->data);
    }
    public function resendmail($id) {
        $emails = DB::table('past_emails')
                 ->where('id','=',$id)
                 ->get();
        foreach ($emails as $email) {
                   $address = $email->email;
                   $feedback = $email->feedback;
                   $author_id = $email->author_id;
                   $post_id = $email->post_id;
                   $category_id = $email->category_id;
                   $applicant_id = $email->applicant_id;
                }
        $post = Posts::find($post_id);
        $categories = DB::table('emailcategories')
                 ->where('id','=',$category_id)
                 ->get();
        foreach ($categories as $category) {
            $categoryname = $category->name;
            $description = $category->description;
        }
        $applicants = DB::table('applicants')
                 ->where('id','=',$applicant_id)
                 ->get();
        foreach ($applicants as $applicant) {
            $arr = explode(' ',trim($applicant->name));
            $name = $arr[0];
        }
        $email = $address;
        \Mail::send(
            'emails.applicantmailsend',
            array(
                'email' => $email,
                'name' => $name,
                'subject' => 'Update on your application for ' . $post->title,
                'description' => $description,
                'feedback' => $feedback,
                'company' => $post->company,
                'slug' => $post->slug,
                'detail' => $categoryname,
            ),
            function ($message) use ($email, $post) {
                $message->to($email)->subject('Update on your application for ' . $post->title);
            }
        );
        $pastmail = new PastEmail;
        $pastmail->email = $address;
        $pastmail->applicant_id = $applicant_id;
        $pastmail->post_id = $post_id;
        $pastmail->author_id = $author_id;
        $pastmail->category_id = $category_id;
        $pastmail->feedback = $feedback;
        $pastmail->save();
        Session::flash('success_msg', 'Email resent to '. $address);
        return redirect("/poster/pastmail/".$applicant_id.'/'.$post_id);
    }
    public function pastmail($id,$post) {
       $pastemails = DB::table('past_emails')
                 ->where('applicant_id',$id)
                 ->where('post_id',$post)
                 ->where('author_id',auth::user()->id)
                 ->orderBy('created_at','desc')
                 ->paginate(10);
        $categories   = DB::table('emailcategories')
                     ->get();
        $applicants   = DB::table('applicants')
                     ->get();
        $posts = Posts::all();
        $this->data['pastemails'] = $pastemails;
        $this->data['categories'] = $categories;
        $this->data['applicants'] = $applicants;
        $this->data['posts'] = $posts;
        $this->data['id'] = $id;
        return view('pastmails', $this->data);
       
    }

    private function getDistance($latitude1, $longitude1, $latitude2, $longitude2)
    {
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin(
                $dLon / 2
            ) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

    public function stripe($id, Users $user)
    {
        $token = Input::get('stripeToken');
        $days = Input::get('days');

        $start = Carbon::now();
        $end = Carbon::now()->addDay($days);

        Posts::where('id', $id)->update(['featured_starts_at' => $start, 'featured_ends_at' => $end]);

        $user->charge(
            (env('PER_DAY_CHARGE')) * $days,
            [
                'source' => $token,
            ]
        );

        return redirect()->back();
    }

    public function checkReferral()
    {
        $code = Input::get('code');

        $check = ReferralCode::where('referral_code', $code)->first();
        if ($check) {
            $result['reason'] = 'Code Matched.';
            $result['status'] = 'ok';
        } else {
            $result['reason'] = 'Code not found';
            $result['status'] = 'err';
        }

        return json_encode($result);
    }

    public function checkCoupon()
    {
        $code = Input::get('code');

        $check = Coupons::active()->where('coupon_code', $code)->first();
        if ($check) {
            $date = date('Y-m-d');
            $date1 = DateTime::createFromFormat('Y-m-d', $check->date_begin);
            $date2 = DateTime::createFromFormat('Y-m-d', $check->date_end);
            $date1 = $date1->format('Y-m-d');
            $date2 = $date2->format('Y-m-d');
            $referralcheck = DB::table('referralcode')->where('id', Auth::user()->ref_id)->first();
            if(count($referralcheck) > 0 && $referralcheck->exclusive == 'partial' && $referralcheck->date_end >= $date){
                if($date >= $date1 && $date <= $date2){
                    if ($check->exclusive == 'partial') {
                        $date = Carbon::now()->toDateString();
                        $referralcode = DB::table('referralcode')->where('referral_code',$check->referral_code)->first();
                        if(count($referralcode) > 0 && Auth::user()->ref_id == $referralcode->id){
                            $jobdiscount='';
                            $boostdiscount='';
                            $packagediscount='';
                            $questiondiscount='';
                            if($check->job_discount_rate){
                                $jobdiscount = $check->job_discount_rate . '% on Job levels';
                            }
                            if($check->boost_discount_rate){
                                $boostdiscount = $check->boost_discount_rate . '% on Boosts';
                            }
                            if($check->question_discount_rate){
                                $questiondiscount = $check->question_discount_rate . '% on Screening Questions';
                            }
                            if($check->packages_discount_rate){
                                $packagediscount = $check->packages_discount_rate . '% on Value Packs';
                            }
                            $result['reason'] = 'Successfully applied coupon discount: '. $jobdiscount .' '. $boostdiscount .' '. $packagediscount .' '. $questiondiscount;
                            $result['date'] = $date;
                            $result['date1'] = $date1;
                            $result['date2'] = $date2;
                            $result['status'] = 'ok';
                            $end = \Carbon\Carbon::now()->toDateString();
                            $expires = new \Carbon\Carbon($check->date_end);
                            $date = $expires->toFormattedDateString();
                            $now = \Carbon\Carbon::now();
                            $difference = ($expires->diff($now)->days);
                            $result['expiry'] = $date;
                            $result['job_discount_rate'] = $check->job_discount_rate;
                            $result['boost_discount_rate'] = $check->boost_discount_rate;
                            $result['question_discount_rate'] = $check->question_discount_rate;
                            $result['packages_discount_rate'] = $check->packages_discount_rate;
                            $result['exclusive'] = 'no';
                            $result['name'] = $check->coupon_code;
                            $result['id'] = $check->id;
                        }else{
                           $result['reason'] = 'Sorry, this Coupon Code cannot be combined with your Registration Code';
                           $result['status'] = 'err'; 
                        }
                }else{
                    $result['reason'] = 'Sorry, this Coupon Code cannot be combined with your Registration Code';
                    $result['status'] = 'err'; 
                }
            }else{
                    $result['reason'] = 'Coupon has expired';
                    $result['date'] = $date;
                    $result['date1'] = $date1;
                    $result['date2'] = $date2;
                    $result['status'] = 'err';
                }
            }else{
                if ($date >= $date1 && $date <= $date2) {
                    if ($check->exclusive == 'partial') {
                        if (!empty(Auth::user()->ref_id)) {
                            $result['reason'] = 'Sorry, this Coupon code cannot be combined with Registration Code.';
                            $result['status'] = 'err';
                        }else{
                            $result['reason'] = 'Sorry, this Coupon Code is not valid for use on this account.';
                            $result['status'] = 'err';
                        }
                    }else{
                        $jobdiscount='';
                        $boostdiscount='';
                        $packagediscount='';
                        $questiondiscount='';
                        if($check->job_discount_rate){
                            $jobdiscount = $check->job_discount_rate . '% on Job levels';
                        }
                        if($check->boost_discount_rate){
                            $boostdiscount = $check->boost_discount_rate . '% on Boosts';
                        }
                        if($check->question_discount_rate){
                            $questiondiscount = $check->question_discount_rate . '% on Screening Questions';
                        }
                        if($check->packages_discount_rate){
                            $packagediscount = $check->packages_discount_rate . '% on Value Packs';
                        }
                        $result['reason'] = 'Successfully applied coupon discount: '. $jobdiscount .' '. $boostdiscount .' '. $packagediscount .' '. $questiondiscount;
                        $result['date'] = $date;
                        $result['date1'] = $date1;
                        $result['date2'] = $date2;
                        $result['status'] = 'ok';
                        $end = \Carbon\Carbon::now()->toDateString();
                        $expires = new \Carbon\Carbon($check->date_end);
                        $date = $expires->toFormattedDateString();
                        $now = \Carbon\Carbon::now();
                        $difference = ($expires->diff($now)->days);
                        $result['expiry'] = $date;
                        $result['job_discount_rate'] = $check->job_discount_rate;
                        $result['boost_discount_rate'] = $check->boost_discount_rate;
                        $result['question_discount_rate'] = $check->question_discount_rate;
                        $result['packages_discount_rate'] = $check->packages_discount_rate;
                        $result['exclusive'] = $check->exclusive;
                        $result['name'] = $check->coupon_code;
                        $result['id'] = $check->id;
                    }
                } else {
                    $result['reason'] = 'Coupon has expired';
                    $result['date'] = $date;
                    $result['date1'] = $date1;
                    $result['date2'] = $date2;
                    $result['status'] = 'err';
                }
            }
        } else {
            $result['reason'] = 'Coupon not valid';
            $result['status'] = 'err';
        }

        return json_encode($result);
    }

    public function checkCouponpac()
    {   
        $code = Input::get('code');
        $check = Coupons::active()->where('coupon_code', $code)->first();
        if ($check) {
            if($check->packages_discount_rate){
                $date = date('Y-m-d');
                $date1 = DateTime::createFromFormat('Y-m-d', $check->date_begin);
                $date2 = DateTime::createFromFormat('Y-m-d', $check->date_end);
                $date1 = $date1->format('Y-m-d');
                $date2 = $date2->format('Y-m-d');
                $referralcheck = DB::table('referralcode')->where('id', Auth::user()->ref_id)->first();
                if(count($referralcheck) > 0 && $referralcheck->exclusive == 'partial' && $referralcheck->date_end >= $date){
                    if($date >= $date1 && $date <= $date2){
                        if ($check->exclusive == 'partial') {
                            $date = Carbon::now()->toDateString();
                            $referralcode = DB::table('referralcode')->where('referral_code',$check->referral_code)->first();
                            if(count($referralcode) > 0 && Auth::user()->ref_id == $referralcode->id){
                                $packagediscount='';
                            if($check->packages_discount_rate){
                                $packagediscount = $check->packages_discount_rate . '% on Value Packs.';
                            }
                            $result['reason'] = 'Successfully applied coupon discount: '. $packagediscount;
                            $result['date'] = $date;
                            $result['date1'] = $date1;
                            $result['date2'] = $date2;
                            $result['status'] = 'ok';
                            $end = \Carbon\Carbon::now()->toDateString();
                            $expires = new \Carbon\Carbon($check->date_end);
                            $date = $expires->toFormattedDateString();
                            $now = \Carbon\Carbon::now();
                            $difference = ($expires->diff($now)->days);
                            $result['expiry'] = $date;
                            $result['rate'] = $check->packages_discount_rate;
                            $result['exclusive'] = 'no';
                            $result['name'] = $check->coupon_code;
                            $result['id'] = $check->id;
                            }else{
                               $result['reason'] = 'Sorry, this Coupon Code cannot be combined with your Registration Code';
                               $result['status'] = 'err'; 
                            }
                    }else{
                        $result['reason'] = 'Sorry, this Coupon Code cannot be combined with your Registration Code';
                        $result['status'] = 'err'; 
                    }
                }else{
                        $result['reason'] = 'Coupon has expired';
                        $result['date'] = $date;
                        $result['date1'] = $date1;
                        $result['date2'] = $date2;
                        $result['status'] = 'err';
                    }
                }else{
                    if ($date >= $date1 && $date <= $date2) {
                        if ($check->exclusive == 'partial') {
                            if (!empty(Auth::user()->ref_id)) {
                                $result['reason'] = 'Sorry, this Coupon code cannot be combined with Registration Code.';
                                $result['status'] = 'err';
                            }else{
                                $result['reason'] = 'Sorry, this Coupon Code is not valid for use on this account.';
                                $result['status'] = 'err';
                            }
                        }else{
                            $packagediscount='';
                            if($check->packages_discount_rate){
                                $packagediscount = $check->packages_discount_rate . '% on Value Packs.';
                            }
                            $result['reason'] = 'Successfully applied coupon discount: '. $packagediscount;
                            $result['date'] = $date;
                            $result['date1'] = $date1;
                            $result['date2'] = $date2;
                            $result['status'] = 'ok';
                            $end = \Carbon\Carbon::now()->toDateString();
                            $expires = new \Carbon\Carbon($check->date_end);
                            $date = $expires->toFormattedDateString();
                            $now = \Carbon\Carbon::now();
                            $difference = ($expires->diff($now)->days);
                            $result['expiry'] = $date;
                            $result['rate'] = $check->packages_discount_rate;
                            $result['exclusive'] = $check->exclusive;
                            $result['name'] = $check->coupon_code;
                            $result['id'] = $check->id;
                            }
                    } else {
                        $result['reason'] = 'Coupon has expired';
                        $result['date'] = $date;
                        $result['date1'] = $date1;
                        $result['date2'] = $date2;
                        $result['status'] = 'err';
                    }
                }
                }else{
                     $result['reason'] = 'This coupon doesnot apply discounts to value packs.';
                     $result['status'] = 'err';
                }   
            }
        else {
            $result['reason'] = 'Coupon not valid';
            $result['status'] = 'err';
        }

        return json_encode($result);
    }

        public function checkRegCoupon()
    {
        $code = Input::get('code');

        $check = Coupons::active()->where('coupon_code', $code)->first();
        if ($check) {
            $user_id = Auth::user()->id;
            $user = Users::find($user_id);
            $user->coupon_id = '';
            $user->save();
            $date = date('Y-m-d');
            $date1 = DateTime::createFromFormat('Y-m-d', $check->date_begin);
            $date2 = DateTime::createFromFormat('Y-m-d', $check->date_end);
            $date1 = $date1->format('Y-m-d');
            $date2 = $date2->format('Y-m-d');
            if ($date >= $date1 && $date <= $date2) {
                $result['reason'] = '<strong>Please note: We have successfully applied your one-time coupon discount ';
                $result['date'] = $date;
                $result['date1'] = $date1;
                $result['date2'] = $date2;
                $result['status'] = 'ok';
                $result['exclusive'] = $check->exclusive;
                $result['id'] = $check->id;
            } else {
                $result['reason'] = 'Your one-time coupon has expired';
                $result['date'] = $date;
                $result['date1'] = $date1;
                $result['date2'] = $date2;
                $result['status'] = 'err';
            }
        } else {
            $result['reason'] = 'Coupon not valid';
            $result['status'] = 'err';
        }

        return json_encode($result);
    }

    public function deleteAccount()
    {
        $user = Auth::user();
        $user->deleted = true;
        $user->deleted_at = Carbon::now();
        $user->save();
        Auth::logout();
        \Session::flash('success_msg', 'Your account has been deleted');
        return redirect('/login');
    }

}
