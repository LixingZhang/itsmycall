<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Categories;
use App\SubCategories;
use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Posts;
use App\PostsPackages;
use App\Subscription;
use App\Users;
use URL;
use App\Applicants;
use Newsletter;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Input;
use Session;
use Hash;
use DB;

class PostsController extends Controller {

    public function mailchimpsignup($state) {
            $user = Auth::user();
            if($state == 'Australian Capital Territory'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['a6b96d0dd0' => true]]
                );
            }else if($state == 'New South Wales'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['ce366ba266' => true]]
                );
            }else if($state == 'Northern Territory'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['4a8674d560' => true]]
                );
            }else if($state == 'Queensland'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['2c35cb8485' => true]]
                );
            }else if($state == 'South Australia'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['f070ff9f60' => true]]
                );
            }else if($state == 'Tasmania'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['a7a18172b9' => true]]
                );
            }else if($state == 'Victoria'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['366876e27c' => true]]
                );
            }else if($state == 'Western Australia'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['89d2c539e5' => true]]
                );
            }else{
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['8192edf584' => true]]
                );
            }
            $update = Users::find($user->id);
            $update->state = $state;
            $update->save();
            \Session::flash('success_msg', ' Your state has been updated Successfully!');
            return redirect('/customer');
    }

    public function jobPost() {
        $admins = Utils::getUsersInGroup(Users::TYPE_ADMIN);

        return view('job_post', ['categories' => Categories::all(), 'admins' => $admins], $this->data);
    }

    public function detail() {
        return view('/detail');
    }

    public function savejob($id) {
        DB::table('savedjobs')->insert(
            ['job_id' => $id, 'user_id' => Auth::user()->id]
        );
        return redirect()->back();
    }
    public function updateapplied()
    {
        //echo "cool";
        $id = Input::get('appid');
        $status = Input::get('status');
        $app = Applicants::where('id', $id)->get()->first();
        $app->status_user = $status;
        $app->date_change = Carbon::now();
        $app->save();
        $response = Carbon::now()->format('d M Y H:i:s');
        return $response;

        //echo "cool";
    }

    public function deleteapplied($id)
    {   
        $app = Applicants::where('id', $id)->get()->first();
        $app->cus_delete = 1;
        $app->save();
        \Session::flash('success_msg', 'Removed Applied Job!');
        return redirect()->back();
    }

    public function removesavejob($id) {
        DB::table('savedjobs')->where('id',$id)->delete();
        \Session::flash('success_msg', 'Successfully Removed Saved Job!');
        return redirect()->back();
    }

    public function savedjobs() {
        $savedjobs = DB::table('savedjobs')->where('user_id',Auth::user()->id)->orderBy('id', 'desc')->paginate(10);
        $users = Users::all();
        $this->data['savedjobs'] = $savedjobs;
        $this->data['users'] = $users;
        return view('savedjobs2', $this->data);
    }

    public function listJobs() {
        $jobs = Posts::where('status', Posts::STATUS_POSITION_ACTIVE)->orderBy('id', 'desc')->paginate(10);
        $images = DB::table('inboundimages')->get();
        $video = DB::table('inboundimages')->whereNotNull('video')->first();
        $this->data['jobs'] = $jobs;
        $this->data['images'] = $images;
        $this->data['video'] = $video;
        return view('account', $this->data);
    }

    public function appliedjobs() {
        $applicants = DB::table('applicants')->where('apply_id', Auth::User()->id)->whereNull('cus_delete')->get();
        $statuses = DB::table('applied_status')->orderBy('order','asc')->get();
        $this->data['applicants'] = $applicants;
         $this->data['statuses'] = $statuses;
        return view('customerapply', $this->data);
    }

    public function appliedjobsstatus($id) {
        if($id == 'Applied'){
            $applicants = DB::table('applicants')->where('apply_id', Auth::User()->id)->whereNull('status_user')->whereNull('cus_delete')->get();
            $statuses = DB::table('applied_status')->orderBy('order','asc')->get();
            $this->data['applicants'] = $applicants;
            $this->data['statuses'] = $statuses;
            $this->data['id'] = $id;
            return view('customerapply', $this->data);

        }else{
            $applicants = DB::table('applicants')->where('apply_id', Auth::User()->id)->where('status_user', $id)->whereNull('cus_delete')->get();
            $statuses = DB::table('applied_status')->orderBy('order','asc')->get();
            $this->data['applicants'] = $applicants;
            $this->data['statuses'] = $statuses;
            $this->data['id'] = $id;
            return view('customerapply', $this->data);
        }
    }

    public function changePassword() {
        return view('change_password', $this->data);
    }

    public function getApplicants() {
        $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->paginate(10);

        $jobsId = array();
        foreach ($applicants as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);

        $this->data['applicants'] = $applicants;
        return view('applicants', $this->data);
    }

    public function editUser() {
        $user = Auth::user();

        $this->data['user'] = Auth::user();
        return view('edit-user', $this->data);
    }

    public function standardProfile() {
        $this->data['categories'] = Categories::all();
        $this->data['subcategories'] = SubCategories::get();
        


        $user = Auth::user();

        $this->data['user'] = Auth::user();
        return view('edit-standard', $this->data);
    }

    public function maximumProfile() {
        $categories = Categories::get();
        $this->data['categories'] = Categories::all();
        $this->data['subcategories'] = SubCategories::get();
        $this->data['categories'] = $categories;
        $user = Auth::user();

        $this->data['user'] = Auth::user();
        return view('edit-maximum', $this->data);
    }

    public function selectProgram() {
        $user = Auth::user();

        $this->data['user'] = Auth::user();
        return view('select-program', $this->data);
    }

    public function editStore(Request $request) {
        $v = \Validator::make($request->all(), [
            'first-name' => 'required',
            'last-name' => 'required',
            'state' => 'required',
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }
        if(Input::hasFile('avatar')){
            $file = Input::file('avatar');
            $folder = 'images';
                if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $unique = uniqid();
                $file->move(public_path() . '/uploads/' . $folder . '/', $unique . $name);
                $imgpath = URL::to('/uploads/' . $folder . '/' . $unique . $name);
                $user = Auth::user();
                $user->avatar = $imgpath;
                $user->save();
                }
            }
        $user = Auth::user();
        DB::table('activitylog')->insert([
            ['description' => 'Edited Profile', 'user_id' => $user->id, 'created_at' => Carbon::now()]
        ]);
        $user->name = Input::get('first-name') . " " . Input::get('last-name');
        $user->first_name = Input::get('first-name');
        $user->last_name = Input::get('last-name');
        $user->state = Input::get('state');
        \Session::flash('success_msg', 'Profile Updated.');
        $user->save();

        return redirect()->to('/customer');
    }

    public function editStoreStandard(Request $request)
    {

        $v = \Validator::make($request->all(), [
            'first-name' => 'required',
            'last-name' => 'required',
            'phone' => 'required|regex:/^[\+\d\s]*$/',
            'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
            'timeinrole' => 'required',
            'availability' => 'required',
            'category' => 'required',
            'subcategory' => 'required',
            'currentposition' => 'required',
            'employment_type' => 'required',
            'lastwork' => 'required',
            'workfromhome' => 'required',
            'employment_term' => 'required',
            'parentsoption' => 'required'
        ], [
            'currentposition.required' => 'Please confirm your current position (just write NA if you are not currently employed)',
            'lastwork.required' => 'Please confirm your current or previous employer (if this is your first job type NA)',
            'employment_term.required' => 'Employment Status is Required',
            'workfromhome.required' => 'Please select a Work Location',
            'parentsoption.required' => 'Please select the shift times closest to your requirements'
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }

        $user = Users::find(Auth::user()->id);
        $user->name = Input::get('first-name') . " " . Input::get('last-name');
        $user->first_name = Input::get('first-name');
        $user->last_name = Input::get('last-name');
        $user->mobile_no = Input::get('phone');
        $user->city = Input::get('suburb');
        $user->state = Input::get('state');
        $user->postcode = Input::get('postcode');
        $user->timeinrole = Input::get('timeinrole');
        $user->availability = Input::get('availability');
        $user->category = Input::get('category');
        $user->subcategory = json_encode(Input::get('subcategory'), JSON_NUMERIC_CHECK);
        $user->currentposition = Input::get('currentposition');
        $user->employment_type = Input::get('employment_type');
        $user->parentsoption = json_encode(Input::get('parentsoption'));
        $user->workfromhome = json_encode(Input::get('workfromhome'));
        $user->employment_term = json_encode(Input::get('employment_term'));
        $user->profile_type = Users::INBOUND_STANDARD;
        $user->lastwork = Input::get('lastwork');
        \Session::flash('success_msg', 'Congratulations, you are now a part of our Connect Program!');
        $user->save();

        return redirect()->to('/customer');
    }
    
    public function editStoreMaximum(Request $request) {
        $user = Users::find(Auth::user()->id);
        if (Auth::user()->resume == '') {
            $v = \Validator::make(
                ['file' => Input::file('file')],
                ['file' => 'required|mimes:doc,docx,pdf'],
                ['required' => 'Please upload your CV so we can provide to the recruiter on request']
            );
            if ($v->fails()) {
                    Session::flash('error_msg', Utils::messages($v));
                    return redirect()->back()->withInput(Input::all());
                }
        }
        if (Input::file('file')) {
                try {
                    $file = Input::file('file');
                    $filename = $file->getClientOriginalName();
                    $folder = 'attachments';
                    $extention = $file->getClientOriginalExtension();

                    $unique = uniqid();

                    $file->move(public_path() . '/uploads/' . $folder . '/', $user->first_name . $user->last_name . 'Resume-' . $unique .'.'. $extention);
                    $attachment_path = URL::to('/uploads/' . $folder . '/' . $user->first_name . $user->last_name . 'Resume-' . $unique .'.'. $extention);
                    $user->resume = $attachment_path;
                    $user->filename = $filename;
                    $user->save();
                } catch (\Exception $e) {
                    \Session::flash('error_msg', 'Please try with a small file size to upload');
                    return redirect()->back()->withInput(Input::all());
                }

        }


        $v = \Validator::make($request->all(), [
            'first-name' => 'required',
            'last-name' => 'required',
            'phone' => 'required|regex:/^[\+\d\s]*$/',
            'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
            'timeinrole' => 'required',
            'availability' => 'required',
            'category' => 'required',
            'subcategory' => 'required',
            'ambitions' => 'required',
            'wage_min' => 'required',
            'wage_max' => 'required',
            'currentposition' => 'required',
            'employment_type' => 'required',
            'lastwork' => 'required',
            'employment_term' => 'required',
            'workfromhome' => 'required',
            'parentsoption' => 'required',
            'incentivestructure' => 'required'
        ], [
            'currentposition.required' => 'Please confirm your current position (just write NA if you are not currently employed)',
            'lastwork.required' => 'Please confirm your current or previous employer (if this is your first job type NA)',
            'ambitions.required' => 'Please complete your Careers Ambition Brief (Recruiters read this section!)',
            'employment_term.required' => 'Employment Status is required',
            'workfromhome.required' => 'Please select a Work Location',
            'parentsoption.required' => 'Please select the shift times closest to your requirements',
            'incentivestructure.required' => 'Incentive Structure is required'
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::get('wage_min') >= Input::get('wage_max')) {
            Session::flash('error_msg', 'Salary max value must be greater than salary min value');
            return redirect()->back()->withInput(Input::all());
        }

            $user->name = Input::get('first-name') . " " . Input::get('last-name');
            $user->first_name = Input::get('first-name');
            $user->last_name = Input::get('last-name');
            $user->mobile_no = Input::get('phone');
            $user->city = Input::get('suburb');
            $user->state = Input::get('state');
            $user->postcode = Input::get('postcode');
            $user->timeinrole = Input::get('timeinrole');
            $user->availability = Input::get('availability');
            $user->category = Input::get('category');
            $user->subcategory = json_encode(Input::get('subcategory'));
            $user->ambitions = Input::get('ambitions');
            $user->wage_min = Input::get('wage_min');
            $user->wage_max = Input::get('wage_max');
            $user->currentposition = Input::get('currentposition');
            $user->employment_type = Input::get('employment_type');
            $user->parentsoption = json_encode(Input::get('parentsoption'));
            $user->workfromhome = json_encode(Input::get('workfromhome'));
            $user->employment_term = json_encode(Input::get('employment_term'));
            $user->incentivestructure = json_encode(Input::get('incentivestructure'));
            $user->profile_type = Users::INBOUND_MAXIMUM;
            $user->lastwork = Input::get('lastwork');
            \Session::flash('success_msg', 'Congratulations, you are now a part of our Connect Program!');
            $user->save();
        
            return redirect()->to('/customer');
    }
    
    public function delinbound() {
        $user = Auth::user();
        $user->profile_type = "";
        \Session::flash('success_msg', 'You have been removed from our Connect Program!');
        $user->save();

        return redirect()->to('/customer');
    }
    
    

    public function getApplicantsJob($id) {
        $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->paginate(10);

        $jobsId = array();
        foreach ($applicants as $applicant) {
            $jobsId[] = $applicant->job_id;
        }
        $jobsId = array_unique($jobsId);
        $this->data['jobs'] = Posts::findMany($jobsId);

        $applicants = DB::table('applicants')->where('author_id', Auth::User()->id)->where('job_id', $id)->paginate(10);

        $this->data['applicants'] = $applicants;
        return view('applicants', $this->data);
    }

    public function updatePassword() {
        $v = \Validator::make([
                    'password' => Input::get('password'),
                    'old_password' => Input::get('old_password'),
                    'password_confirmation' => Input::get('password_confirmation'),
                        ], [
                    'password' => 'required|min:6|confirmed',
                    'old_password' => 'required',
                    'password_confirmation' => 'required'
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }

        $old_password = Input::get('old_password');

        if (\Auth::validate(['password' => $old_password, 'email' => \Auth::user()->email])) {
            Users::where('id', \Auth::id())->update(['password' => \Hash::make(Input::get('password'))]);
            Session::flash('success_msg', trans('passwords.reset'));
            return redirect()->back();
        } else {
            Session::flash('error_msg', trans('passwords.password_old'));
            return redirect()->back();
        }
    }

    public function stripe($id, Users $user) {
        $token = Input::get('stripeToken');
        $days = Input::get('days');

        $start = Carbon::now();
        $end = Carbon::now()->addDay($days);

        Posts::where('id', $id)->update(['featured_starts_at' => $start, 'featured_ends_at' => $end]);

        $user->charge((env('PER_DAY_CHARGE')) * $days, [
            'source' => $token
        ]);

        return redirect()->back();
    }

    public function store() {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $v = \Validator::make([
                    'title' => Input::get('title'),
                    'description' => Input::get('description'),
                    'company' => Input::get('company'),
                    'country' => Input::get('country'),
                    'state' => Input::get('state'),
                    'category_id' => Input::get('category_id'),
                    'category' => Input::get('category'),
                    'sub_category' => Input::get('sub_category'),
                    'salary' => Input::get('salary'),
                    'file' => Input::file('featured_image'),
                    'experience' => Input::get('experience'),
                    'status' => Input::get('status'),
                        ], [
                    'title' => 'required',
                    'description' => 'required',
                    'file' => 'required|image',
                    'company' => 'required',
                    'country' => 'required',
                    'state' => 'required',
                    'category_id' => 'required',
                    'category' => 'required',
                    'sub_category' => 'required',
                    'salary' => 'required',
                    'experience' => 'required',
                    'status' => 'required'
        ]);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }

        $post_item = new Posts();
        $post_item->title = Input::get('title');
        $post_item->author_id = \Auth::id();
        $post_item->description = Input::get('description');
        $post_item->company = Input::get('company');
        $post_item->slug = Str::slug(Input::get('title')) . "-" . Carbon::now()->timestamp;
        $post_item->country = Str::slug(Input::get('country'));
        $post_item->state = Str::slug(Input::get('state'));
        $post_item->category_id = Input::get('sub_category');
        $post_item->category = Str::slug(Input::get('category'));
        $post_item->subcategory = Str::slug(Input::get('sub_category'));
        $post_item->salary = Str::slug(Input::get('salary'));
        $post_item->experience = Str::slug(Input::get('experience'));
        $post_item->status = Input::get('status');
        $post_item->featured_image = Utils::imageUpload(Input::file('featured_image'), 'images');
        $post_item->save();

        Session::flash('success_msg', trans('messages.post_created_success'));
        return redirect()->back();
    }

    public function deletesub($id) {
        $sub = Subscription::where('id', $id)->get()->first();
        $sub->active = 0;
        $sub->deleted_at = Carbon::now();
        $sub->save();
        $user = Auth::User();

        $state = $sub->state;
        $cat = $sub->category_id;
        $subcat = json_decode($sub->sub_category_id);

        if($state == 'Australian Capital Territory'){
            $statesubscribe = '36523aacbb';
        }else if($state == 'New South Wales'){
            $statesubscribe = 'c66d882ee6';
        }else if($state == 'Northern Territory'){
            $statesubscribe = '684dd7b4a5';
        }else if($state == 'Queensland'){
            $statesubscribe = 'e543986677';
        }else if($state == 'South Australia'){
            $statesubscribe = 'f8393e8892';
        }else if($state == 'Tasmania'){
            $statesubscribe = '716b9d48f3';
        }else if($state == 'Victoria'){
            $statesubscribe = '958f23ea5d';
        }else if($state == 'Western Australia'){
            $statesubscribe = 'eee433beaa';
        }

        if($user->state == 'Australian Capital Territory'){
                $stateusersubscribe = '242958c1bc';
        }else if($user->state == 'New South Wales'){
                $stateusersubscribe = '8d8008c417';
        }else if($user->state == 'Northern Territory'){
                $stateusersubscribe = '8c62177499';
        }else if($user->state == 'Queensland'){
                $stateusersubscribe = '8b303bf275';
        }else if($user->state == 'South Australia'){
                $stateusersubscribe = '4e3afb85c2';
        }else if($user->state == 'Tasmania'){
                $stateusersubscribe = '4272833652';
        }else if($user->state == 'Victoria'){
                $stateusersubscribe = '4bfbf1a51b';
        }else if($user->state == 'Western Australia'){
                $stateusersubscribe = '6872d826f3';
        }else{
                $stateusersubscribe = 'e2a82cc975';
        }

        if($cat == '2'){
            $categorysubscribe = '07c5ec1cd7';
        }else if($cat == '3'){
            $categorysubscribe = 'cb22cb0b93';
        }else if($cat == '4'){
            $categorysubscribe = '063b11a68e';
        }else if($cat == '5'){
            $categorysubscribe = '2a0abf58b9';
        }else if($cat == '6'){
            $categorysubscribe = '17090b1081';
        }else if($cat == '7'){
            $categorysubscribe = '440c57ee42';
        }else if($cat == '8'){
            $categorysubscribe = '6b64ff77b8';
        }else if($cat == '9'){
            $categorysubscribe = '19421d8b7a';
        }else if($cat == '10'){
            $categorysubscribe = 'a48fd556bb';
        }else if($cat == '12'){
            $categorysubscribe = '25f40dd1d3';
        }else if($cat == '14'){
            $categorysubscribe = '3e3d3c7567';
        }else if($cat == '15'){
            $categorysubscribe = '067e333768';
        }

        $newsletterSubCategories = '';
        $subCategories = SubCategories::whereIn('id', $subcat)->get();
        foreach ($subCategories as $subCategory) {
            $newsletterSubCategories .= $subCategory->title . ', ';
        }

        Newsletter::subscribeOrUpdate(
            $user->email, 
            [
            'FNAME' => $user->first_name, 
            'LNAME' => $user->last_name, 
            'MAINJOBCAT' => Categories::where('id', $cat)->get()->first()->title, 
            'SUBCAT' => $newsletterSubCategories,
            ], 
            'jobalert',['interests'=>[$statesubscribe => true,$stateusersubscribe => true, $categorysubscribe => true, '9401e40606' => false, 'a91c046b7c' => true]]
            );

        Session::flash('success_msg', "Job Alert removed.");
        return redirect()->back();
    }

    public function subscriptions() {

        $user = Auth::User();
        $userId = $user->id;
        $form = array();
        $subs = array();
        $categories = Categories::get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('category_id', $subcat->id)->count();
            }
        }
        $subscriptions =  Subscription::where('active', 1)->where("user_id", $userId)->get();
        foreach ($subscriptions as $sub) {
            $subcategoryTitles = [];
            $incentivestructureTitle = [];
            $sub->category = Categories::where('id', $sub->category_id)->get()->first();
            $subcategories = SubCategories::whereIn('id', json_decode($sub->sub_category_id, true))->get();

            $subcategories->each(function($subcategory) use (&$subcategoryTitles) {
                $subcategoryTitles[] = $subcategory->title;
            });
            $sub->subcategoryTitles = $subcategoryTitles;

            foreach (json_decode($sub->incentivestructure, true) as $incentivestructure) {
                $incentivestructureTitle[] = Subscription::INCENTIVE_STRUCTURE[$incentivestructure];
            }
            $sub->incentivestructureTitle = $incentivestructureTitle;
        }
        $this->data['subscriptions'] = $subscriptions;

        $this->data['categories'] = $categories;
        $this->data['subscriptions'] = $subscriptions;

        return view('subscriptions', $this->data);
    }

    public function saveSubscriptions(Request $request) {
        $errors = [];
        if (Input::get('category') == '-1') {
            $errors['category'] = 'Please select the main job category';
        }

        if (Input::get('searchour') === 'yes') {
            if (Input::get('hrsalstart') >= Input::get('hrsalend')) {
                $errors['hrsalstart'] = 'Max hourly rate must be larger than min hourly rate';
            }
        } else {
            if (Input::get('wage_min') >= Input::get('wage_max')) {
                $errors['wage_min'] =  'Max wage must be larger than min wage';
            }
        }

        $v = \Validator::make($request->all(), [
                    'subcategory' => 'required',
                    'employment_type' => 'required',
                    'employment_term' => 'required',
                    'incentivestructure'=> 'required',
                    'radius' => 'required|numeric',
                    'joblocation' => 'required',
                    'work_from_home' => 'required',
                    'parentsoption' => 'required',
                    'state' => 'required',
                    'pay_cycle' => 'required',
        ], [
            'joblocation.required' => 'Please confirm your location so we can find jobs near you.',
            'work_from_home.required' => 'Please select your preferred Work Location.',
            'parentsoption.required' => 'Please select the shift times closest to your requirements.',
            'employment_term.required' => 'Please select your preferred Employment Status.',
            'incentivestructure.required' => 'Please select your preferred Incentive Structure.',
            'pay_cycle.required' => 'Please confirm your preferred pay frequency.'
        ]);

        if ($v->fails()) {
            $errors = array_merge($errors, array_combine(array_keys($v->failed()), $v->errors()->all()));
        }

        if (!empty($errors)) {
            return response()->json(compact('errors'));
        }

        $user = Auth::User();
        $userId = $user->id;
        $cat = Input::get('category');
        $subcat = Input::get('subcategory');
        $hourly_start = Input::get('hrsalstart');
        $hourly_end = Input::get('hrsalend');
        $sal_type = Input::get('searchour');
        $wage_max = Input::get('wage_max');
        $wage_min = Input::get('wage_min');
        $joblocation = Input::get('joblocation');
        $pay_cycle = Input::get('pay_cycle');
        $employment_type = Input::get('employment_type');
        $employment_term = Input::get('employment_term');
        $incentivestructure = Input::get('incentivestructure');
        $work_from_home= Input::get('work_from_home');
        $parentsoption= Input::get('parentsoption');
        $radius = Input::get('radius');
        $lat = Input::get('lat');
        $lng = Input::get('lng');
        $city = Input::get('city');
        $postcode = Input::get('postcode');
        $state = Input::get('state');
        $advertiser_type = Input::get('advertiser_type') === 'both' ? null : Input::get('advertiser_type');

        $newsletterSubCategories = '';
        $subCategories = SubCategories::whereIn('id', $subcat)->get();
        foreach ($subCategories as $subCategory) {
            $newsletterSubCategories .= $subCategory->title . ', ';
        }

        if($state == 'Australian Capital Territory'){
            $statesubscribe = '36523aacbb';
        }else if($state == 'New South Wales'){
            $statesubscribe = 'c66d882ee6';
        }else if($state == 'Northern Territory'){
            $statesubscribe = '684dd7b4a5';
        }else if($state == 'Queensland'){
            $statesubscribe = 'e543986677';
        }else if($state == 'South Australia'){
            $statesubscribe = 'f8393e8892';
        }else if($state == 'Tasmania'){
            $statesubscribe = '716b9d48f3';
        }else if($state == 'Victoria'){
            $statesubscribe = '958f23ea5d';
        }else if($state == 'Western Australia'){
            $statesubscribe = 'eee433beaa';
        }

        if($user->state == 'Australian Capital Territory'){
                $stateusersubscribe = '242958c1bc';
        }else if($user->state == 'New South Wales'){
                $stateusersubscribe = '8d8008c417';
        }else if($user->state == 'Northern Territory'){
                $stateusersubscribe = '8c62177499';
        }else if($user->state == 'Queensland'){
                $stateusersubscribe = '8b303bf275';
        }else if($user->state == 'South Australia'){
                $stateusersubscribe = '4e3afb85c2';
        }else if($user->state == 'Tasmania'){
                $stateusersubscribe = '4272833652';
        }else if($user->state == 'Victoria'){
                $stateusersubscribe = '4bfbf1a51b';
        }else if($user->state == 'Western Australia'){
                $stateusersubscribe = '6872d826f3';
        }else{
                $stateusersubscribe = 'e2a82cc975';
        }

        if($cat == '2'){
            $categorysubscribe = '07c5ec1cd7';
        }else if($cat == '3'){
            $categorysubscribe = 'cb22cb0b93';
        }else if($cat == '4'){
            $categorysubscribe = '063b11a68e';
        }else if($cat == '5'){
            $categorysubscribe = '2a0abf58b9';
        }else if($cat == '6'){
            $categorysubscribe = '17090b1081';
        }else if($cat == '7'){
            $categorysubscribe = '440c57ee42';
        }else if($cat == '8'){
            $categorysubscribe = '6b64ff77b8';
        }else if($cat == '9'){
            $categorysubscribe = '19421d8b7a';
        }else if($cat == '10'){
            $categorysubscribe = 'a48fd556bb';
        }else if($cat == '12'){
            $categorysubscribe = '25f40dd1d3';
        }else if($cat == '14'){
            $categorysubscribe = '3e3d3c7567';
        }else if($cat == '15'){
            $categorysubscribe = '067e333768';
        }

        Newsletter::subscribeOrUpdate(
            $user->email, 
            [
            'FNAME' => $user->first_name, 
            'LNAME' => $user->last_name, 
            'MAINJOBCAT' => Categories::where('id', $cat)->get()->first()->title, 
            'SUBCAT' => $newsletterSubCategories,
            ], 
            'jobalert',['interests'=>[$statesubscribe => true,$stateusersubscribe => true, $categorysubscribe => true, '9401e40606' => true, 'a91c046b7c' => false]]
            );

        $sub = new Subscription();

        $sub->user_id = $userId;
        $sub->category_id = $cat;
        $sub->sub_category_id = json_encode($subcat);
        $sub->wage_min = $wage_min;
        $sub->wage_max = $wage_max;
        $sub->hourly_start = $hourly_start;
        $sub->hourly_end = $hourly_end;
        $sub->salary_type = $sal_type;
        $sub->pay_cycle = json_encode($pay_cycle);
        $sub->employment_type = $employment_type;
        $sub->employment_term = json_encode($employment_term);
        $sub->incentivestructure = json_encode($incentivestructure);
        $sub->work_from_home = json_encode($work_from_home);
        $sub->parentsoption = json_encode($parentsoption);
        $sub->active = 1;
        $sub->radius = $radius;
        $sub->lat = $lat;
        $sub->lng = $lng;
        $sub->location = $joblocation;
        $sub->state = $state;
        $sub->advertiser_type = $advertiser_type;
        $sub->created_at = Carbon::now();

        $sub->save();
        return response()->json(['success' => 'job Alert created']);
    }

    public function deleteAccount()
    {
        $user = Auth::user();
        $user->deleted = true;
        $user->deleted_at = Carbon::now();
        $user->save();
        Auth::logout();
        Session::flash('success_msg', 'Your account has been deleted');
        return redirect('/login');
    }
}
