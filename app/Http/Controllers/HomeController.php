<?php

namespace App\Http\Controllers;

use App;
use App\Ads;
use App\Categories;
use App\Countries;
use App\Coupons;
use App\GalleryImage;
use App\Groups;
use App\Libraries\Utils;
use App\Pages;
use App\PostLikes;
use App\PostRatings;
use App\PostTags;
use App\Posts;
use App\Applicants;
use App\ReferralCode;
use App\Settings;
use App\SubCategories;
use App\Tags;
use App\Testimonials;
use App\Users;
use App\UsersGroups;
use App\Views;
use Carbon\Carbon;
use DB;
use DrewM\MailChimp\MailChimp;
use Feed;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Input;
use Newsletter;
use Twitter;
use Session;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use URL;

class HomeController extends BaseController
{

    public function __construct()
    {

        parent::__construct();
    }

    public function allblogs()
    {   
        $categories = DB::table('blogcategories')->get();
        $blogsubcategories = DB::table('blogsubcategories')->get();
        $this->data['keywords'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_keywords;
        $this->data['seodescription'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_description;
        $this->data['categories'] = $categories;
        $this->data['blogsubcategories'] = $blogsubcategories;
        return view('blogsall', $this->data);
    }

    public function jobalertsredirect($id)
    {   
       if (Auth::user() && Utils::isCustomer(Auth::user()->id)) {
           return redirect()->to('/customer/subscriptions');
       }else{
           return redirect()->to('/login'.'?e='.$id);
       }
    }

    public function appliedjobsredirect($id)
    {   
       if (Auth::user() && Utils::isCustomer(Auth::user()->id)) {
           return redirect()->to('/customer/appliedjobs');
       }else{
           return redirect()->to('/login'.'?e='.$id);
       }
    }

    public function blogsbycategory($category_id,$sub_categoryid)
    {
        $category = DB::table('blogcategories')->where('slug',$category_id)->first();
        $blogsubcategory = DB::table('blogsubcategories')->where('slug',$sub_categoryid)->first();
        if($category && $blogsubcategory){
                $blogs = DB::table('blogs')->where('parent_category',$category->id)->where('sub_category',$blogsubcategory->id)->paginate(10);
                $this->data['keywords'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_keywords;
                $this->data['seodescription'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_description;
                $this->data['category'] = $category;
                $this->data['blogs'] = $blogs;
                $this->data['blogsubcategory'] = $blogsubcategory;
                return view('blogs', $this->data);
        }
        else{
            return $this->throw404();
        }
    }

    public function blogssort($category_id,$sub_categoryid,$sortid)
    {
        $category = DB::table('blogcategories')->where('slug',$category_id)->first();
        $blogsubcategory = DB::table('blogsubcategories')->where('slug',$sub_categoryid)->first();
        if($category && $blogsubcategory){
                if($sortid == 'latest'){
                    $blogs = DB::table('blogs')->where('parent_category',$category->id)->where('sub_category',$blogsubcategory->id)->orderBy('id','desc')->paginate(10);
                }elseif($sortid == 'oldest'){
                    $blogs = DB::table('blogs')->where('parent_category',$category->id)->where('sub_category',$blogsubcategory->id)->orderBy('id','asc')->paginate(10);
                }else{
                    $blogs = DB::table('blogs')->where('parent_category',$category->id)->where('sub_category',$blogsubcategory->id)->orderBy('views','desc')->paginate(10);
                }
                $this->data['keywords'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_keywords;
                $this->data['seodescription'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_description;
                $this->data['category'] = $category;
                $this->data['blogs'] = $blogs;
                $this->data['blogsubcategory'] = $blogsubcategory;
                $this->data['option'] = $sortid;
                return view('blogs', $this->data);
        }
        else{
            return $this->throw404();
        }
    }

    public function blogsdetail($category_id,$sub_categoryid,$blogid)
    {  
        $category = DB::table('blogcategories')->where('slug',$category_id)->first();
        $blogsubcategory = DB::table('blogsubcategories')->where('slug',$sub_categoryid)->first();
        if($category && $blogsubcategory){
                $blog = DB::table('blogs')->where('slug',$blogid)->first();
                DB::table('blogs')->where('slug',$blogid)->update(['views' => $blog->views+1]);
                $this->data['keywords'] = $blog->seo_keywords;
                $this->data['seodescription'] = $blog->seo_description;
                $this->data['category'] = $category;
                $this->data['blog'] = $blog;
                $this->data['blogsubcategory'] = $blogsubcategory;
                $this->data['page_title'] = $blog->page_title;
                return view('blog_details', $this->data);
        }
        else{
            return $this->throw404();
        }
    }

    public function checkReferral()
    {
        $code = Input::get('code');
        die();
        $check = ReferralCode::where('referral_code', $code)->first();
        if ($check) {
            $result['reason'] = 'Code Matched';
            $result['status'] = 'ok';
        } else {
            $result['reason'] = 'Code not found';
            $result['status'] = 'err';
        }
        $result['hello'];

        return json_encode($result);
    }

    public function mailtest()
    {   
        abort(404);
        $state = 'New South Wales';
        $cat = 4;
        $user = 'Queensland';
        if($state == 'Australian Capital Territory'){
            $statesubscribe = '36523aacbb';
        }else if($state == 'New South Wales'){
            $statesubscribe = 'c66d882ee6';
        }else if($state == 'Northern Territory'){
            $statesubscribe = '684dd7b4a5';
        }else if($state == 'Queensland'){
            $statesubscribe = 'e543986677';
        }else if($state == 'South Australia'){
            $statesubscribe = 'f8393e8892';
        }else if($state == 'Tasmania'){
            $statesubscribe = '716b9d48f3';
        }else if($state == 'Victoria'){
            $statesubscribe = '958f23ea5d';
        }else if($state == 'Western Australia'){
            $statesubscribe = 'eee433beaa';
        }

        if($user == 'Australian Capital Territory'){
                $stateusersubscribe = '242958c1bc';
        }else if($user == 'New South Wales'){
                $stateusersubscribe = '8d8008c417';
        }else if($user == 'Northern Territory'){
                $stateusersubscribe = '8c62177499';
        }else if($user == 'Queensland'){
                $stateusersubscribe = '8b303bf275';
        }else if($user == 'South Australia'){
                $stateusersubscribe = '4e3afb85c2';
        }else if($user == 'Tasmania'){
                $stateusersubscribe = '4272833652';
        }else if($user == 'Victoria'){
                $stateusersubscribe = '4bfbf1a51b';
        }else if($user == 'Western Australia'){
                $stateusersubscribe = '6872d826f3';
        }else{
                $stateusersubscribe = 'e2a82cc975';
        }

        if($cat == '2'){
            $categorysubscribe = '07c5ec1cd7';
        }else if($cat == '3'){
            $categorysubscribe = 'cb22cb0b93';
        }else if($cat == '4'){
            $categorysubscribe = '063b11a68e';
        }else if($cat == '5'){
            $categorysubscribe = '2a0abf58b9';
        }else if($cat == '6'){
            $categorysubscribe = '17090b1081';
        }else if($cat == '7'){
            $categorysubscribe = '440c57ee42';
        }else if($cat == '8'){
            $categorysubscribe = '6b64ff77b8';
        }else if($cat == '9'){
            $categorysubscribe = '19421d8b7a';
        }else if($cat == '10'){
            $categorysubscribe = 'a48fd556bb';
        }else if($cat == '12'){
            $categorysubscribe = '25f40dd1d3';
        }else if($cat == '14'){
            $categorysubscribe = '3e3d3c7567';
        }else if($cat == '15'){
            $categorysubscribe = '067e333768';
        }
        Newsletter::subscribeOrUpdate(
            'asim1234@gmail.com', 
            [
            'FNAME' => 'Asim', 
            'LNAME' => 'GH', 
            'MAINJOBCAT' => 'Agent', 
            'SUBCAT' => 'New',
            ], 
            'jobalert',['interests'=>[$statesubscribe => true,$stateusersubscribe => true, $categorysubscribe => true, '9401e40606' => false, 'a91c046b7c' => true]]
            );
        dd('Done');
    }

    public function install()
    {
        \Artisan::call('cache:clear');
        \Artisan::call('migrate');

        Session::flash('success_msg', 'Successfully migrated new database columns , Login below to continue ');

        return redirect()->to('/login');
    }

    public function getCounts($cat, $options)
    {

        $categories = Categories::get();

        foreach ($categories as $category) {

            //if (
            $subcategories = SubCategories::where('parent_id', $category->id)->get();

            $category->subcategory = $subcategories;
        }
    }


    public function out($id)
    {

        $job = Posts::where('id', $id)->get()->first();
        if (Auth::user()) {
           $out_entered = \App\Applicants::where('job_id',$job->id)->where('apply_id',Auth::user()->id)->where('applied_url','!=','')->get();
            if (count($out_entered) == 0) {
               DB::table('applicants')->insert(
                ['applied_url' => $job->email_or_link, 'apply_id' => Auth::user()->id, 'author_id' => $job->author_id, 'job_id' => $job->id, 'title' => $job->title]
                );
            }
        }
        $job->out_count = $job->out_count + 1;
        $job->save();

        return redirect()->away($job->email_or_link);
    }

    public function index()
    {

        $recent_jobs = Posts::active()->orderBy('id', 'desc')->paginate(10);


        $categories = Categories::get();

        $countries = Countries::get();

        $companies = Posts::groupby(['company'])->limit(20)->get();

        $states = Posts::groupby(['state'])->limit(20)->get();

        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);


        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();

        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::active()->where('category', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::active()->where('subcategory', $subcat->id)->count();
            }
        }


        $this->data['recent_jobs'] = $recent_jobs;
        $this->data['categories'] = $categories;
        $this->data['countries'] = $countries;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['states'] = $states;
        $this->data['seo_keywords'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_keywords;
        $this->data['seo_description'] = Utils::getSettings(Settings::CATEGORY_SEO)->seo_description;

        return view('index', $this->data);
    }

    public function jobDetails($id)
    {
        $Lat = 0;
        $Lon = 0;


        $jobs = Posts::where('slug', $id)->first();

        if (!$jobs) {
            abort(404);
        }

        $subcategory = Subcategories::where('id', $jobs['attributes']['subcategory'])->get();
        $keywords = $subcategory[0]['attributes']['seo_keywords'];
        $seodescription = $subcategory[0]['attributes']['seo_description'];
        $count = $jobs->views;
        if($jobs->questions){
            $questions = json_decode($jobs->questions);
        }else{
            $questions = NULL;
        }
        $jobs->views = $count + 1;
        $jobs->save();
        $views = new Views();
        $views->job_id = $jobs->id;

        $views->save();

        if (sizeof($jobs) > 0) {
            $Address = urlencode($jobs->state);
            $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $Address . "&sensor=true";
            $xml = simplexml_load_file($request_url) or die("url not loading");
            $status = $xml->status;
            if ($status == "OK") {
                $Lat = $xml->result->geometry->location->lat;
                $Lon = $xml->result->geometry->location->lng;
            }
            $this->data['questions'] = $questions;
            $this->data['lat'] = $Lat;
            $this->data['lon'] = $Lon;
            $this->data['keywords'] = $keywords;
            $this->data['seodescription'] = $seodescription;
            $this->data['page_title'] = "ItsMyCall | " . $jobs->title;
            $this->data['job'] = $jobs;
            $this->data['uploadfiles'] = DB::table('postsimages')
                    ->where('post_id', '=', $jobs->id)
                    ->get();


            return view('job_details', $this->data);
        } else {
            return view('errors.404', $this->data);
        }
    }

    public function jobDetails2($category_id,$subcategory_id,$location_id,$job_id)
    {
        $Lat = 0;
        $Lon = 0;


        $jobs = Posts::where('id', $job_id)->first();

        if (!$jobs) {
            abort(404);
        }

        $subcategory = Subcategories::where('id', $jobs['attributes']['subcategory'])->get();
        $keywords = $subcategory[0]['attributes']['seo_keywords'];
        $seodescription = $subcategory[0]['attributes']['seo_description'];
        $count = $jobs->views;
        if($jobs->questions){
            $questions = json_decode($jobs->questions);
        }else{
            $questions = NULL;
        }
        $jobs->views = $count + 1;
        $jobs->save();
        $views = new Views();
        $views->job_id = $jobs->id;

        $views->save();

        if (sizeof($jobs) > 0) {
            $Address = urlencode($jobs->state);
            $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $Address . "&sensor=true";
            $xml = simplexml_load_file($request_url) or die("url not loading");
            $status = $xml->status;
            if ($status == "OK") {
                $Lat = $xml->result->geometry->location->lat;
                $Lon = $xml->result->geometry->location->lng;
            }
            $this->data['questions'] = $questions;
            $this->data['lat'] = $Lat;
            $this->data['lon'] = $Lon;
            $this->data['keywords'] = $keywords;
            $this->data['seodescription'] = $seodescription;
            $this->data['page_title'] = "ItsMyCall | " . $jobs->title;
            $this->data['job'] = $jobs;
            $this->data['uploadfiles'] = DB::table('postsimages')
                    ->where('post_id', '=', $jobs->id)
                    ->get();


            return view('job_details', $this->data);
        } else {
            return view('errors.404', $this->data);
        }
    }

    public function jobApply(Request $request)
    {   $id = Input::get('user_id');
        $errors = [];
        if (Auth::check()) {
        }else{
            $apply_count = \App\Applicants::where('email',Input::get('email'))->where('job_id',Input::get('job_id'))->count();
            $user_count = \App\Users::where('email',Input::get('email'))->count();
            if ($apply_count > 0) {
                \Session::flash('error_msg', '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> You have already applied for this Job.');
                 return response()->json(['success' => '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> You have already applied for this Job.']);
            }
            if($user_count > 0){
                \Session::flash('error_msg', '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> You already have an account. Please login to apply for this job.');
                 return response()->json(['success' => '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> You already have an account. Please login to apply for this job.']);
            }
        }
        $messages = array(
            'first_name.required' => 'You cant leave the First Name field empty',
            'last_name.required' => 'You cant leave the Last Name field empty',
            'comment.required' => 'Please add a cover note to your application'
        );

        if (Auth::check()) {
            if (Auth::user()->resume == '') {
                $v = \Validator::make(
                    [
                        'resume' => Input::file('file')
                    ],
                    ['resume' => 'required|mimes:doc,docx,pdf'],
                    ['resume.required' => 'Please upload your CV via "Your Resume" field to submit your application']
                );

                if ($v->fails()) {
                    $errors = array_merge($errors, array_combine(array_keys($v->failed()), $v->errors()->all()));
                }
            }
        }else{
            $v = \Validator::make(
                [
                    'resume' => Input::file('file')
                ],
                ['resume' => 'required|mimes:doc,docx,pdf'],
                ['resume.required' => 'Please upload your CV via "Your Resume" field to submit your application']
            );

            if ($v->fails()) {
                $errors = array_merge($errors, array_combine(array_keys($v->failed()), $v->errors()->all()));
            }

        }
        if (!Auth::user() && Input::get('create_account') == 1) {
             $v = \Validator::make(
            [
                'email' => Input::get('email'),
                'first_name' => Input::get('first_name'),
                'last_name' => Input::get('last_name'),
                'comment' => Input::get('comment'),
                'phone' => Input::get('phone'),
                'state' => Input::get('state'),
                'password' => Input::get('password'),
                'password_confirmation' => Input::get('password_confirmation'),
            ],
            ['first_name' => 'required', 'last_name' => 'required', 'email' => 'required|email|unique:users', 'comment' => 'required', 'state' => 'required', 'phone' => 'required|regex:/^[\+\d\s]*$/', 'password' => 'required|confirmed|min:6'],
                $messages
            );
        }else{
            $v = \Validator::make(
            [
                'email' => Input::get('email'),
                'first_name' => Input::get('first_name'),
                'last_name' => Input::get('last_name'),
                'comment' => Input::get('comment'),
                'phone' => Input::get('phone'),
            ],
            ['first_name' => 'required', 'last_name' => 'required', 'email' => 'required|email', 'comment' => 'required', 'phone' => 'required|regex:/^[\+\d\s]*$/'],
            $messages
            );
        }

        if ($v->fails()) {
            $errors = array_merge($errors, array_combine(array_keys($v->failed()), $v->errors()->all()));
        } 

        if (!empty($errors)) {
            return response()->json(compact('errors'));
        }
        $job_id = Input::get('job_id');
        $attachment_path = NULL;
        $answers = NULL;
        $questions = NULL;
        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        if (count($request->answer) > 0) {
            $answers = json_encode($request->answer);
        }
        if (count($request->questions) > 0) {
            $questions = json_encode($request->questions);
        }
        if (!Input::file('file')) {
                $attachment_path = Auth::user()->resume;
        } else {
            try {
                    $file = Input::file('file');
                    $filename = $file->getClientOriginalName();
                    $folder = 'attachments';
                    $extention = $file->getClientOriginalExtension();

                    $unique = uniqid();

                    $file->move(public_path() . '/uploads/' . $folder . '/', $first_name . $last_name . 'Resume-' . $unique .'.'. $extention);
                    $attachment_path = URL::to('/uploads/' . $folder . '/' . $first_name . $last_name . 'Resume-' . $unique .'.'. $extention);
                } catch (\Exception $e) {
                    \Session::flash('error_msg', '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Please try with a small file size to upload');
                    return response()->json(['success' => '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Please try with a small file size to upload']);
                }
        }
        if (Auth::check()) {
            if(Input::file('file')){
                Users::where('id', Auth::user()->id)->update(['resume' => $attachment_path,'filename' => $filename]);
            }
           DB::table('applicants')->insert(
            ['name' => $first_name . ' ' . $last_name, 'email' => Input::get('email'), 'cover_note' => Input::get('comment'), 'title' => Input::get('title'), 'apply_id' => Auth::user()->id, 'job_id' => Input::get('job_id'), 'reason1' => Input::get('reason1'), 'reason2' => Input::get('reason2'), 'reason3' => Input::get('reason3'), 'resume' => $attachment_path, 'author_id' => $id, 'questions' => $questions, 'answers' => $answers, 'phone' => Input::get('phone'), 'status' => \App\ApplicantsStatus::where('deleted', false)->where('order',0)->first()->status]
            );
        }elseif(Input::get('create_account') == 1){
            $customer_group = DB::table('groups')->where('name', 'customer')->first();

            $hash = md5(strtolower(trim(Input::get('email'))));

            $confirmation_code = str_random(30);

            $user = new Users();
            $user->name = Input::get('first_name') . " " . Input::get('last_name');
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->slug = Str::slug(Input::get('name'));
            $user->state = Input::get('state');
            $user->mobile_no = Input::get('phone');
            $user->email = Input::get('email');
            $user->password = \Hash::make(Input::get('password'));
            $user->activation_code = $confirmation_code;
            $user->avatar = "/if_profile.png";
            $user->filename = $filename;
            $user->resume = $attachment_path;
            $user->save();
            $user_group = new UsersGroups();
            $user_group->user_id = $user->id;
            $user_group->group_id = $customer_group->id;
            $user_group->save();

            $new_user = Users::where('email',Input::get('email'))->first();

            DB::table('activitylog')->insert([
                    ['description' => 'Created Account', 'user_id' => $user->id, 'created_at' => Carbon::now()]
                ]);



            DB::table('applicants')->insert(
            ['name' => $first_name . ' ' . $last_name, 'email' => Input::get('email'), 'cover_note' => Input::get('comment'), 'title' => Input::get('title'), 'apply_id' => $new_user->id, 'job_id' => Input::get('job_id'), 'reason1' => Input::get('reason1'), 'reason2' => Input::get('reason2'), 'reason3' => Input::get('reason3'), 'resume' => $attachment_path, 'author_id' => $id, 'questions' => $questions, 'answers' => $answers, 'phone' => Input::get('phone'), 'status' => \App\ApplicantsStatus::where('deleted', false)->where('order',0)->first()->status]
            );
        }else{
            DB::table('applicants')->insert(
            ['name' => $first_name . ' ' . $last_name, 'email' => Input::get('email'), 'cover_note' => Input::get('comment'), 'title' => Input::get('title'), 'apply_id' => NULL, 'job_id' => Input::get('job_id'), 'reason1' => Input::get('reason1'), 'reason2' => Input::get('reason2'), 'reason3' => Input::get('reason3'), 'resume' => $attachment_path, 'author_id' => $id, 'questions' => $questions, 'answers' => $answers, 'phone' => Input::get('phone'), 'status' => \App\ApplicantsStatus::where('deleted', false)->where('order',0)->first()->status]
            );
        }
        $job = Posts::where('id', $job_id)->get()->first();
        $jtitle = $job->title;
        $secondary = $job->person_email;
        $email = Input::get('email');
        $author_email = Users::where('id', $id)->first();
        if ($job && $author_email && $attachment_path) {
            \Mail::send(
                'apply',
                ['name' => $first_name . ' ' . $last_name, 'email' => Input::get('email'), 'comment' => Input::get('comment'), 'jtitle' => $jtitle,'reason1' => Input::get('reason1'),'reason2' => Input::get('reason2'),'reason3' => Input::get('reason3'), 'questions' => $request->questions, 'answers' => $request->answer],
                function ($message) use ($author_email, $attachment_path, $jtitle) {
                    $message->to($author_email->email)->attach($attachment_path)
                            ->subject('ItsMyCall - New Applicant for ' . $jtitle . ' Job');
                }
            );

            if ($secondary != "") {
                \Mail::send(
                    'apply',
                    ['name' => $first_name . ' ' . $last_name, 'email' => Input::get('email'), 'comment' => Input::get('comment'), 'jtitle' => $jtitle,'reason1' => Input::get('reason1'),'reason2' => Input::get('reason2'),'reason3' => Input::get('reason3'), 'questions' => $request->questions, 'answers' => $request->answer],
                    function ($message) use ($secondary, $attachment_path, $jtitle) {
                        $message->to($secondary)->attach($attachment_path)
                                ->subject('ItsMyCall - New Applicant for ' . $jtitle . ' Job');
                    }
                );
            }
            if (Auth::check()) {
                \Mail::send(
                'job-apply',
                ['email' => $email,'first_name' => $first_name, 'job' => $jtitle, 'company' => $job->company, 'comment' => Input::get('comment'),'reason1' => Input::get('reason1'),'reason2' => Input::get('reason2'),'reason3' => Input::get('reason3'), 'questions' => $request->questions, 'answers' => $request->answer],
                function ($message) use ($email, $attachment_path, $jtitle) {
                    $message->to($email)->attach($attachment_path)
                            ->subject('Your Job Application');
                }
                );
            }elseif(Input::get('create_account') == 1){
                \Mail::send(
                'job-apply-register',
                ['confirmation_code' => $confirmation_code,'email' => $email,'first_name' => $first_name, 'job' => $jtitle, 'company' => $job->company, 'comment' => Input::get('comment'),'reason1' => Input::get('reason1'),'reason2' => Input::get('reason2'),'reason3' => Input::get('reason3'), 'questions' => $request->questions, 'answers' => $request->answer],
                function ($message) use ($email, $attachment_path, $jtitle) {
                    $message->to($email)->attach($attachment_path)
                            ->subject('You’re almost done - Please verify your email address');
                }
                );
            }else{
                \Mail::send(
                'job-apply-unregister',
                ['email' => $email,'first_name' => $first_name,'last_name' => Input::get('last_name'), 'job' => $jtitle, 'company' => $job->company, 'comment' => Input::get('comment'),'reason1' => Input::get('reason1'),'reason2' => Input::get('reason2'),'reason3' => Input::get('reason3'), 'questions' => $request->questions, 'answers' => $request->answer],
                function ($message) use ($email, $attachment_path, $jtitle) {
                    $message->to($email)->attach($attachment_path)
                            ->subject('Your Job Application');
                }
                );
            }
        }

        if(Input::get('create_account') == 1){
            \Session::flash('success_msg', 'Your application has now been submitted. To access your new account, please check your email for verification link.');
            return response()->json(['success' => 'Your application has now been submitted. To access your new account, please check your email for verification link.']);
        }else{
            \Session::flash('success_msg', 'Congratulations your application has been successfully submitted. Good luck!');
            return response()->json(['success' => 'Congratulations your application has been successfully submitted. Good luck!']);
        }
    }

    public function uploadResume($id)
    {

        $v = \Validator::make(
            [
                'file' => Input::file('file'),
            ],
            ['file' => 'required|mimes:doc,docx,pdf']
        );


        if ($v->passes()) {
            $file = Input::file('file');
            $filename = $file->getClientOriginalName();
            $folder = 'attachments';
            $extention = $file->getClientOriginalExtension();

            $unique = uniqid();

            $file->move(public_path() . '/uploads/' . $folder . '/', Auth::user()->first_name . Auth::user()->last_name . 'Resume-' . $unique .'.'. $extention);
            $attachment_path = URL::to('/uploads/' . $folder . '/' . Auth::user()->first_name . Auth::user()->last_name . 'Resume-' . $unique .'.'. $extention);


            Users::where('id', $id)->update(['resume' => $attachment_path,'filename' => $filename]);


            \Session::flash(
                'success_msg',
                'Your CV/Resume (' . $filename . ') has been succesfully uploaded.'
            );

            return redirect()->back();
        } else {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput();
        }
    }

    public function registerCustomer()
    {

        $v = \Validator::make(
            [
                'email' => Input::get('email'),
                'first_name' => Input::get('first_name'),
                'last_name' => Input::get('last_name'),
                'state' => Input::get('state'),
                'password' => Input::get('password'),
                'terms_and_conditions' => Input::has('terms_and_conditions'),
                'password_confirmation' => Input::get('password_confirmation'),
            ],
            ['email' => 'required|email|unique:users', 'password' => 'required|confirmed|min:6']
        );

        if ($v->passes()) {

            $customer_group = DB::table('groups')->where('name', 'customer')->first();

            $hash = md5(strtolower(trim(Input::get('email'))));

            $confirmation_code = str_random(30);

            $user = new Users();
            $user->name = Input::get('first_name') . " " . Input::get('last_name');
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->slug = Str::slug(Input::get('name'));
            $user->email = Input::get('email');
            $user->state = Input::get('state');
            $user->password = \Hash::make(Input::get('password'));
            $user->activation_code = $confirmation_code;
            $user->aus_contact_member_no = Input::get('aus_contact_member_no');
            $user->avatar = "/if_profile.png";
            $user->save();
            $user_group = new UsersGroups();
            $user_group->user_id = $user->id;
            $user_group->group_id = $customer_group->id;
            $user_group->save();
            DB::table('activitylog')->insert([
                    ['description' => 'Created Account', 'user_id' => $user->id, 'created_at' => Carbon::now()]
                ]);
            \Mail::send(
                'verify2',
                ['confirmation_code' => $confirmation_code, 'email' => $user->email],
                function ($message) {
                    $message->to(Input::get('email'))
                            ->subject('You’re almost done - Please verify your email address');
                }
            );

            Session::flash('success_msg', 'Thanks for signing up! Please check your email.');

            return redirect()->back();
        } else {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput();
        }
    }

    public function registerUser()
    {
        //var_dump(Input::all());
        $settings_general = Utils::getSettings("general");
        $v = \Validator::make(
            [
                'email' => Input::get('email'),
                'first_name' => Input::get('firstname'),
                'last_name' => Input::get('lastname'),
                'password' => Input::get('password'),
                'terms_and_conditions' => Input::get('terms_and_conditions'),
                'password_confirmation' => Input::get('password_confirmation'),
            ],
            [
                'terms_and_conditions' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed|min:6',
            ]
        );

        if ($v->passes()) {

            $customer_group = DB::table('groups')->where('name', 'customer')->first();

            $hash = md5(strtolower(trim(Input::get('email'))));

            $confirmation_code = str_random(30);

            $user = new Users();
            $user->name = Input::get('firstname') . " " . Input::get('lastname');
            $name = Input::get('firstname');
            $user->slug = Str::slug(Input::get('name'));
            $user->first_name = Input::get('firstname');
            $user->last_name = Input::get('lastname');
            $user->email = Input::get('email');
            $user->state = Input::get('state');
            $user->password = \Hash::make(Input::get('password'));
            $user->aus_contact_member_no = Input::get('aus_contact_member_no');
            $user->activation_code = $confirmation_code;
            $user->avatar = "/if_profile.png";
            $user->save();
            $user_group = new UsersGroups();
            $user_group->user_id = $user->id;
            $user_group->group_id = $customer_group->id;
            $user_group->save();
            DB::table('activitylog')->insert([
                    ['description' => 'Created Account', 'user_id' => $user->id, 'created_at' => Carbon::now()]
                ]);
            \Mail::send(
                'verify2',
                ['confirmation_code' => $confirmation_code, 'name' => $name, 'email' => $user->email],
                function ($message) {
                    $message->to(Input::get('email'))
                            ->subject('You’re almost done - Please verify your email address');
                }
            );

            Session::flash(
                'success_msg',
                'Thanks for joining ItsMyCall. To get started, please check your email for your verification link.' . $settings_general->after_register_poster_conversion
            );

            return redirect('/login/seeker');
        } else {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput();
        }
    }

    public function registerPoster(Request $request)
    {

        ////////USER REGISTRATION
        $settings_general = Utils::getSettings("general");
        $v = \Validator::make(
            $request->all(),
            [
                'company' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'phone' => 'required|regex:/^[\+\d\s]*$/',
                'terms_and_conditions' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed|min:6',
                'advertiser_type' => 'required',
            ]
        );
        $code = Input::get('referral');
        $check = ReferralCode::where('referral_code', $code)->first();
        $date = Carbon::now()->toDateString();
        if (!empty($code)) {
        if (empty($check)) {
                Session::flash(
                    'error_msg',
                    'Sorry, the Registration Code was not recognised. Please try again or contact us on 1300 ITSMYCALL (1300 487 692) or at support@itsmycall.com.au'
                );

                return redirect()->back()->withInput();
        }
        elseif ($check->campaign_end < $date) {
                Session::flash(
                    'error_msg',
                    'Sorry, that Registration Code is no longer valid. Please try again or contact us on 1300 ITSMYCALL (1300 487 692) or at support@itsmycall.com.au'
                );

                return redirect()->back()->withInput();
            }
        elseif ($check->date_begin > $date) {
             Session::flash(
                    'error_msg',
                    'Sorry, the Registration Code you entered has not been activated yet. Please try again or contact us on 1300 ITSMYCALL (1300 487 692) or at support@itsmycall.com.au'
                );

                return redirect()->back()->withInput();
         } 
        } 
        if ($v->passes()) {

            $customer_group = DB::table('groups')->where('name', 'company_admin')->first();

            $hash = md5(strtolower(trim(Input::get('email'))));

            $confirmation_code = str_random(30);

            $user = new Users();
            $user->name = Input::get('firstname') . " " . Input::get('lastname');
            $name = Input::get('firstname');
            if ($check) {
                $user->ref_id = $check->id;
                $user->aus_contact_member_no = Input::get('aus_contact_member_no');
            } else {
                $user->ref_id = '';
                $user->aus_contact_member_no = NULL;
            }

            $user->first_name = Input::get('firstname');
            $user->last_name = Input::get('lastname');
            $user->slug = Str::slug(Input::get($user->name));
            $user->company = Input::get('company');
            $user->email = Input::get('email');
            $user->state = Input::get('state');
            $user->password = \Hash::make(Input::get('password'));
            $user->activation_code = $confirmation_code;
            $user->phone = Input::get('phone');
            $user->avatar = "/if_profile.png";
            $user->advertiser_type = Input::get('advertiser_type');
            $user->save();
            $user_group = new UsersGroups();
            $user_group->user_id = $user->id;
            $user_group->group_id = $customer_group->id;

            $user_group->save();
            DB::table('activitylog')->insert([
                    ['description' => 'Created Account', 'user_id' => $user->id, 'created_at' => Carbon::now()]
                ]);

            \Mail::send(
                'verify',
                ['confirmation_code' => $confirmation_code, 'name' => $name, 'email' => $user->email],
                function ($message) {
                    $message->to(Input::get('email'))
                            ->subject('You’re almost done - Please verify your email address');
                }
            );
            if ($check) {
                $expires = new \Carbon\Carbon($check->date_end);
                $dateend = $expires->toFormattedDateString();
                $now = \Carbon\Carbon::now();
                $difference = ($expires->diff($now)->days);
                $message = 'Thanks for joining ItsMyCall. To get started, please check your email for your verification link.';
                $message2 = 'You have successfully applied the Registration code: '. $check->referral_code.'. The exclusive pricing available with this Registration Code will expire in '.$difference. ' days on '.$dateend.'. After this date prices will return to our standard low rates.';

            } else {
                $message = 'Thanks for joining ItsMyCall. To get started, please check your email for your verification link.';
                $message2 = NULL;
            }

            Session::flash('success_msg', $message);

            return redirect('/login/poster');
        } else {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back()->withInput();
        }
    }

    public function confirm($confirmation_code)
    {
        if (!$confirmation_code) {
            throw new \Exception('invalid confirmation code');
        }

        $user = Users::where('activation_code', $confirmation_code)->first();

        if (!$user) {
           abort(404);
        }

        if ($user && $user->activated) {
            \Session::flash(
                'info_msg',
                'Your email address has already been validated! Please log in below to access your account'
            );

            return redirect('/login?e=' . $user->email);
        }

        $user->activated = 1;
        $user->save();
        //$user = Users::where('activation_code', $confirmation_code)->first();
        if ($user->isPosterPass()) {
            if ($user->ref_id != '') {
                $checkreferral = ReferralCode::where('id', $user->ref_id)->first();
                $referralsubscribe = '94866c94bd';
            } else {
                $referralsubscribe = 'e7db9d5156';
                $checkreferral = false;
            }

            if($user->aus_contact_member_no != ''){
                $auscontactsubscribe = 'cf659c4e9d';
            }else{
                $auscontactsubscribe = '9b02417a70';
            }

            if($user->advertiser_type == 'private_advertiser'){
                $advertisersubscribe = 'd249cc8e59';
            }else{
                $advertisersubscribe = '0d05149478';
            }

            if($user->state == 'Australian Capital Territory'){
                $statesubscribe = 'd28eef602c';
            }else if($user->state == 'New South Wales'){
                $statesubscribe = '1d43831aed';
            }else if($user->state == 'Northern Territory'){
                $statesubscribe = '2b05e5227f';
            }else if($user->state == 'Queensland'){
                $statesubscribe = '2777f7c246';
            }else if($user->state == 'South Australia'){
                $statesubscribe = 'd9195d149b';
            }else if($user->state == 'Tasmania'){
                $statesubscribe = 'e77297997f';
            }else if($user->state == 'Victoria'){
                $statesubscribe = 'dd57954b17';
            }else if($user->state == 'Western Australia'){
                $statesubscribe = 'd77759260d';
            }else{
                $statesubscribe = '53291d3ad2';
            }

            if($checkreferral){
                Newsletter::subscribeOrUpdate(
                    $user->email,
                    [
                        'FNAME' => $user->first_name,
                        'LNAME' => $user->last_name,
                        'COMPANY' => $user->company,
                        'MMERGE4' => $checkreferral->referral_code,
                        'MMERGE6' => \Carbon\Carbon::parse($checkreferral->date_end)->format('m/d/Y')
                    ],
                    'posters', ['interests'=>[$referralsubscribe => true, $auscontactsubscribe => true, $advertisersubscribe => true, $statesubscribe => true]]
                );
            }else{
                Newsletter::subscribeOrUpdate(
                    $user->email,
                    [
                        'FNAME' => $user->first_name,
                        'LNAME' => $user->last_name,
                        'COMPANY' => $user->company
                    ],
                    'posters', ['interests'=>[$referralsubscribe => true, $auscontactsubscribe => true, $advertisersubscribe => true, $statesubscribe => true]]
                );
            }
        } else {
            if($user->state == 'Australian Capital Territory'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['a6b96d0dd0' => true]]
                );
            }else if($user->state == 'New South Wales'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['ce366ba266' => true]]
                );
            }else if($user->state == 'Northern Territory'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['4a8674d560' => true]]
                );
            }else if($user->state == 'Queensland'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['2c35cb8485' => true]]
                );
            }else if($user->state == 'South Australia'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['f070ff9f60' => true]]
                );
            }else if($user->state == 'Tasmania'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['a7a18172b9' => true]]
                );
            }else if($user->state == 'Victoria'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['366876e27c' => true]]
                );
            }else if($user->state == 'Western Australia'){
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['89d2c539e5' => true]]
                );
            }else{
                Newsletter::subscribeOrUpdate(
                $user->email,
                ['FNAME' => $user->first_name, 'LNAME' => $user->last_name],
                'seekers', ['interests' => ['8192edf584' => true]]
                );
            }
        }
        $email = $user->email;
        if($user->isPosterPass()){
            \Session::flash('success_msg', 'Please login below to get started');

            return redirect('/login/poster/verify?e=' . $email);
        }else{
            \Session::flash('success_msg', 'Please login below to get started');

            return redirect('/login/seeker/verify?e=' . $email);
        }

    }

    public function jobList()
    {
        $users = DB::table('users')->get();
        if(Input::get('searchour') == 'yes'){
            if (Input::get('hrsalstart') >= Input::get('hrsalend')) {
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Salary Max value must be greater than Salary Min value.');
                return redirect()->back();
            }
        }else if(Input::get('salstart') != '' &&  Input::get('salend') != '' && Input::get('salstart') >= Input::get('salend')){
                Session::flash('error_msg', '<i class="fa fa-info-circle" aria-hidden="true"></i> Salary Max value must be greater than Salary Min value.');
                return redirect()->back();
            }
        if (!empty(Input::all())) {
            $this->data['filtercount'] = 0;
            $filter = array();
            $filter['title'] = Input::get('title');
            $filter['country'] = Input::get('country');
            $filter['state'] = Input::get('state');
            $filter['category'] = Input::get('category');
            $filter['subcategory'] = Input::get('subcategory');
            $filter['salstart'] = Input::get('salstart');
            $filter['salend'] = Input::get('salend');
            $filter['hrsalstart'] = Input::get('hrsalstart');
            $filter['hrsalend'] = Input::get('hrsalend');
            $filter['employment_type'] = Input::get('employment_type');
            $filter['employment_term'] = Input::get('employment_term');
            $filter['work_from_home'] = Input::get('work_from_home');
            $this->data['work_from_home'] = Input::get('work_from_home');
            $filter['parentsoption'] = Input::get('parentsoption');
            $filter['incentivestructure1'] = Input::get('incentivestructure1');
            $this->data['parentsoption'] = Input::get('parentsoption');
            $filter['location'] = Input::get('joblocation');
            $filter['searchour'] = Input::get('searchour');
            $filter['lat'] = Input::get('lat');
            $filter['lng'] = Input::get('lng');
            $filter['sort'] = Input::get('sort');
            $filter['pay_cycle'] = Input::get('pay_cycle');
            $filter['advertiser_type'] = Input::get('advertiser_type');


            $result = DB::table('keywords')->where('keyword', $filter['title'])->get();
            if (strlen($filter['title']) > 0) {
                if (sizeof($result) == 0) {
                    DB::table('keywords')->insert(['keyword' => $filter['title'], 'count' => 1]);
                    $this->data['filtercount']++;
                } else {
                    $row = DB::table('keywords')->where('keyword', $filter['title'])->first();
                    $this->data['filtercount']++;
                    DB::table('keywords')->where('keyword', $filter['title'])->update(['count' => $row->count + 1]);
                }
            }

            $jobs = DB::table('posts')
                      ->select('posts.*')
                      ->join('users', 'posts.author_id', '=', 'users.id')
                      ->where('users.deleted', false)
                      ->where('posts.status', Posts::STATUS_POSITION_ACTIVE)
                      ->where('posts.expired_at', '>=', Carbon::now());

            if (strlen($filter['title']) > 0) {
                //$jobs = $jobs->where('posts.title', 'LIKE', '%' . $filter['title'] . '%');
                $jobs = $jobs->where(
                    function ($query) use ($filter) {
                        $query->orWhereRaw('LOWER(`posts`.`title`) LIKE ?', ['%' . strtolower($filter['title']) . '%']);
                        $query->orWhereRaw('LOWER(`posts`.`company`) LIKE ?', ['%' . strtolower($filter['title']) . '%']);
                    }
                );
                $this->data['filtercount']++;
            }

            // if (strlen($filter['country']) > 0 && $filter['country'] != -1) {
            // $jobs = $jobs->where('country', 'LIKE', '%' . $filter['country'] . '%');
            // $this->data['filtercount']++;
            // }

            if (strlen($filter['state']) > 0 && $filter['state'] != -1) {
                $jobs = $jobs->where('posts.state', 'LIKE', '%' . $filter['state'] . '%');
                $this->data['filtercount']++;
            }
            if (strlen($filter['category']) > 0 && $filter['category'] != -1) {
                $jobs = $jobs->where('posts.category', $filter['category']);
                $this->data['filtercount']++;
            }
            if (strlen($filter['pay_cycle']) > 0 && $filter['pay_cycle'] != -1) {
                $jobs = $jobs->where('posts.pay_cycle', $filter['pay_cycle']);
                $this->data['filtercount']++;
            }
            if (strlen($filter['subcategory']) > 0 && $filter['subcategory'] != -1) {
                $jobs = $jobs->where('posts.subcategory', $filter['subcategory']);
                $this->data['filtercount']++;
            }
            if ($filter['searchour'] != "yes") {
                if ($filter['salstart'] != 0 && $filter['salend'] != 200000) {
                    if (strlen($filter['salend']) > 3 && $filter['salend'] != -1 && $filter['salend'] > '200000') {
                        $this->data['filtercount']++;
                        if (strlen($filter['salstart']) > 3 && $filter['salstart'] != -1) {
                            $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                        }
                        if (strlen($filter['salend']) > 3 && $filter['salend'] != -1) {
                            $jobs = $jobs->where('posts.salary', '<', $filter['salend']);
                        }
                    } else if (strlen($filter['salend']) > 3 && $filter['salend'] != -1) {
                        $this->data['filtercount']++;
                        $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                        $jobs = $jobs->where('posts.salary', '<', 200000000000000000);
                    }
                }
            } else {
                if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1 && $filter['hrsalend'] > '100') {
                    $this->data['filtercount']++;
                    if (strlen($filter['hrsalstart']) > 0 && $filter['hrsalstart'] != -1) {
                        $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                    }
                    if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1) {
                        $jobs = $jobs->where('posts.hrsalary', '<', $filter['hrsalend']);
                    }
                } else if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1) {
                    $this->data['filtercount']++;
                    $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                    $jobs = $jobs->where('posts.hrsalary', '<', 200000000000000000);
                }
            }
            if ($filter['employment_type']) {
                $this->data['filtercount']++;
                $jobs = $jobs->whereIn('posts.employment_type', $filter['employment_type']);
            }
            if ($filter['employment_term']) {
                $this->data['filtercount']++;
                $jobs = $jobs->whereIn('posts.employment_term', $filter['employment_term']);
            }
            if ($filter['work_from_home'] != '') {
                $this->data['filtercount']++;
                $jobs = $jobs->where('posts.work_from_home', '=', $filter['work_from_home']);
            }

            if ($filter['incentivestructure1'] != '') {
                $this->data['filtercount']++;
                $jobs = $jobs->where('posts.incentivestructure', '=', $filter['incentivestructure1']);
            }

            if ($filter['parentsoption'] != '') {
                $this->data['filtercount']++;
                $jobs = $jobs->where('posts.parentsoption', '=', $filter['parentsoption']);
            }

            if ($filter['advertiser_type'] !== 'both') {
                 $this->data['filtercount']++;
                $jobs = $jobs->where('users.advertiser_type', '=', $filter['advertiser_type']);
            }
            if (strlen($filter['lat']) > 0 && strlen($filter['lng']) > 0) {
                $this->data['filtercount']++;
                $this->data['message'] = 'Jobs Closest to ' . $filter['location'];
                $jobs = $jobs->orderBy(
                    DB::raw('ABS(lat - ' . (float)$filter['lat'] . ') + ABS(lng - ' . (float)$filter['lng'] . ')'),
                    'ASC'
                );
            }

            if (!empty($filter['sort']) && $filter['sort'] == 'date') {
                $jobs = $jobs->orderBy('posts.created_at', 'DESC');
            }

            $jobs = $jobs->paginate(10);
        } else {
            $jobs = Posts::active()->orderBy('created_at', 'DESC')->paginate(10);
        }



        $countries = Countries::get();

        $companies = Posts::groupby('company')->limit(20)->get();

        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();

        $categories = Categories::get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::where('category', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('subcategory', $subcat->id)->count();
            }
        }
        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['countries'] = $countries;
        $this->data['companies'] = $companies;
        $this->data['seodescription'] = '';
        $this->data['keywords'] = $keywords;
        $this->data['users'] = $users;
        //$this->data['keywords'] = $keywords;



        foreach ($this->data['jobs'] as $job) {

            $job->created_at = Carbon::parse($job->created_at);
        }

        return view('jobs', $this->data);

    }

    public function presetsearch($id)
    {
        $users = DB::table('users')->get();
        $preset = DB::table('searchpresets')->where('url',$id)->first();
        if($preset && $preset->status == 1){

        }else{
            return $this->throw404();
        }
        $presetemployment_type = [];
        $presetemployment_term = [];
        if($preset->employment_type != 'null'){
            $presetemployment_type = json_decode($preset->employment_type);
        }
        if($preset->employment_term != 'null'){
            $presetemployment_term = json_decode($preset->employment_term);
        }
            $filter = array();
            $filter['title'] = $preset->title;
            $filter['country'] = $preset->country;
            $filter['state'] = $preset->state;
            $filter['category'] = $preset->category;
            $filter['subcategory'] = $preset->subcategory;
            $filter['salstart'] = $preset->salstart;
            $filter['salend'] = $preset->salend;
            $filter['hrsalstart'] = $preset->hrsalstart;
            $filter['hrsalend'] = $preset->hrsalend;
            $filter['employment_type'] = $presetemployment_type;
            $filter['employment_term'] = $presetemployment_term;
            $filter['work_from_home'] = $preset->work_from_home;
            $this->data['work_from_home'] = $preset->work_from_home;
            $filter['parentsoption'] = $preset->parentsoption;
            $filter['incentivestructure1'] = $preset->incentivestructure1;
            $this->data['parentsoption'] = $preset->parentsoption;
            $filter['location'] = $preset->location;
            $filter['searchour'] = $preset->searchour;
            $filter['lat'] = $preset->lat;
            $filter['lng'] = $preset->lng;
            $filter['sort'] = $preset->sort;
            $filter['pay_cycle'] = $preset->pay_cycle;
            $filter['advertiser_type'] = $preset->advertiser_type;


            $result = DB::table('keywords')->where('keyword', $filter['title'])->get();
            if (strlen($filter['title']) > 0) {
                if (sizeof($result) == 0) {
                    DB::table('keywords')->insert(['keyword' => $filter['title'], 'count' => 1]);
                } else {
                    $row = DB::table('keywords')->where('keyword', $filter['title'])->first();
                    DB::table('keywords')->where('keyword', $filter['title'])->update(['count' => $row->count + 1]);
                }
            }

            $jobs = DB::table('posts')
                      ->select('posts.*')
                      ->join('users', 'posts.author_id', '=', 'users.id')
                      ->where('users.deleted', false)
                      ->where('posts.status', Posts::STATUS_POSITION_ACTIVE)
                      ->where('posts.expired_at', '>=', Carbon::now());

            if (strlen($filter['title']) > 0) {
                //$jobs = $jobs->where('posts.title', 'LIKE', '%' . $filter['title'] . '%');
                $jobs = $jobs->where(
                    function ($query) use ($filter) {
                        $query->orWhereRaw('LOWER(`posts`.`title`) LIKE ?', ['%' . strtolower($filter['title']) . '%']);
                        $query->orWhereRaw('LOWER(`posts`.`company`) LIKE ?', ['%' . strtolower($filter['title']) . '%']);
                    }
                );
            }

            // if (strlen($filter['country']) > 0 && $filter['country'] != -1) {
            // $jobs = $jobs->where('country', 'LIKE', '%' . $filter['country'] . '%');
            // $this->data['filtercount']++;
            // }

            if (strlen($filter['state']) > 0 && $filter['state'] != -1) {
                $jobs = $jobs->where('posts.state', 'LIKE', '%' . $filter['state'] . '%');
            }
            if (strlen($filter['category']) > 0 && $filter['category'] != -1) {
                $jobs = $jobs->where('posts.category', $filter['category']);
            }
            if (strlen($filter['pay_cycle']) > 0 && $filter['pay_cycle'] != -1) {
                $jobs = $jobs->where('posts.pay_cycle', $filter['pay_cycle']);
            }
            if (strlen($filter['subcategory']) > 0 && $filter['subcategory'] != -1) {
                $jobs = $jobs->where('posts.subcategory', $filter['subcategory']);
            }
            if ($filter['searchour'] != "yes") {
                if ($filter['salstart'] != 0 && $filter['salend'] != 200000) {
                    if (strlen($filter['salend']) > 3 && $filter['salend'] != -1 && $filter['salend'] != '200000') {
                        if (strlen($filter['salstart']) > 3 && $filter['salstart'] != -1) {
                            $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                        }
                        if (strlen($filter['salend']) > 3 && $filter['salend'] != -1) {
                            $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                        }
                    } else if (strlen($filter['salend']) > 3 && $filter['salend'] != -1) {
                        $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                        $jobs = $jobs->where('posts.salary', '<', 200000000000000000);
                    }
                }
            } else {
                if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1 && $filter['hrsalend'] != '200000') {
                    if (strlen($filter['hrsalstart']) > 0 && $filter['hrsalstart'] != -1) {
                        $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                    }
                    if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1) {
                        $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                    }
                } else if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1) {
                    $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                    $jobs = $jobs->where('posts.hrsalary', '<', 200000000000000000);
                }
            }
            if ($filter['employment_type']) {
                $jobs = $jobs->whereIn('posts.employment_type', $filter['employment_type']);
            }
            if ($filter['employment_term']) {
                $jobs = $jobs->whereIn('posts.employment_term', $filter['employment_term']);
            }
            if ($filter['work_from_home'] != '') {
                $jobs = $jobs->where('posts.work_from_home', '=', $filter['work_from_home']);
            }

            if ($filter['incentivestructure1'] != '') {
                $jobs = $jobs->where('posts.incentivestructure', '=', $filter['incentivestructure1']);
            }

            if ($filter['parentsoption'] != '') {
                $jobs = $jobs->where('posts.parentsoption', '=', $filter['parentsoption']);
            }

            if ($filter['advertiser_type'] !== 'both') {
                $jobs = $jobs->where('users.advertiser_type', '=', $filter['advertiser_type']);
            }
            if (strlen($filter['lat']) > 0 && strlen($filter['lng']) > 0) {
                $this->data['message'] = 'Jobs Closest to ' . $filter['location'];
                $jobs = $jobs->orderBy(
                    DB::raw('ABS(lat - ' . (float)$filter['lat'] . ') + ABS(lng - ' . (float)$filter['lng'] . ')'),
                    'ASC'
                );
            }

            if (!empty($filter['sort']) && $filter['sort'] == 'date') {
                $jobs = $jobs->orderBy('posts.created_at', 'DESC');
            }

            $jobs = $jobs->paginate(10);



        $countries = Countries::get();

        $companies = Posts::groupby('company')->limit(20)->get();

        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();

        $categories = Categories::get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::where('category', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('subcategory', $subcat->id)->count();
            }
        }
        $keywords = $preset->seokeywords;
        $this->data['filtercount'] = 0;
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['countries'] = $countries;
        $this->data['companies'] = $companies;
        $this->data['seodescription'] = $preset->seodescription;
        $this->data['keywords'] = $keywords;
        $this->data['users'] = $users;
        $this->data['searchheading'] = $preset->heading;
        $this->data['html_notes'] = $preset->html_notes;
        $this->data['page_title'] = $preset->pagetitle;
        $this->data['google_adwards_tracking_code'] = $preset->google_adwards;
        $this->data['facebook_pixel_tracking_code'] = $preset->facebook_pixel;
        //$this->data['keywords'] = $keywords;


        foreach ($this->data['jobs'] as $job) {

            $job->created_at = Carbon::parse($job->created_at);
        }

        return view('jobpreset', $this->data);

    }

    public function categoryFilter($id)
    {   
        $ids = Categories::where('id', $id)->get();
        $id = $ids[0]['attributes']['id'];
        if (sizeof($id) > 0) {
            $jobs = Posts::active()->where('category_id', $id)->orderBy('id', 'desc')->paginate(10);
        } else {
            $jobs = [];
        }

        $categories = Categories::where('id', $id)->get();
        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $keywords = $categories[0]['attributes']['seo_keywords'];
        $seodescription = $categories[0]['attributes']['seo_description'];
        $page_title = $categories[0]['attributes']['page_title'];
        $companies = Posts::groupby('company')->limit(20)->get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->get();
            $category->subcategory = $subcategories;
            $category->count = Posts::active()->where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::active()->where('category_id', $subcat->id)->count();
            }
        }
        $users = Users::all();
        $filter['category'] = $categories;
        $this->data['jobs'] = $jobs;
        $this->data['users'] = $users;
        $this->data['categories'] = $categories;
        $this->data['keywords'] = $keywords;
        $this->data['seodescription'] = $seodescription;
        $this->data['page_title'] = $page_title;
        $this->data['companies'] = $companies;

        return view('jobs', $this->data);
    }

    public function categoryNameFilter($slug)
    {   
        $category = Categories::where('slug', $slug)->get();
        $users = Users::all();
        $id = $category[0]['attributes']['id'];
        //$ids = SubCategories::where('parent_id', $id)->lists('id');
        if (sizeof($id) > 0) {
            $jobs = Posts::active()->where('category_id', $id)->orderBy('id', 'desc')->paginate(10);
        } else {
            $jobs = [];
        }

        $categories = Categories::where('id', $id)->get();
        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $keywords = $categories[0]['attributes']['seo_keywords'];
        $seodescription = $categories[0]['attributes']['seo_description'];
        $page_title = $categories[0]['attributes']['page_title'];
        $companies = Posts::groupby('company')->limit(20)->get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->get();
            $category->subcategory = $subcategories;
            $category->count = Posts::active()->where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::active()->where('category_id', $subcat->id)->count();
            }
        }
        $filter['category'] = $categories;
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['keywords'] = $keywords;
        $this->data['seodescription'] = $seodescription;
        $this->data['page_title'] = $page_title;
        $this->data['companies'] = $companies;
        $this->data['users'] = $users;

        return view('jobs', $this->data);
    }

    public function companyFilter($id)
    {

        $jobs = Posts::active()->where('company', $id)->orderBy('id', 'desc')->paginate(10);
        $categories = Categories::get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::where('category', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('subcategory', $subcat->id)->count();
            }
        }

        $companies = Posts::active()->groupby('company')->limit(20)->get();

        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $users = Users::all();
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['users'] = $users;
        $this->data['seodescription'] = '';
        $this->data['keywords'] = $keywords;
        return view('jobs', $this->data);
    }

    public function keywordFilter($id)
    {
        $keyword = DB::table('keywords')->where('id', $id)->first();
        $jobs = Posts::active()->where('title', 'LIKE', '%' . $keyword->keyword . '%')->orderBy('id', 'desc')->paginate(
            10
        );

        $categories = Categories::where('id', $id)->get();

        $companies = Posts::active()->groupby('company')->limit(20)->get();

        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $users = Users::all();
        $this->data['jobs'] = $jobs;
        $this->data['users'] = $users;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['seodescription'] = '';
        $this->data['keywords'] = $keywords;


        return view('jobs', $this->data);
    }

    public function subcategoryFilter($id)
    {

        $jobs = Posts::active()->where('subcategory', $id)->orderBy('id', 'desc')->paginate(10);

        $categories = Categories::where('id', $id)->get();
        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $companies = Posts::groupby('company')->limit(20)->get();
        $categories = Categories::where('id', $id)->get();
        $subcategory = SubCategories::where('id', $id)->get();
        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $keywords = $subcategory[0]['attributes']['seo_keywords'];
        $seodescription = $subcategory[0]['attributes']['seo_description'];
        $page_title = $subcategory[0]['attributes']['page_title'];
        $companies = Posts::groupby('company')->limit(20)->get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->get();
            $category->subcategory = $subcategories;
            $category->count = Posts::where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('category_id', $subcat->id)->count();
            }
        }
        $users = Users::all();

        $this->data['jobs'] = $jobs;
        $this->data['users'] = $users;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['page_title'] = $page_title;
        $this->data['seodescription'] = $seodescription;


        return view('jobs', $this->data);
    }

    public function subcategoryNameFilter($parentslug, $slug)
    {   
        $subcategory = SubCategories::where('slug', $slug)->get();
        $id = $subcategory['0']['attributes']['id'];
        $jobs = Posts::active()->where('subcategory', $id)->orderBy('id', 'desc')->paginate(10);
        $users = Users::all();
        $categories = Categories::where('id', $id)->get();
        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $companies = Posts::groupby('company')->limit(20)->get();
        $categories = Categories::where('id', $id)->get();
        $subcategory = SubCategories::where('id', $id)->get();
        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $seodescription = $subcategory[0]['attributes']['seo_description'];
        $page_title = $subcategory[0]['attributes']['page_title'];
        $companies = Posts::groupby('company')->limit(20)->get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->get();
            $category->subcategory = $subcategories;
            $category->count = Posts::where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('category_id', $subcat->id)->count();
            }
        }
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['page_title'] = $page_title;
        $this->data['seodescription'] = $seodescription;
        $this->data['users'] = $users;


        return view('jobs', $this->data);
    }

    public function countryFilter($country_name)
    {

        $jobs = Posts::active()->where('country', $country_name)->orderBy('id', 'desc')->paginate(10);

        $categories = Categories::get();

        $companies = Posts::groupby('company')->limit(20)->get();
        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $users = Users::all();
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['seodescription'] = '';
        $this->data['users'] = $users;


        return view('jobs', $this->data);
    }

    public function stateFilter($id)
    {

        $jobs = Posts::active()->where('state', $id)->orderBy('id', 'desc')->paginate(10);
        $categories = Categories::get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->get();
            $category->subcategory = $subcategories;
            $category->count = Posts::where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('category_id', $subcat->id)->count();
            }
        }

        $companies = Posts::groupby('company')->limit(20)->get();
        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $users = Users::all();
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['seodescription'] = '';
        $this->data['users'] = $users;


        return view('jobs', $this->data);
    }

    public function priceFilter($id)
    {
        if ($id == 1) {
            $jobs = Posts::active()->whereBetween('salary', [0, 1000])->orderBy('id', 'desc')->paginate(10);
        }
        if ($id == 2) {
            $jobs = Posts::active()->whereBetween('salary', [1000, 5000])->orderBy('id', 'desc')->paginate(10);
        }
        if ($id == 3) {
            $jobs = Posts::active()->whereBetween('salary', [5000, 10000])->orderBy('id', 'desc')->paginate(10);
        }
        if ($id == 4) {
            $jobs = Posts::active()->whereBetween('salary', [10000, 20000])->orderBy('id', 'desc')->paginate(10);
        }
        if ($id == 5) {
            $jobs = Posts::active()->where('salary', '>', 20000)->orderBy('id', 'desc')->paginate(10);
        }

        $categories = Categories::get();
        $companies = Posts::groupby('company')->limit(20)->get();
        $keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $users = Users::all();
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        $this->data['keywords'] = $keywords;
        $this->data['seodescription'] = '';
        $this->data['users'] = $users;

        return view('jobs', $this->data);
    }

    public function searchJobList()
    {
        $filter = array();
        $filter['title'] = Input::get('title');
        $filter['country'] = Input::get('country');
        $filter['state'] = Input::get('state');
        $filter['category'] = Input::get('category');
        $filter['subcategory'] = Input::get('subcategory');
        $filter['salstart'] = Input::get('salstart');
        $filter['salend'] = Input::get('salend');
        $filter['hrsalstart'] = Input::get('hrsalstart');
        $filter['hrsalend'] = Input::get('hrsalend');
        $filter['employment_type'] = Input::get('employment_type');
        $filter['employment_term'] = Input::get('employment_term');
        $filter['work_from_home'] = Input::get('work_from_home');
        $filter['location'] = Input::get('joblocation');
        $filter['searchour'] = Input::get('searchour');
        $filter['lat'] = Input::get('lat');
        $filter['lng'] = Input::get('lng');
        $filter['sort'] = Input::get('sort');
        $filter['pay_cycle'] = Input::get('pay_cycle');
        $filter['advertiser_type'] = Input::get('advertiser_type');
        $this->data['message'] = '';
        $this->data['filtercount'] = 0;

        $result = DB::table('keywords')->where('keyword', $filter['title'])->get();
        if (strlen($filter['title']) > 0) {
            if (sizeof($result) == 0) {
                DB::table('keywords')->insert(['keyword' => $filter['title'], 'count' => 1]);
                $this->data['filtercount']++;
            } else {
                $row = DB::table('keywords')->where('keyword', $filter['title'])->first();
                $this->data['filtercount']++;
                DB::table('keywords')->where('keyword', $filter['title'])->update(['count' => $row->count + 1]);
            }
        }

        $jobs = DB::table('posts')
                  ->select('posts.*')
                  ->join('users', 'posts.author_id', '=', 'users.id')
                  ->where('users.deleted', false)
                  ->where('posts.status', Posts::STATUS_POSITION_ACTIVE)
                  ->where('posts.expired_at', '>=', Carbon::now());
        $users = DB::table('users')->all();

        if (strlen($filter['title']) > 0) {
            //$jobs = $jobs->where('posts.title', 'LIKE', '%' . $filter['title'] . '%');
            $jobs = $jobs->where(
                function ($query) use ($filter) {
                    $query->orWhereRaw('LOWER(`posts`.`title`) LIKE ?', ['%' . strtolower($filter['title']) . '%']);
                    $query->orWhereRaw('LOWER(`posts`.`company`) LIKE ?', ['%' . strtolower($filter['title']) . '%']);
                }
            );
            $this->data['filtercount']++;
        }

        // if (strlen($filter['country']) > 0 && $filter['country'] != -1) {
        // $jobs = $jobs->where('country', 'LIKE', '%' . $filter['country'] . '%');
        // $this->data['filtercount']++;
        // }

        if (strlen($filter['state']) > 0 && $filter['state'] != -1) {
            $jobs = $jobs->where('posts.state', 'LIKE', '%' . $filter['state'] . '%');
            $this->data['filtercount']++;
        }
        if (strlen($filter['category']) > 0 && $filter['category'] != -1) {
            $jobs = $jobs->where('posts.category', $filter['category']);
            $this->data['filtercount']++;
        }
        if (strlen($filter['pay_cycle']) > 0 && $filter['pay_cycle'] != -1) {
            $jobs = $jobs->where('posts.pay_cycle', $filter['pay_cycle']);
            $this->data['filtercount']++;
        }
        if (strlen($filter['subcategory']) > 0 && $filter['subcategory'] != -1) {
            $jobs = $jobs->where('posts.subcategory', $filter['subcategory']);
            $this->data['filtercount']++;
        }
        if ($filter['searchour'] != "yes") {
            if ($filter['salstart'] != 0 && $filter['salend'] != 200000) {
                if (strlen($filter['salend']) > 3 && $filter['salend'] != -1 && $filter['salend'] != '200000') {
                    $this->data['filtercount']++;
                    if (strlen($filter['salstart']) > 3 && $filter['salstart'] != -1) {
                        $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                    }
                    if (strlen($filter['salend']) > 3 && $filter['salend'] != -1) {
                        $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                    }
                } else if (strlen($filter['salend']) > 3 && $filter['salend'] != -1) {
                    $this->data['filtercount']++;
                    $jobs = $jobs->where('posts.salary', '>', $filter['salstart']);
                    $jobs = $jobs->where('posts.salary', '<', 200000000000000000);
                }
            }
        } else {
            if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1 && $filter['hrsalend'] != '200000') {
                $this->data['filtercount']++;
                if (strlen($filter['hrsalstart']) > 0 && $filter['hrsalstart'] != -1) {
                    $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                }
                if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1) {
                    $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                }
            } else if (strlen($filter['hrsalend']) > 0 && $filter['hrsalend'] != -1) {
                $this->data['filtercount']++;
                $jobs = $jobs->where('posts.hrsalary', '>', $filter['hrsalstart']);
                $jobs = $jobs->where('posts.hrsalary', '<', 200000000000000000);
            }
        }
        if ($filter['employment_type']) {
            $this->data['filtercount']++;
            $jobs = $jobs->whereIn('posts.employment_type', $filter['employment_type']);
        }
        if ($filter['employment_term']) {
            $this->data['filtercount']++;
            $jobs = $jobs->whereIn('posts.employment_term', $filter['employment_term']);
        }
        if ($filter['work_from_home'] == 1) {
            $this->data['filtercount']++;
            $jobs = $jobs->where('posts.work_from_home', '=', $filter['work_from_home']);
        }

        if ($filter['advertiser_type'] !== 'both') {
            $jobs = $jobs->where('users.advertiser_type', '=', $filter['advertiser_type']);
        }
        if (strlen($filter['lat']) > 0 && strlen($filter['lng']) > 0) {
            $this->data['filtercount']++;
            $this->data['message'] = 'Jobs Closest to ' . $filter['location'];
            $jobs = $jobs->orderBy(
                DB::raw('ABS(lat - ' . (float)$filter['lat'] . ') + ABS(lng - ' . (float)$filter['lng'] . ')'),
                'ASC'
            );
        }

        if (!empty($filter['sort']) && $filter['sort'] == 'date') {
            $jobs = $jobs->orderBy('posts.created_at', 'DESC');
        }

        $jobs = $jobs->paginate(10);

        $categories = Categories::get();
        foreach ($categories as $category) {
            $subcategories = SubCategories::where('parent_id', $category->id)->orderBy('title')->get();

            $category->subcategory = $subcategories;
            $category->count = Posts::where('category_id', $category->id)->count();
            foreach ($category->subcategory as $subcat) {
                $subcat->count = Posts::where('category_id', $subcat->id)->count();
            }
        }

        $companies = Posts::groupby('company')->limit(20)->get();
        //$keywords = DB::table('keywords')->orderBy('count', 'desc')->limit(20)->get();
        $this->data['jobs'] = $jobs;
        $this->data['categories'] = $categories;
        $this->data['companies'] = $companies;
        //$this->data['keywords'] = $keywords;
        $this->data['filter'] = $filter;
        $this->data['users'] = $users;

        foreach ($this->data['jobs'] as $job) {

            $job->created_at = Carbon::parse($job->created_at);
        }


        return view('jobs', $this->data);
    }

    public function contact(request $request)
    {
        $v = \Validator::make(
            [
                'email' => Input::get('email'),
                'name' => Input::get('name'),
                'subject' => Input::get('subject'),
                'message' => Input::get('message'),
            ],
            ['name' => 'required', 'email' => 'required|email', 'subject' => 'required', 'message' => 'required']
        );

        if ($v->passes()) {
            $email = Input::get('email');
            $subject = Input::get('subject');


            \Mail::send(
                'emails.contact',
                array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'subject' => $request->get('subject'),
                    'user_message' => $request->get('message'),
                ),
                function ($message)
                use ($email, $subject) {
                    $message->to(env('MAIL_C'))->subject($subject);
                }
            );

            \Session::flash('success_msg', 'Thanks For Contacting us');

            return Redirect()->back();
        } else {
            Session::flash('error_msg', Utils::messages($v));

            return redirect()->back();
        }
    }

    public function getContact()
    {

        return view('contact', $this->data);
    }

    public function terms()
    {
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->terms;

        return view('default-page', $this->data);
    }

    public function adterms()
    {
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->adterms;

        return view('default-page', $this->data);
    }

    public function privacy()
    {
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->privacy;

        return view('default-page', $this->data);
    }

    public function privacycollection()
    {
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->privacycollection;

        return view('default-page', $this->data);
    }

    public function aboutus()
    {

        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->about;
        $this->data['seo_keywords'] = Utils::getSettings(Settings::CATEGORY_SEO)->about_seo_keywords;
        $this->data['seo_description'] = Utils::getSettings(Settings::CATEGORY_SEO)->about_seo_description;
        $this->data['page_title'] = Utils::getSettings(Settings::CATEGORY_SEO)->about_page_title;

        return view('default-page', $this->data);
    }

    public function connect()
    {   $desc2 =  \App\Settings::where('column_key','connect')->first();
        $connect_seo_description =  \App\Settings::where('column_key','connect_seo_description')->first();
        $connect_seo_keywords =  \App\Settings::where('column_key','connect_seo_keywords')->first();
        $connect_page_title =  \App\Settings::where('column_key','connect_page_title')->first();
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] =  $desc2->value_txt;
        $this->data['seo_keywords'] = $connect_seo_keywords->value_string;
        $this->data['seo_description'] = $connect_seo_description->value_string;
        $this->data['page_title'] = $connect_page_title->value_string;
        return view('connect_program', $this->data);
    }

    public function faq()
    {   $desc =  \App\Settings::where('column_key','faq')->first();
        $faq_seo_description =  \App\Settings::where('column_key','faq_seo_description')->first();
        $faq_seo_keywords =  \App\Settings::where('column_key','faq_seo_keywords')->first();
        $faq_page_title =  \App\Settings::where('column_key','faq_page_title')->first();
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] =  $desc->value_txt;
        $this->data['seo_keywords'] = $faq_seo_keywords->value_string;
        $this->data['seo_description'] = $faq_seo_description->value_string;
        $this->data['page_title'] = $faq_page_title->value_string;
        return view('faqpage', $this->data);
    }

    public function connectq()
    {   $desc =  \App\Settings::where('column_key','quality')->first();
        $faq_seo_description =  \App\Settings::where('column_key','quality_seo_description')->first();
        $faq_seo_keywords =  \App\Settings::where('column_key','quality_seo_keywords')->first();
        $faq_page_title =  \App\Settings::where('column_key','quality_page_title')->first();
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] =  $desc->value_txt;
        $this->data['seo_keywords'] = $faq_seo_keywords->value_string;
        $this->data['seo_description'] = $faq_seo_description->value_string;
        $this->data['page_title'] = $faq_page_title->value_string;
        return view('faqpage', $this->data);
    }

    public function contactus(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'firstname' => 'required',
                'lastname' => 'required',
                'company' => 'required',
                'email' => 'required|email',
                'number' => 'required|regex:/(^\+?[0-9 ]+$)+/',
                'type' => 'required',
                'message' => 'required'
            ], [
                'firstname.required' => 'The First name field is required.',
                'lastname.required' => 'The Last name field is required.',
                'company.required' => 'The Company name field is required.',
                'email.required' => 'Your contact email field is required.',
                'email.email' => 'Your contact email field must be a valid email address.',
                'number.required' => 'Your contact number field is required.',
                'number.regex' => 'Your contact number format is invalid.',
                'type.required' => 'The Enquiry type field is required.',
                'message.required' => 'The Message field is required.',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            }
            $settings = Utils::getSettings(Settings::CATEGORY_GENERAL);
            $secret = $settings->contact_recaptcha_secret_key;
            $client = new Client;
            $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'query' => [
                    'secret' => $secret,
                    'response' => $request->input('g-recaptcha-response'),
                    'remoteip' => $request->ip()
                ]
            ]);
            $array = json_decode($response->getBody()->getContents(), 1);
            if (isset($array['success']) && $array['success']) {
                Mail::send('emails.contact-us', [
                    'firstname' => $request->input('firstname'),
                    'lastname' => $request->input('lastname'),
                    'company' => $request->input('company'),
                    'email' => $request->input('email'),
                    'number' => $request->input('number'),
                    'type' => $request->input('type'),
                    'enquiry' => $request->input('message'),
                ], function ($message) use ($request) {
                    $message->from($request->input('email'), $request->input('firstname') . ' ' . $request->input('lastname'));
                    $message->to('support@itsmycall.com.au');
                    $message->subject('Contact Enquiry: ' . $request->input('type'));
                });
                return response()->json(['status' => 'success', 'message' => 'Thanks for submitting your enquiry. We will respond to your enquiry as soon as possible.'], 200);
            }
            return response()->json(['status' => 'failed', 'error' => 'reCAPTCHA validation failed.'], 400);
        }

        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->contactus;
        $this->data['seo_keywords'] = Utils::getSettings(Settings::CATEGORY_SEO)->contact_seo_keywords;
        $this->data['seo_description'] = Utils::getSettings(Settings::CATEGORY_SEO)->contact_seo_description;
        $this->data['page_title'] = Utils::getSettings(Settings::CATEGORY_SEO)->contact_page_title;
        $this->data['recaptcha_site_key'] = Utils::getSettings(Settings::CATEGORY_GENERAL)->contact_recaptcha_site_key;

        return view('contact-us', $this->data);
    }

    public function learnMore()
    {
        $this->data['html'] = '';
        return view('default-page', $this->data);
    }

    public function referral()
    {

        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_GENERAL);
        $this->data['html'] = $this->data['html']->referral;

        return view('default-page', $this->data);
    }

    public function welcome()
    {   
        $this->data['html'] = Utils::getSettings(Settings::CATEGORY_WELCOME);
        $this->data['google_adwards_tracking_code'] = $this->data['html']->google_adwards_tracking_code;
        $this->data['facebook_pixel_tracking_code'] = $this->data['html']->facebook_pixel_tracking_code;
        $this->data['section_one'] = $this->data['html']->section_one;
        $this->data['section_two'] = $this->data['html']->section_two;
        $this->data['section_three'] = $this->data['html']->section_three;
        $this->data['section_four'] = $this->data['html']->section_four;
        $this->data['section_five'] = $this->data['html']->section_five;
        $this->data['testimonial_interval'] = (int) $this->data['html']->testimonial_interval * 1000;

        $this->data['testimonials'] = Testimonials::orderBy('order')->get();

        return view('welcome-page', $this->data);
    }

    public function welcomecampaign($id)
    {
        $page = DB::table('campaign_pages')->where('url',$id)->where('status',1)->first();
        if ($page) {
            $this->data['html'] = Utils::getSettings(Settings::CATEGORY_WELCOME);
            $this->data['seo_keywords'] = $page->seokeywords;
            $this->data['seo_description'] = $page->seodescription;
            $this->data['page_title'] = $page->page_title;
            $this->data['testimonial_interval'] = (int) $this->data['html']->testimonial_interval * 1000;
            $testimonials = DB::table('testimonials')
                    ->whereIn('id', json_decode($page->testimonial))
                    ->get();
            $this->data['testimonials'] = $testimonials;
            $this->data['google_adwards_tracking_code'] = $page->google;
            $this->data['facebook_pixel_tracking_code'] = $page->facebook;
            $this->data['section_one'] = $page->section1;
            $this->data['video'] = $page->video;
            $this->data['autoplay'] = $page->autoplay;
            $this->data['section_two'] = $page->section2;
            $this->data['section_three'] = $page->section3;
            $this->data['section_four'] = $page->section4;
            $this->data['section_five'] = $page->section5;
            $this->data['section_six'] = $page->section6;
            $this->data['section_seven'] = $page->section7;
            $this->data['section_eight'] = $page->section8;
            $this->data['facebook_download'] = $page->facebook_download;
            $this->data['google_download'] = $page->google_download;
            $this->data['facebook_register'] = $page->facebook_register;
            $this->data['google_register'] = $page->google_register;
            return view('campaignpage', $this->data);
        }else{
            return $this->throw404();
        }
    }

    public function download(Request $request)
    {
        $email = $request->input('email');
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $company = $request->input('company');
        $number = $request->input('number');

        $mailChimp = new MailChimp(config('laravel-newsletter.apiKey'));
        $listId = config('laravel-newsletter.lists.advertiser_guide.id');
        $result = $mailChimp->post('lists/' . $listId . '/members', [
            'email_address' => $email,
            'status' => 'pending',
            'merge_fields' => [
                'FNAME' => $firstname,
                'LNAME' => $lastname,
                'COMPNAME' => $company,
                'PNUMBER' => $number
            ]
        ]);

        if (isset($result['status']) && $result['status'] == 400) {
            return response()->json([
                'error' => isset($result['title']) ? $result['title'] : 'Error'
            ], 400);
        }

        return response()->json([
            'message' => '<strong>Success!</strong> Please check your inbox for your copy of our Job Advertisers Guide.'
        ], 200);
    }

    public function faqs()
    {

        return view('faq', $this->data);
    }

    public function page($page_slug)
    {
        $page = Pages::where('slug', $page_slug)->first();

        if (empty($page)) {
            return $this->throw404();
        }

        $page->next = Pages::where("id", ">", $page->id)->first();
        $page->prev = Pages::where("id", "<", $page->id)->orderBy('created_at', 'desc')->first();
        $page->author = Users::where('id', $page->author_id)->first();

        $related_pages = Pages::active()->where('id', '!=', $page->id)->orderBy('created_at', 'desc')->where('description', 'LIKE', '%' . $page->description . '%')->limit(6)->get();

        foreach ($related_pages as $p) {
            $p->author = Users::where('id', $p->author_id)->first();
        }

        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();
        $this->data['ads'][Ads::TYPE_ABOVE_PAGE] = Ads::where('position', Ads::TYPE_ABOVE_PAGE)->orderByRaw(
            "RAND()"
        )->first();
        $this->data['ads'][Ads::TYPE_BELOW_PAGE] = Ads::where('position', Ads::TYPE_BELOW_PAGE)->orderByRaw(
            "RAND()"
        )->first();
        $this->data['page'] = $page;
        $this->data['related_pages'] = $related_pages;

        return view('page', $this->data);
    }

    public function byAuthor($author_slug)
    {

        $author = Users::where('slug', $author_slug)->first();

        if (empty($author)) {
            return $this->throw404();
        }

        $group_id = UsersGroups::where('user_id', $author->id)->pluck('group_id');

        $author->group = Groups::where('id', $group_id)->first();

        $posts = Posts::active()->where('author_id', $author->id)->orderBy('created_at', 'desc')->paginate(15);;

        foreach ($posts as $post) {
            $post->sub_category = SubCategories::where('id', $post->category_id)->first();
            $post->category = Categories::where('id', $post->sub_category->parent_id)->first();
        }

        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();
        $this->data['ads'][Ads::TYPE_BETWEEN_AUTHOR_INDEX] = Ads::where(
            'position',
            Ads::TYPE_BETWEEN_AUTHOR_INDEX
        )->orderByRaw("RAND()")->first();
        $this->data['author'] = $author;
        $this->data['posts'] = $posts;

        return view('author', $this->data);
    }

    public function byCategory($category_slug)
    {

        $category = Categories::where('slug', $category_slug)->first();

        if (empty($category)) {
            return $this->throw404();
        }

        $sub_cat_ids = SubCategories::where('parent_id', $category->id)->lists('id');

        if (sizeof($sub_cat_ids) > 0) {
            $posts = Posts::active()->whereIn('category_id', $sub_cat_ids)->orderBy('created_at', 'desc')->paginate(15);
        } else {
            $posts = [];
        }

        foreach ($posts as $post) {
            $post->sub_category = SubCategories::where('id', $post->category_id)->first();
            $post->category = Categories::where('id', $post->sub_category->parent_id)->first();
            $post->author = Users::where('id', $post->author_id)->first();
        }

        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();
        $this->data['ads'][Ads::TYPE_BETWEEN_CATEGORY_INDEX] = Ads::where(
            'position',
            Ads::TYPE_BETWEEN_CATEGORY_INDEX
        )->orderByRaw("RAND()")->first();
        $this->data['posts'] = $posts;
        $this->data['category'] = $category;

        return view('category', $this->data);
    }

    public function bySearch()
    {

        $search_term = Input::get('search');

        $posts = Posts::active()->where('title', 'LIKE', '%' . $search_term . '%')->where(
            'description',
            'LIKE',
            '%' . $search_term . '%'
        )->orderBy('created_at', 'desc')->orderBy(
            'id',
            'desc'
        )->paginate(
            15
        );

        foreach ($posts as $post) {
            $post->sub_category = SubCategories::where('id', $post->category_id)->first();
            $post->category = Categories::where('id', $post->sub_category->parent_id)->first();
            $post->author = Users::where('id', $post->author_id)->first();
        }

        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();
        $this->data['ads'][Ads::TYPE_BETWEEN_SEARCH_INDEX] = Ads::where(
            'position',
            Ads::TYPE_BETWEEN_SEARCH_INDEX
        )->orderByRaw("RAND()")->first();
        $this->data['posts'] = $posts;
        $this->data['search_term'] = $search_term;

        return view('search', $this->data);
    }

    public function byTag($tag_slug)
    {

        $tag = Tags::where('slug', $tag_slug)->first();

        if (empty($tag)) {
            return $this->throw404();
        }

        $post_ids = PostTags::where('tag_id', $tag->id)->lists('post_id');

        if (sizeof($post_ids) > 0) {
            $posts = Posts::active()->whereIn('id', $post_ids)->orderBy('created_at', 'desc')->paginate(15);
        } else {
            $posts = [];
        }

        foreach ($posts as $post) {
            $post->sub_category = SubCategories::where('id', $post->category_id)->first();
            $post->category = Categories::where('id', $post->sub_category->parent_id)->first();
            $post->author = Users::where('id', $post->author_id)->first();
        }

        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();
        $this->data['ads'][Ads::TYPE_BETWEEN_TAG_INDEX] = Ads::where(
            'position',
            Ads::TYPE_BETWEEN_TAG_INDEX
        )->orderByRaw(
            "RAND()"
        )->first();
        $this->data['posts'] = $posts;
        $this->data['tag'] = $tag;

        return view('tag', $this->data);
    }

    public function bySubCategory($category_slug, $sub_category_slug)
    {

        $sub_category = SubCategories::where('slug', $sub_category_slug)->first();

        if (empty($sub_category)) {
            return $this->throw404();
        }

        $category = Categories::where('id', $sub_category->parent_id)->first();

        if (empty($category)) {
            return $this->throw404();
        }

        $posts = Posts::active()->where('category_id', $sub_category->id)->orderBy('created_at', 'desc')->paginate(15);

        foreach ($posts as $post) {
            $post->author = Users::where('id', $post->author_id)->first();
        }

        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();
        $this->data['ads'][Ads::TYPE_BETWEEN_SUBCATEGORY_INDEX] = Ads::where(
            'position',
            Ads::TYPE_BETWEEN_SUBCATEGORY_INDEX
        )->orderByRaw("RAND()")->first();
        $this->data['posts'] = $posts;
        $this->data['category'] = $category;
        $this->data['sub_category'] = $sub_category;

        return view('sub_category', $this->data);
    }

    public function submitLike()
    {
        $post_id = Input::get('id');
        $type = Input::get('type');

        if ($post_id < 0) {
            Session::flash('error_msg', trans('messages.internal_server_error'));

            return redirect()->back();
        } else {

            $post_rating = PostLikes::where('email', Input::get('email'))->where('post_id', $post_id)->first();

            if ($type == 1 || $type == 0) {

                if (!empty($post_rating)) {
                    $post_rating->rating = $type;
                    $post_rating->approved = 1;
                    $post_rating->save();
                } else {
                    $post_rating = new PostLikes();
                    $post_rating->post_id = $post_id;
                    $post_rating->name = Input::get('name');
                    $post_rating->email = Input::get('email');
                    $post_rating->rating = $type;
                    $post_rating->approved = 1;
                    $post_rating->save();
                }

                Session::flash('success_msg', trans('messages.thanks_for_rating'));

                return redirect()->back();
            }
        }
    }

    public function submitRating()
    {
        $post_id = Input::get('id');

        if (!Input::has('star') || !Input::has('name') || !Input::has('email') || !Input::has('id')) {
            Session::flash('error_msg', trans('messages.all_field_required_to_submit_rating'));

            return redirect()->back();
        } else {

            $post_rating = PostRatings::where('email', Input::get('email'))->where('post_id', $post_id)->first();

            if (!empty($post_rating)) {
                $post_rating->rating = Input::get('star');
                $post_rating->approved = 1;
                $post_rating->save();
            } else {
                $post_rating = new PostRatings();
                $post_rating->post_id = $post_id;
                $post_rating->name = Input::get('name');
                $post_rating->email = Input::get('email');
                $post_rating->rating = Input::get('star');
                $post_rating->approved = 1;
                $post_rating->save();
            }

            Session::flash('success_msg', trans('messages.thanks_for_rating'));

            return redirect()->back();
        }
    }

    public function article($slug) {
        $this->data['ads'][Ads::TYPE_ABOVE_POST] = Ads::where('position', Ads::TYPE_ABOVE_POST)->orderByRaw(
            "RAND()"
        )->first();
        $this->data['ads'][Ads::TYPE_BELOW_POST] = Ads::where('position', Ads::TYPE_BELOW_POST)->orderByRaw(
            "RAND()"
        )->first();
        $this->data['ads'][Ads::TYPE_SIDEBAR] = Ads::where('position', Ads::TYPE_SIDEBAR)->get();

        $post = Posts::where('slug', $slug)->first();

        if (empty($post)) {
            return $this->throw404();
        }

        Posts::where('slug', $slug)->update(['views' => $post->views + 1]);

        $post->author = Users::where('id', $post->author_id)->first();
        $post->sub_category = SubCategories::where('id', $post->category_id)->first();
        $post->category = Categories::where('id', $post->sub_category->parent_id)->first();

        if ($post->rating_box == 1) {
            $all_ratings = PostRatings::orderBy('created_at', 'desc')->where('post_id', $post->id)->where(
                'approved',
                1
            )->lists('rating');

            if (sizeof($all_ratings) > 0) {

                $total = 0;

                foreach ($all_ratings as $rating) {
                    $total = $total + $rating;
                }

                $post->average_rating = (float)($total / sizeof($all_ratings));
                $post->rating_count = sizeof($all_ratings);
            } else {
                $post->average_rating = 0;
                $post->rating_count = 0;
            }
        }

        if ($post->rating_box == 2) {
            $ups = PostLikes::where('post_id', $post->id)->where('rating', 1)->count();
            $downs = PostLikes::where('post_id', $post->id)->where('rating', 0)->count();


            $post->ups = $ups;
            $post->downs = $downs;
        }

        if ($post->render_type == Posts::RENDER_TYPE_GALLERY) {
            $post->gallery = GalleryImage::where('post_id', $post->id)->get();
        }

        $tag_ids = PostTags::where('post_id', $post->id)->get();

        if (sizeof($tag_ids->lists('tag_id')) > 0) {
            $post->tags = Tags::whereIn('id', $tag_ids->lists('tag_id'))->get();
        } else {
            $post->tags = [];
        }

        foreach ($tag_ids as $tag) {
            PostTags::where('post_id', $post->id)->where('tag_id', $tag->id)->update(['views' => $tag->views + 1]);
        }

        foreach ($post->tags as $tag) {
            Tags::where('id', $tag->id)->update(['views' => $tag->views + 1]);
        }

        $post->next = Posts::where("id", ">", $post->id)->first();
        $post->prev = Posts::where("id", "<", $post->id)->orderBy('created_at', 'desc')->first();

        $related_posts = Posts::active()->where('id', '!=', $post->id)->orderBy('created_at', 'desc')->where('description', 'LIKE', '%' . $post->description . '%')->whereIn(
            'render_type',
            [Posts::RENDER_TYPE_IMAGE, Posts::RENDER_TYPE_VIDEO]
        )->limit(6)->get();

        foreach ($related_posts as $p) {
            $p->author = Users::where('id', $p->author_id)->first();
            $p->sub_category = SubCategories::where('id', $p->category_id)->first();
            $p->category = Categories::where('id', $p->sub_category->parent_id)->first();
        }

        $this->data['post'] = $post;

        if ($post->type == Posts::TYPE_SOURCE) {
            $this->data['source'] = App\Sources::where('id', $post->source_id)->first();
        }

        $this->data['related_posts'] = $related_posts;

        return view('article', $this->data);
    }

    public function rss()
    {

        $settings_general = Utils::getSettings("general");
        $settings_seo = Utils::getSettings("seo");

        if ($settings_general->generate_rss_feeds != 1) {
            return $this->throw404();
        }

        $feed = Feed::feed('2.0', 'UTF-8');

        $feed->channel(
            array(
                'title' => $settings_general->site_title,
                'description' => $settings_seo->seo_description,
                'link' => $settings_general->site_url,
            )
        );

        $posts = Posts::active()->orderBy('created_at', 'desc')->get();

        foreach ($posts as $post) {
            $author = Users::where('id', $post->author_id)->first();

            if ($post->type == Posts::TYPE_SOURCE) {
                if ($settings_general->include_sources == 1) {
                    $feed->item(
                        [
                            'title' => $post->title,
                            'description|cdata' => $post->description,
                            'link' => URL::to($post->slug),
                            'guid' => $post->id,
                            'author' => $author->name,
                            'media:content | cdata' => $post->featured_image,
                            'media:text' => $post->title,
                        ]
                    );
                }
            } else {
                $feed->item(
                    [
                        'title' => $post->title,
                        'description|cdata' => $post->description,
                        'link' => URL::to($post->slug),
                        'guid' => $post->id,
                        'author' => $author->name,
                        'media:content | cdata' => $post->featured_image,
                        'media:text' => $post->title,
                    ]
                );
            }
        }

        return response($feed, 200, array('Content-Type' => 'text/xml'));
    }

    public function categoryRss(
        $slug
    ) {
        $settings_general = Utils::getSettings("general");
        $settings_seo = Utils::getSettings("seo");

        if ($settings_general->generate_rss_feeds != 1) {
            return $this->throw404();
        }

        $feed = Feed::feed('2.0', 'UTF-8');

        $feed->channel(
            array(
                'title' => $settings_general->site_title,
                'description' => $settings_seo->seo_description,
                'link' => $settings_general->site_url,
            )
        );

        $category = Categories::where('slug', $slug)->first();

        $sub_ids = SubCategories::where('parent_id', $category->id)->lists('id');

        if (sizeof($sub_ids) > 0) {
            $posts = Posts::active()->orderBy('created_at', 'desc')->whereIn(
                'category_id',
                $sub_ids->toArray()
            )->get();
        } else {
            $posts = [];
        }

        foreach ($posts as $post) {
            $author = Users::where('id', $post->author_id)->first();

            if ($post->type == Posts::TYPE_SOURCE) {
                if ($settings_general->include_sources == 1) {
                    $feed->item(
                        [
                            'title' => $post->title,
                            'description|cdata' => $post->description,
                            'link' => URL::to($post->slug),
                            'guid' => $post->id,
                            'author' => $author->name,
                            'media:content | cdata' => $post->featured_image,
                            'media:text' => $post->title,
                        ]
                    );
                }
            } else {
                $feed->item(
                    [
                        'title' => $post->title,
                        'description|cdata' => $post->description,
                        'link' => URL::to($post->slug),
                        'guid' => $post->id,
                        'author' => $author->name,
                        'media:content | cdata' => $post->featured_image,
                        'media:text' => $post->title,
                    ]
                );
            }
        }

        return response($feed, 200, array('Content-Type' => 'text/xml'));
    }

    public function subCategoryRss(
        $category_slug,
        $subcategory_slug
    ) {
        $settings_general = Utils::getSettings("general");
        $settings_seo = Utils::getSettings("seo");

        if ($settings_general->generate_rss_feeds != 1) {
            return $this->throw404();
        }

        $feed = Feed::feed('2.0', 'UTF-8');

        $feed->channel(
            array(
                'title' => $settings_general->site_title,
                'description' => $settings_seo->seo_description,
                'link' => $settings_general->site_url,
            )
        );

        $subcategory = SubCategories::where('slug', $subcategory_slug)->first();

        if (!empty($subcategory)) {
            $posts = Posts::active()->orderBy('created_at', 'desc')->where(
                'category_id',
                $subcategory->id
            )->get();
        } else {
            $posts = [];
        }

        foreach ($posts as $post) {
            $author = Users::where('id', $post->author_id)->first();

            if ($post->type == Posts::TYPE_SOURCE) {
                if ($settings_general->include_sources == 1) {
                    $feed->item(
                        [
                            'title' => $post->title,
                            'description|cdata' => $post->description,
                            'link' => URL::to($post->slug),
                            'guid' => $post->id,
                            'author' => $author->name,
                            'media:content | cdata' => $post->featured_image,
                            'media:text' => $post->title,
                        ]
                    );
                }
            } else {
                $feed->item(
                    [
                        'title' => $post->title,
                        'description|cdata' => $post->description,
                        'link' => URL::to($post->slug),
                        'guid' => $post->id,
                        'author' => $author->name,
                        'media:content | cdata' => $post->featured_image,
                        'media:text' => $post->title,
                    ]
                );
            }
        }

        return response($feed, 200, array('Content-Type' => 'text/xml'));
    }

    public function sitemap()
    {
        $settings_general = Utils::getSettings("general");

        if ($settings_general->generate_sitemap == 1) {

            // create new sitemap object
            $sitemap = App::make("sitemap");

            // add pages aboutus,referral program and contactus etc
            $aboutus = DB::table('settings')->where('column_key','about')->get();
            foreach ($aboutus as $about) {
            $sitemap->add(
                    URL::to('/') . '/aboutus',
                    $about->updated_at,
                    '1',
                    'weekly',
                    null,
                    $about->value_txt
                );
            }

            $privacy = DB::table('settings')->where('column_key','privacy')->get();
            foreach ($privacy as $privacysingle) {
            $sitemap->add(
                    URL::to('/') . '/privacy',
                    $privacysingle->updated_at,
                    '1',
                    'weekly',
                    null,
                    $privacysingle->value_txt
                );
            }

            $privacycollection = DB::table('settings')->where('column_key','privacycollection')->get();
            foreach ($privacycollection as $privacysingle) {
            $sitemap->add(
                    URL::to('/') . '/terms-conditions',
                    $privacysingle->updated_at,
                    '1',
                    'weekly',
                    null,
                    $privacysingle->value_txt
                );
            }

            $contactus = DB::table('settings')->where('column_key','contactus')->get();
            foreach ($contactus as $contactussingle) {
            $sitemap->add(
                    URL::to('/') . '/contact',
                    $contactussingle->updated_at,
                    '1',
                    'weekly',
                    null,
                    $contactussingle->value_txt
                );
            }

            $referral = DB::table('settings')->where('column_key','referral')->get();
            foreach ($referral as $referralsingle) {
            $sitemap->add(
                    URL::to('/') . '/referral-program',
                    $referralsingle->updated_at,
                    '1',
                    'weekly',
                    null,
                    $referralsingle->value_txt
                );
            }

            // get all posts from db
            $posts = DB::table('posts')->orderBy('created_at', 'desc')->get();

            // add every post to the sitemap
            foreach ($posts as $post) {
                $sitemap->add(URL::to('/') . "/" . $post->slug, $post->updated_at, '1', 'weekly', null, $post->title);
            }

            $pages = DB::table('pages')->orderBy('created_at', 'desc')->get();

            $categories = DB::table('categories')->orderBy('created_at', 'desc')->get();

            // add every category to the sitemap
            foreach ($categories as $category) {

                $sub_categories = SubCategories::where('parent_id', $category->id)->get();

                $sitemap->add(
                    URL::to('/') . "/category/" . $category->slug,
                    $category->updated_at,
                    '1',
                    'weekly',
                    null,
                    $category->title
                );

                foreach ($sub_categories as $sub_category) {
                    $sitemap->add(
                        URL::to('/') . "/category/" . $category->slug . "/" . $sub_category->slug,
                        $category->updated_at,
                        '1',
                        'weekly',
                        null,
                        $category->title
                    );
                }
            }

            return $sitemap->render('xml');
        }
    }

}
