<?php

App::setLocale(env('LOCALE', 'en'));


Route::get('/', 'HomeController@index');
Route::get('/blogs', 'HomeController@allblogs');
Route::get('/jobalertsredirect/{id}', 'HomeController@jobalertsredirect');
Route::get('/appliedjobsredirect/{id}', 'HomeController@appliedjobsredirect');
Route::get('/blogs/{categoryid}/{subcategoryid}', 'HomeController@blogsbycategory');
Route::get('/blogs/{categoryid}/{subcategoryid}/{blogid}', 'HomeController@blogsdetail');
Route::get('/blogs/{categoryid}/{subcategoryid}/sort/{sortid}', 'HomeController@blogssort');
Route::get('/mailtest', 'HomeController@mailtest');
Route::get('/rss.xml', 'HomeController@rss');
Route::get('/sitemap.xml', 'HomeController@sitemap');
Route::get('/login', 'AuthController@getLogin');
Route::get('/login/seeker', 'AuthController@getLoginseeker');
Route::get('/login/seeker/verify', 'AuthController@getLoginseekerverify');
Route::get('/login/poster', 'AuthController@getLoginposter');
Route::get('/login/poster/verify', 'AuthController@getLoginposterverify');
Route::get('/login/alert', 'AuthController@getLoginAlert');
Route::get('/connect/{provider}', 'AuthController@redirectToProvider');
Route::get('/connect/{provider}/callback', 'AuthController@handleProviderCallback');
Route::get('/register', 'AuthController@getRegister');
Route::get('/register-poster', 'AuthController@getRegisterPoster');
Route::get('/register-user', 'AuthController@getRegisterUser');
Route::get('/logout', 'AuthController@getLogout');
Route::post('/login', 'AuthController@postLogin');
Route::get('/job_list', 'HomeController@jobList');
Route::get('/aboutus', 'HomeController@aboutus');
Route::get('/connect-program', 'HomeController@connect');
Route::get('/faq', 'HomeController@faq');
Route::get('/connect-program-advertiser', 'HomeController@connectq');
Route::get('/referral-program', 'HomeController@referral');
Route::get('/welcome', 'HomeController@welcome');
Route::get('/welcome/{id}', 'HomeController@welcomecampaign');
Route::get('/callcentrejobs/{id}', 'HomeController@presetsearch');
Route::post('/download', 'HomeController@download');
Route::get('/terms-conditions', 'HomeController@terms');
Route::get('/advertiser-terms-conditions', 'HomeController@adterms');
Route::get('/privacy', 'HomeController@privacy');
Route::get('/privacy-collection-notice', 'HomeController@privacyCollection');
Route::get('/faqs', 'HomeController@faqs');
Route::get('/category_filter/{id}', 'HomeController@categoryFilter');
Route::get('/category/{slug}', 'HomeController@categoryNameFilter');
Route::get('/sub_category_filter/{id}', 'HomeController@subcategoryFilter');
Route::get('/category/{parentslug}/{slug}', 'HomeController@subcategoryNameFilter');
Route::get('/country_filter/{country_name}', 'HomeController@countryFilter');
Route::post('/job_list', 'HomeController@searchJobList');
Route::post('/job_apply/{id}', 'HomeController@jobApply');
Route::post('/upload_resume/{id}', 'HomeController@uploadResume');
Route::get('/price_filter/{id}', 'HomeController@priceFilter');
Route::get('/filter_company/{id}', 'HomeController@companyFilter');
Route::get('/filter_state/{id}', 'HomeController@stateFilter');
Route::get('/out/{id}', 'HomeController@out');
Route::get('/testmail', 'HomeController@testmail');
Route::get('/filter_keyword/{id}', 'HomeController@keywordFilter');
Route::post('/reset_password', 'AuthController@postReset');
Route::get('/reset_password/{email}/{reset_code}', 'AuthController@getReset');
Route::post('/forgot_password', 'AuthController@getForgotPassword');
Route::get('/forgot-password', 'AuthController@:getForgotPassword');
Route::post('/forgot-password', 'AuthController@postForgotPassword');
Route::get('/register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'HomeController@confirm'
]);

Route::post('/register-user', 'HomeController@registerUser');
Route::post('/register-poster', 'HomeController@registerPoster');


Route::get('/contact', 'HomeController@contactus');
Route::post('/contact', 'HomeController@contactus');
Route::get('/learn-more', 'HomeController@learnMore');



//Error Handler
Route::get('/403', 'HomeController@show403');
Route::get('/404', 'HomeController@show404');
Route::get('/500', 'HomeController@show500');
Route::get('/503', 'HomeController@show503');

//Site Routes
Route::get('/page/{page_slug}', 'HomeController@page');
Route::get('/author/{author_slug}', 'HomeController@byAuthor');
Route::get('/category/{category_slug}', 'HomeController@byCategory');
Route::get('/category/{category_slug}/{sub_category_slug}', 'HomeController@bySubCategory');
Route::get('/tag/{tag_slug}', 'HomeController@byTag');
Route::get('/search', 'HomeController@bySearch');
Route::get('/rss.xml', 'HomeController@rss');
Route::get('/rss/{category_slug}', 'HomeController@categoryRss');
Route::get('/rss/{category_slug}/{sub_category_slug}', 'HomeController@subCategoryRss');
Route::get('/sitemap.xml', 'HomeController@sitemap');

Route::post('/submit_rating', 'HomeController@submitRating');
Route::post('/submit_likes', 'HomeController@submitLike');
Route::post('/checkReferral', 'HomeController@checkReferral');
Route::group(array('namespace' => 'Customer', 'prefix' => 'customer', 'middleware' => 'customer_auth'), function () {
    Route::get('/', 'PostsController@listJobs');
    Route::get('/update/{state}', 'PostsController@mailchimpsignup');
    Route::get('/delete-account', 'PostsController@deleteAccount');
    //Route::post('/{id}', 'PostsController@stripe');
    Route::get('/job_post', 'PostsController@jobpost');
    Route::get('/savejob/{id}', 'PostsController@savejob');
    Route::get('/removesavejob/{id}', 'PostsController@removesavejob');
    Route::get('/savedjobs', 'PostsController@savedjobs');
    Route::get('/appliedjobs', 'PostsController@appliedjobs');
    Route::get('/appliedjobs/{id}', 'PostsController@appliedjobsstatus');
    Route::post('/posts/create', 'PostsController@store');
    Route::get('/change_password', 'PostsController@changePassword');
    Route::post('/change_password', 'PostsController@updatePassword');
    Route::get('/applicants', 'PostsController@getApplicants');
    Route::post('/updateapplied', 'PostsController@updateapplied');
    Route::get('/deleteapplied/{id}', 'PostsController@deleteapplied');
    Route::get('/applicants/{id}', 'PostsController@getApplicantsJob');
    Route::get('/subscriptions', 'PostsController@subscriptions');
    Route::get('/job_alerts', 'PostsController@subscriptions');
    Route::post('/subscriptions/user', 'PostsController@saveSubscriptions');
    Route::post('/inbound-program', 'PostsController@inboundSave');
    Route::get('/inbound-program', 'PostsController@selectProgram');
    Route::get('/connect-program', 'PostsController@selectProgram');
    Route::get('/inbound/standard-profile', 'PostsController@standardProfile');
    Route::get('/connect/basic-profile', 'PostsController@standardProfile');
    Route::get('/inbound/maximum-profile', 'PostsController@maximumProfile');
    Route::get('/connect/advanced-profile', 'PostsController@maximumProfile');
    Route::get('/deletesub/{id}', 'PostsController@deletesub');
    Route::get('/edit-user/', 'PostsController@editUser');
    Route::post('/editsave', 'PostsController@editStore');
    Route::post('/editsavestandard', 'PostsController@editStoreStandard');
    Route::post('/editsavemaximum', 'PostsController@editStoreMaximum');
    Route::get('/inbound/del', 'PostsController@delinbound');
});

Route::group(array('namespace' => 'Poster', 'prefix' => 'poster', 'middleware' => 'customer_auth'), function () {
    Route::get('/', 'PostsController@listJobs');
    Route::get('/delete-account', 'PostsController@deleteAccount');
    Route::get('/inactive', 'PostsController@listJobsInactive');
    Route::get('/expired', 'PostsController@listJobsExpired');
    Route::post('/save_job', 'PostsController@saveJob');
    //Route::post('/{id}', 'PostsController@stripe');
    Route::get('/job_post', 'PostsController@jobpost');
    Route::get('/buy_credits', 'PostsController@buyCredits');
    Route::get('/valuepack/{id}', 'PostsController@valuedetails');
    Route::post('/save_credit', 'PostsController@savecredit');
    Route::get('/pastmail/{id}/{post}', 'PostsController@pastmail');
    Route::post('/emailpreview', 'PostsController@previewmail');
    Route::post('/emailpreviewjob', 'PostsController@previewmailjob');
    Route::get('/resendmail/{id}', 'PostsController@resendmail');
    Route::get('/resendmailpreview/{id}', 'PostsController@resendmailpreview');
    Route::post('/applicantmail', 'PostsController@applicantmail');
    Route::post('/applicantmailjob', 'PostsController@applicantmailjob');
    Route::get('/applicants/active/{id}', 'PostsController@applicantactive');
    Route::post('/posts/create', 'PostsController@store');
    Route::post('/uploadimg', 'PostsController@uploadimg');
    Route::post('/uploadfiles', 'PostsController@uploadfiles');
    Route::post('/uploadimgredactor', 'PostsController@uploadimgredactor');
    Route::get('/change_password', 'PostsController@changePassword');
    Route::post('/change_password', 'PostsController@updatePassword');
    Route::post('/myaccount', 'PostsController@myaccount');
    Route::get('/applicants', 'PostsController@getApplicants');
    Route::get('/applicants/filterstillactive/{id}', 'PostsController@filterstillactive'); 
    Route::get('/applicants/filterstillactive/{filter}/status/{status}', 'PostsController@filterstatus');
    Route::get('/applicants/{id}/filterstillactive/{filter}', 'PostsController@filterjobstillactive');
    Route::get('/applicants/{id}', 'PostsController@getjobfilter');
    Route::get('/applicants/{id}/filterstillactive/{filter}/status/{status}', 'PostsController@filterstatusjob');
    Route::get('/boostdetails/{id}', 'PostsController@boostdetails');
    Route::get('/transaction', 'PostsController@transactiondetails');
    Route::get('/edit/{id}', 'PostsController@edit');
    Route::get('/activesearch', 'PostsController@activesearch');
    Route::get('/inactivesearch', 'PostsController@inactivesearch');
    Route::get('/expiredsearch', 'PostsController@expiredsearch');
    Route::get('/active/{id}', 'PostsController@sortactive');
    Route::get('/inactive/{id}', 'PostsController@sortinactive');
    Route::get('/expired/{id}', 'PostsController@sortexpired');
    Route::post('/changejobstatus/{id}', 'PostsController@changejobstatus');
    Route::post('/changejobstatusinactive/{id}', 'PostsController@changejobstatusinactive');
    Route::post('/changejobstatusexpired/{id}', 'PostsController@changejobstatusexpired');
    Route::get('/edit-poster', 'PostsController@editPoster');
    Route::get('/deleteimage/{id}', 'PostsController@deleteimage');
    Route::post('/editsave', 'PostsController@editStore');
    Route::post('/edit-post', 'PostsController@editPost');
    Route::post('/updateApplicant', 'PostsController@updateApplicant');
    Route::post('/checkCoupon', 'PostsController@checkCoupon');
    Route::post('/addquestion', 'PostsController@addquestion');
    Route::post('/removequestion', 'PostsController@removequestion');
    Route::post('/edit_question', 'PostsController@edit_question');
    Route::get('/checkCouponpac', 'PostsController@checkCouponpac');
    Route::post('/checkRegCoupon', 'PostsController@checkRegCoupon');
});

//Admin Routes
Route::group(array('namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'admin_auth'), function () {

    Route::get('/', 'DashboardController@index');
    Route::get('/reports', 'DashboardController@reports');
    Route::get('/reports/get', 'DashboardController@getreports');

    Route::get('/update_application', 'DashboardController@updateApplication');

    Route::get('/give-me-write-access', 'DashboardController@giveMeWriteAccess');
    Route::get('/remove-write-access', 'DashboardController@removeWriteAccess');




    Route::group(array('prefix' => 'crons'), function () {

        Route::get('/', 'CronController@all');
        Route::get('/all', 'CronController@all');
        Route::get('/run', 'CronController@run');
        Route::get('/expire-posts', 'CronController@expirePosts');
        Route::get('/view/{id}', 'CronController@view')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'CronController@delete')->where(array('id' => '[0-9]+'));
        ;
    });

    Route::group(array('prefix' => 'roles'), function () {

        Route::get('/', 'UserRolesController@all');
        Route::get('/all', 'UserRolesController@all');

        Route::get('/create', 'UserRolesController@create');
        Route::get('/edit/{id}', 'UserRolesController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'UserRolesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'UserRolesController@store');
        Route::post('/update', 'UserRolesController@update');
    });

    Route::group(array('prefix' => 'companies'), function () {

        Route::get('/', 'CompanyController@all');
        Route::get('/all', 'CompanyController@all');

        Route::get('/create', 'CompanyController@create');
        Route::get('/edit/{id}', 'CompanyController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'CompanyController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'CompanyController@store');
        Route::post('/update', 'CompanyController@update');
    });


    Route::group(array('prefix' => 'users'), function () {

        Route::get('/', 'UsersController@all');
        Route::get('/applicants', 'UsersController@applicants');
        Route::get('/applicants_status', 'UsersController@applicantsStatus');

        Route::get('/inboundimages', 'UsersController@inboundimages');
        Route::get('/inboundimages/create', 'UsersController@inboundimagescreate');
        Route::post('/inboundimages/createvid', 'UsersController@inboundimagescreatevid');
        Route::get('/inboundimages/{id}', 'UsersController@inboundimagesdelete');
        Route::post('/inboundimages/create', 'UsersController@inboundimagessave');

        Route::get('/applied_status', 'UsersController@appliedstatus');

        Route::post('/applied_status/create', 'UsersController@appliedstatusCreate');
        Route::get('/applied_status/view', 'UsersController@appliedstatusview');
        Route::get('/applied_status/{id}', 'UsersController@appliedstatusdelete');
        Route::get('/applied_status/edit/{id}', 'UsersController@applicantsStatusEditCustomer');
        Route::post('/applied_status/update', 'UsersController@applicantsStatusUpdateCustomer');

        Route::get('/applicants_status/view', 'UsersController@applicantsStatusView');
        Route::post('/applicants_status/create', 'UsersController@applicantsStatusCreate');

        Route::get('/applicants_status/{id}', 'UsersController@applicantsStatusEdit');
        Route::post('/applicants_status/{id}', 'UsersController@applicantsStatusEdit');
        Route::get('/auscontact', 'UsersController@auscontact');
        Route::get('/all', 'UsersController@all');
        Route::get('/transferhistory', 'UsersController@transferhistory');
        Route::get('/creditshistory', 'UsersController@creditshistory');
        Route::get('/transferbalance', 'UsersController@transferbalance');
        Route::post('/transferbalance', 'UsersController@transferbalancestore');
        Route::get('/credits', 'UsersController@addcredits');
        Route::post('/credits', 'UsersController@addcreditsstore');
        Route::post('/creditsuser', 'UsersController@addcreditsstoreuser');
        Route::get('/inbound', 'UsersController@inbound');
        Route::get('/connect', 'UsersController@inbound');
        Route::get('/withreferral/{id}', 'UsersController@withreferral');
        Route::get('/applicant/{id}/delete', 'UsersController@deleteApplicant');
        Route::get('/create', 'UsersController@create');
        Route::get('/edit/{id}', 'UsersController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'UsersController@delete')->where(array('id' => '[0-9]+'));

        Route::post('/create', 'UsersController@store');
        Route::post('/update', 'UsersController@update');
        Route::get('/{id}', 'UsersController@user');
    });

    Route::group(array('prefix' => 'packages_stats'), function () {

        Route::get('/', 'PackagesStatsController@allnew');

        Route::get('/packageinfo', 'PackagesStatsController@allnew');

        Route::get('/packageinfo/edit/{id}', 'PackagesStatsController@editpackage');

        Route::get('/edit/{id}', 'PackagesStatsController@edit')->where(array('id' => '[0-9]+'));
        Route::post('/update', 'PackagesStatsController@update');
        Route::post('/setstatus/{id}', 'PackagesStatsController@setstatus');
    });
    Route::group(array('prefix' => 'boost_manage'), function () {

        Route::get('/all', 'UpgradeInfoController@boosts');
        Route::get('/list/{id}', 'UpgradeInfoController@boostslist');
        Route::get('/list/add/{id}', 'UpgradeInfoController@createUpgrade');
        Route::post('/list/add/{id}', 'UpgradeInfoController@createUpgrade');
        Route::get('/list/edit/{id}', 'UpgradeInfoController@boostedit');
        Route::post('/list/edit/{id}', 'UpgradeInfoController@boostedit');
        Route::get('/list/delete/{id}/{job_id}', 'UpgradeInfoController@boostdelete');
    });

    Route::group(array('prefix' => 'categories'), function () {

        Route::get('/', 'CategoryController@all');
        Route::get('/all', 'CategoryController@all');

        Route::get('/create', 'CategoryController@create');
        Route::get('/edit/{id}', 'CategoryController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'CategoryController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'CategoryController@store');
        Route::post('/update', 'CategoryController@update');
    });

    Route::group(array('prefix' => 'sub_categories'), function () {

        Route::get('/', 'SubCategoryController@all');
        Route::get('/all', 'SubCategoryController@all');

        Route::get('/create', 'SubCategoryController@create');
        Route::get('/edit/{id}', 'SubCategoryController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'SubCategoryController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'SubCategoryController@store');
        Route::post('/update', 'SubCategoryController@update');
    });

    Route::group(array('prefix' => 'sources'), function () {

        Route::get('/', 'SourcesController@all');
        Route::get('/all', 'SourcesController@all');
        Route::get('/pull_feeds', 'SourcesController@pullFeeds');
        Route::get('/pull_page', 'SourcesController@pullPages');

        Route::get('/create', 'SourcesController@create');
        Route::get('/edit/{id}', 'SourcesController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'SourcesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'SourcesController@store');
        Route::post('/update', 'SourcesController@update');
    });

    Route::group(array('prefix' => 'posts'), function () {

        Route::get('/jobsearch', 'PostsController@jobsearch');
        Route::get('/jobsearch/create', 'PostsController@jobsearchcreate');
        Route::post('/jobsearch/create', 'PostsController@jobsearchsave');
        Route::get('/jobsearch/delete/{id}', 'PostsController@jobsearchdelete');
        Route::get('/jobsearch/edit/{id}', 'PostsController@jobsearchedit');
        Route::post('/jobsearch/edit/{id}', 'PostsController@jobsearchupdate');

        Route::get('/', 'PostsController@all');
        Route::get('/changelog', 'PostsController@changelog');
        Route::get('/errorsall', 'PostsController@errors');
        Route::get('/errors/create', 'PostsController@errorscreate');
        Route::post('/errors/store', 'PostsController@errorsstore');
        Route::post('/errors/update', 'PostsController@errorsupdate');
        Route::get('/errors/edit/{id}', 'PostsController@errorsedit');
        Route::get('/errors/delete/{id}', 'PostsController@errorsdelete');
        Route::get('/userchangelog/{id}', 'PostsController@userchangelog');
        Route::get('/postchangelog/{id}', 'PostsController@postchangelog');

        Route::post('/questions_pricing/create', 'PostsController@questions_pricing');
        Route::get('/questions_pricing/view', 'PostsController@questions_pricing_view');
        Route::get('/questions_pricing/all', 'PostsController@questions_pricing_all');
        Route::get('/questions_pricing/delete/{id}', 'PostsController@questions_pricing_delete');
        Route::get('/questions_pricing/edit/{id}', 'PostsController@questions_pricing_edit');
        Route::post('/questions_pricing/update', 'PostsController@questions_pricing_update');


        Route::get('/questions', 'PostsController@questions');
        Route::get('/questions/create', 'PostsController@questionscreate');
        Route::post('/questions/create', 'PostsController@questionssave');
        Route::get('/questions/edit/{id}', 'PostsController@questionsedit');
        Route::post('/questions/edit/{id}', 'PostsController@questionsupdate');
        Route::get('/questions/delete/{id}', 'PostsController@questionsdelete');
        Route::post('/addquestion', 'PostsController@addquestion');
        Route::post('/removequestion', 'PostsController@removequestion');
        Route::post('/edit_question', 'PostsController@edit_question');
        Route::get('/all', 'PostsController@all');
        Route::get('/unpaid', 'PostsController@unpaid');
        Route::get('/withcoupon/{id}', 'PostsController@withcoupon');
        Route::get('/withreferral/{id}', 'PostsController@withreferral');
        Route::get('/auscontact', 'PostsController@auscontact');
        Route::get('/create', 'PostsController@create');
        Route::get('/deleteimage/{id}', 'PostsController@deleteimage');
        Route::get('/create-upgrade/{id}', 'PostsController@createUpgrade');
        Route::get('/boosts/add/{id}', 'PostsController@createUpgrade');
        Route::post('/create-upgrade/{id}', 'PostsController@createUpgrade');
        Route::post('/boosts/add/{id}', 'PostsController@createUpgrade');
        Route::get('/edit/{id}', 'PostsController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'PostsController@delete')->where(array('id' => '[0-9]+'));

        Route::post('/create', 'PostsController@store');
        Route::post('/update', 'PostsController@update');
    });

    Route::group(array('prefix' => 'ratings'), function () {
        Route::get('/', 'RatingsController@all');
        Route::get('/all', 'RatingsController@all');
        Route::get('/delete/{id}', 'RatingsController@delete')->where(array('id' => '[0-9]+'));
    });

    Route::group(array('prefix' => 'tags'), function () {
        Route::get('/', 'TagsController@all');
        Route::get('/all', 'TagsController@all');
        Route::get('/delete/{id}', 'TagsController@delete')->where(array('id' => '[0-9]+'));
    });

    Route::group(array('prefix' => 'pages'), function () {

        Route::get('/blogs', 'PagesController@blogs');
        Route::get('/blogtext', 'PagesController@blogtext');
        Route::get('/blogs/create', 'PagesController@blogscreate');
        Route::post('/blogs/create', 'PagesController@blogsstore');
        Route::get('/blogs/delete/{id}', 'PagesController@blogsdelete');
        Route::get('/blogs/edit/{id}', 'PagesController@blogsedit');
        Route::post('/blogs/edit/{id}', 'PagesController@blogsupdate');

        Route::get('/blogs/categories', 'PagesController@categories');
        Route::get('/blogs/categories/create', 'PagesController@categoriescreate');
        Route::post('/blogs/categories/create', 'PagesController@categoriesstore');
        Route::get('/blogs/categories/delete/{id}', 'PagesController@categoriesdelete');
        Route::get('/blogs/categories/edit/{id}', 'PagesController@categoriesedit');
        Route::post('/blogs/categories/edit/{id}', 'PagesController@categoriesupdate');
        Route::post('/blogs/categories/create', 'PagesController@categoriesstore');

        Route::get('/blogs/subcategories', 'PagesController@subcategories');
        Route::get('/blogs/subcategories/create', 'PagesController@subcategoriescreate');
        Route::post('/blogs/subcategories/create', 'PagesController@subcategoriesstore');
        Route::get('/blogs/subcategories/delete/{id}', 'PagesController@subcategoriesdelete');
        Route::get('/blogs/subcategories/edit/{id}', 'PagesController@subcategoriesedit');
        Route::post('/blogs/subcategories/edit/{id}', 'PagesController@subcategoriesupdate');

        Route::get('/', 'PagesController@all');
        Route::get('/testimonial', 'PagesController@testimonial');
        Route::get('/welcome', 'PagesController@welcome');
        Route::get('/all', 'PagesController@all');
        Route::get('/redactor/images.json', 'DashboardController@redactorImages');
        Route::post('redactor', 'DashboardController@uploadimgredactor');
        Route::get('/create', 'PagesController@create');
        Route::get('/edit/{id}', 'PagesController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/duplicate/{id}', 'PagesController@duplicate');
        Route::get('/delete/{id}', 'PagesController@delete')->where(array('id' => '[0-9]+'));

        Route::post('/create', 'PagesController@store');
        Route::post('/update', 'PagesController@update');
    });

    Route::group(array('prefix' => 'ads'), function () {

        Route::get('/', 'AdsController@all');
        Route::get('/all', 'AdsController@all');

        Route::get('/create', 'AdsController@create');
        Route::get('/edit/{id}', 'AdsController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'AdsController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'AdsController@store');
        Route::post('/update', 'AdsController@update');
    });

    Route::group(array('prefix' => 'packages'), function () {

        Route::get('/', 'PackagesController@all');
        Route::get('/all', 'PackagesController@all');

        Route::get('/create', 'PackagesController@create');
        Route::get('/edit/{id}', 'PackagesController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'PackagesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'PackagesController@store');
        Route::post('/update', 'PackagesController@update');
    });

    Route::group(array('prefix' => 'coupons'), function () {

        Route::get('/', 'CouponsController@all');
        Route::get('/all', 'CouponsController@all');

        Route::get('/create', 'CouponsController@create');
        Route::get('/edit/{id}', 'CouponsController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'CouponsController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'CouponsController@store');
        Route::post('/update', 'CouponsController@update');
    });


    Route::group(array('prefix' => 'transactions'), function () {

        Route::get('/', 'TransactionsController@all');
        Route::get('/auscontact', 'TransactionsController@auscontact');
        Route::get('/{id}', 'TransactionsController@byuser');
        Route::get('/post/{id}', 'TransactionsController@bypost');
        Route::get('/credit/{id}', 'TransactionsController@credit');
        Route::get('/welcome/{id}', 'TransactionsController@welcome');
        Route::get('/creditreason/{id}', 'TransactionsController@creditreason');
        Route::get('/all', 'TransactionsController@all');
        Route::get('/search/auscontact', 'TransactionsController@search');

        Route::get('/create', 'TransactionsController@create');
        Route::get('/edit/{id}', 'TransactionsController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'TransactionsController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'TransactionsController@store');
        Route::post('/update', 'TransactionsController@update');
    });

    Route::group(array('prefix' => 'referralcode'), function () {

        Route::get('/', 'ReferralCodeController@all');
        Route::get('/all', 'ReferralCodeController@all');

        Route::get('/create', 'ReferralCodeController@create');
        Route::get('/edit/{id}', 'ReferralCodeController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'ReferralCodeController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'ReferralCodeController@store');
        Route::post('/update', 'ReferralCodeController@update');
    });

    Route::group(array('prefix' => 'refferalcode'), function () {

        Route::get('/', 'RefferalCodeController@all');
        Route::get('/all', 'RefferalCodeController@all');

        Route::get('/create', 'RefferalCodeController@create');
        Route::get('/edit/{id}', 'RefferalCodeController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'RefferalCodeController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'RefferalCodeController@store');
        Route::post('/update', 'RefferalCodeController@update');
    });


    Route::group(array('prefix' => 'upgrades'), function () {

        Route::get('/', 'UpgradesController@all');
        Route::get('/all', 'UpgradesController@all');
        Route::get('/deleteimage/{id}', 'UpgradesController@deleteimage');

        Route::get('/create', 'UpgradesController@create');
        Route::get('/edit/{id}', 'UpgradesController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'UpgradesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'UpgradesController@store');
        Route::post('/update', 'UpgradesController@update');
    });

        Route::group(array('prefix' => 'email'), function () {

        Route::get('/all', 'EmailCategoriesController@index');

        Route::get('/create', 'EmailCategoriesController@create');
        Route::get('/edit/{id}', 'EmailCategoriesController@edit');
        ;
        Route::get('/delete/{id}', 'EmailCategoriesController@destroy');
        ;

        Route::post('/store', 'EmailCategoriesController@store');
        Route::post('/update', 'EmailCategoriesController@update');
    });

    Route::group(array('prefix' => 'post_types'), function () {

        Route::get('/', 'PostTypesController@all');
        Route::get('/all', 'PostTypesController@all');

        Route::get('/create', 'PostTypesController@create');
        Route::get('/edit/{id}', 'PostTypesController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'PostTypesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'PostTypesController@store');
        Route::post('/update', 'PostTypesController@update');
    });

    Route::group(array('prefix' => 'job_alerts'), function () {

        Route::get('/', 'JobAlertsController@all');
        Route::get('/all', 'JobAlertsController@all');

        Route::get('/create', 'JobAlertsController@create');
        Route::get('/edit/{id}', 'JobAlertsController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'JobAlertsController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'JobAlertsController@store');
        Route::post('/update', 'JobAlertsController@update');
    });


    Route::group(array('prefix' => 'statistics'), function () {

        Route::get('/', 'StatisticsController@all');
        Route::get('/all', 'StatisticsController@all');
    });

    Route::group(array('prefix' => 'settings'), function () {

        Route::get('/', 'SettingsController@all');
        Route::get('/all', 'SettingsController@all');

        Route::post('blog_text', 'SettingsController@blog_text');
        Route::post('advetisingtabs', 'SettingsController@advetisingtabs');
        Route::post('iconsinfo', 'SettingsController@iconsinfo');
        Route::post('update_custom_css', 'SettingsController@updateCustomCSS');
        Route::post('update_custom_js', 'SettingsController@updateCustomJS');
        Route::post('update_live_js', 'SettingsController@updateLiveJS');
        Route::post('update_social', 'SettingsController@updateSocial');
        Route::post('update_comments', 'SettingsController@updateComments');
        Route::post('update_seo', 'SettingsController@updateSEO');
        Route::post('update_general', 'SettingsController@updateGeneral');
        Route::get('update_auscontact', 'SettingsController@update_auscontact');
        Route::post('update_terms', 'SettingsController@updateGeneralTerms');
        Route::post('update_adterms', 'SettingsController@updateGeneralAdTerms');
        Route::post('update_about', 'SettingsController@updateAbout');
        Route::post('update_faq', 'SettingsController@updatefaq');
        Route::post('update_quality', 'SettingsController@updatequality');
        Route::post('homestatus', 'SettingsController@homestatus');
        Route::post('development', 'SettingsController@development');
        Route::post('loginpage', 'SettingsController@loginpage');
        Route::post('update_connect', 'SettingsController@updateconnect');
        Route::post('update_privacy', 'SettingsController@updateGeneralPrivacy');
        Route::post('update_privacy_collection', 'SettingsController@updateGeneralPrivacyCollection');
        Route::post('update_referral', 'SettingsController@updateReferral');
        Route::post('update_welcome', 'SettingsController@updateWelcome');
        Route::post('update_contact', 'SettingsController@updateContact');
        Route::post('update_testimonials', 'SettingsController@updateTestimonials');
        Route::delete('delete_testimonial', 'SettingsController@deleteTestimonial');
        Route::get('/redactor/images.json', 'DashboardController@redactorImages');
        Route::post('redactor', 'DashboardController@uploadimgredactor');
    });
});


//Poster Routes
Route::group(array('namespace' => 'Cadmin', 'prefix' => 'cadmin', 'middleware' => 'admin_auth'), function () {

    Route::get('/', 'DashboardController@index');

    Route::get('/update_application', 'DashboardController@updateApplication');

    Route::get('/give-me-write-access', 'DashboardController@giveMeWriteAccess');
    Route::get('/remove-write-access', 'DashboardController@removeWriteAccess');





    Route::group(array('prefix' => 'crons'), function () {

        Route::get('/', 'CronController@all');
        Route::get('/all', 'CronController@all');
        Route::get('/run', 'CronController@run');

        Route::get('/view/{id}', 'CronController@view')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'CronController@delete')->where(array('id' => '[0-9]+'));
        ;
    });

    Route::group(array('prefix' => 'roles'), function () {

        Route::get('/', 'UserRolesController@all');
        Route::get('/all', 'UserRolesController@all');

        Route::get('/create', 'UserRolesController@create');
        Route::get('/edit/{id}', 'UserRolesController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'UserRolesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'UserRolesController@store');
        Route::post('/update', 'UserRolesController@update');
    });



    Route::group(array('prefix' => 'users'), function () {

        Route::get('/', 'UsersController@all');
        Route::get('/all', 'UsersController@all');

        Route::get('/create', 'UsersController@create');
        Route::get('/edit/{id}', 'UsersController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'UsersController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'UsersController@store');
        Route::post('/update', 'UsersController@update');
    });

    Route::group(array('prefix' => 'categories'), function () {

        Route::get('/', 'CategoryController@all');
        Route::get('/all', 'CategoryController@all');

        Route::get('/create', 'CategoryController@create');
        Route::get('/edit/{id}', 'CategoryController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'CategoryController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'CategoryController@store');
        Route::post('/update', 'CategoryController@update');
    });

    Route::group(array('prefix' => 'sub_categories'), function () {

        Route::get('/', 'SubCategoryController@all');
        Route::get('/all', 'SubCategoryController@all');

        Route::get('/create', 'SubCategoryController@create');
        Route::get('/edit/{id}', 'SubCategoryController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'SubCategoryController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'SubCategoryController@store');
        Route::post('/update', 'SubCategoryController@update');
    });

    Route::group(array('prefix' => 'sources'), function () {

        Route::get('/', 'SourcesController@all');
        Route::get('/all', 'SourcesController@all');
        Route::get('/pull_feeds', 'SourcesController@pullFeeds');
        Route::get('/pull_page', 'SourcesController@pullPages');

        Route::get('/create', 'SourcesController@create');
        Route::get('/edit/{id}', 'SourcesController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'SourcesController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'SourcesController@store');
        Route::post('/update', 'SourcesController@update');
    });

    Route::group(array('prefix' => 'posts'), function () {

        Route::get('/', 'PostsController@all');
        Route::get('/all', 'PostsController@all');

        Route::get('/create', 'PostsController@create');
        Route::get('/edit/{id}', 'PostsController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'PostsController@delete')->where(array('id' => '[0-9]+'));

        Route::post('/create', 'PostsController@store');
        Route::post('/update', 'PostsController@update');
    });

    Route::group(array('prefix' => 'ratings'), function () {
        Route::get('/', 'RatingsController@all');
        Route::get('/all', 'RatingsController@all');
        Route::get('/delete/{id}', 'RatingsController@delete')->where(array('id' => '[0-9]+'));
    });

    Route::group(array('prefix' => 'tags'), function () {
        Route::get('/', 'TagsController@all');
        Route::get('/all', 'TagsController@all');
        Route::get('/delete/{id}', 'TagsController@delete')->where(array('id' => '[0-9]+'));
    });

    Route::group(array('prefix' => 'pages'), function () {

        Route::get('/', 'PagesController@all');
        Route::get('/all', 'PagesController@all');

        Route::get('/create', 'PagesController@create');
        Route::get('/edit/{id}', 'PagesController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'PagesController@delete')->where(array('id' => '[0-9]+'));

        Route::post('/create', 'PagesController@store');
        Route::post('/update', 'PagesController@update');
    });

    Route::group(array('prefix' => 'ads'), function () {

        Route::get('/', 'AdsController@all');
        Route::get('/all', 'AdsController@all');

        Route::get('/create', 'AdsController@create');
        Route::get('/edit/{id}', 'AdsController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'AdsController@delete')->where(array('id' => '[0-9]+'));
        ;

        Route::post('/create', 'AdsController@store');
        Route::post('/update', 'AdsController@update');
    });

    Route::group(array('prefix' => 'statistics'), function () {

        Route::get('/', 'StatisticsController@all');
        Route::get('/all', 'StatisticsController@all');
    });

    Route::group(array('prefix' => 'settings'), function () {

        Route::get('/', 'SettingsController@all');
        Route::get('/all', 'SettingsController@all');

        Route::post('update_custom_css', 'SettingsController@updateCustomCSS');
        Route::post('update_custom_js', 'SettingsController@updateCustomJS');
        Route::post('update_social', 'SettingsController@updateSocial');
        Route::post('update_comments', 'SettingsController@updateComments');
        Route::post('update_seo', 'SettingsController@updateSEO');
        Route::post('update_general', 'SettingsController@updateGeneral');
        Route::post('update_terms', 'SettingsController@updateGeneralTerms');
        Route::post('update_privacy', 'SettingsController@updateGeneralPrivacy');
        Route::post('update_privacycollection', 'SettingsController@updateGeneralPrivacyCollection');
    });

    Route::get('/redactor/images.json', 'DashboardController@redactorImages');
});

Route::group(array('middleware' => 'auth'), function () {
    Route::group(array('prefix' => 'api'), function () {
        Route::get('/get_sub_categories_by_category/{id}', 'APIController@getSubCategories');
        Route::get('/get_blogs_sub_categories_by_category/{id}', 'APIController@getblogsSubCategories');
        Route::get('/get_tags', 'APIController@getTags');

    });
    Route::post('redactor', 'DashboardController@handleRedactorUploads');

});

//should be last route
Route::get('/callcentrejobs/{category_id}/{subcategory_id}/{location_id}/{job_id}', 'HomeController@jobDetails2');
Route::get('/{id}', 'HomeController@jobDetails');
