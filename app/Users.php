<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Session;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

/**
 * App\Users
 *
 * @mixin \Eloquent
 */
class Users extends Model implements AuthenticatableContract, CanResetPasswordContract, BillableContract
{
    use Billable;

    const TYPE_ADMIN = "admin";
    const TYPE_CUSTOMER = "customer";
	const TYPE_POSTER = "poster";
	const TYPE_CADMIN = "company_admin";
    const TYPE_PUBLISHER = "publisher";
    const TYPE_AUTHOR = "author";
	
    const GENDER_MALE = "male";
    const GENDER_FEMALE = "female";

    const INBOUND_STANDARD = 'standard';
    const INBOUND_MAXIMUM = 'maximum';

    const TYPE_RECRUITMENT_AGENCY = 'recruitment_agency';
    const TYPE_PRIVATE_ADVERTISER = 'private_advertiser';

    static $inboundProgram = [self::INBOUND_STANDARD, self::INBOUND_MAXIMUM];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    protected $table = 'users';


    public function applicants() {
        return $this->hasMany(Applicants::class, 'author_id');
    }

    public static function hasPermission($permission)
    {

        $users_group = DB::table('users_groups')->where("user_id", Auth::user()->id)->first();

        $group = Groups::find($users_group->group_id);

        if ($group->name == Users::TYPE_ADMIN) {
            return true;
        }

        $permissions = explode(",", $group->permissions);

        if (in_array($permission, $permissions)) {
            return true;
        } else {
            return false;
        }

    }
	
	public static function isPoster()
    {
		
        $users_group = DB::table('users_groups')->where("user_id", Auth::user()->id)->first();

        $group = Groups::find($users_group->group_id);

        return $group->name == 'company_admin';
    }
    
    public function isPosterPass()
    {
		
        $users_group = DB::table('users_groups')->where("user_id", $this->id)->first();

        $group = Groups::find($users_group->group_id);

        return $group->name == 'company_admin';
    }
	
	public static function isCustomer()
    {
		
        $users_group = DB::table('users_groups')->where("user_id", Auth::user()->id)->first();

        $group = Groups::find($users_group->group_id);

        return $group->name == 'customer';
    }

    //getAuthPassword , getAuthIdentifier , getRememberToken , getRememberTokenName, setRememberToken , getEmailForPasswordReset
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return $this->remember_token;
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public static function getAdvertiserTypes()
    {
        return [
            self::TYPE_PRIVATE_ADVERTISER => trans('messages.private_advertiser'),
            self::TYPE_RECRUITMENT_AGENCY => trans('messages.recruitment_agency')
        ];
    }
    public function getPostsPackagesPurchasedCount($postPackageId)
    {
        $postPackage = PostsPackages::find($postPackageId);

        $result = DB::table('posts')
            ->select(DB::raw(('COUNT(*) AS count')))
            ->where('posts.author_id', $this->id)
            ->where('posts.posts_packages', $postPackage->id)
            ->first();

        if (!$result->count) {
            return null;
        }
        else{
            return $result->count;
        }
    }


    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('deleted', false);
    }

    public function getSubcategories($index)
    {
        $subcategory = json_decode($this->subcategory, true);
        if (empty($subcategory) || !is_array($subcategory)) {
            return '';
        }

        if (!isset($subcategory[$index])) {
            return '';
        }

        return SubCategories::getSubCategoryById($subcategory[$index]);
    }

}

