<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Transactions
 *
 * @mixin \Eloquent
 */
class Transactions extends Model
{

    const TYPE_INDEX_HEADER = "index_header";
    const TYPE_INDEX_FOOTER = "index_footer";
    const TYPE_SIDEBAR = "sidebar";
    const TYPE_ABOVE_POST = "above_post";
    const TYPE_BELOW_POST = "below_post";
    const TYPE_BETWEEN_CATEGORY_INDEX = "between_category_index";
    const TYPE_BETWEEN_SUBCATEGORY_INDEX = "between_sub_category_index";
    const TYPE_BETWEEN_AUTHOR_INDEX = "between_author_index";
    const TYPE_BETWEEN_TAG_INDEX = "between_tag_index";
    const TYPE_BETWEEN_SEARCH_INDEX = "between_search_index";
    const TYPE_ABOVE_PAGE = "above_page";
    const TYPE_BELOW_PAGE = "below_page";

    protected $table = 'transactions';

    public function post()
    {
        return $this->hasOne(Posts::class, 'id', 'job_id');
    }

    public function postUpgrades()
    {
        return $this->hasMany(PostUpgrades::class, 'post_id', 'job_id');
    }
    public function getListPrice()
    {
        $post = $this->post()->getResults();

        if ($post) {
            $package = $post->package()->getResults();
            return money_format('%.2n', $package->price * $this->getDiscount());
        }

        return '';
    }

    public function getUpgradePrice()
    {
        $postUpgrades = $this->postUpgrades()->getResults();
        $prices = [];
        foreach ($postUpgrades as $postUpgrade) {
            $upgrade = $postUpgrade->upgrade()->getResults();
            $prices[] =  $upgrade->name .  " (" . money_format('%.2n', $upgrade->price * $this->getDiscount()) . ") ";
         }

        return implode('<br />', $prices);
    }

    public function formatPayment()
    {
        if (preg_match('/(Credits\s-\sCC\sFee:\s)([\d\.]+)/', $this->payment, $matches)) {
            return $matches[1] . money_format('%.2n', $matches[2]);
        }

        return $this->payment;
    }

    public function getDiscount()
    {
        if (!empty($this->coupon_used)) {
            $coupon = Coupons::where('coupon_code', $this->coupon_used)->get()->first();

            if ($coupon) {
                return (1 - $coupon->discount_rate / 100);
            }
        }

        return 1;
    }

}
