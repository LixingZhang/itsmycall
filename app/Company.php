<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Company
 *
 * @mixin \Eloquent
 */
class Company extends Model
{

    protected $table = 'company';

}
