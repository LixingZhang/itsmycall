<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Subscription
 *
 * @mixin \Eloquent
 */
class Subscription extends Model
{

    const INCENTIVE_STRUCTURE = [
        'fixed' => 'Base + Super',
        'basecommbonus' => 'Base + Super + R&R/Bonus',
        'basecomm' => 'Base + Super + Commissions',
        'commonly' => 'Commissions Only'
    ];

    const HOURLY_RATE = [
        '0' => '$0',
        '15' => '$15',
        '20' => '$20',
        '25' => '$25',
        '30' => '$30',
        '35' => '$35',
        '40' => '$40',
        '50' => '$50',
        '60' => '$60',
        '70' => '$70',
        '80' => '$80',
        '90' => '$90',
        '100' => '$100'
    ];
    const MAX_HOURLY_RATE = [
        '15' => '$15',
        '20' => '$20',
        '25' => '$25',
        '30' => '$30',
        '35' => '$35',
        '40' => '$40',
        '50' => '$50',
        '60' => '$60',
        '70' => '$70',
        '80' => '$80',
        '90' => '$90',
        '100' => '$100',
        '1000' => '$100+'
    ];

    const WAGE_RANGE = [
        '0' => '$0',
        '30000' => '$30K',
        '40000' => '$40K',
        '50000' => '$50K',
        '60000' => '$60K',
        '70000' => '$70K',
        '80000' => '$80K',
        '100000' => '$100K',
        '120000' => '$120K',
        '150000' => '$150K',
        '175000' => '$175K',
        '500000' => '$200K+',
    ];

    const WAGE_RANGE_MIN = [
        '0' => '$0',
        '30000' => '$30K',
        '40000' => '$40K',
        '50000' => '$50K',
        '60000' => '$60K',
        '70000' => '$70K',
        '80000' => '$80K',
        '100000' => '$100K',
        '120000' => '$120K',
        '150000' => '$150K',
        '175000' => '$175K',
        '200000' => '$200K',
    ];

    protected $table = 'subscriptions';


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }
}
