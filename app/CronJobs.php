<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CronJobs
 *
 * @mixin \Eloquent
 */
class CronJobs extends Model
{

    protected $table = 'cron_jobs';

}
