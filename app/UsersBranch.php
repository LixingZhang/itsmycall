<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersBranch
 *
 * @mixin \Eloquent
 */
class UsersBranch extends Model
{

    protected $table = 'company_branch';

}
