<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Categories
 *
 * @mixin \Eloquent
 */
class Categories extends Model
{

    const SCROLL_TYPE_PAGINATION = "pagination";
    const SCROLL_TYPE_SCROLL = "infinite_scroll";

    protected $table = 'categories';

    public static function getCategoryById($id)
    {
        $category = Categories::find($id);
        if ($category) {
            return $category->title;
        }
        return $id;
    }

}
