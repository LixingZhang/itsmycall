<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PostsPackages
 *
 * @mixin \Eloquent
 */
class PostsPackages extends Model
{

    const PACKAGE_STANDARD_ID = 3;
    const PACKAGE_PREMIUM_ID = 2;
    const PACKAGE_FEATURED_ID = 1;

    protected $table = 'posts_packages';

}
