<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Posts
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PostUpgrades[] $upgrades
 * @property-read \App\PostsPackages $package
 * @mixin \Eloquent
 */
class Posts extends Model
{
    const TYPE_SOURCE = "source";
    const TYPE_MANUAL = "manual";

    const RENDER_TYPE_TEXT = "text";
    const RENDER_TYPE_IMAGE = "image";
    const RENDER_TYPE_GALLERY = "gallery";
    const RENDER_TYPE_VIDEO = "video";

    const COMMENT_FACEBOOK = "facebook";
    const COMMENT_DISQUS = "disqus";

    const STATUS_POSITION_FILLED = 'filled';
    const STATUS_POSITION_ACTIVE = 'active';
    const STATUS_POSITION_REMOVE = 'remove';
    const STATUS_POSITION_CLOSED_WITHDRAWN = 'closed_withdrawn';
    const STATUS_POSITION_CLOSED_FILLED = 'closed_filled';
    const STATUS_POSITION_CLOSED_RESPONSE = 'closed_response';

    const EMPLOYMENT_TYPE_PART_TIME = 'part_time';
    const EMPLOYMENT_TYPE_FULL_TIME = 'full_time';
    const EMPLOYMENT_TYPE_CASUAL = 'casual';

    const EMPLOYMENT_TERM_PERMANENT = 'permanent';
    const EMPLOYMENT_FIXED_TERM = 'fixed_term';
    const EMPLOYMENT_TEMP = 'temp';

    const EXPIRY_DAYS = 30;


    protected $table = 'posts';

    /**
     * Get the Upgrades
     */
    public function upgrades()
    {
        return $this->hasMany(PostUpgrades::class, 'post_id');
    }
    /**
     * Get the Package
     */
    public function package()
    {
        return $this->hasOne(PostsPackages::class, 'id', 'posts_packages');
    }

    public function user()
    {
        return $this->belongsTo(Users::class, 'author_id');
    }

    public static function getPositionStatuses() {
        return [
            self::STATUS_POSITION_ACTIVE => trans('messages.active'),
            self::STATUS_POSITION_REMOVE => trans('messages.remove'),
            self::STATUS_POSITION_FILLED => trans('messages.filled'),
            self::STATUS_POSITION_CLOSED_WITHDRAWN => 'Closed - Withdrawn - The job has been withdrawn and is no longer available.',
            self::STATUS_POSITION_CLOSED_FILLED => 'Closed - Filled Elsewhere - The job was filled outside of ItsMyCall.',
            self::STATUS_POSITION_CLOSED_RESPONSE => 'Closed - Poor Response - The job had a poor response/lack of quality candidates.',
        ];
    }

    public function transaction()
    {
        return $this->hasOne(Transactions::class, 'job_id');
    }

    public function getAdvertiserType() {
        $user = Users::find($this->author_id);

        return $user->advertiser_type;
    }

    public function scopeActive($query) {
        return $query->where('expired_at', '>=', Carbon::now())
            ->where('status', Posts::STATUS_POSITION_ACTIVE)
            ->whereHas('user', function ($q) {
                $q->where('deleted', false);
            });
    }

    public function isExpired()
    {
        return $this->expired_at < Carbon::now();
    }
}
