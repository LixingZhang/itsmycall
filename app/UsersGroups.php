<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersGroups
 *
 * @property-read \App\Users $user
 * @mixin \Eloquent
 */
class UsersGroups extends Model
{

    protected $table = 'users_groups';
	
	public function user()
    {
        return $this->belongsTo(Users::class);
    }

}
