<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Branch
 *
 * @mixin \Eloquent
 */
class Branch extends Model
{

    protected $table = 'branch';

}
