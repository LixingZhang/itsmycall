<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Groups
 *
 * @mixin \Eloquent
 */
class Groups extends Model
{

    protected $table = 'groups';

}
