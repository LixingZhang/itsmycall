<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostUpgradesLists extends Model
{
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_NOT_ACTIONED = 'not_actioned';
    const STATUS_COMPLETED = 'completed';

    static $status = [
        self::STATUS_IN_PROGRESS => 'In Progress',
        self::STATUS_NOT_ACTIONED => 'Not Actioned',
        self::STATUS_COMPLETED => 'Completed'
    ];


    static $postEditionsFacebook = [
        'Facebook - CX & Contact Centre Champions Page' => 'Facebook - CX & Contact Centre Champions Page',
        'Facebook - ItsMyCall Page' => 'Facebook - ItsMyCall Page'
    ];
    static $postEditions = [
        'Facebook - CX & Contact Centre Champions Page' => 'Facebook - CX & Contact Centre Champions Page',
        'Facebook - ItsMyCall Page' => 'Facebook - ItsMyCall Page',
        'LinkedIn - ItsMyCall Company Page' => 'LinkedIn - ItsMyCall Company Page',
        'LinkedIn - Justin Tippett\'s network' => 'LinkedIn - Justin Tippett\'s network',
        'LinkedIn - CX & Contact Centre Professionals Aus/NZ Group' => 'LinkedIn - CX & Contact Centre Professionals Aus/NZ Group',
        'The Pulse Newsletter' => 'The Pulse Newsletter'
    ];

    static $postEditionsLinkedIn = [
        'LinkedIn - ItsMyCall Company Page' => 'LinkedIn - ItsMyCall Company Page',
        'LinkedIn - Justin Tippett\'s network' => 'LinkedIn - Justin Tippett\'s network',
        'LinkedIn - CX & Contact Centre Professionals Aus/NZ Group' => 'LinkedIn - CX & Contact Centre Professionals Aus/NZ Group',
        'LinkedIn - CCC Company Page' => 'LinkedIn - CCC Company Page'
    ];

    static $postEditionsPulse = [
        'The Pulse Newsletter' => 'The Pulse Newsletter'
    ];

    protected $table = 'posts_upgrade_lists';

    public function upgrades()
    {
        return $this->belongsTo(PostUpgrades::class, 'post_upgrades_id');
    }

    /**
     * @return Posts mixed
     */
    public function getJob()
    {
        return $this->upgrades()->getResults()->job()->getResults();
    }

    public function getUser()
    {
       return $this->getJob()->user()->getResults();
    }

    public function getPostPackage()
    {
        return $this->getJob()->package()->getResults();
    }

    public function getPostUpgradePrice()
    {
        $postUpgrade = $this->upgrades()->getResults();

        $upgrade = $postUpgrade->upgrade()->getResults();

        return $upgrade->price;
    }

    public function getActualPrice()
    {
        $postUpgrade = $this->upgrades()->getResults();

        if ($postUpgrade->is_purchased) {
            return $postUpgrade->getActualPrice();
        }

        return 'Free';
    }
}
