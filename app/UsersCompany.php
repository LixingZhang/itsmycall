<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UsersCompany
 *
 * @mixin \Eloquent
 */
class UsersCompany extends Model
{

    protected $table = 'users_company';

}
