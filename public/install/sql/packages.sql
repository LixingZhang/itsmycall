DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `jobs_included` int(255) NOT NULL,
  `price` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `description_listing` text NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `packages` (`id`, `package_name`, `jobs_included`, `price`, `created_at`, `updated_at`, `description`, `description_listing`, `type`) VALUES
(1,	'Feature in our newsletter',	4,	35,	'2016-08-10 02:26:54',	'2016-08-09 12:26:54',	'Feature your job in our weekly newsletter sent to contact centre industry professionals along with key news & articles. Great way to attract passive job seekers!',	'Company Logo Displayed;Unlimited Changes',	'per week/edition'),
(2,	'Feature on our Facebook',	4,	50,	'2016-08-10 02:26:46',	'2016-08-09 12:26:46',	'Feature your job on our Facebook page - perfect for agent level roles',	'Company Logo Displayed;Stay at the top of your Category;Larger Company Logo;30 Day Listing;Unlimited Changes',	'2 posts per week'),
(3,	'Feature on our home page',	4,	75,	'2016-08-09 12:31:54',	'2016-08-09 12:31:54',	'Feature your job on our website with a dedicated advertising tile on our home page',	'Company Logo Displayed;Stay at the top of your Category;Include a Video on your Listing;Selling Points Displayed in Listing Pages;Larger Company Logo;30 Day Listing;Unlimited Changes',	'per week'),
(4,	'Feature on our website',	4,	130,	'2016-08-09 12:31:54',	'2016-08-09 12:31:54',	'Feature your job on our website with a dedicated leaderboard add and advertising tile across all website pages',	'Company Logo Displayed;Stay at the top of your Category;Add Social Media Links;Include a Video on your Listing;Larger Company Logo;30 Day Listing;Unlimited Changes',	'per week'),
(5,	'VIP',	4,	245,	'2016-08-09 12:31:54',	'2016-08-09 12:31:54',	'Executive Upgrade package - perfect for senior roles',	'Company Logo Displayed;Stay at the top of your Category;Standout with Exclusive Featured Colouring;Add Social Media Links;Include a Video on your Listing;Selling Points Displayed in Listing Pages;Larger Company Logo;30 Day Listing;Unlimited Changes',	'per week');
