-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 08, 2016 at 08:17 PM
-- Server version: 5.6.31
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fashionr_cccjobs`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` text COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resume` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `title`, `name`, `email`, `resume`, `author_id`, `job_id`, `date_created`) VALUES
(1, 'Pozo', '0475422227', 'test-mark@pozo.com.au', 'http://cccjobs.pozo.com.au/uploads/attachments/57301e27c1124_file.pdf', 3, 4, '2016-05-10 09:47:46'),
(2, 'Test', 'Test', 'test@test.com', 'http://cccjobs.pozo.com.au/uploads/attachments/5731976ed6ef1_file.docx', 12, 4, '2016-05-10 09:47:46'),
(3, 'Test', 'Test', 'test@test.com', 'http://cccjobs.pozo.com.au/uploads/attachments/5731976f75845_file.docx', 12, 4, '2016-05-10 09:47:46'),
(4, 'Test', 'Test', 'test@test.com', 'http://cccjobs.pozo.com.au/uploads/attachments/5731a3c850688_file.pdf', 12, 4, '2016-05-10 09:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Sub Branch Test', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scroll_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_in_menu` tinyint(4) NOT NULL,
  `show_in_sidebar` tinyint(4) NOT NULL,
  `show_in_footer` tinyint(4) NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `show_as_mega_menu` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `scroll_type`, `show_in_menu`, `show_in_sidebar`, `show_in_footer`, `seo_keywords`, `seo_description`, `show_as_mega_menu`, `created_at`, `updated_at`) VALUES
(2, 'Agent', 'agent', 'pagination', 0, 0, 0, 'Agent', 'Agent Jobs', 0, '2016-05-09 13:17:48', '2016-05-09 13:17:48'),
(3, 'Team Leader, Manager, 2IC', 'team-leader-manager-2ic', 'pagination', 0, 0, 0, 'Team Leader, Manager / 2IC', 'Team Leader, Manager, 2IC', 0, '2016-05-09 13:18:14', '2016-05-09 13:23:53'),
(4, 'Management', 'management', 'pagination', 0, 0, 0, 'Management', 'Management', 0, '2016-05-09 13:18:26', '2016-05-09 13:18:26'),
(5, 'Workforce Optimisation', 'workforce-optimisation', 'pagination', 0, 0, 0, 'Workforce Optimisation', 'Workforce Optimisation', 0, '2016-05-09 13:18:40', '2016-05-09 13:18:40'),
(6, 'Learning & Development', 'learning-development', 'pagination', 0, 0, 0, 'Learning & Development', 'Learning & Development', 0, '2016-05-09 13:19:11', '2016-05-09 13:19:11'),
(7, 'Human Resources', 'human-resources', 'pagination', 0, 0, 0, 'Human Resources', 'Human Resources', 0, '2016-05-09 13:19:23', '2016-05-09 13:19:23'),
(8, 'Executive Roles', 'executive-roles', 'pagination', 0, 0, 0, 'Executive Roles', 'Executive Roles', 0, '2016-05-09 13:20:07', '2016-05-09 13:20:07'),
(9, 'Other', 'other', 'pagination', 0, 0, 0, 'Other', 'Other', 0, '2016-05-09 13:20:30', '2016-05-09 13:20:30'),
(10, 'Information Technology (ICT)', 'information-technology-ict', 'pagination', 0, 0, 0, 'Information, Communication, Technology (ICT)', 'Information, Communication, Technology (ICT)', 0, '2016-05-09 13:33:40', '2016-05-09 15:55:51'),
(11, 'Chief Tester', 'chief-tester', 'pagination', 0, 0, 0, 'chief tester', 'Someone who test shootings ', 0, '2016-05-31 20:32:41', '2016-05-31 20:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Test Company', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `company_branch`
--

CREATE TABLE IF NOT EXISTS `company_branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `company_branch`
--

INSERT INTO `company_branch` (`id`, `user_id`, `company_id`, `branch_id`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 4, 0, 1, '2016-05-08 14:56:46', '2016-05-08 14:56:46'),
(15, 4, 0, 1, '2016-05-08 14:56:55', '2016-05-08 14:56:55'),
(16, 9, 0, 0, '2016-05-08 15:02:29', '2016-05-08 15:02:29'),
(17, 10, 0, 0, '2016-05-08 15:03:11', '2016-05-08 15:03:11');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=157 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(15, 'AU', 'Australia'),
(156, 'NZ', 'New Zealand');

-- --------------------------------------------------------

--
-- Table structure for table `cron_jobs`
--

CREATE TABLE IF NOT EXISTS `cron_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cron_started_on` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cron_completed_on` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `what` text COLLATE utf8_unicode_ci NOT NULL,
  `result` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', '2015-09-09 01:52:01', '2015-09-09 01:52:01'),
(2, 'customer', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'company_admin', 'users.add,users.edit,users.view,users.delete', '2016-05-08 09:30:36', '2016-05-08 14:20:20'),
(4, 'poster', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `image_gallery`
--

CREATE TABLE IF NOT EXISTS `image_gallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE IF NOT EXISTS `keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `keyword`, `count`) VALUES
(1, 'Pozo', 1),
(2, 'Lorum', 1),
(3, 'Test', 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_06_23_103902_create_categories_table', 1),
('2015_06_24_104137_create_sources_table', 1),
('2015_06_25_111523_create_pages_table', 1),
('2015_06_25_130151_create_ads_table', 1),
('2015_06_28_112539_create_settings_table', 1),
('2015_07_18_115508_create_avatar', 1),
('2015_07_22_213918_create_sub_categories_table', 1),
('2015_07_22_225256_add_featured_to_posts', 1),
('2015_07_23_130937_create_groups_table', 1),
('2015_07_23_131055_create_user_groups_table', 1),
('2015_07_23_145432_add_fields_to_users', 1),
('2015_07_23_192626_create_countries_table', 1),
('2015_07_23_194056_add_fb_columns_users', 1),
('2015_07_24_061620_add_fields_to_posts', 1),
('2015_07_24_071211_make_tags_table', 1),
('2015_07_24_073148_create_image_gallery_table', 1),
('2015_07_29_171303_create_post_ratings_table', 1),
('2015_07_29_173535_add_views_to_tags', 1),
('2015_07_29_175532_add_slug_to_tags', 1),
('2015_07_29_180532_add_views_to_tagss', 1),
('2015_07_29_180819_show_as_mega_menu_cats', 1),
('2015_07_29_192856_add_author_to_posts', 1),
('2015_07_30_085814_add_slug_to_users', 1),
('2015_07_30_175935_add_show_featured_image', 1),
('2015_07_31_061109_add_columns_to_users', 1),
('2015_08_03_070051_add_twitter_handle_settings', 1),
('2015_08_03_125746_add_link_to_posts', 1),
('2015_08_03_160735_add_status_author_pages', 1),
('2015_08_03_170949_add_groups_and_admin', 1),
('2015_08_07_061421_add_dont_show_author_as_publisher', 1),
('2015_08_09_115604_big_sharing_btns', 1),
('2015_08_11_063723_create_permissions_table', 1),
('2015_08_11_102144_add_rating_desc', 1),
('2015_08_11_103313_add_post_likes_table', 1),
('2015_08_12_195811_create_cron_jobs_table', 1),
('2015_08_18_084146_add_dummy_table', 1),
('2015_08_19_194624_drop_permissions_and_dummy', 1),
('2015_08_27_100547_posts', 1),
('2015_08_30_113327_add_customer_group', 1),
('2015_09_03_155335_keywords', 1),
('2015_09_05_060253_users', 1),
('2015_09_07_061641_applicants', 1),
('2015_09_11_130824_add_cashier_columns', 2),
('2015_09_12_074915_add_featured_time_table', 3),
('2015_10_06_060154_add_author_id_to_applicants', 4);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `jobs_included` int(255) NOT NULL,
  `price` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_name`, `jobs_included`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Package 1', 15, 253, '2016-08-10 05:26:54', '2016-08-09 15:26:54'),
(2, 'Package 2', 30, 457, '2016-08-10 05:26:46', '2016-08-09 15:26:46'),
(3, 'Package 3', 46, 15000, '2016-08-09 15:31:54', '2016-08-09 15:31:54');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `show_in_menu` tinyint(4) NOT NULL,
  `show_in_sidebar` tinyint(4) NOT NULL,
  `show_in_footer` tinyint(4) NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `views` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `show_in_mega_menu` tinyint(4) NOT NULL,
  `render_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_embed_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_parallax` tinyint(4) NOT NULL,
  `video_parallax` tinyint(4) NOT NULL,
  `rating_box` tinyint(4) NOT NULL,
  `show_featured_image_in_post` tinyint(4) NOT NULL,
  `show_author_box` tinyint(4) NOT NULL DEFAULT '1',
  `show_author_socials` tinyint(4) NOT NULL DEFAULT '1',
  `dont_show_author_publisher` tinyint(4) NOT NULL,
  `show_post_source` tinyint(4) NOT NULL,
  `rating_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `experience` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_starts_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured_ends_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `title`, `slug`, `link`, `featured`, `category_id`, `type`, `source_id`, `description`, `featured_image`, `views`, `status`, `created_at`, `updated_at`, `show_in_mega_menu`, `render_type`, `video_embed_code`, `image_parallax`, `video_parallax`, `rating_box`, `show_featured_image_in_post`, `show_author_box`, `show_author_socials`, `dont_show_author_publisher`, `show_post_source`, `rating_desc`, `company`, `country`, `city`, `salary`, `experience`, `state`, `category`, `subcategory`, `featured_starts_at`, `featured_ends_at`) VALUES
(3, 2, 'Test', 'test-1462851556', '', 0, 2, '', 0, '<p><a href="http://cccjobs.pozo.com.au/">http://cccjobs.pozo.com.au/</a></p>', 'http://cccjobs.pozo.com.au/uploads/images/573157e4bfa86_file.png', 1, 1, '2016-05-09 13:39:16', '2016-05-31 18:18:54', 0, '', '', 0, 0, 0, 0, 1, 1, 0, 0, '', 'Test', 'australia', '', 36602, '0', 'victoria', 'agent', '2', NULL, NULL),
(4, 12, 'Test', 'test-1462866747', '', 0, 2, '', 0, '<p>Test</p>', 'http://cccjobs.pozo.com.au/uploads/images/5731933be3a46_file.png', 23, 1, '2016-05-09 17:52:27', '2016-05-31 18:17:59', 0, '', '', 0, 0, 0, 0, 1, 1, 0, 0, '', 'Test', 'australia', '', 36602, '1', 'victoria', '2', '2', NULL, NULL),
(5, 4, 'Pozo', 'pozo-1470804825', '', 0, 2, '', 0, '<p>Are you passionate about online retail, thrive in a fast paced environment, have a strong work ethos with MYOB experience, then you will be just who we are looking for!</p><p>About us</p><p>We are a small, fun and dynamic team that is passionate about offering superior customer service.  We offer a luxurious range of home-wares, the latest in outdoor furnishings and a well-regarded range of garden products.  We sell via retail, wholesale, online and eBay.</p><p>This is a proud, value driven business, which is fast paced, collaborative and has a strong focus on employees, customers and innovation.  You will be a team player with superior attention to detail and take pride in organising an efficient office.</p><p>We are always looking for new ideas - your opinion counts!  Your voice will be heard; when we brainstorm at weekly meetings we need your input.</p><p>Benefits</p><ul><li>Immediate start available</li><li>Permanent full time position</li><li>Northern suburbs location</li><li>On-site parking</li></ul><p>There is no such thing as a typical day and this role will see you:</p><ul><li>Enter online, eBay and wholesale orders in MYOB</li><li>Respond to customer enquiries by phone and email</li><li>Liaise with our friendly warehouse team</li><li>Co-ordinate freight</li><li>Compare and choose best freight rates for interstate orders</li><li>Reconcile daily banking</li><li>Use your Excel skills to update weekly sales forecasts</li></ul><p>What you will bring to our company</p><ul><li>Fast and accurate order entry</li><li>Easy going, but you love a busy busy office</li><li>MYOB experience</li><li>Superior organisational skills, and you love procedures and processes</li><li>Magento and eBay knowledge preferred</li><li>Professional customer service skills</li><li>Bookkeeping experience useful</li><li>Exceptional phone manner, always with a positive attitude</li><li>Excellent Excel and Word skills</li><li>Excellent spoken and written English</li><li>Be well-presented, outgoing, enthusiastic and self motivated</li><li>Have a "Can Do" attitude</li></ul><p>If this looks like you, we would love to hear from you!</p><p>In your covering letter PLEASE answer the following questions:</p><ol><li>Why does this role appeal to you?</li><li>What specific skill set do you bring that will be valuable to us?</li></ol>', '', 9, 1, '2016-08-09 14:53:45', '2016-08-18 17:11:43', 0, '', '', 0, 0, 0, 0, 1, 1, 0, 0, '', 'Pozo', 'australia', '', 36602, '', 'south-australia', '2', '2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_likes`
--

CREATE TABLE IF NOT EXISTS `post_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_ratings`
--

CREATE TABLE IF NOT EXISTS `post_ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE IF NOT EXISTS `post_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `column_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value_txt` text COLLATE utf8_unicode_ci,
  `value_check` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `show_big_sharing` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `category`, `column_key`, `value_string`, `value_txt`, `value_check`, `created_at`, `updated_at`, `show_big_sharing`) VALUES
(1, 'social', 'twitter_handle', '', NULL, NULL, '2015-09-09 01:51:58', '2015-09-09 01:51:58', 0),
(2, 'custom_js', 'custom_js', NULL, '', NULL, '2015-10-23 00:23:42', '2015-11-05 17:28:55', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE IF NOT EXISTS `sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `channel_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_pubDate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_lastBuildDate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_generator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auto_update` tinyint(4) NOT NULL,
  `items_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dont_show_author_publisher` tinyint(4) NOT NULL,
  `show_post_source` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scroll_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_in_menu` tinyint(4) NOT NULL,
  `show_in_sidebar` tinyint(4) NOT NULL,
  `show_in_footer` tinyint(4) NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `parent_id`, `priority`, `title`, `slug`, `scroll_type`, `show_in_menu`, `show_in_sidebar`, `show_in_footer`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 'Appointment Setting', 'appointment-setting', 'pagination', 0, 0, 0, 'Appointment Setting', 'Appointment Setting', '2016-05-09 13:21:12', '2016-05-09 13:21:12'),
(3, 2, 1, 'Billiing', 'billiing', 'pagination', 0, 0, 0, 'Billiing', 'Billiing', '2016-05-09 13:21:20', '2016-05-09 13:21:20'),
(4, 2, 1, 'Claims & Resolutions', 'claims-resolutions', 'pagination', 0, 0, 0, 'Claims & Resolutions', 'Claims & Resolutions', '2016-05-09 13:21:28', '2016-05-09 13:21:28'),
(5, 2, 1, 'Collections', 'collections', 'pagination', 0, 0, 0, 'Collections', 'Collections', '2016-05-09 13:21:34', '2016-05-09 13:21:34'),
(6, 2, 1, 'Customer Service', 'customer-service', 'pagination', 0, 0, 0, 'Customer Service', 'Customer Service', '2016-05-09 13:21:44', '2016-05-09 13:21:44'),
(7, 2, 1, 'Emails/Written Corrospondence', 'emailswritten-corrospondence', 'pagination', 0, 0, 0, 'Emails/Written Corrospondence', 'Emails/Written Corrospondence', '2016-05-09 13:21:51', '2016-05-09 13:21:51'),
(8, 2, 1, 'Multi-skilled  (all rounder)', 'multi-skilled-all-rounder', 'pagination', 0, 0, 0, 'Multi-skilled  (all rounder)', 'Multi-skilled  (all rounder)', '2016-05-09 13:21:58', '2016-05-09 13:21:58'),
(9, 2, 1, 'Sales', 'sales', 'pagination', 0, 0, 0, 'Sales', 'Sales', '2016-05-09 13:22:08', '2016-05-09 13:22:08'),
(10, 2, 1, 'Social Media', 'social-media', 'pagination', 0, 0, 0, 'Social Media', 'Social Media', '2016-05-09 13:22:15', '2016-05-09 13:22:15'),
(11, 2, 1, 'Technical Support', 'technical-support', 'pagination', 0, 0, 0, 'Technical Support', 'Technical Support', '2016-05-09 13:22:23', '2016-05-09 13:22:23'),
(12, 3, 1, 'Sales ', 'sales', 'pagination', 0, 0, 0, 'Sales ', 'Sales ', '2016-05-09 13:24:29', '2016-05-09 13:24:29'),
(13, 3, 1, 'Customer Service', 'customer-service', 'pagination', 0, 0, 0, 'Customer Service', 'Customer Service', '2016-05-09 13:24:38', '2016-05-09 13:24:38'),
(14, 3, 1, 'Outbound', 'outbound', 'pagination', 0, 0, 0, 'Outbound', 'Outbound', '2016-05-09 13:24:48', '2016-05-09 13:24:48'),
(15, 3, 1, 'Technical Support', 'technical-support', 'pagination', 0, 0, 0, 'Technical Support', 'Technical Support', '2016-05-09 13:24:58', '2016-05-09 13:24:58'),
(16, 3, 1, 'Resolutions', 'resolutions', 'pagination', 0, 0, 0, 'Resolutions', 'Resolutions', '2016-05-09 13:25:08', '2016-05-09 13:25:08'),
(17, 4, 1, 'Campaign Manager', 'campaign-manager', 'pagination', 0, 0, 0, 'Campaign Manager', 'Campaign Manager', '2016-05-09 13:25:20', '2016-05-09 13:25:20'),
(18, 4, 1, 'Contact Centre Manager', 'contact-centre-manager', 'pagination', 0, 0, 0, 'Contact Centre Manager', 'Contact Centre Manager', '2016-05-09 13:25:31', '2016-05-09 13:25:31'),
(19, 4, 1, 'Contact Centre Supervisor', 'contact-centre-supervisor', 'pagination', 0, 0, 0, 'Contact Centre Supervisor', 'Contact Centre Supervisor', '2016-05-09 13:25:46', '2016-05-09 13:25:46'),
(20, 4, 1, 'Customer Experience', 'customer-experience', 'pagination', 0, 0, 0, 'Customer Experience', 'Customer Experience', '2016-05-09 13:25:55', '2016-05-09 13:25:55'),
(21, 4, 1, 'Customer Service', 'customer-service', 'pagination', 0, 0, 0, 'Customer Service', 'Customer Service', '2016-05-09 13:26:06', '2016-05-09 13:26:06'),
(22, 4, 1, 'Operational functions (e.g. Sales, service, support)', 'operational-functions-eg-sales-service-support', 'pagination', 0, 0, 0, 'Operational functions (e.g. Sales, service, support)', 'Operational functions (e.g. Sales, service, support)', '2016-05-09 13:26:13', '2016-05-09 13:26:13'),
(23, 4, 1, 'Human Resources', 'human-resources', 'pagination', 0, 0, 0, 'Human Resources', 'Human Resources', '2016-05-09 13:26:24', '2016-05-09 13:26:24'),
(24, 4, 1, 'Workforce Optimisation', 'workforce-optimisation', 'pagination', 0, 0, 0, 'Workforce Optimisation', 'Workforce Optimisation', '2016-05-09 13:26:38', '2016-05-09 13:26:38'),
(25, 4, 1, 'ICT ', 'ict', 'pagination', 0, 0, 0, 'ICT ', 'ICT ', '2016-05-09 13:26:51', '2016-05-09 13:26:51'),
(26, 4, 1, 'Sales', 'sales', 'pagination', 0, 0, 0, 'Sales', 'Sales', '2016-05-09 13:27:05', '2016-05-09 13:27:05'),
(27, 4, 1, 'L&D Manager', 'ld-manager', 'pagination', 0, 0, 0, 'L&D Manager', 'L&D Manager', '2016-05-09 13:27:17', '2016-05-09 13:27:17'),
(28, 4, 1, 'Reporting ', 'reporting', 'pagination', 0, 0, 0, 'Reporting ', 'Reporting ', '2016-05-09 13:27:27', '2016-05-09 13:27:27'),
(29, 5, 1, 'Scheduling', 'scheduling', 'pagination', 0, 0, 0, 'Scheduling', 'Scheduling', '2016-05-09 13:27:36', '2016-05-09 13:27:36'),
(30, 5, 1, 'Analyst ', 'analyst', 'pagination', 0, 0, 0, 'Analyst ', 'Analyst ', '2016-05-09 13:27:43', '2016-05-09 13:27:43'),
(31, 5, 1, 'Forecasting', 'forecasting', 'pagination', 0, 0, 0, 'Forecasting', 'Forecasting', '2016-05-09 13:27:56', '2016-05-09 13:27:56'),
(32, 5, 1, 'Planner', 'planner', 'pagination', 0, 0, 0, 'Planner', 'Planner', '2016-05-09 13:28:12', '2016-05-09 13:28:12'),
(33, 5, 1, 'All rounder', 'all-rounder', 'pagination', 0, 0, 0, 'All rounder', 'All rounder', '2016-05-09 13:28:22', '2016-05-09 13:28:22'),
(34, 5, 1, 'Process Optimisation ', 'process-optimisation', 'pagination', 0, 0, 0, 'Process Optimisation ', 'Process Optimisation ', '2016-05-09 13:28:44', '2016-05-09 13:28:44'),
(35, 5, 1, 'Quality Assurance', 'quality-assurance', 'pagination', 0, 0, 0, 'Quality Assurance', 'Quality Assurance', '2016-05-09 13:28:54', '2016-05-09 13:28:54'),
(36, 5, 1, 'Reporting ', 'reporting', 'pagination', 0, 0, 0, 'Reporting ', 'Reporting ', '2016-05-09 13:29:05', '2016-05-09 13:29:05'),
(37, 6, 1, 'Training Coordinator', 'training-coordinator', 'pagination', 0, 0, 0, 'Training Coordinator', 'Training Coordinator', '2016-05-09 13:31:14', '2016-05-09 13:31:14'),
(38, 6, 1, 'Induction coordinator ', 'induction-coordinator', 'pagination', 0, 0, 0, 'Induction coordinator ', 'Induction coordinator ', '2016-05-09 13:31:24', '2016-05-09 13:31:24'),
(39, 6, 1, 'L&D Coordinator', 'ld-coordinator', 'pagination', 0, 0, 0, 'L&D Coordinator', 'L&D Coordinator', '2016-05-09 13:31:33', '2016-05-09 13:31:33'),
(40, 6, 1, 'eLearning Designer', 'elearning-designer', 'pagination', 0, 0, 0, 'eLearning Designer', 'eLearning Designer', '2016-05-09 13:31:42', '2016-05-09 13:31:42'),
(41, 6, 1, 'Instructional Designer', 'instructional-designer', 'pagination', 0, 0, 0, 'Instructional Designer', 'Instructional Designer', '2016-05-09 13:31:57', '2016-05-09 13:31:57'),
(42, 6, 1, 'Trainer', 'trainer', 'pagination', 0, 0, 0, 'Trainer', 'Trainer', '2016-05-09 13:32:05', '2016-05-09 13:32:05'),
(43, 7, 1, 'Recruitment Coordinator', 'recruitment-coordinator', 'pagination', 0, 0, 0, 'Recruitment Coordinator', 'Recruitment Coordinator', '2016-05-09 13:32:29', '2016-05-09 13:32:29'),
(44, 7, 1, 'OH&S', 'ohs', 'pagination', 0, 0, 0, 'OH&S', 'OH&S', '2016-05-09 13:32:39', '2016-05-09 13:32:39'),
(45, 7, 1, 'HR Coordinator', 'hr-coordinator', 'pagination', 0, 0, 0, 'HR Coordinator', 'HR Coordinator', '2016-05-09 13:32:48', '2016-05-09 13:32:48'),
(46, 7, 1, 'Culture', 'culture', 'pagination', 0, 0, 0, 'Culture', 'Culture', '2016-05-09 13:32:57', '2016-05-09 13:32:57'),
(47, 7, 1, 'Talent acquisition', 'talent-acquisition', 'pagination', 0, 0, 0, 'Talent acquisition', 'Talent acquisition', '2016-05-09 13:33:06', '2016-05-09 13:33:06'),
(48, 10, 1, 'Dialler Administration', 'dialler-administration', 'pagination', 0, 0, 0, 'Dialler Administration', 'Dialler Administration', '2016-05-09 13:33:50', '2016-05-09 13:33:50'),
(49, 10, 1, 'Application Support', 'application-support', 'pagination', 0, 0, 0, 'Application Support', 'Application Support', '2016-05-09 13:33:58', '2016-05-09 13:33:58'),
(50, 10, 1, 'CRM Tools', 'crm-tools', 'pagination', 0, 0, 0, 'CRM Tools', 'CRM Tools', '2016-05-09 13:34:06', '2016-05-09 13:34:06'),
(51, 10, 1, 'Network Administration', 'network-administration', 'pagination', 0, 0, 0, 'Network Administration', 'Network Administration', '2016-05-09 13:34:14', '2016-05-09 13:34:14'),
(52, 10, 1, 'Desktop Support', 'desktop-support', 'pagination', 0, 0, 0, 'Desktop Support', 'Desktop Support', '2016-05-09 13:34:22', '2016-05-09 13:34:22'),
(53, 9, 1, 'NPS', 'nps', 'pagination', 0, 0, 0, 'NPS', 'NPS', '2016-05-09 13:34:35', '2016-05-09 13:34:35'),
(54, 9, 1, 'Continous Improvement', 'continous-improvement', 'pagination', 0, 0, 0, 'Continous Improvement', 'Continous Improvement', '2016-05-09 13:34:46', '2016-05-09 13:34:46'),
(55, 9, 1, 'Business Development ', 'business-development', 'pagination', 0, 0, 0, 'Business Development ', 'Business Development ', '2016-05-09 13:34:55', '2016-05-09 13:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reset_requested_on` datetime NOT NULL,
  `activated` tinyint(4) NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activated_at` datetime NOT NULL,
  `fb_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fb_page_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_plus_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resume` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_active` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_subscription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_plan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_four` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `subscription_ends_at` timestamp NULL DEFAULT NULL,
  `company_admin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `slug`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `avatar`, `birthday`, `bio`, `gender`, `mobile_no`, `country`, `timezone`, `reset_password_code`, `reset_requested_on`, `activated`, `activation_code`, `activated_at`, `fb_url`, `fb_page_url`, `website_url`, `twitter_url`, `google_plus_url`, `resume`, `stripe_active`, `stripe_id`, `stripe_subscription`, `stripe_plan`, `last_four`, `trial_ends_at`, `subscription_ends_at`, `company_admin`) VALUES
(1, 'Admin', 'admin', 'admin@mail.com', '$2y$10$axha8uPO6Y0R.9cpZKT2m.O9osXHN4mKrtNh.ibiVgn0DLMODjnau', 'Iy0A7S6TcU1pXEhDVzWXYtOOCfoKE4vCGSGyxlHs9Pc0BdCoyeiAoQ8APoPm', '2015-09-09 01:52:02', '2016-04-29 10:43:52', 'http://cccjobs.pozo.com.au/uploads/5723f88aadfd9_file.png', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 1, '', '2016-04-29 20:12:58', '', '', '', '', '', '', 1, 'cus_6y5GDYnPVYd3es', 'sub_6y5GW2tPWXPgJv', '1', '4242', NULL, NULL, 0),
(2, 'justin', 'justin', 'justin@contactcentrecentral.com', '$2y$10$70oEtls.S/s4i6qBUxRtQ.4jMMFNMeDS9gRi8mypD/zTgibDJyVaW', 'Aag5keek6TQtLjsk8AYUyX4oUH4zdSg6375hWNvtZQ1JMwdzArKgH9fqCYXc', '2016-04-29 10:15:44', '2016-06-09 10:31:28', '', '04/20/2016', '', 'male', '', '1', '', '', '0000-00-00 00:00:00', 1, '', '2016-04-29 20:15:54', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'customer.test', 'customertest', 'customer.test@pozo.com.au', '$2y$10$blDmQv7fRyQO23J15u.re.lCUSvOR6FjDWkzg0nGYLHZ8naE5ypta', '9vPGHNOfgoh1VIWefGGCXw6jolHSTf4CTqQlHcXdwbgPpCT2MoerzFuHzrwY', '2016-04-29 10:43:32', '2016-08-09 14:27:52', '', '04/28/2016', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 1, '', '2016-06-01 04:28:43', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 'Mark Carroll2', 'mark-carroll2', 'info@pozo.com.au', '$2y$10$axha8uPO6Y0R.9cpZKT2m.O9osXHN4mKrtNh.ibiVgn0DLMODjnau', '5SnVnOSbWskOCiN2ulONj0xQY31OmDS8fuRNFC59j5qVl3x7xhzlQ4zkcZMN', '2016-05-08 09:31:13', '2016-09-07 17:37:45', '', '', '', 'male', '475422227', '15', '', '', '0000-00-00 00:00:00', 1, '', '2016-08-10 00:32:13', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(5, 'Tester', 'tester', 'test@pozo.com.au', '$2y$10$3273gaUVPBi2F9qjSAa5seGvrDB6COYHoJMyEhBZqkSeCxpDksVVS', NULL, '2016-05-08 13:59:56', '2016-05-31 18:26:33', '', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'Tester1', '', 'test1@pozo.com.au', '$2y$10$a7p8a7gONnd0v0T849w7Ee9o/iQ2NiQ3/aDT1pIenXhB/bi/5ycvi', NULL, '2016-05-08 14:00:42', '2016-05-08 14:00:42', '', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'Mark Carroll1', '', 'mark1@pozo.com.au', '$2y$10$BSpYwX/ad62DBj8nrBxod.9P946BFd0dwa1NRvFk7wXK2L99RalQC', NULL, '2016-05-08 14:01:06', '2016-05-08 14:01:06', '', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'Mark Carroll13', '', 'mark13@pozo.com.au', '$2y$10$WHOAfg5mAetgC3s49jEz.OPIdEACPDhK2l6Tl5Pnh2beRNGhd84ji', NULL, '2016-05-08 14:02:03', '2016-05-08 14:02:03', '', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, 'Mark Carroll134', 'mark-carroll134', 'mark13@pozo.com.au3', '$2y$10$5edF4UG11abxB3QR0tfexexWuA.cjcvFVDYLtNHPgERmKHuF2Sfjy', NULL, '2016-05-08 14:02:37', '2016-05-08 15:02:29', '', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'Mark Carroll234', '', '1234123mark@pozo.com.au', '$2y$10$g196yQqLXAxwtoaluGvG..rQoHIY4iSYuxq.yYvw1/goc/ykA7vrW', NULL, '2016-05-08 15:03:11', '2016-05-08 15:03:11', '', '05/09/2016', '', 'male', '475422227', '15', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, '0475422227', '0475422227', 'test-mark@pozo.com.au', '$2y$10$F/qW9JwqJNLIKDtybrT.uu.VPbAw63tdJVcb1L7U47NtdZ/bE/3fO', 'YBa61mUUpM33nzFW9ZV7ZXfyxtXOmG7aYYOsqjDL07B56EIGJtCipYbYHRBK', '2016-05-08 15:06:18', '2016-05-08 15:20:52', 'http://www.gravatar.com/avatar/52fe9e5c47146d071721196e9d87c8d7', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 1, 'voPGaFK4j04LHB4lBHInDskkdOINxH', '2016-05-09 01:08:57', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, 'Test', '', 'test@test.com', '$2y$10$fVbo5AGEtoLiPyjQBrw4NeUkwvzxDwIj6/Z8pUIB2/c31jYZQvzKa', 'Lt5NS5sv5wHjVq2K4RSBra9KIRoMGpkmNYN44mGyC6SQUpY9VcIc57EHN5pF', '2016-05-09 17:33:47', '2016-05-09 23:35:34', '', '', '', 'male', '', '15', '', '', '0000-00-00 00:00:00', 1, '', '2016-05-10 03:33:47', '', '', '', '', '', 'http://cccjobs.pozo.com.au/uploads/attachments/5731cef3944ef_file.pdf', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, 'Mark Carroll', 'mark-carroll', 'info@nulance.com.au', '$2y$10$OJ11C88twHjvXZDLJ4.uW.uqyZAdDOf.joH5Hzq/9V3gLBg.4U6eW', NULL, '2016-08-09 14:19:06', '2016-08-09 14:19:06', 'http://www.gravatar.com/avatar/0a824c3675a2582938ec4bdbbf691fea', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 0, 'NN2oOOxDZQBdUHoaqzKq9AJso08zAb', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, 'Sergey', 'Sergey', 'support@pozo.com.au', '$2y$10$m/Ca6RVterPRxivPwofi3uS1vo9MpKMio1yXisZuGXllknwyhnf3i', 'GYOAS1djRjQTsnMZCGcVkxWCkazWO3gm6O5yEbBNrcO8w0EYvEs5AbDvsWHq', '2016-09-07 17:38:22', '2016-09-07 20:15:46', 'http://www.gravatar.com/avatar/dad78511dc414baabd33ab00f825fb2f', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 1, 'qQ9xw0AcsattmiUR79Vj0Sb3TmyQys', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, '0123', '0123', 'sergey.markov@gmail.com', '$2y$10$ckP.JInOOsnKNKOA8XJdQ.z4PW2ATdWjd.Z3FKSTrUhrbq707GmxK', NULL, '2016-09-07 20:16:14', '2016-09-07 20:16:14', 'http://www.gravatar.com/avatar/ead20802a47a337220779e07c455b1f9', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 0, 'qBQyBI9KOtpEkYTdsjuFP81fdHREWh', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_company`
--

CREATE TABLE IF NOT EXISTS `users_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `users_company`
--

INSERT INTO `users_company` (`id`, `user_id`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, 1, '2016-05-08 14:02:37', '2016-05-08 14:02:37'),
(10, 10, 1, '2016-05-08 15:03:11', '2016-05-08 15:03:11');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-09-09 01:52:02', '2015-09-09 01:52:02'),
(3, 2, 1, '2016-04-29 10:15:44', '2016-04-29 10:15:44'),
(4, 2, 1, '2016-04-29 10:15:54', '2016-04-29 10:15:54'),
(10, 6, 3, '2016-05-08 14:00:42', '2016-05-08 14:00:42'),
(11, 7, 3, '2016-05-08 14:01:06', '2016-05-08 14:01:06'),
(12, 8, 3, '2016-05-08 14:02:03', '2016-05-08 14:02:03'),
(24, 9, 3, '2016-05-08 15:02:29', '2016-05-08 15:02:29'),
(25, 10, 3, '2016-05-08 15:03:11', '2016-05-08 15:03:11'),
(28, 11, 2, '2016-05-08 15:08:57', '2016-05-08 15:08:57'),
(29, 12, 4, '2016-05-09 17:33:47', '2016-05-09 17:33:47'),
(30, 5, 3, '2016-05-31 18:26:33', '2016-05-31 18:26:33'),
(31, 3, 2, '2016-05-31 18:28:43', '2016-05-31 18:28:43'),
(32, 13, 2, '2016-08-09 14:19:06', '2016-08-09 14:19:06'),
(33, 4, 3, '2016-08-09 14:32:13', '2016-08-09 14:32:13'),
(34, 14, 4, '2016-09-07 17:38:22', '2016-09-07 17:38:22'),
(35, 15, 2, '2016-09-07 20:16:14', '2016-09-07 20:16:14');

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE IF NOT EXISTS `views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `date_viewed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `job_id`, `date_viewed`, `updated_at`, `created_at`) VALUES
(1, 4, '2016-05-10 08:30:35', '2016-05-10 04:30:35', '2016-05-10 04:30:35'),
(2, 4, '2016-05-10 08:31:01', '2016-05-10 04:31:01', '2016-05-10 04:31:01'),
(3, 4, '2016-05-10 08:42:14', '2016-05-10 04:42:14', '2016-05-10 04:42:14'),
(4, 4, '2016-05-10 09:02:35', '2016-05-10 05:02:35', '2016-05-10 05:02:35'),
(5, 4, '2016-05-10 09:02:47', '2016-05-10 05:02:47', '2016-05-10 05:02:47'),
(6, 4, '2016-05-10 09:03:10', '2016-05-10 05:03:10', '2016-05-10 05:03:10'),
(7, 4, '2016-05-10 09:04:18', '2016-05-10 05:04:18', '2016-05-10 05:04:18'),
(8, 4, '2016-05-10 10:25:06', '2016-05-10 06:25:06', '2016-05-10 06:25:06'),
(9, 4, '2016-05-10 11:06:29', '2016-05-10 07:06:28', '2016-05-10 07:06:28'),
(10, 4, '2016-05-10 11:07:24', '2016-05-10 07:07:24', '2016-05-10 07:07:24'),
(11, 4, '2016-05-10 11:36:01', '2016-05-10 07:36:01', '2016-05-10 07:36:01'),
(12, 5, '2016-05-10 11:38:48', '2016-05-10 07:38:48', '2016-05-10 07:38:48'),
(13, 4, '2016-05-10 12:21:54', '2016-05-10 08:21:54', '2016-05-10 08:21:54'),
(14, 4, '2016-05-10 12:22:01', '2016-05-10 08:22:01', '2016-05-10 08:22:01'),
(15, 4, '2016-05-11 00:05:23', '2016-05-10 20:05:22', '2016-05-10 20:05:22'),
(16, 4, '2016-06-01 08:17:59', '2016-06-01 04:17:59', '2016-06-01 04:17:59'),
(17, 3, '2016-06-01 08:18:54', '2016-06-01 04:18:54', '2016-06-01 04:18:54'),
(18, 5, '2016-08-10 04:56:26', '2016-08-10 00:56:26', '2016-08-10 00:56:26'),
(19, 5, '2016-08-10 04:56:38', '2016-08-10 00:56:38', '2016-08-10 00:56:38'),
(20, 5, '2016-08-10 04:58:52', '2016-08-10 00:58:52', '2016-08-10 00:58:52'),
(21, 5, '2016-08-10 06:45:03', '2016-08-10 02:45:03', '2016-08-10 02:45:03'),
(22, 5, '2016-08-10 22:00:47', '2016-08-10 18:00:47', '2016-08-10 18:00:47'),
(23, 5, '2016-08-18 07:08:28', '2016-08-18 03:08:28', '2016-08-18 03:08:28'),
(24, 5, '2016-08-19 02:15:58', '2016-08-18 22:15:58', '2016-08-18 22:15:58'),
(25, 5, '2016-08-19 02:23:08', '2016-08-18 22:23:08', '2016-08-18 22:23:08'),
(26, 5, '2016-08-19 07:11:44', '2016-08-19 03:11:44', '2016-08-19 03:11:44');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
