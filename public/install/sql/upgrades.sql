DROP TABLE IF EXISTS `upgrades`;
CREATE TABLE `upgrades` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `upgrades` (`id`, `name`, `price`, `created_at`, `updated_at`, `description`, `type`) VALUES
(1,	'Feature in our newsletter',	35,	'2016-08-10 02:26:54',	'2016-08-09 12:26:54',	'Feature your job in our weekly newsletter sent to contact centre industry professionals along with key news & articles. Great way to attract passive job seekers!',	'per week/edition'),
(2,	'Feature on our Facebook',	50,	'2016-08-10 02:26:46',	'2016-08-09 12:26:46',	'Feature your job on our Facebook page - perfect for agent level roles',	'2 posts per week'),
(3,	'Feature on our home page',	75,	'2016-08-09 12:31:54',	'2016-08-09 12:31:54',	'Feature your job on our website with a dedicated advertising tile on our home page',	'per week'),
(4,	'Feature on our website',	130,	'2016-08-09 12:31:54',	'2016-08-09 12:31:54',	'Feature your job on our website with a dedicated leaderboard add and advertising tile across all website pages',	'per week'),
(5,	'VIP',	245,	'2016-08-09 12:31:54',	'2016-08-09 12:31:54',	'Executive Upgrade package - perfect for senior roles',	'per week');
