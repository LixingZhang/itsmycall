@extends('layouts.master')

@section('extra_css')
@if( ! empty($seo_keywords))
    <meta name="keywords" content="{{$seo_keywords->value_txt}}">
@endif
@if( ! empty($seo_description))
    <meta name="description" content="{{$seo_description->value_txt}}">
@endif
@endsection

@section('google_adwards')
    {!! $google_adwards_tracking_code->value_txt !!}
@endsection

@section('facebook_pixel')
    {!! $facebook_pixel_tracking_code->value_txt !!}
@endsection

@section('content')
<style type="text/css">
    @media only screen and (min-width: 767px) {
    .alignborder {
        border-right: 2px solid #e8efe8;
    }
    }
    @media only screen and (max-width: 767px) {
    .alignwidth {
        padding-top: 25px !important;
    }
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css">
<div class="block-section ">
    <div class="container">
                <div class="row">
                    <div class="col-sm-6 alignborder">
                        <div class="col-md-11">
                        <h3 style="font-weight: bold;margin-top: 7px;margin-bottom: 20px;">Login with Email</h3>
                        @include('admin.layouts.notify')
                        @if(isset($message))
                        <div class="alert alert-success">To create a job alert, please login or create a free account account <a style="color:navy!important" href="/register-user">here</a></div>
                        @endif
                        <!-- form login -->
                        <form action="/login" method="POST">
                         {{ csrf_field() }}
                            <div class="form-group">
                                <input type="email"  name="email" class="form-control" placeholder="Your Email" value="<?php if (isset($_GET['e'])) {
                                            echo $_GET['e'];
                                        } ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password"  class="form-control" placeholder="Your Password">
                            </div>
                            <div class="form-group" style="margin-bottom: 5px;">
                                <div class="row">
                                    <div class="col-xs-6" style="padding-top: 4px;">
                                        <div class="checkbox flat-checkbox">
                                            <label>
                                                <input type="checkbox">
                                                <span class="fa fa-check"></span>
                                                Remember me?
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="help-block"><a href="#myModal" data-toggle="modal">Forgot password?</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group no-margin">
                                <button class="btn btn-theme btn-lg btn-t-primary btn-block">Log In</button>
                                <p style="font-size: 15px;text-align: center;margin-top: 7px;margin-bottom: 0;">New to Itsmycall?<a href="/register"> Register Now</a></p>
                            </div>
                        </form><!-- form login -->
                        </div>
                    </div>
                    <div class="col-sm-4 alignwidth" style="padding-top: 55px; text-align: -webkit-center;">
                          <a href="/connect/facebook" class="btn btn-block btn-lg btn-social btn-facebook" style="font-size: 15px; max-width: 280px; margin-bottom: 10px;">
                            <span class="fa fa-facebook"></span>  Login with Facebook
                          </a>
                          <a href="/connect/twitter" class="btn btn-block btn-lg btn-social btn-twitter" style="font-size: 15px; max-width: 280px; margin-bottom: 10px;">
                            <span class="fa fa-twitter"></span>  Login with Twitter
                          </a>
                          <a href="/connect/linkedin" class="btn btn-block btn-lg btn-social btn-linkedin" style="font-size: 15px; max-width: 280px; margin-bottom: 10px;">
                            <span class="fa fa-linkedin"></span>  Login with LinkedIn
                          </a>
                          <a href="/connect/google" class="btn btn-block btn-lg btn-social btn-google" style="font-size: 15px; max-width: 280px; margin-bottom: 10px;">
                            <span class="fa fa-google"></span>  Login with Google
                          </a>
                    </div>


                </div>
    </div>


</div>


<!-- modal forgot password -->
<div class="modal fade" id="myModal" >
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form action="/forgot-password" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Enter Your Email</label>
                        <input type="email" class="form-control " name="email" placeholder="Email">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-theme" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-theme">Send</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- end modal forgot password -->
@endsection