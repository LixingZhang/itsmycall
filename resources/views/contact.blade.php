@extends('layouts.master')


@section('content')
<div class="container min-hight" style="padding-top:20px">
    <div class="row" >
        <div class="col-md-9 col-sm-9"  style="padding-bottom:20px">
            <h2>Contact</h2>
            <p>We are committed to providing the best jobs website for the Australian contact centre industry.</p>
            <p>If you'd like to contact us for any reason you can contact us during business hours on <a href="tel:+61 1300 487 692">1300 ITSMYCALL (1300 487 692)</a> or though our Live Chat function <here>. If you'd rather send us a message please email us at <a href="mailto:hello@itsmycall.com.au">hello@itsmycall.com.au</a> or complete the form below and we will contact you within 24 hours. </p>
                <div class="space20"></div>
                <!-- BEGIN FORM-->
                @include('admin.layouts.notify')

                <form action="/contact" method="post" class="horizontal-form margin-bottom-40" role="form">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="name" name="name" class="form-control" placeholder="Your Name">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>
                    <div class="form-group">
                        <label>Subject</label>
                        <select name="subject" class="form-control">
                            <option>Posting a job</option>
                            <option>Searching for a job</option>
                            <option>Website feedback</option>
                            <option>Applicant Tracking Integration </option>
                            <option>Bulk discounts</option>
                            <option>Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Your Email">
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea class="form-control" name="message" rows="8"></textarea>
                    </div>


                    <div class="form-group no-margin">
                        <button type="submit" class="btn btn-theme btn-md-3 btn-t-primary pull-right">Send</button>
                    </div>

                </form>
                <!-- END FORM-->
        </div>

        <div class="col-md-3 col-sm-3">
            <h2>Our Contacts</h2>
            <address>
                <strong>Phone:</strong><br><a href="tel:+61 1300 487 692">1300 ITSMYCALL (1300 487 692)</a><br>
                <strong>Email</strong><br>
                <a href="mailto:hello@itsmycall.com.au">hello@itsmycall.com.au</a><br>
                <a href="mailto:support@itsmycall.com.au">support@itsmycall.com.au</a>
            </address>


            <div class="clearfix margin-bottom-30"></div>


        </div>
    </div>
</div>

@endsection