<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2 style="color: #639941; font-size: 14px; font-weight: 100;">Hi Admin, a new job has just been posted</h2>
<p><b>Job Title: </b><span style="font-weight: 100;">{{$subject}}</span></p>
<p><b>Company: </b><span style="font-weight: 100;">{{$company}}</span></p>
<p><b>Job Level: </b><span style="font-weight: 100;">{{$job_level}}</span></p>
@if (!$upgrades->isEmpty())
    <p><b>Boosts:</b></p>
    @foreach($upgrades as $upgrade)
        <p>{{$upgrade->count}} X {{$upgrade->upgrade()->getResults()->name}}</p>
    @endforeach
@endif
<div>
    <br>
    To view this job, please use the link below:
    <br>
    <a href="{{ URL::to($slug) }}">{{$subject}}</a><br/>
</div>

</body>
</html>