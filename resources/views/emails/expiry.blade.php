<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div style="width:100%">
<a href="{{ URL::to('/') }}">
<img src="http://cccjobs.pozo.com.au/jobs.png" alt="logo" style="height:100px; margin-left: auto; margin-right: auto;">
</a>

<hr style="color: #639941;">


</div>
<h2 style="color: #639941;">Your Jobs Package will expire soon!</h2>
<p><b>Your package: {{$subject}}</b></p>
<p></p>
<p>Dear {{$name}},</p>
<p>This is a courtesy email to let you know you have {{$days_left}} days left to use your remaining balance of {{$balance}} before it expires. Remember you can use your credit for advertising jobs and purchasing Boosts!</p>

<p>Kind regards, </p>
<p></p>
<p>ItsMyCall Team </p>

</body>
</html>