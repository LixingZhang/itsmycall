<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<h3>First Name : {{ $firstname }}</h3>
<h3>Last Name : {{ $lastname }}</h3>
<h3>Company Name : {{ $company }}</h3>
<h3>Email : {{ $email }}</h3>
<h3>Contact Number : {{ $number }}</h3>
<h3>Enquiry Type : {{ $type }}</h3>

Message :
<div>
    {{ $enquiry }}<br/>
</div>

</body>
</html>
