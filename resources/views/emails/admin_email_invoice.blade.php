<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<?php 
$user = \App\Users::find($packageinfo->user_id);
$start = new \Carbon\Carbon($packageinfo->purchase_date);
$date_process = $start->addDays($invoiceterm);
 ?>
<h2 style="color: #639941; font-size: 14px; font-weight: 100;">Hi Admin, a new Invoice needs Actioning</h2>
@if($user && $user->name)
<p><b>Customer Name: </b><span style="font-weight: 100;">{{$user->name}}</span></p>
@endif
@if($user && $user->email)
<p><b>Customer Email: </b><span style="font-weight: 100;">{{$user->email}}</span></p>
@endif
<p><b>Value Pack Purchase ID: </b><span style="font-weight: 100;">{{$packageinfo->id}}</span></p>
<p><b>Value Pack Purchased: </b><span style="font-weight: 100;">{{$packageinfo->name}}</span></p>
<p><b>Value Pack Credits Remaining: </b><span style="font-weight: 100;">${{number_format($packageinfo->remaining, 2, '.', '')}}</span></p>
<p><b>Invoice Amount: </b><span style="font-weight: 100;">${{number_format($packageinfo->price, 2, '.', '')}}</span></p>
<p><b>Invoice Term: </b><span style="font-weight: 100;">{{$invoiceterm}} days</span></p>
<p><b>Invoice due date/time: </b><span style="font-weight: 100;">{{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}</span></p>

</body>
</html>