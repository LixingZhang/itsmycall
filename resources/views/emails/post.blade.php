<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<br>
<?php
$arr = explode(' ', trim($name));
$namewrite = $arr[0];
 ?>
<h2>Hi {{$namewrite}},</h2>
<h2 style="color: #639941; font-size: 15px; font-weight: 100; color: black; ">Good news! A new job has just been added that matches your Job Alert. Details of the job are:</h2>
<h4><b>Title:</b><span style="font-weight: 100;"> {{$subject}}</span></h4>
<h4><b>Company:</b><span style="font-weight: 100;"> {{$company}}</span></h4>
<div>
    To view the job just click this link: <a href="{{ URL::to($slug) }}">{{$subject}}</a><br/>
</div>
<h4 style="font-weight: 100; margin-bottom: 2px;">Best of Luck!</h4>
<h4 style="font-weight: 100;">Kind regards,</h4>
<a href="{{ URL::to($slug) }}"><img src="http://dev.itsmycall.com.au/jobs.png" alt="logo" style="width:auto; height: 50px; margin-left: 5px;"></a>
<br>
<p>To manage your Job Alert <a href="http://dev.itsmycall.com.au/jobalertsredirect/{{$email}}" style="font-weight: bold;">Click here</a></p>
<br>
</body>
</html>