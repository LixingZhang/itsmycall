@extends('layouts.master')

@section('extra_css')
@if( ! empty($seo_keywords))
    <meta name="keywords" content="{{$seo_keywords->value_txt}}">
@endif
@if( ! empty($seo_description))
    <meta name="description" content="{{$seo_description->value_txt}}">
@endif
@endsection

@section('google_adwards')
    {!! $google_adwards_tracking_code->value_txt !!}
@endsection

@section('facebook_pixel')
    {!! $facebook_pixel_tracking_code->value_txt !!}
@endsection


@section('content')
<div class="block-section ">
    <div class="container" style="padding-bottom:70px">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6" >


                        @include('admin.layouts.notify')
                        @if(isset($message))
                        <div class="alert alert-success">To create a job alert, please login or create a free account account <a style="color:navy!important" href="/register-user">here</a></div>
                        @endif
                        <!-- form login -->
                        <form action="/login" method="POST">
                         {{ csrf_field() }}
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email"  name="email" class="form-control" placeholder="Your Email" value="<?php if (isset($_GET['e'])) {
                                            echo $_GET['e'];
                                        } ?>">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password"  class="form-control" placeholder="Your Password">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="checkbox flat-checkbox">
                                            <label>
                                                <input type="checkbox">
                                                <span class="fa fa-check"></span>
                                                Remember me?
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="help-block"><a href="#myModal" data-toggle="modal">Forgot password?</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group no-margin">
                                <button class="btn btn-theme btn-lg btn-t-primary btn-block">Log In</button>
                            </div>
                        </form><!-- form login -->

                    </div>
                    <?php $advertiser_login_verify = \App\Settings::where('column_key','advertiser_login_verify')->first(); ?>
                    @if($advertiser_login_verify) {!!$advertiser_login_verify->value_txt!!} @endif


                </div>
            </div>
        </div>
    </div>


</div>


<!-- modal forgot password -->
<div class="modal fade" id="myModal" >
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form action="/forgot-password" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Enter Your Email</label>
                        <input type="email" class="form-control " name="email" placeholder="Email">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-theme" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-theme">Send</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- end modal forgot password -->
@endsection