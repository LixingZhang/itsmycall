@extends('layouts.master')


@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<style type="text/css">
        .swal2-content{
            line-height: inherit !important;
        }
        .helo{
            padding-top: 15px;
            padding-bottom: 15px;
            border: 2px solid green;
        }
        .featuredjob{
            background-color: #E5FFEC;
        }
        @media only screen and (max-width: 768px) {
            .removeborder1{
                border-right: 1px solid !important;
                border-bottom: 0 !important;
            }
            .removeborder2{
                border-right: 1px solid !important;
                border-bottom: 0 !important;
            }
            .removeborder3{
                border-right: 1px solid !important;
            }
        }
</style>
<?php use App\Categories; use App\SubCategories; use App\PostUpgrades;use Illuminate\Support\Str; ?>
<div class="bg-color1">
    <div class="container">
            <div class="col-md-3 col-sm-3">
                <div class="block-section text-center ">
                    <?php
                       $posts = \App\Posts::where('author_id',Auth::user()->id)->orderBy('id', 'desc')->first();
                     ?>
                    @if(Auth::user()->advertiser_type == 'private_advertiser')
                        @if(Auth::user()->avatar)
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                        @elseif($posts && $posts->featured_image)
                        <img style="max-width: 150px;" src="{{$posts->featured_image}}" class="img-rounded" alt="">
                        @endif
                    @endif
                    @if(Auth::user()->advertiser_type == 'recruitment_agency')
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                    @endif
                    <div class="white-space-20"></div>
                    <h4>{{Auth::user()->name}}</h4>
                    <div class="white-space-20"></div>
                    <ul class="list-unstyled">
                        <li><a href="/poster/edit-poster"> Edit Profile </a></li>
                        <li><a href="/poster/change_password"> Change Password</a></li>
                        <li><a href="/poster/applicants">View All Applicants</a></li>
                        <li><a href="/poster">View Active Jobs</a></li>
                        <li><a href="/poster/inactive"> Inactive Jobs</a></li>
                    </ul>
                    <div class="white-space-20"></div>
                    <a href="/poster/job_post" class="btn btn-primary btn-block" style="padding: 9px 12px;">Advertise a Job</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/buy_credits" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">Buy Credits</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/transaction" class="btn btn-info btn-block" style="background-color:#639941!important; color: white; border:0;">Transactions</a>
                    <div class="white-space-20"></div>
                    <a href="http://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support"
                       target="_blank"
                       class="btn btn-info btn-block">View Pricing and FAQ's</a>
                </div> 
        </div>
        <div class="col-md-9" style="margin-top:15px;">
            @include('admin.layouts.notify')
        </div>
        @if(sizeof($jobs)>0)

        <div class="col-md-9 col-sm-9">
                <!-- desc top -->
                <h3 class="orangeudnerline">Your Expired Jobs</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <form style="padding-bottom: 5px;" action="/poster/expiredsearch" method="GET" role="form">
                            <div class="input-group">
                                <input type="text" id="search" name="search" class="form-control" style="height: 34px;" placeholder="Search Jobs....">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default">Search</button>
                                </span>
                            </div>
                        </form>
                        @if($search)
                        <strong>Search results for: {{$search}}</strong>
                        @endif
                    </div>
                    <div class="col-sm-12">
                        <hr>
                        <span class="pull-right">
                            <select onchange="location = this.value;" class="selectpicker">
                                <option value="" disabled="disabled" selected style="display: none;">Sort By</option>
                                <option @if($option == 'latest') selected="selected" @endif value="/poster/expired/latest">Latest</option>
                                <option @if($option == 'oldest') selected="selected" @endif value="/poster/expired/oldest">Oldest</option>
                                <option @if($option == 'views') selected="selected" @endif value="/poster/expired/views">Views</option>
                                <option @if($option == 'applicants') selected="selected" @endif value="/poster/expired/applicants">Applicants</option>
                                <option @if($option == 'urlclicks') selected="selected" @endif value="/poster/expired/urlclicks">URL Clicks</option>
                            </select>
                        </span>
                    </div>
                </div>
            <!-- item list -->
            <div class="box-list">
             @foreach($jobs as $job)
                    <div class="row helo @if($job->posts_packages == 1) featuredjob @endif " style="margin:0;">
                               <div class="col-lg-6">

                                <?php 
                                  $category =  \App\Categories::find($job->category);
                                  $sub_category =  \App\SubCategories::find($job->subcategory);
                                ?>
                                @if($category && $sub_category)
                                <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" style="font-size: 25px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>
                                @else
                                <a href="/{{$job->slug}}" style="font-size: 25px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>
                                @endif
                                <br>
                                <small style="color: #777;">{{Categories::where('id', $job->category_id)->get()->first()? Categories::where('id', $job->category_id)->first()->title :''}} / {{SubCategories::where('id', $job->subcategory)->first()? SubCategories::where('id', $job->subcategory)->first()->title : ''}}</small>
                                <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
                                <div class="col-sm-12">
                                <div class="col-sm-3 text-center" style="padding: 0;">
                                    <div class="removeborder1" style="border:1px solid; border-right: 0; width: 100%; height: 60px;">
                                        <strong>
                                            Job Views
                                            <br>
                                            {{$job->views}}
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center" style="padding: 0;">
                                    <div class="removeborder2" style="border:1px solid; border-right: 0; width: 100%; height: 60px;">
                                        <strong>
                                            @if($job->email_or_link != '')
                                                URL Clicks
                                                <br>
                                                {{$job->out_count}}
                                            @else
                                                Applicants
                                                <br>
                                                {{$job->apps_count}}
                                            @endif
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center" style="padding: 0;">
                                    <div class="removeborder3" style="border:1px solid; width: 100%; height: 60px;">
                                        <strong>
                                            @if($job->email_or_link != '')
                                                Conversion
                                                <br>
                                                @if($job->views > 0)
                                                {{number_format(($job->out_count/$job->views)*100, 2, '.', '')}}%
                                                @else
                                                0.00%
                                                @endif
                                            @else
                                                Conversion
                                                <br>
                                                @if($job->views > 0)
                                                {{number_format(($job->apps_count/$job->views)*100, 2, '.', '')}}%
                                                @else
                                                0.00%
                                                @endif
                                            @endif
                                        </strong>
                                    </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="col-lg-6">
                                            <?php  $expires = new \Carbon\Carbon($job->expired_at);
                                                $created_at = new \Carbon\Carbon($job->created_at);
                                                $posted = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $created_at);
                                                $expired = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $expires);
                                            ?>
                                            <div class="row">
                                            <div class="col-sm-12">
                                            <small style="color: grey; padding-right: 15px; display: block;">Orginally posted on {{$posted->format('d M Y H:i:s')}}</small>
                                            <small style="color: grey; padding-right: 15px; padding-top: 2px;">Expired on {{$expired->format('d M Y')}} </small>
                                            </div>
                                            </div>
                                            <div class="row" style="padding-top: 14px;padding-bottom: 6px;">
                                            <div class="col-sm-12">
                                            <?php
                                               $boosts = \App\PostUpgrades::where('post_id',$job->id)->get();
                                             ?>
                                             @if(count($boosts) > 0)
                                            <div class="col-sm-3 text-center" style="padding-left: 5px; padding-right: 5px; margin-bottom: 5px;">
                                                <a href="/poster/boostdetails/{{$job->id}}" style="width: 100%; height: 60px;padding: 8px 0px;" class="btn btn-info">View<br>Boost</a>
                                            </div>
                                             @endif
                                            <?php
                                               $apply = \App\Applicants::where('job_id',$job->id)->get();
                                             ?>
                                             @if(count($apply) > 0)
                                            <div class="col-sm-3 text-center" style="padding-left: 5px; padding-right: 5px; margin-bottom: 5px;">
                                                <a href="/poster/applicants/{{$job->id}}" style="width: 100%; height: 60px;padding: 8px 0px;" class="btn btn-info">View<br>Applicants</a>
                                            </div>
                                             @endif
                                            </div>
                                            </div>
                                </div>
                            <div class="col-md-12">
                                <p style="padding-top:10px; padding-bottom: 10px; margin: 0;word-wrap: break-word;"><strong>Preview Text:</strong> {{\Illuminate\Support\Str::limit(strip_tags($job->short_description),700)}}</p>              
                            </div> 
                            <div class="col-md-12">
                                        <div class="table-responsive">
                                          <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                              <thead>
                                                  <tr>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Status</strong></td>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Company</strong></td>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Job Location</strong></td>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Salary</strong></td>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Job Ad Level</strong></td>
                                                      <td class="text-center"><strong>Work Location</strong></td>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                              </tfoot>
                                              <tbody>
                                                          <tr>
                                                              <td class="text-center" style="border-right: 1px solid #ddd;">
                                                                Expired
                                                              </td>
                                                              <td class="text-center" style="border-right: 1px solid #ddd;"><span><a href="/filter_company/{{$job->company}}">{{$job->company}}</a></span></td>
                                                              <td class="text-center" style="border-right: 1px solid #ddd;"><span><a href="https://www.google.com/maps/place/{{$job->joblocation}}">{{$job->joblocation}}</a></span></td>
                                                              <td class="text-center" style="border-right: 1px solid #ddd;">
                                                                @if($job->salarytype == "annual")
                                                                <span>Annual Salary: ${{number_format($job->salary)}}
                                                                </span>
                                                                @endif
                                                                @if($job->salarytype == "hourly")
                                                                <span>Hourly Rate: ${{number_format($job->hrsalary, 2)}}
                                                                </span>
                                                                @endif
                                                                <td class="text-center" style="border-right: 1px solid #ddd;"><span>
                                                                @if($job->posts_packages == 1) Featured @endif
                                                                @if($job->posts_packages == 2) Premium @endif
                                                                @if($job->posts_packages == 3) Standard @endif
                                                                </span></td>
                                                                <td class="text-center"><span>
                                                                @if($job->work_from_home == 1)
                                                                At business address
                                                                @endif 
                                                                @if($job->work_from_home == 2)
                                                                By mutual agreement
                                                                @endif 
                                                                @if($job->work_from_home == 3)
                                                                Work from home
                                                                @endif   
                                                                </span></td>
                                                              </span></td>
                                                          </tr>
                                              </tbody>
                                          </table>
                                      </div>
                            </div>                   
                    </div>
                    <br>
                @endforeach
                <!-- pagination -->
                <nav >
                    <ul class="pagination pagination-theme  no-margin pull-right">
                        {!! $jobs->render() !!}
                    </ul>
                </nav><!-- pagination -->

            </div>


        </div><!-- end box listing -->
        @elseif($filtermessage)
                <div class="col-md-9 col-sm-9" style="padding-top: 15px;">
                @if($search)
                <h3 class="orangeudnerline">No Results Found for {{$search}}</h3>
                @else
                <h3 class="orangeudnerline">No Results Found!</h3>
                @endif
                <div class="row">
                    <div class="col-sm-6">
                        <form style="padding-bottom: 5px;" action="/poster/expiredsearch" method="GET" role="form">
                            <div class="input-group">
                                <input type="text" id="search" name="search" class="form-control" style="height: 34px;" placeholder="Search Jobs....">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default">Search</button>
                                </span>
                            </div>
                        </form>
                    </div>

                </div>
                <hr>
                </div><!-- end box listing -->
        @else
                <div class="col-md-9 col-sm-9" style="padding-top: 25px;">
                    <!-- item list -->
                    <div class="box-list text-center">
                        <h3 class="orangeudnerline">You currently have no Expired Job.</h3>
                        <h4 style="margin-top: 25px;">To get started, click on the Advertise a Job button below.</h4>
                        <a href="/poster/job_post" style="margin-top: 25px;"><button type="button" style="margin-top: 25px;" class="btn btn-primary btn-block"><i class="fa fa-plus"></i>
                                Advertise a Job
                            </button></a>
                    </div>
                </div><!-- end box listing -->
        @endif

    </div>
</div>
</div>
<script type="text/javascript">
    $('.selectpicker').selectpicker({
      style: 'btn-default',
    });

</script>
<script type="text/javascript">
    if($('.helloref').val()){
        $refdesc = $('.helloref').val();
        swal({
        title: 'Welcome!',
        html: $refdesc,
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK, Got it!'
            })
    }
</script>
@endsection