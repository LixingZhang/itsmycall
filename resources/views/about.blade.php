@extends('layouts.master')


@section('content')
<div class="container min-hight" style="padding-top:20px">
    <div class="row" >
        <div class="col-md-9 col-sm-9"  style="padding-bottom:20px">
            <h2>About ItsMyCall.com.au</h2>
            <p>ItsMyCall is a dedicated jobs website that's been built by people with a genuine passion for the Australian contact centre industry.</p>
            <p>We understand the diverse skills, passion and expertise required to run a successful contact centre operations.  From the small offices with a few people on the phone through to the centres with thousands of employees, our aim is to have all the roles you will find in a contact centre all in the one location!   
            </p>
            <h4>Our Goal</h4>
            <p>To promote the diverse career opportunities available in the Australian contact centre industry and showcase it as a genuine career choice. </p>
            <h4>Our Partners</h4>
            <p><b>Auscontact Assocation</b> - the united voice for the customer contact industry in Australia, connecting people, industries and organisations, locally and globally, to deliver excellence in customer contact experience. <a href="https://www.auscontact.com.au/">Learn more</a></p>
            <h4>Our Partners</h4>
            <p><b>Contact Centre Central </b> - an online resource for contact centre professionals in Australia and a strong advocate for the contact centre industry.  The website contains latest news, a range of best practice tips, guides, articles as well as practical tools including a Business Directory, Events Calendar, Glossary and more. <a href="http://www.contactcentrecentral.com.au/">Visit website</a></p>
            <h4>Our Audience</h4>
            <p>By partnering with the Australian contact centre industries biggest players, we've got a great audience base to get us started! </p>
            <p>Both Auscontact and Contact Centre Central have a significant market presence from over 10k Page Likes on the Australian Call Centre Employees Facebook Page, thousands of followers across their respective LinkedIn company profiles and groups and over 10k unique visitors to their websites each month. </p>
            <p>This provides a great base to promote the ItsMyCall website and to encourage participation, all Auscontact Members with a current membership will receive a discount for all job placements.  </p>
            <h4>Marketing</h4>
            <p>Whilst we are incredibly grateful to be able to leverage the extensive base of our partner's contact centre audience, we also know we need to invest further to continue our growth. </p>
            <ul>
                <li>
                  Are investing in Google Adwords, LinkedIn advertising and Facebook advertising   
                </li>
                <li>
                  We have an extensive SEO strategy to ensure we are ranking well for contact centre jobs in Australia 
                </li><li>
                  We will be sponsoring and participating in a range of contact centre industry events promoting our website as the premium destination for all levels of contact centre roles
                </li>
                <li>
                  Will use our partners networks to continually promote ItsMyCall.com.au as the Contact Centre Industry's own jobs website.   
                </li>
                
            </ul>
        </div>
    </div>
</div>

@endsection