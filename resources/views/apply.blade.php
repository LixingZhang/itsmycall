<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2 style="color: #639941;">New candidate for {{$jtitle}} job</h2>
<div>
    <p>Congratulations, you just received another application! A copy of their CV is attached and a summary of their application is below:</p>
    <h4>Name:</h4>{{$name}}
    <h4>Email:</h4>{{$email}}
    <h4>Applicant Cover Note:</h4>
	<p>{{$comment}}</p>
    @if(!empty($reason1) || !empty($reason2) || !empty($reason3))
    <h4><strong>Reasons: <small>Why you should hire the candidate?</small></strong></h4>
    	@if($reason1) <p>1. {{$reason1}}</p> @endif
        @if($reason2) <p>2. {{$reason2}}</p> @endif
        @if($reason3) <p>3. {{$reason3}}</p> @endif
    @endif
    @if(count($answers) > 0)
    <h4><strong>Applicant answers to your Screening Questions</strong></h4>
    @if(count($answers) > 0)
        @for ($i = 0; $i <= count($answers)-1 ; $i++)
            <p id="Note" style="color: #639941;">{{$questions[$i]}} </p>
            <p id="Note">{{$answers[$i]}}</p>
        @endfor
    @endif
    @endif
    <br>
	<p>To view all your applicants, log into your account at <a href="https://www.itsmycall.com.au/login">https://www.itsmycall.com.au</a></p>
</div>

</body>
</html>