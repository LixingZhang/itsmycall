@extends('layouts.master')

@section('extra_js')
	@if( ! empty($seo_keywords))
    	<meta name="keywords" content="{{$seo_keywords}}">
	@endif
	@if( ! empty($seo_description))
    	<meta name="description" content="{{$seo_description}}">
	@endif
@endsection

@section('content')
<div class="container min-hight" style="padding-top:20px">
    <div class="row" >
        <div class="col-md-9 col-sm-9"  style="padding-bottom:20px">
            {!! $html !!}
        </div>
    </div>
</div>

@endsection
