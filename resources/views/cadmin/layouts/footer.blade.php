<div class="page-footer">
    <div class="page-footer-inner">
        2016 &copy; CCC Jobs - <a href="http://pozo.com.au" title="" target="_blank">A Pozo powered system.</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
