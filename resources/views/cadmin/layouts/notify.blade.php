@if(Session::has('success_msg'))
    <div class="alert alert-success">
        <strong>{{trans('messages.success')}}!</strong> {!! Session::get('success_msg') !!}
    </div>
@endif

@if(Session::has('error_msg'))
    <div class="alert alert-danger">
    {!! Session::get('error_msg') !!}
    </div>
@endif

@if(Session::has('info_msg'))
    <div class="alert alert-info">
        {!! Session::get('info_msg') !!}
    </div>
@endif
