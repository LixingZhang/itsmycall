@extends('layouts.master')


@section('content')
    <script>
        $(document).ready(function () {
            var category_el = $('#category')
            var $sub_category_select = $('#sub_category')

            category_el.on('change', function () {
                if (category_el.val().length) {
                    $.ajax({
                        url: '/api/get_sub_categories_by_category/' + $('#category').val(),
                        success: function (sub_categories) {
                            $sub_category_select.find('option').remove()

                            $.each(sub_categories, function (key, value) {
                                $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>')
                            })

                            var selected = [];
                            $('#sub_category').multipleSelect({
                                placeholder: 'Please Select a Category First...',
                                selectAll: false,
                                onClick: function(view) {
                                    var getSelects = $('#sub_category').multipleSelect('getSelects');
                                    if (getSelects.length > 3) {
                                        var selected = getSelects.slice(0, 3);
                                        $('#sub_category').multipleSelect("setSelects", selected);
                                    }
                                }
                            })

                            @if(json_decode($user->subcategory, true))
                            $('#sub_category').multipleSelect('setSelects', JSON.parse('{!! $user->subcategory !!}'))
                            @endif

                            @if(!empty(old('subcategory')))
                            <?php $old_subcategory = json_encode(old('subcategory'));?>
                            $('#sub_category').multipleSelect('setSelects', JSON.parse('{!! $old_subcategory !!}'))
                            @endif
                        },
                        error: function (response) {
                        }
                    })
                } else {
                    $sub_category_select.find('option').remove()
                    $sub_category_select.append('<option value="">Please Select a Category First...</option>')
                }
            }).trigger('change');


            $('#sub_category').multipleSelect({
                placeholder: 'Please Select a Category First...',
                selectAll: false,
                onClick: function(view) {
                    var getSelects = $('#sub_category').multipleSelect('getSelects');
                    if (getSelects.length > 3) {
                        var selected = getSelects.slice(0, 3);
                        $('#sub_category').multipleSelect("setSelects", selected);
                    }
                }
            });

            @if(json_decode($user->workfromhome, true))
                $('#workfromhome').selectpicker('val', JSON.parse('{!! $user->workfromhome !!}'))
            @endif

            @if(!empty(old('workfromhome')))
            <?php $old_workfromhome = json_encode(old('workfromhome'));?>
                $('#workfromhome').selectpicker('val', JSON.parse('{!! $old_workfromhome !!}'))
            @endif

            @if(json_decode($user->parentsoption, true))
                $('#parentsoption').selectpicker('val', JSON.parse('{!! $user->parentsoption !!}'))
            @endif

            @if(!empty(old('parentsoption')))
            <?php $old_parentsoption = json_encode(old('parentsoption'));?>
                $('#parentsoption').selectpicker('val', JSON.parse('{!! $old_parentsoption !!}'))
            @endif

            @if(json_decode($user->employment_term, true))
                $('#employment_term').selectpicker('val', JSON.parse('{!! $user->employment_term !!}'))
            @endif

            @if(!empty(old('employment_term')))
            <?php $old_employment_term = json_encode(old('employment_term'));?>
                $('#employment_term').selectpicker('val', JSON.parse('{!! $old_employment_term !!}'))
            @endif

        })
    </script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <script type="text/javascript">
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });

    </script>

<div class="block-section "   style="padding-bottom:130px">
    <div class="container">
        <div class="panel panel-lg">
            <div class="panel-body">
                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important">
                            <?php $stantext =  \App\Settings::where('column_key','stan_text')->first(); ?>
                            @if($stantext) {!!$stantext->value_txt!!} @endif
                        </div>
                    </div>

                    @include('admin.layouts.notify')

                    <!-- form login -->
                    <form action="/customer/editsavestandard" method="POST" novalidate>

                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" name="first-name"  value="{{ old('first-name', $user->first_name)}}" class="form-control required" placeholder="First Name" required>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" name="last-name"  value="{{ old('last-name', $user->last_name)}}" class="form-control required" placeholder="Last Name" required>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone"  value="{{ old('phone', $user->mobile_no)}}" class="form-control required" placeholder="Phone" required>
                        </div>								
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" disabled="disabled"  value="{{ old('email', $user->email)}}" class="form-control required" placeholder="Your Email" required>
                        </div>

                        <div class="form-group">
                            <label>Your Location</label>
                            <input type="suburb" id="suburb" name="suburb"  value="{{ old('suburb', $user->city)}}" class="form-control required" placeholder="Enter an address, suburb or state" required>
                        </div>	

                        <div class="form-group">
                            <label>State</label>
                            <select id="stateselect" name="state" class="form-control">
                                <option @if($user->state == "" || old('state') == "") selected="selected" @endif value="">Select State</option><option value="Australian Capital Territory">Australian Capital Territory</option>
                                <option @if($user->state == "New South Wales" || old('state') == "New South Wales") selected="selected" @endif value="New South Wales">New South Wales</option>
                                <option @if($user->state == "Northern Territory" || old('state') == "Northern Territory") selected="selected" @endif value="Northern Territory">Northern Territory</option>
                                <option @if($user->state == "Queensland" || old('state') == "Queensland") selected="selected" @endif value="Queensland">Queensland</option>
                                <option @if($user->state == "South Australia" || old('state') == "South Australia") selected="selected" @endif value="South Australia">South Australia</option>
                                <option @if($user->state == "Tasmania" || old('state') == "Tasmania") selected="selected" @endif value="Tasmania">Tasmania</option>
                                <option @if($user->state == "Victoria" || old('state') == "Victoria") selected="selected" @endif value="Victoria">Victoria</option>
                                <option @if($user->state == "Western Australia" || old('state') == "Western Australia") "selected"="selected" @endif value="Western Australia">Western Australia</option>
                            </select>
                        </div>	

                        <div class="form-group">
                            <label>PostCode</label>
                            <input type="text" name="postcode" id="postcode"  value="{{ old('postcode', $user->postcode)}}" class="form-control required" placeholder="Your Postcode" required>
                        </div>

                        <div class="form-group">
                            <label>Current Job Title (if applicable)</label>
                            <input type="currentposition" name="currentposition"  value="{{ old('currentposition', $user->currentposition)}}" class="form-control required" placeholder="Your Current Position" required>
                        </div>	

                        <div class="form-group">
                            <label>Current Company where you work (if applicable)</label>
                            <input type="text" name="lastwork"  value="{{ old('lastwork', $user->lastwork)}}" class="form-control required" placeholder="Current or Last company" required>
                        </div>

                        <div class="form-group">
                            <label>Time Spent in Current Role</label>
                            <select id="timeinrole" name="timeinrole" class="form-control" required>
                                <option  @if($user->timeinrole == "not_applicable" || old('timeinrole') == "not_applicable" ) selected=selected @endif value="6 months">Not Applicable</option>
                                <option @if($user->timeinrole == "6 months" || old('timeinrole') == "6 months" ) selected=selected @endif value="6 months">Less than 6 months</option>
                                <option @if($user->timeinrole == "6-12 months" || old('timeinrole') == "6-12 months") selected=selected @endif value="6-12 months">6-12 months</option>
                                <option @if($user->timeinrole == "1-2 years" || old('timeinrole') == "1-2 years") selected=selected @endif value="1-2 years">1-2 years</option>
                                <option @if($user->timeinrole == "2-4 years" || old('timeinrole') == "2-4 years") selected=selected @endif value="2-4 years">2-4 years</option>
                                <option @if($user->timeinrole == "5-10 years" || old('timeinrole') == "5-10 years") selected=selected @endif value="5-10 years">5-10 years</option>
                                <option @if($user->timeinrole == "10 + years" || old('timeinrole') == "10 + years") selected=selected @endif value="10 + years">10 + years</option>
                            </select>
                        </div>	

                        <div class="form-group">
                            <label>Your Availability</label>
                            <select id="availability" name="availability" class="form-control" required>
                                <option @if($user->availability == "Now" || old('availability') == "Now" ) selected=selected @endif value="Now">Now</option>
                                <option @if($user->availability == "within 1 week" || old('availability') == "within 1 week") selected=selected @endif value="within 1 week">within 1 week</option>
                                <option @if($user->availability == "within 2 weeks" || old('availability') == "within 2 weeks") selected=selected @endif value="within 2 weeks">within 2 weeks</option>
                                <option @if($user->availability == "within 3 weeks" || old('availability') == "within 3 weeks") selected=selected @endif value="within 3 weeks">within 3 weeks</option>
                                <option @if($user->availability == "within 4 weeks" || old('availability') == "within 4 weeks") selected=selected @endif value="within 4 weeks">within 4 weeks</option>
                                <option @if($user->availability == "within 3 months" || old('availability') == "within 3 months") selected=selected @endif value="within 3 months">within 3 months</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="workfromhome" class="control-label required" required>Work Location</label>
                            <p style="color: green; margin-bottom: 0;">Select the options below about what type of job you are looking for</p>

                            <select name="workfromhome[]" id="workfromhome" class="form-control selectpicker" multiple title="Select a Work Location..." data-actions-box="true" data-selected-text-format="count">
                                    <option value="1" data-content="<img src='/Work is only at the business address.png' style='max-width:20px;'>  At business address">At business address</option>
                                    <option value="2" data-content="<img src='/There is potential to work from home.png' style='max-width:20px;'>  By mutual agreement">By mutual agreement </option>
                                    <option value="3" data-content="<img src='/Only display Work From Home Jobs.png' style='max-width:20px;'>  Work from home">Work from home</option>
                            </select>
                        </div>  
                        <div class="form-group">
                            <label for="parentsoption" class="control-label required" required>Shift Guide (between these hours)</label>

                            <select name="parentsoption[]" id="parentsoption" class="form-control selectpicker" multiple title="Select Shift Time..." data-actions-box="true" data-selected-text-format="count">
                                            <option value="1" data-content="<img src='/Standard Business Hours.png' style='max-width:20px;'>  Standard (9am-5pm Mon to Fri)">Standard (9am-5pm Mon to Fri)</option>
                                            <option value="2" data-content="<img src='/Extended Business Hours.png' style='max-width:20px;'>  Extended (8am-8pm Mon to Fri)">Extended (8am-8pm Mon to Fri)</option>
                                            <option value="3" data-content="<img src='/Parent Friendly.png' style='max-width:20px;'>  Parent Friendly (9am-3pm Mon to Fri)">Parent Friendly (9am-3pm Mon to Fri)</option>
                                            <option value="4" data-content="<img src='/Afternoon Shift.png' style='max-width:20px;'>  Afternoon (12pm-12am Mon to Fri)">Afternoon (12pm-12am Mon to Fri)</option>
                                            <option value="5" data-content="<img src='/Night Shift.png' style='max-width:20px;'>  Night Shift">Night Shift</option>
                                            <option value="6" data-content="<img src='/Rotating Shifts no weekends.png' style='max-width:20px;'>  Rotating Shifts - no weekends">Rotating Shifts - no weekends</option>
                                            <option value="7" data-content="<img src='/Rotating shifts - including weekend work.png' style='max-width:20px;'>  Rotating Shifts - including weekend work">Rotating Shifts - including weekend work</option>
                            </select>

                        </div> 	

                        <div class="form-group">
                            <label for="category" class="control-label required" required>Select a Job Category </label>
                            <small><br>Please tell prospective employers what type of job you are looking for</small>

                            <select id="category" name="category" class="form-control">
                                <option value="" >Please Select..</option>
                                @foreach($categories as $category)
                                <option value="{{$category->id}}" @if($category->id == $user->category || $category->id == old('category')) selected="selected" @endif>{{$category->title}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label for="sub_category" class="control-label">Secondary Category</label>
                            <small><br>Please select up to a maximum of 3 sub-categories</small>
                            <select id="sub_category" name="subcategory[]" class="form-control" multiple>
                                <option value="" >Please Select a Category First...</option>
                                @foreach($subcategories as $subcategory)
                                    @if($subcategory->parent_id == $user->category)
                                        <option value="{{$subcategory->id}}" @if($subcategory->id == $user->subcategory || $subcategory->id == old('subcategory')) selected="selected" @endif >{{$subcategory->title}}</option>
                                    @endif
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                                <label for="employment_type" class=" control-label required">{{trans('messages.post_employment_type')}}</label>
                                <small><br>Please tell prospective employers what type of employment you are looking for</small>
                                <select id="employment_type" name="employment_type" class="form-control">
                                    <option @if($user->employment_type == "" || old('employment_type') == "") selected='selected' @endif value="">{{trans('messages.post_employment_select_type')}}</option>
                                    <option @if($user->employment_type == "part_time" || old('employment_type') == "part_time") selected='selected' @endif value="part_time">{{trans('messages.post_employment_type_part_time')}}</option>
                                    <option @if($user->employment_type == "full_time" || old('employment_type') == "full_time") selected='selected' @endif value="full_time">{{trans('messages.post_employment_type_full_time')}}</option>
                                    <option @if($user->employment_type == "casual" || old('employment_type') == "casual") selected='selected' @endif value="casual">{{trans('messages.post_employment_type_casual')}}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="employment_term" class=" control-label required">{{trans('messages.post_employment_term')}}</label>
                                <small><br>Please tell prospective employers what type of employment Status you are looking for</small>
                                <select id="employment_term" name="employment_term[]" class="form-control selectpicker" multiple title="Select Employment Status..." data-actions-box="true" data-selected-text-format="count">
                                    <option value="permanent">{{trans('messages.post_employment_permanent')}}</option>
                                    <option value="fixed">{{trans('messages.post_employment_fixed_term')}}</option>
                                    <option value="temp">{{trans('messages.post_employment_temp')}}</option>
                                </select>

                            </div>

                        <div class="form-group no-margin">
                            <button class="btn btn-theme btn-lg btn-t-primary btn-block">Opt In</button>
                        </div>
                    </form><!-- form login -->

                </div>

                <div class="col-md-6">

                </div>

            </div>
        </div>

    </div>
</div>
@endsection

@section('extra_js')
<script>
    $(function () {
        $('#is_aus_contact_member').on('change', function () {
            if ($('#is_aus_contact_member').val() == 'Yes') {
                $('#aus_contact_member_no').show();
                $('#aus_contact_member_no0').show();
                $('#aus_contact_member_no').focus();
                $('#aus_contact_member_no01').hide();
            } else {
                $('#aus_contact_member_no').val('');
                $('#aus_contact_member_no').hide();
                $('#aus_contact_member_no0').hide();
                $('#aus_contact_member_no01').show();
            }
        });
    });



</script>
<script>  
$(function () {
            //result.address_components[0]
            $("#suburb").geocomplete({
                details: ".details",
                detailsAttribute: "data-geo",
                country: "AU"
            }).bind("geocode:result", function (event, result) {
                geo_pickup = true;
                //console.log(result.address_components);

                results = result.address_components;

                $.each(results, function (index, value) {
                    if (value.types == 'postal_code') {
                        $("#postcode").val(value.long_name);
                        //console.log(value.long_name); // here you get the zip code
                    } else if (value.types[0] == 'locality') {
                        $("#city").val(value.long_name);
                    } else if (value.types[0] == 'administrative_area_level_1') {
                        $('#stateselect option[value="' + value.long_name + '"]').attr('selected', 'selected');
                        $("#stateselect").val(value.long_name).change();
                        $("#state").val(value.long_name);
                    } else if (value.types[0] == 'country') {
                        $("#country").val(value.long_name);
                    }
                });

                //$("#postcode").val(result.address_components[5]['long_name']);
                //console.log(result.geometry.location.lat());
                $("#lat").val(result.geometry.location.lat());
                $("#lng").val(result.geometry.location.lng());
                //console.log(result.geometry.location.lng());
                //console.log(result);


            });
        });
</script>
@endsection