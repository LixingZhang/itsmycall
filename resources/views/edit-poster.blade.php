@extends('layouts.master')


@section('content')


<div class="bg-color1">
    <div class="container">
            <div class="col-md-3 col-sm-3">
                <div class="block-section text-center ">
                    <?php
                       $posts = \App\Posts::where('author_id',Auth::user()->id)->orderBy('id', 'desc')->first();
                     ?>
                    @if(Auth::user()->advertiser_type == 'private_advertiser')
                        @if(Auth::user()->avatar)
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                        @elseif($posts && $posts->featured_image)
                        <img style="max-width: 150px;" src="{{$posts->featured_image}}" class="img-rounded" alt="">
                        @endif
                    @endif
                    @if(Auth::user()->advertiser_type == 'recruitment_agency')
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                    @endif
                    <div class="white-space-20"></div>
                    <h4>{{Auth::user()->name}}</h4>
                    <div class="white-space-20"></div>
                    <ul class="list-unstyled">
                        <li><a href="/poster/change_password"> Change Password</a></li>
                        <li><a href="/poster/applicants">View All Applicants</a></li>
                        <li><a href="/poster">View Active Jobs</a></li>
                        <li><a href="/poster/inactive"> Inactive Jobs</a></li>
                        <li><a href="/poster/expired"> Expired Jobs</a></li>
                    </ul>
                    <div class="white-space-20"></div>
                    <a href="/poster/job_post" class="btn btn-primary btn-block" style="padding: 9px 12px;">Advertise a Job</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/buy_credits" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">Buy Credits</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/transaction" class="btn btn-info btn-block" style="background-color:#639941!important; color: white; border:0;">Transactions</a>
                    <div class="white-space-20"></div>
                    <a href="http://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support"
                       target="_blank"
                       class="btn btn-info btn-block">View Pricing and FAQ's</a>
                </div> 
        </div>
            <div class="col-md-9 col-sm-9">
                <!-- Block side right -->
                <div class="block-section box-side-account">
                    <h3 class="no-margin-top">Edit Profile</h3>
                    <hr/>
                    <div class="row">
                        @include('admin.layouts.notify')
                        <div class="col-md-10">
                            <!-- form login -->
                            <form action="/poster/editsave" method="POST" enctype="multipart/form-data">

                                {!! csrf_field() !!}
                                
                                <div class="form-group">
                                    <label for="Avatar">Upload New Avatar</label>
                                    <div class="input-group">

                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file" style="padding-top: 9px;">
                                                Add Avavtar  
                                                <input type="file" name="avatar">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control form-flat" readonly>
                                    </div>
                                    <div class="loader hidden">Uploading Avatar <img style="width: 25px;" src="/Spinner.gif" alt="Loading" title="Loading" /></div>
                                    <div class="done hidden"><i class="fa fa-check" aria-hidden="true"></i> Your Avatar has been uploaded Successfully!</div>

                                </div>

                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="company" value="{{ $user->company }}" class="form-control" required placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" required name="first-name"  value="{{ $user->first_name }}" class="form-control" placeholder="First Name">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" required name="last-name"  value="{{ $user->last_name }}" class="form-control" placeholder="Last Name">
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="phone"  value="{{ $user->phone  }}" class="form-control" placeholder="Phone">
                                </div>                              
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" disabled="disabled" name="email"  value="{{ $user->email }}" class="form-control" placeholder="Your Email">
                                </div>

                                <div class="form-group">
                                    <label for="advertiser_type">Are you a Recruitment Agency or Private Advertiser?</label>
                                    <select id="advertiser_type" required name="advertiser_type" class="form-control">
                                        <option value="">Please Select...</option>
                                        @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                            <option value="{{$type}}" {{$user->advertiser_type === $type || old('advertiser_type') === $type ? 'selected': ''}}>{{$label}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                        <label>State</label>
                                          <select id="state" name="state" class="form-control" required>
                                                <option @if($user->state == "" ) selected @endif value="">Select State</option>
                                                <option @if($user->state == "Australian Capital Territory" ) selected @endif value="Australian Capital Territory">Australian Capital Territory</option>
                                                <option @if($user->state == "New South Wales" ) selected @endif value="New South Wales">New South Wales</option>
                                                <option @if($user->state == "Northern Territory" ) selected @endif value="Northern Territory">Northern Territory</option>
                                                <option @if($user->state == "Queensland" ) selected @endif value="Queensland">Queensland</option>
                                                <option @if($user->state == "South Australia" ) selected @endif value="South Australia">South Australia</option>
                                                <option @if($user->state == "Tasmania" ) selected @endif value="Tasmania">Tasmania</option>
                                                <option @if($user->state == "Victoria" ) selected @endif value="Victoria">Victoria</option>
                                                <option @if($user->state == "Western Australia" ) selected @endif value="Western Australia">Western Australia</option>
                                                <option @if($user->state == "I don't live in Australia" ) selected @endif value="I don't live in Australia">I don't live in Australia</option>
                                            </select>
                                </div>

                                @if($user->aus_contact_member_no)
                                <div class="form-group">
                                    <label>Auscontact Membership Number</label>
                                    <input type="text" disabled="disabled" value="{{ $user->aus_contact_member_no }}" class="form-control">
                                </div>
                                @endif



                                <div class="form-group" id = 'aus_contact_member_no0' style="display:none">
                                    <label>Please enter your Auscontact Membership to receive your discount</label>
                                    <input type="text" style="display:none;" value="{{ old('aus_contact_member_no') }}" name="aus_contact_member_no" id="aus_contact_member_no" class="form-control" placeholder="Aus Contact member number" maxlength="10">
                                </div>

                                <div class="form-group" id = 'aus_contact_member_no01' style="display:none">
                                    <label>Auscontact Members receive specially negoitated rates on of our all Jobs Packages as well as a range of other benefits. <br> <a target="_blank" href="https://www.auscontact.com.au/member-services/membership-categories-and-fees ">Click here</a> for more information and to join.</label>
                                    <input type="text" style="display:none;" value="{{ old('aus_contact_member_no') }}" name="aus_contact_member_no" id="aus_contact_member_no" class="form-control" placeholder="Aus Contact member number" maxlength="10">
                                </div>
                                <h2 class="orangeudnerline">Billing Details</h2>
                                <div class="form-group">
                                    <label>Billing Company</label>
                                    <input type="text" name="billing_business"  value="{{ $user->billing_business }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Billing Phone Number</label>
                                    <input type="text" name="billing_number"  value="{{ $user->billing_number }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>ABN</label>
                                    <input type="text" name="billing_abn"  value="{{ $user->billing_abn }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Billing Address</label>
                                    <input type="text" name="billing_address"  value="{{ $user->billing_address }}" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Billing Email</label>
                                    <input type="text" name="billing_email"  value="{{ $user->billing_email }}" class="form-control" placeholder="">
                                </div>
                                <div class="white-space-10"></div>
                                <div class="form-group no-margin">
                                    <button class="btn btn-theme btn-lg btn-t-primary btn-block">Save</button>
                                </div>
                            </form><!-- form login -->
                        </div>
                    </div>
                </div><!-- end Block side right -->
            </div>

    </div>
</div>
<script type="text/javascript">
$('INPUT[type="file"]').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    if(this.files[0].size > 3000000){
        alert('This Image size should not be greater than 3MB.');
        this.value = ''; 
    }else{
    if($(this).val().length > 0){
        $( ".loader" ).removeClass('hidden');
        setTimeout(function () { 
            $( ".loader" ).addClass('hidden');
            $( ".done" ).removeClass('hidden');
         }, 6000);
    }
    }
});
</script>
@endsection

@section('extra_js')
<script>
    $(function () {
        $('#is_aus_contact_member').on('change', function () {
            if ($('#is_aus_contact_member').val() == 'Yes') {
                $('#aus_contact_member_no').show();
                $('#aus_contact_member_no0').show();
                $('#aus_contact_member_no').focus();
                $('#aus_contact_member_no01').hide();
            } else {
                $('#aus_contact_member_no').val('');
                $('#aus_contact_member_no').hide();
                $('#aus_contact_member_no0').hide();
                $('#aus_contact_member_no01').show();
            }
        });
    });
</script>
@endsection