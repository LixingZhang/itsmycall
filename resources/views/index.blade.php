@extends('layouts.master-home')

@section('extra_css')
@if( ! empty($seo_keywords))
    <meta name="keywords" content="{{$seo_keywords}}">
@endif
@if( ! empty($seo_description))
    <meta name="description" content="{{$seo_description}}">
@endif
@endsection

@section('extra_js')

   {!!$settings_general->general_conversion!!}

<script type="text/javascript" src="/assets/js/countries.js"></script>
<script>//populateCountries("country", "state");</script>
<script>populateCountries("country1", "state2");</script>
<script>

    var arCats = [];
    var arSubCats = [];
<?php foreach ($categories as $category) { ?>
        var oCatValKey = {
            id: "<?php echo $category->id ?>",
            title: "<?php echo $category->title ?>"
        };
        arCats.push(oCatValKey);

        var arSubCat = [];
    <?php foreach ($category->subcategory as $subcategory) { ?>
            var oSubCatValKey = {
                id: "<?php echo $subcategory->id ?>",
                title: "<?php echo $subcategory->title ?>"
            };
            arSubCat.push(oSubCatValKey);
    <?php } ?>

        arSubCats[oCatValKey.id] = arSubCat;
<?php } ?>

    function populateSubCats(categoryElementId, subcategoryElementId) {
        var select = document.getElementById(categoryElementId);
        var value = select.options[select.selectedIndex].value;

        var subcategoryElement = document.getElementById(subcategoryElementId);
        subcategoryElement.length = 0;
        subcategoryElement.options[0] = new Option('All Sub Categories', '-1');
        subcategoryElement.selectedIndex = 0;

        var arSubCat = arSubCats[value];
        for (var i = 0; i < arSubCat.length; i++) {
            var objSubCat = arSubCat[i];
            subcategoryElement.options[subcategoryElement.length] = new Option(objSubCat.title, objSubCat.id);
            if (subcategoryElement.getAttribute('value') == objSubCat.id) {
                subcategoryElement.value = subcategoryElement.getAttribute('value');
            }
        }
    }
    
    var selector = $('.panel-heading a[data-toggle="collapse"]');
            selector.on('click', function () {
                console.log("hekllo")
                console.log(this)
                var self = this;
                if ($(this).hasClass('collapsed')) {
                    $.each(selector, function (key, value) {
                        if (!$(value).hasClass('collapsed') && value != self) {
                            $(value).trigger('click');
                        }
                    });
                }
            });

    function populateMainCats(categoryElementId, subcategoryElementId) {
        var categoryElement = document.getElementById(categoryElementId);
        categoryElement.length = 0;
        categoryElement.options[0] = new Option('Select Category', '-1');
        categoryElement.selectedIndex = 0;

        for (var i = 0; i < arCats.length; i++) {
            var objCat = arCats[i];
            categoryElement.options[categoryElement.length] = new Option(objCat.title, objCat.id);
        }
        if (categoryElement.getAttribute('value')) {
            categoryElement.value = categoryElement.getAttribute('value');
        }

        // Assigned all countries. Now assign event listener for the states.
        if (subcategoryElementId) {
            categoryElement.onchange = function () {
                populateSubCats(categoryElementId, subcategoryElementId);
            };
            //populateSubCats(categoryElementId, subcategoryElementId);
        }
    }

    populateMainCats("category", "subcategory");


    $(function () {
        //result.address_components[0]
        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    console.log("Hello");
                    $("#state2").val(value.long_name).change();
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            console.log(result.geometry.location.lng());
            console.log(result);


        });

        $("#find").click(function () {
            $("#joblocation").trigger("geocode");
        });

        $('#find').keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                $("#joblocation").trigger("geocode");
            }
        });


        $("#examples a").click(function () {
            $("#joblocation").val($(this).text()).trigger("geocode");
            return false;
        });

        $('#searchour').on('change', function () {
                if ($('#searchour').is(':checked')) {
                    $('.salrwapperannual').addClass("hidden")
                    $('.hourlywapperannual').removeClass("hidden")
                } else {
                    $('.hourlywapperannual').addClass("hidden")
                    $('.salrwapperannual').removeClass("hidden")
                }
        });

    });


</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });
    });

</script>
<style type="text/css">
    .alert{
                margin-bottom: 3px !important;
        }
    h5{
        font-weight: 700;
        font-size: 13px;
    }
    .row-bor{
        border-right: 0px;
        min-height: ;
    }
    @media screen and (min-width: 995px) {
    .row-bor{
        border-right: 2px solid lightgray;
        min-height: 520px;
    }
    }
</style>
@stop

@section('content')
<?php 
$business_info = \App\Settings::where('column_key','business_info')->first();
$home_info = \App\Settings::where('column_key','home_info')->first();
$agreement_info = \App\Settings::where('column_key','agreement_info')->first();


$standard_info = \App\Settings::where('column_key','standard_info')->first();
$exten_info = \App\Settings::where('column_key','exten_info')->first();
$parent_info = \App\Settings::where('column_key','parent_info')->first();
$after_info = \App\Settings::where('column_key','after_info')->first();
$night_info = \App\Settings::where('column_key','night_info')->first();
$rotno_info = \App\Settings::where('column_key','rotno_info')->first();
$rotyes_info = \App\Settings::where('column_key','rotyes_info')->first();

$business_info_img = \App\Settings::where('column_key','business_info_img')->first();
$home_info_img = \App\Settings::where('column_key','home_info_img')->first();
$agreement_info_img = \App\Settings::where('column_key','agreement_info_img')->first();

$standard_info_img = \App\Settings::where('column_key','standard_info_img')->first();
$exten_info_img = \App\Settings::where('column_key','exten_info_img')->first();
$parent_info_img = \App\Settings::where('column_key','parent_info_img')->first();
$after_info_img = \App\Settings::where('column_key','after_info_img')->first();
$night_info_img = \App\Settings::where('column_key','night_info_img')->first();
$rotno_info_img = \App\Settings::where('column_key','rotno_info_img')->first();
$rotyes_info_img = \App\Settings::where('column_key','rotyes_info_img')->first();

?>
<div class="" style="background-color:white;">

    <div class="container">

        <div class="row text-left">
            <?php $front_page_logo_text = \App\Settings::where('column_key','front_page_logo_text')->first(); ?>
            @if($front_page_logo_text) {!!$front_page_logo_text->value_txt!!} @endif
            @if(\App\Settings::where('column_key','homestatuson')->first()->value_txt)
            <div style="max-width: 900px; padding-bottom: 15px; padding-left: 20px; padding-right: 20px;margin: 0 auto;">
                {!!\App\Settings::where('column_key','homestatus')->first()->value_txt!!}
            </div>
            @endif
            <div class="col-md-12">
                <div class="side-right">
                    @include('admin.layouts.notify')
                    <div class="panel panel-default">
                        <div class="panel-heading text-center" style="font-weight: 500; font-size: 30px; color: gray;">Start your job search</div>
                        <div class="panel-body">

                            <div class="result-filter">
                                <form action="/job_list" method="GET" role="form">
                                    <input id="sort" class="form-control" name="sort" type="hidden" value="date"/>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="row row-bor">
                                                <div class="col-md-12">
                                                <H3 style="padding-bottom: 20px;">Job Details</H3>
                                                </div>
                                                <div class="col-md-12">
                                                <h5>
                                                <a href="#s_collapse_2" data-toggle="collapse">Job Category</a>
                                                </h5>
                                                <div class="form-group ">
                                                    <div class="select-style">
                                                        <select class="form-control category" name="category" id="category" value="{{isset($filter['category']) ? $filter['category'] : ''}}">
                                                            @foreach($categories as $category)
                                                            @if (isset($filter['category']))
                                                            <option @if($category->id == $filter['category']) selected=selected @endif value="{{$category->id}}" >{{$category->title}}</option>
                                                            @else 
                                                            <option value="{{$category->id}}" >{{$category->title}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="col-md-12 text-center" style="padding-bottom: 10px;">
                                                           <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i> 
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="select-style">
                                                        <select class="form-control"
                                                                style="margin-top:15px;"
                                                                name="subcategory" id="subcategory">
                                                            <option value="-1">All Sub Categories</option>
                                                            @foreach($categories as $category)
                                                            @foreach($category->subcategory as $subcategory)
                                                            <option value="{{$subcategory->title}}">{{$subcategory->title}}</option>
                                                            @endforeach
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                            <h5 class="font-bold margin-b-20 ">
                                                <a href="#s_collapse_2" data-toggle="collapse">{{trans('messages.post_employment_type')}}</a>
                                            </h5>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="employment_type[]" value="{{\App\Posts::EMPLOYMENT_TYPE_PART_TIME}}">
                                                    {{trans('messages.post_employment_type_part_time')}}
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="employment_type[]" value="{{\App\Posts::EMPLOYMENT_TYPE_FULL_TIME}}">
                                                    {{trans('messages.post_employment_type_full_time')}}
                                                </label>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="font-bold margin-b-20">
                                            <a href="#s_collapse_3"
                                               data-toggle="collapse">{{trans('messages.post_employment_term')}}</a>
                                        </h5>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="employment_term[]"
                                                       value="{{\App\Posts::EMPLOYMENT_TERM_PERMANENT}}">
                                                {{trans('messages.post_employment_permanent')}}
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="employment_term[]"
                                                       value="{{\App\Posts::EMPLOYMENT_FIXED_TERM}}">
                                                Fixed-Term / Contract
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="employment_term[]"
                                                       value="{{\App\Posts::EMPLOYMENT_TEMP}}">
                                                {{trans('messages.post_employment_temp')}}
                                            </label>
                                        </div>
                                    </div>
                                        <div class="col-md-12">
                                            <h5 class="font-bold">
                                                <a href="#s_collapse_2" data-toggle="collapse">Job Keywords</a>
                                            </h5>
                                            <div class="form-group" >
                                                <input type="text" name="title" class="form-control" placeholder="Keywords">
                                            </div>
                                        </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4" style="">
                                            <div class="row row-bor">
                                            <div class="col-md-12">
                                                    <H3 style="padding-bottom: 20px;">Job Specifics</H3>
                                                </div>
                                            <div class="col-md-12">
                                            <h5>
                                            <a href="#s_collapse_2" data-toggle="collapse">Job Location</a>
                                            </h5>

                                            <div class="form-group" style="margin-bottom: 0px;" >
                                                <div class="form-group" style="margin-bottom: 5px;">
                                                    <input id="joblocation" class="form-control geocomplete" type="text" name="joblocation" placeholder="Enter an address, suburb or state" />
                                                    <input id="lat" class="form-control geocomplete" type="hidden" name="lat" />
                                                    <input id="lng" class="form-control geocomplete" type="hidden" name="lng" />
                                                    <input id="city" class="form-control geocomplete" type="hidden" name="city" />
                                                    <input id="postcode" class="form-control geocomplete" type="hidden" name="postcode" />
                                                </div>
                                            </div>

                                            <div class="col-md-12 text-center" style="padding-bottom: 5px;">
                                                    <h5>OR</h5>
                                                </div>
                                            <div class="form-group">
                                                <div class="form-group" style="display:none">
                                                    <select  id="country1" name="country" class="form-control"></select>
                                                    <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i>
                                                </div>
                                                <div class="form-group">
                                                    <select margin-top:15px;" id="state2"
                                                            name="state" class="form-control">
                                                        <option value="">Select State</option>
                                                        <option value="Australian Capital Territory">Australian
                                                            Capital Territory
                                                        </option>
                                                        <option value="New South Wales">New South Wales</option>
                                                        <option value="Northern Territory">Northern Territory
                                                        </option>
                                                        <option value="Queensland">Queensland</option>
                                                        <option value="South Australia">South Australia</option>
                                                        <option value="Tasmania">Tasmania</option>
                                                        <option value="Victoria">Victoria</option>
                                                        <option value="Western Australia">Western Australia</option>
                                                    </select>
                                                </div>
                                            </div>
                                                    </div>
                                        <div class="">
                                        <div class="col-md-12">
                                            <h5 class="font-bold ">Salary</h5>
                                        </div>
                                        <div class="salrwapperannual">
                                            <div class="col-md-6">
                                                <small>Min</small>
                                                <select class="form-control"  name="salstart" id="salstart">
                                                    <option selected="selected" value="0" data-reactid="$0">$0</option>
                                                    <option value="30000" data-reactid="$30000">$30k</option>
                                                    <option value="40000" data-reactid="$40000">$40k</option>
                                                    <option value="50000" data-reactid="$50000">$50k</option>
                                                    <option value="60000" data-reactid="$60000">$60k</option>
                                                    <option value="70000" data-reactid="$70000">$70k</option>
                                                    <option value="80000" data-reactid="$80000">$80k</option>
                                                    <option value="100000" data-reactid="$100000">$100k</option>
                                                    <option value="120000" data-reactid="$120000">$120k</option>
                                                    <option value="150000" data-reactid="$150000">$150k</option>
                                                    <option value="175000" data-reactid="$175000">$175k</option>
                                                    <option value="200000" data-reactid="$200000">$200k</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <small>Max</small>
                                                <select class="form-control" name="salend" id="salend">
                                                    <option value="0" data-reactid="$0">$0</option>
                                                    <option value="30000" data-reactid="$30000">$30k</option>
                                                    <option value="40000" data-reactid="$40000">$40k</option>
                                                    <option value="50000" data-reactid="$50000">$50k</option>
                                                    <option value="60000" data-reactid="$60000">$60k</option>
                                                    <option value="70000" data-reactid="$70000">$70k</option>
                                                    <option value="80000" data-reactid="$80000">$80k</option>
                                                    <option value="100000" data-reactid="$100000">$100k</option>
                                                    <option value="120000" data-reactid="$120000">$120k</option>
                                                    <option value="150000" data-reactid="$150000">$150k</option>
                                                    <option value="500000" selected="selected" data-reactid="$250000">$200k+</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="hourlywapperannual hidden">
                                            <div class="col-md-6">
                                                <small>Min Hourly Rate</small>
                                                <select class="form-control" name="hrsalstart" id="salstart">
                                                    <option value="0" data-reactid="$0">$0</option>
                                                    <option value="15" data-reactid="15"">$15</option>
                                                    <option value="20" data-reactid="25">$20</option>
                                                    <option value="25" data-reactid="35">$25</option>
                                                    <option value="30" data-reactid="40">$30</option>
                                                    <option value="35" data-reactid="50">$35</option>
                                                    <option value="40" data-reactid="60">$40</option>
                                                    <option value="50" data-reactid="70+">$50</option>
                                                    <option value="60" data-reactid="70+">$60</option>
                                                    <option value="70" data-reactid="70+">$80</option>
                                                    <option value="100" data-reactid="70+">$100</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <small>Max Hourly Rate</small>
                                                <select class="form-control" name="hrsalend" id="salend">
                                                    <option value="0" data-reactid="$0">$0</option>
                                                    <option value="15" data-reactid="15"">$15</option>
                                                    <option value="20" data-reactid="25">$20</option>
                                                    <option value="25" data-reactid="35">$25</option>
                                                    <option value="30" data-reactid="40">$30</option>
                                                    <option value="35" data-reactid="50">$35</option>
                                                    <option value="40" data-reactid="60">$40</option>
                                                    <option value="50" data-reactid="70+">$50</option>
                                                    <option value="60" data-reactid="70+">$60</option>
                                                    <option value="70" data-reactid="70+">$80</option>
                                                    <option value="100" data-reactid="70+">$100</option>
                                                    <option value="1000" selected="selected" data-reactid="70+">$100+</option>
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-md-12 text-center" style="padding-top: 20px;">
                                                <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="searchour" id="searchour" value="yes">
                                                Switch to Hourly Rates
                                            </label>
                                        </div>
                                            </div>
                                        <div class="col-md-12">
                                            <h5 class="font-bold" style="margin-top: 15px;">
                                                <a href="#s_collapse_2" data-toggle="collapse">Incentive Structure</a>
                                            </h5>
                                            <div class="form-group">
                                                <select id="incentivestructure" name="incentivestructure1" class="form-control">
                                                    <option value="">Any</option>
                                                    <option value="fixed">Base + Super</option>
                                                    <option value="basecommbonus">Base + Super + R&R/Bonus</option>
                                                    <option value="basecomm">Base + Super + Commissions</option>
                                                    <option value="commonly">Commissions Only</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row row-bor" style="border-right: 0px;">
                                                <div class="col-md-12">
                                                <H3 style="padding-bottom: 20px;">Job Preferences</H3>
                                                </div>
                                                <div class="col-md-12">
                                                    <h5 class="font-bold" >
                                                        <a href="#s_collapse_2" data-toggle="collapse">Shift Guide (between these hours)</a>
                                                    </h5>
                                                    <div class="form-group" style="margin-bottom: 10px;">
                                                        <select id="parentsfiltervalue" name="parentsoption" class="form-control selectpicker">
                                                            <option value=""> I'm open to any shift time</option>
                                                            <option value="1" data-content="<img src='{{$standard_info_img->value_string}}' style='max-width:25px;'>  Standard (9am-5pm Mon to Fri)">Standard (9am-5pm Mon to Fri)</option>
                                                            <option value="2" data-content="<img src='{{$exten_info_img->value_string}}' style='max-width:25px;'>  Extended (8am-8pm Mon to Fri)">Extended (8am-8pm Mon to Fri)</option>
                                                            <option value="3" data-content="<img src='{{$parent_info_img->value_string}}' style='max-width:25px;'>  Parent Friendly (9am-3pm Mon to Fri)">Parent Friendly (9am-3pm Mon to Fri)</option>
                                                            <option value="4" data-content="<img src='{{$after_info_img->value_string}}' style='max-width:25px;'>  Afternoon (12pm-12am Mon to Fri)">Afternoon (12pm-12am Mon to Fri)</option>
                                                            <option value="5" data-content="<img src='{{$night_info_img->value_string}}' style='max-width:25px;'>  Night Shift">Night Shift</option>
                                                            <option value="6" data-content="<img src='{{$rotno_info_img->value_string}}' style='max-width:25px;'>  Rotating Shifts - no weekends">Rotating Shifts - no weekends</option>
                                                            <option value="7" data-content="<img src='{{$rotyes_info_img->value_string}}' style='max-width:25px;'>  Rotating Shifts - including weekend work">Rotating Shifts - including weekend work</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <h5 class="font-bold " >
                                                        <a href="#s_collapse_2" data-toggle="collapse">Work Location</a>
                                                    </h5>
                                                    <div class="form-group">
                                                        <select id="workfromhome" name="work_from_home" class="form-control selectpicker">
                                                            <option value="">Happy to work anywhere</option>
                                                            <option value="1" data-content="<img src='{{$business_info_img->value_string}}' style='max-width:25px;'>  At business address">At business address</option>
                                                            <option value="2" data-content="<img src='{{$agreement_info_img->value_string}}' style='max-width:25px;'>  By mutual agreement"> By mutual agreement</option>
                                                            <option value="3" data-content="<img src='{{$home_info_img->value_string}}' style='max-width:25px;'>  Work from home">Work from home</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                <div class="col-md-12">
                                                <h5 class="font-bold" >
                                                    <a href="#s_collapse_2" data-toggle="collapse">Pay Frequency</a>
                                                </h5>
                                                <div class="form-group">
                                                    <select id="pay_cycle" class="form-control selectpicker" name="pay_cycle">
                                                        <option value="">Dont care, as long as I get paid!</option>
                                                        <option value="weekly">Weekly</option>
                                                        <option value="fortnightly">Fortnightly</option>
                                                        <option value="monthly">Monthly</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <h5 class="font-bold">
                                                    <a href="" data-toggle="collapse">Job Advertiser Type</a>
                                                </h5>
                                                <select class="form-control" name="advertiser_type">
                                                    <option value="both">Both</option>
                                                    @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                                        <option value="{{$type}}" {{old('advertiser_type') == $type ? 'selected' : ''}}>{{$label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                                
                                            </div>
                                        </div>



                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    </div>


                                    <div class="col-md-12" style="padding-bottom: 15px;padding-top: 30px;">
                                        <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary btn-block" style="font-weight: bold; font-size: 25px; height: auto;" ><i class="fa fa-search"></i> Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="block-section-sm side-right">
                    <h1 class=" text-center" style="margin-bottom:45px;">Welcome to ItsMyCall</h1>
                    <div class="col-md-8 col-md-offset-2">
                        <?php $video =  \App\Settings::where('column_key','front_page_video')->first(); ?>
                        @if($video) {!!$video->value_string!!} @endif 
                        <br>
                        {!!$html->front_page_text!!}
                        <a href="http://itsmycall.com.au/register"><button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i>
                                Open a free account
                            </button></a>
                    </div>

                    <div class="clearfix">
                    </div>
                </div>

                <div class="panel panel-default" style="margin-bottom: 45px">
                    <div class="panel-heading text-center" style="font-size: 22px;">Browse By Category</div>
                    <div class="panel-body">
                        <div class="col-md-12" style="padding-top: 25px;">
                            <div class="row multi-columns-row">

                                <!--<div class="panel-group" id="accordion">-->
                                @foreach($categories as $category)
                                <div class="col-xs-12 col-sm-4 col-lg-4">    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed smaller-mobile" data-parent="#accordion" href="#collapse{{$category->id}}">{{$category->title}} <span id="{{$category->title}}" class="badge pull-right jcount" style="background-color: #639941;">{{$category->count}}</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{$category->id}}" class="panel-collapse collapse">
                                            <ul class="list-group">
                                                @foreach($category->subcategory as $subcategory)
                                                <li class="list-group-item">
                                                    <a href="/category/{{$category->slug}}/{{$subcategory->slug}}" >{{$subcategory->title}}</a><span id="{{$subcategory->title}}" class="badge subcount">{{$subcategory->count}}</span>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                                <!--</div>-->
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    @endsection

    