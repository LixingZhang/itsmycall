@extends('layouts.master')

@section('extra_css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style type="text/css">
        .swal2-content{
            line-height: inherit !important;
        }
        .helo{
            padding-top: 15px;
            padding-bottom: 15px;
            border: 2px solid green;
        }
        .featuredjob{
            background-color: #E5FFEC;
        }
        @media only screen and (min-width: 992px) {
            .align_top{
                text-align: right;
            }
        }
</style>
@stop

@section('extra_js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@stop

@section('content')
<?php use App\Categories; use App\SubCategories; use App\PostUpgrades;?>
<div class="bg-color1">
    <div class="container">
            <div class="col-md-3 col-sm-3">
                <div class="block-section text-center ">
                    <?php
                       $posts = \App\Posts::where('author_id',Auth::user()->id)->orderBy('id', 'desc')->first();
                     ?>
                    @if(Auth::user()->advertiser_type == 'private_advertiser')
                        @if(Auth::user()->avatar)
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                        @elseif($posts && $posts->featured_image)
                        <img style="max-width: 150px;" src="{{$posts->featured_image}}" class="img-rounded" alt="">
                        @endif
                    @endif
                    @if(Auth::user()->advertiser_type == 'recruitment_agency')
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                    @endif
                    <div class="white-space-20"></div>
                    <h4>{{Auth::user()->name}}</h4>
                    <div class="white-space-20"></div>
                    <ul class="list-unstyled">
                        <li><a href="/poster/edit-poster"> Edit Profile </a></li>
                        <li><a href="/poster/change_password"> Change Password</a></li>
                        <li><a href="/poster/applicants">View All Applicants</a></li>
                        <li><a href="/poster/inactive"> Inactive Jobs</a></li>
                        <li><a href="/poster/expired"> Expired Jobs</a></li>
                    </ul>
                    <div class="white-space-20"></div>
                    <a href="/poster/job_post" class="btn btn-primary btn-block" style="padding: 9px 12px;">Advertise a Job</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/buy_credits" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">Buy Credits</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/transaction" class="btn btn-info btn-block" style="background-color:#639941!important; color: white; border:0;">Transactions</a>
                    <div class="white-space-20"></div>
                    <a href="http://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support"
                       target="_blank"
                       class="btn btn-info btn-block">View Pricing and FAQ's</a>
                </div> 
        </div>
        <div class="col-md-9" style="margin-top:15px;">
            @include('admin.layouts.notify')
        </div>
        @if(sizeof($applicants)>0)

        <div class="col-md-9 col-sm-9">
            <!-- desc top -->
            <?php if(isset($id)){ $job_detail = \App\Posts::find($id); } ?>
            @if(isset($job_detail))
            <h3 class="orangeudnerline">Applicants for Job: {{$job_detail->title}}</h3>
            @else
            <h3 class="orangeudnerline">Applicants</h3>
            @endif
                <div class="row">
                    <div class="col-md-4">
                      <p style="margin: 10px 2px 0px; font-size: 14px;font-weight: bold;">Job Title:</p>
                    <select onchange="location = this.value;" class="selectpicker show-tick" data-width="100%">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Jobs</option>
                            <option value="/poster/applicants">All</option>
                            @foreach($jobs as $job)
                            <option @if(isset($id) && $id == $job->id) selected="selected" @endif value="/poster/applicants/{{$job->id}}">{{$job->title}}</option>
                            @endforeach
                    </select>
                    </div>
                    <div class="col-md-4">
                      <p style="margin: 10px 2px 0px; font-size: 14px;font-weight: bold;">Active Status:</p>
                    <select onchange="location = this.value;" class="selectpicker show-tick" data-width="100%">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Still Active</option>
                            @if(isset($status))
                            <option @if(isset($filter) && $filter == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/all/status/{{$status}}">All</option>
                            <option @if(isset($filter) && $filter == 'no') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/no/status/{{$status}}">No</option>
                            <option @if(isset($filter) && $filter == 'yes') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/yes/status/{{$status}}">Yes</option>
                            @else
                            <option @if(isset($filter) && $filter == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}">All</option>
                            <option @if(isset($filter) && $filter == 'no') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/no">No</option>
                            <option @if(isset($filter) && $filter == 'yes') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/yes">Yes</option>
                            @endif
                    </select>
                    </div>
                    <div class="col-md-4">
                      <p style="margin: 10px 2px 0px; font-size: 14px;font-weight: bold;">Applicants Status:</p>
                    <select onchange="location = this.value;" class="selectpicker show-tick" data-dropup-auto="false"  data-size="auto" data-width="100%">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Status</option>
                            @if(isset($filter))
                            <option @if(isset($status) && $status == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/{{$filter}}/status/all">All</option>
                            @else
                            <option @if(isset($status) && $status == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/all/status/all">All</option>
                            @endif
                            @foreach(\App\ApplicantsStatus::where('deleted', false)->orderBy('order','asc')->get() as $applicantStatus)
                            @if(isset($filter))
                            <option @if(isset($status) && $applicantStatus->status == $status) selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/{{$filter}}/status/{{$applicantStatus->status}}">{{$applicantStatus->status}}</option>
                            @else
                            <option @if(isset($status) && $applicantStatus->status == $status) selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/all/status/{{$applicantStatus->status}}">{{$applicantStatus->status}}</option>
                            @endif
                            @endforeach
                    </select>
                    </div>
                </div>
            <hr/>
            <!-- item list -->
            <div class="box-list">
                @foreach($applicants as $applicant)
                    <div class="row helo" style="margin:0;">
                            <div class="col-md-12" style="padding-bottom: 10px;">
                                <div class="row">
                                  <?php $user = \App\Users::find($applicant->apply_id); ?>
                                  @if($user && $user->avatar)
                                   <div class="col-md-7">
                                     <div class="row">
                                        <div class="col-md-3" style="padding-bottom: 10px;">
                                            <img style="max-width: 90px;max-height: 90px;" src="{{$user->avatar}}">
                                        </div>
                                        <div class="col-md-9">
                                              <h3 style="color: #616060; font-weight: bold; margin-top: 0;">{{$applicant->name}}</h3>
                                              <?php $job = \App\Posts::find($applicant->job_id);?> 
                                              @if($job)
                                              <p class="no-margin">
                                              Job Title: <a target="_blank" href="/{{$job->slug}}" style="word-break: break-all;" class="featured-heading">{{$job->title}}</a>
                                              </p>
                                               <p style="color: #777;" class="no-margin" >Category: <small>{{Categories::where('id', $job->category_id)->get()->first()? Categories::where('id', $job->category_id)->first()->title :''}} / {{SubCategories::where('id', $job->subcategory)->first()? SubCategories::where('id', $job->subcategory)->first()->title : ''}}</small></p>
                                              @else
                                              <p class="no-margin">
                                              Job Title: <a href="" style="word-break: break-all;" class="featured-heading">{{$applicant->title}}</a>
                                              </p>
                                              @endif
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-5 align_top">
                                        <div class="row">
                                            <div class="col-sm-12" style="padding-bottom: 10px;">
                                             <strong>Keep Candidate Active: <input type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" @if($applicant->still_active) @else checked @endif value="{{$applicant->id}}" onchange="link(this);"></strong>
                                             </div>
                                             <div class="col-sm-12">
                                                 <strong>Allocate Status: </strong>
                                                     <select id="statusApp" onchange="saveStatus({{$applicant->id}},this.value)">
                                                        @foreach(\App\ApplicantsStatus::where('deleted', false)->orderBy('order','asc')->get() as $applicantStatus)
                                                            <option @if($applicant->status == $applicantStatus->status) selected="selected" @endif value="{{$applicantStatus->status}}">{{$applicantStatus->status}}</option>
                                                        @endforeach
                                                    </select>
                                             </div>
                                         </div>
                                    </div>
                                    @else
                                    <div class="col-md-6">
                                              <h3 style="color: #616060; font-weight: bold; margin-top: 0;">{{$applicant->name}}</h3>
                                              <?php $job = \App\Posts::find($applicant->job_id);?> 
                                              @if($job)
                                              <p class="no-margin">
                                              Job Title: <a href="/{{$job->slug}}" style="word-break: break-all;" class="featured-heading">{{$job->title}}</a>
                                              </p>
                                               <p style="color: #777;" class="no-margin" >Category: <small>{{Categories::where('id', $job->category_id)->get()->first()? Categories::where('id', $job->category_id)->first()->title :''}} / {{SubCategories::where('id', $job->subcategory)->first()? SubCategories::where('id', $job->subcategory)->first()->title : ''}}</small></p>
                                              @else
                                              <p class="no-margin">
                                              Job Title: <a href="" style="word-break: break-all;" class="featured-heading">{{$applicant->title}}</a>
                                              </p>
                                              @endif
                                    </div>
                                    <div class="col-md-6 align_top">
                                        <div class="row">
                                            <div class="col-sm-12" style="padding-bottom: 10px;">
                                             <strong>Keep Candidate Active: <input type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" @if($applicant->still_active) @else checked @endif value="{{$applicant->id}}" onchange="link(this);"></strong>
                                             </div>
                                             <div class="col-sm-12">
                                                 <strong>Allocate Status: </strong>
                                                     <select id="statusApp" onchange="saveStatus({{$applicant->id}},this.value)">
                                                        @foreach(\App\ApplicantsStatus::where('deleted', false)->orderBy('order','asc')->get() as $applicantStatus)
                                                            <option @if($applicant->status == $applicantStatus->status) selected="selected" @endif value="{{$applicantStatus->status}}">{{$applicantStatus->status}}</option>
                                                        @endforeach
                                                    </select>
                                             </div>
                                         </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                                <div class="col-md-12">
                                        <div class="table-responsive">
                                          <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                              <thead>
                                                  <tr>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Email</strong></td>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Phone</strong></td>
                                                      <td class="text-center" style="border-right: 1px solid #ddd;"><strong>Application Date</strong></td>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                    <td class="text-center" style="border-right: 1px solid #ddd;">
                                                    <span>{{$applicant->email}}</span>
                                                    </td>
                                                    <td class="text-center" style="border-right: 1px solid #ddd;">
                                                    <span>@if($applicant->phone){{$applicant->phone}} @else nil @endif</span>
                                                    </td>
                                                    <td class="text-center" style="border-right: 1px solid #ddd;">
                                                    <span>{{\Carbon\Carbon::parse($applicant->date_created)->format('d M Y H:i')}}</span>
                                                    </td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                      </div>
                                </div>
                                <div class="col-md-12" style="padding-top: 20px;">
                                    <div class="col-sm-2 text-center" style="padding-left: 0px; padding-right: 5px; margin-bottom: 5px;">
                                        <a target="_blank" href="{{$applicant->resume}}" style="height: 60px; width: 100%;padding: 8px 0px;" class="btn btn-info">View<br>Resume</a>
                                    </div>
                                    <div class="col-sm-2 text-center" style="padding-left: 0px; padding-right: 5px; margin-bottom: 5px;">
                                        <a data-toggle="modal" data-target="#coverModal{{$applicant->id}}" style="height: 60px; width: 100%;padding: 8px 0px;" class="btn btn-info">Cover<br>Letter</a>
                                    </div>
                                                    <div class="modal fade" tabindex="-1" id="coverModal{{$applicant->id}}" role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title">Applicant Cover Note</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p id="Note">{{$applicant->cover_note}}</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->

                                        @if(!empty($applicant->reason1) || !empty($applicant->reason2) || !empty($applicant->reason3))
                                        <div class="col-sm-3 text-center" style="padding-left: 0px; padding-right: 5px; margin-bottom: 5px;">
                                        <a data-toggle="modal"
                                               data-target="#ReasonModal{{$applicant->id}}" style="width: 100%; height: 60px;padding: 8px 0px;" class="btn btn-info">Why you should<br>hire them</a>
                                        </div>
                                        <div class="modal fade" tabindex="-1" id="ReasonModal{{$applicant->id}}" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Reasons:<small> Why you should hire the applicant?</small></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p id="Note">@if($applicant->reason1) 1. {{$applicant->reason1}} @endif</p>
                                                        <p id="Note"> @if($applicant->reason2) 2. {{$applicant->reason2}} @endif</p>
                                                        <p id="Note">@if($applicant->reason3) 3. {{$applicant->reason3}} @endif</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        @endif
                                    
                                    <?php
                                    $answers = [];
                                    $questions = [];
                                    $answer_count = NULL;
                                    if($applicant->answers){
                                        $answers = json_decode($applicant->answers);
                                        $questions = json_decode($applicant->questions);
                                        $answer_count = count($answers);
                                            }
                                     ?>
                                    @if($applicant->answers)
                                        <div class="col-sm-3 text-center" style="padding-left: 0px; padding-right: 5px; margin-bottom: 5px;">
                                            <a data-toggle="modal"
                                                   data-target="#AnswerModal{{$applicant->id}}" style="width: 100%; height: 60px;padding: 8px 0px;" class="btn btn-info">View Screening<br>Questions</a>
                                        </div>
                                        <div class="modal fade" tabindex="-1" id="AnswerModal{{$applicant->id}}" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Your Screening questions and applicant answers:</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        @if($answer_count)
                                                        @for ($i = 0; $i <= $answer_count-1 ; $i++)
                                                        <p id="Note" style="color: #639941;">{{$questions[$i]}} </p>
                                                        <p id="Note">{{$answers[$i]}}</p>
                                                        @endfor
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                    @endif              
                                    <div class="col-sm-2 text-center" style="padding-left: 0px; padding-right: 5px; margin-bottom: 5px;">
                                        <?php
                                        $hey = DB::table('past_emails')->where('author_id',Auth::User()->id )->where('applicant_id',$applicant->id)->get();
                                         ?>
                                        @if(count($hey) > 0)
                                        <a href="/poster/pastmail/{{$applicant->id}}/{{$applicant->job_id}}" style="width: 100%; height: 60px;padding: 8px 0px;"
                                               class="btn btn-primary">View<br>Email</a>
                                        @else
                                        <a data-toggle="modal"
                                               data-target="#confirm-email{{$applicant->id}}" style="width: 100%; height: 60px;padding: 8px 0px;"
                                               class="btn btn-primary">Send<br>Email</a>
                                        @endif
                                    </div>
                                </div>                   
                    </div>
                    <br>
                      <div class="modal fade" tabindex="-1" id="confirm-email{{$applicant->id}}" role="dialog">
                    <div class="modal-dialog" role="document" style="margin:60px auto;">
                    <form enctype="multipart/form-data" action="/poster/emailpreviewjob" id="form-username" method="post" class="form-horizontal form-bordered">

                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div class="modal-content">
                            <div class="modal-header" style="padding:0; border:0; min-height: 0;">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <h4>Email Template</h4>
                                <p class="error" style="display: none; color: red;">You have to select an option.</p>
                                <select id="category" name="category" class="form-control " onchange="getval(this);">
                                    <option disabled selected value="">Select</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                    @foreach($categories as $category)
                                    <div class="desc{{$category->id}} hidedesc" style="display: none;">
                                    <h4>Preview</h4>
                                    {!!$category->description!!}
                                    </div>
                                    @endforeach
                                <h4>Add your personal feedback to candidate (optional)</h4>
                                <textarea rows="4" name="description" style="width: 85%;"></textarea>
                                <input type="hidden" id="currStatus" name="applicant" value="{{$applicant->id}}">
                                <input type="hidden" id="currStatus" name="applicantemail" value="{{$applicant->email}}">
                                <input type="hidden" id="currStatus" name="applicantname" value="{{$applicant->name}}">
                                <input type="hidden" id="currStatus" name="author" value="{{$applicant->author_id}}">
                                <input type="hidden" id="currStatus" name="post" value="{{$applicant->job_id}}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" >Preview Email</button>
                            </div>
                        </div><!-- /.modal-content -->
                      </form>
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                @endforeach
                <!-- pagination -->
                <nav >
                    <ul class="pagination pagination-theme  no-margin pull-right">
                        {!! $applicants->render() !!}
                    </ul>
                </nav><!-- pagination -->
            </div>
          <!-- end box listing -->

        @else
        <div class="col-md-8 col-sm-8" style="padding-top: 25px;">
            <!-- item list -->
            <div class="box-list">
                <h3 class="orangeudnerline">No Applicants!</h3>
                <div class="row">
                    <div class="col-md-4">
                      <p style="margin: 10px 2px 0px; font-size: 14px;font-weight: bold;">Job Title:</p>
                    <select onchange="location = this.value;" class="selectpicker show-tick" data-width="100%">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Jobs</option>
                            <option value="/poster/applicants">All</option>
                            @foreach($jobs as $job)
                            <option @if(isset($id) && $id == $job->id) selected="selected" @endif value="/poster/applicants/{{$job->id}}">{{$job->title}}</option>
                            @endforeach
                    </select>
                    </div>
                    <div class="col-md-4">
                      <p style="margin: 10px 2px 0px; font-size: 14px;font-weight: bold;">Active Status:</p>
                    <select onchange="location = this.value;" class="selectpicker show-tick" data-width="100%">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Still Active</option>
                            @if(isset($status))
                            <option @if(isset($filter) && $filter == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/all/status/{{$status}}">All</option>
                            <option @if(isset($filter) && $filter == 'no') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/no/status/{{$status}}">No</option>
                            <option @if(isset($filter) && $filter == 'yes') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/yes/status/{{$status}}">Yes</option>
                            @else
                            <option @if(isset($filter) && $filter == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}">All</option>
                            <option @if(isset($filter) && $filter == 'no') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/no">No</option>
                            <option @if(isset($filter) && $filter == 'yes') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/yes">Yes</option>
                            @endif
                    </select>
                    </div>
                    <div class="col-md-4">
                      <p style="margin: 10px 2px 0px; font-size: 14px;font-weight: bold;">Applicants Status:</p>
                    <select onchange="location = this.value;" class="selectpicker show-tick" data-dropup-auto="false"  data-size="auto" data-width="100%">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Status</option>
                            @if(isset($filter))
                            <option @if(isset($status) && $status == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/{{$filter}}/status/all">All</option>
                            @else
                            <option @if(isset($status) && $status == 'all') selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/all/status/all">All</option>
                            @endif
                            @foreach(\App\ApplicantsStatus::where('deleted', false)->orderBy('order','asc')->get() as $applicantStatus)
                            @if(isset($filter))
                            <option @if(isset($status) && $applicantStatus->status == $status) selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/{{$filter}}/status/{{$applicantStatus->status}}">{{$applicantStatus->status}}</option>
                            @else
                            <option @if(isset($status) && $applicantStatus->status == $status) selected="selected" @endif value="/poster/applicants/{{$id}}/filterstillactive/all/status/{{$applicantStatus->status}}">{{$applicantStatus->status}}</option>
                            @endif
                            @endforeach
                    </select>
                    </div>
                </div>
                <hr>
            </div>
        </div><!-- end box listing -->
        @endif

    </div>
</div>
</div>
<script>
    var $select_option;
    function getval(sel)   {   
        $('.modal-backdrop').css('height', '900px');
        $('.hidedesc').css('display', 'none');
        $('.desc'+sel.value).css('display', '');
        if(sel.value !== '') {
            jQuery(".error").hide(); // show Warning 
            jQuery(".modal-footer .btn-primary").show();  // Focus the select box   
            $select_option = sel.value;   
        }
        else{
            $select_option = null;
        }
    }
    $(".modal").on("hidden.bs.modal", function(){
    $(".hidedesc").css('display', 'none');
    $select_option = null;
    });
    console.log($select_option);
    function link(sel)   { 
    $.ajax({
    url: "/poster/applicants/active/"+sel.value,
            data: sel.value,
            headers:
    {
    'X-CSRF-Token': $('input[name="_token"]').val()
    },
            method: "get",
            success: function (response) {
            console.log(response);
            },
            error:  function (response) {
            console.log(response);
            }
    });
    }

    jQuery(".modal-footer .btn-primary").on("click", function() {
        if($select_option == null) {
            event.preventDefault();
            jQuery(".error").show(); // show Warning 
            jQuery(".modal-footer .btn-primary").hide();  // Focus the select box      
        }
    });

    
    function loadModal(id) {
    // alert("hello");
    $('#myModal').modal('toggle');
    }

    function saveStatus(id,status) {
    // alert("hello");
    var data = {appid: id, status: status};
    $.ajax({
    url: "/poster/updateApplicant",
            data: data,
            headers:
    {
    'X-CSRF-Token': $('input[name="_token"]').val()
    },
            method: "post",
            success: function (response) {
            console.log(response);
            },
            error:  function (response) {
            console.log(response);
            }
    });
    }
</script>
<div class="modal fade" tabindex="-1" id="coverModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Applicant Cover Note</h4>
            </div>
            <div class="modal-body">
                <p id="Note"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
@endsection