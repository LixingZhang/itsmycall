@extends('layouts.master')
@section('extra_css')
    <!--BEGIN DATATABLE STYLES-->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/media/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="/assets/plugins/yadcf/jquery.dataTables.yadcf.css" />
    <!--END DATATABLE STYLES-->
@stop
@section('extra_js')
    <!-- BEGIN DATATABLE JS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jszip.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/assets/plugins/yadcf/jquery.dataTables.yadcf.js" type="text/javascript"></script>
    <!-- END DATATABLE JS -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Metronic.handleTables();
            $('#table').dataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        })
    </script>
@stop
<style>

    div#table_filter {
        float: right;
    }

    .pagination>li>a, .pagination>li>span {
        line-height: 1!important;
    }
    th{
        font-size: 13px;
        font-weight: 600;
        text-align: -webkit-center !important;
        border-right: 1px solid #ddd;
        border-top: 1px solid #ddd !important;
    }
    td{
        text-align: -webkit-center;
        font-size: 14px;
        font-weight: 400;
        border-right: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }
    

</style>
@section('content')

<div class="bg-color1">
    <div class="container">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="bg-color2 block-section-xs line-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">

                    </div>
                    <div class="col-sm-6">
                        <div class="text-right"><a href="/poster">&laquo; Go back</a></div>
                    </div>
                </div>
            </div>
        </div><!-- end link top -->

        <div class="col-md-12 col-sm-9">
            <div class="block-section">
            <?php setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>
                @if(sizeof($transactions)>0)
                <div class="col-xs-6">
                <h3 class="no-margin-top">Transaction History</h3>
                </div>
                @else
                <div class="col-xs-6">
                <h3 class="no-margin-top">No Transactions</h3>
                </div>
                @endif
                <div class="col-xs-6 text-right">
                <a href="/poster/valuepack/{{Auth::id()}}" class="btn btn-info" style=" color: white; border:0;">Value Pack Details</a>
                <a href="/poster/buy_credits" class="btn btn-info" style="background-color:#639941; color: white; border:0;">Buy More Credits</a>
                </div>
            </div>
                <hr/>
                <div class="table-responsive table-striped table-hover">
                    <table class="table" id="table">
                        <thead>
                        <tr>
                            <th style="border-left: 1px solid #ddd;">Job</th>
                            <th>Billing Reference</th>
                            <th>Level</th>
                            <th>Screening Questions</th>
                            <th>Boosts</th>
                            <th>Value Pack</th>
                            <th>Date of Purchase</th>
                            <th>Time of Purchase</th>
                            <th>Payment Type</th>
                            <th>Standard Price</th>
                            <th>Discount</th>
                            <th>Total Amount (Pre GST)</th>
                            <th>Credits Used</th>
                            <th>Balance Payable</th>
                            <th>GST Paid</th>
                            <th>Total Paid</th>
                            <th>Credit Card Fees</th>
                            <th>Total Amount Invoiced</th>
                            <th>Bonus Credits</th>
                            <th>Current Balance</th>
                            <th>Reason Code</th>
                            {{--<th>Edit</th>--}}
                        </tr>
                        </thead>
                         <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                     $expires = new \Carbon\Carbon($referralcode->date_end);
                                     $date = $expires->toFormattedDateString();
                                     $now = \Carbon\Carbon::now();
                                     $difference = ($expires->diff($now)->days);
                                    }
                                ?>  
                            @if(count($referralcode) > 0 && $referralcode->credit_amount == 0)                           
                                @if($referralcode->date_end >= $end)
                                <div class="alert alert-info text-center">
                                  <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                  Your Registration Code expires in {{$difference}} days on {{$date}}.
                                </div>
                                @else
                                <div class="alert alert-info text-center">
                                  <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                Your Registration Code expired on {{$date}}.
                                </div>
                                @endif
                            @elseif(count($referralcode) > 0 && $referralcode->credit_amount > 0 && $referralcode->referral_alert == NULL)
                                <?php
                                    $end = \Carbon\Carbon::now()->toDateString();
                                     $expires = new \Carbon\Carbon($referralcode->bonus_end);
                                     $date = $expires->toFormattedDateString();
                                     $now = \Carbon\Carbon::now();
                                     $difference = ($expires->diff($now)->days);
                                 ?>
                                @if($referralcode->bonus_end >= $end)
                                <div class="alert alert-info text-center">
                                  <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                  Your Welcome Bonus expires in {{$difference}} days on {{$date}}.
                                </div>
                                @else
                                <div class="alert alert-info text-center">
                                  <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                Your Welcome Bonus expired on {{$date}}.
                                </div>
                                @endif
                            @elseif(count($referralcode) > 0 && $referralcode->credit_amount > 0 && $referralcode->referral_alert == 1)
                                <?php
                                    $end = \Carbon\Carbon::now()->toDateString();
                                     $expires2 = new \Carbon\Carbon($referralcode->bonus_end);
                                     $date2 = $expires2->toFormattedDateString();
                                     $now = \Carbon\Carbon::now();
                                     $difference2 = ($expires2->diff($now)->days);
                                 ?>
                                <div class="alert alert-info text-center">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                @if($referralcode->date_end >= $end)
                                Your Registration Code expires in {{$difference}} days on {{$date}}
                                @else
                                Your Registration Code expired on {{$date}} 
                                @endif
                                @if($referralcode->bonus_end >= $end) 
                                   and your Welcome Bonus expires in {{$difference2}} days on {{$date2}}.
                                @else
                                 and your Welcome Bonus expired on {{$date2}}.
                                @endif
                                </div>
                            @endif
                        <tbody>
                        @foreach($transactions as $transaction)
                        @if($transaction->package_id)
                            <?php $details = app\Packages::find($transaction->package_id); ?>
                            <?php   $discount = 0;
                                    $bonuscredits = 0;
                                    if($transaction->package_price){
                                        $discount = $transaction->package_price + $transaction->package_price * $transaction->package_discount / 100;
                                        $bonuscredits = $transaction->package_price * $transaction->package_discount / 100;
                                    }else{
                                        $discount = $details->price + $details->price * $details->discount_percent / 100;
                                        $bonuscredits = $details->price * $details->discount_percent / 100;
                                    }
                                 ?>
                        <tr> 
                            <td style="border-left:1px solid #ddd;"><?php
                                $job = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                    }
                                }
                                 ?> 
                                 {{$job}}
                                 </td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td> @if($transaction->listing_type) {{$transaction->listing_type}} @else nil @endif</td>
                            <td> @if($transaction->questions) {{$transaction->questions}} @else nil @endif</td>
                            <td> @if(empty($transaction->upgrades)) nil @else {!! $transaction->upgrades !!} @endif </td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                            <td>@if($transaction->total) Credits @else nil @endif </td>
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount + $transaction->refdiscup + $transaction->coupon_disc_up) - $transaction->creditsremaing)}}
                            </td>
                            <td>
                            @if($transaction->coupon_disc_up > 0 || $transaction->refdiscup > 0)
                                 @if($transaction->ref_used)
                                    @if($transaction->refdiscup > 0)
                                    Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br>  {{money_format('%.2n',$transaction->refdiscup)}}
                                    <br>
                                     <br>
                                    @endif
                                 @endif
                                 @if($transaction->coupon_used) 
                                    @if($transaction->coupon_disc_up > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc_up)}} 
                                     @endif
                                 @endif
                            @else
                            nil
                            @endif
                            </td> 
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}}</td>
                            <td>-{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}} </td>
                            <td> nil
                             </td>
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}} </td>
                            <td>nil</td>
                            <td> @if(empty($transaction->creditsused) && empty($transaction->package)) {{money_format('%.2n',$transaction->creditsin)}} @else {{ money_format('%.2n',$transaction->creditsremaing )}} @endif </td>
                            <td>nil</td>
                        </tr>
                        <tr> 
                            <td style="border-left:1px solid #ddd;">nil</td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> {{ $transaction->package }} </td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                            <td>@if($transaction->total) {{$transaction->payment}} @else nil @endif </td>

                            <td>@if($transaction->coupon_used) {{ money_format('%.2n',$transaction->totalnogst+$transaction->coupon_disc+$transaction->refdisc )}} @else {{money_format('%.2n',$transaction->totalnogst+$transaction->refdisc)}} @endif </td>
                            <td>@if($transaction->coupon_disc > 0 || $transaction->refdisc > 0) 
                                     @if($transaction->refdisc > 0)
                                        Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br> {{money_format('%.2n',$transaction->refdisc)}}
                                        <br>
                                     <br>
                                     @endif
                                     @if($transaction->coupon_used && $transaction->coupon_disc > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc)}} 
                                     @endif
                                 @else
                                    nil
                                @endif
                            </td> 
                            <td>{{ money_format('%.2n',$transaction->totalnogst)}}</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>{{ money_format('%.2n',$transaction->gst_paid )}}</td>
                            <td> {{money_format('%.2n',$transaction->totalnogst + $transaction->gst_paid)}} </td>
                            <td> @if($transaction->creditfee)
                                    {{money_format('%.2n',$transaction->creditfee)}}
                                    @else nil
                                 @endif

                             </td>
                            <td> {{money_format('%.2n',$transaction->totalnogst + $transaction->gst_paid + $transaction->creditfee)}} </td>
                            <td>{{money_format('%.2n',$bonuscredits)}}</td>
                            <td>
                                {{money_format('%.2n',$discount)}}
                            </td>
                            <td>nil</td>
                        </tr>
                    @elseif($transaction->reason)
                        <tr> 
                            <td style="border-left:1px solid #ddd;">nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Credits</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>{{ money_format('%.2n', $transaction->creditsgiven)}}</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td>{{$transaction->reason}}</td>
                        </tr>
                    @elseif($transaction->free_upgrade)
                        <tr> 
                            <td style="border-left:1px solid #ddd;"><?php
                                $job = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                    }
                                }
                                 ?> 
                                 {{$job}}
                                 </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>Screening Questions {{ money_format('%.2n', $transaction->free_upgrade_price)}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Free Upgrade</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td>{{$transaction->free_upgrade}}</td>
                        </tr>
                    @elseif($transaction->free_upgrade_boost)
                        <tr> 
                            <td style="border-left:1px solid #ddd;"><?php
                                $job = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                    }
                                }
                                 ?> 
                                 {{$job}}
                                 </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade_price}}</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Free Upgrade</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td>{{$transaction->free_upgrade_boost}}</td>
                        </tr>
                    @elseif($transaction->free_upgrade_package)
                        <tr> 
                            <td style="border-left:1px solid #ddd;"><?php
                                $job = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                    }
                                }
                                 ?> 
                                 {{$job}}
                                 </td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade_price}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Free Upgrade</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td>{{$transaction->free_upgrade_package}}</td>
                        </tr>
                    @elseif($transaction->welcome_credits)
                        <tr> 
                            <td style="border-left:1px solid #ddd;">nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Credits</td>
                            <td>{{money_format('%.2n',$transaction->welcome_credits)}}</td>
                            <td>nil
                            </td> 
                            <td>{{ money_format('%.2n', $transaction->welcome_credits)}}</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>
                            nil</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td>Welcome Bonus</td>
                        </tr>
                    @elseif($transaction->partial_credits == 1)
                        <tr> 
                            <td style="border-left:1px solid #ddd;"><?php
                                $job = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                    }
                                }
                                 ?> 
                                 {{$job}}
                                 </td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td> @if($transaction->listing_type) {{$transaction->listing_type}} @else nil @endif</td>
                            <td> @if($transaction->questions) {{$transaction->questions}} @else nil @endif</td>
                            <td> @if(empty($transaction->upgrades)) nil @else {!! $transaction->upgrades !!} @endif </td>
                            <td> @if(empty($transaction->package)) nil @else {{ $transaction->package }} @endif </td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Credit Card & Credits</td>
                            <td>@if($transaction->coupon_used) {{ money_format('%.2n',$transaction->totalnogst+$transaction->coupon_disc+$transaction->refdisc+$transaction->refdiscup+$transaction->creditsin) }} @else {{money_format('%.2n',$transaction->totalnogst+$transaction->refdisc+$transaction->refdiscup+$transaction->creditsin)}} @endif </td>
                            <td>@if($transaction->coupon_disc > 0 || $transaction->refdisc > 0 || $transaction->refdiscup > 0) 
                                     @if($transaction->ref_used)
                                        @if($transaction->refdisc > 0 || $transaction->refdiscup > 0)
                                        Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br>  {{money_format('%.2n',$transaction->refdisc+$transaction->refdiscup)}}
                                        <br>
                                     <br>
                                        @endif
                                     @endif
                                     @if($transaction->coupon_used && $transaction->coupon_disc > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc)}} 
                                     @endif
                                 @else
                                    nil
                                @endif
                            </td> 
                            <td>{{ money_format('%.2n', $transaction->totalnogst+$transaction->creditsin)}}</td>
                            <td> @if(empty($transaction->creditsused)) - @else -{{money_format('%.2n',$transaction->creditsin-$transaction->creditsremaing)}} @endif </td>
                            <td>{{ money_format('%.2n', $transaction->totalnogst)}}</td>
                            <td>{{money_format('%.2n', $transaction->gst_paid)}}</td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid)}} </td>
                            <td> @if($transaction->creditfee)
                                    {{money_format('%.2n', $transaction->creditfee)}}
                                    @else nil
                                @endif

                             </td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid+$transaction->creditfee)}} </td>
                            <td>
                            <?php $details = app\Packages::find($transaction->packageid); ?>
                            <?php  
                                    $bonuscredits = 0;
                                    if($transaction->package_price){
                                        $bonuscredits = $transaction->package_price * $transaction->package_discount / 100;
                                    }else{
                                        if($details){
                                            $bonuscredits = $details->price * $details->discount_percent / 100;
                                        }
                                    }
                                 ?>
                            @if($bonuscredits > 0) {{money_format('%.2n',$bonuscredits)}} @else nil @endif </td>
                            <td> @if(empty($transaction->creditsused) && empty($transaction->package)) {{money_format('%.2n',$transaction->creditsin)}} @else {{money_format('%.2n', $transaction->creditsremaing )}} @endif </td>
                            <td>nil</td>
                        </tr>
                    @else
                        <tr> 
                            <td style="border-left:1px solid #ddd;"><?php
                                $job = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                    }
                                }
                                 ?> 
                                 {{$job}}
                                 </td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td> @if($transaction->listing_type) {{$transaction->listing_type}} @else nil @endif</td>
                            <td> @if($transaction->questions) {{$transaction->questions}} @else nil @endif</td>
                            <td> @if(empty($transaction->upgrades)) nil @else {!! $transaction->upgrades !!} @endif </td>
                            <td> @if(empty($transaction->package)) nil @else {{ $transaction->package }} @endif </td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>@if($transaction->total) {{$transaction->payment}} @else nil @endif </td>
                            <td>@if($transaction->coupon_used) {{ money_format('%.2n',$transaction->totalnogst+$transaction->coupon_disc+$transaction->refdisc+$transaction->refdiscup) }} @else {{money_format('%.2n',$transaction->totalnogst+$transaction->refdisc+$transaction->refdiscup)}} @endif </td>
                            <td>@if($transaction->coupon_disc > 0 || $transaction->refdisc > 0 || $transaction->refdiscup > 0) 
                                     @if($transaction->ref_used)
                                        @if($transaction->refdisc > 0 || $transaction->refdiscup > 0)
                                        Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br>  {{money_format('%.2n',$transaction->refdisc+$transaction->refdiscup)}}
                                        <br>
                                     <br>
                                        @endif
                                     @endif
                                     @if($transaction->coupon_used && $transaction->coupon_disc > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc)}} 
                                     @endif
                                 @else
                                    nil
                                @endif
                            </td> 
                            <td>{{ money_format('%.2n', $transaction->totalnogst)}}</td>
                            <td> @if(empty($transaction->creditsused)) - @else -{{money_format('%.2n',$transaction->creditsin-$transaction->creditsremaing)}} @endif </td>
                            <td>nil</td>
                            <td>{{money_format('%.2n', $transaction->gst_paid)}}</td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid)}} </td>
                            <td> @if($transaction->creditfee)
                                    {{money_format('%.2n', $transaction->creditfee)}}
                                    @else nil
                                @endif

                             </td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid+$transaction->creditfee)}} </td>
                            <td>
                            <?php $details = app\Packages::find($transaction->packageid); ?>
                            <?php  
                                    $bonuscredits = 0;
                                    if($transaction->package_price){
                                        $bonuscredits = $transaction->package_price * $transaction->package_discount / 100;
                                    }else{
                                        if($details){
                                            $bonuscredits = $details->price * $details->discount_percent / 100;
                                        }
                                    }
                                 ?>
                            @if($bonuscredits > 0) {{money_format('%.2n',$bonuscredits)}} @else nil @endif </td>
                            <td> @if(empty($transaction->creditsused) && empty($transaction->package)) {{money_format('%.2n',$transaction->creditsin)}} @else {{money_format('%.2n', $transaction->creditsremaing )}} @endif </td>
                            <td>nil</td>
                        </tr>
                        @endif
                          <!-- Modal -->
                          <div class="modal fade" id="coupondetail{{$transaction->id}}" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Coupon Description:</h4>
                                </div>
                                <?php 
                                    $couponcode = App\Coupons::where('coupon_code',$transaction->coupon_used)->first();
                                ?>
                                <div class="modal-body">
                                  <p>@if($couponcode) {!!$couponcode->description!!} @endif</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                        <!-- Modal -->
                          <div class="modal fade" id="refdetails{{$transaction->id}}" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Registration Code Description:</h4>
                                </div>
                                <?php 
                                    $refcode = App\ReferralCode::where('referral_code',$transaction->ref_used)->first();
                                ?>
                                <div class="modal-body">
                                  <p>@if($refcode) {!!$refcode->description!!} @endif</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>

@endsection