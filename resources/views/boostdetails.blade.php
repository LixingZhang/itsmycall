@extends('layouts.master')
@section('extra_css')
    <!--BEGIN DATATABLE STYLES-->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/media/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="/assets/plugins/yadcf/jquery.dataTables.yadcf.css" />
    <!--END DATATABLE STYLES-->
@stop
@section('extra_js')
    <!-- BEGIN DATATABLE JS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jszip.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/assets/plugins/yadcf/jquery.dataTables.yadcf.js" type="text/javascript"></script>
    <!-- END DATATABLE JS -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Metronic.handleTables();
            $('#table').dataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        })
    </script>
@stop
<style>

    div#table_filter {
        float: right;
    }

    .pagination>li>a, .pagination>li>span {
        line-height: 1!important;
    }
    .block-section{
        padding: 30px 0 !important;
    }
    

</style>
@section('content')

<div class="bg-color1">
    <div class="container">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="bg-color2 block-section-xs line-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">

                    </div>
                    <div class="col-sm-6">
                        <div class="text-right"><a href="/poster">&laquo; Go back</a></div>
                    </div>
                </div>
            </div>
        </div><!-- end link top -->

        <div class="col-md-12 col-sm-9">
            <div class="block-section">
                @if(sizeof($ads)>0)
                <h3 class="no-margin-top no-margin-bottom">Boost Details</h3>
                <p>Please note, Boost statistics are updated each weekend only.</p>
                <?php  $totalviews = 0; $totalclicks = 0; ?>
                <?php
                foreach ($ads as $ad) {
                    $totalviews = $totalviews+$ad->getTotalViews();
                    $totalclicks = $totalclicks+$ad->getTotalClicks();
                }
                ?>

                @else
                <h3 class="no-margin-top">No Purchased Boost</h3>
                @endif

                <hr style="margin-bottom: 0px;" />
                <div class="table-responsive">
                    <table class="table" id="table" style="margin-top: 0px !important;">
                        <thead>
                        <tr>
                            <th>Job Title</th>
                            <th>Boost Name</th>
                            <th>Location</th>
                            <th>Date of Publication</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Boost Views</th>
                            <th>Boost Clicks</th>
                            <th>Comments</th>
                            <th>Boost Image</th>
                            {{--<th>Edit</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($boosts as $boost)
                        <tr>
                            <td> <a target="_blank" href="http://itsmycall.com.au/{{$jobname->slug}}">{{$jobname->title}}</a> </td>
                            <td>
                                <?php 
                                    $upgradename = \App\PostUpgrades::find($boost->post_upgrades_id);
                                    $boostname = \App\Upgrades::find($upgradename->upgrade_id);
                                    echo $boostname->name;
                                ?>
                            </td>
                                <td><label class="label label-success">{{$boost->post_edition}}</label></td>
                                 <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $boost->post_datetime);
                                    $print = $date->format('d M Y'); ?>
                                <td>
                                    @if($boost->post_status !== 'not_actioned' && $print)
                                       <label class="label label-success"> {{$print}}
                                        </label>
                                    @endif
                                </td>
                                <?php
                                      $pricetotal = \App\PostUpgrades::where('id',$boost->post_upgrades_id)->first();
                                     ?>
                                <td> @if($pricetotal->is_purchased == 1) ${{number_format($boost->boost_cost, 2, '.', '')}} @else Free  @endif </td>
                                <td><label class="label label-info"> @if($boost->post_status == 'completed') Completed @endif
                                @if($boost->post_status == 'in_progress') In Progress @endif
                                @if($boost->post_status == 'not_actioned') Not Actioned @endif </label>
                                 </td>
                                <td> {{number_format($boost->post_views)}}</td>
                                <td> {{number_format($boost->post_clicks)}} </td>
                                <td>{{$boost->post_comment}}</td>
                                <td>
                                    @if(!empty($boost->boost_image))
                                        <a href="{{$boost->boost_image}}" target="_blank"><img src="{{$boost->boost_image}}" width="100"/></a>
                                    @endif
                                </td>
                        </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="font-weight: bold;">Total:</td>
                                <td>{{number_format($totalviews)}}</td>
                                <td>{{number_format($totalclicks)}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>

@endsection