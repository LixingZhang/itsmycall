@extends('layouts.master')


@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.3/css/fileinput.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.3/js/fileinput.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });
    });
</script>
<style type="text/css">
    .file-caption-main .btn-file{padding-top: 8px;}
    .modal-dialog { margin: 15px auto !important;}
    .btn-default{height: 40px;}
    .btn-file{padding-top: 9px;}
</style>

<div class="bg-color1">
    <div class="container">
        <div class="col-md-3 col-sm-3">

            <div class="block-section text-center">
                <img src="{{Auth::user()->avatar}}" class="img-rounded" alt="" style="max-width: 150px;">
                <div class="white-space-20"></div>
                <h4>Hi {{Auth::user()->name}}</h4>
                <div class="white-space-20"></div>
                <ul class="list-unstyled">
                    <li><a href="/customer/edit-user"> Edit Profile </a></li>
                    <li><a href="/customer/change_password"> Change Password</a></li>
                </ul>
                <div class="white-space-20"></div>
                <a href="/customer/job_alerts" class="btn btn-info btn-block" style="background-color:#ff7200; color: white; border:0;">Job Alerts</a>
                <div class="white-space-20"></div>
                <a href="/customer/savedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Saved Jobs</a>
                <div class="white-space-20"></div>
                <a href="/customer/appliedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Applied Jobs</a>
                <div class="white-space-20"></div>
                <a href="/customer/connect-program" class="btn btn-info btn-block" style=" border:0;">Connect Program</a>
            </div>
        </div>
        <div class="col-md-9" style="margin-top:15px;">
            @include('admin.layouts.notify')
        </div>
        <div class="col-md-9 col-sm-9">
            @if(!Auth::user()->state)
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important">
                            <h4>State you live in?</h4>
                            <p><small>Please select a state you live in below to complete your registration:  <br> </small> </p>
                            <select id="state" name="state" class="form-control selectpicker" onchange="location = this.value;">
                                    <option @if(Auth::user()->state == "" ) selected @endif value="">Select State</option>
                                    <option value="/customer/update/Australian Capital Territory" @if(Auth::user()->state == "Australian Capital Territory" ) selected @endif value="Australian Capital Territory">Australian Capital Territory</option>
                                    <option value="/customer/update/New South Wales" @if(Auth::user()->state == "New South Wales" ) selected @endif value="New South Wales">New South Wales</option>
                                    <option value="/customer/update/Northern Territory" @if(Auth::user()->state == "Northern Territory" ) selected @endif value="Northern Territory">Northern Territory</option>
                                    <option value="/customer/update/Queensland" @if(Auth::user()->state == "Queensland" ) selected @endif value="Queensland">Queensland</option>
                                    <option value="/customer/update/South Australia" @if(Auth::user()->state == "South Australia" ) selected @endif value="South Australia">South Australia</option>
                                    <option value="/customer/update/Tasmania" @if(Auth::user()->state == "Tasmania" ) selected @endif value="Tasmania">Tasmania</option>
                                    <option value="/customer/update/Victoria" @if(Auth::user()->state == "Victoria" ) selected @endif value="Victoria">Victoria</option>
                                    <option value="/customer/update/Western Australia" @if(Auth::user()->state == "Western Australia" ) selected @endif value="Western Australia">Western Australia</option>
                                    <option value="/customer/update/I don't live in Australia" @if(Auth::user()->state == "I don't live in Australia" ) selected @endif value="I don't live in Australia">I don't live in Australia</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="row" style="margin-top: 15px;">
                <div class="col-md-6 text-center">
                <div class="panel panel-default">
                    <div class="panel-body" style="padding:25px!important">
                        <h4>Create your own Job Alerts!</h4>
                        <a style="text-decoration: none;" href="/customer/job_alerts"><button type="button" class="btn btn-primary btn-block">Manage Job Alerts</button></a>
                    </div>
                </div>
            </div>
            
                <div class="col-md-6 text-center">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important">
                            <h4>Looking for a Job?</h4>
                            <a style="text-decoration: none;" href="/"><button type="button" class="btn btn-primary btn-block">Search Jobs Now</button></a>
                        </div>
                    </div>
                </div>


            </div>

            <div class="row" style="margin-top: 15px;">
                <div @if(Auth::user()->resume != '') class="col-md-6" @else class="col-md-12" @endif >
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important">
                            <div class="form-group">
                                <h4>Upload CV/Resume</h4>
                                @if(Auth::user()->resume != '')
                                <p><small> Not satisfied with your current CV/Resume? Upload a new CV/Resume below: </small> </p>
                                @else
                                <p> Upload Your CV / Resume. Once uploaded your CV/Resume will be available to attach to any Job Application or the Connect Program.</p>
                                @endif
                                <button @if(Auth::user()->resume != '') style="width: 100%;" @endif type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add new CV / Resume</button>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->resume != '')
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important">
                            <div class="form-group">
                                <h4>View your current CV/Resume</h4>
                                <p><small>Click below to view your current CV/Resume that we have on file:  <br> </small> </p>
                                <a href="{{Auth::user()->resume}}" style="width: 100%; padding: 8px 12px;" target="_blank" class="btn btn-primary"> @if(Auth::user()->filename) {{Auth::user()->filename}} @else View Your CV/Resume @endif</a>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12">

                    @if (Auth::user()->profile_type != 'standard' && Auth::user()->profile_type != 'maximum')
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important;">
                            <div class="row">
                                @if($video)
                                <div class="col-sm-12" style="padding-top: 5px; padding-bottom: 10px;"><iframe width="100%" height="400" src="//www.youtube.com/embed/{{$video->video}}">
                                </iframe></div>
                                @endif
                                @foreach($images as $image)
                                @if($image->video == '')
                                <div class="col-sm-12" style="padding-top: 5px; padding-bottom: 10px;">
                                    <a target="_blank" href="{{$image->url}}"><img style="border:4px solid;" width="100%" src="{{$image->image}}">
                                    </a>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <h4> ItsMyCall Connect Program</h4>
                            <p>Want to be found by potential employers? Sign up to our Connect Program today.</p>
                            <a style="padding-top: 25px;" href="/customer/connect-program"><button type="button" class="btn btn-primary">Click here to Join</button></a>
                        </div>
                    </div>
                    @endif
                    @if (Auth::user()->profile_type == 'standard' || Auth::user()->profile_type == 'maximum')
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important">
                            <div class="row">
                                @if($video)
                                <div class="col-sm-12" style="padding-top: 5px; padding-bottom: 10px;"><iframe width="100%" height="400" src="//www.youtube.com/embed/{{$video->video}}">
                                </iframe></div>
                                @endif
                                @foreach($images as $image)
                                @if($image->video == '')
                                <div class="col-sm-12" style="padding-top: 5px; padding-bottom: 10px;">
                                    <a target="_blank" href="{{$image->url}}"><img style="border:4px solid;" width="100%" src="{{$image->image}}">
                                    </a>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <h4>Connect Program</h4>

                            <p>You're participating in our Connect program and can be found by potential employers.</p>

                            @if (Auth::user()->profile_type == 'standard')<p>Your Level: <b>Basic</b></p>@endif
                            @if (Auth::user()->profile_type == 'maximum')<p>Your Level: <b>Advanced</b></p>@endif
                            <a href="/customer/connect-program"><button type="button" class="btn btn-primary">Edit Connect Program Settings</button></a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            





        </div><!-- end box listing -->
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin: 46px auto !important;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload CV / Resume</h4>
      </div>
      <div class="modal-body">
        <form action="/upload_resume/{{Auth::user()->id}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input id="input-b6" name="file" type="file">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            </div>
        </form>
        <div class="loader hidden">Saving your CV/Resume <img style="width: 25px;" src="/Spinner.gif" alt="Loading" title="Loading" /></div>
        <p style="font-size: 85%;" class="info"><i class="fa fa-info-circle" aria-hidden="true"></i> File type must be doc,docx or pdf. Max file size is <strong>3MB</strong>. Once uploaded your CV/Resume will be available to attach to any Job Application or Connect Program</p>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    $(document).on('ready', function() {
    $("#input-b6").fileinput({
        showUpload: false,
         showRemove: false,
        mainClass: "input-group-md",
        browseLabel: "Select CV/Resume",
    });
    });
    $('INPUT[type="file"]').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    if(this.files[0].size > 3000000){
        alert('This file size should not be greater than 3MB.');
        this.value = ''; 
    }else{
        switch (ext) {
        case 'pdf':
        case 'doc':
        case 'docx':
            $('#uploadButton').attr('disabled', false);
            break;
        default:
            alert('This file type is not allowed.');
            this.value = '';
    }
    }if($(this).val().length > 0){
        $( ".info" ).addClass('hidden');
        $( ".loader" ).removeClass('hidden');
        this.form.submit();
    }
});
</script>
@endsection