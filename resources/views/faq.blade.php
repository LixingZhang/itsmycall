@extends('layouts.master')


@section('content')
<div class="container min-hight" style="padding-top:20px">
    <div class="row" >
        <div class="col-md-9 col-sm-9"  style="padding-bottom:20px">

            <!-- Blog Image -->
            <!-- Blog Content -->
            <p><h4>General</h4></p>
            <p><strong>&nbsp;</strong></p>
            <p><strong>Where are all the jobs?</strong></p>
            <p>&nbsp;</p>
            <p>Starting a new jobs website is never easy. Advertisers won't place jobs on a website no one visits, and if you visit a jobs website and there are no jobs chances are you won't come back.</p>
            <p>&nbsp;</p>
            <p>But why should just one or two website who have no interest in the success of our industry, and who don't understand it, be the only place to find jobs?&nbsp; Well we aren't going to die wondering!</p>
            <p>&nbsp;</p>
            <p>And with support from our key partners in the Auscontact Association and Contact Centre Central, we've got the two biggest industry players behind us. Add to that our referral program and our marketing across all key Social Media platforms and we hope it won't be long before you are giving our filters a real work out to narrow the search for available jobs!</p>
            <p>&nbsp;</p>
            <p><strong>How does the referral program work?</strong></p>
            <p>&nbsp;</p>
            <p>We tried to keep it as simple as possible! If you refer our jobs website to an organisation who goes ahead and opens an account with us<sup>1</sup>, they will get 10%<sup>2</sup> off all jobs they list and you will get 5%<sup>3</sup> back off everything they spend for the first 12 months!</p>
            <p>&nbsp;</p>
            <p><sup>1 </sup>Only one referral coupon is allowed per organisation</p>
            <p><sup>2 </sup>The 10% discount applies to all jobs and jobs packages available on our website</p>
            <p><sup>3 </sup>5% is paid to you off the total spend and is paid quarterly from their first purchase.</p>
            <p>&nbsp;</p>
            <p><strong>Auscontact Association discount </strong></p>
            <p>&nbsp;</p>
            <p>If you are an Auscontact Association member we've got good news, you get a discount! You can find more details about the current discounts here, and if you're not an Auscontact Member there has never been a better time to join! Click here for more details.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><h4>Job Advertisers</h4></p>
            <p>&nbsp;</p>
            <p><strong>We have a job we'd like to fill that doesn't fit one of your categories or isn't listed?</strong></p>
            <p>&nbsp;</p>
            <p>We tried to cater for all job types and roles but it's a fast moving industry! Email us the details at <a href="mailto:admin@itsmycall.com.au">admin@itsmycall.com.au</a> and if its suitable, we'll make the necessary changes to our website straight away!</p>
            <p>&nbsp;</p>
            <p><strong>Can I include screening questions when people apply for jobs?</strong></p>
            <p>&nbsp;</p>
            <p>At this stage no. We will be releasing this functionality in the next quarter and will provide the opportunity for all our customers to have input in how they would like it to work. In the interim we can direct people to your website or to another website where the screening process can take place if you require it.</p>
            <p>&nbsp;</p>
            <p><strong>Why would I place a job on your website?</strong></p>
            <p>&nbsp;</p>
            <p>Straight to the point hey! We like how you roll.&nbsp; Here's a few points to consider:</p>
            <p>&nbsp;</p>
            <ul>
                <li>Through both our partners we already have a base of over XXXX thousand of contact centre professionals ranging from agents through to senior executives.</li>
                <li>We understand the contact centre industry and have a range of categories and job types that help match advertisers and job seekers more accurately than ever before.</li>
                <li>We've included things that are important to agents - like the ability to search for roles by pay frequency as we know that's important to them.</li>
                <li>You can extend your reach beyond just the <a href="http://www.itsmycall.com">itsmycall.com</a> website. With each job listing you have the opportunity to extend the advertising across newsletters, Facebook and LinkedIn.</li>
            </ul>
            <p>&nbsp;</p>
            <p><strong>How do the packages work?</strong></p>
            <p><strong>&nbsp;</strong></p>
            <p>If you regularly post jobs we offer a range of different packages to provide even more value. You can view the available packages here.&nbsp; When you purchase jobs packages you can use that credit to list a job, and to purchase our upgrades.</p>
            <p><strong>&nbsp;</strong></p>
            <p><strong>Can we purchase details of applicants who have signed up for Job Alerts?</strong></p>
            <p><strong>&nbsp;</strong></p>
            <p>Our website has been built to accommodate this feature however we will not be introducing this service until a later stage. Walk before you run comes to mind!</p>
            <p><strong>&nbsp;</strong></p>
            <p><strong>What are upgrades?</strong></p>
            <p>&nbsp;</p>
            <p>In addition to posting a job on the <a href="http://www.itsmycall.com.au">www.itsmycall.com.au</a> website, thanks to our key partners we offer some additional upgrades to provide you with even further reach to find that right candidate:</p>
            <p>&nbsp;</p>
            <ul>
                <li>Have the job posted on the Australian Contact Centre Employees Facebook page with over 11,500 page likes</li>
                <li>Have the job posted across the LinkedIn network for Contact Centre Central - approximately 4,000 contact centre professionals.</li>
                <li>Feature the job in The Pulse, a weekly email sent to contact centre professionals</li>
            </ul>
            <p><strong>&nbsp;</strong></p>
            <p><strong>Do you offer any further discounts for large volumes?</strong></p>
            <p>&nbsp;</p>
            <p>Yes we do! Please contact us at <a href="mailto:sales@itsmycall.com.au">sales@itsmycall.com.au</a> for more information.</p>
            <p>&nbsp;</p>
            <p><strong>We don't have credit card facilities. Is there any other way we can purchase jobs?</strong></p>
            <p>&nbsp;</p>
            <p>You can pay via direct debit or in some instances by invoice. Please contact us at <a href="mailto:accounts@itsmycall.com.ayu">accounts@itsmycall.com.ayu</a> for further information.</p>
            <p>&nbsp;</p>
            <p><h4>Job Seekers</h4></p>
            <p>&nbsp;</p>
            <p><strong>How do I sign up for Job Alerts?</strong></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><strong>How can I update my Job Alerts?</strong></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </div>
</div>

@endsection