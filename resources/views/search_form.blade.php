<form action="/job_list" method="POST" role="form">

    <div class="row">

        <div class="col-md-3">
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Keywords" >
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="form-group">
                    <select id="country" name="country" class="form-control">
                    </select>
                </div>
            </div>
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="col-md-3">
            <div class="form-group">
                <select id="state" name="state" class="form-control">
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <div class="select-style">
                    <select class="form-control" name="category">
                        <option value="-1">All</option>
                        @foreach($categories as $category)
                            <option>{{$category->title}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
