<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
 	<style type="text/css">
 		.btn {
		    background-color: #ff7200; /* Green */
		    border: none;
		    color: white;
		    padding: 10px 20px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 18px;
		    border-radius: 4px;
		}
 	</style>
</head>
<body>

<div>
	Hi {{$name}}, <br><br>
    Thanks for creating an account and for your interest in placing job adverts on ItsMyCall, Australia’s dedicated contact centre jobs website.<br><br>
    Before you can post your first job adverts, we need you to confirm your email address.
    <br><br>
    <a class="btn" href="{{ URL::to('/register/verify/' . $confirmation_code) }}?e={{$email}}">Click here to validate your email address now</a>
    <br/>
    <br/>
    <p>Kind regards,</p>
    <img src="http://dev.itsmycall.com.au/jobs.png" alt="logo" style="width: auto; height:50px; margin-left: auto; margin-right: auto;" />


</div>

</body>
</html>