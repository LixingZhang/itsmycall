@extends('layouts.master')


@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
<style type="text/css">
    .swal2-content{
        line-height: inherit !important;
    }
</style>
<?php use App\Categories; use App\SubCategories; use App\PostUpgrades;?>
<div class="bg-color1">
    <div class="container">
        <div class="col-md-3 col-sm-3">

            <div class="block-section text-center">
                <img src="{{Auth::user()->avatar}}" class="img-rounded" alt="" style="max-width: 150px;">
                <div class="white-space-20"></div>
                <h4>Hi {{Auth::user()->name}}</h4>
                <div class="white-space-20"></div>
                <ul class="list-unstyled">
                    <li><a href="/customer"> My Account </a></li>
                    <li><a href="/customer/edit-user"> Edit Profile </a></li>
                    <li><a href="/customer/change_password"> Change Password</a></li>
                </ul>
                <div class="white-space-20"></div>
                <a href="/customer/job_alerts" class="btn btn-info btn-block" style="background-color:#ff7200; color: white; border:0;">Job Alerts</a>
                <div class="white-space-20"></div>
                <a href="/customer/appliedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Applied Jobs</a>
                <div class="white-space-20"></div>
                <a href="/customer/connect-program" class="btn btn-info btn-block" style=" border:0;">Connect Program</a>
            </div>
        </div>
        <div class="col-md-9" style="margin-top:15px;">
            @include('admin.layouts.notify')
        </div>

        @if(sizeof($savedjobs)>0)

        <div class="col-md-9 col-sm-9">

            <h3 class="orangeudnerline">Your Saved Jobs</h3>
            <p>To save a job while you are searching, click on <a style="padding-left: 3px; padding-right: 3px; text-decoration: none;"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a> in the bottom left hand side of each job. When you do, the jobs will appear on this page ready for you to apply!</p>
            <!-- item list -->
            <div class="box-list">

                @foreach($savedjobs as $savedjob)
                <?php 
                    $date = Carbon\Carbon::now();
                    $job = \App\Posts::where('id',$savedjob->job_id)->where('status','active')->where('expired_at','>',$date)->first();
                ?>
                @if(sizeof($job) > 0)
                <div class="item <?php
                if ($job->posts_packages == 1) {
                    echo "featured";
                } else if ($job->posts_packages == 2) {
                    echo "premium";
                }
                ?>">
                    <div class="row">
                        <div class="col-md-12 no-pad">
                            <h3 class="no-margin-top">
                                <a href="/{{$job->slug}}" style="text-decoration: none;" class="">
                                    {{$job->title}}
                                </a>
                        @if(strlen($job->featured_image)>0)
                        <div class="col-md-1 hidden-sm hidden-xs">
                            <div class="img-item"><img src="{{$job->featured_image}}" alt=""></div>
                        </div>
                        @else

                        @endif
                                
                            </h3>
                            
                            <div class="col-md-4 no-pad">
                                @if(strlen($job->company)>0&&strlen($job->state)>0)
                                <ul class="list-group">
                                    <li class="list-group-item">Company: <a href="/filter_company/{{$job->company}}">{{$job->company}}</a></li>
                                    <li class="list-group-item">Location: <a href="https://www.google.com/maps/place/{{$job->joblocation}}">{{$job->joblocation}}</a></li>
                                    <li class="list-group-item"><span class="color-black">Employment Hours: <span style="color: grey">{{ucfirst(str_replace("_", " ", $job->employment_type))}}</span></span></li>
                                    <li class="list-group-item"><span class="color-black">Employment Type: <span style="color: grey">{{ucfirst(str_replace("_", " ", $job->employment_term))}}</span></span></li>
                                    @if($job->showsalary == "1")
                                    @if($job->posts_packages == '3')
                                    @if($job->salarytype == "annual")<li class="list-group-item"><span class="color-black">Annual Salary: <span style="color: grey">${{number_format($job->salary)}}</span></span></li>@endif
                                    @else
                                    @if($job->salarytype == "annual")<li class="list-group-item"><span class="color-black">Annual Salary: <span style="color: grey">${{number_format($job->salary)}}</span></span></li>@endif
                                    @if($job->salarytype == "hourly")<li class="list-group-item"><span class="color-black">Hourly Rate: <span style="color: grey">${{number_format($job->hrsalary, 2)}}</span></span></li>@endif
                                    @if($job->paycycle != "no")<li class="list-group-item" ><span class="color-black">Pay Frequency: <span style="color: grey">{{ucfirst($job->paycycle)}}</span></span></li>@endif
                                    @if($job->paycycle == "no")<li class="list-group-item"><span class="color-black">Pay Frequency: <span style="color: grey">Not Specified</span></span></li>@endif
                                    @endif
                                    @endif
                                    @if($job->posts_packages != '3')
                                    <li class="list-group-item" style=""><span class="color-black">Advertiser: <span style="color: grey"> @foreach($users as $user) @if($job->author_id == $user->id) @if($user->advertiser_type == 'private_advertiser') Private Advertiser @endif @if($user->advertiser_type == 'recruitment_agency') Recruitment Agency @endif  @endif @endforeach </span></span></li>
                                    @endif
                                    <li class="list-group-item"><a href="/{{$job->slug}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>

                                </ul>
                                @endif
                            </div>
                            <div class="col-md-5">
                                <div style="word-wrap: break-word;">
                                    {{\Illuminate\Support\Str::limit(strip_tags($job->short_description),700)}}
                                </div>

                                <div>
<?php if ($job->posts_packages == 1 && strlen($job->selling1) > 0 && strlen($job->selling2) > 0 && strlen($job->selling3) > 0) { ?>
                                        <br>
                                        <ul style="font-size: 15px;">
                                            @if(strlen($job->selling1)>0)<li>{{$job->selling1}}</li>@endif
                                            @if(strlen($job->selling2)>0)<li>{{$job->selling2}}</li>@endif
                                            @if(strlen($job->selling3)>0)<li>{{$job->selling3}}</li>@endif
                                        </ul>
                                        <br>
<?php } ?>
                                </div>
                                <p>
                                    <span class="color-white-mute"><small>Posted {{$job->created_at->diffForHumans()}}</small></span>
                                </p>

                            </div>
                            <div class="col-md-3">
                                 <a style="background-color: #639941; border:none; margin-top: 5px;" class="btn btn-info btn-sm" href="/customer/removesavejob/{{$savedjob->id}}">Remove Job</a>
                                 <br>
                                 <?php 
                                   $apply_check = \DB::table('applicants')->where('job_id', $job->id)->count();
                                 ?>
                                 @if($apply_check > 0)
                                 <p style="color: #639941; font-size: 18px;padding-top: 8px; "><i class="fa fa-check-circle" aria-hidden="true"></i> Applied</p>
                                 @endif
                            </div>
                            <div class="col-md-12 no-pad"><span style="color: grey">{{Categories::where('id', $job->category_id)->get()->first()->title}} / {{SubCategories::where('id', $job->subcategory)->get()->first()->title}}</span> </div>
                        </div>
                    </div>
                </div><!-- end item list -->
                @endif
                @endforeach
                <!-- pagination -->
                <nav >
                    <ul class="pagination pagination-theme  no-margin pull-right">
                        {!! $savedjobs->render() !!}
                    </ul>
                </nav><!-- pagination -->

            </div>


        </div><!-- end box listing -->
        @else
        <div class="col-md-9 col-sm-9" style="padding-top: 15px;">
            <!-- item list -->
            <div class="box-list">
                <h3 class="orangeudnerline">No Saved Jobs!</h3>
                <p>To save a job while you are searching, click on <a style="padding-left: 3px; padding-right: 3px; text-decoration: none;"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a> in the bottom left hand side of each job. When you do, the jobs will appear on this page ready for you to apply!</p>
            </div>
        </div><!-- end box listing -->
        @endif

    </div>
</div>
</div>
@endsection