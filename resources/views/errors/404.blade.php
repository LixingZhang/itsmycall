<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="author" content="http://itsmycall.com.au">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    $error = \DB::table('errors')->where('name','404')->first();
    ?>
    @if($error)
    <title>{{$error->pagetitle}}</title>
    @endif

    <!--favicon-->
    <link rel="apple-touch-icon" href="/assets/theme/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="http://itsmycall.com.au/uploads/images/58a581dd0e781_file.png" type="image/x-icon">
    <!-- bootstrap -->
    <link href="/assets/plugins/bootstrap-3.3.2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="/assets/plugins/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- lightbox -->
    <link href="/assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/plugins/jquery.js"></script>
    <script src="/assets/plugins/jquery.easing-1.3.pack.js"></script>
    <!-- jQuery Bootstrap -->
    <script src="/assets/plugins/bootstrap-3.3.2/js/bootstrap.min.js"></script>
    <!-- Lightbox -->
    <script src="/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Theme JS -->
    <script src="/assets/theme/js/theme.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/assets/plugins/multiple-select/multiple-select.css" />
    <link rel="stylesheet" href="/css/app.css" />
    <!-- maps -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAP06wLpfieGKFt1mz996L8i7Z6d42-lDQ"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script src="/assets/plugins/gmap3.min.js"></script>
    <!-- maps single marker -->
    <script src="/assets/theme/js/map-detail.js"></script>


    <!-- Themes styles-->
    <link href="/assets/theme/css/theme.css" rel="stylesheet">
    <!-- Your custom css -->
    <link href="/assets/theme/css/theme-custom.css" rel="stylesheet">
    @yield('extra_css')
    <link rel="shortcut icon" href="/favicon.ico"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/assets/plugins/cleave/dist/cleave.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/multiple-select/multiple-select.js" type="text/javascript"></script>
</head>
<body>
<!-- wrapper page -->
<div class="wrapper">

<!-- main-header -->
<style type="text/css">
    @if(Auth::check())
    @if(Auth::user()->isPoster())
    .main-navbar .navbar-nav > li > a{
            padding: 20px 10px;
    }
    @endif
    @if(Auth::user()->isCustomer())
    .main-navbar .navbar-nav > li > a{
            padding: 20px 15px;
    }
    @endif
    @else
    .main-navbar .navbar-nav > li > a{
            padding: 20px 15px;
    }
    @endif
    .main-navbar .dropdown-menu > li > a{
            padding: 8px 30px !important;
    }
    a.link-profile {
        padding-left: 0px !important;
    }
    .dropdown-menu .sub-menu {
      left: 100%;
      position: absolute;
      top: 0;
      visibility: hidden;
      margin-top: -1px;
    }

    .dropdown-menu li:hover .sub-menu {
      visibility: visible;
    }

    .dropdown:hover .dropdown-menu {
      display: block;
    }

    .nav-tabs .dropdown-menu,
    .nav-pills .dropdown-menu,
    .navbar .dropdown-menu {
      margin-top: 0;
    }
</style>
<header class="main-header">
    <div class="container">
        <nav class="mobile-nav hidden-md hidden-lg">
            <a href="#" class="btn-nav-toogle first">
                <span class="bars"></span>
                Menu
            </a>

            <div class="mobile-nav-block">
                <h4>Navigation</h4>
                <a href="#" class="btn-nav-toogle">
                    <span class="barsclose"></span>
                    Close
                </a>

                <ul class="nav navbar-nav">
                    @if(Auth::check())
                    @if(Auth::user()->isPoster())
                    <li class=""><a href="/poster/job_post"><strong>Advertise Job</strong></a></li>
                    @if(Auth::user()->credits == 0)
                    <li class=""><a href="/poster/buy_credits"><strong>Buy Credits</strong></a></li>
                    @endif
                    <li><strong><a style="text-decoration: none;">View my Jobs</a></strong></li>
                            <ul style="text-decoration: none; list-style-type: none; padding-left: 25px;">
                                <li><a href="/poster">Active jobs</a></li>
                                <li><a href="/poster/inactive">Inactive jobs</a></li>
                                <li><a href="/poster/expired">Expired jobs</a></li>
                            </ul>
                    <li class=""><a href="/poster/applicants"><strong>View Applicants</strong></a></li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115004009668-What-is-the-Connect-Program-"><strong>Connect Program</strong></a></li>
                    <li class=""><a href="/poster/transaction"><strong>Transactions</strong></a></li>
                    <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support"><strong>Support/FAQ's</strong></a></li>
                    <li><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif
                    @if(Auth::user()->isCustomer())
                    <li class=""><a href="/customer"><strong>Home</strong></a></li>
                    <li><a href="/customer/subscriptions"><strong>Job Alerts</strong></a></li>
                    <li><a href="/customer/savedjobs"><strong>Saved Jobs</strong></a></li>
                    <li><a href="/customer/appliedjobs"><strong>Applied Jobs</strong></a></li>
                    <li><a href="/customer/inbound-program"><strong>Connect Program</strong></a></li>
                    <li><strong><a style="text-decoration: none;">Profile</a></strong></li>
                            <ul style="text-decoration: none; list-style-type: none; padding-left: 25px;">
                                <li><a href="/customer/edit-user">Edit profile</a></li>
                                <li><a href="/customer/change_password">Change Password</a></li>
                                <li><a href="/customer/delete-account" onclick="return confirm('Are you sure you want to delete your account? This will:\n\n  * Remove any active Job Alerts you have set up\n  * Remove your records from the Inbound Program (if you had registered)\n  * Prevent you from being able to log back into your account\n\nPress OK to continue or Cancel to go back')">Delete Account</a></li>
                            </ul>
                    <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436708-Job-Seekers-Support"><strong>FAQ's</strong></a></li>
                    <li><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif
                    @else
                    <li class=""><a href="/"><strong>Home</strong></a></li>
                    <li class=""><a href="/register-user"><strong> Job Alerts</strong></a></li>
                    <li><strong><a href=""> Job Advertiser</a></strong>
                            <ul style="list-style-type: none; padding-left: 25px;">
                                <li><a href="/register-poster">Advertise a Job</a></li>
                                <li><a href="/connect-program-advertiser">Search quality candidates</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-">Prices</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support">FAQ's</a></li>
                            </ul>
                    </li>
                    <li class=""><a href="/connect-program"><strong>Connect Program</strong></a></li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-"><strong>Pricing</strong></a></li>
                    <li class=""><a href="/faq"><strong>FAQ's</strong></a></li>
                    <li class=""><a href="/aboutus"><strong>About Us</strong></a></li>
                    <li class=""><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif
                    @if(!Auth::check())
                    <li class="link-btn"><a href="/login">Login</a></li>
                    <li class="link-btn"><a href="/register">Register</a>
                    @else 
                    <li class="link-btn"><a href="/logout">Logout</a></li>
                    @endif

                </ul>
            </div>
        </nav>
    </div>
    <!-- main navbar -->
    <div class="container">
        <div class="pull-left">
            <a href="/" >
                <img src="/jobs.png" alt="logo" style="height:100px; padding-top: 15px; padding-bottom: 15px;"/>
            </a>
            <div><a href="tel:+61 1300 487 692" style="

                    font-weight: bold;
                    ">1300 ITSMYCALL (1300 487 692)</a>
            </div>
        </div>
        <div class="pull-right hidemobile" style="margin-top: -10px;">
            <div class="pull-left" style="position: relative;left: 140px;top: 15px;">
                <b>Proudly partnered by:</b>
            </div>
            <a href="http://www.contactcentrecentral.com" target="_blank">
                <img src="/ccctop.png" alt="logo" style="width:150px; padding-top: 15px; padding-bottom: 15px;"/>
            </a>
            <a href="http://www.auscontact.com.au/" target="_blank">
                <img src="/actop.png" alt="logo" style="width:150px; padding-top: 15px; padding-bottom: 15px;"/>
            </a>

        </div>
    </div>

    <nav class="navbar navbar-default main-navbar hidden-sm hidden-xs">

        <div class="container">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-right: 0;">


                <ul class="nav navbar-nav">
                    @if(Auth::check())
                    @if(Auth::user()->isPoster())
                    <li class=""><a href="/poster/job_post"><strong>Advertise Job</strong></a></li>
                    @if(Auth::user()->credits == 0)
                    <li class=""><a href="/poster/buy_credits"><strong>Buy Credits</strong></a></li>
                    @endif
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" style="padding-left: 10px !important;"><strong>View my Jobs <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/poster">Active jobs</a></li>
                                <li><a href="/poster/inactive">Inactive jobs</a></li>
                                <li><a href="/poster/expired">Expired jobs</a></li>
                            </ul>
                    </li>
                    <li class=""><a href="/poster/applicants"><strong>View Applicants</strong></a></li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115004009668-What-is-the-Connect-Program-"><strong>Connect Program</strong></a></li>
                    <li class=""><a href="/poster/transaction"><strong>Transactions</strong></a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" style="padding-left: 10px !important;"><strong>Contact <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support">Support/FAQ's</a></li>
                                <li><a href="/contact">Contact Us</a></li>
                            </ul>
                    </li>
                    @endif
                    @if(Auth::user()->isCustomer())
                    <li class=""><a href="/customer"><strong>Home</strong></a></li>
                    <li><a href="/customer/subscriptions"><strong>Job Alerts</strong></a></li>
                    <li><a href="/customer/savedjobs"><strong>Saved Jobs</strong></a></li>
                    <li><a href="/customer/appliedjobs"><strong>Applied Jobs</strong></a></li>
                    <li><a href="/customer/inbound-program"><strong>Connect Program</strong></a></li>
                    <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436708-Job-Seekers-Support"><strong>FAQ's</strong></a></li>
                    <li><a href="/contact"><strong>Contact</strong></a></li>
                    @endif
                    @else
                    <li class=""><a href="/"><strong>Home</strong></a></li>
                    <li class=""><a href="/register-user"><strong>Job Alerts</strong></a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" style="padding-left: 20px !important;"><strong>Job Advertiser <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/register-poster">Advertise a Job</a></li>
                                <li><a href="/connect-program-advertiser">Search quality candidates</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-">Prices</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support">FAQ's</a></li>
                            </ul>
                    </li>
                    <li class=""><a href="/connect-program"><strong>Connect Program</strong></a></li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-"><strong>Pricing</strong></a></li>
                    <li class=""><a href="/faq"><strong>FAQ's</strong></a></li>
                    <li class=""><a href="/aboutus"><strong>About Us</strong></a></li>
                    <li class=""><a href="/contact"><strong>Contact</strong></a></li>
                    @endif

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                    @if(Auth::user()->isPoster())<li class="credits"><a style="text-decoration: none;" href="/poster/transaction">Balance: ${{(Auth::user()->credits > 0) ? number_format(Auth::user()->credits, 2, '.', '') : '0.00'}}</a></li>@endif
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="link-profile dropdown-toggle"  data-toggle="dropdown" >
                                @if(Auth::user()->first_name) {{Auth::user()->first_name}} @else Profile @endif <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu" role="menu">

                                @if(Auth::user()->isPoster())
                                <li><a href="/poster/edit-poster">Edit Profile</a></li>
                                <li><a href="/poster/change_password">Change Password</a></li>
                                <li><a href="/poster/delete-account" onclick="return confirm('Are you sure you want to delete your account? This will:\n\n    * Remove any active jobs you have on our website (Please note it is your responsibility to advise any candidates that may have applied.)\n    * Remove any remaining credits you have in your account\n    * Prevent you from being able to log back into your account so you will not be able to view previous jobs, applicants etc\n\nPress OK to continue or Cancel to go back.')">Delete Account</a></li>
                                @else
                                <li><a href="/customer/edit-user">Edit profile</a></li>
                                <li><a href="/customer/change_password">Change Password</a></li>
                                <li><a href="/customer/delete-account" onclick="return confirm('Are you sure you want to delete your account? This will:\n\n  * Remove any active Job Alerts you have set up\n  * Remove your records from the Inbound Program (if you had registered)\n  * Prevent you from being able to log back into your account\n\nPress OK to continue or Cancel to go back')">Delete Account</a></li>
                                @endif
                            </ul>
                        </li>
                        <li class="link-btn"><a href="/logout"><span class="btn btn-theme  btn-pill btn-xs btn-line">Logout</span></a></li>
                    </ul>
                    @else

                    <ul class="nav navbar-nav navbar-right">
                        <li class="link-btn"><a href="/login"><span
                                    class="btn btn-theme btn-pill btn-xs btn-line">Login</span></a></li>
                        <li class="link-btn"><a href="/register"><span class="btn btn-theme  btn-pill btn-xs btn-line">Register</span></a>
                        </li>
                    </ul>

                    @endif

                </ul>
            </div>
        </div>
    </nav>
    <!-- end main navbar -->

    <!-- mobile navbar -->

</header><!-- end main-header -->
<?php
$error = \DB::table('errors')->where('name','404')->first();
?>
    @if($error)
    {!!$error->description!!}
    @endif

<!-- main-footer -->
<footer class="main-footer">


    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="list-inline link-footer text-center-xs">
                    <li><a href="/">Home</a></li>
                    <li><a href="/job_list">Find a Job</a></li>
                    <li><a href="/poster/job_post">Post a Job</a></li>
                    <li><a href="/aboutus">About Us</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                    <li><a href="/terms-conditions">Website Terms of Use</a></li>
                    <li><a href="/privacy">Privacy Policy</a></li>
                    <li><a href="http://itsmycall.zendesk.com/hc/en-us" target="_blank">Support</a></li>
                </ul>
            </div>
            <div class="col-sm-12 ">
                <p class="text-center-xs hidden-lg hidden-md hidden-sm">Stay Connected</p>
                <div class="socials text-center" style="margin-top:25px">
                    <a target="_blank" href="https://www.facebook.com/Its-My-Call-673427489500903/"><i class="fa fa-facebook" style="line-height: 2.2!important;"></i></a>
                    <a target="_blank" href="https://twitter.com/itsmycall_au"><i class="fa fa-twitter" style="line-height: 2.2!important;"></i></a>
                    <a target="_blank" href="https://www.youtube.com/channel/UCBnwgSP4BdTuaG34eJn9P3A"><i class="fa fa-youtube-play" style="line-height: 2.2!important;"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/itsmycall"><i class="fa fa-linkedin" style="line-height: 2.2!important;"></i></a>
                </div>
                <div style="text-align:center; margin-top:25px; margin-bottom:-5px;">
                    <b>Proudly partnered by:</b>
                </div>
                <div style="text-align:center;">
                    <a href="http://www.contactcentrecentral.com.au" target="_blank">
                        <img src="/ccctop.png" alt="logo" style="width:150px; padding-bottom: 15px;"/>
                    </a>
                    <a href="https://www.auscontact.com.au/" target="_blank">
                        <img src="/actop.png" alt="logo" style="width:150px; padding-bottom: 15px;"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</footer><!-- end main-footer -->

</div><!-- end wrapper page -->

</body>
</html>


