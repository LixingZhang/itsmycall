@extends('layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/basic.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/image-picker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <style>
        .dropzone {
            min-height: 100px;
            border: 1px solid #ccc;
            background: white;
            padding: 20px 20px;
            border-radius: 4px;
    }
        .com {
            color: #93a1a1;
        }

        .lit {
            color: #195f91;
        }

        .pun, .opn, .clo {
            color: #93a1a1;
        }

        .fun {
            color: #dc322f;
        }

        .str, .atv {
            color: #D14;
        }

        .kwd, .prettyprint .tag {
            color: #1e347b;
        }

        .typ, .atn, .dec, .var {
            color: teal;
        }

        .pln {
            color: #48484c;
        }

        .prettyprint {
            padding: 8px;
            background-color: #f7f7f9;
            border: 1px solid #e1e1e8;
        }

        .prettyprint.linenums {
            -webkit-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
            -moz-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
            box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
        }

        /* Specify class=linenums on a pre to get line numbering */
        ol.linenums {
            margin: 0 0 0 33px; /* IE indents via margin-left */
        }

        ol.linenums li {
            padding-left: 12px;
            color: #bebec5;
            line-height: 20px;
            text-shadow: 0 1px 0 #fff;
        }

        input[type=radio] {
            display: none;
        }

        /* to hide the checkbox itself */
        input[type=radio] + label:before {
            font-family: FontAwesome;
            display: inline-block;
        }

        input[type=radio],
        input[type=checkbox] {
            border: 0;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }

        input[type=radio] ~ label:before,
        input[type=checkbox] ~ label:before {
            font-family: FontAwesome;
            display: inline-block;
            content: "\f1db";
            letter-spacing: 10px;
            font-size: 23px;
            color: #FFF;
            width: 1.4em;
        }

        input[type=radio]:checked ~ label:before,
        input[type=checkbox]:checked ~ label:before {
            content: "\f00c";
            font-size: 23px;
            color: white;
            letter-spacing: 5px;
        }

        input[type=checkbox] ~ label:before {
            content: "\f096";
        }

        input[type=checkbox]:checked ~ label:before {
            content: "\f046";
            color: white;
        }

        input[type=radio]:focus ~ label:before,
        input[type=checkbox]:focus ~ label:before,
        input[type=radio]:focus ~ label,
        input[type=checkbox]:focus ~ label {
            color: white;
        }

        label.required:after {
            content: "*";
            color: red;
        }

        .required-empty,
        .required-empty:focus {
            background-color: #FFC0BF;
        }

         ul.thumbnails.image_picker_selector li .thumbnail img {
        max-height: 60px;
        background: #fff;
        }
        ul.thumbnails.image_picker_selector li .thumbnail.selected {
            background: #639941;
        }
        ul.thumbnails.image_picker_selector li .thumbnail {
            margin-bottom: 0;
        }
        .image_picker_selector .delete-icon {
            position: absolute;
            top: 3px;
            right: 3px;
            padding: 1px 2px;
            margin: 0;
            background: red;
            line-height: 13px;
            font-size: 12px;
            color: #fff;
        }
        .image_picker_selector .delete-icon:hover {
            cursor: pointer;
            text-decoration: none;
        }
        .image_picker_selector .thumbnail {
            position: relative;
        }
        .dz-preview {
          display: none;
        }
        .btn-block.disabled {
            background: grey;
        }
    </style>
@stop

@section('extra_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });

    </script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone-amd-module.min.js"></script>
    <script type="text/javascript">

      $(document).ready(function () {
        $( document ).tooltip({
              content: function () {
                  return $(this).prop('title');
              }
          });
        $('#summernote').summernote({
          height: 400,   //set editable area's height
          disableDragAndDrop: true,
          dialogsFade: true,
          toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
          ],
          callbacks: {
            onBlur: function() {
               var str =  $('#summernote').summernote('code');
                var res = [];
                res = str.match(/www/g);
                var com = [];
                com = str.match(/@/g);
                var matches = [];
                matches = str.match(/\bhttps?::\/\/\S+/gi);
                var matches2 = [];
                matches2 = str.match(/\b(http|https)?(:\/\/)?(\S*)\.(\w{2,4})(.*)/g);
                if(matches2 || matches || res || com){
                     $('.next').addClass('hidden');
                     $('.nextscreeningerror').addClass('hidden');
                     $('.nexterror').removeClass('hidden');
                }else{
                     $('.next').removeClass('hidden');
                     $('.nexterror').addClass('hidden');
                }
            }
          }
        });
        function deleteclick() {
            $('.image_picker_selector .delete-icon').on('click', function (e) {
                idvalue = $('input[name="featured_image_delete"]').val();
                listnumber = $(this).parent().parent().attr("class");
                lastChar = listnumber[listnumber.length -1];
                console.log(idvalue);
                if(idvalue == "") {
                    idvalue = $(".image-picker option:nth-child(" + lastChar + ")").val();
                } else {
                    idvalue = idvalue + ", " + $(".image-picker option:nth-child(" + lastChar + ")").val();
                }
                if($(".image-picker option:nth-child(" + lastChar + ")").hasClass("dbload") == true) {
                    $('input[name="featured_image_delete"]').val(idvalue);
                };
                $(".image-picker option:nth-child(" + lastChar + ")").remove();
                $(".image-picker").imagepicker();
                $(".image_picker_selector .thumbnail").append("<a class='delete-icon'>X</a>");
                $(".image_picker_selector li").each(function( ) {
                    preindex = $(this).prevAll().length;
                    index = preindex + 1;
                    listclass = "item-" + index;
                    $(this).addClass(listclass);
                });
                deleteclick();
            });
        } 
        function removeclick() {
             $('#dropzone-img .dz-remove').on('click', function (e) {
                $refimage = $(this).attr("refimage");
                $uploaded_images = $("#multipleimg").val();
                $replaced_images = $uploaded_images.replace($refimage, "");
                multiimg = multiimg.replace($refimage, "");
                $("#multipleimg").val($replaced_images);
             });
        }
          Dropzone.options.dropzoneImg = {
          url: "/poster/uploadfiles",
          acceptedFiles: 'image/jpeg,image/png,image/gif,image/svg',
          uploadMultiple: true,
          addRemoveLinks: true,
          parallelUploads: 1,
          maxFiles: {{ $numberofimages }},
          dictDefaultMessage: 'Select or drop files to upload', // MB
            init: function () {
                this.on('addedfile', function (file) {
                    if(file.size > (1024 * 1024 * 2)) // not more than 5mb
                    {
                    this.removeFile(file); // if you want to remove the file or you can add alert or presentation of a message
                    alert("File can not be larger than 2MB");
                    }
                })
                this.on("sending", function(file, xhr, formData) {
                  // Will send the filesize along with the file as POST data.
                  formData.append("_token", "{{ csrf_token() }}");
                });
                this.on('success', function (file, res) {
                    console.log('upload success...')
                    console.log(res)
                    console.log(file)
                    if($('input[name="multipleimg"]').val() == "") {
                        multiimg = res;
                    } else {
                        multiimg = multiimg + ", " + res;
                    }
                    console.log(multiimg);
                    $div_number = $('#dropzone-img .dz-preview').length + 2;
                    $('input[name="multipleimg"]').val(multiimg);
                    $('#dropzone-img .dz-preview:nth-child(' + $div_number + ') .dz-remove').attr('refimage', res);
                    removeclick();
                })
            }
        };
        @if($post->posts_packages == 1)
        var dropzoneImg = new Dropzone("#dropzone-img");
        @endif
        //Dropzone.js Options - Upload an image via AJAX.
        Dropzone.options.myDropzone = {
            acceptedFiles: 'image/jpeg,image/png,image/gif,image/svg',
            maxFilesize: 2,
            uploadMultiple: false,
            // previewTemplate: '',
            addRemoveLinks: false,
            // maxFiles: 1,
            dictDefaultMessage: '',
            init: function () {
                this.on('addedfile', function (file) {
                    if(file.size > (1024 * 1024 * 2)) // not more than 5mb
                    {
                    this.removeFile(file); // if you want to remove the file or you can add alert or presentation of a message
                    swal({
                        html: "File can not be larger than 2MB",                        
                    })
                    }
                })
                this.on('thumbnail', function (file, dataUrl) {
                    // console.log('thumbnail...');
                    $('.dz-image-preview').hide()
                    $('.dz-file-preview').hide()
                })
                this.on('success', function (file, res) {
                    console.log('upload success...')
                    console.log(res)
                    console.log(file)
                    var val = Math.floor(1000 + Math.random() * 9000);
                    $('.image-picker').append("<option data-img-src='" + res + "' value='" + val + "'></option>")
                    $(".image-picker").imagepicker();
                    $('input[name="featured_image"]').val(res)
                    $('input[name="featured_image_use"]').val(res)
                    $('.image_picker_selector img').on('click', function (e) {
                        logosrc = $(this).attr("src");
                        $('input[name="featured_image_use"]').val(logosrc)
                    });
                    $(".image_picker_selector .thumbnail").append("<a class='delete-icon'>X</a>");
                    $(".image_picker_selector li").each(function( ) {
                        preindex = $(this).prevAll().length;
                        index = preindex + 1;
                        listclass = "item-" + index;
                        $(this).addClass(listclass);
                    });
                    deleteclick();
                })
            }
        }
        var myDropzone = new Dropzone('#my-dropzone')


        $('#upload-submit').on('click', function (e) {
            e.preventDefault()
            //trigger file upload select
            $('#my-dropzone').trigger('click')
        })

        $(".image_picker_selector li").each(function( ) {
            preindex = $(this).prevAll().length;
            index = preindex + 1;
            listclass = "item-" + index;
            $(this).addClass(listclass);
        });
        
        var idvalue;

        deleteclick();

        $('.image_picker_selector img').on('click', function (e) {
            logosrc = $(this).attr("src");
            $('input[name="featured_image_use"]').val(logosrc)
        })

        var firstimage = $(".image-picker option:first-child").attr("data-img-src");
        $('input[name="featured_image_use"]').val(firstimage)
        Dropzone.autoDiscover = false;

        $('#selling1').keyup(function (e) {
          updateOutput(1);
        });

        $('#selling2').keyup(function (e) {
          updateOutput(2);
        });

        $('#selling3').keyup(function (e) {
          updateOutput(3);
        });

        function updateOutput(id) {
          var sampleInput = $('#selling' + id + '').val(),
            sampleInputLength = sampleInput.length;

          if (sampleInputLength >= 155) {
            sampleInput = sampleInput.substr(0, 150) + ".....";
          }

          $('#charCounter' + id + '').html('<span style="color:green">' + sampleInputLength + '</span> / 60 characters left.');
        }

        var category_el = $('#category');
        var render_type_el = $('#render_type');

        category_el.on('change', function () {
          $.ajax({
            url: "/api/get_sub_categories_by_category/" + $('#category').val(),
            success: function (sub_categories) {

              var $sub_category_select = $('#sub_category');
              $sub_category_select.find('option').remove();

              $.each(sub_categories, function (key, value) {
                $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>');
              });
            },
            error: function (response) {
            }
          });
        });

        render_type_el.on('change', function (ev) {
          var val = $(this).find('option:selected').val();

          console.log(val);

          if (val == "{{\App\Posts::RENDER_TYPE_TEXT}}") {
            $('#featured_image_div').hide();
            $('#image_parallax_div').hide();

            $('#gallery_image_div').hide();
            $('#video_div').hide();
            $('#video_parallax_div').hide();
          }

          if (val == "{{\App\Posts::RENDER_TYPE_IMAGE}}") {
            $('#featured_image_div').show();
            $('#image_parallax_div').show();

            $('#gallery_image_div').hide();
            $('#video_div').hide();
            $('#video_parallax_div').hide();
          }

          if (val == "{{\App\Posts::RENDER_TYPE_GALLERY}}") {
            $('#gallery_image_div').show();

            $('#featured_image_div').hide();
            $('#image_parallax_div').hide();
            $('#video_div').hide();
            $('#video_parallax_div').hide();
          }

          if (val == "{{\App\Posts::RENDER_TYPE_VIDEO}}") {
            $('#video_div').show();
            $('#video_parallax_div').show();
            $('#featured_image_div').show();

            $('#gallery_image_div').hide();
            //$('#featured_image_div').hide();
            $('#image_parallax_div').hide();
          }

        });

        //category_el.trigger('change');
        render_type_el.trigger('change');


          /* fill step 3 from step 2 */
        // get input value & put it by data-class
        $('#tab2 input, #tab2 textarea').on('change', function () {
          var value = $(this).val();
          var className = $(this).data('class');
          if (value.length > 0) {
            $('.' + className).html(value).removeClass('hidden');
          } else {
            $('.' + className).html(value).addClass('hidden');
          }
        });
        // get text from editor
        $('#tab2 #shortie .redactor-editor').bind("DOMSubtreeModified", function () {
          $('.company-description').html(($(this).html()));
        });
        // get attachment image
        $("#featured_image").on('change', function () {
          var reader = new FileReader();

          reader.onload = function (e) {
            // get loaded data and render thumbnail.
            $(".company-image").attr('src', e.target.result).css('max-width', '110px');
          };
          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
        });

          /* Calculate Price on all steps */
        var PriceStandartJob = $('#step3_box #standard').data('price'); // From: Standard Listing in Step 3
        var PriceStep3 = 0;
        var PriceStep4 = 0;
        var PriceStep5 = 0;
        var TotalPrice = 0;
        var BonusCredits = 0;

          /* fill step 4 from step 3 */
        $('#step3_box [name=posts_packages]').on('change', function () {
          var _this = $(this).parent().parent().parent();

          PriceStep3 = parseInt($(this).data('price'), 10) | 0;

          var title = _this.find('.title').html();
          var description = _this.find('.package-description').text();

            /* fill step5 */
          $('#step5_box .title').html(title);
          $('#step5_box .price').html('$' + PriceStep3);
          $('#step5_box .description').html(description);

            /* fill step6 */
          $('.package_name').html('<b>' + title + '</b>');
          $('#PostsPackages').val($(this).val());

          setTotalPrice();
        });

          /* Fill step 6 on change additional services step 4 */
        $('#step4_box .additional-upgrade, #step4_box .additional-upgrade-count').on('change', function () {
          PriceStep4 = 0;
          var upgrades = new Array();
          var posts_upgrades = new Array();
          $('#step4_box .additional-upgrade').each(function () {
            if ($(this).is(':checked')) {
              var id = $(this).data('id');
              var _this = $(this).parent().parent().parent();
              PriceStep4 += parseInt($(this).data('price'), 10) * parseInt($('#count_' + id).val(), 10);
                /* get upgrades to array */
              upgrades[id] = {
                'title': _this.find('.add-upgrade-title').html(),
                'price': _this.find('.add-upgrade-price').html()
              };
                /* create value to POST */
              posts_upgrades[id] = parseInt($('#count_' + id).val(), 10);
            }
          });
            /* fill step6 */
          $('#step6_box .upgrades .upgrade-title').html('$' + PriceStep4);

          if (PriceStep4 > 0) {
            $('#step6_box .upgrades').removeClass('hidden');
            $('#PostsUpgrades').val(JSON.stringify(posts_upgrades));

              /* fill Upgrades on step 5 */
            var upgradesHtml = '<h2>Upgrades:</h2>';
            upgrades.forEach(function (e) {
              upgradesHtml = upgradesHtml + '<li><h3>' + e.title + ': <small>' + e.price + '</small></h3></li>'
            });
            $('#step5Upgrades').html(upgradesHtml);
          }

          setTotalPrice();
        });

          /* Fill step 6 on change step 5 */
        $('#step5_box .package').on('change', function () {
          var id = $(this).data('id');

          PriceStep5 = parseInt($(this).data('price'), 10) | 0;
            /* Use Bonus on Step 5 */
          BonusCredits = parseInt($(this).data('bonus'), 10) | 0;

          var _this = $(this).parent().parent().parent();

          var title = _this.find('.add-package-title').html();
          title = title + ' ' + _this.find('.add-package-price').html();

            /* fill step6 */
          $('#step6_box .add-package .add-package-title').html(title);

          if (PriceStep5 > 0) {
            $('#step6_box .add-package').show();
            $('#PackagesStats').val($(this).val());
          } else {
            $('#step6_box .add-package').hide();
            $('#PackagesStats').val(0);
          }

          setTotalPrice();
        });

          /* Use my credits */
        $('#use_credits').on('change', function () {
          var credits = 0;
          if (this.checked) {
            credits = $(this).data('credits');
            $('#credits').val(credits);
            $('#step6_box .use-credits').removeClass('hidden');
          } else {
            $('#credits').val(0);
            $('#step6_box .use-credits').addClass('hidden');
          }
          setTotalPrice();
        });

        function setTotalPrice() {

          var credits = parseInt($('#use_credits').data('credits'), 10) | 0;
          var restToCredits = 0;

          var selectedPrice = PriceStep3 + PriceStep4;
          TotalPrice = selectedPrice;
          // Purchase as part of one of our Listing Packages
          if (PriceStep5 > 0) {
            if (PriceStep5 >= selectedPrice) {
              TotalPrice = PriceStep5;
              restToCredits = TotalPrice - selectedPrice;
              if (restToCredits < 0)
                restToCredits = 0;
            }
          }

            /* Calculate Credits */
          if ($('#use_credits').is(':checked')) {
            if (credits <= TotalPrice) {
              TotalPrice = TotalPrice - credits;
              $('#use_my_credits').html('$' + credits);
              credits = restToCredits;
            } else { // credits > TotalPrice
              credits = credits - TotalPrice + restToCredits;
              $('#use_my_credits').html('$' + TotalPrice);
              TotalPrice = 0;
            }
          } else {  //Not use credits and select Listing Packages
            if (PriceStep5 > 0) {
              credits = credits + restToCredits;
            }
          }

            /* Payment details */
          if (TotalPrice <= 0) {
            $('#payment-form .card-details').hide();
          } else {
            $('#payment-form .card-details').show();
          }

            /* Credits */
          //console.log(credits + ' ' + BonusCredits + ' ' + (credits + BonusCredits));
          $('#credits').val(credits + BonusCredits);
          $('#rest_credits').html(credits + BonusCredits);
          if ((credits + BonusCredits) > 0) {
            $('#step6_box .rest-credits').removeClass('hidden');
          } else {
            $('#step6_box .rest-credits').addClass('hidden');
          }

          var aus_member_discount = ($('#aus_contact_member_no').val() != '') ? (TotalPrice / 10).toFixed(2) : 0;
          $('#step6_aus_member_discount').html('$' + aus_member_discount);
          TotalPrice = TotalPrice - aus_member_discount;

          $('#step6_total_price').html(TotalPrice);
          $('#TotalPrice').val(TotalPrice);
          $('#step6_GST').html((TotalPrice / 10).toFixed(2));

          return;
        }
        ;

        // disable inactive tabs
        $('.a-tab').on('click', function (e) {
          if ($(this).hasClass('disabled')) {
            return false;
          }
        });

        // by clicking to 'next' button enable the following tab
        $('ul.pager li.next a').on('click', function () {
          var id = $('.tab-pane.active').first().attr('id'); // id of 'old' tab. Example: tab2
          var enabled = true;
          var focused = false;
          $('#' + id + ' label.required').each(function () {
            var el = $(this).next().find('input').first();
            if (el.val() == "") {
              el.addClass('required-empty');
              if (!focused) {
                el.focus();
                focused = true;
              }
              enabled = false;
            }

            var se = $(this).next().find('select').first();
            if (se && se.val() == "") {
              se.addClass('required-empty');
              enabled = false;
            }
          });
          if (enabled === false) {
            return false;
          }
          // Listing Options required
          if (id == 'tab3') {
            if (!$('#tab3 [name=posts_packages]:checked').val()) {
              $('#tab3 .alert').removeClass('hidden');
              $('html, body').animate({
                scrollTop: $('#tab3 .alert').offset().top
              }, 200);
              return false;
            } else {
              $('#tab3 .alert').addClass('hidden');
            }
          }
          $('ul.nav.nav-pills .a-' + id).parent().next().find('a').removeClass('disabled');
        });

        $('input, select').on('blur', function (e) {
          var label = $(this).parent().parent().find('label').first();
          if (label.hasClass('required')) {
            if ($(this).val() != "") {
              $(this).removeClass('required-empty');
            } else {
              $(this).addClass('required-empty');
            }
          }
        });

      });
    </script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
      Stripe.setPublishableKey('pk_test_E2XIeaxGflAFKx1waejDVOOw');
    </script>
    <script>
      $(function () {
        var $form = $('#payment-form');
        $form.submit(function (event) {
          if ($('#TotalPrice').val() > 0) {
            // Disable the submit button to prevent repeated clicks:
            $form.find('.submit').prop('disabled', true);

            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);

          } else {
            submitForm(false);
          }
          // Prevent the form from being submitted:
          return false;
        });

        function stripeResponseHandler(status, response) {
          // Grab the form:
          var $form = $('#payment-form');

          if (response.error) { // Problem!

            // Show the errors on the form:
            $form.find('.payment-errors').text(response.error.message);
            $form.find('.submit').prop('disabled', false); // Re-enable submission


          } else { // Token was created!

            // Get the token ID:
            var token = response.id;

            submitForm(token);

            return false;

            // Submit the form:
            // $form.get(0).submit();
          }
        }
        ;

        function submitForm(token) {
          var $form = $('#payment-form');
          // Insert the token ID into the form so it gets submitted to the server:
          $form.append($('<input type="hidden" id="stripeToken" name="stripeToken">').val(token));

          // Append Summ
          // var summ = parseInt($('#step6_total_price').text(), 10) | 0;
          // $form.append($('<input type="hidden" id="summ" name="summ">').val(summ));

          //append fields from form on Step 2
          $form.append('<div class="hidden" id="submitted_form"></div>');
          $('#submitted_form').append($("#form-username").find(":input"));
          $('#submitted_form').append($("#billing-form").find(":input"));

          var _self = $('#payment-form'),
            _data = _self.serialize();

          $.ajax({
            url: _self.attr('action'),
            data: _data,
            method: _self.attr('method'),
            dataType: 'json',
            beforeSend: function (xhr) {
              // $('#').loadingIndicator();
              $('.modal').show();
            },
            success: function (response) {
              if (response.status == 200) {
                $('.modal').hide();
                $('#popup').removeClass('hidden');


              } else {
                $('.modal').hide();
                $('#error').append(response.error);

              }
            },
            error: function (response) {
              if (response.status == 200) {
                $('.modal').hide();
                $('#popup').removeClass('hidden');


              } else {
                $('.modal').hide();
                $('#error').append(response.error);

              }
            }
          });
          return true;
        }
        ;
      });

      function checkVid() {
        if (jQuery("#form-username").hasClass("jobpackage-1")) {
            if ($('#video_link').val().indexOf('youtu') > -1 || $('#video_link').val().indexOf('vimeo.com') > -1) {
                var url = $('#video_link').val();
                var a = youtube_parser(url);
                var b = '';
                if (a) {
                   b = "http://i3.ytimg.com/vi/" + a +  "/1.jpg";
                   $('#video_embed img').attr('src', b);
                    $('#video_embed').show();
                }
                
                $('#vidOK').html('<span style="color:green">Valid video URL</span>');
            } else {
                $('#video_embed').hide();
                if ($('#video_link').val() == "") {
                    $('#vidOK').html('Please enter a valid Vimeo or Youtube URL');
                } else {
                    $('#vidOK').html('<span style="color:red">Not a valid video URL</span>');
                }
            }  
        } else {
            swal({
                html: "We've noticed that you’ve added a video in your job description. Please note that video can only be included with our Featured Listings.",
            })
            $('#video_link').val("");
        }
        
      }
      




//      $('#hrsalary').on('keydown', function (e) {
//        console.log(e);
//        // tab, esc, enter
//        if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
//          // Ctrl+A
//          (e.keyCode == 65 && e.ctrlKey === true) ||
//          // home, end, left, right, down, up
//          (e.keyCode >= 35 && e.keyCode <= 40)) {
//          return;
//        }
//
//        e.preventDefault();
//
//        // backspace & del
//        if ($.inArray(e.keyCode, [8, 46]) !== -1) {
//          $(this).val('');
//          return;
//        }
//
//        var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
//        var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
//
//        var value = $(this).val();
//        var clean = value.replace(/\./g, '').replace(/,/g, '').replace(/^0+/, '');
//
//        var charCode = String.fromCharCode(e.keyCode);
//        console.log(charCode);
//        var p = $.inArray(charCode, a);
//        var g = $.inArray(charCode, n);
//
//        if (p !== -1) {
//          value = clean + n[p];
//
//          if (value.length == 2)
//            value = '0' + value;
//          if (value.length == 1)
//            value = '00' + value;
//
//          var formatted = '';
//          for (var i = 0; i < value.length; i++) {
//            var sep = '';
//            if (i == 2)
//              sep = '.';
//            if (i > 3 && (i + 1) % 3 == 0)
//              sep = ',';
//            formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
//          }
//
//          $(this).val(formatted);
//        }
//
//        if (g !== -1) {
//          console.log("WORK GOD DAMMIT");
//          value = clean + n[g];
//
//          if (value.length == 2)
//            value = '0' + value;
//          if (value.length == 1)
//            value = '00' + value;
//
//          var formatted = '';
//          for (var i = 0; i < value.length; i++) {
//            var sep = '';
//            if (i == 2)
//              sep = '.';
//            if (i > 3 && (i + 1) % 3 == 0)
//              sep = ',';
//            formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
//          }
//
//          $(this).val(formatted);
//        }
//
//        return;
//
//      });


      function getoembed(url1) {

        $.ajax({
          url: 'http://query.yahooapis.com/v1/public/yql',
          data: {
            q: "select * from json where url ='http://www.youtube.com/oembed?url='" + url1 + "",
            format: "json"
          },
          dataType: "jsonp",
          success: function (data) {

            alert(JSON.stringify(data));


          },
          error: function (result) {
            alert("Sorry no data found.");
          }
        });


      }

//      $('#salary').on('keydown', function (e) {
//        console.log(e);
//        // tab, esc, enter
//        if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
//          // Ctrl+A
//          (e.keyCode == 65 && e.ctrlKey === true) ||
//          // home, end, left, right, down, up
//          (e.keyCode >= 35 && e.keyCode <= 40)) {
//          return;
//        }
//
//        e.preventDefault();
//
//        // backspace & del
//        if ($.inArray(e.keyCode, [8, 46]) !== -1) {
//          $(this).val('');
//          return;
//        }
//
//        var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
//        var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
//
//        var value = $(this).val();
//        var clean = value.replace(/\./g, '').replace(/,/g, '').replace(/^0+/, '');
//
//        var charCode = String.fromCharCode(e.keyCode);
//        console.log(charCode);
//        var p = $.inArray(charCode, a);
//        var g = $.inArray(charCode, n);
//
//        if (p !== -1) {
//          value = clean + n[p];
//
//          if (value.length == 2)
//            value = '0' + value;
//          if (value.length == 1)
//            value = '00' + value;
//
//          var formatted = '';
//          for (var i = 0; i < value.length; i++) {
//            var sep = '';
//            if (i == 2)
//              sep = '.';
//            if (i > 3 && (i + 1) % 3 == 0)
//              sep = ',';
//            formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
//          }
//
//          $(this).val(formatted);
//        }
//
//        if (g !== -1) {
//          console.log("WORK GOD DAMMIT");
//          value = clean + n[g];
//
//          if (value.length == 2)
//            value = '0' + value;
//          if (value.length == 1)
//            value = '00' + value;
//
//          var formatted = '';
//          for (var i = 0; i < value.length; i++) {
//            var sep = '';
//            if (i == 2)
//              sep = '.';
//            if (i > 3 && (i + 1) % 3 == 0)
//              sep = ',';
//            formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
//          }
//
//          $(this).val(formatted);
//        }
//
//        return;
//
//      });

      $(function () {
        //result.address_components[0]
        $("#joblocation").geocomplete({
          details: ".details",
          detailsAttribute: "data-geo",
          country: "AU",
        }).bind("geocode:result", function (event, result) {
          geo_pickup = true;
          console.log(result.address_components);

          results = result.address_components;

          $.each(results, function (index, value) {
            if (value.types == 'postal_code') {
              $("#postcode").val(value.long_name);
              console.log(value.long_name); // here you get the zip code
            } else if (value.types[0] == 'locality') {
              $("#city").val(value.long_name);
            } else if (value.types[0] == 'administrative_area_level_1') {
              $('#state option[value=' + value.long_name + ']').attr('selected', 'selected');
            } else if (value.types[0] == 'country') {
              $("#country").val(value.long_name);
            }
          });

          //$("#postcode").val(result.address_components[5]['long_name']);
          console.log(result.geometry.location.lat());
          $("#lat").val(result.geometry.location.lat());
          $("#lng").val(result.geometry.location.lng());
          console.log(result.geometry.location.lng());
          console.log(result);


        });

        $("#find").click(function () {
          $("#joblocation").trigger("geocode");
        });

        $('#find').keypress(function (e) {
          if (e.which == 13) {//Enter key pressed
            $("#joblocation").trigger("geocode");
          }
        });


        $("#examples a").click(function () {
          $("#joblocation").val($(this).text()).trigger("geocode");
          return false;
        });

      });

    </script>



@stop


@section('content')
    <style>
        .center-pills {
            display: inline-block;
        }
    </style>
                                    <?php 
                                        $tab_company_logo = \App\Settings::where('column_key','tab_company_logo')->first();
                                        $tab_company_name = \App\Settings::where('column_key','tab_company_name')->first();
                                        $tab_title = \App\Settings::where('column_key','tab_title')->first();
                                        $tab_preview = \App\Settings::where('column_key','tab_preview')->first();
                                        $tab_main = \App\Settings::where('column_key','tab_main')->first();

                                        $tab_video = \App\Settings::where('column_key','tab_video')->first();
                                        $tab_photos = \App\Settings::where('column_key','tab_photos')->first();

                                        $tab_category = \App\Settings::where('column_key','tab_category')->first();
                                        $tab_subcategory = \App\Settings::where('column_key','tab_subcategory')->first();
                                        $tab_salary = \App\Settings::where('column_key','tab_salary')->first();
                                        $tab_estsalary = \App\Settings::where('column_key','tab_estsalary')->first();
                                        $tab_incentive = \App\Settings::where('column_key','tab_incentive')->first();
                                        $tab_freq = \App\Settings::where('column_key','tab_freq')->first();
                                        $tab_showsal = \App\Settings::where('column_key','tab_showsal')->first();
                                        $tab_emptype = \App\Settings::where('column_key','tab_emptype')->first();
                                        $tab_empstatus = \App\Settings::where('column_key','tab_empstatus')->first();
                                        $tab_workloc = \App\Settings::where('column_key','tab_workloc')->first();
                                        $tab_shiftguide = \App\Settings::where('column_key','tab_shiftguide')->first();

                                        $tab_applic = \App\Settings::where('column_key','tab_applic')->first();
                                        $tab_secemail = \App\Settings::where('column_key','tab_secemail')->first();
                                        $tab_url = \App\Settings::where('column_key','tab_url')->first();

                                    ?>
    <div class="modal"><!-- Place at bottom of page --></div>
    <div class="bg-color1 block-section line-bottom">
        <div class="container">
            <form action="/poster/uploadimg" id="my-dropzone" method="post" class="form single-dropzone form-horizontal form-bordered" enctype="multipart/form-data"> 
                <div class="form-group" id="featured_image_div">

                    <label for="featured_image" class="col-sm-3 control-label">Please choose the Company Logo to display</label>
                    <div class="col-sm-8" ><input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <select class="image-picker show-html">
                            <?php 
                              if (count($companylogos) == 0) {
                                if (!empty($post->featured_image)) {?>
                                  <option data-img-src="{{$post->featured_image}}" value="1"></option>
                              <?php }; } else { ?>
                                @foreach ($companylogos as $companylogo)
                                  <option class="dbload" <?php if ($companylogo->file == $post->featured_image) { print "selected";}; ?> data-img-src="{{ $companylogo->file }}" value="{{ $companylogo->id }}"></option>
                                @endforeach
                            <?php  }  ?>
                        </select>
                        <button id="upload-submit" class="btn btn-default margin-t-5"><i class="fa fa-upload"></i> Upload New Company Logo</button>
                    </div>
                </div>
            </form>

            <form action="/poster/edit-post" id="form-username" method="post" class="jobpackage-{{$post->posts_packages}} form-horizontal form-bordered"
                  enctype="multipart/form-data" novalidate>
                <input id="id" class="form-control" type="hidden" value="{{$post->id}}" name="id"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-6">
                        @include('admin.layouts.notify')
                    </div>
                </div>
                <input id="multipleimg" class="form-control" type="hidden" name="multipleimg"/>
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <h3 class='orangeudnerline' style="margin-bottom:25px">Advertisement Details</h3>


                <div class="form-group">
                    <label for="company" class="col-sm-3 control-label required">{{trans('messages.company')}}</label>
                    <div class="col-sm-8">
                        <input id="company" data-class="company-name" class="form-control" type="text" name="company"
                               placeholder="{{trans('messages.enter_post_company')}}" value="{{$post->company}}"/>
                    </div>
                </div>
                {{-- <div class="form-group" id="featured_image_div">
                    <label for="featured_image"
                           class="col-sm-3 control-label">{{trans('messages.company_logo')}}</label>
                    <div class="col-sm-8">
                        <img class="company-image" style="padding-bottom: 35px" src="{{$post->featured_image}}"
                             width="150"/>
                        <input id="featured_image" style="padding-bottom: 45px" class="form-control" type="file"
                               name="featured_image"/>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label for="title" class="col-sm-3 control-label required">{{trans('messages.title')}}</label>
                    <div class="col-sm-8">
                        <input id="title" data-class="job-title" class="form-control" type="text" name="title"
                               placeholder="{{trans('messages.enter_post_title')}}" value="{{$post->title}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="shortdescription" class="col-sm-3 control-label required">Preview Description</label>
                    <div class="col-sm-8" id="shortie">
                        <textarea id="shortdescription" class="form-control" name="shortdescription"
                                  maxlength="200">{{$post->short_description}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description"
                           class="col-sm-3 control-label required">Main Description</label>
                    <div class="col-sm-8">
                        <textarea id="summernote" class="form-control" name="description" maxlength="2500">{{$post->description}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-3 control-label">Selling Point 1</label>
                    <div class="col-sm-8">
                        <textarea id="selling1" data-class="selling-point1" class="form-control" name="selling1"
                                  maxlength="60">{{$post->selling1}}</textarea>
                        <small><span id="charCounter1"></span></small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-3 control-label">Selling Point 2</label>
                    <div class="col-sm-8">
                        <textarea id="selling2" data-class="selling-point2" class="form-control" name="selling2"
                                  maxlength="60">{{$post->selling2}}</textarea>
                        <small><span id="charCounter2"></span></small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-sm-3 control-label">Selling Point 3</label>
                    <div class="col-sm-8">
                        <textarea id="selling3" data-class="selling-point3" class="form-control" name="selling3"
                                  maxlength="60">{{$post->selling3}}</textarea>
                        <small><span id="charCounter3"></span></small>
                    </div>
                </div>
                @if($post->posts_packages == 1)
                <h3 class='orangeudnerline' style="margin-bottom:25px">Video and Graphics</h3>
                <div class="form-group">
                    <label for="video_link" class="col-sm-3 control-label">Include a video on this listing?<br>
                    </label>
                    <div class="col-sm-8">
                        <input onchange="checkVid()" id="video_link" class="form-control" type="text" name="video_link"
                               placeholder="Video’s can only be displayed for Featured Jobs" value="{{$post->video_link}}"/>
                        <small><span id="vidOK">Please enter a valid Vimeo or Youtube URL</span></small>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3" style="text-align: right;">
                        <label for="code">Uploaded Images</label>
                    </div>

                    <div class="col-sm-8" style="padding-right: 3px; padding-left: 7px; margin-bottom: 20px;">
                        @foreach($uploadfiles as $img)
                        <div class="col-sm-2" style="margin-bottom:5px; padding: 0;">
                            <a style="top: 0%; margin: 1px; position: absolute;" class="btn btn-danger btn-xs" href="/poster/deleteimage/{{$img->id}}">X</a>
                            <a href="{{$img->jobimages}}"><img style="width:120px; height: 70px;" src="{{$img->jobimages}}"></a>
                        </div>
                        @endforeach
                        @if(count($uploadfiles) == 0)
                            <p style="margin-top: 7px;">No Images Uploaded!</p>
                        @endif
                    </div>
                </div>

                <div class="form-group">    
                    <div class="col-sm-3" style="text-align: right;">
                        <label for="uploadimg" >Job Photos</label>
                    </div>
                    <div class="col-sm-8" style="padding-right: 3px; padding-left: 7px; margin-bottom: 20px;">
                        @if(count($uploadfiles) >= 5 )
                            <p>Maximum 5 images allowed!</p>
                        @else
                        <div id="dropzone-img" class="dropzone">
                            <div class="form-group">
                              <div class="fallback">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input id="uploadimg" name="file" type="file" multiple />
                              </div>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
                @endif
                <h3 class='orangeudnerline' style="margin-bottom:25px;  margin-top: 25px;">Job Details</h3>
                <div class="form-group" style="display:none">
                    <label for="country" class="col-sm-3 control-label">{{trans('messages.countries')}}</label>
                    <div class="col-sm-8">
                        <select id="country" name="country" class="form-control"></select>
                    </div>
                </div>


                <div class="form-group">
                    <label for="joblocation" class="col-sm-3 control-label">Job Location</label>
                    <div class="col-sm-8">
                        <input id="joblocation" class="form-control geocomplete" type="text" value="{{$post->joblocation}}" name="joblocation"/>
                        <input id="lat" class="form-control geocomplete" type="hidden" value="{{$post->lat}}"
                               name="lat"/>
                        <input id="lng" class="form-control geocomplete" type="hidden" value="{{$post->lng}}"
                               name="lng"/>
                        <input id="city" class="form-control geocomplete" type="hidden" value="{{$post->city}}"
                               name="city"/>
                        <input id="postcode" class="form-control geocomplete" type="hidden" value="{{$post->postcode}}"
                               name="postcode"/>
                    </div>
                </div>


                <div class="form-group">
                    <label for="state" class="col-sm-3 control-label required">{{trans('messages.states')}}</label>
                    <div class="col-sm-8">
                        <select id="state" name="state" class="form-control">
                            <option @if($post->state == "") selected="selected" @endif value="">Select State</option>
                            <option @if($post->state == "Australian Capital Territory") selected="selected" @endif value="Australian Capital Territory">
                                Australian Capital Territory
                            </option>
                            <option @if($post->state == "New South Wales") selected="selected"
                                    @endif value="New South Wales">New South Wales
                            </option>
                            <option @if($post->state == "Northern Territory") selected="selected"
                                    @endif value="Northern Territory">Northern Territory
                            </option>
                            <option @if($post->state == "Queensland") selected="selected" @endif value="Queensland">
                                Queensland
                            </option>
                            <option @if($post->state == "South Australia") selected="selected"
                                    @endif value="South Australia">South Australia
                            </option>
                            <option @if($post->state == "Tasmania") selected="selected" @endif value="Tasmania">
                                Tasmania
                            </option>
                            <option @if($post->state == "Victoria") selected="selected" @endif value="Victoria">
                                Victoria
                            </option>
                            <option @if($post->state == "Western Australia") selected="selected" @endif value="Western Australia">
                                Western Australia
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label for="category" class="col-sm-3 control-label">Job Category</label>
                    <div class="col-sm-8">
                        <select id="category" name="category" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                        @if($category->id == $post->category) selected="selected" @endif>{{$category->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="sub_category" class="col-sm-3 control-label">Secondary Category</label>
                    <div class="col-sm-8">

                        <select id="sub_category" name="sub_category" class="form-control">
                            @foreach($subcategories as $subcategory)
                                <?php if($subcategory->parent_id == $post->category_id) { ?>
                                <option value="{{$subcategory->id}}"
                                        @if($subcategory->id == $post->subcategory) selected="selected" @endif >{{$subcategory->title}}</option>
                                <?php } ?>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="salary_type" class="col-sm-3 control-label required">Salary Type</label>
                    <div class="col-sm-8">
                        <select id="salary_type" class="form-control" name="salarytype" onchange="changeSal()">
                            <option @if($post->salarytype == "annual") selected="selected" @endif value="annual">Annual
                                Salary
                            </option>
                            <option @if($post->salarytype == "hourly") selected="selected" @endif value="hourly">Hourly
                                Salary
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group" @if($post->salarytype != 'annual') style="display:none" @endif id="sal">
                    <label for="salary" class="col-sm-3 control-label required">Annual Salary ($)</label>

                    <div class="col-sm-8">
                        <input id="salary" class="form-control" type="number" onchange="changeAnnualSal()" value="{{round($post->salary, 0)}}" name="salary"/>
                        <p class="annual_error" style="display: none; color: red;"><small>Please enter an amount less than $500,000</small></p>
                    </div>
                </div>

                <div class="form-group" id="annual_ask" @if($post->salarytype != 'annual') style="display:none" @endif >
                    <label for="annual_ask" id='annual_ask_req' class="col-sm-3 control-label required">As you have entered an Annual Salary, do you want this job to appear in hourly salary searches as well?
                    </label>
                    <div class="col-sm-8">
                        <select id="annual_ask_select" class="form-control" onchange="annual_ask_select_change()" name="annual_ask_select">
                            <option @if($post->annual_ask_select == 'yes') selected="selected" @endif value="yes">Yes</option>
                            <option @if($post->annual_ask_select == 'no') selected="selected" @endif value="no">No</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="annual_ask_salary" @if($post->annual_ask_select == 'no' || $post->salarytype != 'annual') style="display:none" @endif>
                    <label for="annual_ask_salary" id='annual_ask_salary_req' class="col-sm-3 control-label required">Hourly Salary($)
                    </label>
                    <div class="col-sm-8">
                        <input id="annual_ask_salary_enter" class="form-control" value="{{$post->hrsalary}}" type="number" name="annual_ask_salary_enter" max="999.99" min="0" onchange="annual_ask_salary_enter_check()"/>
                        <p class="text_danger_annual" style="color: red; display: none;"><small>Please enter an amount less than $999.99</small></p>
                    </div>
                </div>

                <div class="form-group" id="hourlysal" @if($post->salarytype != 'hourly') style="display:none" @endif>
                    <label for="hrsalary" class="col-sm-3 control-label required">Hourly Salary ($)</label>
                    <div class="col-sm-8">
                        <input id="hrsalary" class="form-control" type="number"
                               min="1" max="999" step="any" value="{{$post->hrsalary}}"
                               name="hrsalary" onchange="changeHrSal()"/>
                        <p class="hourly_error" style="display: none; color: red;"><small>Please enter an amount less than $999.99</small></p>
                    </div>
                </div>

                <div class="form-group" id="hourly_ask" @if($post->salarytype != 'hourly') style="display:none" @endif>
                    <label for="hourly_ask" id='hourly_ask_req' class="col-sm-3 control-label required">As you have entered an Hourly Salary, do you want this job to appear in annual salary searches as well?
                    </label>
                    <div class="col-sm-8">
                        <select id="hourly_ask_select" class="form-control" onchange="hourly_ask_select_change()" name="hourly_ask_select">
                            <option @if($post->hourly_ask_select == 'yes') selected="selected" @endif value="yes">Yes</option>
                            <option @if($post->hourly_ask_select == 'no') selected="selected" @endif value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="hourly_ask_salary" @if($post->hourly_ask_select == 'no' || $post->salarytype != 'hourly') style="display:none" @endif>
                    <label for="hourly_ask_salary" id='hourly_ask_salary_req' class="col-sm-3 control-label required">Annual Salary($)
                    </label>
                    <div class="col-sm-8">
                        <input id="hourly_ask_salary_enter" class="form-control" value="{{round($post->salary, 0)}}" type="number" name="hourly_ask_salary_enter" max="500000" min="0" onchange="hourly_ask_salary_enter_check()"/>
                        <p class="text_danger_hourly" style="color: red; display: none;"><small>Please enter an amount less than $500,000</small></p>
                    </div>
                </div>


                <div class="form-group">
                    <label for="salary_info" class="col-sm-3 control-label required">Incentive Structure</label>
                    <div class="col-sm-8">
                        <select id="incentivestructure" class="form-control" name="incentivestructure">
                            <option @if($post->incentivestructure == "fixed") selected='selected' @endif value="fixed">
                               Base + Super
                            </option>
                            <option @if($post->incentivestructure == "basecommbonus") selected='selected'
                                    @endif value="basecommbonus">Base + Super + R&R/Bonus
                            </option>
                            <option @if($post->incentivestructure == "basecomm") selected='selected'
                                    @endif value="basecomm">Base + Super + Commissions
                            </option>
                            <option @if($post->incentivestructure == "commonly") selected='selected'
                                    @endif value="commonly">Commissions Only
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="pay_cycle" class="col-sm-3 control-label required">Pay Frequency</label>
                    <div class="col-sm-8">
                        <select id="pay_cycle" class="form-control" name="paycycle">
                            <option @if($post->paycycle == "weekly") selected='selected' @endif value="weekly">Weekly
                            </option>
                            <option @if($post->paycycle == "fortnightly") selected='selected'
                                    @endif value="fortnightly">Fortnightly
                            </option>
                            <option @if($post->paycycle == "monthly") selected='selected' @endif value="monthly">
                                Monthly
                            </option>
                            <option @if($post->paycycle == "no") selected='selected' @endif value="no">Not Disclosed
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Show salary on listing?</label>
                    <div class="col-sm-8">

                        <select id="showsalary" name="showsalary" class="form-control">
                            <option @if($post->showsalary == "0") selected='selected' @endif value="0">No</option>
                            <option @if($post->showsalary == "1") selected='selected' @endif value="1">Yes</option>
                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="employment_type"
                           class="col-sm-3 control-label required">{{trans('messages.post_employment_type')}}</label>
                    <div class="col-sm-8">
                        <select id="employment_type" name="employment_type" class="form-control">
                            <option @if($post->employment_type == "") selected='selected'
                                    @endif value="">{{trans('messages.post_employment_select_type')}}</option>
                            <option @if($post->employment_type == "part_time") selected='selected'
                                    @endif value="part_time">{{trans('messages.post_employment_type_part_time')}}</option>
                            <option @if($post->employment_type == "full_time") selected='selected'
                                    @endif value="full_time">{{trans('messages.post_employment_type_full_time')}}</option>
                            <option @if($post->employment_type == "casual") selected='selected'
                                    @endif value="casual">{{trans('messages.post_employment_type_casual')}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="employment_term"
                           class="col-sm-3 control-label required">{{trans('messages.post_employment_term')}}</label>
                    <div class="col-sm-8">
                        <select id="employment_term" name="employment_term" class="form-control">
                            <option @if($post->employment_term == "") selected='selected'
                                    @endif value="">{{trans('messages.post_employment_select_term')}}</option>
                            <option @if($post->employment_term == "permanent") selected='selected'
                                    @endif value="permanent">{{trans('messages.post_employment_permanent')}}</option>
                            <option @if($post->employment_term == "fixed_term") selected='selected'
                                    @endif value="fixed_term">{{trans('messages.post_employment_fixed_term')}}</option>
                            <option @if($post->employment_term == "temp") selected='selected'
                                    @endif value="temp">{{trans('messages.post_employment_temp')}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label required" for="workfromhome">Job Venue</label>
                    <div class="col-sm-8">
                        <select  name="work_from_home" class="form-control">
                                <option value="">Please Select</option>
                                <option value="1" @if($post->work_from_home == 1) selected="selected" @endif >I want to work at the business address only</option>
                                <option value="2" @if($post->work_from_home == 2) selected="selected" @endif >I'd be open to working from home on ocassions</option>
                                <option value="3" @if($post->work_from_home == 3) selected="selected" @endif >I'm only interested in work from home jobs</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label required" for="parentsfilter">Shift Times</label>
                    <div class="col-sm-8">
                        <select id="parentsoption" name="parentsoption" class="form-control">
                                <option value="">Please select the shift times closest to your requirements</option>
                                <option value="1" @if($post->parentsoption == 1) selected="selected" @endif >Standard Business Hours (Shifts between hours of 9.00am and 5.00pm Mon to Fri)</option>
                                <option value="2" @if($post->parentsoption == 2) selected="selected" @endif >Extended Business Hours (Shifts between hours of 8.00am and 8.00pm Mon to Fri)</option>
                                <option value="3" @if($post->parentsoption == 3) selected="selected" @endif >Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only)</option>
                                <option value="4" @if($post->parentsoption == 4) selected="selected" @endif >Afternoon Shift (Shifts between hours of 12pm and 12am)</option>
                                <option value="5" @if($post->parentsoption == 5) selected="selected" @endif >Night Shifts</option>
                                <option value="6" @if($post->parentsoption == 6) selected="selected" @endif >Rotating Shifts - no weekends</option>
                                <option value="7" @if($post->parentsoption == 7) selected="selected" @endif >Rotating Shifts - including weekend work</option>
                        </select>
                    </div>
                </div>

                <h3 class='orangeudnerline' style="margin-bottom:25px; margin-top: 25px;">Application Submission Information</h3>   

                <div class="form-group">
                    <label class="col-sm-3 control-label">How would you like to receive applications?</label>
                    <div class="col-sm-8">

                        <select id="wheretosend" name="application_method" onchange="showFields()" class="form-control">
                            <option @if($post->email_or_link == '') selected='selected' @endif value="0">I want to
                                receive applications via ItsMyCall
                            </option>
                            <option @if($post->email_or_link != '') selected='selected' @endif value="1">Send applicants
                                to a website or link to complete their application
                            </option>
                        </select>

                    </div>
                </div>
                <div class="field-wrap @if($post->email_or_link == '') @else hidden @endif" id="inside-itsmycall">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="padding-top: 0;">This is how you'll recieve job applications</label>
                        <ul class="col-sm-8" style="list-style-type: disc !important; margin-left: 15px; ">
                        <li>All applications and resumes will be stored in your account</li>
                        <li>You can manage the applicants status and send emails directly to applicants from ItsMyCall</li>
                        <li>You can still receive email alerts for each application and even send an alert to second email address if you choose to.</li>
                    </ul>
                    </div>
                    <div class="form-group">
                        <label for="person_email" style="padding-top: 0;" class="col-sm-3 control-label">Applications will be sent to your current email address</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" disabled="disabled" value="{{Auth::user()->email}}" />
                            <p>You can change this in your user preferences. <a target="_blank" href="/poster/edit-poster" tabindex="-1"> Click here to change </a></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="person_email" style="padding-top: 0;" class="col-sm-3 control-label">If required, enter a second email address you'd like us to send Job Application notifications to @if($tab_secemail->value_txt) <a style="text-decoration: none;" class="fa fa-info-circle" title="{!!$tab_secemail->value_txt!!}"></a>@endif </label>
                        <div class="col-sm-8">
                            <input id="person_email" class="form-control" type="email" name="person_email"
                                   placeholder="Secondary Email" value="{{$post->person_email}}"/>
                        </div>
                    </div>
                    @if($post->selectquestions)
                    <h3 class='col-sm-12' style="margin-top: 30px;">Optional Screening Questions </h3>
                    <div class="col-sm-12">
                    <?php 
                        $posts_select_question_text =  \App\Settings::where('column_key','posts_select_question_text')->first();
                    ?>
                    @if($posts_select_question_text) {!!$posts_select_question_text->value_txt!!} @endif             
                    </div> 
                    @if(json_decode($post->questions) > 0)
                    <?php
                       $questions = json_decode($post->questions);
                    ?>
                    <div class="form-group">
                        <label for="" style="padding-top: 0;" class="col-sm-3 control-label">Screening Questions selected for this job <a style="text-decoration: none;" class="fa fa-info-circle" title="<b>Screening Questions selected for this job: </b> Screening Questions that you have already selected for this job and will be asked to applicants when they apply for this job"></a></label>
                        <div class="col-sm-8">
                            <ul>
                                @foreach($questions as $question)
                                <li>
                                    {{$question}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="col-sm-12" style="padding-top: 0;">
                             <div class="table-responsive countgreater">
                                <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                <thead>
                                <tr style="color: white;background-color: #639941; ">
                                    <th style="padding: 10px;">Question   
                                    </th>
                                    <th style="padding: 10px;">Modify</th>
                                    <th style="padding: 10px;">Select</th>
                                </tr>
                                </thead>
                                <tbody class="added_quest">
                                    <tr>
                                        <td style="border-right: 0;"><h4 style="font-size: 16px; font-weight: 600;">Default Questions</h4></td>
                                        <td style="width: 160px;border-right: 0; border-left: 0;"></td>
                                        <td style="border-left: 0;"></td>
                                    </tr>
                                    <?php
                                        $questions = NULL;
                                        if($post->questions){
                                            $questions = json_decode($post->questions);
                                        }
                                     ?>
                                    @foreach($admin_questions as $question)
                                    <tr>
                                        <td style="border-right: 0;word-wrap:break-word;">{{$question->question}}</td>
                                        <td style="border-right: 0; border-left: 0;"></td>
                                        <td style="width: 160px;border-left: 0;">
                                          <div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;">
                                            <input id="checkbox{{$question->id}}" class="question" type="checkbox" value="{{$question->question}}" name="question[]" @if($questions) @foreach($questions as $key) @if($key == $question->question) checked="checked" @endif @endforeach @endif />
                                            <label for="checkbox{{$question->id}}" style="font-size: 22px;margin-bottom: 2px; font-weight: 200;">Select</label>
                                          </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="border-right: 0;"><h4 style="font-size: 16px; font-weight: 600;"><a style="text-decoration: none;" onclick="openall()">Your Questions <i style="    font-size: 15px;" class="fa fa-chevron-down" aria-hidden="true"></i></a></h4></td>
                                        <td style="width: 160px;border-right: 0; border-left: 0;"></td>
                                        <td style="border-left: 0;"><a data-toggle="modal" style="color: ; padding: 7px 12px; height: 36px;" class="btn btn-primary" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> Add New Question</a></td>
                                    </tr>
                                    <?php
                                        $questions = NULL;
                                        if($post->questions){
                                            $questions = json_decode($post->questions);
                                        }
                                     ?>
                                    @foreach($user_questions as $question)
                                    <tr class="table_question{{$question->id}} open_question hidden">
                                        <td class="question_edit" style="border-right: 0;word-wrap:break-word;">{{$question->question}}</td>
                                        <td style="width: 160px;border-right: 0; border-left: 0;">
                                            <a class="btn-info btn" style="text-decoration: none;" data-id="{{$question->id}}" data-question="{{$question->question}}" onclick="questionedit(this)">
                                            Edit
                                            </a>
                                            <a class="btn-danger btn" style="text-decoration: none;" data-id="{{$question->id}}" onclick="questionremove(this)">
                                            Remove
                                            </a>
                                        </td>
                                            
                                        <td style="border-left: 0;">
                                          <div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;">
                                            <input id="checkbox{{$question->id}}" class="question" type="checkbox" value="{{$question->question}}" name="question[]"
                                            @if($questions) @foreach($questions as $key) @if($key == $question->question) checked="checked" @endif @endforeach @endif  />
                                            <label for="checkbox{{$question->id}}" style="font-size: 22px;margin-bottom: 2px; font-weight: 200;">Select</label>
                                          </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                             </div>
                        </div>
                    </div>
                    @endif
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog" style="background: rgba( 255, 255, 255, 1 );">
                      <div class="modal-dialog modal-lg" style="margin: 155px auto !important;">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Create a Custom Question</h4>
                          </div>
                          <div class="modal-body">
                                <span class="question_success hidden"><i class="fa fa-check" aria-hidden="true"></i> Your Question has been added Successfully!</span>
                                <input type="text" id="question" class="form-control" style="height: 34px;" placeholder="Enter Question" onkeyup="countChar(this)" maxlength="100">
                                <span id="charNum2" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                          </div>
                          <div class="modal-footer">
                            <a class="btn btn-default questiongo" onclick="questionadd()">Save</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="field-wrap @if($post->email_or_link == '') hidden @endif" id="outside-itsmycall">
                    <div class="form-group">
                        <label for="email_or_link" id="linker" class="col-sm-3 control-label">Please enter the URL in which your applicants will be redirected to  @if($tab_url->value_txt) <a style="text-decoration: none;" class="fa fa-info-circle" title="{!!$tab_url->value_txt!!}"></a>@endif </label>
                        <div class="col-sm-8">
                            <input id="email_or_link" class="form-control" type="text" name="email_or_link"
                                   placeholder="Enter valid URL (e.g. http://www.google.com)" value="{{$post->email_or_link}}"/>
                        </div>
                    </div>
                </div>
                <input id="featured_image_use" class="form-control" type="hidden" name="featured_image_use"/>
                <input id="featured_image_delete" class="form-control" type="hidden" name="featured_image_delete"/>
                <input id="featured_image" style="padding-bottom: 45px" class="form-control" type="hidden" name="featured_image"/>

                <button type="submit" class="btn btn-primary btn-block">Save</button>
            </form>
        </div>
    </div>
<div class="modal fade" tabindex="-1" id="coverModal" role="dialog" style="background: rgba( 255, 255, 255, 1 );">
    <div class="modal-dialog" style="margin: 155px auto !important;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <p id="Note"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    <div class="row"></div>
    <script src="/js/image-picker.min.js"></script>
    <script>
    function openall(){
        if($('.open_question').hasClass( "hidden" )){
           $('.open_question').removeClass('hidden');
        }else{
            $('.open_question').addClass('hidden');
        }

    }
    function questionremove(val) {
        var id = $(val).data('id');
        var data = {id: id};
        $.ajax({
        url: "/poster/removequestion",
                data: data,
                headers:
        {
        'X-CSRF-Token': $('input[name="_token"]').val()
        },
                method: "post",
                success: function (response) {
                    $('.table_question'+id).remove();
                console.log(response);
                },
                error:  function (response) {
                console.log(response);
                }
        });
      };
    function questionedit(val) {
        var id = $(val).data('id');
        var question = $('.table_question'+ id +' .question_edit').text();
        $('.table_question'+ id +' .btn-info').attr('disabled', 'disabled');
        var data = {id: id};
        $('.table_question'+ id +' .question_edit').html('<input data-id="'+ id +'" data-question="'+ question +'" class="form-control edited_question" style="height: 34px; margin-bottom:5px;" placeholder="Edit Question" value="'+ question +'" maxlength="100" /> <a style="text-decoration: none;" data-id="'+ id +'" data-question="'+ question +'" class="btn-default btn" onclick="editsave(this)">Save</a> <a style="text-decoration: none;" data-id="'+ id +'" data-question="'+ question +'" class="btn-default btn" onclick="savecancel(this)">Cancel</a>')
      };
    function savecancel(val) {
        var id = $(val).data('id');
        var question = $(val).data('question');
        $('.table_question'+ id +' .question_edit').html(question)
        $('.table_question'+ id +' .btn-info').removeAttr('disabled');
      };
    function editsave(val) {
        var id = $(val).data('id');
        var question = $('.edited_question').val();
        $('.table_question'+ id +' .btn-info').removeAttr('disabled');
        if(!$('.edited_question').val()){
            var data = {id: id};
            $.ajax({
            url: "/poster/removequestion",
                    data: data,
                    headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            },
                    method: "post",
                    success: function (response) {
                        $('.table_question'+id).remove();
                    console.log(response);
                    },
                    error:  function (response) {
                    console.log(response);
                    }
            }); 
        }else{
            var data = {id: id , question:question};
            $.ajax({
            url: "/poster/edit_question",
                    data: data,
                    headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            },
                    method: "post",
                    success: function (response) {
                        $('.table_question'+ id +' .question_edit').html(question)
                        $('.table_question'+ id +' .question').val(question)
                    console.log(response);
                    },
                    error:  function (response) {
                    console.log(response);
                    }
            });
        }
      };
        $(document).ready(function () {
           $(".question").change(function () {
              var maxAllowed = 5;
              var cnt = $(".question:checked").length;
              if (cnt > maxAllowed)
              {
                 $(this).prop("checked", "");
                 alert('You can only select a maximum of 5 screening questions.');
             }
          });
        });

    function countChar(val) {
        if($(val).val()){
            $('.questiongo').removeAttr('disabled')
            $('#question').css('background-color', 'initial');
        }
        $('#charNum2').removeClass('hidden');
        var maxLength = 100;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum2').text('Remaining Characters: '+length+'/100');
      };
    function questionadd() {
    // alert("hello");
    if(!$('#question').val()){
        $('#question').css('background-color', '#FFC0BF !important');
        $('.questiongo').attr('disabled', 'disabled');
    }else{
        var question = $('#question').val();
        var data = {question: question};
        $.ajax({
        url: "/poster/addquestion",
                data: data,
                headers:
        {
        'X-CSRF-Token': $('input[name="_token"]').val()
        },
                method: "post",
                success: function (response) {
                    $('.added_quest').append('<tr class="table_question'+ response +' open_question"><td class="question_edit" style="border-right: 0;word-wrap:break-word;">' + question + '</td><td style="width: 160px;border-right: 0; border-left: 0;"><a class="btn-info btn" style="text-decoration: none;" data-question="'+ question +'" data-id="'+ response +'" onclick="questionedit(this)">Edit</a> <a style="text-decoration: none;" data-question="'+ question +'" data-id="'+ response +'" class="btn-danger btn" onclick="questionremove(this)">Remove</a></td><td style="border-left: 0;"><div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;"><input id="checkbox'+ response +'" class="question" type="checkbox" value="'+ question +'" name="question[]"  /><label for="checkbox'+ response +'" style="font-size: 22px; margin-bottom: 2px; font-weight:200;">Select</label></div></td></tr>');
                    $('#question').val('')
                    $('.open_question').removeClass('hidden');
                    $('#charNum2').addClass('hidden');
                    $('#button_remove').addClass('hidden');
                    $('.question_success').removeClass('hidden');
                    $('.countgreater').removeClass('hidden');
                    $('#myModal').modal('hide');
                setTimeout(function () { 
                    $('.question_success').addClass('hidden');
                 }, 5000);
                console.log(response);
                },
                error:  function (response) {
                console.log(response);
                }
        });
    }
    }
      $(".image-picker").imagepicker();
      $(".image_picker_selector .thumbnail").append("<a class='delete-icon'>X</a>");
      <?php if (strlen($post->email_or_link) < 0) { ?>
$("#onbtn").click();

      <?php } else { ?>
$("#inbtn").click();
      <?php } ?>

      function showFields() {
        console.log("hello");
        var val = $('#wheretosend').val();

        if (val == 0) {
          $('#inside-itsmycall').removeClass('hidden');
          $('#outside-itsmycall').addClass('hidden');
          $('#email_or_link').removeClass("required");
        } else {
          $('#inside-itsmycall').addClass('hidden');
          $('#outside-itsmycall').removeClass('hidden');
          $('#email_or_link').addClass("required");
        }
      }

      function loadCoverModal(id) {
        $('#coverModal').modal('toggle');
        $('#Note').html(id);
        }

      function changeSal() {
        var val = $('#salary_type').val();
        if (val == 'hourly') {
            $('#hourlysal').show('slow');
            $('#sal').hide('slow');

            $('#annual_ask').hide('slow');
            $('#annual_ask_req').removeClass("required");
            $('#annual_ask_salary').hide('slow');
            $('#annual_ask_salary_req').removeClass("required");

            $('#hourly_ask').show('slow');
            $('#hourly_ask_req').addClass("required");
            $('#hourly_ask_select').val("no");
        } else {

            $('#hourlysal').hide('slow');
            $('#sal').show('slow');

            $('#annual_ask').show('slow');
            $('#annual_ask_req').addClass("required");
            $('#annual_ask_select').val("no");

            $('#hourly_ask').hide('slow');
            $('#hourly_ask_req').removeClass("required");
            $('#hourly_ask_salary').hide('slow');
            $('#hourly_ask_salary_req').removeClass("required");
        }
      }

      function changeHrSal() {
        $( "#hrsalary" ).val(Math.round($('#hrsalary').val() * 100) / 100);
        if ($('#hrsalary').val() < 0){
            $('.hourly_error').show('slow');
            $('#hrsalary').val(0);
        }else if ($('#hrsalary').val() > 999){
            $('.hourly_error').show('slow');
            $('#hrsalary').val(999);
        }else{
            $('.hourly_error').hide('slow');
        }
      }

      function changeAnnualSal() {
        $( "#salary" ).val(Math.round($('#salary').val() * 100) / 100);
        if ($('#salary').val() < 0){
            $('.annual_error').show('slow');
            $('#salary').val(0);
        }else if ($('#salary').val() > 500000){
            $('.annual_error').show('slow');
            $('#salary').val(500000);
        }else{
            $('.annual_error').hide('slow');
        }
      }

    function annual_ask_select_change() {
        var val = $('#annual_ask_select').val();
        if (val == 'yes') {
            $('#annual_ask_salary').show('slow');
            $('#annual_ask_salary_req').addClass("required");
        } else {
            $('#annual_ask_salary').hide('slow');
            $('#annual_ask_salary_req').removeClass("required");
        }

    }

    function hourly_ask_select_change() {
        var val = $('#hourly_ask_select').val();
        if (val == 'yes') {
            $('#hourly_ask_salary').show('slow');
            $('#hourly_ask_salary_req').addClass("required");
        } else {
            $('#hourly_ask_salary').hide('slow');
            $('#hourly_ask_salary_req').removeClass("required");
        }

    }

    function annual_ask_salary_enter_check() {
        $( "#annual_ask_salary_enter" ).val(Math.round($('#annual_ask_salary_enter').val() * 100) / 100);
        var val = $('#annual_ask_salary_enter').val();
        if (val > 999.99) {
            $('#annual_ask_salary_enter').val('999.99');
            $('.text_danger_annual').show('slow');
        } else if(val < 0){
            $('#annual_ask_salary_enter').val('0');
            $('.text_danger_annual').show('slow');
        }else{
            $('.text_danger_annual').hide('slow');
        }

    }

    $( "#salary" ).change(function() {
      var salary = parseFloat($("#salary").val().replace(/,/g, ''));
        if (salary > 0 && salary < 500000) {
            $go_sal = salary/1976;
            $('#annual_ask_salary_enter').attr("placeholder",'e.g =' + ($go_sal).toFixed(0));
        }else{
            $('#annual_ask_salary_enter').removeAttr('placeholder')
        }
    });

    $( "#hrsalary" ).change(function() {
      var salary = parseFloat($("#hrsalary").val().replace(/,/g, ''));
        if (salary > 0 && salary < 999.99) {
            $go_sal = salary*1976;
            $('#hourly_ask_salary_enter').attr("placeholder",'e.g =' + ($go_sal).toFixed(0));
        }else{
            $('#hourly_ask_salary_enter').removeAttr('placeholder')
        }
    });

    function hourly_ask_salary_enter_check() {
        $( "#hourly_ask_salary_enter" ).val(Math.round($('#hourly_ask_salary_enter').val() * 100) / 100);
        var val = $('#hourly_ask_salary_enter').val();
        if (val > 500000) {
            $('#hourly_ask_salary_enter').val('500000');
            $('.text_danger_hourly').show('slow');
        } else if(val < 0){
            $('#hourly_ask_salary_enter').val('0');
            $('.text_danger_hourly').show('slow');
        }else{
            $('.text_danger_hourly').hide('slow');
        }

    }

      function emptypeChange() {
        $('#employment_type').val($('#emptype').val());
      }

      function emptermChange() {
        $('#employment_term').val($('#empterm').val());
      }

      var changer = false;
      function changeCat() {
        if (changer) {
          $.ajax({
            url: "/api/get_sub_categories_by_category/" + $('#category').val(),
            success: function (sub_categories) {

              var $sub_category_select = $('#sub_category');
              $sub_category_select.find('option').remove();

              $.each(sub_categories, function (key, value) {
                $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>');
              });
            },
            error: function (response) {
            }
          });
        }
      }

      $(document).ready(function () {
          $alert = false;
          $(".redactor-editor").on('DOMSubtreeModified', function() {
            if (jQuery("#form-username").hasClass("jobpackage-1")) {
            } else {
                productdescription = $(".redactor-editor").html();
                matchs = productdescription.match(/<img/gi);
                if (matchs !== null) {
                    if ($alert == false) {
                        swal({
                            html: "We've noticed that you’ve added photos in your job description. Please note that photos can only be included with our Featured Listings. Please remove the photo to save the Job",
                        });
                        $alert = true;
                    }
                        jQuery(".btn-primary.btn-block").addClass("disabled");
                } else {
                    jQuery(".btn-primary.btn-block").removeClass("disabled");
                }
            }
          });
      });

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
@endsection