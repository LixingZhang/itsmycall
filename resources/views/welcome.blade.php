@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="clearfix">
			<div class="col-sm-6">
				<img src="/uploads/images/Fotolia_3368514_Subscription_Monthly_M.jpg" style="width: 100%; margin-top: 20px;">
			</div>
			<div class="col-sm-6">
				<h3>Finally a new solution!</h3>
				<p>Finding call centre staff has never been easy and with the current employment market and the increasing complexity of call centre work its never been tougher! That why we’ve set out to provide a new alternative. On ItsMyCall:</p>
				<ul>
					<li>We have over 59 job categories from agents to executive roles so you can target the exact call centre skills you need. </li>
					<li>We’ve got the biggest audience of contact centre professionals in Australia</li>
					<li>Our prices start from an amazing $14 with our launch special</li>
				</ul>
				<div class="col-xs-6 text-center"><span style="background-color: #ffffff; color: #ff6600;">Post a job now!</span></div>
				<div class="col-xs-6 text-center"><a href="/poster/job_post" class="btn btn-primary" style="height: auto;">Click here</a></div>
			</div>
		</div>
		<hr>
		<div class="clearfix">
			<div class="col-sm-6">
				<div class="col-sm-12">
					<h2>Testimonials</h2>
				</div>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				    	<div class="col-sm-4 text-center">
			    			<img src="/uploads/images/Jason-Price.jpg" style="width: 100%; max-width:300px; margin: 5px 0;">
				    	</div>
				    	<div class="col-sm-8">
				    		<div>
									<p><i>“By using ItsMyCall I was able to fill a senior role in a fraction of the cost of using an agency.</i></p>
									<p><i>Tapping into their huge audience base put my role directly in front of senior executives and the results were almost instantaneous!”</i></p>
									<p><b>Brad Shaw</b>, Managing Director, LivePro</p>
				    		</div>
				    	</div>
				    </div>
				    <div class="item">
				    	<div class="col-sm-4 text-center">
			    			<img src="/uploads/images/Jason-Price.jpg" style="width: 100%; max-width:300px; margin: 5px 0;">
				    	</div>
				    	<div class="col-sm-8">
				    		<div>
									<p><i>“By using ItsMyCall I was able to fill a senior role in a fraction of the cost of using an agency.</i></p>
									<p><i>Tapping into their huge audience base put my role directly in front of senior executives and the results were almost instantaneous!”</i></p>
									<p><b>Brad Shaw</b>, Managing Director, LivePro</p>
				    		</div>
				    	</div>
				    </div>
				    <div class="item">
				    	<div class="col-sm-4 text-center">
			    			<img src="/uploads/images/Jason-Price.jpg" style="width: 100%; max-width:300px; margin: 5px 0;">
				    	</div>
				    	<div class="col-sm-8">
				    		<div>
									<p><i>“By using ItsMyCall I was able to fill a senior role in a fraction of the cost of using an agency.</i></p>
									<p><i>Tapping into their huge audience base put my role directly in front of senior executives and the results were almost instantaneous!”</i></p>
									<p><b>Brad Shaw</b>, Managing Director, LivePro</p>
				    		</div>
				    	</div>
				    </div>
				  </div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="col-sm-12">
					<h2>Great Prices!</h2>
				</div>
				<div class="col-sm-4">
					<img src="/uploads/images/Fotolia_92810408_Subscription_Monthly_M.jpg" style="width: 100%; max-width:300px; margin-top: 5px;">
				</div>
				<div class="col-sm-8">
					<p>Being niche and without the large overheads of the bigger players we are able to offer some great pricing to stretch your recruiting dollar further! With our current launch specials our pricing is:</p>
					<ul>
						<li>Standard Job Ad – $20.65 (normally $59)</li>
						<li>Premium Job Ad – $29.75 (normally $85)</li>
						<li>Featured Job Ad – $38.15 (normally $109)</li>
					</ul>
				</div>
			</div>
		</div>
		<hr>
		<div class="clearfix">
			<div class="col-sm-6">
				<h2>BOOST your Job Ad!</h2>
				<p>With Australia’s largest audience of Contact Centre Professionals wouldn’t it be great if you could put your Job Ad directly in front of them? Well you can with one of our Boosts!!!</p>
				<p>When you place your Job Ad simply choose:</p>
				<ul>
					<li>A LinkedIn Post (to over 5k senior contact centre professionals) is just $35</li>
					<li>A Facebook Post (to over 13k contact centre agents) is just $35</li>
					<li>A Newsletter Post (to over 1k senior contact centre professionals) is just $12.50</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<img src="/uploads/images/Fotolia_120482961_Subscription_Monthly_M.jpg" style="width: 100%; margin-top: 20px;">
			</div>
		</div>
		<hr>
		<div class="clearfix">
			<div class="col-sm-6 pull-right">
				<h2>Target your exact role</h2>
				<p>The call centre of today is more complex than ever. We understand the diversity and nuances of the different skills and jobs that are required to be successful and that’s why we’ve ensured we’ve got the key categories you need to target the exact skills you require saving both you, and the applicant time For example:</p>
				<ul>
					<li><b>Agents</b> – Inbound, outbound, sales, customer service, collections and more</li>
					<li><b>Workforce Optimisation</b> – forecasters, planners, schedulers and more</li>
					<li><b>ICT</b> – Dialler, Network and CRM Administrators, desktop support and more</li>
					<li><b>Specialist Roles</b> including reporting, analysts, L&D, coaches, quality and more</li>
					<li><b>Team Leader</b>, Management and Executive roles</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<img src="/uploads/images/dartboard_target_bullseye_Fotolia_95573575_Subscription_Monthly_M-1.jpg" style="width: 100%; margin-top: 20px;">
			</div>
		</div>
		<hr>
		<div class="clearfix">
			<div class="Advertise">
				<div class="Advertise__text">
					<h4>Advertise your first role in minutes!</h4>
					<p>It takes just seconds to open an account and you’ll have your first job advertised in just minutes.  We’ve made it super easy to advertise a job, purchase Boosts or even purchase one of our Value Packs to save even more if you advertise jobs frequently.</p>
				</div>
				<div class="Advertise__button text-center">
					<a class="btn btn-primary" href="https://itsmycall.com.au/register-poster" style="height: auto;">Advertise Job Now</a>
				</div>
			</div>
			<hr>
			<div class="Contact" style="margin-bottom: 20px;"">
				<div class="Contact__text">
					<h4>Still got more questions?</h4>
					<p>If you’d like to learn more about advertising jobs on ItsMyCall our Australian based support team are here to help:</p>
					<ul>
						<li>Contact us on 1300 ITSMYCALL (1300 487 692) or via our live chat service between 8.30am and 5.30pm AEST Monday to Friday.</li>
						<li>Download our free Job Advertisers Guide that contains all our key information to get your started.</li>
						<li>Visit the <a href="https://itsmycall.zendesk.com/hc/en-us">ItsMyCall Support Centre</a> with all of our Frequently Asked Questions, How to Guides and more.</li>
					</ul>
				</div>
				<div class="Contact__button text-center">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height: auto;">
						Download Job Advertisers Guide
					</button>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title text-center" id="myModalLabel">Download our free Job Advertisers Guide!</h4>
		      </div>
		      <div class="modal-body">
		        <form class="Download">
						  <div class="form-group">
						  	<label for="first-name">First name:</label>
						  	<input type="text" class="form-control" id="first-name" name="first-name">
						  </div>
						  <div class="form-group">
						    <label for="last-name">Last name:</label>
						    <input type="text" class="form-control" id="last-name" name="last-name">
						  </div>
						  <div class="form-group">
						    <label for="company">Company:</label>
						    <input type="text" class="form-control" id="company" name="company">
						  </div>
						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" class="form-control" id="email" name="email">
						  </div>
						  <div class="form-group">
						    <label for="phone-number">Phone number:</label>
						    <input type="text" class="form-control" id="phone-number" name="phone-number">
						  </div>
						  <button type="submit" class="btn btn-primary">Download</button>
						</form>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
@endsection
