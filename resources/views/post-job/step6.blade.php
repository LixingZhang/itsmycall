<style>
.job_preview .list-group-item {
    position: relative;
    display: block;
    padding: 10px 15px;
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid #ddd;
}
.job_preview #affix-box.affix {
    position: inherit !important;
    width: 100%;
}
.job_preview .box-item-details {
    padding-top: 0px;
}
.job_preview iframe {
    width: 100% !important;
    height: 300px !important;
}
.list-group-itemuu {
    margin-bottom: 0px;
    border-bottom: 1px solid #ddd;
    position: relative;
    display: block;
    padding: 10px 15px;
    background-color: #fff;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/nivo-slider.min.css" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/jquery.nivo.slider.min.js" type="text/javascript"></script>
<script>
$(document).ready(function () {
    $('ul.pager li.next a, .a-tab6').on('click', function () {
        var id = $('.tab-pane.active').first().attr('id'); // id of 'old' tab. Example: tab2
            $preview_img = $("input#featured_image_use").attr("value");
            $preview_title = $("input#title").val();
            $preview_short_description = $("#selling4").val();
            $preview_selling1 = $("#selling1").val();
            $preview_selling2 = $("#selling2").val();
            $preview_selling3 = $("#selling3").val();
            $preview_category = $("#category").find("option:selected").text();
            $preview_subcategory = $("#sub_category").find("option:selected").text();
            $preview_company = $("#company").val();
            $preview_joblocation = $("#joblocation").val();
            $preview_hrsalary = $("#hrsalary").val();
            $preview_salary = $("#salary").val();
            $preview_showsalary = $("#showsalary1").val();
            $preview_pay_cycle = $("#pay_cycle1").find("option:selected").text();
            $preview_emptype = $("#emptype").find("option:selected").text();
            $preview_empterm = $("#empterm").find("option:selected").text();
            $preview_incentive = $("#incentivestructure1").find("option:selected").text();
            $preview_vid = $("#video_link").val();
            $preview_jobdesc = $("#summernote").val();
            $preview_salary_type = $("#salary_type1").val();

            $(".job_preview .img-item img, .box-item-details .img-responsive").attr("src", $preview_img);
            $(".job_preview .job-title").text($preview_title);
            $(".job_preview .short_description").text($preview_short_description);
            $(".job_preview .selling1").text($preview_selling1);
            $(".job_preview .selling2").text($preview_selling2);
            $(".job_preview .selling3").text($preview_selling3);
            $(".job_preview .company").text($preview_company);
            $(".job_preview .location").text($preview_joblocation);
            $(".job_preview .paycycle").text($preview_pay_cycle);
            $(".job_preview .em_type").text($preview_emptype);
            $(".job_preview .em_term").text($preview_empterm);
            $(".job_preview .incentive").text($preview_incentive);
            $(".job_preview .main-category").text($preview_category);
            $(".job_preview .sub-category").text($preview_subcategory); 
            $(".job_preview .jobdesc").html($preview_jobdesc);

            if ($preview_selling1.length < 1) {
                $(".job_preview .selling1").hide();
            } else {
                $(".job_preview .selling1").show();
            }
            if ($preview_selling2.length < 1) {
                $(".job_preview .selling2").hide();
            } else {
                $(".job_preview .selling2").show();
            }
            if ($preview_selling3.length < 1) {
                $(".job_preview .selling3").hide();
            } else {
                $(".job_preview .selling3").show();
            }
            if ($("#wheretosend").val() == 1) {
               $(".applicants_out").removeClass('hidden');
            } else {
                $(".applicants_out").addClass('hidden');
            }

            if($preview_showsalary == 1 && ($preview_hrsalary.length > 0 || $preview_salary.length > 0)) {
                if ($preview_salary_type == "hourly") {
                    $(".salary_detail").html("<span class='color-black'>Hourly Rate: <span style='color: grey'>$" + $preview_hrsalary + "</span></span>");
                } else {
                    $(".salary_detail").html("<span class='color-black'>Annual Salary: <span style='color: grey'>$" + $preview_salary + "</span></span>");
                }
            } else {
                $(".salary_detail").html("<span class='color-black'>Salary Details: <span style='color: grey'>Not Specified</span></span>");
            }
            
            if(featuredproduct == false) {
                $(".featured-job").hide();
                $(".job-item").removeClass("featured");
            } else {
                $(".featured-job").show();
                $(".job-item").addClass("featured");
            };
            var id = $('.tab-pane.active').first().attr('id'); // id of 'old' tab.
            if(id == 'tab5' && $preview_vid.length > 10) {
                url = $preview_vid;
                $youtubecheck = url.match(/youtube/gi);
                $vimeocheck = url.match(/vimeo/gi);
                if ($youtubecheck !== null) {
                    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
                    if(videoid !== null) {
                       jQuery(".embed").html('<iframe height="300px" src="https://www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe');
                       $('.embed').css('display', '');
                    }
                } else if ($vimeocheck !== null) {
                    var videoid = url.match(/https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/);
                    if(videoid !== null) {
                       jQuery(".embed").html('<iframe height="300px" src="https://player.vimeo.com/video/' + videoid[3] + '" frameborder="0" allowfullscreen></iframe');
                       $('.embed').css('display', '');
                    }
                };
            } else {
                $(".embed").hide();
            }

            if (id == 'tab5' && featuredproduct == true && $preview_vid.length > 10 && $("#multipleimg").val() != '') {
                if($("#multipleimg").size() > 0){
                    $preview_multiimg = $("#multipleimg").val();
                    $("#slider-preview").empty(); 
                    $("#slider-preview2").empty(); 
                    $("#slider-preview2").prepend('<div class="nivoSlider-2"></div>');
                    $imgarr = $preview_multiimg.split(",");
                    $imgarr.forEach(function(entry) {
                        if(entry.length > 10){
                        $(".nivoSlider-2").prepend('<img src="' + entry + '" style="width:100%">' );
                        }
                    });
                    $('.nivoSlider-2').nivoSlider({
                      effect:'fade',
                      directionNav: true,
                      controlNav: false,
                      prevText: '<',
                      nextText: '>',
                      pauseOnHover: false,
                      pauseTime: 5000,
                    }); 
                }
            }
               if (id == 'tab5' && featuredproduct == true && $preview_vid.length < 10 && $("#multipleimg").val() != '') {
                if($("#multipleimg").size() > 0){
                    $preview_multiimg = $("#multipleimg").val();
                    $("#slider-preview").empty(); 
                    $("#slider-preview2").empty(); 
                    $("#slider-preview").prepend('<div class="nivoSlider-3"></div>');
                    $imgarr = $preview_multiimg.split(",");
                    $imgarr.forEach(function(entry) {
                        if(entry.length > 10){
                        $(".nivoSlider-3").prepend('<img src="' + entry + '" style="width:100%">' );
                        }
                    });
                    $('.nivoSlider-3').nivoSlider({
                      effect:'fade',
                      directionNav: true,
                      controlNav: false,
                      prevText: '<',
                      nextText: '>',
                      pauseOnHover: false,
                      pauseTime: 5000,
                    }); 
                }
            } 

    });
});
</script>
    <style type="text/css">
        .helo{
            background-color: #E5FFEC;
            padding-top: 15px;
            border: 2px solid;
        }
        .featured-img{
            width: 100px;
            margin-bottom: 10px;
        }
        .col-centered{
            float: none;
            margin: 0 auto;
            }
        .featured-heading{
            color: #1B6008;
            margin: 0;
            padding-top: 10px;
        }
        .job-list-label{
            color: #66A866;
        }
        .job-list-text{
            font-size: 14px;
        }
        .list-group-item{
            border-radius: 0px !important; 
        }
        .standard{
           min-height: 160px !important;
        }
        @media only screen and (max-width: 990px) {
            .standard{
            min-height: initial !important;
            }
            .standardline{
            position: initial !important;
            }
        }
}
    </style>
<?php 
$business_info = \App\Settings::where('column_key','business_info')->first();
$home_info = \App\Settings::where('column_key','home_info')->first();
$agreement_info = \App\Settings::where('column_key','agreement_info')->first();


$standard_info = \App\Settings::where('column_key','standard_info')->first();
$exten_info = \App\Settings::where('column_key','exten_info')->first();
$parent_info = \App\Settings::where('column_key','parent_info')->first();
$after_info = \App\Settings::where('column_key','after_info')->first();
$night_info = \App\Settings::where('column_key','night_info')->first();
$rotno_info = \App\Settings::where('column_key','rotno_info')->first();
$rotyes_info = \App\Settings::where('column_key','rotyes_info')->first();

$business_info_img = \App\Settings::where('column_key','business_info_img')->first();
$home_info_img = \App\Settings::where('column_key','home_info_img')->first();
$agreement_info_img = \App\Settings::where('column_key','agreement_info_img')->first();

$standard_info_img = \App\Settings::where('column_key','standard_info_img')->first();
$exten_info_img = \App\Settings::where('column_key','exten_info_img')->first();
$parent_info_img = \App\Settings::where('column_key','parent_info_img')->first();
$after_info_img = \App\Settings::where('column_key','after_info_img')->first();
$night_info_img = \App\Settings::where('column_key','night_info_img')->first();
$rotno_info_img = \App\Settings::where('column_key','rotno_info_img')->first();
$rotyes_info_img = \App\Settings::where('column_key','rotyes_info_img')->first();

?>
<div class="row job_preview">
<div class="col-md-12">
    <h3 style="color: white;
    font-size: 21px;
    background: #639941;
    padding-left: 20px;
    margin-bottom: 50px;
    padding-top: 13px;
    padding-right: 20px;
    padding-bottom: 13px;
    min-height: 50px;
    margin-top: 30px;"><strong style="font-weight: 200;">This is how your job will appear in the search:</strong> <p class="valuego" style="display: none;"></p></h3>
    <div class="box-list">
    <div class="row helo display1" style="margin:0; display: none;">
                        <div class="col-md-4 col-md-push-8 ">
                                    <img class="featured-img" src="/featured.png" style="width: 140px;">
                                    <small style="color: grey; float: right; padding-right: 15px; padding-top: 2px;">Posted 1 minute ago</small>
                                </div>
                            <div class="col-md-8 col-md-pull-4">

                                <a href="#" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading job-title"></a>


                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}">
                                <img class="homeif1 homenone" src="{{$business_info_img->value_string}}" style="padding-left: 7px; width: 35px; margin-top: -15px; display: none;"> 
                                </span>
                                <div class="tooltip_templates">
                                    <span id="tooltip_content{!!$business_info->id!!}">
                                        {!!$business_info->value_txt!!}
                                    </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}">
                                <img class="homeif2 homenone" src="{{$agreement_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}">
                                <img class="homeif3 homenone" src="{{$home_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}">
                                <img class="parentif1 parentnone" src="{{$standard_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}">
                                <img class="parentif2 parentnone" src="{{$exten_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}">
                                <img class="parentif3 parentnone" src="{{$parent_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}">
                                <img class="parentif4 parentnone" src="{{$after_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}">
                                <img class="parentif5 parentnone" src="{{$night_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}">
                                <img class="parentif6 parentnone" src="{{$rotno_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}">
                                <img class="parentif7 parentnone" src="{{$rotyes_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                <br>
                                <a class="company" href="#" style="color: grey; font-size: 20px; font-weight: 500"></a>
                                </div>


                                <div class="col-md-8">
                                <p class="short_description" style="padding-top:20px; padding-bottom: 20px; font-size: 18px; margin: 0;word-wrap: break-word; "></p>
                                    <li class="selling1" style="font-size: 16px;"></li> 
                                    <li class="selling2" style="font-size: 16px;"></li> 
                                    <li class="selling3" style="font-size: 16px;"></li> 
                                     <br><br>

                                <small style="color: #777;"><span class="main-category"></span> / <span class="sub-category"></span></small> <br>
                                <div class="featured-job">
                                <div class="img-item">
                                <img class="featured-logo" style="max-width: 250px; max-height: 100px; margin-bottom: 20px; margin-top: 10px;" src="">
                                </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <ul class="list-group" style="max-width: 300px; border: 2px solid; box-shadow: 8px 9px 7px #888888; padding-bottom: 10px; padding-top: 10px; background-color: white;" >
                                    <li class="list-group-item" style="border:0; padding:5px 15px; font-size: 15px; color: black;">Location: <a class="location" href="#"></a></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span class="em_type" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Employment Status: <span class="em_term" style="color: grey"></span></span></li>
                                    <li class="list-group-item salary_detail" style="border:0;padding:5px 15px; font-size: 15px;"></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Pay Frequency: <span class="paycycle" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Advertiser: <span style="color: grey"> @if(Auth::user()->advertiser_type == 'private_advertiser') Private Advertiser @endif @if(Auth::user()->advertiser_type == 'recruitment_agency') Recruitment Agency @endif </span></span></li>
                                    <li class="list-group-item" style="border:0;"><a href="#" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                </ul>                            
                            </div>
                        
                    </div>
        <div class="row helo display2" style="margin:0; display: none; border: 4px solid #078d00 !important; background-color: white !important;">
                        <div class="col-md-4 col-md-push-8 ">
                                    <small style="color: grey; float: right; padding-right: 15px;">Posted 1 minute ago</small>
                                </div>
                            <div class="col-md-8 col-md-pull-4">

                                <a href="#" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading job-title"></a>
                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}">
                                <img class="homeif1 homenone" src="{{$business_info_img->value_string}}" style="padding-left: 7px; width: 35px; margin-top: -15px; display: none;"> 
                                </span>
                                <div class="tooltip_templates">
                                    <span id="tooltip_content{!!$business_info->id!!}">
                                        {!!$business_info->value_txt!!}
                                    </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}">
                                <img class="homeif2 homenone" src="{{$agreement_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}">
                                <img class="homeif3 homenone" src="{{$home_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}">
                                <img class="parentif1 parentnone" src="{{$standard_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}">
                                <img class="parentif2 parentnone" src="{{$exten_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}">
                                <img class="parentif3 parentnone" src="{{$parent_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}">
                                <img class="parentif4 parentnone" src="{{$after_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}">
                                <img class="parentif5 parentnone" src="{{$night_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}">
                                <img class="parentif6 parentnone" src="{{$rotno_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}">
                                <img class="parentif7 parentnone" src="{{$rotyes_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                <br>
                                <a class="company" href="#" style="color: grey; font-size: 20px; font-weight: 500"></a>
                                </div>


                                <div class="col-md-8">
                                <p class="short_description" style="padding-top:10px; padding-bottom: 10px;font-size: 17px; margin: 0;word-wrap: break-word; "></p>
                                    <li class="selling1" style="font-size: 15px;"></li> 
                                    <li class="selling2" style="font-size: 15px;"></li> 
                                    <li class="selling3" style="font-size: 15px;"></li> 
                                <br><br>
                                <small style="color: #777;"><span class="main-category"></span> / <span class="sub-category"></span></small> <br>
                                <div class="img-item">
                                <img class="featured-logo" style="max-width: 200px; max-height: 70px; margin-bottom: 20px; margin-top: 10px; display: initial !important;" src="">
                                </div>

                            </div>
                            <div class="col-md-4">
                                <ul class="list-group" style="max-width: 300px; padding-bottom: 5px; padding-top: 5px; background-color: white;" >
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px; color: black;">Location: <a class="location" href="#"></a></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span class="em_type" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Employment Status: <span class="em_term" style="color: grey"></span></span></li>
                                    <li class="list-group-item salary_detail" style="border:0;padding:5px 15px; font-size: 15px;"></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Pay Frequency: <span class="paycycle" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Advertiser: <span style="color: grey"> @if(Auth::user()->advertiser_type == 'private_advertiser') Private Advertiser @endif @if(Auth::user()->advertiser_type == 'recruitment_agency') Recruitment Agency @endif </span></span></li>
                                    <li class="list-group-item" style="border:0;"><a href="#" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                </ul>                            
                            </div>
                        
                    </div>
                    <div class="row helo display3" style="display: none; margin:0; background-color: white !important;  border: 1px solid lightgrey !important;">
                        <div class="col-md-4 col-md-push-8 ">
                                    <small style="color: grey; float: right; padding-right: 15px;">Posted 1 minute ago</small>
                                </div>
                            <div class="col-md-8 col-md-pull-4 ">

                                <a href="#" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading job-title"></a>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}">
                                <img class="homeif1 homenone" src="{{$business_info_img->value_string}}" style="padding-left: 7px; width: 35px; margin-top: -15px; display: none;"> 
                                </span>
                                <div class="tooltip_templates">
                                    <span id="tooltip_content{!!$business_info->id!!}">
                                        {!!$business_info->value_txt!!}
                                    </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}">
                                <img class="homeif2 homenone" src="{{$agreement_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}">
                                <img class="homeif3 homenone" src="{{$home_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}">
                                <img class="parentif1 parentnone" src="{{$standard_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}">
                                <img class="parentif2 parentnone" src="{{$exten_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}">
                                <img class="parentif3 parentnone" src="{{$parent_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}">
                                <img class="parentif4 parentnone" src="{{$after_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}">
                                <img class="parentif5 parentnone" src="{{$night_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}">
                                <img class="parentif6 parentnone" src="{{$rotno_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}">
                                <img class="parentif7 parentnone" src="{{$rotyes_info_img->value_string}}" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                <br>
                                <a class="company" href="#" style="color: grey; font-size: 20px; font-weight: 500"></a>
                                </div>


                                <div class="col-md-8 standard">
                                <p class="short_description" style="padding-top:5px; margin: 0; word-wrap: break-word;"></p><br><br><br><br>
                                <div class="standardline" style="color: #777; font-size: 85%; position: absolute; bottom: 10px;"><span class="main-category"></span> / <span class="sub-category"></span></div> <br>
                            </div>
                            <div class="col-md-4">
                                <ul class="list-group" style="max-width: 300px; padding-bottom: 0px; padding-top: 0px; background-color: white;" >
                                    <li class="list-group-item" style="border:0;padding:0px 15px; font-size: 15px; color: black;">Location: <a class="location" href="#"></a></li>
                                    <li class="list-group-item" style="border:0;padding:0px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span class="em_type" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:0px 15px; font-size: 15px;"><span class="color-black">Employment Status: <span class="em_term" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding-top: 3px;"><a href="#" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                </ul>                            
                            </div>
                        
                    </div>
    </div>
</div>

<div class="col-md-12">
<div class="row">
<div class="col-md-12">
<hr>
<h3 style="color: white;
    font-size: 21px;
    background: #639941;
    padding-left: 20px;
    padding-right: 20px;
    margin-bottom: 50px;
    padding-top: 13px;
    padding-bottom: 13px;
    min-height: 50px;
    margin-top: 50px;"><strong style="font-weight: 200;">This is how your job ad will appear when the candidate clicks the 'View More' button:</strong>
</h3>
</div>
<div class="col-md-10">
    <!-- box item details -->
    <div class="block-section box-item-details" >
        <div class="row title" style="margin-top: 0px !important; padding-top: 20px !important; border-bottom: 1px solid #e1e1e1; border-top: 1px solid #e1e1e1;">
            <div class="col-md-4" style="text-align: -webkit-center; padding-bottom: 20px;">
                <img style="width: 250px; margin-left: auto; margin-right: auto;" src="" class="img-responsive" alt="">
            </div>
            <div class="col-md-8 text-center">
                <h1 class="job-title" style="color: #639941; margin-top: 0px; font-size: 32px;"></h1>
                <h2 class="company" style="color: #666666; margin-top: 0; font-size: 28px;"></h2>
                <span style="color: grey"><span class="main-category"></span> / <span class="sub-category"></span></span>     
            </div>
        </div>
        <div class="row" style="padding-top: 25px;">

            <div class="col-md-4">
                <div class="block-section"  style="padding:10px 0!important;" >
                    <div class="text-center" style="padding-bottom: 10px">
                        <ul class="list-group" style="border: 2px solid; background-color: white;">
                            <li class="list-group-itemuu"><span class="color-black">Company: </span> <a class="company"></a></li> 
                            <li class="list-group-itemuu"><span class="color-black">Location: </span> <a class="location"></a></li> 
                            <li class="list-group-itemuu salary_detail"></li>
                            <li class="list-group-itemuu"><span class="color-black">Pay Frequency: <span class="paycycle" style="color: grey"></span></span></li>
                            <li class="list-group-itemuu"><span class="color-black">Employment Hours: <span style="color: grey" class="em_type"></span></span></li> 
                            <li class="list-group-itemuu"><span class="color-black">Employment Status: <span style="color: grey" class="em_term"></span></span></li>
                            <li class="list-group-itemuu"><span class="color-black">Advertiser: <span style="color: grey"> @if(Auth::user()->advertiser_type == 'private_advertiser') Private Advertiser @endif @if(Auth::user()->advertiser_type == 'recruitment_agency') Recruitment Agency @endif </span></span></li>
                            <li class="list-group-itemuu"><span class="color-black">Incentive Structure: <span style="border:0; color: grey" class="incentive"></span></span></li> 
                            <li class="list-group-itemuu" style="border:0;">
                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}">
                                <img class="homeif1 homenone" src="{{$business_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;"> 
                                </span>
                                <div class="tooltip_templates">
                                    <span id="tooltip_content{!!$business_info->id!!}">
                                        {!!$business_info->value_txt!!}
                                    </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}">
                                <img class="homeif2 homenone" src="{{$agreement_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}">
                                <img class="homeif3 homenone" src="{{$home_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}">
                                <img class="parentif1 parentnone" src="{{$standard_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}">
                                <img class="parentif2 parentnone" src="{{$exten_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}">
                                <img class="parentif3 parentnone" src="{{$parent_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}">
                                <img class="parentif4 parentnone" src="{{$after_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}">
                                <img class="parentif5 parentnone" src="{{$night_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}">
                                <img class="parentif6 parentnone" src="{{$rotno_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>

                                <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}">
                                <img class="parentif7 parentnone" src="{{$rotyes_info_img->value_string}}" style="padding-left: 4px; width: 35px;  display: none;">
                                </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                            </li>
                        </ul>
                    <p><a  target="_blank" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 180px; background: #ff7200;">Apply Now</a></p>
                    <p class="applicants_out hidden" style="line-height: 1.2em !important; font-size: 12px;">* You will be taken to the Job Advertisers website to submit your application.</p>
                    </div>
                </div><!-- box affix right -->
            </div>	

            <div class="col-md-8">
                <ul>
                    <li class="selling1" style="font-size: 18px;"></li> 
                    <li class="selling2" style="font-size: 18px;"></li> 
                    <li class="selling3" style="font-size: 18px;"></li> 
                </ul>
                <hr>
                <div class="embed"> </div>
                 <div class="col-md-12" style="margin-top: 15px; margin-bottom: 15px;">
                    <div id="slider-preview"></div>    
                 </div>
                <div class="jobdesc" style="word-wrap: break-word;">
                </div>
                 <div class="col-md-12" style="margin-top: 20px;">
                    <div id="slider-preview2"></div>    
                 </div>
                 <p style="text-align: center;"><a href="#" class="btn btn-theme btn-t-primary btn-block-xs" style="margin-top: 20px; min-width: 300px; background: #ff7200;">Apply Now</a></p>
                 <div class="applicants_out hidden col-sm-6 col-centered" style="font-size: 12px; line-height: 1.2em; padding:0; text-align: left;">*
                    You will be taken to the Job Advertisers website to submit your application.
                </div>
            </div>
        </div>
    </div><!-- end box item details -->

</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="col-md-10">
    <h3 style="color: grey; padding-left: 15px; padding-right: 15px; font-size: 20px;">&#x1f44d; - Happy with how your job looks? Just click next and complete the checkout process</h3>
    <h3 class="go_back" style="color: grey; padding-left: 15px; padding-right: 15px; font-size: 20px;">&#x1f44e; - Need to make some changes? Go directly back to <a  class="go_jobdetail">Job Details page</a> or <a class="go_joblevel">Choose Level page</a></h3>
</div>
</div>
</div>

</div>