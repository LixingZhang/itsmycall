<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/nivo-slider.min.css" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.4/js/jquery.swipebox.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.swipebox/1.4.4/css/swipebox.min.css">
<style type="text/css">
  .nivo-directionNav a {
    font-size: 30px;
    color: white;
    padding: 0 5px;
    margin: -6px 0;
}
.nivo-directionNav a:hover {
  text-decoration: none;
}

</style>
<h2>Select Your Boosts</h2>
{!! $settings->posts_boosts_text !!}
                              <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                     $expires = new \Carbon\Carbon($referralcode->date_end);
                                     $date = $expires->toFormattedDateString();
                                     $now = \Carbon\Carbon::now();
                                     $difference = ($expires->diff($now)->days);
                                    }
                                ?>  
                          @if(!empty($referralcode) && $referralcode->exclusive == 'no' && $referralcode->date_end >= $end)
                            <input type="hidden" class="codeallow" name="" value="yes">
                          @elseif(!empty($referralcode) && $referralcode->exclusive == 'partial' && $referralcode->date_end >= $end)
                            <input type="hidden" class="codeallow" name="" value="yes">
                          @else
                            <input type="hidden" class="codeallow" name="" value="no">
                          @endif
                          @if(!empty($referralcode) && $referralcode->date_end >= $end)
                             <input type="hidden" class="ref_apply" name="" value="yes">
                          @else
                             <input type="hidden" class="ref_apply" name="" value="no">
                          @endif

<div class="box-list" id="step4_box">
<div class="outerbox clearfix">
  @foreach($upgrades as $upgrade)
  @if($upgrade->status != 0)
  <?php
     $actualprice = App\Upgrades::where('id',$upgrade->id)->first(); 
     ?>
  <div class="item">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-3">
            <input type="hidden" id="nocoupon" class="price" value="{{number_format($upgrade->price, 2, '.', '')}}">
            <input type="hidden" id="coupongo" class="actualprice" value="{{number_format($actualprice->actual_price, 2, '.', '')}}">
          <?php $upgradeoff = App\Upgrades::where('id', $upgrade->id)->get()->first();            
            ?>
          <input type="hidden" class="price_stan_up" value="{{$upgradeoff->price}}">
          <h3 class="add-upgrade-title">{{$upgrade->name}}</h3>
                <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                    }
                                ?>  
                            @if($referralcode)                                 
                                @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                                <?php 
                                    $discountupgrade = 0;
                                ?>
                                <h4 style="color: #ff7200;">
                                  ${{number_format($upgrade->price-$discountupgrade, 2, '.', '')}} {{$upgrade->type}} </h4>
                                <h4 style="color: #ff7200;" class="add-upgrade-price hidden">
                                  ${{number_format($upgrade->price, 2, '.', '')}} {{$upgrade->type}} </h4>
                                @else 
                                <h4 style="color: #ff7200;" class="add-upgrade-price">
                                  ${{number_format($upgrade->price, 2, '.', '')}} {{$upgrade->type}} </h4>
                                @endif
                            @else
                            <h4 style="color: #ff7200;" class="add-upgrade-price">
                              ${{number_format($upgrade->price, 2, '.', '')}} {{$upgrade->type}} </h4>
                            @endif
          <div id="slider" class="nivoSlider">     
              @foreach($images as $photo)
                @if($photo->upgrade_id == $upgrade->id)
                      <a rel="gallery-{{$upgrade->id}}" title="{{$upgrade->name}} Example" href="{{$photo->image}}" class="swipebox">
                        <img src="{{$photo->image}}" alt="image">
                      </a>
                @endif
              @endforeach
          </div>
        </div>
        <div class="col-md-6 add-upgrade-description">
          {!! $upgrade->description !!}
        </div>
        <div class="col-md-3" style="font-size: 20px; font-weight:400;margin-top: 15px; color:#078d00">
          <div class="standout">
            <input id="box_{{$upgrade->id}}" data-id="{{$upgrade->id}}" data-price="{{number_format($upgrade->price, 2, '.', '')}}" class="additional-upgrade helo" name="additional-upgrade" type="checkbox" value="box{{$upgrade->id}}"/>
            <label for="box_{{$upgrade->id}}">Select Boost</label>
          </div>
          <br/>
          @if($upgrade->id == 1)
          <select id="count_{{$upgrade->id}}" data-id="{{$upgrade->id}}" data-price="{{number_format($upgrade->price, 2, '.', '')}}" class="additional-upgrade-count" >
            @for ($i = 1; $i <= $upgrade->boost_number; $i++)
                @if($i == 1) 
                <option value="{{$i}}" selected >{{$i}} Edition</option>
                @else
                <option value="{{$i}}" >{{$i}} Editions</option>
                @endif
            @endfor
          </select>
          @else
          <select id="count_{{$upgrade->id}}" data-id="{{$upgrade->id}}" data-price="{{number_format($upgrade->price, 2, '.', '')}}" class="additional-upgrade-count">
            @for ($i = 1; $i <= $upgrade->boost_number; $i++)
                @if($i == 1) 
                <option value="{{$i}}" selected >{{$i}} Post</option>
                @else
                <option value="{{$i}}" >{{$i}} Posts</option>
                @endif
            @endfor
          </select>
          @endif
        </div>
      </div>
    </div>
  </div><!-- end item list -->
  @endif
  @endforeach
  <p class="valuego" style="display: none;"></p>
</div><!-- end item list -->
</div><!-- end item list -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/jquery.nivo.slider.min.js" type="text/javascript"></script>
<script type="text/javascript"> 
$(window).on('load', function() {
    $('.swipebox').swipebox();
    $('.nivoSlider').nivoSlider({
      effect:'fade',
      directionNav: true,
      controlNav: false,
      prevText: '<',
      nextText: '>',
      pauseOnHover: false,
      pauseTime: 5000,
    }); 
}); 
</script>
<script type="text/javascript">
  $(".helo").on( 'click', function (e) {
      var helo = $('.valuego').text();
      var go = helo.substr(0,4);
      if(go == 'Stan'){
        e.preventDefault();
        swal({
              html: "Boosts are only available with a Featured Job Ad.",
            });
      }
      else if(go == 'Prem'){
        e.preventDefault();
        swal({
              html: "Boosts are only available with a Featured Job Ad.",
            });
      }
      else{
        return true;
      }
  });


</script>
