<script>
$(document).ready(function () {
    $('ul.pager li.next a, .a-tab3').on('click', function () {
        var id = $('.tab-pane.active').first().attr('id'); // id of 'old' tab. Example: tab2
            $preview_img = $("input#featured_image_use").attr("value");
            $preview_title = $("input#title").val();
            $preview_short_description = $("#selling4").val();
            $preview_selling1 = $("#selling1").val();
            $preview_selling2 = $("#selling2").val();
            $preview_selling3 = $("#selling3").val();
            $preview_company = $("#company").val();
            $preview_joblocation = $("#joblocation").val();
            $preview_hrsalary = $("#hrsalary").val();
            $preview_salary = $("#salary").val();
            $preview_showsalary = $("#showsalary1").val();
            $preview_pay_cycle = $("#pay_cycle1").find("option:selected").text();
            $preview_emptype = $("#emptype").find("option:selected").text();
            $preview_empterm = $("#empterm").find("option:selected").text();
            $preview_incentive = $("#incentivestructure1").find("option:selected").text();
            $preview_jobdesc = $("#description").val();
            $preview_salary_type = $("#salary_type1").val();
            $(".img-responsive").attr("src", $preview_img);
            $(".job-title").text($preview_title);
            $(".selling1").text($preview_selling1);
            $(".selling2").text($preview_selling2);
            $(".selling3").text($preview_selling3);
            $(".company").text($preview_company);
            $(".location").text($preview_joblocation);
            $(".paycycle").text($preview_pay_cycle);
            $(".em_type").text($preview_emptype);
            $(".em_term").text($preview_empterm);
            $(".incentive").text($preview_incentive);
            $(".jobdesc").html($preview_short_description);
            if ($preview_selling1.length < 1) {
                $(".selling1").hide();
            } else {
                $(".selling1").show();
            }
            if ($preview_selling2.length < 1) {
                $(".selling2").hide();
            } else {
                $(".selling2").show();
            }
            if ($preview_selling3.length < 1) {
                $(".selling3").hide();
            } else {
                $(".selling3").show();
            }

            if($preview_showsalary == 1 && ($preview_hrsalary.length > 0 || $preview_salary.length > 0)) {
                if ($preview_salary_type == "hourly") {
                    $(".salary_detail").html("<span class='color-black'>Hourly Rate: <span style='color: grey'>$" + $preview_hrsalary + "</span></span>");
                } else {
                    $(".salary_detail").html("<span class='color-black'>Annual Salary: <span style='color: grey'>$" + $preview_salary + "</span></span>");
                }
            } else {
                $(".salary_detail").html("<span class='color-black'>Salary Details: <span style='color: grey'>Not Specified</span></span>");
            }
            
            var id = $('.tab-pane.active').first().attr('id'); // id of 'old' tab.

    });
});
</script>
    <style type="text/css">
        .featuredjob{
            background-color: #E5FFEC !important;
            border: 2px solid !important;
            border-radius: 0px;
        }
        .premiumjob{
            background-color: white !important;
            border: 4px solid #078d00 !important;
            border-radius: 0px;
        }
        .standardjob{
            background-color: white !important;
            border: 1px solid lightgrey !important;
            border-radius: 0px;
        }
        .featured-img{
            width: 100px;
            margin-bottom: 10px;
        }
        .featured-heading{
            color: #1B6008;
            margin: 0;
            padding-top: 10px;
        }
        .job-list-label{
            color: #66A866;
        }
        .job-list-text{
            font-size: 14px;
        }
        .list-group-item{
            border-radius: 0px !important; 
        }
        .standard1{
           min-height: 380px !important;
        }
        .standard2{
           min-height: 320px !important;
        }
        @media only screen and (max-width: 990px) {
            .standard1{
            min-height: initial !important;
            }
            .standard2{
            min-height: initial !important;
            }
            .standardline1 img{
            position: initial !important;
            }
        }
}
    </style>
<h2>Select Job Ad Display Level</h2>
<div class="alert alert-danger hidden">Please select one of the options below.</div>
<p>Please select from either a Featured, Premium or Standard Job Ad. Just tick the orange box for the level you'd like to purchase with a preview below of how your Job Ad will appear in search results.</p>
                            <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                     $expires = new \Carbon\Carbon($referralcode->date_end);
                                     $date = $expires->toFormattedDateString();
                                     $now = \Carbon\Carbon::now();
                                     $difference = ($expires->diff($now)->days);
                                    }
                                ?>  
                            @if($referralcode)                                 
                                @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                                @endif
                            @endif
<div class="box-list" id="step3_box">
  <div class="row">
  @foreach($posts_packages as $package)
    <div class="outerbox clearfix">
      <input type="hidden" class="price price_pc" value="{{number_format($package->price, 2, '.', '')}}">
      <?php $packageoff = App\PostsPackages::where('id', $package->id)->get()->first();            
        ?>
      <input type="hidden" class="price_stan" value="{{$packageoff->price}}">
      <input type="hidden" class="job_price" value="{{$package->price}}">
      <input type="hidden" class="packagename" value="{{$package->title}}">
                        <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                    }
                                ?>  
                            @if($referralcode)                                 
                                @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                                <input type="hidden" class="ref_disc_go" value="{{$referralcode->discount_rate}}" name="">
                                <?php 
                                    $discountpackage = 0;
                                ?>
                                <h3>{{$package->title}} - ${{number_format($package->price-$discountpackage, 2, '.', '')}}</h3>
                                <h3 class="title hidden">{{$package->title}} - ${{number_format($package->price, 2, '.', '')}}</h3>
                                @elseif($referralcode->date_end >= $end && $referralcode->discount_rate == 0)
                                    <input type="hidden" class="ref_disc_go" value="0" name=""> 
                                    <h3 class="title">{{$package->title}} - ${{number_format($package->price, 2, '.', '')}}</h3>
                                @else
                                    <h3 class="title">{{$package->title}} - ${{number_format($package->price, 2, '.', '')}}</h3>
                                @endif
                            @else
                            <h3 class="title">{{$package->title}} - ${{number_format($package->price, 2, '.', '')}}</h3>
                            @endif


      <div style="border-radius: 0px !important;" class="item col-md-9 <?php if($package->id == 1) { echo "featuredjob"; } else if($package->id == 2) { echo "premiumjob"; } else{ echo "standardjob"; } ?>" >
        <div class="row">
           <div class="col-md-12">
            @if($package->id == 1)
            <div class="pull-right-md"><img class="pull-right" style="padding-left: 5px;" src="/featured.png" alt=""></div>
            @endif
              <a href="#" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading job-title"></a> 
              <img class="homeif1 homenone" src="/Work is only at the business address.png" style="padding-left: 7px; width: 35px; margin-top: -15px; display: none;"> 
                                <img class="homeif2 homenone" src="/There is potential to work from home.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="homeif3 homenone" src="/Only display Work From Home Jobs.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif1 parentnone" src="/Standard Business Hours.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif2 parentnone" src="/Extended Business Hours.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif3 parentnone" src="/Parent Friendly.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif4 parentnone" src="/Afternoon Shift.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif5 parentnone" src="/Night Shift.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif6 parentnone" src="/Rotating Shifts no weekends.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
                                <img class="parentif7 parentnone" src="/Rotating shifts - including weekend work.png" style="display: none; padding-left: 4px; width: 38px;margin-top: -15px;">
              <h2 class="company" style="margin-top: 5px; color: grey; font-size: 20px; font-weight: 500">Bitsblue</h2>
            <div class="col-md-8 <?php if($package->id == 1) { echo "standard1"; } else if($package->id == 2) { echo "standard2"; } ?>"" style="padding-left: 0;">
              <p class="jobdesc"></p>
			  <?php if($package->id == 1) { ?> 
                <li class="diplay_sp1 selling1"></li>
                <li class="diplay_sp1 selling2"></li>
                <li class="diplay_sp1 selling3"></li>
              <div class="img-item standardline1"><img src="" style="max-width: 250px; max-height: 100px; position: absolute; bottom: 10px; margin-top: 10px;" class="img-responsive" alt=""></div>
			  <?php } ?>
              <?php if($package->id == 2) { ?> 
                <li class="diplay_sp1 selling1"></li>
                <li class="diplay_sp1 selling2"></li>
                <li class="diplay_sp1 selling3"></li>
              <div class="img-item standardline1"><img src="" style="max-width: 200px; max-height: 70px; position: absolute; bottom: 10px; margin-top: 10px;" class="img-responsive" alt=""></div>
              <?php } ?>
              <!-- <a href="#" class="btn btn-theme btn-xs btn-default">View Job</a> -->
              <br>
            </div>
            @if($package->id == 1)
                  <div class="col-md-4" style="padding-right: 0; padding-left: 0;">
                         <ul class="list-group" style="border: 2px solid; box-shadow: 8px 9px 7px #888888; padding-bottom: 10px; padding-top: 10px; background-color: white;" >
                                    <li class="list-group-item" style="border:0; padding:5px 15px; font-size: 15px; color: black;">Location: <a class="location" href="#"></a></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span class="em_type" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Employment Term: <span class="em_term" style="color: grey"></span></span></li>
                                    <li class="list-group-item salary_detail" style="border:0;padding:5px 15px; font-size: 15px;"></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Pay Frequency: <span class="paycycle" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:5px 15px; font-size: 15px;"><span class="color-black">Advertiser: <span style="color: grey"> @if(Auth::user()->advertiser_type == 'private_advertiser') Private Advertiser @endif @if(Auth::user()->advertiser_type == 'recruitment_agency') Recruitment Agency @endif </span></span></li>
                                    <li class="list-group-item" style="border:0;"><a href="#" class="btn btn-theme btn-default" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                </ul>                            
            </div>
            @endif 
            @if($package->id == 2)
                  <div class="col-md-4" style="padding-right: 0; padding-left: 0;">
                         <ul class="list-group" style="padding-bottom: 5px; padding-top: 5px; background-color: white;" >
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px; color: black;">Location: <a class="location" href="#"></a></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span class="em_type" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Employment Term: <span class="em_term" style="color: grey"></span></span></li>
                                    <li class="list-group-item salary_detail" style="border:0;padding:5px 15px; font-size: 15px;"></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Pay Frequency: <span class="paycycle" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:2px 15px; font-size: 15px;"><span class="color-black">Advertiser: <span style="color: grey"> @if(Auth::user()->advertiser_type == 'private_advertiser') Private Advertiser @endif @if(Auth::user()->advertiser_type == 'recruitment_agency') Recruitment Agency @endif </span></span></li>
                                    <li class="list-group-item" style="border:0;"><a href="#" class="btn btn-theme btn-default" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                </ul>                           
            </div>
            @endif 
            @if($package->id == 3)
                  <div class="col-md-4" style="padding-right: 0; padding-left: 0;">
                                <ul class="list-group" style="padding-bottom: 0px; padding-top: 0px; background-color: white;" >
                                    <li class="list-group-item" style="border:0;padding:0px 15px; font-size: 15px; color: black;">Location: <a class="location" href="#"></a></li>
                                    <li class="list-group-item" style="border:0;padding:0px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span class="em_type" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding:0px 15px; font-size: 15px;"><span class="color-black">Employment Term: <span class="em_term" style="color: grey"></span></span></li>
                                    <li class="list-group-item" style="border:0;padding-top: 3px;"><a href="#" class="btn btn-theme btn-default" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                </ul> 
            </div>
            @endif 
          </div>
        </div>
      </div><!-- end item list -->
      
      <div class="col-md-3" style="color:#078d00;">
        <h2 class="text-center">{{$package->name}}</h2>
        <ul class="package-description">
          {!! $package->description !!}
        </ul>
        <div class="standout">
          <input id="{{$package->title}}" class="wewfiwef" name="posts_packages" data-title="{{$package->title}}" data-price="{{$package->price}}" type="radio" value="{{$package->id}}"/>
                            <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                    }
                                ?>  
                            @if($referralcode)                                 
                                @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                                <?php 
                                    $discountpackage = 0;
                                ?>
                                <label for="{{$package->title}}" style="font-size: 22px;">Select ${{number_format($package->price-$discountpackage, 2, '.', '')}}</label>
                                @elseif($referralcode->date_end >= $end && $referralcode->discount_rate == 0)
                                    <label for="{{$package->title}}" style="font-size: 22px;">Select ${{number_format($package->price, 2, '.', '')}}</label>
                                @else
                                    <label for="{{$package->title}}" style="font-size: 22px;">Select ${{number_format($package->price, 2, '.', '')}}</label>
                                @endif
                            @else
                            <label for="{{$package->title}}" style="font-size: 22px;">Select ${{number_format($package->price, 2, '.', '')}}</label>
                            @endif 
        </div>
      </div>
    </div>
  @endforeach
</div>
</div>