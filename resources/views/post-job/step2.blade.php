<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/basic.min.css" />
<link rel="stylesheet" type="text/css" href="/css/image-picker.css" />
<script>
        $(document).ready(function () {
            $('.tooltips').tooltipster({
                contentCloning: true,
                theme: 'tooltipster-shadow',
                side: 'right',
                maxWidth: '765'
            });
        function deleteclick() {
            $('.image_picker_selector .delete-icon').on('click', function (e) {
                idvalue = $('input[name="featured_image_delete"]').val();
                listnumber = $(this).parent().parent().attr("class");
                lastChar = listnumber[listnumber.length -1];
                console.log(idvalue);
                if(idvalue == "") {
                    idvalue = $(".image-picker option:nth-child(" + lastChar + ")").val();
                } else {
                    idvalue = idvalue + ", " + $(".image-picker option:nth-child(" + lastChar + ")").val();
                }
                if($(".image-picker option:nth-child(" + lastChar + ")").hasClass("dbload") == true) {
                    $('input[name="featured_image_delete"]').val(idvalue);
                };
                $(".image-picker option:nth-child(" + lastChar + ")").remove();
                $(".image-picker").imagepicker();
                $(".image_picker_selector .thumbnail").append("<a class='delete-icon'>X</a>");
                $(".image_picker_selector li").each(function( ) {
                    preindex = $(this).prevAll().length;
                    index = preindex + 1;
                    listclass = "item-" + index;
                    $(this).addClass(listclass);
                });
                deleteclick();
            });
        }

        function removeclick() {
             $('#dropzone-img .dz-remove').on('click', function (e) {
                $refimage = $(this).attr("refimage");
                $uploaded_images = $("#multipleimg").val();
                $replaced_images = $uploaded_images.replace($refimage, "");
                multiimg = multiimg.replace($refimage, "");
                $("#multipleimg").val($replaced_images);
                $("#slider-preview2").empty();
                $("#slider-preview").empty(); 
             });
        }

       
            
        Dropzone.options.dropzoneImg = {
          url: "/poster/uploadfiles",
          acceptedFiles: 'image/jpeg,image/png,image/gif,image/svg',
          uploadMultiple: true,
          addRemoveLinks: true,
          parallelUploads: 1,
          maxFiles: 5,
          dictDefaultMessage: 'Select or drop files to upload', // MB 
            init: function () {
                this.on('addedfile', function (file) {
                    if(file.size > (1024 * 1024 * 2)) // not more than 5mb
                    {
                    this.removeFile(file); // if you want to remove the file or you can add alert or presentation of a message
                    swal("File can not be larger than 2MB");
                    }
                })
                this.on("sending", function(file, xhr, formData) {
                  // Will send the filesize along with the file as POST data.
                  formData.append("_token", "{{ csrf_token() }}");
                });
                this.on('success', function (file, res) {
                    console.log('upload success...')
                    console.log(res)
                    console.log(file)
                    if($('input[name="multipleimg"]').val() == "") {
                        multiimg = res;
                    } else {
                        multiimg = multiimg + ", " + res;
                    }
                    console.log(multiimg);
                    $div_number = $('#dropzone-img .dz-preview').length + 2;
                    $('input[name="multipleimg"]').val(multiimg);
                    $('#dropzone-img .dz-preview:nth-child(' + $div_number + ') .dz-remove').attr('refimage', res);
                    removeclick();
                })
            }
        };
        var dropzoneImg = new Dropzone("#dropzone-img");
        //Dropzone.js Options - Upload an image via AJAX.
        Dropzone.options.myDropzone = {
            acceptedFiles: 'image/jpeg,image/png,image/gif,image/svg',
            uploadMultiple: false,
            // previewTemplate: '',
            addRemoveLinks: false,
            // maxFiles: 1,
            dictDefaultMessage: '',
            init: function () {
                this.on('addedfile', function (file) {
                    if(file.size > (1024 * 1024 * 2)) // not more than 5mb
                    {
                    this.removeFile(file); // if you want to remove the file or you can add alert or presentation of a message
                    swal({
                        html: "File can not be larger than 2MB",                        
                    })
                    }else{
                       $( ".spin" ).removeClass('hidden');
                       $( ".info" ).addClass('hidden');
                    }
                })
                this.on('thumbnail', function (file, dataUrl) {
                    // console.log('thumbnail...');
                    $('.dz-image-preview').hide()
                    $('.dz-file-preview').hide()
                })
                this.on('success', function (file, res) {
                    console.log('upload success...')
                    console.log(res)
                    console.log(file)
                    $( ".info" ).removeClass('hidden');
                    setTimeout(function () { 
                        $( ".info" ).addClass('hidden');
                     }, 8000);
                    $( ".spin" ).addClass('hidden');
                    var val = Math.floor(1000 + Math.random() * 9000);
                    $('.image-picker').append("<option data-img-src='" + res + "' value='" + val + "'></option>")
                    $(".image-picker").imagepicker();
                    $('input[name="featured_image"]').val(res)
                    $('input[name="featured_image_use"]').val(res)
                    $('.image_picker_selector img').on('click', function (e) {
                        logosrc = $(this).attr("src");
                        $('input[name="featured_image_use"]').val(logosrc)
                    });
                    $(".image_picker_selector .thumbnail").append("<a class='delete-icon'>X</a>");
                    $(".image_picker_selector li").each(function( ) {
                        preindex = $(this).prevAll().length;
                        index = preindex + 1;
                        listclass = "item-" + index;
                        $(this).addClass(listclass);
                    });
                    deleteclick();

                })
            }
        }
        var myDropzone = new Dropzone('#my-dropzone');

        $('#upload-submit').on('click', function (e) {
            e.preventDefault()
            //trigger file upload select
            $('#my-dropzone').trigger('click')
        })


        $(".image_picker_selector li").each(function( ) {
            preindex = $(this).prevAll().length;
            index = preindex + 1;
            listclass = "item-" + index;
            $(this).addClass(listclass);
        });
        
        var idvalue;

        deleteclick();

        $('.image_picker_selector img').on('click', function (e) {
            logosrc = $(this).attr("src");
            $('input[name="featured_image_use"]').val(logosrc)
        })

        var firstimage = $(".image-picker option:first-child").attr("data-img-src");
        $('input[name="featured_image_use"]').val(firstimage)

        var salary = new Cleave('#salary', {
            numeral: true,
            numeralPositiveOnly: true,
            numeralThousandsGroupStyle: 'thousand'
        })

        var salary = new Cleave('#hourly_ask_salary_enter', {
            numeral: true,
            numeralPositiveOnly: true,
            numeralThousandsGroupStyle: 'thousand'
        })

        var hrsalary = new Cleave('#hrsalary', {
            numeral: true,
            numeralPositiveOnly: true,
            numeralIntegerScale: 3,
            numeralDecimalScale: 2
        })

        var hrsalary = new Cleave('#annual_ask_salary_enter', {
            numeral: true,
            numeralPositiveOnly: true,
            numeralIntegerScale: 3,
            numeralDecimalScale: 2
        })
                $('#hrsalary, #salary').bind('change blur', function () {
                    var salary = parseFloat($(this).val().replace(/,/g, ''));
                    var max = parseFloat($(this).attr('max'))
                    var min = parseFloat($(this).attr('min'))
                    if (salary && (salary > max || salary < min)) {
                        $(this).next('p.text-danger').show();
                        $(this).addClass('required-empty');
                    } else {
                        $(this).next('p.text-danger').hide();
                        $(this).removeClass('required-empty');
                    }
                }).trigger('change')

            })

//we want to manually init the dropzone.
    Dropzone.autoDiscover = false;
</script>
<style>
    .dropzone .dz-preview.dz-image-preview {
        display: inline-block !important;
    }
    .dz-preview, #video_embed {
        display:none;
    }
    .description_label:after {
      content:"*";
      color:red;
    }
    ul.thumbnails.image_picker_selector li .thumbnail img {
        max-height: 60px;
        background: #fff;
    }
    ul.thumbnails.image_picker_selector li .thumbnail.selected {
        background: #639941;
    }
    ul.thumbnails.image_picker_selector li .thumbnail {
        margin-bottom: 0;
    }
    .image_picker_selector .delete-icon {
        position: absolute;
        top: 3px;
        right: 3px;
        padding: 1px 2px;
        margin: 0;
        background: red;
        line-height: 13px;
        font-size: 12px;
        color: #fff;
    }
    .note-editor.note-frame {
        border: 2px solid #a9a9a9;
    }
    .dropzone {
    min-height: 100px;
    border: 1px solid #ccc;
    background: white;
    padding: 20px 20px;
    border-radius: 4px;
}
    .image_picker_selector .delete-icon:hover {
        cursor: pointer;
        text-decoration: none;
    }
    .image_picker_selector .thumbnail {
        position: relative;
    }
    .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
a:hover {
 cursor:pointer;
}
.tooltip_templates { display: none; }
</style>
                                    <?php 
                                        $tab_company_logo = \App\Settings::where('column_key','tab_company_logo')->first();
                                        $tab_company_name = \App\Settings::where('column_key','tab_company_name')->first();
                                        $tab_title = \App\Settings::where('column_key','tab_title')->first();
                                        $tab_preview = \App\Settings::where('column_key','tab_preview')->first();
                                        $tab_main = \App\Settings::where('column_key','tab_main')->first();
                                        $tab_selling = \App\Settings::where('column_key','tab_selling')->first();

                                        $tab_video = \App\Settings::where('column_key','tab_video')->first();
                                        $tab_photos = \App\Settings::where('column_key','tab_photos')->first();

                                        $tab_location = \App\Settings::where('column_key','tab_location')->first();
                                        $tab_state = \App\Settings::where('column_key','tab_state')->first();
                                        $tab_category = \App\Settings::where('column_key','tab_category')->first();
                                        $tab_subcategory = \App\Settings::where('column_key','tab_subcategory')->first();
                                        $tab_salary = \App\Settings::where('column_key','tab_salary')->first();
                                        $tab_estsalary = \App\Settings::where('column_key','tab_estsalary')->first();
                                        $tab_hrsalinfo = \App\Settings::where('column_key','tab_hrsalinfo')->first();
                                        $tab_annualsalinfo = \App\Settings::where('column_key','tab_annualsalinfo')->first();
                                        $tab_incentive = \App\Settings::where('column_key','tab_incentive')->first();
                                        $tab_freq = \App\Settings::where('column_key','tab_freq')->first();
                                        $tab_showsal = \App\Settings::where('column_key','tab_showsal')->first();
                                        $tab_emptype = \App\Settings::where('column_key','tab_emptype')->first();
                                        $tab_empstatus = \App\Settings::where('column_key','tab_empstatus')->first();
                                        $tab_workloc = \App\Settings::where('column_key','tab_workloc')->first();
                                        $tab_shiftguide = \App\Settings::where('column_key','tab_shiftguide')->first();

                                        $tab_applic = \App\Settings::where('column_key','tab_applic')->first();
                                        $tab_secemail = \App\Settings::where('column_key','tab_secemail')->first();
                                        $tab_url = \App\Settings::where('column_key','tab_url')->first();

                                    ?>
<div class="col-md-12">
    <div class="scrollto" style="padding-bottom: 20px;display: none;"></div>
    <div class="alert alert-danger required_errors" style="display: none;">
        <li class="comp_name_required" style="display: none;">Please enter company name to display.</li>
        <li class="title_required" style="display: none;">Job title field is required.</li>
        <li class="preview_required" style="display: none;">Preview description field is required.</li>
        <li class="description_required" style="display: none;">Main description field is required.</li>
        <li class="location_required" style="display: none;">Job location field is required.</li>
        <li class="state_required" style="display: none;">State field is required.</li>
        <li class="sal_type_required" style="display: none;">Please select your preferred Salary Type.</li>
        <li class="sal_est_required" style="display: none;">Please enter your preferred estimated salary.</li>
        <li class="sal_hr_required" style="display: none;">Please enter estimated salary less than $999.99</li>
        <li class="sal_annual_required" style="display: none;">Please enter estimated salary less than $500,000</li>
        <li class="select_annual_required" style="display: none;">Please select do you want this job to appear in hourly salary searches as well?</li>
        <li class="enter_annual_required" style="display: none;">Please enter your estimated salary to appear in hourly salary searches</li>
        <li class="enter_annual_required_check" style="display: none;">Please enter estimated salary less than $500,000 to appear in annual salary searches</li>
        <li class="select_hourly_required" style="display: none;">Please select do you want this job to appear in annual salary searches as well?</li>
        <li class="enter_hourly_required" style="display: none;">Please enter your estimated salary to appear in annual salary searches</li>
        <li class="incent_required" style="display: none;">Please select your preferred Incentive Structure.</li>
        <li class="freq_required" style="display: none;">Please confirm your preferred pay frequency.</li>
        <li class="showsal_required" style="display: none;">Show salary on listing field is required.</li>
        <li class="emptype_required" style="display: none;">The employment type field is required.</li>
        <li class="empstatus_required" style="display: none;">Please select your preferred Employment Status.</li>
        <li class="workloc_required" style="display: none;">Please select your preferred Work Location.</li>
        <li class="shiftguide_required" style="display: none;">Please select shift guide closest to your requirements.</li>
        <li class="recappl_required" style="display: none;">Please select how would you like to receive applications?</li>
        <li class="url_required" style="display: none;">Please URL in which your applicants will be redirected to.</li>
    </div>
    <div class="clearfix"></div>


    <h3 class='orangeudnerline' style="margin-bottom:25px">Advertisement Details</h3>

    <form id="my-dropzone" action="/poster/uploadimg"  method="post" class="form single-dropzone form-horizontal form-bordered" enctype="multipart/form-data"> 
        <div class="form-group" id="featured_image_div">

            <label for="featured_image" class="col-sm-3 control-label">Please choose the Company Logo to display @if($tab_company_logo->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_company_logo->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_company_logo->id!!}">
                    {!!$tab_company_logo->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8" ><input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <select class="image-picker show-html">
                    @foreach ($companylogos as $companylogo)
                        <option class="dbload" data-img-src="{{ $companylogo->file }}" value="{{ $companylogo->id }}"></option>
                    @endforeach
                </select>
                <button id="upload-submit" class="btn btn-default margin-t-5"><i class="fa fa-upload"></i> Upload New Company Logo</button>
                <div class="spin hidden" style="padding-top: 3px;">Uploading Company Logo <img style="width: 25px;" src="/Spinner.gif" alt="Loading" title="Loading" /></div>
                <div class="info hidden" style="padding-top: 3px;"><i class="fa fa-check" aria-hidden="true"></i> Your Company Logo has been uploaded Successfully!</div>
            </div>
        </div>
    </form>

    <form action="/poster/posts/create" id="form-username" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" novalidate>

        <input type="hidden" name="_token" value="{{csrf_token()}}"/> 

        <div class="cleafix"></div>
        <div class="form-group">
            <label for="company"  class="col-sm-3 control-label required" >Company name to display @if($tab_company_name->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_company_name->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_company_name->id!!}">
                    {!!$tab_company_name->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <input id="company" data-class="company-name" class="form-control" type="text" name="company" 
                       placeholder="{{trans('messages.enter_post_company')}}" value="{{old('title')}}"/>
            </div>
        </div>
        
        <input id="featured_image_use" class="form-control" type="hidden" name="featured_image_use"/>
        <input id="featured_image_delete" class="form-control" type="hidden" name="featured_image_delete"/>
        <input id="featured_image" style="padding-bottom: 45px" class="form-control" type="hidden" name="featured_image"/>

        <input id="multipleimg" class="form-control" type="hidden" name="multipleimg"/>

        <div class="form-group">
            <label for="title" class="col-sm-3 control-label required">Job Title @if($tab_title->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_title->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_title->id!!}">
                    {!!$tab_title->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <input id="title" data-class="job-title" class="form-control" type="text" name="title" 
                       placeholder="{{trans('messages.enter_post_title')}}" value="{{old('title')}}"/>
            </div>
        </div>

        <div class="form-group">
            <label for="shortdescription" class="col-sm-3 control-label description_label">Preview Description @if($tab_preview->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_preview->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_preview->id!!}">
                    {!!$tab_preview->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8" id="shortie">
                <textarea id="selling4" class="form-control" name="short_description" maxlength="200"></textarea>
                <small><span id ="charCounter4"></span></small>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-3 control-label description_label">Main Description @if($tab_main->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_main->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_main->id!!}">
                    {!!$tab_main->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <textarea id="summernote" class="form-control" name="description" maxlength="2500"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-3 control-label">Selling Point 1
            @if($tab_selling->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_selling->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_selling->id!!}">
                    {!!$tab_selling->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <textarea id="selling1" data-class="selling-point1" class="form-control" name="selling1" maxlength="60" ></textarea>
                <small><span id ="charCounter1"></span></small>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-3 control-label">Selling Point 2</label>
            <div class="col-sm-8">
                <textarea id="selling2" data-class="selling-point2" class="form-control" name="selling2" maxlength="60"></textarea>
                <small><span id ="charCounter2"></span></small>
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-3 control-label">Selling Point 3</label>
            <div class="col-sm-8">
                <textarea id="selling3" data-class="selling-point3" class="form-control" name="selling3" maxlength="60"></textarea>
                <small><span id ="charCounter3"></span></small>
            </div>
        </div>
        <h4 class='orangeudnerline'>Video & Graphics<br> <small>(only available with our Featured Job Levels)</small> </h4>
        <div class="form-group">
            <label for="video_link" class="col-sm-3 control-label">Include a video on this listing? @if($tab_video->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_video->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_video->id!!}">
                    {!!$tab_video->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <input onchange="checkVid()" id="video_link" class="form-control" type="text" name="video_link" 
                       placeholder="Video’s can only be displayed for Featured Jobs" value="http://"/>
                 <div id="video_embed"><img src="#"></div>
                <small><span id ="vidOK">Please enter a valid Vimeo or Youtube URL e.g  <a target="_blank" href="https://www.youtube.com/watch?v=lhYEyoyAD6g">https://www.youtube.com/watch?v=lhYEyoyAD6g</a></span></small>
            </div>
        </div>

        <div class="form-group">
            <label for="uploadimg" class="col-sm-3 control-label">Job Photos to display @if($tab_photos->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_photos->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_photos->id!!}">
                    {!!$tab_photos->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8" style="padding-right: 3px; padding-left: 7px; margin-bottom: 20px;">
                <div id="dropzone-img" class="dropzone">
                    <div class="form-group">
                      <div class="fallback">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="uploadimg" name="file" type="file" />
                      </div>
                    </div>
                </div>
                <small><i style="color: #639941;" class="fa fa-info-circle" aria-hidden="true"></i> Max file size is <strong>2MB</strong> per image and maximum of 5 photos can be uploaded!</small>
            </div>
        </div>


       

        <h3 class='orangeudnerline' style="margin-bottom:25px;  margin-top: 25px;">Job Details</h3>          
        <div class="form-group" style="display:none">
            <label for="country" class="col-sm-3 control-label">{{trans('messages.countries')}}</label>
            <div class="col-sm-8">
                <select id="country" name="country" class="form-control" selected>Australia</select>
            </div>
        </div>


        <div class="form-group">
            <label for="joblocation" class="col-sm-3 control-label required">Job Location @if($tab_location->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_location->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_location->id!!}">
                    {!!$tab_location->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <input id="joblocation" class="form-control geocomplete" type="text" name="joblocation" />
                <input id="lat" class="form-control geocomplete" type="hidden" name="lat" />
                <input id="lng" class="form-control geocomplete" type="hidden" name="lng" />
                <input id="city" class="form-control geocomplete" type="hidden" name="city" />
                <input id="postcode" class="form-control geocomplete" type="hidden" name="postcode" />
            </div>
        </div>



        <div class="form-group">
            <label for="state" class="col-sm-3 control-label required">{{trans('messages.states')}} @if($tab_state->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_state->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_state->id!!}">
                    {!!$tab_state->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="stateselect" onchange="stateChange()" name="stateselect" class="form-control">
                    <option value="">Select State</option>
                    <option value="Australian Capital Territory">Australian Capital Territory</option>
                    <option value="New South Wales">New South Wales</option>
                    <option value="Northern Territory">Northern Territory</option>
                    <option value="Queensland">Queensland</option>
                    <option value="South Australia">South Australia</option>
                    <option value="Tasmania">Tasmania</option>
                    <option value="Victoria">Victoria</option>
                    <option value="Western Australia">Western Australia</option>
                </select>
            </div>
            <input type="hidden" id="state" name="state" value="">
        </div>

        <div class="form-group">
            <label for="category" class="col-sm-3 control-label" required>Job Category @if($tab_category->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_category->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_category->id!!}">
                    {!!$tab_category->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="category" name="category1" class="form-control" onchange="changeCat()">
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
            </div>
            <input id="category2" class="form-control" type="hidden" name="category" value="{{old('category')}}"/>
        </div>

        <div class="form-group">
            <label for="sub_category" class="col-sm-3 control-label" required>Secondary Category @if($tab_subcategory->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_subcategory->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_subcategory->id!!}">
                    {!!$tab_subcategory->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="sub_category" name="sub_category1" onchange="changeSubCat()" class="form-control"></select>
            </div>
            <input id="sub_category1" class="form-control" type="hidden" name="sub_category" value="{{old('sub_category')}}"/>
        </div>

        <div class="form-group">
            <label for="salary_type" class="col-sm-3 control-label required">Salary Type @if($tab_salary->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_salary->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_salary->id!!}">
                    {!!$tab_salary->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="salary_type1" class="form-control" name="salarytype1" onchange="changeSal()">
                    <option value="">Please Select</option>
                    <option value="annual">Annual Salary</option>
                    <option value="hourly">Hourly Salary</option>
                </select>
            </div>
            <input id="salary_type" class="form-control" type="hidden" name="salarytype" value="{{old('salarytype')}}"/>
        </div>


        <div class="form-group" id="sal">
            <label for="salary" id='sallab' class="col-sm-3 control-label required">Estimated Annual Salary($) @if($tab_estsalary->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_estsalary->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_estsalary->id!!}">
                    {!!$tab_estsalary->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <input id="salary" class="form-control" type="text" name="salary" max="500000" min="0" />
                <p class="text-danger" style="color: red;"><small>Please enter an amount less than $500,000</small></p>
            </div>
        </div>

        <div class="form-group" id="annual_ask" style="display:none">
            <label for="annual_ask" id='annual_ask_req' class="col-sm-3 control-label">As you have entered an Annual Salary, do you want this job to appear in hourly salary searches as well? @if($tab_hrsalinfo->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_hrsalinfo->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_hrsalinfo->id!!}">
                    {!!$tab_hrsalinfo->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="annual_ask_select" class="form-control" onchange="annual_ask_select_change()" name="annual_ask_select">
                    <option value="">Please Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <input id="annual_ask_select_pass" class="form-control" type="hidden" name="annual_ask_select_pass"/>
            </div>
        </div>

        <div class="form-group" id="annual_ask_salary" style="display:none">
            <label for="annual_ask_salary" id='annual_ask_salary_req' class="col-sm-3 control-label">Estimated Hourly Salary($)
            </label>
            <div class="col-sm-8">
                <input id="annual_ask_salary_enter" class="form-control" type="number" name="annual_ask_salary_enter" max="999.99" min="0" onchange="annual_ask_salary_enter_check()"/>
                <p class="text_danger_annual" style="color: red; display: none;"><small>Please enter an amount less than $999.99</small></p>
            </div>
        </div>

        <div class="form-group" id="hourlysal" style="display:none">
            <label for="hrsalary" id='hrsallab'  class="col-sm-3 control-label">Estimated Hourly Salary($) @if($tab_estsalary->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_estsalary->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_estsalary->id!!}">
                    {!!$tab_estsalary->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <input id="hrsalary" class="form-control" type="text" name="hrsalary" max="999.99" min="0"/>
                <p class="text-danger" style="color: red;"><small>Please enter an amount less than $999.99</small></p>
            </div>
        </div>

        <div class="form-group" id="hourly_ask" style="display:none">
            <label for="hourly_ask" id='hourly_ask_req' class="col-sm-3 control-label">As you have entered an Hourly Salary, do you want this job to appear in annual salary searches as well? @if($tab_annualsalinfo->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_annualsalinfo->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_annualsalinfo->id!!}">
                    {!!$tab_annualsalinfo->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="hourly_ask_select" class="form-control" onchange="hourly_ask_select_change()" name="hourly_ask_select">
                    <option value="">Please Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <input id="hourly_ask_select_pass" class="form-control" type="hidden" name="hourly_ask_select_pass"/>
            </div>
        </div>

        <div class="form-group" id="hourly_ask_salary" style="display:none">
            <label for="hourly_ask_salary" id='hourly_ask_salary_req' class="col-sm-3 control-label">Estimated Annual Salary($)
            </label>
            <div class="col-sm-8">
                <input id="hourly_ask_salary_enter" class="form-control" type="text" name="hourly_ask_salary_enter" onchange="hourly_ask_salary_enter_check()"/>
                <p class="text_danger_hourly" style="color: red; display: none;"><small>Please enter an amount less than $500,000</small></p>
            </div>
        </div>


        <div class="form-group">
            <label for="salary_info" class="col-sm-3 control-label required">Incentive Structure @if($tab_incentive->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_incentive->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_incentive->id!!}">
                    {!!$tab_incentive->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="incentivestructure1" class="form-control" onchange="incChange()" name="incentivestructure1">
                    <option value="">Please Select</option>
                    <option value="fixed">Base + Super</option>
                    <option value="basecommbonus">Base + Super + R&R/Bonus</option>
                    <option value="basecomm">Base + Super + Commissions</option>
                    <option value="commonly">Commissions Only</option>
                </select>
            </div>
            <input id="incentivestructure" class="form-control" type="hidden" name="incentivestructure" value="{{old('incentivestructure')}}"/>
        </div>

        <div class="form-group">
            <label for="pay_cycle" class="col-sm-3 control-label required">Pay Frequency  @if($tab_freq->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_freq->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_freq->id!!}">
                    {!!$tab_freq->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="pay_cycle1" onchange="pcChange()" class="form-control" name="paycycle1">
                    <option value="">Please Select</option>
                    <option value="weekly">Weekly</option>
                    <option value="fortnightly">Fortnightly</option>
                    <option value="monthly">Monthly</option>
                    <option value="no">Not Disclosed</option>
                </select>
            </div>
            <input id="pay_cycle" class="form-control" type="hidden" name="paycycle" value="{{old('pay_cycle')}}"/>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label required">Show salary on listing? @if($tab_showsal->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_showsal->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_showsal->id!!}">
                    {!!$tab_showsal->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">

                <select id="showsalary1" name="showsalary1" onchange="ssChange()" class="form-control">
                    <option value="">Please Select</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                    
                </select>

            </div>
             <input id="showsalary" class="form-control" type="hidden" name="showsalary" value="{{old('pay_cycle')}}"/>
        </div>

        <div class="form-group" style="display:none">
            <label for="status" class="col-sm-3 control-label">{{trans('messages.status')}}</label>
            <div class="col-sm-8">
                <select id="status" class="form-control" name="status">
                    @foreach(\App\Posts::getPositionStatuses() as $status => $label)
                        <option value="{{$status}}">{{$label}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="employment_type" class="col-sm-3 control-label required">{{trans('messages.post_employment_type')}} @if($tab_emptype->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_emptype->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_emptype->id!!}">
                    {!!$tab_emptype->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="emptype" name="emptype" onchange="emptypeChange()" class="form-control">
                    <option value="">Please Select</option>
                    <option value="part_time">{{trans('messages.post_employment_type_part_time')}}</option>
                    <option value="full_time">{{trans('messages.post_employment_type_full_time')}}</option>
                    <option value="casual">{{trans('messages.post_employment_type_casual')}}</option>
                </select>
                <input id="employment_type" class="form-control" type="hidden" name="employment_type" value="{{old('employment_type')}}"/>
            </div>
        </div>

        <div class="form-group">
            <label for="employment_term" class="col-sm-3 control-label required">{{trans('messages.post_employment_term')}} @if($tab_empstatus->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_empstatus->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_empstatus->id!!}">
                    {!!$tab_empstatus->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="empterm" name="empterm" onchange="emptermChange()" class="form-control">
                    <option value="">Please Select</option>
                    <option value="permanent">{{trans('messages.post_employment_permanent')}}</option>
                    <option value="fixed_term">Fixed-Term / Contract</option>
                    <option value="temp">{{trans('messages.post_employment_temp')}}</option>
                </select>
                <input id="employment_term" class="form-control" type="hidden" name="employment_term" value="{{old('employment_term')}}"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label required" for="workfromhome">Work Location @if($tab_workloc->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_workloc->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_workloc->id!!}">
                    {!!$tab_workloc->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="workfromhome" onchange="workFromHomeChange()" class="form-control">
                    <option value="">Please Select</option>
                    <option value="1">At business address</option>
                    <option value="2">By mutual agreement</option>
                    <option value="3">Work from home</option>
                </select>
                <input id="work_from_home" class="form-control" type="hidden" name="work_from_home" value="{{old('work_from_home')}}"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label required" for="parentsfilter">Shift Guide @if($tab_shiftguide->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_shiftguide->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_shiftguide->id!!}">
                    {!!$tab_shiftguide->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="parentsfiltervalue" onchange="parentsfilterChange()" class="form-control">
                    <option value="">Please select the shift times closest to your requirements</option>
                    <option value="1">Standard Business Hours (Shifts between hours of 9.00am and 5.00pm Mon to Fri)</option>
                    <option value="2">Extended Business Hours (Shifts between hours of 8.00am and 8.00pm Mon to Fri)</option>
                    <option value="3">Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only)</option>
                    <option value="4">Afternoon Shift (Shifts between hours of 12pm and 12am)</option>
                    <option value="5">Night Shifts</option>
                    <option value="6">Rotating Shifts - no weekends</option>
                    <option value="7">Rotating Shifts - including weekend work</option>
                </select>
                <input id="parents_filter_pass" class="form-control" type="hidden" name="parents_filter" value=""/>
            </div>
        </div>

        <h3 class='orangeudnerline' style="margin-bottom:25px; margin-top: 25px;">Application Submission Information</h3>   
        <div class="form-group">
            <label class="col-sm-3 control-label required" for="wheretosend">How would you like to receive applications? @if($tab_applic->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_applic->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
            <div class="tooltip_templates">
                <span id="tooltip_content{!!$tab_applic->id!!}">
                    {!!$tab_applic->value_txt!!}
                </span>
            </div>
            @endif
            </label>
            <div class="col-sm-8">
                <select id="wheretosend" onchange="showFields();"  class="form-control">
                    <option value="" selected>Please Select</option>
                    <option value="0">I want to receive and manage applications on ItsMyCall</option>
                    <option value="1">Send applicants to a website or link to complete their application</option>
                </select>
                <input id="application_method" class="form-control" type="hidden" name="application_method" value="{{old('application_method')}}"/>
            </div>
        </div>
        <div class="field-wrap" id="inside-itsmycall" style="display:none;">
            <div class="form-group">
                <label for="person_email" style="padding-top: 0;" class="col-sm-3 control-label">Job application notifications will be sent to:</label>
                <div class="col-sm-8">
                    <input class="form-control" type="text" disabled="disabled" value="{{Auth::user()->email}}" />
                    <p>You can change this in your user preferences. <a target="_blank" href="/poster/edit-poster" tabindex="-1"> Click here to change </a></p>
                </div>
            </div>
            <div class="form-group">
                <label for="person_email" style="padding-top: 0;" class="col-sm-3 control-label">Secondary email address @if($tab_secemail->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_secemail->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
                <div class="tooltip_templates">
                    <span id="tooltip_content{!!$tab_secemail->id!!}">
                        {!!$tab_secemail->value_txt!!}
                    </span>
                </div>
                @endif
                </label>
                <div class="col-sm-8">
                    <input id="person_email" class="form-control" type="email" name="person_email" placeholder="Secondary Email" value="{{old('person_email')}}"/>
                </div>
            </div>
            <h3 style="margin-top: 30px;">Optional Screening Questions</h3>
            <?php 
                $posts_question_text =  \App\Settings::where('column_key','posts_question_text')->first();
            ?>
            @if($posts_question_text) {!!$posts_question_text->value_txt!!} @endif
            <div class="box-list" id="step2_box">
            <div class="outerbox clearfix">
              <div class="item">
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-3">
                      <h4>{!!$price_questions->title!!}</h4>
                      <h4 style="color: #ff7200;">
                        ${{number_format($price_questions->price, 2, '.', '')}}
                       </h4>
                    </div>
                    <div class="col-md-6">
                        {!!$price_questions->description!!}
                    </div>
                    <div class="col-md-3" style="font-size: 20px; font-weight:400;margin-top: 15px; color:#078d00">
                    <?php $questionoff = \DB::table('questionspricing')->first();?>
                            <div class="standout">
                                <input id="questionprice" class="questiontotal" data-pricestan="{{$questionoff->price}}" data-price="{{$price_questions->price}}" name="questionspricing" type="checkbox" value="{{$price_questions->id}}" />
                                <label for="questionprice">Select ${{number_format($price_questions->price, 2, '.', '')}}</label>
                              </div>
                    </div>
                  </div>
                </div>
              </div><!-- end item list -->
            </div><!-- end item list -->
            </div><!-- end item list --> 
    <div class="row hidden questiondisplay">
            <h3 class='col-sm-12' style="margin-top: 30px;">Select Screening Questions </h3>
            <div class="col-sm-12">
            <?php 
                $posts_select_question_text =  \App\Settings::where('column_key','posts_select_question_text')->first();
            ?>
            @if($posts_select_question_text) {!!$posts_select_question_text->value_txt!!} @endif             
            </div> 
            <div class="col-sm-12">
            <div class="form-group">
                <div class="col-sm-12" style="padding-top: 20px;">
                     <div class="table-responsive countgreater">
                        <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                        <thead>
                        <tr style="color: white;background-color: #639941; ">
                            <th style="padding: 10px;">Question   
                            </th>
                            <th style="padding: 10px;">Modify</th>
                            <th style="padding: 10px;">Select</th>
                        </tr>
                        </thead>
                        <tbody class="added_quest">
                            <tr>
                                <td style="border-right: 0;"><h4 style="font-size: 16px; font-weight: 600;"><a style="text-decoration: none;" onclick="openall2()">Default Questions <i style="    font-size: 15px;" class="fa fa-chevron-down" aria-hidden="true"></i></a></h4></td>
                                <td style="border-right: 0; border-left: 0;"></td>
                                <td style="width: 160px;border-left: 0;"></td>
                            </tr>
                            @foreach($admin_questions as $question)
                            <tr class="admin_open hidden">
                                <td style="border-right: 0;word-wrap:break-word;">{{$question->question}}</td>
                                <td style="border-right: 0; border-left: 0;"></td>
                                <td style="width: 160px;border-left: 0;">
                                  <div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;">
                                    <input id="checkbox{{$question->id}}" class="question" type="checkbox" value="{{$question->question}}" name="question[]"  />
                                    <label for="checkbox{{$question->id}}" style="font-size: 22px;margin-bottom: 2px; font-weight: 200;">Select</label>
                                  </div>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td style="border-right: 0;"><h4 style="font-size: 16px; font-weight: 600;"><a style="text-decoration: none;" onclick="openall()">Your Questions <i style="    font-size: 15px;" class="fa fa-chevron-down" aria-hidden="true"></i></a></h4></td>
                                <td style="width: 160px;border-right: 0; border-left: 0;"></td>
                                <td style="border-left: 0;"><a data-toggle="modal" style="color: ; padding: 7px 12px; height: 36px;" class="btn btn-primary" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> Add New Question</a></td>
                            </tr>
                            @foreach($user_questions as $question)
                            <tr class="table_question{{$question->id}} open_question hidden">
                                <td class="question_edit" style="border-right: 0;word-wrap:break-word;">{{$question->question}}</td>
                                <td style="width: 160px;border-right: 0; border-left: 0;">
                                    <a class="btn-info btn" style="text-decoration: none;" data-id="{{$question->id}}" data-question="{{$question->question}}" onclick="questionedit(this)">
                                    Edit
                                    </a>
                                    <a class="btn-danger btn" style="text-decoration: none;" data-id="{{$question->id}}" onclick="questionremove(this)">
                                    Remove
                                    </a>
                                </td>
                                    
                                <td style="border-left: 0;">
                                  <div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;">
                                    <input id="checkbox{{$question->id}}" class="question" type="checkbox" value="{{$question->question}}" name="question[]"  />
                                    <label for="checkbox{{$question->id}}" style="font-size: 22px;margin-bottom: 2px; font-weight: 200;">Select</label>
                                  </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                     </div>
                </div>
            </div>
          </div>
    </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog" style="background: rgba( 255, 255, 255, 1 );">
              <div class="modal-dialog modal-lg" style="margin: 155px auto !important;">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create a Custom Question</h4>
                  </div>
                  <div class="modal-body">
                        <span class="question_success hidden"><i class="fa fa-check" aria-hidden="true"></i> Your Question has been added Successfully!</span>
                        <input type="text" id="question" class="form-control" style="height: 34px;" placeholder="Enter Question" onkeyup="countChar(this)" maxlength="100">
                        <span id="charNum2" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-default questiongo" onclick="questionadd()">Save</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
        </div>
        <div class="field-wrap" id="outside-itsmycall" style="display:none">
            <div class="form-group">
                <label for="email_or_link" id="linker" class="col-sm-3 control-label">Please enter the URL in which your applicants will be redirected to @if($tab_url->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$tab_url->id!!}"><a style="text-decoration: none;" class="fa fa-info-circle"></a></span>
                <div class="tooltip_templates">
                    <span id="tooltip_content{!!$tab_url->id!!}">
                        {!!$tab_url->value_txt!!}
                    </span>
                </div>
                @endif
                </label>
                <div class="col-sm-8">
                    <input id="email_or_link" class="form-control" type="text" name="email_or_link" placeholder="Enter valid URL (e.g. http://www.google.com)" value="{{old('email_or_link')}}"/>
                </div>
            </div>

        </div> 
    </form>
</div>
<div class="modal fade" tabindex="-1" id="coverModal" role="dialog" style="background: rgba( 255, 255, 255, 1 );">
    <div class="modal-dialog" style="margin: 155px auto !important;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <p id="Note"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="/js/image-picker.min.js"></script>
<script>
    $(document).on('change', 'input.question', function () {
              var maxAllowed = 5;
              var cnt = $(".question:checked").length;
              if (cnt > maxAllowed)
              {
                 $(this).prop("checked", "");
                 swal({
                            html: "You can only select a maximum of 5 screening questions.",
                        })
             }
          });
    function openall(){
        if($('.open_question').hasClass( "hidden" )){
           $('.open_question').removeClass('hidden');
        }else{
            $('.open_question').addClass('hidden');
        }

    }
    function openall2(){
        if($('.admin_open').hasClass( "hidden" )){
           $('.admin_open').removeClass('hidden');
        }else{
            $('.admin_open').addClass('hidden');
        }

    }
    function questionremove(val) {
        var id = $(val).data('id');
        var data = {id: id};
        $.ajax({
        url: "/poster/removequestion",
                data: data,
                headers:
        {
        'X-CSRF-Token': $('input[name="_token"]').val()
        },
                method: "post",
                success: function (response) {
                    $('.table_question'+id).remove();
                console.log(response);
                },
                error:  function (response) {
                console.log(response);
                }
        });
      };
    function questionedit(val) {
        var id = $(val).data('id');
        var question = $('.table_question'+ id +' .question_edit').text();
        $('.table_question'+ id +' .btn-info').attr('disabled', 'disabled');
        var data = {id: id};
        $('.table_question'+ id +' .question_edit').html('<input data-id="'+ id +'" data-question="'+ question +'" class="form-control edited_question" style="height: 34px; margin-bottom:5px;" placeholder="Edit Question" value="'+ question +'" maxlength="100" /> <a style="text-decoration: none;" data-id="'+ id +'" data-question="'+ question +'" class="btn-default btn" onclick="editsave(this)">Save</a> <a style="text-decoration: none;" data-id="'+ id +'" data-question="'+ question +'" class="btn-default btn" onclick="savecancel(this)">Cancel</a>')
      };
    function savecancel(val) {
        var id = $(val).data('id');
        var question = $(val).data('question');
        $('.table_question'+ id +' .question_edit').html(question)
        $('.table_question'+ id +' .btn-info').removeAttr('disabled');
      };
    function editsave(val) {
        var id = $(val).data('id');
        var question = $('.edited_question').val();
        $('.table_question'+ id +' .btn-info').removeAttr('disabled');
        if(!$('.edited_question').val()){
            var data = {id: id};
            $.ajax({
            url: "/poster/removequestion",
                    data: data,
                    headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            },
                    method: "post",
                    success: function (response) {
                        $('.table_question'+id).remove();
                    console.log(response);
                    },
                    error:  function (response) {
                    console.log(response);
                    }
            }); 
        }else{
            var data = {id: id , question:question};
            $.ajax({
            url: "/poster/edit_question",
                    data: data,
                    headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            },
                    method: "post",
                    success: function (response) {
                        $('.table_question'+ id +' .question_edit').html(question)
                        $('.table_question'+ id +' .question').val(question)
                    console.log(response);
                    },
                    error:  function (response) {
                    console.log(response);
                    }
            });
        }
      };

    function countChar(val) {
        if($(val).val()){
            $('.questiongo').removeAttr('disabled')
            $('#question').css('background-color', 'initial');
        }
        $('#charNum2').removeClass('hidden');
        var maxLength = 100;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum2').text('Remaining Characters: '+length+'/100');
      };
    function questionadd() {
    // alert("hello");
    if(!$('#question').val()){
        $('#question').css('background-color', '#FFC0BF !important');
        $('.questiongo').attr('disabled', 'disabled');
    }else{
        var question = $('#question').val();
        var data = {question: question};
        $.ajax({
        url: "/poster/addquestion",
                data: data,
                headers:
        {
        'X-CSRF-Token': $('input[name="_token"]').val()
        },
                method: "post",
                success: function (response) {
                    $('.added_quest').append('<tr class="table_question'+ response +' open_question"><td class="question_edit" style="border-right: 0;word-wrap:break-word;">' + question + '</td><td style="width: 160px;border-right: 0; border-left: 0;"><a class="btn-info btn" style="text-decoration: none;" data-question="'+ question +'" data-id="'+ response +'" onclick="questionedit(this)">Edit</a> <a style="text-decoration: none;" data-question="'+ question +'" data-id="'+ response +'" class="btn-danger btn" onclick="questionremove(this)">Remove</a></td><td style="border-left: 0;"><div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;"><input id="checkbox'+ response +'" class="question" type="checkbox" value="'+ question +'" name="question[]"  /><label for="checkbox'+ response +'" style="font-size: 22px; margin-bottom: 2px; font-weight:200;">Select</label></div></td></tr>');
                    $('#question').val('')
                    $('.open_question').removeClass('hidden');
                    $('#charNum2').addClass('hidden');
                    $('#button_remove').addClass('hidden');
                    $('.question_success').removeClass('hidden');
                    $('.countgreater').removeClass('hidden');
                    $('#myModal').modal('hide');
                setTimeout(function () { 
                    $('.question_success').addClass('hidden');
                 }, 5000);
                console.log(response);
                },
                error:  function (response) {
                console.log(response);
                }
        });
    }
    }
    $(".image-picker").imagepicker();
    $(".image_picker_selector .thumbnail").append("<a class='delete-icon'>X</a>");
    function loadCoverModal(id) {
    $('#coverModal').modal('toggle');
    $('#Note').html(id);
    }
    
    function showFields() {
        var val = $('#wheretosend').val();
        $('#application_method').val(val);
        if(val == ''){
            $('#inside-itsmycall').hide();
            $('#outside-itsmycall').hide();
        }else{
            if (val == 0) {
            $('#inside-itsmycall').show();
            $('#outside-itsmycall').hide();
            $('#linker').removeClass("required");
            } else if(val == 1) {
                $('#outside-itsmycall').show();
                $('#linker').addClass("required");
                $('#inside-itsmycall').hide();
            }
        }

    }

    function changeSal() {
        var val = $('#salary_type1').val();
        $('#salary_type').val($('#salary_type1').val());
        if (val == 'hourly') {
            $('#hourlysal').show('slow');
            $('#sal').hide('slow');
            $('#hrsallab').addClass("required");
            $('#sallab').removeClass('yes');
            $('#sallab').removeClass("required");

            $('#annual_ask').hide('slow');
            $('#annual_ask_req').removeClass("required");
            $('#annual_ask_salary').hide('slow');
            $('#annual_ask_salary_req').removeClass("required");

            $('#hourly_ask').show('slow');
            $('#hourly_ask_req').addClass("required");
            $('#hourly_ask_select').val("");
        } else {
            $('#hourlysal').hide('slow');
            $('#sal').show('slow');
            $('#sallab').addClass("required");
            $('#hrsallab').removeClass('yes');
            $('#hrsallab').removeClass('required');

            $('#annual_ask').show('slow');
            $('#annual_ask_req').addClass("required");

            $('#hourly_ask').hide('slow');
            $('#hourly_ask_req').removeClass("required");
            $('#hourly_ask_salary').hide('slow');
            $('#hourly_ask_salary_req').removeClass("required");
            $('#annual_ask_select').val("");
        }

    }

    function annual_ask_select_change() {
        var val = $('#annual_ask_select').val();
        $('#annual_ask_select_pass').val(val);
        if (val == 'yes') {
            $('#annual_ask_salary').show('slow');
            $('#annual_ask_salary_req').addClass("required");
        } else {
            $('#annual_ask_salary').hide('slow');
            $('#annual_ask_salary_req').removeClass("required");
        }

    }

    function hourly_ask_select_change() {
        var val = $('#hourly_ask_select').val();
        $('#hourly_ask_select_pass').val(val);
        if (val == 'yes') {
            $('#hourly_ask_salary').show('slow');
            $('#hourly_ask_salary_req').addClass("required");
        } else {
            $('#hourly_ask_salary').hide('slow');
            $('#hourly_ask_salary_req').removeClass("required");
        }

    }

    function annual_ask_salary_enter_check() {
        var val = $('#annual_ask_salary_enter').val();
        if (val > 999.99) {
            $('#annual_ask_salary_enter').val('999.99');
            $('.text_danger_annual').show('slow');
        } else if(val < 0){
            $('#annual_ask_salary_enter').val('0');
            $('.text_danger_annual').show('slow');
        }else{
            $('.text_danger_annual').hide('slow');
        }

    }

    $( "#salary" ).change(function() {
      var salary = parseFloat($(this).val().replace(/,/g, ''));
        if (salary > 0 && salary < 500000) {
            $go_sal = salary/1976;
            $('#annual_ask_salary_enter').attr("placeholder",'e.g =' + ($go_sal).toFixed(0));
        }else{
            $('#annual_ask_salary_enter').removeAttr('placeholder')
        }
    });

    $( "#hrsalary" ).change(function() {
      var salary = parseFloat($(this).val().replace(/,/g, ''));
        if (salary > 0 && salary < 999.99) {
            $go_sal = salary*1976;
            $('#hourly_ask_salary_enter').attr("placeholder",'e.g =' + ($go_sal).toFixed(0));
        }else{
            $('#hourly_ask_salary_enter').removeAttr('placeholder')
        }
    });

    function hourly_ask_salary_enter_check() {
        var val = parseFloat($('#hourly_ask_salary_enter').val().replace(/,/g, ''));
        if (val > 500000) {
            $('.text_danger_hourly').show('slow');
        } else if(val < 0){
            $('.text_danger_hourly').show('slow');
        }else{
            $('.text_danger_hourly').hide('slow');
        }

    }

    function workFromHomeChange() {
      $('#work_from_home').val($('#workfromhome').val());
      $('.homenone').css('display','none');
      if($('#workfromhome').val() == 1){
        $('.homeif1').css('display','');
      }else if($('#workfromhome').val() == 2){
        $('.homeif2').css('display','');
      }else if($('#workfromhome').val() == 3){
        $('.homeif3').css('display','');
      }
    }
    function parentsfilterChange() {
      $('#parents_filter_pass').val($('#parentsfiltervalue').val());
      $('.parentnone').css('display','none');
      if($('#parentsfiltervalue').val() == 1){
        $('.parentif1').css('display','');
      }else if($('#parentsfiltervalue').val() == 2){
        $('.parentif2').css('display','');
      }else if($('#parentsfiltervalue').val() == 3){
        $('.parentif3').css('display','');
      }else if($('#parentsfiltervalue').val() == 4){
        $('.parentif4').css('display','');
      }else if($('#parentsfiltervalue').val() == 5){
        $('.parentif5').css('display','');
      }else if($('#parentsfiltervalue').val() == 6){
        $('.parentif6').css('display','');
      }else if($('#parentsfiltervalue').val() == 7){
        $('.parentif7').css('display','');
      }
    }
    function stateChange() {
            $('#state').val($('#stateselect').val());
        }
        
    function emptypeChange() {
            $('#employment_type').val($('#emptype').val());
        }
        
    function emptermChange() {
            $('#employment_term').val($('#empterm').val());
        }
    function incChange() {
            $('#incentivestructure').val($('#incentivestructure1').val());
        }
    function pcChange() {
            $('#pay_cycle').val($('#pay_cycle1').val());
        }
    function changeCat() {
            $('#category2').val($('#category').val());
        }
    function ssChange() {
            $('#showsalary').val($('#showsalary1').val());
        }
    function changeSubCat() {
            $('#sub_category1').val($('#sub_category').val());
        }


</script>

<style>
    .field-wrap  {

        margin-top: 35px;

    }
    .yes {

        color: #ff7200!important;
        background-color: #ffffff!important;
        border-color: #ff7200!important;
        height: 40px!important;
        min-width: 60px!important;
    }
</style>