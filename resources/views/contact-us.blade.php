@extends('default-page')

@section('extra_js')
	@parent
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script>
	$(document).ready(function () {
		var form = $('.ContactUs');
		form.on('submit', function (e) {
			e.preventDefault()
			var data = {};
			var errors = []
			var element;
			$.each($(this).serializeArray(), function (index, value) {
				data[value.name] = value.value
			})
			form.find('.alert').remove();
			$.ajax({
				method: 'POST',
				url: "/contact",
				data: data
			}).done(function (response) {
				element = createAlert('alert-success', response.message)
			}).fail(function (response) {
				var json = response.responseJSON
				if (response.status == 422) {
					$.each(json, function (key) {
						$.each(json[key], function (index) {
							errors.push(json[key][index])
						})
					})
				}
				if (response.status == 400) {
					errors.push(json.error)
				}
				element = createAlert('alert-danger', '<ul>' + errors.map(function (error) {
      		return '<li>' + error + ' </li>'
      	}).join('') + '</ul>');
      }).always(function () {
				form.prepend(element);
      });
		})

		function createAlert(alertClass, message) {
			var alert = $([
				'<div class="alert alert-dismissible" role="alert">',
				  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
				'</div>'].join(''))

			return alert.html(alert.html() + message).addClass(alertClass);
		}
	})
	</script>
@endsection

@section('content')
<div class="container min-hight">
  <div class="row" >
    <div class="col-md-9 col-sm-9">
	    {!! $html !!}
    </div>
  </div>
	<hr>
	<div class="row">
    <div class="col-md-9 col-sm-9"  style="padding-bottom:20px">
			<form action="/contact" method="POST" class="ContactUs">
				<div class="form-group">
					<label for="firstname">First name:</label>
					<input type="text" class="form-control" id="firstname" name="firstname" required>
				</div>
				<div class="form-group">
					<label for="lastname">Last name:</label>
					<input type="text" class="form-control" id="lastname" name="lastname" required>
				</div>
				<div class="form-group">
					<label for="company">Company name:</label>
					<input type="text" class="form-control" id="company" name="company" required>
				</div>
				<div class="form-group">
					<label for="email">Your contact email:</label>
					<input type="email" class="form-control" id="email" name="email" required>
				</div>
				<div class="form-group">
					<label for="number">Your contact number:</label>
					<input type="tel" class="form-control" id="number" name="number" required>
				</div>
				<div class="form-group">
					<label for="type">Enquiry type:</label>
					<select class="form-control" name="type" id="type" required>
						<option value="Advertising a Job">Advertising a Job</option>
						<option value="Applying for a Job">Applying for a Job</option>
						<option value="Other">Other</option>
					</select>
				</div>
				<div class="form-group">
					<label for="message">Message:</label>
					<textarea class="form-control" name="message" id="message" rows="3" required></textarea>
				</div>
				<div class="form-group">
					<div class="g-recaptcha" data-sitekey="{{ $recaptcha_site_key }}"></div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" class="btn btn-primary">Send</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
