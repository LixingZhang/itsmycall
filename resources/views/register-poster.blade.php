@extends('layouts.master')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
<div class="block-section "   style="padding-bottom:130px">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            {!!$settings_general->register_poster!!}
        </div>
        <div class="panel panel-lg">
            <div class="panel-body">
                <div class="col-md-12  col-xs-12">

                    <!-- form login -->
                    <form class="tosubmit" action="/register-poster" method="POST">

                        {!! csrf_field() !!}
                        @include('admin.layouts.notify')
                        <div class="white-space-10"></div>
                        <div class="form-group">
                            <label for="company" class="control-label required" required>Company Name</label>
                            <input type="text" required name="company" value="{{ old('company') }}" class="form-control" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="control-label required" required>First Name</label>
                            <input type="text" name="firstname"  value="{{ old('firstname') }}" class="form-control" required placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="control-label required" required>Last Name</label>
                            <input type="text" required name="lastname"  value="{{ old('lastname') }}" class="form-control" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label required" required>Phone</label>
                            <input type="text" required name="phone"  value="{{ old('phone') }}" class="form-control" placeholder="Phone">
                        </div>								
                        <div class="form-group" >
                            <label for="email" class="control-label required" required>Email</label>
                            <input type="email" required name="email"  value="{{ old('email') }}" class="form-control" placeholder="Your Email">
                        </div>

                        <div class="form-group">
                            <label class="control-label required" required>State you live in</label>
                            <select id="state" required name="state" class="form-control" required>
                                <option @if(old('state') == "" ) selected @endif value="">Select State</option>
                                <option @if(old('state') == "Australian Capital Territory" ) selected @endif value="Australian Capital Territory">Australian Capital Territory</option>
                                <option @if(old('state') == "New South Wales" ) selected @endif value="New South Wales">New South Wales</option>
                                <option @if(old('state') == "Northern Territory" ) selected @endif value="Northern Territory">Northern Territory</option>
                                <option @if(old('state') == "Queensland" ) selected @endif value="Queensland">Queensland</option>
                                <option @if(old('state') == "South Australia" ) selected @endif value="South Australia">South Australia</option>
                                <option @if(old('state') == "Tasmania" ) selected @endif value="Tasmania">Tasmania</option>
                                <option @if(old('state') == "Victoria" ) selected @endif value="Victoria">Victoria</option>
                                <option @if(old('state') == "Western Australia" ) selected @endif value="Western Australia">Western Australia</option>
                                <option @if(old('state') == "I don't live in Australia" ) selected @endif value="I don't live in Australia">I don't live in Australia</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="advertiser_type" class="control-label required">Are you a Recruitment Agency or Private Advertiser</label>
                            <select name="advertiser_type" id="advertiser_type" class="form-control" required>
                                <option value="">Please Select</option>
                                @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                    <option value="{{$type}}" {{old('advertiser_type') == $type ? 'selected' : ''}}>{{$label}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="is_aus_contact_member" class="control-label required">Please enter a Registration Code if you have one</label>

                            <select class="form-control" id="is_aus_contact_member" name="is_aus_contact_member">
                                <option @if(old('is_aus_contact_member') == "" ) selected @endif value="">No Code</option>
                                <option @if(old('is_aus_contact_member') == "no" ) selected @endif value="no">I have a Registration Code</option>
                            </select>
                        </div>

                        <div class="form-group" @if(old('referral') == "" ) style="display:none;" @endif id="refcode">
                            <label for="referral" class="control-label required">Registration Code</label>
                            <input type="text"  name="referral" id="ref" class="form-control" placeholder="Please enter your Registration Code" value="{{old('referral')}}">
                        </div>

                        <div class="form-group" id ='aus_contact_member_no0' @if(old('aus_contact_member_no') == "" ) style="display:none;" @endif>
                            <br><img src="https://www.auscontact.com.au/sb/styles/auscontact_responsive/css/images/logo.png" width="150"><br><br>
                            <label id="aussiec" class="control-label required">Please enter your Auscontact Association Membership Number</label>
                            <input type="text" name="aus_contact_member_no" class="form-control aus_code" placeholder="Aus Contact member number" maxlength="10" value="{{old('aus_contact_member_no')}}">
                        </div>


                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Your Password" required>
                            <small>Password must be at least 6 characters long</small>
                        </div>
                        <div class="form-group">
                            <label>Re-type Password</label>
                            <input type="password" required name="password_confirmation" class="form-control" placeholder="Re-type Your Password">
                        </div>
                        <div class="form-group">

                            <input type="checkbox" required name="terms_and_conditions" placeholder="Enter Code"> <label> I agree to the <a target="_blank" href="/advertiser-terms-conditions">Advertiser Terms & Conditions</a>, <a target="_blank" href="/privacy">Privacy Policy</a>, the <a target="_blank" href="/privacy-collection-notice">Collection Notice</a> and the <a target="_blank" href="/terms-conditions">Website Terms of Use</a></label>
                        </div>
                        <div class="form-group no-margin">
                            <button type="submit" class="btn btn-theme btn-lg btn-t-primary btn-block btnnoterror">Register</button>
                            <a onclick="submit()" class="btn btn-theme btn-lg btn-t-primary btn-block btnerror hidden">Register</a>
                        </div>
                    </form><!-- form login -->

                </div>
             
            </div>
        </div>

    </div>
</div>
@endsection


@section('extra_js')
{!!$settings_general->register_poster_conversion!!}
<script>
    $(document).ready(function () {

        $('#is_aus_contact_member').on('change', function () {
             if ($('#is_aus_contact_member').val() == 'no') {
                $('#refcode').show();
                $('#ref').val('');
                $('.aus_code').val('');
             }else{
                $('.btnnoterror').removeClass('hidden');
                $('.btnerror').addClass('hidden');
                $('#ref').val('');
                $('#refcode').hide();
                $('#aus_contact_member_no0').hide();
                $('.aus_code').val('');
             } 
        });
        $('#ref').on('keyup', function () {
            var str = $('#ref').val();
            res = str.match(/AUS_/g);
             if (res) {
                $('#aus_contact_member_no0').show();
                $('.btnnoterror').addClass('hidden');
                $('.btnerror').removeClass('hidden');
             }else{
                $('#aus_contact_member_no0').hide();
                $('.btnnoterror').removeClass('hidden');
                $('.btnerror').addClass('hidden');
             } 
        });
    });

    function submit(){
        if($('.aus_code').val() == ''){
            swal(
              '',
              'Please enter your Auscontact Association Membership Number!',
              'error'
            )
        }else{
            $('.tosubmit').submit();
        }
    }      
</script>
<style>
    label.required:after {
        content:"*";
        color:red;
    }
    .required-empty,
    .required-empty:focus {
        background-color: #FFC0BF;
    }
</style>
@endsection