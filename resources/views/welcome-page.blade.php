@extends('layouts.master')

@section('google_adwards')
	{!! $google_adwards_tracking_code !!}
@endsection

@section('facebook_pixel')
	{!! $facebook_pixel_tracking_code !!}
@endsection

@section('content')
	<div class="container">
		<div class="clearfix">
			{!! $section_one !!}
		</div>
		<div class="clearfix">
			<div class="col-sm-6">
				<div class="col-sm-12">
					<h2>Testimonials</h2>
				</div>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="{{ $testimonial_interval }}">
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
						@foreach ($testimonials as $key => $testimonial)
							@if ($key == 0)
								<div class="item active">
							@else
								<div class="item">
							@endif
						    	<div class="col-sm-4 text-center">
						    		<img src="/uploads/images/{{ $testimonial->image }}" style="width: 100%; max-width:300px; margin: 5px 0;">
						    	</div>
						    	<div class="col-sm-8">
						    		<div>
							    		{!! $testimonial->html !!}
						    		</div>
					    		</div>
					    	</div>
						@endforeach
				  </div>
				</div>
			</div>
			<div class="col-sm-6">
				{!! $section_two !!}
			</div>
		</div>
		<div class="clearfix">
			{!! $section_three !!}
		</div>
		<div class="clearfix">
			{!! $section_four !!}
		</div>
		<div class="clearfix">
			{!! $section_five !!}
		</div>
		<?php $sectionsix = \App\Settings::where('category','welcome')->where('column_key','section_six')->first();
              $sectioneight = \App\Settings::where('category','welcome')->where('column_key','section_eight')->first();
              $sectionseven = \App\Settings::where('category','welcome')->where('column_key','section_seven')->first(); ?>
		<div class="clearfix">
			@if($sectionsix) {!! $sectionsix->value_txt !!} @endif
		</div>
		<div class="clearfix">
			@if($sectionseven) {!! $sectionseven->value_txt !!} @endif
		</div>
		<div class="clearfix">
			@if($sectioneight) {!! $sectioneight->value_txt !!} @endif
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title text-center" id="myModalLabel">Download our free Job Advertisers Guide!</h4>
		      </div>
		      <div class="modal-body">
		        <form class="Download" method="POST">
						  <div class="form-group">
						  	<label for="firstname">First name:</label>
						  	<input type="text" class="form-control" id="firstname" name="firstname" required>
						  </div>
						  <div class="form-group">
						    <label for="lastname">Last name:</label>
						    <input type="text" class="form-control" id="lastname" name="lastname" required>
						  </div>
						  <div class="form-group">
						    <label for="company">Company:</label>
						    <input type="text" class="form-control" id="company" name="company" required>
						  </div>
						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" class="form-control" id="email" name="email" required>
						  </div>
						  <div class="form-group">
						    <label for="number">Phone number:</label>
						    <input type="tel" class="form-control" id="number" name="number" required >
						  </div>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						  <button type="submit" class="btn btn-primary">Download</button>
						</form>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
                                    <?php 
                                    $googledownload = \App\Settings::where('category','welcome')->where('column_key','google_adwards_tracking_code-download')->first();
                                    $pixeldownload = \App\Settings::where('category','welcome')->where('column_key','facebook_pixel_tracking_code-download')->first();
                                    ?>
                                   <input type="hidden" class="googledownload" value="@if($googledownload) {!! $googledownload->value_txt !!} @endif" name="">
                                   <input type="hidden" class="pixeldownload" value="@if($pixeldownload) {!! $pixeldownload->value_txt !!} @endif" name="">
                                    <?php 
                                    $googleregister = \App\Settings::where('category','welcome')->where('column_key','google_adwards_tracking_code-register')->first();
                                    $pixelregister = \App\Settings::where('category','welcome')->where('column_key','facebook_pixel_tracking_code-register')->first();
                                    ?>
                                   <input type="hidden" class="googleregister" value="@if($googleregister) {!! $googleregister->value_txt !!} @endif" name="">
                                   <input type="hidden" class="pixelregister" value="@if($pixelregister) {!! $pixelregister->value_txt !!} @endif" name="">
<script type="text/javascript">
		function register() {
				if($('.googleregister').val()){
			        $googleregister = $('.googleregister').val();
			        eval($googleregister);
			    }
				if($('.pixelregister').val()){
			        $pixelregister = $('.pixelregister').val();
			        eval($pixelregister);
			    }
	}
</script>
@endsection

@section('extra_js')
<script>
	$(document).ready(function () {
		$('.Download').on('submit', function (e) {
			e.preventDefault()
			var data = {};
			$.each($(this).serializeArray(), function (index, value) {
				data[value.name] = value.value
			})
			var element;
			var modal = $('.modal-body');
			modal.find('.alert').remove();
			$.ajax({
				method: 'POST',
				url: "/download",
				data: data
			}).done(function (response) {
				element = createAlert('alert-success', response.message)
				if($('.googledownload').val()){
			        $googledownload = $('.googledownload').val();
			        eval($googledownload);
			    }
				if($('.pixeldownload').val()){
			        $pixelregister = $('.pixeldownload').val();
			        eval($pixelregister);
			    }
			}).fail(function (response) {
				element = createAlert('alert-danger', response.responseJSON.error)
      }).always(function () {
				modal.prepend(element);
      });
		});
	});

	function createAlert(alertClass, message) {
		var alert = $([
			'<div class="alert alert-dismissible" role="alert">',
			  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
			'</div>'].join(''))

		return alert.html(alert.html() + message).addClass(alertClass);
	}

</script>
@endsection
