@extends('layouts.master')
@section('extra_css')
    <!--BEGIN DATATABLE STYLES-->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/media/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="/assets/plugins/yadcf/jquery.dataTables.yadcf.css" />
    <!--END DATATABLE STYLES-->
@stop
@section('extra_js')
    <!-- BEGIN DATATABLE JS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jszip.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/assets/plugins/yadcf/jquery.dataTables.yadcf.js" type="text/javascript"></script>
    <!-- END DATATABLE JS -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Metronic.handleTables();
            $('#table').dataTable({
                ordering: false,
            });
        })
    </script>
@stop
<style>

    div#table_filter {
        float: right;
    }

    .pagination>li>a, .pagination>li>span {
        line-height: 1!important;
    }
    .fa {
            line-height: 2.2!important;
    }
    td{
        font-size: 15px;
    }
    

</style>
@section('content')

<div class="bg-color1">
    <div class="container">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="bg-color2 block-section-xs line-bottom">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">

                    </div>
                    <div class="col-sm-6">
                        <div class="text-right"><a href="/customer">&laquo; Go back</a></div>
                    </div>
                </div>
        </div><!-- end link top -->

        <div class="col-md-12 col-sm-9">
            <div class="block-section">
                <div class="col-xs-6">
                @if($savedjobs)
                <h3 class="no-margin-top">Your Saved Jobs:</h3>
                @else
                <h3 class="no-margin-top">No Saved Jobs!</h3>
                @endif
                </div>
            </div>
                <hr/>
                <div class="col-md-12">
                <div class="table-responsive table-striped table-hover">
                    <table class="table" id="table">
                        <thead>
                        <tr>
                            <td><strong>Job Title</strong></td>
                            <td><strong>Company</strong></td>
                            <td><strong>Location</strong></td>
                            <td><strong>Employment Hours</strong></td>
                            <td><strong>Employment Term</strong></td>
                            <td><strong>View</strong></td>
                            <td><strong>Edit</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($savedjobs as $job)
                            <?php 
                                $date = Carbon\Carbon::now();
                                $job_detail = \App\Posts::where('id',$job->job_id)->where('status','active')->where('expired_at','>',$date)->first();
                            ?>
                            @if($job_detail)
                            <tr>
                                <td>{{$job_detail->title}}</td>
                                <td><a href="/filter_company/{{$job_detail->company}}">{{$job_detail->company}}</a></td>
                                <td><a href="https://www.google.com/maps/place/{{$job_detail->joblocation}}">{{$job_detail->joblocation}}</a></td>
                                <td>{{ucfirst(str_replace("_", " ", $job_detail->employment_type))}}</td>
                                <td>{{ucfirst(str_replace("_", " ", $job_detail->employment_term))}}</td>
                                <td><a href="/{{$job_detail->slug}}" class="btn btn-info btn-xs"><i class="fa fa-info-circle" aria-hidden="true"></i> View More & Apply</a></td>
                                <td><a href="/customer/removesavejob/{{$job->id}}" class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>

            </div>
        </div>

    </div>
</div>

@endsection