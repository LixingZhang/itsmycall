<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style type="text/css">
        .btn {
            background-color: #ff7200; /* Green */
            border: none;
            color: white;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 18px;
            border-radius: 4px;
        }
    </style>
</head>
<body>
<div>
    <p>Hi {{$first_name}},</p>
    <p>Thanks for creating an account with ItsMyCall and congratulations on successfully applying for {{$job}} with {{$company}}.</p>
    <p>A summary of your application, along with a copy of the CV you submitted is attached for your reference.</p>
    <p>To be able to access your new account you need to confirm your email address.</p>
    <a class="btn" href="{{ URL::to('/register/verify/' . $confirmation_code) }}?e={{$email}}">Click here to validate your email address now</a>
    <br />
    <p>Kind regards,</p>
    <img src="http://dev.itsmycall.com.au/jobs.png" alt="logo" style="width: auto; height:50px; margin-left: auto; margin-right: auto;" />
    <p><strong>Application details for {{$job}} at {{$company}}</strong></p>
    <p><strong>Your Cover Letter:</strong></p>
    <p>{{$comment}}</p>
    @if(!empty($reason1) || !empty($reason2) || !empty($reason3))
    <p><strong>Reasons: <small> Why the employer should hire you?</small></strong></p>
        @if($reason1) <p>1. {{$reason1}}</p> @endif
        @if($reason2) <p>2. {{$reason2}}</p> @endif
        @if($reason3) <p>3. {{$reason3}}</p> @endif
    @endif
    @if(count($answers) > 0)
        <h4><strong>Your Answers to Advertiser Screening Questions</strong></h4>
        @if(count($answers) > 0)
            @for ($i = 0; $i <= count($answers)-1 ; $i++)
                <p id="Note" style="color: #639941;">{{$questions[$i]}} </p>
                <p id="Note">{{$answers[$i]}}</p>
            @endfor
        @endif
    @endif
    <br>
</div>
</body>
</html>