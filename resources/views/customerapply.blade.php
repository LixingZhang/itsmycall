@extends('layouts.master')
@section('extra_css')
    <!--BEGIN DATATABLE STYLES-->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/media/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="/assets/plugins/yadcf/jquery.dataTables.yadcf.css" />
    <!--END DATATABLE STYLES-->
@stop
@section('extra_js')
    <!-- BEGIN DATATABLE JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jszip.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/assets/plugins/yadcf/jquery.dataTables.yadcf.js" type="text/javascript"></script>
    <!-- END DATATABLE JS -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Metronic.handleTables();
            $('#table').dataTable({
                ordering: true,
                order: [ 2, 'asc' ],
                paginate: true,
                bInfo : false,
            });
        })
    </script>
@stop
<style>
    div#table_filter {
        float: right;
    }

    .pagination>li>a, .pagination>li>span {
        line-height: 1!important;
    }
    .block-section{
        padding: 30px 0 !important;
    }
    

</style>
@section('content')
<?php

use App\Categories;
use App\SubCategories;
use Illuminate\Support\Str;
?>
<div class="bg-color1">
    <div class="container">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="bg-color2 block-section-xs line-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">

                    </div>
                    <div class="col-sm-6">
                        <div class="text-right"><a href="/customer">&laquo; Go back</a></div>
                    </div>
                </div>
            </div>
        </div><!-- end link top -->

        <div class="col-md-12 col-sm-9">
            <div class="block-section">
                @if(sizeof($applicants)>0)
                <h3 class="no-margin-top">Your Applied Jobs</h3>
                <div class="row">
                    <div class="col-md-12">
                    <select onchange="location = this.value;" class="selectpicker">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Status</option>
                            <option value="/customer/appliedjobs">All</option>
                            <option value="/customer/appliedjobs/Applied">Applied</option>
                            @foreach($statuses as $status)
                               <option @if(isset($id) && $id == $status->status) selected="selected" @endif value="/customer/appliedjobs/{{$status->status}}">{{$status->status}}</option>
                            @endforeach
                    </select>
                    </div>
                </div>
            @include('admin.layouts.notify')
                @else
                <h3 class="no-margin-top">No Applied Jobs!</h3>
                <div class="row">
                    <div class="col-md-12">
                    <select onchange="location = this.value;" class="selectpicker">
                            <option value="" disabled="disabled" selected style="display: none;">Filter by Status</option>
                            <option value="/customer/appliedjobs">All</option>
                            <option value="/customer/appliedjobs/Applied">Applied</option>
                            @foreach($statuses as $status)
                               <option @if(isset($id) && $id == $status->status) selected="selected" @endif value="/customer/appliedjobs/{{$status->status}}">{{$status->status}}</option>
                            @endforeach
                    </select>
                    </div>
                </div>
                @endif

                <hr/>
                <div class="table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Job Title</th>
                                <th>Company</th>
                                <th>Job Location</th>
                                <th>Date Applied</th>
                                <th>Status</th>
                                <th>Status Change</th>
                                <th>Cover Note</th>
                                <th>Reasons</th>
                                <th>Screening Questions</th>
                                <th>Resume/CV</th>
                                <th>URL</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($applicants as $applicant)
                            @if($applicant->applied_url)
                                <tr>
                                    <?php
                                        $post = \App\Posts::find($applicant->job_id); 
                                    ?>
                                    @if($post && $post->status == 'active')
                                    <?php 
                                      $category =  \App\Categories::find($post->category);
                                      $sub_category =  \App\SubCategories::find($post->subcategory);
                                    ?>
                                        @if($category && $sub_category)
                                        <th scope="row"><a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($post->state)}}/{{$post->id}}">{{$applicant->title}}</a></th>
                                        @else
                                        <th scope="row"><a href="/{{$post->slug}}">{{$applicant->title}}</a></th>
                                        @endif
                                    @else
                                    <th scope="row">{{$applicant->title}}</th>
                                    @endif
                                    <td>
                                        <?php
                                            $post = \App\Posts::find($applicant->job_id); 
                                        ?>
                                        @if($post)
                                            <a href="/filter_company/{{$post->company}}">{{$post->company}}</a>
                                        @else
                                        nil
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                            $post = \App\Posts::find($applicant->job_id); 
                                        ?>
                                        @if($post)
                                            <a href="https://www.google.com/maps/place/{{$post->joblocation}}">{{$post->joblocation}}</a>
                                        @else
                                        nil
                                        @endif
                                    </td>
                                    <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $applicant->date_created);
                                        $print = $date->format('d M Y'); ?>
                                    <td>@if($print)
                                            {{$print}} 
                                    @endif</td> 
                                    <td><button type="button" onclick="loadModal({{$applicant->id}})" class="btn btn-theme btn-xs btn-default" id="status{{$applicant->id}}">@if (strlen($applicant->status_user) > 0) {{$applicant->status_user}} @else Applied <i class="fa fa-chevron-down" style="line-height: inherit !important;"  aria-hidden="true"></i> @endif</button></td>
                                    <td>@if($applicant->date_change)
                                        <p class="response{{$applicant->id}}">
                                            <?php  $datechange = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $applicant->date_change);
                                                $printchange = $datechange->format('d M Y H:i:s'); ?>
                                            {{$printchange}}</p>  
                                        @else 
                                        <p class="response{{$applicant->id}}">nil</p>
                                        @endif
                                    </td>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td><a target="_blank" href="{{$applicant->applied_url}}">{{$applicant->applied_url}}</a></td> 
                                    <td><a href="/customer/deleteapplied/{{$applicant->id}}" class="btn btn-danger btn-xs">Remove</a></td>                                   
                                </tr>
                            @else
                            <tr>
                                <?php
                                    $post = \App\Posts::find($applicant->job_id); 
                                ?>
                                @if($post && $post->status == 'active')
                                <?php 
                                  $category =  \App\Categories::find($post->category);
                                  $sub_category =  \App\SubCategories::find($post->subcategory);
                                ?>
                                    @if($category && $sub_category)
                                    <th scope="row"><a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($post->state)}}/{{$post->id}}">{{$applicant->title}}</a></th>
                                    @else
                                    <th scope="row"><a href="/{{$post->slug}}">{{$applicant->title}}</a></th>
                                    @endif
                                @else
                                <th scope="row">{{$applicant->title}}</th>
                                @endif
                                <td>
                                    <?php
                                        $post = \App\Posts::find($applicant->job_id); 
                                    ?>
                                    @if($post)
                                        <a href="/filter_company/{{$post->company}}">{{$post->company}}</a>
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        $post = \App\Posts::find($applicant->job_id); 
                                    ?>
                                    @if($post)
                                        <a href="https://www.google.com/maps/place/{{$post->joblocation}}">{{$post->joblocation}}</a>
                                    @endif
                                </td>
                                <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $applicant->date_created);
                                    $print = $date->format('d M Y'); ?>
                                <td>@if($print)
                                        {{$print}} 
                                @endif</td> 
                                <td><button type="button" onclick="loadModal({{$applicant->id}})" class="btn btn-theme btn-xs btn-default" id="status{{$applicant->id}}">@if (strlen($applicant->status_user) > 0) {{$applicant->status_user}} @else Applied <i class="fa fa-chevron-down" style="line-height: inherit !important;"  aria-hidden="true"></i> @endif</button></td>
                                <td>@if($applicant->date_change)
                                        <p class="response{{$applicant->id}}">
                                            <?php  $datechange = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $applicant->date_change);
                                                $printchange = $datechange->format('d M Y H:i:s'); ?>
                                            {{$printchange}}</p>  
                                        @else 
                                        <p class="response{{$applicant->id}}">nil</p>
                                        @endif
                                    </td>
                            <td><a data-toggle="modal"
                                       data-target="#coverModal{{$applicant->id}}"
                                       class="btn btn-theme btn-xs btn-default">View Cover Note</a>
                                <div class="modal fade" tabindex="-1" id="coverModal{{$applicant->id}}" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Cover Note</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p id="Note">{{$applicant->cover_note}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                </td>
                                @if(!empty($applicant->reason1) || !empty($applicant->reason2) || !empty($applicant->reason3))
                                <td><a data-toggle="modal"
                                       data-target="#ReasonModal{{$applicant->id}}"
                                       class="btn btn-theme btn-xs btn-default">View Reasons</a>
                                <div class="modal fade" tabindex="-1" id="ReasonModal{{$applicant->id}}" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Reasons:<small> Why Employer should hire you?</small></h4>
                                            </div>
                                            <div class="modal-body">
                                                <p id="Note">@if($applicant->reason1) 1. {{$applicant->reason1}} @endif</p>
                                                <p id="Note"> @if($applicant->reason2) 2. {{$applicant->reason2}} @endif</p>
                                                <p id="Note">@if($applicant->reason3) 3. {{$applicant->reason3}} @endif</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                </td>
                                @else
                                <td>nil</td>
                                @endif

                                <?php
                                    $answers = [];
                                    $questions = [];
                                    $answer_count = NULL;
                                    if($applicant->answers){
                                        $answers = json_decode($applicant->answers);
                                        $questions = json_decode($applicant->questions);
                                        $answer_count = count($answers);
                                    }
                                 ?>
                                @if($applicant->answers)
                                <td><a data-toggle="modal"
                                       data-target="#AnswerModal{{$applicant->id}}"
                                       class="btn btn-theme btn-xs btn-default">View Answers</a>
                                <div class="modal fade" tabindex="-1" id="AnswerModal{{$applicant->id}}" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Screening questions & answers:</h4>
                                            </div>
                                            <div class="modal-body">
                                                @if($answer_count)
                                                @for ($i = 0; $i <= $answer_count-1 ; $i++)
                                                <p id="Note" style="color: #639941;">{{$questions[$i]}} </p>
                                                <p id="Note">{{$answers[$i]}}</p>
                                                <br>
                                                @endfor
                                                @endif
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                </td>
                                @else
                                <td>nil</td>
                                @endif

                                <td><a target="_blank" href="{{$applicant->resume}}" class="btn btn-theme btn-xs btn-default">View Resume / CV</a></td>
                                <td>nil</td>
                                <td><a href="/customer/deleteapplied/{{$applicant->id}}" class="btn btn-danger btn-xs">Remove</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <a href="/customer/savedjobs">View Saved Jobs</a> |
                 <a href="/job_list">Find a Job</a> |
                   <a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us">Support Centre</a>
            </div>
        </div>
    </div>
</div>

<script>    
    function loadModal(id) {
    // alert("hello");
    $('#myModal').modal('toggle');
    $('#currStatus').val(id);
    }

    function saveStatus() {
    // alert("hello");
    $('#myModal').modal('toggle');
    var id = $('#currStatus').val();
    var statusApp = $('#statusApp').val();
    var data = {appid: id, status: statusApp};
    console.log(statusApp)
    $.ajax({
    url: "/customer/updateapplied",
            data: data,
            headers:
    {
    'X-CSRF-Token': $('input[name="_token"]').val()
    },
            method: "post",
            success: function (response) {
            console.log(response);
            $('#status' + id).text(statusApp);
            $('.response' + id).text(response);
            },
            error:  function (response) {
            console.log(response);
            }
    });
    }
</script>
<div class="modal fade" tabindex="-1" id="myModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Status</h4>
            </div>
            <div class="modal-body">
                <select id="statusApp" class="form-control">
                @foreach($statuses as $status)
                    <option value="{{$status->status}}">{{$status->status}}</option>
                @endforeach
                </select>
                <input type="hidden" id="currStatus">
                <p>Can't find one you need? Suggest a new Status Reason <a href="https://itsmycall.zendesk.com/hc/en-us/requests/new" target="_blank">here</a></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="saveStatus()">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" tabindex="-1" id="coverModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Your Cover Note</h4>
            </div>
            <div class="modal-body">
                <p id="Note" style="word-wrap: break-word"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>

@endsection