@extends('layouts.master')
@section('extra_js')
@if($keywords)
<meta name="keywords" content="{{$keywords}}">
@endif
@if($seodescription)
<meta name="description" content="{{$seodescription}}">
@endif
@stop
@section('content')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<div class="bg-color2">
    <div class="container">

        <!-- form search area-->
        <div class="row">
            <div class="col-md-12" style="margin-top:30px">
                @foreach($ads as $ad)
                @if($ad->position=='above_page')
                {!! $ad->code !!}
                @endif
                @endforeach
                <!-- form search -->
            </div>
        </div>


        <div class="row">
                <div class="col-md-12">
                <p style="color: #c3c3c3;">Home <i class="fa fa-angle-right"></i> {{$category->title}} <i class="fa fa-angle-right"></i> {{$blogsubcategory->title}}</p>
                @if(sizeof($blogs) == 0)
                <h2 class="orangeudnerline" style="margin-top: 0;font-weight: 600;">No Blogs Found!
                @else
                <h2 class="orangeudnerline" style="margin-top: 0;font-weight: 600;">{{$blogsubcategory->title}} <span class="pull-right">
                            <select onchange="location = this.value;" class="selectpicker">
                                <option value="" disabled="disabled" selected style="display: none;">Sort By</option>
                                <option @if(isset($option) && $option == 'latest') selected="selected" @endif  value="/blogs/{{$category->slug}}/{{$blogsubcategory->slug}}/sort/latest">Latest</option>
                                <option @if(isset($option) && $option == 'oldest') selected="selected" @endif value="/blogs/{{$category->slug}}/{{$blogsubcategory->slug}}/sort/oldest">Oldest</option>
                                <option @if(isset($option) && $option == 'trending') selected="selected" @endif value="/blogs/{{$category->slug}}/{{$blogsubcategory->slug}}/sort/trending">Trending</option>
                            </select>
                        </span>
                </h2>
                @endif
                </div>
                @include('admin.layouts.notify')
                    @foreach($blogs as $blog)
                  <div class="col-md-10" style="margin-bottom: 30px;">
                     <div class="col-md-3 col-xs-12">
                     <img src="{{$blog->image}}" alt="post img" class="img-responsive" style="width: 100%; margin-bottom: 10px;max-height: 150px;">
                     </div>
                     <div class="col-md-9 col-xs-12">
                        <h3 class="no-margin"><a style="text-decoration: none;" href="/blogs/{{$category->slug}}/{{$blogsubcategory->slug}}/{{$blog->slug}}">{{$blog->title}}</a></h3>
                     <small><i class="fa fa-clock-o" aria-hidden="true"></i> Posted on {{\Carbon\Carbon::parse($blog->created_at)->format('d M Y')}} at {{\Carbon\Carbon::parse($blog->created_at)->format('g:i A')}}</small>
                        <article>
                         {!!$blog->preview!!}
                         </article>
                    </div>
                 </div>
                 @endforeach
        </div>
        <!-- pagination -->
                <nav>
                    @if(sizeof($blogs)>0)
                    <ul class="pagination pagination-theme  no-margin pull-right">
                        {!! $blogs->appends(Input::except('page'))->render() !!}
                    </ul>
                    @endif
                </nav>
                @foreach($ads as $ad)
                @if($ad->position=='below_page')
                {!! $ad->code !!}
                @endif
                @endforeach
    </div>
</div>
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            z-index: 2;
            color: #fff!important;
            cursor: default;
            background-color: #639941!important;
            border-color: #639941!important;
        }

        .pagination>li>a, .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #639941!important;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }
    </style>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a0fe4efa4ad971d"></script> 
    @endsection