@extends('layouts.master')

@section('extra_js')
<meta name="keywords" content="{{$keywords}}">
<meta name="description" content="{{$seodescription}}">
@endsection
@section('content')

<!-- link top -->
<div class="bg-color2 block-section-xs line-bottom">
    <div class="container">
        @include('admin.layouts.notify')
        <div class="row">
            <div class="col-sm-6 hidden-xs">
                <div>{{$blog->title}}</div>
            </div>
            <div class="col-sm-6">
                <div class="text-right"><a onclick="goBack()" href="#">&laquo; Go back to blog listings</a></div>
            </div>
        </div>
    </div>
</div><!-- end link top -->

<div class="bg-color2">
    <div class="container">
        <div class="row">
            <div class="col-md-10">

                @foreach($ads as $ad)

                @if($ad->position=='above_page')
                {!! $ad->code !!}
                @endif

                @endforeach

                <!-- box item details -->
                <div class="block-section box-item-details" >

                      <!-- Title -->
                      <h1 style="color: #639941; margin-top: 0px; font-size: 32px;">{{$blog->title}}</h1>

                      <hr>

                      <!-- Date/Time -->
                      <p><i class="fa fa-clock-o" aria-hidden="true"></i> Posted on {{\Carbon\Carbon::parse($blog->created_at)->format('d M Y')}} at {{\Carbon\Carbon::parse($blog->created_at)->format('g:i A')}} <span class="pull-right"><i class="fa fa-eye" aria-hidden="true"></i> {{$blog->views}}</span></p>

                      <hr>
                        <div style="max-width:700px; max-height: 450px;">
                            <img src="{{$blog->image}}" alt="blog img" class="img-responsive">
                        </div>                    
                      <hr>
                        {!!$blog->share!!}

                        {!!$blog->page!!}

                </div>

                @foreach($ads as $ad)

                @if($ad->position=='below_page')
                {!! $ad->code !!}
                @endif

                @endforeach

            </div>
        </div>
    </div>
</div>
<script>
    function goBack() {
        window.history.back();
    }
</script>
@endsection