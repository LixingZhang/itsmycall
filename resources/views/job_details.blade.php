@extends('layouts.master')

@section('extra_js')
<meta name="keywords" content="{{$keywords}}">
<meta name="description" content="{{$seodescription}}">
@endsection
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/nivo-slider.min.css" type="text/css" />
<link rel="stylesheet" href="/css/tooltipster.bundle.min.css">
<link rel="stylesheet" href="/css/tooltipster-sideTip-shadow.min.css">
<link rel="stylesheet" href="/css/tooltipster-sideTip-light.min.css">
<link rel="stylesheet" href="/css/tooltipster-sideTip-borderless.min.css">
<style>
    iframe {
        width: 100%;

    }
    .nivo-directionNav a {
        font-size: 40px;
        color: white;
        padding: 0 15px;
        margin: -5px 0;
    }
    .nivo-directionNav a:hover {
        text-decoration: none;
    }
    .col-centered{
        float: none;
        margin: 0 auto;
    }
    .list-group-itemuu {
        margin-bottom: 0px;
        border-bottom: 1px solid #ddd;
        position: relative;
        display: block;
        padding: 10px 15px;
        background-color: #fff;
    }
    .form-group {
        margin-bottom: 0px !important;
    }
    .tooltip_templates { display: none; }
</style>
<?php

use App\Categories;
use App\SubCategories; ?>
<!-- link top -->
<div class="bg-color2 block-section-xs line-bottom">
    <div class="container">
        @include('admin.layouts.notify')
        <div class="row">
            <div class="col-sm-6 hidden-xs">
                <div>{{$job->title}}</div>
            </div>
            <div class="col-sm-6">
                <div class="text-right"><a onclick="goBack()" href="#">&laquo; Go back to job listings</a></div>
            </div>
        </div>
    </div>
</div><!-- end link top -->
<?php 
$business_info = \App\Settings::where('column_key','business_info')->first();
$home_info = \App\Settings::where('column_key','home_info')->first();
$agreement_info = \App\Settings::where('column_key','agreement_info')->first();


$standard_info = \App\Settings::where('column_key','standard_info')->first();
$exten_info = \App\Settings::where('column_key','exten_info')->first();
$parent_info = \App\Settings::where('column_key','parent_info')->first();
$after_info = \App\Settings::where('column_key','after_info')->first();
$night_info = \App\Settings::where('column_key','night_info')->first();
$rotno_info = \App\Settings::where('column_key','rotno_info')->first();
$rotyes_info = \App\Settings::where('column_key','rotyes_info')->first();

$business_info_img = \App\Settings::where('column_key','business_info_img')->first();
$home_info_img = \App\Settings::where('column_key','home_info_img')->first();
$agreement_info_img = \App\Settings::where('column_key','agreement_info_img')->first();

$standard_info_img = \App\Settings::where('column_key','standard_info_img')->first();
$exten_info_img = \App\Settings::where('column_key','exten_info_img')->first();
$parent_info_img = \App\Settings::where('column_key','parent_info_img')->first();
$after_info_img = \App\Settings::where('column_key','after_info_img')->first();
$night_info_img = \App\Settings::where('column_key','night_info_img')->first();
$rotno_info_img = \App\Settings::where('column_key','rotno_info_img')->first();
$rotyes_info_img = \App\Settings::where('column_key','rotyes_info_img')->first();

?>

<div class="bg-color2">
    <div class="container">
        <div class="row">
            <div class="col-md-10">

                @foreach($ads as $ad)

                @if($ad->position=='above_page')
                {!! $ad->code !!}
                @endif

                @endforeach

                <!-- box item details -->
                <div class="block-section box-item-details" >
                    @if ($job->isExpired())
                    <h1 style="font-size: 32px;">Bummer! The Job you are looking for has expired.</h1>
                    <br>
                    <p style="font-size: 26px;">Just <a href="/job_list">click here</a> to search for more great roles.</p>
                    @elseif(!Auth::user() && $job->status != 'active')
                    <h1 style="font-size: 32px;">Bummer! The Job you are looking for has been removed.</h1>
                    <br>
                    <p style="font-size: 26px;">Just <a href="/job_list">click here</a> to search for more great roles.</p>
                    @elseif(Auth::user() && $job->author_id != Auth::user()->id && $job->status != 'active')
                    <h1 style="font-size: 32px;">Bummer! The Job you are looking for has been removed.</h1>
                    <br>
                    <p style="font-size: 26px;">Just <a href="/job_list">click here</a> to search for more great roles.</p>
                    @else
                        @if(Auth::check())
                            @if (\App\Users::isCustomer())
                                    <?php
                                    $apply_count = \App\Applicants::where('apply_id',Auth::id())->where('job_id',$job->id)->count();
                                    ?>
                                    @if(strlen($job->email_or_link) == 0 && $apply_count > 0 && !Session::has('success_msg'))
                                         <div class="alert alert-success" style="margin-top: -50px;">
                                          <i class="fa fa-info-circle" aria-hidden="true"></i> You have already applied for this Job. <a href="/customer/appliedjobs">View your applied Jobs</a>
                                        </div>       
                                    @endif
                            @endif
                        @endif

                    <div class="row title" style="margin-top: 0px; border-top:0px; border-bottom: 1px solid #e1e1e1; padding-top: 0;">
                        <div class="col-md-4" style="text-align: -webkit-center; padding-bottom: 20px;">
                        @if(strlen($job->featured_image)>0)
                            <img style="max-width: 280px; max-height: 100px;" src="{{$job->featured_image}}" class="img-responsive" alt="">
                        @endif
                        </div>
                        <div class="col-md-8 text-center">

                            <h1 style="color: #639941; margin-top: 0px; font-size: 32px;">{{$job->title}}</h1>
                            <h2 style="color: #666666; margin-top: 0; font-size: 28px;">{{$job->company}}</h2>
                            <span style="color: grey">{{Categories::where('id', $job->category_id)->get()->first()->title}} / {{SubCategories::where('id', $job->subcategory)->get()->first()->title}}</span>     
                        </div>
                    </div>
                    <div class="row" style="padding-top: 25px;">
                        <div class="col-md-4">
                            <div class="block-section" style="padding: 10px 0 !important;">
                                <div class="text-center" style="padding-bottom: 10px">
                                    @if(strlen($job->company)>0&&strlen($job->state)>0)
                                    <ul class="list-group" style="border: 2px solid; background-color: white;" >
                                        <li class="list-group-itemuu"><span class="color-black">Company: </span><a href="/filter_company/{{$job->company}}">{{$job->company}}</a></li>
                                        <li class="list-group-itemuu"><span class="color-black">Location: </span> <a href="https://www.google.com/maps/place/{{$job->joblocation}}">{{$job->joblocation}}</a></li>
                                        @if($job->showsalary == "1") 
                                        @if($job->salarytype == "annual")<li class="list-group-itemuu"><span class="color-black">Annual Salary: <span style="color: grey">${{number_format($job->salary)}}</span></span></li>@endif
                                        @if($job->salarytype == "hourly")<li class="list-group-itemuu"><span class="color-black">Hourly Rate: <span style="color: grey">${{number_format($job->hrsalary, 2)}}</span></span></li>@endif
                                        @if($job->paycycle != "no")<li class="list-group-itemuu"><span class="color-black">Pay Frequency: <span style="color: grey">{{ucfirst($job->paycycle)}}</span></span></li>@endif
                                        @if($job->paycycle == "no")<li class="list-group-itemuu"><span class="color-black">Pay Frequency: <span style="color: grey">Not Specified</span></span></li>@endif
                                        @endif

                                        <li class="list-group-itemuu"><span class="color-black">Employment Hours: <span style="color: grey">{{ucfirst(str_replace("_", " ", $job->employment_type))}}</span></span></li>
                                        <li class="list-group-itemuu"><span class="color-black">Employment Status: <span style="color: grey">
                                        @if($job->employment_term == 'fixed_term')
                                        Fixed-Term / Contract
                                        @else
                                        {{ucfirst(str_replace("_", " ", $job->employment_term))}}
                                        @endif
                                        </span></span></li>
                                        <li class="list-group-itemuu">
                                        <span class="color-black">Advertiser: <span style="color: grey">
                                        @if($job->user->advertiser_type == 'private_advertiser')
                                        Private Advertiser
                                        @else
                                        Recruitment Agency
                                        @endif
                                        </span>
                                        </span>
                                        </li>
                                        <li class="list-group-itemuu">
                                            <span class="color-black">Incentive Structure: 
                                                <span style="color: grey">
                                                    @if($job->incentivestructure == "fixed")Base + Super @endif
                                                    @if($job->incentivestructure == "basecommbonus")Base + Super + R&R/Bonus @endif
                                                    @if($job->incentivestructure == "basecomm")Base + Super + Commissions @endif
                                                    @if($job->incentivestructure == "commonly")Commissions Only @endif
                                                </span></span></li>
                                        <li class="list-group-itemuu">
                                        @if($job->work_from_home == 1) 
                                        @if($business_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}"><img src="{{$business_info_img->value_string}}" style="padding-left: 7px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$business_info->id!!}">
                                                {!!$business_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 2) 
                                        @if($agreement_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}"><img src="{{$agreement_info_img->value_string}}" style="padding-left: 7px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 3) 
                                        @if($home_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}"><img src="{{$home_info_img->value_string}}" style="padding-left: 7px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->parentsoption == 1) 
                                        @if($standard_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}"><img src="{{$standard_info_img->value_string}}" style="padding-left: 4px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 2) 
                                        @if($exten_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}"><img src="{{$exten_info_img->value_string}}" style="padding-left: 4px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 3) 
                                        @if($parent_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}"><img src="{{$parent_info_img->value_string}}" style="padding-left: 4px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 4) 
                                        @if($after_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}"><img src="{{$after_info_img->value_string}}" style="padding-left: 4px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 5) 
                                        @if($night_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}"><img src="{{$night_info_img->value_string}}" style="padding-left: 4px; width: 35px; "></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 6) 
                                        @if($rotno_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}"><img src="{{$rotno_info_img->value_string}}" style="padding-left: 4px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 7)  
                                        @if($rotyes_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}"><img src="{{$rotyes_info_img->value_string}}" style="padding-left: 4px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        </li>
                                            <?php
                                                $usergroup = false;
                                                if(Auth::user()){
                                                $usergroup = \App\UsersGroups::where('user_id',Auth::user()->id)->first();
                                                $status = \DB::table('savedjobs')->where('user_id',Auth::user()->id)->where('job_id',$job->id)->get();
                                                }   
                                             ?>
                                            @if($usergroup)
                                                @if($usergroup->group_id == 2)
                                                    @if($status)
                                                    <li class="list-group-itemuu">
                                                    <p style="display: block; margin: 0; color: #639941;">Saved!</p>
                                                     </li>
                                                    @else
                                                    <li class="list-group-itemuu">
                                                    <a style="display: block; text-decoration: none;" href="/customer/savejob/{{$job->id}}"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                                    </li>
                                                    @endif
                                                @endif
                                            @else
                                            <li class="list-group-itemuu">
                                            <a style="display: block; text-decoration: none;" href="/register-user"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                            </li>
                                            @endif
                                    </ul>
                                    @endif	

                                    @if (!$job->isExpired())
                                        @if(Auth::check())
                                            @if (\App\Users::isCustomer())
                                                <hr>
                                                <?php
                                                $apply_count = \App\Applicants::where('apply_id',Auth::id())->where('job_id',$job->id)->count();
                                                ?>
                                                @if(strlen($job->email_or_link) > 0)
                                                <p><a href="/out/{{$job->id}}"  target="_blank" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 180px; background: #ff7200;">Apply Now</a></p>
                                                <p style="line-height: 1.2em !important;  font-size: 12px;">* You will be taken to the Job Advertisers website to submit your application.</p>
                                                @elseif($apply_count == 0)
                                                <p><a href="#modal-apply"  data-target="#modal-apply" data-toggle="modal" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 180px; background: #ff7200;">Apply Now</a></p>
                                                @endif
                                            @endif
                                        @else
                                            @if(strlen($job->email_or_link) > 0)
                                            <p><a href="/out/{{$job->id}}"  target="_blank" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 180px; background: #ff7200;">Apply Now</a></p>
                                            <p style="line-height: 1.2em !important; font-size: 12px;">* You will be taken to the Job Advertisers website to submit your application.</p>
                                            @else
                                             <p><a href="#modal-apply"  data-target="#modal-apply" data-toggle="modal" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 180px; background: #ff7200;">Apply Now</a></p>
                                            @endif
                                        @endif
                                    @endif
                                </div>

                            </div><!-- box affix right -->

                        </div>	
                        <div class="col-md-8">
<?php if ($job->posts_packages == 1 && strlen($job->selling1) > 0 && strlen($job->selling2) > 0 && strlen($job->selling3) > 0) { ?>

                                <ul>
                                    @if(strlen($job->selling1)>0)<li style="font-size: 18px;">{{$job->selling1}}</li>@endif
                                    @if(strlen($job->selling2)>0)<li style="font-size: 18px;">{{$job->selling2}}</li>@endif
                                    @if(strlen($job->selling3)>0)<li style="font-size: 18px;">{{$job->selling3}}</li>@endif
                                </ul>
                                <hr>
<?php } ?>		

                            @if(strlen($job->video_link)>10)
                            <div class="embed">
                            </div>
                            <script>
                                var url = "{{$job->video_link}}";
                                $youtubecheck = url.match(/youtube/gi);
                                $vimeocheck = url.match(/vimeo/gi);
                                if ($youtubecheck !== null) {
                                    var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
                                    if(videoid !== null) {
                                       jQuery(".embed").append('<iframe height="300px" src="https://www.youtube.com/embed/' + videoid[1] + '" frameborder="0" allowfullscreen></iframe');
                                    } else { 
                                    }
                                } else if ($vimeocheck !== null) {
                                    var videoid = url.match(/https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/
);
                                    if(videoid !== null) {
                                       jQuery(".embed").append('<iframe height="300px" src="https://player.vimeo.com/video/' + videoid[3] + '" frameborder="0" allowfullscreen></iframe');
                                    } else { 
                                    }
                                };
                            </script>
                            @else
                         <div class="col-md-12" style="margin-bottom: 30px;">
                          <div id="slider" class="nivoSlider">   
                          @foreach($uploadfiles as $photo)
                            @if($photo->post_id == $job->id)
                                <img style="max-width: 100%;" src="{{$photo->jobimages}}" />
                            @endif
                          @endforeach
                          </div>    
                        </div>
                            @endif
                            <div class="jobdesc" style="margin-bottom: 30px;margin-top: 20px; word-wrap: break-word;">
                            {!!$job->description !!}
                            </div>
                        @if(strlen($job->video_link)>10) 
                        <div class="col-md-12" style="margin-bottom: 20px;">
                          <div id="slider" class="nivoSlider">   
                          @foreach($uploadfiles as $photo)
                            @if($photo->post_id == $job->id)
                                <img style="max-width: 100%;" src="{{$photo->jobimages}}" />
                            @endif
                          @endforeach
                          </div>    
                        </div>
                        @endif

                                    @if (!$job->isExpired())
                                        @if(Auth::check())
                                            @if (\App\Users::isCustomer())
                                                <hr>
                                                <?php
                                                $apply_count = \App\Applicants::where('apply_id',Auth::id())->where('job_id',$job->id)->count();
                                                ?>
                                                @if(strlen($job->email_or_link) > 0)
                                                <p style="text-align: center;"><a href="/out/{{$job->id}}"  target="_blank" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 300px; background: #ff7200;">Apply Now</a></p>
                                                 <div class="col-sm-6 col-centered" style="font-size: 12px; line-height: 1.2em; padding:0; text-align: left;">*
                                                 You will be taken to the Job Advertisers website to submit your application.
                                                 </div>
                                                @elseif($apply_count == 0)
                                                <p style="text-align: center;"><a href="#modal-apply"  data-target="#modal-apply" data-toggle="modal" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 300px; background: #ff7200;">Apply Now</a></p>
                                                @endif
                                            @endif
                                        @else
                                            @if(strlen($job->email_or_link) > 0)
                                            <p style="text-align: center;"><a href="/out/{{$job->id}}"  target="_blank" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 300px; background: #ff7200;">Apply Now</a></p>
                                                 <div class="col-sm-6 col-centered" style="font-size: 12px; line-height: 1.2em; padding:0; text-align: left;">*
                                                 You will be taken to the Job Advertisers website to submit your application.
                                                 </div>
                                            @else
                                             <p style="text-align: center;"><a href="#modal-apply"  data-target="#modal-apply" data-toggle="modal" class="btn btn-theme btn-t-primary btn-block-xs" style="min-width: 300px; background: #ff7200;">Apply Now</a></p>
                                            @endif
                                        @endif
                                    @endif
                        </div>
                    </div>
                @endif
                </div><!-- end box item details -->

                @foreach($ads as $ad)

                @if($ad->position=='below_page')
                {!! $ad->code !!}
                @endif

                @endforeach

            </div>
        </div>
    </div>
</div>

<!-- block map -->
<div class="collapse" id="map-toogle">
    <div class=" bg-color2" id="map-area">
        <div class="container">
            <div class="marker-description">
                <div class="inner">
                    <h4 class="no-margin-top">Office Location</h4>
                    {{$job->state}},{{$job->country}}
                </div>
            </div>
        </div>
        <div class="map-area-detail">
            <!-- change data  lat abd lng here -->
            @if(strlen($job->state)>0)
            <div class="box-map-detail" id="map-detail-job" data-lat="{{$lat}}" data-lng="{{$lon}}"></div>
            @endif
        </div>
    </div>
</div><!-- end block map -->

<!-- modal apply -->
<div class="modal fade" id="modal-apply" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            @include('admin.layouts.notify')

            <form action="/job_apply/{{$job->author_id}}" method="post" enctype="multipart/form-data" id="apply_form">
                <input type="hidden" name="job_id" value="{{$job->id}}"/>
                <input type="hidden" name="user_id" value="{{$job->author_id}}"/>
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <input type="hidden" name="title" value="{{$job->title}}"/>
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Apply</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" id="first_name" class="form-control " name="first_name" placeholder="Enter First Name" value="{{Auth::check() ? Auth::user()->first_name : ''}}">
                        <small class="first_name" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">First name field is required</small>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" id="last_name" class="form-control " name="last_name" placeholder="Enter Last Name" value="{{Auth::check() ? Auth::user()->last_name : ''}}">
                        <small class="last_name" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">Last name field is required</small>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control " name="phone" id="phone" placeholder="Enter Phone No." value="{{Auth::check() ? Auth::user()->mobile_no : ''}}">
                        <small class="phone" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">Phone number field is required</small>
                    </div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control email_required" name="email" id="email" placeholder="Enter Email" value="{{Auth::check() ? Auth::user()->email : ''}}">
                        <small class="email" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">Email address field is required</small>
                    </div>
                    <div class="form-group">
                        <label>Add a cover note to accompany your CV/resume </label>
                        <textarea class="form-control" rows="6" name="comment" id="comment" placeholder="Enter Message" onkeyup="countChardesc(this)" maxlength="1000">{{old('comment')}}</textarea>
                        <span id="charNumdesc" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 10px;">&nbsp</span>
                        <small class="comment" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">Cover note field is required</small>
                    </div>
                     <div class="form-group">
                        <label>Optional: <small>Give the Employer three reasons why they should hire you!</small></label>
                        <textarea class="form-control" rows="3" name="reason1" maxlength="100" placeholder="Reason 1" style="margin-top: 3px; margin-bottom: 3px;" onkeyup="countChar1(this)"></textarea>
                        <span id="charNum1" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                        <textarea class="form-control " rows="3" name="reason2" maxlength="100" placeholder="Reason 2" style="margin-top: 3px; margin-bottom: 3px;" onkeyup="countChar2(this)"></textarea>
                        <span id="charNum2" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                        <textarea class="form-control " rows="3" name="reason3" maxlength="100" placeholder="Reason 3" style="margin-top: 3px; margin-bottom: 3px;" onkeyup="countChar3(this)"></textarea>
                        <span id="charNum3" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                    </div>
                    @if($questions)
                     <div class="form-group">
                        <label style="margin-bottom: 10px;">The Job Advertiser has added some additional screening questions:<br> <small>Please provide your answers to the following questions</small></label>
                        @foreach($questions as $key => $question)
                        <input type="hidden" value="{{$question}}" name="questions[]">
                        <h4 style="margin: 0; padding-bottom: 5px;padding-top: 10px;">{{$question}}</h4>
                        <textarea class="form-control" rows="3" name="answer[]" maxlength="100" placeholder="Your answer" style="margin-top: 5px; margin-bottom: 0px;" required="required" onkeyup="countAns{{$key}}(this)"></textarea>
                        <span id="char{{$key}}" style="color: #639941; font-size: 12px; width:100%; clear:both; height: 8px;">&nbsp</span>
                        <script type="text/javascript">
                            function countAns{{$key}}(val) {
                                var maxLength = 100;
                                var length = $(val).val().length;
                                var length = maxLength-length;
                                $('#char{{$key}}').text('Remaining Characters: '+length+'/100');
                              };
                        </script>
                        @endforeach
                    </div>
                    @endif
                    @if(Auth::check() && !empty(Auth::user()->resume))
                        <div class="form-group previous_resume" style="padding-bottom: 20px;">
                                @if(Auth::user()->resume != '')
                                <label for="sub_category" required>Your Current CV/Resume</label>
                                <p style="margin-bottom: 0;"><small>We already have attached your current CV/Resume to this job application. View your current CV/Resume below: <br> </small> </p>
                                <a href="{{Auth::user()->resume}}" target="_blank" class="btn btn-primary" style="padding-top: 8px;">View your CV/Resume @if(Auth::user()->filename)({{Auth::user()->filename}}) @endif </a>
                                <br>
                                @endif
                        </div>
                        <div class="form-group">
                            <label>Add new CV/Resume:</label>
                            <p style="margin-bottom: 0;"><small>Not satisfied with your current CV/Resume? Add a new CV/Resume to this job application below: <br> </small> </p>
                            <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-theme btn-file">
                                    Add new CV/Resume  <input type="file" name="file" id="file">
                                </span>
                            </span>
                                <input type="text" class="form-control form-flat" readonly>
                            </div>
                            <div class="loader hidden">Uploading your new CV/Resume to this job application <img style="width: 25px;" src="/Spinner.gif" alt="Loading" title="Loading" /></div>
                            <div class="done hidden"><i class="fa fa-check" aria-hidden="true"></i> Your new CV/Resume has been uploaded and attached to this job application!</div>
                            <span class="info">File type must be doc,docx or pdf. Max file size is <strong>3MB</strong></span>
                            <small class="file" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">CV/Resume is required for application</small>
                        </div>
                    @else
                        <div class="form-group">
                            <label>Add CV/Resume:</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-theme btn-file">
                                        Add CV/Resume  <input type="file" name="file" id="file">
                                    </span>
                                </span>
                                <input type="text" class="form-control form-flat" readonly>
                            </div>

                            <div class="loader hidden">Uploading your CV/Resume to this job application <img style="width: 25px;" src="/Spinner.gif" alt="Loading" title="Loading" /></div>
                            <div class="done hidden"><i class="fa fa-check" aria-hidden="true"></i> Your CV/Resume has been uploaded and attached to this job application!</div>
                            <span class="info">File type must be doc,docx or pdf. Max file size is <strong>3MB</strong></span>
                            <small class="file" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">CV/Resume is required for job application</small>
                        </div>
                    @endif
                    @if(!Auth::user())
                        <div class="form-group">
                            <h4 style="font-weight: 600;"><input class="create_account" type="checkbox" value="1" name="create_account"> Would you like to create an account?</h4>
                        </div>
                        <div class="form-group">
                                <p style="font-weight: 600;font-size: 15px;margin: 10px 20px 0px;">Create an account and:</p>
                                <ul style="margin-bottom: 20px;margin-left: 10px;">
                                    <li>Save time when you apply for more jobs (we store all your details for next time)</li>
                                    <li>Track all the jobs you have applied for</li>
                                </ul>
                        </div>
                        <div class="form-group state_display" style="display: none;">
                                <label class="control-label required">State you live in</label>
                                <select id="state" name="state" class="form-control state_required">
                                    <option @if(old('state') == "" ) selected @endif value="">Select State</option>
                                    <option @if(old('state') == "Australian Capital Territory" ) selected @endif value="Australian Capital Territory">Australian Capital Territory</option>
                                    <option @if(old('state') == "New South Wales" ) selected @endif value="New South Wales">New South Wales</option>
                                    <option @if(old('state') == "Northern Territory" ) selected @endif value="Northern Territory">Northern Territory</option>
                                    <option @if(old('state') == "Queensland" ) selected @endif value="Queensland">Queensland</option>
                                    <option @if(old('state') == "South Australia" ) selected @endif value="South Australia">South Australia</option>
                                    <option @if(old('state') == "Tasmania" ) selected @endif value="Tasmania">Tasmania</option>
                                    <option @if(old('state') == "Victoria" ) selected @endif value="Victoria">Victoria</option>
                                    <option @if(old('state') == "Western Australia" ) selected @endif value="Western Australia">Western Australia</option>
                                    <option @if(old('state') == "I don't live in Australia" ) selected @endif value="I don't live in Australia">I don't live in Australia</option>
                                </select>
                                <small class="state_error" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">State field is required</small>
                        </div> 
                        <div class="form-group pass_display" style="display: none;">
                                <label class="control-label required">Password</label>
                                <input type="password" name="password" class="form-control password_required" placeholder="Your Password">
                                <small>Password must be at least 6 characters long</small>
                        </div>
                        <div class="form-group passconf_display" style="display: none;">
                                <label class="control-label required">Re-type Password</label>
                                <input type="password" name="password_confirmation" class="form-control password_confirmation_required" placeholder="Re-type Your Password">
                                <small class="password_error" style="color: #cc0000; margin: 0; display: block; visibility: hidden;"></small>
                        </div>
                        <div class="form-group terms_display" style="display: none;">
                                 <label>
                                    <input type="checkbox" name="terms_and_conditions"  value="yes" placeholder="Enter Code" class="terms_and_conditions_required"> I agree to the <a target="_blank" href="/terms-conditions">Terms and Conditions</a>, <a target="_blank" href="/privacy">Privacy Policy</a> and the <a target="_blank" href="/privacy-collection-notice">Collection Notice</a>
                                </label>
                        </div>
                    @endif
                    <p class="fieldsrequired" style="color: #cc0000; margin: 0;display: block;visibility: hidden;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please fill in the required fields!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-theme" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-theme">Send Application</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- end modal  apply -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/jquery.nivo.slider.min.js" type="text/javascript"></script>
<script type="text/javascript"> 
$('.create_account').change(function () {
    $val = $('.create_account').prop('checked');
    if($val == true){
        $('.state_display').show('slow');
        $('.pass_display').show('slow');
        $('.passconf_display').show('slow');
        $('.terms_display').show('slow');
        $('.state_required').attr('required', 'required');
        $('.password_required').attr('required', 'required');
        $('.password_confirmation_required').attr('required', 'required');
        $('.terms_and_conditions_required').attr('required', 'required');
        $('.email_required').attr('required', 'required');
    }else{
        $('.state_display').hide('slow');
        $('.pass_display').hide('slow');
        $('.passconf_display').hide('slow');
        $('.terms_display').hide('slow');
        $('.state_required').removeAttr('required')
        $('.password_required').removeAttr('required')
        $('.password_confirmation_required').removeAttr('required')
        $('.terms_and_conditions_required').removeAttr('required')
        $('.email_required').removeAttr('required')
    }
    $('.modal-backdrop').css('height', '2850px');
});
$(window).on('beforeunload', function(){
  $(window).scrollTop(0);
});
$(window).on('load', function() {
    $('.nivoSlider').nivoSlider({
      effect:'fade',
      directionNav: true,
      controlNav: false,
      prevText: '<',
      nextText: '>',
      pauseOnHover: false,
      pauseTime: 5000,
    }); 
}); 
</script>
    <script>
      function countChardesc(val) {
        var maxLength = 1000;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNumdesc').text('Remaining Characters: '+length+'/1000');
      };
      function countChar1(val) {
        var maxLength = 100;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum1').text('Remaining Characters: '+length+'/100');
      };
      function countChar2(val) {
        var maxLength = 100;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum2').text('Remaining Characters: '+length+'/100');
      };
      function countChar3(val) {
        var maxLength = 100;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum3').text('Remaining Characters: '+length+'/100');
      };
    </script>
<script>
    function goBack() {
        window.history.back();
    }

    $(function() {
        $('#apply_form').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var formData = new FormData(form[0]);
            var errors = {};
            var url = form.attr('action')
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                beforeSend: function (xhr) {
                    var file = form.find('input#file');
                    $('#fileupload-error').remove();
                    if (file[0].files.length) {
                        if (file[0].files[0].size > 3145728|| file[0].files[0].fileSize > 3145728) { //3MB
                            $('<p id="fileupload-error" class="text-danger">Allowed file size exceeded. (Max. 3 MB)</p>').insertAfter(file.parent().parent().parent());
                            return false;
                        } else {
                            $('#fileupload-error').remove();
                        }
                    }
                    $('#first_name').css('border-color', '');
                    $('.first_name').css('visibility', 'hidden');
                    $('#last_name').css('border-color', '');
                    $('.last_name').css('visibility', 'hidden');
                    $('#email').css('border-color', '');
                    $('.email').css('visibility', 'hidden');
                    $('#comment').css('border-color', '');
                    $('.comment').css('visibility', 'hidden');
                    $('#phone').css('border-color', '');
                    $('.phone').css('visibility', 'hidden');
                    $('.file').css('visibility', 'hidden');
                    $('.fieldsrequired').css('visibility', 'hidden');
                    $('.state_error').css('visibility', 'hidden');
                    $('.state_required').css('border-color', '');
                    $('.password_error').css('visibility', 'hidden');
                    $('.password_required').css('border-color', '');
                    $('.password_confirmation_required').css('border-color', '');
                    form.find('button[type="submit"]').text('Submitting your application... Please wait ').prop('disabled', true)
                },
                success: function (data) {
                    console.log(data);
                    if (typeof data.errors !== 'undefined') {
                        errors = data.errors;
                        var messages = [];
                        for(k in errors) {
                            if(k == 'first_name'){
                                $('#first_name').css('border-color', '#a94442');
                                $('.first_name').css('visibility', 'visible');
                            }
                            if(k == 'last_name'){
                                $('#last_name').css('border-color', '#a94442');
                                $('.last_name').css('visibility', 'visible');
                            }
                            if(k == 'email'){
                                $('#email').css('border-color', '#a94442');
                                $('.email').css('visibility', 'visible');
                                if($('.create_account').prop('checked')){
                                    $('.email').text('The email address already exists.');
                                }else{
                                    $('.email').text('The email address field is required.');
                                }
                            }
                            if(k == 'comment'){
                                $('#comment').css('border-color', '#a94442');
                                $('.comment').css('visibility', 'visible');
                            }
                            if(k == 'phone'){
                                $('#phone').css('border-color', '#a94442');
                                $('.phone').css('visibility', 'visible');
                            }
                            if(k == 'resume'){
                                $('.file').css('visibility', 'visible');
                            }
                            if(k == 'state'){
                                $('.state_required').css('border-color', '#a94442');
                                $('.state_error').css('visibility', 'visible');
                            }
                            if(k == 'password'){
                                $('.password_error').css('visibility', 'visible');
                                $('.password_required').css('border-color', '#a94442');
                                $('.password_confirmation_required').css('border-color', '#a94442');
                                if($('.password_required').val().replace(/\s+/g, '').length < 6 || $('.password_confirmation_required').val().replace(/\s+/g, '').length < 6){
                                    $('.password_error').text('The password must be at least 6 characters.');
                                }else{
                                    $('.password_error').text('The password confirmation does not match.');
                                }
                            }
                            if(k){
                                $('.fieldsrequired').css('visibility', 'visible');
                            }
                        }
                        form.find('button[type="submit"]').text('Send Application').prop('disabled', false)
                    } else {
                        location.reload();
                    }
                }
            });
        });
    });

</script>
<script src="/js/tooltipster.bundle.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
            $('.tooltips').tooltipster({
                contentCloning: true,
                theme: 'tooltipster-shadow',
                side: 'right',
                maxWidth: '765'
            });
});
$('INPUT[type="file"]').change(function () {
    $('.previous_resume').hide('slow');
    var ext = this.value.match(/\.(.+)$/)[1];
    if(this.files[0].size > 3000000){
        alert('This file size should not be greater than 3MB.');
        this.value = ''; 
    }else{
        switch (ext) {
        case 'pdf':
        case 'doc':
        case 'docx':
            $('#uploadButton').attr('disabled', false);
            break;
        default:
            alert('This file type is not allowed.');
            this.value = '';
    }
    }
    if($(this).val().length > 0){
        $( ".loader" ).removeClass('hidden');
        $( ".info" ).addClass('hidden');
        setTimeout(function () { 
            $( ".loader" ).addClass('hidden');
            $( ".done" ).removeClass('hidden');
         }, 5000);
    }
});
</script>
@endsection