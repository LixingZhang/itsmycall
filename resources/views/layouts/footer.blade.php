
<!-- main-footer -->
<footer class="main-footer">


    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="list-inline link-footer text-center-xs">
                    <li><a href="/">Home</a></li>
                    <li><a href="/job_list">Find a Job</a></li>
                    <li><a href="/poster/job_post">Post a Job</a></li>
                    <li><a href="/aboutus">About Us</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                    <li><a href="/terms-conditions">Website Terms of Use</a></li>
                    <li><a href="/privacy">Privacy Policy</a></li>
                    <li><a href="http://itsmycall.zendesk.com/hc/en-us" target="_blank">Support</a></li>
                </ul>
            </div>
            <div class="col-sm-12 ">
                <p class="text-center-xs hidden-lg hidden-md hidden-sm">Stay Connected</p>
                <div class="socials text-center" style="margin-top:25px">
                    <a target="_blank" href="{{$settings_social->fb_page_url}}"><i class="fa fa-facebook" style="line-height: 2.2!important;"></i></a>
                    <a target="_blank" href="{{$settings_social->twitter_url}}"><i class="fa fa-twitter" style="line-height: 2.2!important;"></i></a>
                    <a target="_blank" href="{{$settings_social->youtube_channel_url}}"><i class="fa fa-youtube-play" style="line-height: 2.2!important;"></i></a>
                    <a target="_blank" href="{{$settings_social->linkedin_page_url}}"><i class="fa fa-linkedin" style="line-height: 2.2!important;"></i></a>
                </div>
                <div style="text-align:center; margin-top:25px; margin-bottom:-5px;">
                    <b>Proudly partnered by:</b>
                </div>
                <div style="text-align:center;">
                    <a href="http://cxcentral.com.au" target="_blank">
                        <img src="/ccctopnew.png" alt="logo" style="width:135px;padding-bottom: 20px;"/>
                    </a>
                    <a href="https://www.auscontact.com.au/" target="_blank">
                        <img src="/actop.png" alt="logo" style="width:150px; padding-bottom: 15px;"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</footer><!-- end main-footer -->