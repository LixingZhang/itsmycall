<!-- main-header -->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<style type="text/css">
    @if(Auth::check())
    @if(Auth::user()->isPoster())
    .main-navbar .navbar-nav > li > a{
            padding: 20px 6px;
    }
    a.link-profile {
        padding-left: 0px !important;
    }
    @endif
    @if(Auth::user()->isCustomer())
    .main-navbar .navbar-nav > li > a{
            padding: 20px 15px;
    }
    a.link-profile {
        padding-left: 0px !important;
    }
    @endif
    @else
    .main-navbar .navbar-nav > li > a{
            padding: 20px 15px;
    }
    .dropdown-submenu {
    position: relative;
    }

    .dropdown-submenu .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }
    @media (min-width: 768px) {
      .main-navbar .dropdown-menu:before {
        content: "";
        display: inline-block;
        position: absolute;
        top: -10px;
        left: 22px;
        bottom: auto;
        border: 0 solid transparent;
        border-top: 0;
        border-bottom-color: rgba(0, 0, 0, 0.1);
      }
      .hidetop {
          display:none;
      }
      .main-navbar .dropdown-menu:after {
        content: "";
        display: inline-block;
        position: absolute;
        top: -9px;
        left: 23px;
        bottom: auto;
        border: 0 solid transparent;
        border-top: 0;
        border-bottom-color: #FFF;
      }
    }
    @endif
</style>
<header class="main-header">
    <div class="container">
        <nav class="mobile-nav hidden-md hidden-lg">
            <a href="#" class="btn-nav-toogle first">
                <span class="bars"></span>
                Menu
            </a>

            <div class="mobile-nav-block">
                <h4>Navigation</h4>
                <a href="#" class="btn-nav-toogle">
                    <span class="barsclose"></span>
                    Close
                </a>

                <ul class="nav navbar-nav">
                    @if(Auth::check())
                    @if(Auth::user()->isPoster())
                    <li class=""><a href="/poster"><strong>Home</strong></a></li>
                    <li class=""><a href="/poster/job_post"><strong>Advertise Job</strong></a></li>
                    <li class=""><a href="/poster/buy_credits"><strong>Buy Credits</strong></a></li>
                    <li><strong><a style="text-decoration: none;">View my Jobs</a></strong></li>
                            <ul style="text-decoration: none; list-style-type: none; padding-left: 25px;">
                                <li><a href="/poster">Active jobs</a></li>
                                <li><a href="/poster/inactive">Inactive jobs</a></li>
                                <li><a href="/poster/expired">Expired jobs</a></li>
                                <li><a href="/poster/applicants">View Applicants</a></li>
                            </ul>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115004009668-What-is-the-Connect-Program-"><strong>Connect Program</strong></a></li>
                    <li class=""><a href="/poster/transaction"><strong>Transactions</strong></a></li>
                    <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support"><strong>Support/FAQ's</strong></a></li>
                    <li><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif
                    @if(Auth::user()->isCustomer())
                    <li class=""><a href="/customer"><strong>Home</strong></a></li>
                    <li class=""><a href="/"><strong>Search Jobs</strong></a></li>
                    <li><a href="/customer/job_alerts"><strong>Job Alerts</strong></a></li>
                    <li><a href="/customer/savedjobs"><strong>Saved Jobs</strong></a></li>
                    <li><a href="/customer/appliedjobs"><strong>Jobs you've applied for</strong></a></li>
                    <li><a href="/customer/connect-program"><strong>Join our Connect Program</strong></a></li>
                    <li><strong><a style="text-decoration: none;">Profile</a></strong></li>
                            <ul style="text-decoration: none; list-style-type: none; padding-left: 25px;">
                                <li><a href="/customer/edit-user">Edit profile</a></li>
                                <li><a href="/customer/change_password">Change Password</a></li>
                                <li><a href="/customer/delete-account" onclick="return confirm('Are you sure you want to delete your account? This will:\n\n  * Remove any active Job Alerts you have set up\n  * Remove your records from the Inbound Program (if you had registered)\n  * Prevent you from being able to log back into your account\n\nPress OK to continue or Cancel to go back')">Delete Account</a></li>
                            </ul>
                    <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436708-Job-Seekers-Support"><strong>FAQ's</strong></a></li>
                    <li><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif
                    @else
                    <li class=""><a href="/"><strong>Home</strong></a></li>
                    <li class=""><a href="/register-user"><strong> Job Alerts</strong></a></li>
                    <li><strong><a href=""> Job Advertiser</a></strong>
                            <ul style="list-style-type: none; padding-left: 25px;">
                                <li><a href="/register-poster">Advertise a Job</a></li>
                                <li><a href="/connect-program">Search quality candidates</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-">Prices</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support">FAQ's</a></li>
                            </ul>
                    </li>
                    <li class=""><a href="/blogs"><strong> Blogs</strong></a></li>
                    <li class=""><a href="/connect-program"><strong>Connect Program</strong></a></li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-"><strong>Pricing</strong></a></li>
                    <li class=""><a href="/faq"><strong>FAQ's</strong></a></li>
                    <li class=""><a href="/aboutus"><strong>About Us</strong></a></li>
                    <li class=""><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif
                    @if(!Auth::check())
                    <li class="link-btn"><a href="/login">Login</a></li>
                    <li class="link-btn"><a href="/register">Register</a>
                    @else 
                    <li class="link-btn"><a href="/logout">Logout</a></li>
                    @endif

                </ul>
            </div>
        </nav>
    </div>
    <!-- main navbar -->
    <div class="container">
        <div class="pull-left">
            @if(strlen($settings_general->logo_120)>0)
            <a href="/">
                <img src="{{$settings_general->logo_120}}" alt="logo" style="height:100px; padding-top: 15px; padding-bottom: 15px;"/>
            </a>
            @endif
            <div><a href="tel:+61 1300 487 692" style="

                    font-weight: bold;
                    ">1300 ITSMYCALL (1300 487 692)</a>
            </div>
        </div>
        <div class="pull-right hidemobile" style="margin-top: -10px;">
            <div class="pull-left" style="position: relative;left: 140px;top: 15px;">
                <b>Proudly partnered by:</b>
            </div>
            <a href="http://cxcentral.com.au" target="_blank" style="text-decoration: none;">
                <img src="/ccctopnew.png" alt="logo" style="width:135px; padding-top: 10px; padding-bottom: 15px;"/>
            </a>
            <a href="https://www.auscontact.com.au/" target="_blank">
                <img src="/actop.png" alt="logo" style="width:150px; padding-top: 15px; padding-bottom: 15px;"/>
            </a>

        </div>
    </div>

    <nav class="navbar navbar-default main-navbar hidden-sm hidden-xs">

        <div class="container">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-right: 0;">


                <ul class="nav navbar-nav">
                    @if(Auth::check())
                    @if(Auth::user()->isPoster())
                    <li class=""><a href="/poster"><strong>Home</strong></a></li>
                    <li class=""><a href="/poster/job_post"><strong>Advertise Job</strong></a></li>
                    <li class=""><a href="/poster/buy_credits"><strong>Buy Credits</strong></a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" style="padding-left: 10px !important;"><strong>View my Jobs <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/poster">Active jobs</a></li>
                                <li><a href="/poster/inactive">Inactive jobs</a></li>
                                <li><a href="/poster/expired">Expired jobs</a></li>
                                <li><a href="/poster/applicants"><strong>View Applicants</strong></a></li>
                            </ul>
                    </li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115004009668-What-is-the-Connect-Program-"><strong>Connect Program</strong></a></li>
                    <li class=""><a href="/poster/transaction"><strong>Transactions</strong></a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" style="padding-left: 10px !important;"><strong>Contact <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support">Support/FAQ's</a></li>
                                <li><a href="/contact">Contact Us</a></li>
                            </ul>
                    </li>
                    @endif
                    @if(Auth::user()->isCustomer())
                    <li class=""><a href="/customer"><strong>Home</strong></a></li>
                    <li class=""><a href="/"><strong>Search Jobs</strong></a></li>
                    <li><a href="/customer/job_alerts"><strong>Job Alerts</strong></a></li>
                    <li><a href="/customer/savedjobs"><strong>Saved Jobs</strong></a></li>
                    <li><a href="/customer/appliedjobs"><strong>Jobs you've applied for</strong></a></li>
                    <li><a href="/customer/connect-program"><strong>Join our Connect Program</strong></a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" style="padding-left: 10px !important;"><strong>Contact <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436708-Job-Seekers-Support">Support/FAQ's</a></li>
                                <li><a href="/contact">Contact Us</a></li>
                            </ul>
                    </li>
                    @endif
                    @else
                    <li class=""><a href="/"><strong>Home</strong></a></li>
                    <li class=""><a href="/register-user"><strong>Job Alerts</strong></a></li>
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" style="padding-left: 20px !important;"><strong>Job Advertiser <i class="fa fa-caret-down" aria-hidden="true"></i></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/register-poster">Advertise a Job</a></li>
                                <li><a href="/connect-program">Search quality candidates</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-">Prices</a></li>
                                <li><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support">FAQ's</a></li>
                            </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown"><strong> Blogs <span class="caret"></span></strong></a>
                        <ul class="dropdown-menu">
                        <?php $categories = \DB::table('blogcategories')->get(); ?>
                          @foreach($categories as $category)
                          <li class="dropdown-submenu">
                            <a tabindex="-1" href="#">{{$category->title}} <span class="caret"></span></a>
                            <?php $blogsubcategories = \DB::table('blogsubcategories')->where('parent_category',$category->id)->get(); ?>
                            <ul class="dropdown-menu">
                              @foreach($blogsubcategories as $subcategory)
                              <li><a tabindex="-1" href="/blogs/{{$category->slug}}/{{$subcategory->slug}}">{{$subcategory->title}}</a></li>
                              @endforeach
                            </ul>
                          </li>
                          @endforeach
                        </ul>
                    </li>
                    <li class=""><a href="/connect-program"><strong>Connect Program</strong></a></li>
                    <li class=""><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us/articles/115002165468-What-are-your-prices-"><strong>Pricing</strong></a></li>
                    <li class=""><a href="/faq"><strong>FAQ's</strong></a></li>
                    <li class=""><a href="/aboutus"><strong>About Us</strong></a></li>
                    <li class=""><a href="/contact"><strong>Contact Us</strong></a></li>
                    @endif

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                    @if(Auth::user()->isPoster())
                    <li class="credits">
                    <a style="text-decoration: none;" href="/poster/transaction">Balance: ${{(Auth::user()->credits > 0) ? number_format(Auth::user()->credits, 2, '.', '') : '0.00'}}
                    </a> 
                    </li>
                    @endif
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="link-profile dropdown-toggle"  data-toggle="dropdown" >
                                @if(Auth::user()->first_name) 
                                <?php
                                $arr = explode(' ', trim(Auth::user()->first_name));
                                $namewrite = $arr[0];
                                 ?>
                                {{$namewrite}} 
                                @else Profile @endif <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu" role="menu">

                                @if(Auth::user()->isPoster())
                                <li><a href="/poster/edit-poster">Edit Profile</a></li>
                                <li><a href="/poster/change_password">Change Password</a></li>
                                <li><a href="/poster/delete-account" onclick="return confirm('Are you sure you want to delete your account? This will:\n\n    * Remove any active jobs you have on our website (Please note it is your responsibility to advise any candidates that may have applied.)\n    * Remove any remaining credits you have in your account\n    * Prevent you from being able to log back into your account so you will not be able to view previous jobs, applicants etc\n\nPress OK to continue or Cancel to go back.')">Delete Account</a></li>
                            @else
                                <li><a href="/customer/edit-user">Edit profile</a></li>
                                <li><a href="/customer/change_password">Change Password</a></li>
                                <li><a href="/customer/delete-account" onclick="return confirm('Are you sure you want to delete your account? This will:\n\n  * Remove any active Job Alerts you have set up\n  * Remove your records from the Inbound Program (if you had registered)\n  * Prevent you from being able to log back into your account\n\nPress OK to continue or Cancel to go back')">Delete Account</a></li>
                                @endif
                            </ul>
                        </li>
                        <li class="link-btn"><a href="/logout"><span class="btn btn-theme  btn-pill btn-xs btn-line">Logout</span></a></li>
                    </ul>
                    @else

                    <ul class="nav navbar-nav navbar-right">
                        <li class="link-btn"><a href="/login"><span
                                    class="btn btn-theme btn-pill btn-xs btn-line">Login</span></a></li>
                        <li class="link-btn"><a href="/register"><span class="btn btn-theme  btn-pill btn-xs btn-line">Register</span></a>
                        </li>
                    </ul>

                    @endif

                </ul>
            </div>
        </div>
    </nav>
    <!-- end main navbar -->

    <!-- mobile navbar -->

</header><!-- end main-header -->
<script type="text/javascript">
    $(document).ready(function(){
      $(".dropdown").hover(
        function() {
            $(this).children("ul").slideDown('fast');
        },
        function() {
            $(this).children("ul").slideUp('fast');
        }
    );
      $(".dropdown-submenu").hover(
        function() {
            $(this).children("ul").slideDown('fast');
        },
        function() {
            $(this).children("ul").slideUp('fast');
        }
    );
    });
</script>