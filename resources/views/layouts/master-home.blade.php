<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="author" content="{{$settings_general->site_url}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{$settings_seo->seo_description}}">
    <meta property="og:site_name" content="{{$settings_general->site_title}}">
    <meta property="og:type" content="article" /> 
    @if (isset($job))
        @if(!empty($job->job_image))
            <meta property="og:image" content="{{$job->job_image}}">
            <meta name="twitter:image" content="{{$job->job_image}}">
        @else
            <meta property="og:image" content="{{$settings_seo->default_job_image}}">
            <meta name="twitter:image" content="{{$settings_seo->default_job_image}}">
        @endif
            <meta name="twitter:card" content="summary" />
            <meta property="og:url" content="{{request()->url()}}">
            @if(!empty($job->title))
            <meta property="og:title" content="{{$job->title}}">
            <meta name="twitter:title" content="{{$job->title}}">
            @endif
            @if(!empty($job->short_description))
            <meta property="og:description" content="{{$job->short_description}}">
            <meta name="twitter:description" content="{{$job->short_description}}">
            @endif
    @elseif(isset($blog))
        <meta name="twitter:card" content="summary" />
        <meta property="og:url" content="{{request()->url()}}">
        <meta property="og:image" content="{{$blog->image}}">
        <meta name="twitter:image" content="{{$blog->image}}">
        <meta property="og:title" content="{{$blog->title}}">
        <meta name="twitter:title" content="{{$blog->title}}">
        <meta property="og:description" content="{{$blog->preview}}">
        <meta name="twitter:description" content="{{$blog->preview}}">
    @else
        <meta name="twitter:card" content="summary" />
        <meta property="og:image" content="{{$settings_seo->og_image}}">
        <meta name="twitter:image" content="{{$settings_seo->og_image}}">
        {!! $settings_seo->og_url !!}
        {!! $settings_seo->og_title !!}
        {!! $settings_seo->og_description !!}
        <meta name="twitter:title" content="ItsMyCall - Australia's own call centre jobs website">
        <meta name="twitter:description" content="Search for all contact centre and call centre jobs in Australia from agent to executive roles with over 59 jobs types!">
    @endif
    {!! $settings_seo->google_verify !!}

    {!! $settings_seo->bing_verify !!}

    {!! $settings_custom_css->custom_css !!}

    {!! $settings_seo->fb_app_id !!}
    
    {!! $settings_general->analytics_code !!}

    <title><?php if (!empty($page_title)) { print($page_title);} else { print($settings_general->site_title);}?></title>

    <!--favicon-->
    <link rel="apple-touch-icon" href="/assets/theme/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="{!! $settings_general->favicon !!}" type="image/x-icon">
    <!-- bootstrap -->
    <link href="/assets/plugins/bootstrap-3.3.2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="/assets/plugins/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- lightbox -->
    <link href="/assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/plugins/jquery.js"></script>
    <script src="/assets/plugins/jquery.easing-1.3.pack.js"></script>
    <!-- jQuery Bootstrap -->
    <script src="/assets/plugins/bootstrap-3.3.2/js/bootstrap.min.js"></script>
    <!-- Lightbox -->
    <script src="/assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Theme JS -->
    <script src="/assets/theme/js/theme.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/assets/plugins/multiple-select/multiple-select.css" />
    <link rel="stylesheet" href="/css/app.css" />
    @yield('extra_css')


    {!! $settings_custom_js->custom_js !!}

    <?php $desc = \App\Settings::where('category','live_js')->first();?>
    @if($desc)
    {!! $desc->value_txt !!}
    @endif

    @section('facebook_pixel')
        <?php $desc = \App\Settings::where('category','general')->where('column_key','facebook_pixel_tracking_code')->first();?>
        @if($desc)
        {!! $desc->value_txt !!}
        @endif
    @show

    <!-- Themes styles-->
    <link href="/assets/theme/css/theme.css" rel="stylesheet">
    <!-- Your custom css -->
    <link href="/assets/theme/css/theme-custom.css" rel="stylesheet">
    @yield('extra_css')
    <link rel="shortcut icon" href="/favicon.ico"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/assets/plugins/cleave/dist/cleave.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/multiple-select/multiple-select.js" type="text/javascript"></script>
</head>
<body>
@section('google_adwards')
    <?php
    if (isset($_GET['t'])):

    if ($_GET['t'] == 'MNNnsnsdin'): ?>
    <!-- Google Code for Find a Job Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 857367242;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "KV1jCLPD3m8QysXpmAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//www.googleadservices.com/pagead/conversion/857367242/?label=KV1jCLPD3m8QysXpmAM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>

    <?php   elseif ($_GET['t'] == '12sdsdknNuuBmnu') : ?>
    <!-- Google Code for Post A Job Conversion Tracking Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 857367242;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "twwSCI_D3m8QysXpmAM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//www.googleadservices.com/pagead/conversion/857367242/?label=twwSCI_D3m8QysXpmAM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>

    <?php endif;
    endif; ?>
@show
<!-- wrapper page -->
<div class="wrapper">

    @include('layouts.header-home')

    <!-- body-content -->
    <div class="body-content clearfix" >

       @yield('content')

    </div><!--end body-content -->


    @include('layouts.footer')

</div><!-- end wrapper page -->

<!-- maps -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBmOmEA3LExX-ak1b-Y9EaUGV598LRZb9M"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
<script src="/assets/plugins/gmap3.min.js"></script>
<!-- maps single marker -->
<script src="/assets/theme/js/map-detail.js"></script>
@yield('extra_js')
<script>
  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 300000,
      values: [ 30000, 100000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });
</script>
</body>
</html>
