@extends('layouts.master')

@section('extra_js')
	@if( ! empty($seo_keywords))
    	<meta name="keywords" content="{{$seo_keywords}}">
	@endif
	@if( ! empty($seo_description))
    	<meta name="description" content="{{$seo_description}}">
	@endif
@endsection

@section('google_adwards')
	{!! $google_adwards_tracking_code !!}
@endsection

@section('facebook_pixel')
	{!! $facebook_pixel_tracking_code !!}
@endsection

@section('content')
	<div class="container">
		<div class="clearfix">
			{!! $section_one !!}
		</div>
		@if($video)
		<div class="clearfix">
			@if($autoplay == 1)
			<div class="col-sm-12">
			<iframe width="100%" height="450" src="//www.youtube.com/embed/{{$video}}?autoplay=1">
            </iframe>
            </div>
            @else
            <div class="col-sm-12">
            <iframe width="100%" height="450" src="//www.youtube.com/embed/{{$video}}">
            </iframe>
            </div>
            @endif
		</div>
		@endif
		<div class="clearfix">
		@if(count($testimonials) > 0)
			<div class="col-sm-6">
				<div class="col-sm-12">
					<h2>Testimonials</h2>
				</div>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="{{ $testimonial_interval }}">
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
						@foreach ($testimonials as $key => $testimonial)
							@if ($key == 0)
							  <div class="item active">
							@else
							  <div class="item">
							@endif
						    	<div class="col-sm-4 text-center">
						    		<img src="/uploads/images/{{ $testimonial->image }}" style="width: 100%; max-width:300px; margin: 5px 0;">
						    	</div>
						    	<div class="col-sm-8">
						    		<div>
							    		{!! $testimonial->html !!}
						    		</div>
					    		</div>
					    	</div>
						@endforeach
				  </div>
				</div>
			</div>
			<div class="col-sm-6">
				{!! $section_two !!}
			</div>
		@else
			{!! $section_two !!}
		@endif
		</div>
		<div class="clearfix">
			{!! $section_three !!}
		</div>
		<div class="clearfix">
			{!! $section_four !!}
		</div>
		<div class="clearfix">
			{!! $section_five !!}
		</div>
		<div class="clearfix">
			{!! $section_six !!}
		</div>
		<div class="clearfix">
			{!! $section_seven !!}
		</div>
		<div class="clearfix">
			{!! $section_eight !!}
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title text-center" id="myModalLabel">Download our free Job Advertisers Guide!</h4>
		      </div>
		      <div class="modal-body">
		        <form class="Download" method="POST">
						  <div class="form-group">
						  	<label for="firstname">First name:</label>
						  	<input type="text" class="form-control" id="firstname" name="firstname" required>
						  </div>
						  <div class="form-group">
						    <label for="lastname">Last name:</label>
						    <input type="text" class="form-control" id="lastname" name="lastname" required>
						  </div>
						  <div class="form-group">
						    <label for="company">Company:</label>
						    <input type="text" class="form-control" id="company" name="company" required>
						  </div>
						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" class="form-control" id="email" name="email" required>
						  </div>
						  <div class="form-group">
						    <label for="number">Phone number:</label>
						    <input type="tel" class="form-control" id="number" name="number" required >
						  </div>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						  <button type="submit" class="btn btn-primary">Download</button>
						</form>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
                                   <input type="hidden" class="googledownload" value="{!! $google_download !!}" name="">
                                   <input type="hidden" class="pixeldownload" value="{!! $facebook_download !!}" name="">
                                   <input type="hidden" class="googleregister" value="{!! $google_register !!}" name="">
                                   <input type="hidden" class="pixelregister" value="{!! $facebook_register !!}" name="">
<script type="text/javascript">
		function register() {
				if($('.googleregister').val()){
			        $googleregister = $('.googleregister').val();
			        eval($googleregister);
			    }
				if($('.pixelregister').val()){
			        $pixelregister = $('.pixelregister').val();
			        eval($pixelregister);
			    }
	}
</script>
@endsection


@section('extra_js')
<script>
	$(document).ready(function () {
		$('.Download').on('submit', function (e) {
			e.preventDefault()
			var data = {};
			$.each($(this).serializeArray(), function (index, value) {
				data[value.name] = value.value
			})
			var element;
			var modal = $('.modal-body');
			modal.find('.alert').remove();
			$.ajax({
				method: 'POST',
				url: "/download",
				data: data
			}).done(function (response) {
				element = createAlert('alert-success', response.message)
				if($('.googledownload').val()){
			        $googledownload = $('.googledownload').val();
			        eval($googledownload);
			    }
				if($('.pixeldownload').val()){
			        $pixelregister = $('.pixeldownload').val();
			        eval($pixelregister);
			    }
			}).fail(function (response) {
				element = createAlert('alert-danger', response.responseJSON.error)
      }).always(function () {
				modal.prepend(element);
      });
		});
	});

	function createAlert(alertClass, message) {
		var alert = $([
			'<div class="alert alert-dismissible" role="alert">',
			  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
			'</div>'].join(''))

		return alert.html(alert.html() + message).addClass(alertClass);
	}

</script>
@endsection