<style>
    .modal {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, 1 ) 
            url('/assets/img/default.gif') 
            50% 50% 
            no-repeat;
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    body.loading {
        overflow: hidden;   
    }

    /* Anytime the body has the loading class, our
       modal element will be visible */
    body.loading .modal {
        display: block;
    }
</style>
<h2 class="orangeudnerline">Checkout Page</h2>
<?php $discount_class = ''; ?>
<input type="hidden" id="aus_contact_member_no" value="" />
<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-6">
            @include('admin.layouts.notify')
        </div>
    </div>
    <?php
    $end = \Carbon\Carbon::now()->toDateString();
    $codecheck = App\ReferralCode::where('id',Auth::user()->ref_id)->first(); 
    ?>
    @if(!empty($codecheck) && $codecheck->date_end >= $end && $codecheck->exclusive == 'yes')
        <div class="col-md-6" style="margin-bottom: 15px">
            <div class="input-group">
                <input type="text" id="coupon" name="coupon" class="form-control" style="    height: 34px;" placeholder="Enter Coupon Code....">
                <span class="input-group-btn">
                    <a class="btn btn-default" onclick="referralcheck()">Go!</a>
                </span>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 15px">
            <p id="couponresponse"></p>
            <p style="display: none; color: red;" class="displaygo">Sorry, Coupon Codes cannot be combined with your Registration Code.</p>
        </div>
        <script type="text/javascript">
            function referralcheck() {
            $('.displaygo').css('display','');
          };
        </script>
    @else
        <div class="col-md-6" style="margin-bottom: 15px">
            <div class="input-group">
                <input type="text" id="coupon" name="coupon" class="form-control" style="    height: 34px;" placeholder="Enter Coupon Code....">
                <span class="input-group-btn">
                    <button class="btn btn-default" id="couponbtn" type="button">Go!</button>
                </span>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 15px">
            <p id="couponresponse"></p>
        </div>
    @endif
    <div class="col-md-12">

        <div id="step6_box" class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Order Summary</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed" style="margin-bottom: 0;">
                        <thead>
                            <tr style="background-color: #639941; color: white;">
                                <td style="padding: 8px;"><strong>Item</strong></td>
                                <td class="text-center" style="padding: 8px;"><strong>Standard Price</strong></td>
                                <td class="text-center" style="padding: 8px;"><strong>Quantity</strong></td>
                                <td class="text-right" style="padding: 8px;"><strong>Totals</strong></td>
                            </tr>
                        </thead>
                        <tbody id="ordertable">
                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <tbody id="">
                            <tr>
                                <td id="step6_QOTHERNAME">Registration Code Discount</td>
                                <td class="text-center"></td>
                                <td class="text-right" id="step6_QOTHER"></td>
                                <td class="text-right" id="step6_DISCOTHER">$0</td>
                            </tr>
                            <tr>
                                <td>Coupon Discount</td>
                                <td class="text-center"></td>
                                <td class="text-right" id="step6_Q"></td>
                                <td class="text-right" id="step6_DISC">$0</td>
                            </tr>
                            <tr>
                            <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                                    if ($referralcode) {
                                    $end = \Carbon\Carbon::now()->toDateString();
                                     $expires = new \Carbon\Carbon($referralcode->date_end);
                                     $date = $expires->toFormattedDateString();
                                     $now = \Carbon\Carbon::now();
                                     $difference = ($expires->diff($now)->days);
                                    }
                                ?> 
                                @if(count($referralcode) > 0 && $referralcode->credit_amount == 0)
                                @if($referralcode->date_end >= $end)
                                <td>
                                <p style="margin:0;">Days remaining until your Registration Discount expires: {{$difference}} days</p>
                                </td>
                                <td class="text-center"></td>
                                <td class="text-right"></td>
                                <td class="text-right"></td>
                                @elseif(empty(Auth::user()->referral_viewed))
                                <td>
                                <p style="margin:0;">Your Registration Code expired on {{$date}}.</p>
                                </td>
                                <td class="text-center"></td>
                                <td class="text-right"></td>
                                <td class="text-right"></td>
                                @endif
                                 @endif
                            </tr>
                            <tr class="creditsused hidden">
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-right"><strong>Credits Applied</strong></td>
                                <td class="no-line text-right" id="step6_CREDITSUSED"></td>
                            </tr>
                            <tr>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-right"><strong>Sub Total</strong></td>
                                <td class="no-line text-right" id="step6_SUB"></td>
                            </tr>
                            <tr>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-right"><strong>GST (10%)</strong></td>
                                <td class="no-line text-right" id="step6_GST"></td>
                            </tr>
                            <tr class="ccrow">
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-right"><strong>Credit Card Fee</strong></td>
                                <td class="no-line text-right" id="step6_CCFEE"></td>
                            </tr>
                            <tr>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-right"><strong style="font-size: 18px;">Total</strong></td>
                                <td class="no-line text-right" id="step6_total_price"></td>
                            </tr>
                            <tr class="cremain hidden">
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-right"><strong>Credits Remaining After Purchase</strong></td>
                                <td class="no-line text-right" id="step6_REMAINING"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <ul class="list-group hidden">
                    <h4 class="orangeudnerline text-left">Items</h4>
                    <li class="list-group-item">Listing Type: <span class="package_name pull-right"></span></li>
                    <li class="list-group-item upgrades hidden">Listing Upgrades: <strong><span class="add-upgrade-title pull-right"></span></strong></li>
                    <li class="list-group-item add-package"  style="display: none;">Credits Package: <strong><span class="add-package-title pull-right" style="font-size: 10px;"></span></strong> </li>
                    <li class="list-group-item use-credits hidden" class="use-credits hidden">Credits Used</li>
                    <li class="list-group-item rest-credits hidden" class="rest-credits hidden">Credits balance after purchase: <strong><span class=" pull-right"  id="rest_credits"></span></strong></li>
                    <li class="list-group-item discount" style="border-bottom:0px!important;">Auscontact Member Discount: <strong><span class=" pull-right" id="step6_aus_member_discount"></span></strong><br><br><small><a target="_blank" href="https://www.auscontact.com.au/member-services/membership-categories-and-fees">Not a member? Click here to join</a></small></li>
                    <li class="list-group-item discount" style="border-bottom:0px!important;"><div class="input-group">
                            <input type="text" class="form-control" style="    height: 34px;" placeholder="Coupon Code...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                        </div><!-- /input-group --></li>
                    <h4 class="orangeudnerline text-left">Totals</h4>
                    <li class="list-group-item total">Coupon Discount: <strong><span id="step6_DISC" class="pull-right">None</span></strong></li>

                    <li class="list-group-item total">GST: <strong><span id="step6_GST" class="pull-right"></span></strong></li>
                    <li class="list-group-item total" ><strong>Outstanding balance: <span id="step6_total_price" class="pull-right"></span></strong></li>

                </ul>
            </div>
        </div>
    </div>
        <div style="clear: both;"></div>
    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Billing Details</h3>
            </div>
            <div class="panel-body">
            <form id="billing-form" class="form-horizontal form-bordered">

                    <div class="form-group" style="margin-top: 15px;">
                        <label for="company" class="col-sm-4 control-label required">Name of company to send invoice to</label>
                        <div class="col-sm-8">
                            <input id="billing_company" class="form-control" type="text" name="billing_company" placeholder="Billing Company Name" value="{{Auth::user()->company}}" />
                        </div>
                    </div>
                    
                    <div class="form-group" style="margin-top: 15px;">
                        <label for="company" class="col-sm-4 control-label required">Billing First Name</label>
                        <div class="col-sm-8">
                            <input id="billing_name" class="form-control" type="text" name="billing_name" placeholder="Billing First Name" value="{{Auth::user()->first_name}}"/>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 15px;">
                        <label for="company" class="col-sm-4 control-label required">Billing Last Name</label>
                        <div class="col-sm-8">
                            <input id="billing_last_name" class="form-control" type="text" name="billing_last_name" placeholder="Billing Last Name" value="{{Auth::user()->last_name}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-4 control-label required">Billing Phone Number</label>
                        <div class="col-sm-8">
                            <input id="billing_phone" class="form-control" type="text" name="billing_phone" placeholder="Billing Phone Number" @if(Auth::user()->billing_number) value="{{Auth::user()->billing_number}}" @else value="{{Auth::user()->phone}}" @endif />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-4 control-label required">Billing Email</label>
                        <div class="col-sm-8">
                            <input id="billing_email" class="form-control" type="text" name="billing_email" placeholder="Billing Email" @if(Auth::user()->billing_email) value="{{Auth::user()->billing_email}}" @else value="{{Auth::user()->email}}" @endif />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-4 control-label required">Billing Reference</label>
                        <div class="col-sm-8">
                            <input id="billing_ref" onkeyup="countChar1(this)" maxlength="20" class="form-control" type="text" name="billing_ref" placeholder="Your Billing Reference" value=""/>
                            <span id="charNum1" style="color: #639941; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-4 control-label">ABN</label>
                        <div class="col-sm-8">
                            <input id="abn" class="form-control" type="text" name="abn" placeholder="ABN" value="{{Auth::user()->billing_abn}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="company" class="col-sm-4 control-label required">Billing Address</label>
                        <div class="col-sm-8">
                            <small><i class="fa fa-info-circle"></i> We'll remember these details for next time.</small>
                            <input id="billing_address" class="form-control geocomplete" type="text" name="billing_address" placeholder="Billing Address" value="{{Auth::user()->billing_address}}" onfocus="geolocate()" />
                        </div>
                        <div class="col-sm-8 col-sm-offset-4" style="padding-top: 5px;">
                            <input class="form-control" id="street2" type="text" name="billing_street" placeholder="Street Number" value="{{Auth::user()->billing_street}}" />
                        </div>
                        <div class="col-sm-8 col-sm-offset-4" style="padding-top: 5px;">
                            <input class="form-control" id="house2" type="text" name="billing_house" placeholder="Street Address" value="{{Auth::user()->billing_house}}" />
                        </div>
                        <div class="col-sm-8 col-sm-offset-4" style="padding-top: 5px;">
                            <input class="form-control" id="city2" type="text" name="billing_city" placeholder="City/Town" value="{{Auth::user()->billing_city}}" />
                        </div>
                        <div class="col-sm-8 col-sm-offset-4" style="padding-top: 5px; ">
                            <input id="stateselectbill" class="form-control" placeholder="State" type="text" name="billing_state" value="{{Auth::user()->billing_state}}">
                        </div>
                        <div class="col-sm-8 col-sm-offset-4" style="padding-top: 5px;">
                            <input class="form-control" id="postcode2" name="billing_zipcode" placeholder="Postcode" value="{{Auth::user()->billing_zipcode}}" />
                        </div>
                        <div class="col-sm-8 col-sm-offset-4" style="padding-top: 5px;">
                            <input class="form-control" type="text" name="" placeholder="Country" value="Australia" disabled />
                        </div>
                    </div>
                    <input type="hidden" id="NhibunsniiMNSK992" name="NhibunsniiMNSK992" value="0">
                    <input type="hidden" id="NhibunsniiMNSK992ID" name="NhibunsniiMNSK992ID" value="">
                    <input type="hidden" id="ZKONONOSDN3223NSK992" name="ZKONONOSDN3223NSK992" value="0">
                    <input type="hidden" id="rf09871280971" value="{{$rf09871280971}}">
                    <input type="hidden" id="rf09871280971EXC" value="{{$rf09871280971EXC}}">
                    <input type="hidden" id="ac09871280971" value="{{$settings->aus_contact_discount}}">
                    <input type="hidden" id="ac09871280971EXC" value="{{$settings->aus_contact_exclusive}}">
                    <input type="hidden" id="cf89073789" value="{{$settings->card_fee}}">
                    <input type="hidden" id="cf89073789old" value="{{$settings->card_fee}}">
                    <input type="hidden" id="rf09871280971NAME" value="">
                    <input type="hidden" id="ZZZnnusdnosnonjweoj" name="ZZZnnusdnosnonjweoj" value="">
                    <input type="hidden" id="assdsda23312312" name="assdsda23312312" value="">
                    <input type="hidden" id="uuuucMNNMNmmd" name="uuuucMNNMNmmd" value="">
                    <input type="hidden" id="adssdar32rfdsfdsf" name="adssdar32rfdsfdsf" value="">
                    <input type="hidden" id="fintot" value="">
                    <div id="addressblock">
                        <input type="hidden" id="street_number" name="street_number" value="{{Auth::user()->billing_street}}">
                        <input type="hidden" id="route" name="route" value="{{Auth::user()->billing_route}}">
                        <input type="hidden" id="locality" name="locality" value="{{Auth::user()->billing_city}}">
                        <input type="hidden" id="administrative_area_level_1" name="administrative_area_level_1" value="{{Auth::user()->billing_administrative_area}}">
                        <input type="hidden" id="postal_code" name="postal_code" value="{{Auth::user()->billing_postal_code}}">
                        <input type="hidden" id="country" name="country" value="Australia">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" id="jobpaymentitle">Payment</h3>
            </div>
            <div class="panel-body" style="margin-top: 15px;">
                <div class="col-md-12 text-center">
                </div>
                <div class="clearfix"></div>
                <div id="error" class="alert alert-danger hidden"></div>
                <form action="/poster/save_credit" class="form-horizontal form-bordered" method="POST" id="payment-form">
                    <span class="payment-errors"></span>
                    <div class="form-group" id="selectpayment">
                        <label class="col-sm-3 control-label required">Payment Option</label>
                        <div class="col-sm-8" style="padding-top: 10px;">
                            <select id="paymentopt" class="form-control" name="paymentopt" onchange="">
                                <option value="creditc">Credit Card</option>
                                <option value="eft">Pay By Invoice</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-details">
                    <div class="form-group">
                         <label class="col-sm-3 control-label required">Card Details</label>
                         <div class="col-sm-8" style="padding-top: 10px;">
                            <div class="card-js">
                                <input type="text" class="card-number" size="20" data-stripe="number" value="">
                                <input type="text" class="expiry-month" size="2" placeholder="MM" data-stripe="exp_month" value="">
                                <input type="text" class="expiry-year" size="2" placeholder="YY" data-stripe="exp_year" value="">
                                <input class="cvc" type="text" size="4" data-stripe="cvc" value="">
                            </div>
                            <a target="_blank" href="https://stripe.com/"><img style="float: right;padding-top: 15px; max-width: 150px;" src="/stripevali.png"></a>
                         </div>
                    </div>
                    </div>
                    <?php $current_term = \App\Settings::where('column_key','invoice_terms')->first(); ?>
                    <div id="invwarning" class="alert alert-warning hidden">You will receive an invoice within 24 hours requiring payment strictly within @if(Auth::user()->invoice_terms) {{Auth::user()->invoice_terms}} days @elseif($current_term && $current_term->value_string) {{$current_term->value_string}} days @else 7 days @endif as per our Job Advertiser Terms and Conditions <a target="_blank" href="https://itsmycall.com.au/advertiser-terms-conditions">available here.</div>
                    <input type="hidden" id="PostsPackages" name="posts_packages" value="1" />
                    <input type="hidden" id="PostsUpgrades" name="posts_upgrades" value="0" />
                    <input type="hidden" id="PackagesStats" name="packages_stats1" value="0" />
                    <input type="hidden" id="TotalPrice" name="summ" value="0" />
                    <input id="credits" name="credits" type="hidden" value="0" />
                    <input name="_token" id="token" type="hidden" value="{!! csrf_token() !!}" />

                    <button type="submit" class="submit btn btn-primary btn-block" value="Submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear: both;"></div>
<!-- 
<div class="pager">
  <ul><li>
<a href="#">Make a Payment</a>
</li></ul>
</div> -->
<div id="popup" class="hidden">
    <div class="body">
        <h2>Thank You!</h2>
        <p>Hi Justin, I need some copy for this. Something along the lines of.. thanks! Your job has been posted.</p>
        <a href="/">OK</a>
    </div>
</div>
<style>
    #popup {
        position: fixed;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.5);
        top: 0;
        left: 0;
        z-index: 9999;
    }
    #popup .body {
        position: fixed;
        top: 30%;
        left: 20%;
        width: 60%;
        height: 100px;
        font-size: 24px;
        background-color: #fff;
        color: #ff7200;
        text-align: center;
        line-height: 40px;
        border-radius: 5px;
    }
    .list-group-item {
        position: relative;
        display: block;
        padding: 10px 15px;
        margin-bottom: 0px;
        background-color: #fff;
        border-bottom: 1px solid #ddd;
        border-top: 0px solid #ddd;
        border-left: 0px solid #ddd;
        border-right: 0px solid #ddd;
    }
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<script>  
    function countChar1(val) {
        var maxLength = 20;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum1').text('Remaining Characters: '+length+'/20');
      };
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.
    var autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function initialize() {
        // Create the autocomplete object, restricting the search
        // to geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */
            (document.getElementById('billing_address')), {
                types: ['geocode'],
                componentRestrictions: {country: "au"}
            });
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
        });
    }
    // [START region_fillform]
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            //document.getElementById(component).value = '';
            //document.getElementById(component).disabled = false;
        }
        results = place.address_components;
        $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode2").val(value.long_name);
                    //console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city2").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    $("#stateselectbill").val(value.long_name);
                    $("#state").val(value.long_name);
                } else if (value.types[0] == 'street_number') {
                    $("#street2").val(value.long_name);
                } else if (value.types[0] == 'route') {
                    $("#house2").val(value.long_name);
                } 
            });
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }
    // [END region_fillform]
    // [START region_geolocation]
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = new google.maps.LatLng(
                    position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
    // [END region_geolocation]
</script>
<script type="text/javascript">
    window.onload=function(){ 
        initialize();
    };
</script>  

<script>
//    $(document).ready(function () {
//        function changeBillForm() {
//            console.log("hello");
//            console.log($('#paymentopt').val());
//            if ($('#paymentopt').val() == "creditc") {
//                $('.card-details').removeClass("hidden");
//                $('#invwarning').addClass("hidden");
//                $('#cf89073789').val($('#cf89073789old').val());
//                setTotalPrice();
//            } else if ($('#paymentopt').val() == "eft") {
//                $('.card-details').addClass("hidden");
//                $('#invwarning').removeClass("hidden");
//                $('#cf89073789').val(0);
//                setTotalPrice();
//            }
//
//        }
//    });
</script>