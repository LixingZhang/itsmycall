<style type="text/css">
 #step5_box td{
      border-right: 1px solid #ddd;
  }
.standout {
  border-radius: 25px !important;
}
</style>
<h2 class="text-center">Post job ads regularly? Save money with one of our Value Packs</h2>
<div class="box-list" id="step5_box">
  <div class="outerbox clearfix">
  <?php setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>
  <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
    if ($referralcode) {
        $end = \Carbon\Carbon::now()->toDateString();
        $expires = new \Carbon\Carbon($referralcode->date_end);
        $date = $expires->toFormattedDateString();
        $now = \Carbon\Carbon::now();
        $difference = ($expires->diff($now)->days);
    }
  ?>
  @if(!empty($referralcode) && $referralcode->date_end >= $end)
    <input type="hidden" class="ref_apply" name="" value="yes">
  @else
  <input type="hidden" class="ref_apply" name="" value="no">
  @endif
  @if(!empty($referralcode) && $referralcode->exclusive == 'no' && $referralcode->date_end >= $end)
  <input type="hidden" class="codeallow" name="" value="yes">
  @elseif(!empty($referralcode) && $referralcode->exclusive == 'partial' && $referralcode->date_end >= $end)
  <input type="hidden" class="codeallow" name="" value="yes">
  @else
  <input type="hidden" class="codeallow" name="" value="no">
  @endif
    @foreach($packages as $package)
    <div class="item">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-9">
            <h3 class="add-package-title" style="margin-top: 5px;">{{$package->package_name}}</h3>
              <?php $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
                          if ($referralcode) {
                              $end = \Carbon\Carbon::now()->toDateString();
                                    }
                                ?>  
                            @if($referralcode)                                 
                                @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                                    @if ($package->discount_percent > 0)
                                    <?php 
                                        $ref_disc = $package->price-($package->price*$referralcode->discount_rate/100);
                                        $increase = $package->price * ($package->discount_percent + 100)/100-$ref_disc;
                                        $perc_increase = $increase / $ref_disc * 100;
                                        $discfinal = $package->discount_percent;
                                    ?>
                                    <input type="hidden" value="{{ number_format($package->price * ($package->discount_percent + 100)/100, 0 ,'.', ',') }}" class="priceBonus">
                                    <p style="display: none;" class="add-package-price">
                                      Spend ${{$ref_disc}} and get a {{number_format($perc_increase, 0 ,'.', ',')}}% Bonus (${{ number_format($package->price * ($package->discount_percent + 100)/100, 0 ,'.', ',') }})
                                    </p>
                                        <div class="table-responsive">
                                          <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                              <thead>
                                                  <tr>
                                                      <td class="text-center"><strong>Price</strong></td>
                                                      <td class="text-center"><strong>Standard Price</strong></td>
                                                      <td class="text-center"><strong>Credits to your account</strong></td>
                                                      <td class="text-center"><strong>Bonus Value</strong></td>
                                                      <td class="text-center"><strong>Bonus Percentage</strong></td>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                              </tfoot>
                                              <tbody>
                                                  <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                          <tr>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price-($package->price*$referralcode->discount_rate/100))}}</span></td>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                              <td class="text-center"><span>{{ money_format('%.2n',$package->price * ($package->discount_percent + 100)/100) }}</span></td>
                                                              <td class="text-center"><span>{{ money_format('%.2n',($package->price * ($package->discount_percent + 100)/100)-$ref_disc) }}</span></td>
                                                              <td class="text-center"><span>{{number_format($perc_increase, 0 ,'.', ',')}}%</span></td>
                                                          </tr>
                                              </tbody>
                                          </table>
                                      </div>
                                    @else
                                    <?php 
                                        $ref_disc = $package->price-($package->price*$referralcode->discount_rate/100);
                                        $increase = $package->price-$ref_disc;
                                        $perc_increase = $increase / $ref_disc * 100;
                                    ?>
                                    <input type="hidden" value="{{$package->price}}" class="priceBonus">
                                    <p style="display: none;" class="add-package-price">
                                      Spend ${{$ref_disc}} and get a {{number_format($perc_increase, 0 ,'.', ',')}}% Bonus (${{ number_format($package->price, 0 ,'.', ',') }})
                                    </p>
                                      <div class="table-responsive">
                                          <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                              <thead>
                                                  <tr>
                                                      <td class="text-center"><strong>Price</strong></td>
                                                      <td class="text-center"><strong>Standard Price</strong></td>
                                                      <td class="text-center"><strong>Credits to your account</strong></td>
                                                      <td class="text-center"><strong>Bonus Value</strong></td>
                                                      <td class="text-center"><strong>Bonus Percentage</strong></td>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                              </tfoot>
                                              <tbody>
                                                  <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                          <tr>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price-($package->price*$referralcode->discount_rate/100))}}</span></td>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                              <td class="text-center"><span>{{ money_format('%.2n',$package->price-$ref_disc) }}</span></td>
                                                              <td class="text-center"><span>{{number_format($perc_increase, 0 ,'.', ',')}}%</span></td>
                                                          </tr>
                                              </tbody>
                                          </table>
                                      </div>
                                    @endif
                                @else 
                                  @if ($package->discount_percent > 0)
                                    <input type="hidden" value="{{ number_format($package->price * ($package->discount_percent + 100)/100, 0 ,'.', ',') }}" class="priceBonus">
                                    <p style="display: none;" class="add-package-price">
                                      Spend ${{$package->price}} and get a {{number_format($package->discount_percent, 0 ,'.', ',')}}% Bonus (${{ number_format($package->price * ($package->discount_percent + 100)/100, 0 ,'.', ',') }})
                                    </p>
                                      <div class="table-responsive">
                                          <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                              <thead>
                                                  <tr>
                                                      <td class="text-center"><strong>Price</strong></td>
                                                      <td class="text-center"><strong>Credits to your account</strong></td>
                                                      <td class="text-center"><strong>Bonus Value</strong></td>
                                                      <td class="text-center"><strong>Bonus Percentage</strong></td>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                              </tfoot>
                                              <tbody>
                                                  <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                          <tr>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                              <td class="text-center"><span>{{ money_format('%.2n',$package->price * ($package->discount_percent + 100)/100) }}</span></td>
                                                              <td class="text-center"><span>{{ money_format('%.2n',($package->price * ($package->discount_percent + 100)/100)-$package->price) }}</span></td>
                                                              <td class="text-center"><span>{{number_format($package->discount_percent, 0 ,'.', ',')}}%</span></td>
                                                          </tr>
                                              </tbody>
                                          </table>
                                      </div>
                                    @else
                                    <input type="hidden" value="{{$package->price}}" class="priceBonus">
                                    <p style="display: none;" class="add-package-price">
                                      Spend ${{$package->price}} and get ${{$package->price}} Credits
                                    </p>
                                      <div class="table-responsive">
                                          <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                              <thead>
                                                  <tr>
                                                      <td class="text-center"><strong>Price</strong></td>
                                                      <td class="text-center"><strong>Credits to your account</strong></td>
                                                  </tr>
                                              </thead>
                                              <tfoot>
                                              </tfoot>
                                              <tbody>
                                                  <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                          <tr>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                              <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                          </tr>
                                              </tbody>
                                          </table>
                                      </div>
                                    @endif
                                @endif
                            @else
                                @if ($package->discount_percent > 0)
                                <input type="hidden" value="{{ number_format($package->price * ($package->discount_percent + 100)/100, 0 ,'.', ',') }}" class="priceBonus">
                                <p style="display: none;" class="add-package-price">
                                      Spend ${{$package->price}} and get a {{number_format($package->discount_percent, 0 ,'.', ',')}}% Bonus (${{ number_format($package->price * ($package->discount_percent + 100)/100, 0 ,'.', ',') }})
                                    </p>
                                  <div class="table-responsive">
                                      <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                          <thead>
                                              <tr>
                                                  <td class="text-center"><strong>Price</strong></td>
                                                  <td class="text-center"><strong>Credits to your account</strong></td>
                                                  <td class="text-center"><strong>Bonus Value</strong></td>
                                                  <td class="text-center"><strong>Bonus Percentage</strong></td>
                                              </tr>
                                          </thead>
                                          <tfoot>
                                          </tfoot>
                                          <tbody>
                                              <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                      <tr>
                                                          <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                          <td class="text-center"><span>{{ money_format('%.2n',$package->price * ($package->discount_percent + 100)/100) }}</span></td>
                                                          <td class="text-center"><span>{{ money_format('%.2n',($package->price * ($package->discount_percent + 100)/100)-$package->price) }}</span></td>
                                                          <td class="text-center"><span>{{number_format($package->discount_percent, 0 ,'.', ',')}}%</span></td>
                                                      </tr>
                                          </tbody>
                                      </table>
                                  </div>
                                @else
                                <input type="hidden" value="{{$package->price}}" class="priceBonus">
                                <p style="display: none;" class="add-package-price">
                                      Spend ${{$package->price}} and get ${{$package->price}} Credits
                                </p>
                                  <div class="table-responsive">
                                      <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                          <thead>
                                              <tr>
                                                  <td class="text-center"><strong>Price</strong></td>
                                                  <td class="text-center"><strong>Credits to your account</strong></td>
                                              </tr>
                                          </thead>
                                          <tfoot>
                                          </tfoot>
                                          <tbody>
                                              <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                      <tr>
                                                          <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                          <td class="text-center"><span>{{money_format('%.2n',$package->price)}}</span></td>
                                                      </tr>
                                          </tbody>
                                      </table>
                                  </div>
                                @endif
                            @endif

                <br>
          <div class="add-package-description">
            Spend
                              @if($referralcode)                                 
                                @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                                    @if ($package->discount_percent > 0)
                                      {{money_format('%.2n',$package->price-($package->price*$referralcode->discount_rate/100))}}
                                    @else
                                      {{money_format('%.2n',$package->price-($package->price*$referralcode->discount_rate/100))}}
                                    @endif
                                @else 
                                  @if ($package->discount_percent > 0)
                                      {{money_format('%.2n',$package->price)}}
                                    @else
                                      {{money_format('%.2n',$package->price)}}
                                    @endif
                                @endif
                            @else
                                @if ($package->discount_percent > 0)
                                  {{money_format('%.2n',$package->price)}}
                                @else
                                  {{money_format('%.2n',$package->price)}}
                                @endif
                            @endif
            {!! $package->description !!}
          </div>
          </div>
          <div class="col-md-3" style="font-size: 20px; font-weight:400;margin-top: 10px; color:#078d00">
            <?php
              $referralcode = App\ReferralCode::where('id', Auth::user()->ref_id)->get()->first();
              if ($referralcode) {
              $end = \Carbon\Carbon::now()->toDateString();
              }
             ?>  
            @if($referralcode)                                 
              @if($referralcode->discount_rate > 0 && $referralcode->date_end >= $end)
                  @if ($package->discount_percent > 0)
                    <div class="standout">
                      <input id="{{$package->package_name}}" 
                        data-id="{{$package->id}}"
                        data-price="{{$package->price}}"
                        data-bonus="{{ ($package->discount_percent > 0) ? $package->price * $package->discount_percent / 100 : 0}}"
                        class="package"
                        name="packages_stats" type="radio" value="{{$package->id}}" />
                      <label style="font-size: 20px;" for="{{$package->package_name}}">Select {{money_format('%.2n',$package->price-($package->price*$referralcode->discount_rate/100))}}</label><br>
                      <p style="color: black; font-size: 12px; line-height: 1.2;">(Receive {{ money_format('%.2n',$package->price * ($package->discount_percent + 100)/100) }} in your account)</p>
                    </div>
                  @else
                     <div class="standout">
                      <input id="{{$package->package_name}}" 
                        data-id="{{$package->id}}"
                        data-price="{{$package->price}}"
                        data-bonus="{{ ($package->discount_percent > 0) ? $package->price * $package->discount_percent / 100 : 0}}"
                        class="package"
                        name="packages_stats" type="radio" value="{{$package->id}}" />
                      <label style="font-size: 20px;" for="{{$package->package_name}}">Select {{money_format('%.2n',$package->price-($package->price*$referralcode->discount_rate/100))}}</label>
                      <p style="color: black; font-size: 12px; line-height: 1.2;">(Receive {{ money_format('%.2n',$package->price) }} in your account)</p>
                    </div>
                  @endif
                @else
                  @if ($package->discount_percent > 0)
                    <div class="standout">
                      <input id="{{$package->package_name}}" 
                        data-id="{{$package->id}}"
                        data-price="{{$package->price}}"
                        data-bonus="{{ ($package->discount_percent > 0) ? $package->price * $package->discount_percent / 100 : 0}}"
                        class="package"
                        name="packages_stats" type="radio" value="{{$package->id}}" />
                      <label style="font-size: 20px;" for="{{$package->package_name}}">Select {{money_format('%.2n',$package->price)}}</label>
                      <p style="color: black; font-size: 12px; line-height: 1.2;">(Receive {{ money_format('%.2n',$package->price * ($package->discount_percent + 100)/100) }} in your account)</p>
                    </div>
                  @else
                    <div class="standout">
                      <input id="{{$package->package_name}}" 
                        data-id="{{$package->id}}"
                        data-price="{{$package->price}}"
                        data-bonus="{{ ($package->discount_percent > 0) ? $package->price * $package->discount_percent / 100 : 0}}"
                        class="package"
                        name="packages_stats" type="radio" value="{{$package->id}}" />
                      <label style="font-size: 20px;" for="{{$package->package_name}}">Select {{money_format('%.2n',$package->price)}}</label>
                      <p style="color: black; font-size: 12px; line-height: 1.2;">(Receive {{ money_format('%.2n',$package->price) }} in your account)</p>
                    </div>
                  @endif
              @endif
            @else
                @if ($package->discount_percent > 0)
                  <div class="standout">
                        <input id="{{$package->package_name}}" 
                          data-id="{{$package->id}}"
                          data-price="{{$package->price}}"
                          data-bonus="{{ ($package->discount_percent > 0) ? $package->price * $package->discount_percent / 100 : 0}}"
                          class="package"
                          name="packages_stats" type="radio" value="{{$package->id}}" />
                        <label style="font-size: 20px;" for="{{$package->package_name}}">Select {{money_format('%.2n',$package->price)}}</label>
                        <p style="color: black; font-size: 12px; line-height: 1.2;">(Receive {{ money_format('%.2n',$package->price * ($package->discount_percent + 100)/100) }} in your account)</p>
                  </div>
                @else
                  <div class="standout">
                      <input id="{{$package->package_name}}" 
                        data-id="{{$package->id}}"
                        data-price="{{$package->price}}"
                        data-bonus="{{ ($package->discount_percent > 0) ? $package->price * $package->discount_percent / 100 : 0}}"
                        class="package"
                        name="packages_stats" type="radio" value="{{$package->id}}" />
                      <label style="font-size: 20px;" for="{{$package->package_name}}">Select {{money_format('%.2n',$package->price)}}</label>
                      <p style="color: black; font-size: 12px; line-height: 1.2;">(Receive {{ money_format('%.2n',$package->price) }} in your account)</p>                      
                </div>
                @endif
            @endif
          </div>
        </div>
      </div>
    </div><!-- end item list -->
    @endforeach
  </div><!-- end item list -->
</div><!-- end item list -->