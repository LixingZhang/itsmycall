@extends('layouts.master')


@section('content')


    <div class="block-section "   style="padding-bottom:130px">
        <div class="container">
		 <div class="col-md-12 col-xs-12">
		<h2 class="orangeudnerline">Edit Profile</h2>
							</div>
            <div class="panel panel-lg">
                <div class="panel-body">
                        <div class="col-md-12  col-xs-12">

                            @include('admin.layouts.notify')

                            <!-- form login -->
                            <form action="/customer/editsave" method="POST">

                                {!! csrf_field() !!}
				
				                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="first-name"  value="{{ $user->first_name }}" class="form-control" placeholder="First Name">
                                </div>
								<div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="last-name"  value="{{ $user->last_name }}" class="form-control" placeholder="Last Name">
                                </div>
				                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="phone"  value="{{ $user->phone  }}" class="form-control" placeholder="Phone">
                                </div>								
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email"  value="{{ $user->email }}" class="form-control" placeholder="Your Email">
                                </div>	
                                
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="address" name="address"  value="{{ $user->address }}" class="form-control" placeholder="Your Address">
                                </div>	
                                
                                <div class="form-group">
                                    <label>Suburb</label>
                                    <input type="suburb" name="suburb"  value="{{ $user->city }}" class="form-control" placeholder="Your Suburb">
                                </div>	
                                
                                <div class="form-group">
                                    <label>State</label>
                                    <input type="postcode" name="state"  value="{{ $user->state }}" class="form-control" placeholder="Your State">
                                </div>	
                                
                                <div class="form-group">
                                    <label>PostCode</label>
                                    <input type="postcode" name="postcode"  value="{{ $user->postcode }}" class="form-control" placeholder="Your Postcode">
                                </div>	
                                
                                <div class="form-group">
                                    <label>Current Position</label>
                                    <input type="currentposition" name="currentposition"  value="{{ $user->currentposition }}" class="form-control" placeholder="Your Current Position">
                                </div>	
                                
                                <div class="form-group">
                                    <label>Time in Role</label>
                                    <input type="timeinrole" name="timeinrole"  value="{{ $user->timeinrole }}" class="form-control" placeholder="How long have you held this position?">
                                </div>	
                                
                                <div class="form-group">
                                    <label>Career Ambitions</label>
                                    <input type="ambitions" name="ambitions"  value="{{ $user->ambitions }}" class="form-control" placeholder="What are you career ambitions?">
                                </div>	
                                
                                <div class="form-group">
                                    <label>Salary Expectations</label>
                                    <input type="expected_salary" name="expected_salary"  value="{{ $user->expected_salary }}" class="form-control" placeholder="Whats your expected Salary?">
                                </div>
                               
                                <div class="form-group no-margin">
                                    <button class="btn btn-theme btn-lg btn-t-primary btn-block">Save</button>
                                </div>
                            </form><!-- form login -->

                        </div>
                    
                </div>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
$(function() {
  $('#is_aus_contact_member').on('change', function() {
    if ($('#is_aus_contact_member').val() == 'Yes') {
      $('#aus_contact_member_no').show();
      $('#aus_contact_member_no0').show();
      $('#aus_contact_member_no').focus();
	  $('#aus_contact_member_no01').hide();
    } else {
      $('#aus_contact_member_no').val('');
      $('#aus_contact_member_no').hide();
	  $('#aus_contact_member_no0').hide();
	  $('#aus_contact_member_no01').show();
    }
  });
});
</script>
@endsection