@extends('layouts.master')
@section('google_adwards')
    {!! $google_adwards_tracking_code !!}
@endsection

@section('facebook_pixel')
    {!! $facebook_pixel_tracking_code !!}
@endsection
@section('extra_js')
@if($keywords)
<meta name="keywords" content="{{$keywords}}">
@endif
@if($seodescription)
<meta name="description" content="{{$seodescription}}">
@endif

<!--<script type="text/javascript" src="/assets/js/countries.js"></script>-->
<script>//populateCountries("country", "state");</script>
<script>//populateCountries("country1", "state2");</script>
<script>


</script><script>
    var arCats = [];
    var arSubCats = [];
<?php foreach ($categories as $category) { ?>
        var oCatValKey = {
            id: "<?php echo $category->id; ?>",
            title: "<?php echo $category->title; ?>"
        };
        arCats.push(oCatValKey);

        var arSubCat = [];
    <?php foreach ($category->subcategory as $subcategory) { ?>
            var oSubCatValKey = {
                id: "<?php echo $subcategory->id; ?>",
                title: "<?php echo $subcategory->title; ?>"
            };
            arSubCat.push(oSubCatValKey);
    <?php } ?>

        arSubCats[oCatValKey.id] = arSubCat;
<?php }  ?>

    function populateSubCats(categoryElementId, subcategoryElementId) {
        var select = document.getElementById(categoryElementId);
        var value = select.options[select.selectedIndex].value;

        var subcategoryElement = document.getElementById(subcategoryElementId);
        subcategoryElement.length = 0;
        subcategoryElement.options[0] = new Option('All Sub Categories', '-1');
        subcategoryElement.selectedIndex = 0;

        var arSubCat = arSubCats[value];
        for (var i = 0; i < arSubCat.length; i++) {
            var objSubCat = arSubCat[i];
            subcategoryElement.options[subcategoryElement.length] = new Option(objSubCat.title, objSubCat.id);
            if (subcategoryElement.getAttribute('value') === objSubCat.id) {
                subcategoryElement.value = subcategoryElement.getAttribute('value');
            }
        }
    }

    function populateMainCats(categoryElementId, subcategoryElementId) {
        var categoryElement = document.getElementById(categoryElementId);
        categoryElement.length = 0;
        categoryElement.options[0] = new Option('Select Category', '-1');
        categoryElement.selectedIndex = 0;

        for (var i = 0; i < arCats.length; i++) {
            var objCat = arCats[i];
            categoryElement.options[categoryElement.length] = new Option(objCat.title, objCat.id);
        }
        if (categoryElement.getAttribute('value')) {
            categoryElement.value = categoryElement.getAttribute('value');
        }

        // Assigned all countries. Now assign event listener for the states.
        if (subcategoryElementId) {
            categoryElement.onchange = function () {
                populateSubCats(categoryElementId, subcategoryElementId);
            };
            //populateSubCats(categoryElementId, subcategoryElementId);
        }
    }

//        $('#searchour').on('change', function () {
//            if ($('#searchour').is(':checked')) {
//                $('.salrwapperannual').addClass("hidden")
//                $('.hourlywapperannual').removeClass("hidden")
//            } else {
//                $('.hourlywapperannual').addClass("hidden")
//                $('.salrwapperannual').removeClass("hidden")
//            }
//        });



    $(function () {
        populateMainCats("category", "subcategory");
        //result.address_components[0]
        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    console.log("Hello");
                    $("#state2").val(value.long_name).change();
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            console.log(result.geometry.location.lng());
            console.log(result);


        });

        $("#find").click(function () {
            $("#joblocation").trigger("geocode");
        });

        $('#find').keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                $("#joblocation").trigger("geocode");
            }
        });


        $("#examples a").click(function () {
            $("#joblocation").val($(this).text()).trigger("geocode");
            return false;
        });

    });


</script>

@stop
    <style type="text/css">
        .alert{
                margin-bottom: 3px !important;
        }
        .helo{
            background-color: #E5FFEC;
            padding-top: 15px;
            border: 2px solid;
        }
        .featured-img{
            width: 100px;
            margin-bottom: 10px;
        }
        .featured-heading{
            color: #1B6008;
            margin: 0;
            padding-top: 10px;
        }
        .job-list-label{
            color: #66A866;
        }
        .job-list-text{
            font-size: 14px;
        }
        .list-group-item{
            border-radius: 0px !important; 
        }
        .list-group-item{
            border:0 !important;
        }
        .standard{
            min-height: 190px !important;
        }
        .tooltip_templates { display: none; }
        @media only screen and (max-width: 990px) {
            .standard{
            min-height: initial !important;
            }
            .standardline{
            position: initial !important;
            }
        }
}
    </style>
    <link rel="stylesheet" href="/css/tooltipster.bundle.min.css">
    <link rel="stylesheet" href="/css/tooltipster-sideTip-shadow.min.css">
    <link rel="stylesheet" href="/css/tooltipster-sideTip-light.min.css">
    <link rel="stylesheet" href="/css/tooltipster-sideTip-borderless.min.css">

<?php

use App\Categories;
use App\SubCategories;
use Illuminate\Support\Str;
?>
@section('content')
<div class="bg-color2">
    <div class="container">

        <!-- form search area-->
        <div class="row">
            <div class="col-md-12" style="margin-top:30px">
                @foreach($ads as $ad)
                @if($ad->position=='above_page')
                {!! $ad->code !!}
                @endif
                @endforeach
                <!-- form search -->
            </div>
        </div>


        <div class="row">

            <div class="col-md-12">


                <h2>{{$searchheading}}</h2>
                {!!$html_notes!!}
                @include('admin.layouts.notify')

                @include('filter-form-searchpage')
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" style="margin-top: 15px;">
                        <p class="text-left">
                            @if(sizeof($jobs)>0)
                            Displaying <?php echo sizeof($jobs); ?> Results
                            @else
                            No Results found
                            @endif
                        </p>
                        <!-- pagination -->


                    </div>
                    <div class="col-sm-6" style="margin-top: 15px;">
                        <nav>
                            @if(sizeof($jobs)>0)
                            <ul class="pagination pagination-theme  no-margin pull-right" style='margin-top:-30px!important'>
                                {!! $jobs->render() !!}
                            </ul>
                            @endif
                        </nav>
                    </div>
                </div><!-- end desc top -->
                <?php 
                $business_info = \App\Settings::where('column_key','business_info')->first();
                $home_info = \App\Settings::where('column_key','home_info')->first();
                $agreement_info = \App\Settings::where('column_key','agreement_info')->first();


                $standard_info = \App\Settings::where('column_key','standard_info')->first();
                $exten_info = \App\Settings::where('column_key','exten_info')->first();
                $parent_info = \App\Settings::where('column_key','parent_info')->first();
                $after_info = \App\Settings::where('column_key','after_info')->first();
                $night_info = \App\Settings::where('column_key','night_info')->first();
                $rotno_info = \App\Settings::where('column_key','rotno_info')->first();
                $rotyes_info = \App\Settings::where('column_key','rotyes_info')->first();

                $business_info_img = \App\Settings::where('column_key','business_info_img')->first();
                $home_info_img = \App\Settings::where('column_key','home_info_img')->first();
                $agreement_info_img = \App\Settings::where('column_key','agreement_info_img')->first();

                $standard_info_img = \App\Settings::where('column_key','standard_info_img')->first();
                $exten_info_img = \App\Settings::where('column_key','exten_info_img')->first();
                $parent_info_img = \App\Settings::where('column_key','parent_info_img')->first();
                $after_info_img = \App\Settings::where('column_key','after_info_img')->first();
                $night_info_img = \App\Settings::where('column_key','night_info_img')->first();
                $rotno_info_img = \App\Settings::where('column_key','rotno_info_img')->first();
                $rotyes_info_img = \App\Settings::where('column_key','rotyes_info_img')->first();

                ?>
                <!-- box list -->
                <div class="box-list">
                    @if(sizeof($jobs)==0)
                    <h1>No results found</h1>
                    @else
                    @foreach($jobs as $job)
                    <br>
                    @if( $job->posts_packages == 1)
                    <div class="row helo" style="margin:0;">
                        <div class="col-md-4 col-md-push-8 ">
                                    <img class="featured-img" src="/featured.png" style="width: 140px">
                                    <small style="color: grey; float: right; padding-right: 15px; padding-top: 2px;">Posted {{$job->created_at->diffForHumans()}}</small>
                                </div>
                            <div class="col-md-8 col-md-pull-4">
                                <?php 
                                  $category =  \App\Categories::find($job->category);
                                  $sub_category =  \App\SubCategories::find($job->subcategory);
                                ?>
                                @if($category && $sub_category)
                                <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>
                                @else
                                <a href="/{{$job->slug}}" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>
                                @endif
                                        @if($job->work_from_home == 1) 
                                        @if($business_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}"><img src="{{$business_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$business_info->id!!}">
                                                {!!$business_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 2) 
                                        @if($agreement_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}"><img src="{{$agreement_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 3) 
                                        @if($home_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}"><img src="{{$home_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 


                                        @if($job->parentsoption == 1) 
                                        @if($standard_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}"><img src="{{$standard_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 2) 
                                        @if($exten_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}"><img src="{{$exten_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 3) 
                                        @if($parent_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}"><img src="{{$parent_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 4) 
                                        @if($after_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}"><img src="{{$after_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 5) 
                                        @if($night_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}"><img src="{{$night_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 6) 
                                        @if($rotno_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}"><img src="{{$rotno_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 7)  
                                        @if($rotyes_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}"><img src="{{$rotyes_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                <br>
                                <a href="/filter_company/{{$job->company}}" style="color: grey; font-size: 20px; font-weight: 500">{{$job->company}}</a>
                                
                                </div>


                                <div class="col-md-8">

                                <p style="padding-top:20px; padding-bottom: 20px; margin: 0;word-wrap: break-word; font-size: 18px; ">{{\Illuminate\Support\Str::limit(strip_tags($job->short_description),700)}}</p>
                                @if(strlen($job->selling1)>0)<li style="font-size: 16px;">{{$job->selling1}}</li>@endif
                                 @if(strlen($job->selling2)>0)<li style="font-size: 16px;">{{$job->selling2}}</li> @endif
                                  @if(strlen($job->selling3)>0)<li style="font-size: 16px;">{{$job->selling3}}</li> @endif <br><br>
                                <small style="color: #777;">{{Categories::where('id', $job->category_id)->get()->first()? Categories::where('id', $job->category_id)->first()->title :''}} / {{SubCategories::where('id', $job->subcategory)->first()? SubCategories::where('id', $job->subcategory)->first()->title : ''}}</small> <br>
                                <?php
                                    $usergroup = false;
                                    if(Auth::user()){
                                    $usergroup = \App\UsersGroups::where('user_id',Auth::user()->id)->first();
                                    $status = \DB::table('savedjobs')->where('user_id',Auth::user()->id)->where('job_id',$job->id)->get();
                                    }   
                                 ?>
                                @if($usergroup)
                                    @if($usergroup->group_id == 2)
                                        @if($status)
                                        <p style="display: block; margin: 0; color: #639941;">Saved!</p>
                                        @else
                                        <a style="display: block; text-decoration: none;" href="/customer/savejob/{{$job->id}}"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                        @endif
                                    @endif
                                @else
                                <a style="display: block; text-decoration: none;" href="/register-user"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                @endif
                                @if(strlen($job->featured_image)>0)
                                <img class="featured-logo" style="max-width: 250px; max-height: 100px; margin-bottom: 20px; margin-top: 10px;" src="{{$job->featured_image}}">
                                @endif

                            </div>
                            <div class="col-md-4">
                            @if($job->posts_packages == 1)
                                <ul class="list-group" style="max-width: 300px; border: 2px solid; box-shadow: 8px 9px 7px #888888; padding-bottom: 10px; padding-top: 10px; background-color: white;" >
                                    <li class="list-group-item" style="padding:5px 15px; font-size: 15px; color: black;">Location: <a href="https://www.google.com/maps/place/{{$job->joblocation}}">{{$job->joblocation}}</a></li>
                                    <li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Employment Hours: <span style="color: grey">{{ucfirst(str_replace("_", " ", $job->employment_type))}}</span></span></li>
                                    <li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Employment Status: <span style="color: grey">
                                        @if($job->employment_term == 'fixed_term')
                                        Fixed-Term / Contract
                                        @else
                                        {{ucfirst(str_replace("_", " ", $job->employment_term))}}
                                        @endif
                                    </span></span></li>
                                    @if($job->showsalary == "1") 
                                    @if($job->salarytype == "annual")<li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Annual Salary: <span style="color: grey">${{number_format($job->salary)}}</span></span></li>@endif
                                    @if($job->salarytype == "hourly")<li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Hourly Rate: <span style="color: grey">${{number_format($job->hrsalary, 2)}}</span></span></li>@endif
                                    @if($job->paycycle != "no")<li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Pay Frequency: <span style="color: grey">{{ucfirst($job->paycycle)}}</span></span></li>@endif
                                    @if($job->paycycle == "no")<li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Pay Frequency: <span style="color: grey">Not Specified</span></span></li>@endif
                                    @endif
                                    <li class="list-group-item" style="padding:5px 15px; font-size: 15px;"><span class="color-black">Advertiser: <span style="color: grey"> @foreach($users as $user) @if($job->author_id == $user->id) @if($user->advertiser_type == 'private_advertiser') Private Advertiser @endif @if($user->advertiser_type == 'recruitment_agency') Recruitment Agency @endif  @endif @endforeach </span></span></li>
                                    <li class="list-group-item">
                                    <?php 
                                      $category =  \App\Categories::find($job->category);
                                      $sub_category =  \App\SubCategories::find($job->subcategory);
                                    ?>
                                    @if($category && $sub_category)
                                    <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                    @else
                                    <a href="/{{$job->slug}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                    @endif
                                </ul>
                            @endif
                            
                            </div>
                        
                    </div>
                    @endif
                    @if( $job->posts_packages == 2)
                    <div class="row helo" style="margin:0; border: 4px solid #078d00 !important; background-color: white !important;">
                        <div class="col-md-4 col-md-push-8 text-right">
                                    <small style="color: grey; padding-right: 15px;">Posted {{$job->created_at->diffForHumans()}}</small>
                                </div>
                            <div class="col-md-8 col-md-pull-4">
                                <?php 
                                      $category =  \App\Categories::find($job->category);
                                      $sub_category =  \App\SubCategories::find($job->subcategory);
                                    ?>
                                    @if($category && $sub_category)
                                    <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>
                                    @else
                                    <a href="/{{$job->slug}}" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>

                                    @endif

                                        @if($job->work_from_home == 1) 
                                        @if($business_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}"><img src="{{$business_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$business_info->id!!}">
                                                {!!$business_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 2) 
                                        @if($agreement_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}"><img src="{{$agreement_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 3) 
                                        @if($home_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}"><img src="{{$home_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 


                                        @if($job->parentsoption == 1) 
                                        @if($standard_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}"><img src="{{$standard_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 2) 
                                        @if($exten_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}"><img src="{{$exten_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 3) 
                                        @if($parent_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}"><img src="{{$parent_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 4) 
                                        @if($after_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}"><img src="{{$after_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 5) 
                                        @if($night_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}"><img src="{{$night_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 6) 
                                        @if($rotno_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}"><img src="{{$rotno_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 7)  
                                        @if($rotyes_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}"><img src="{{$rotyes_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                 <br>
                                <a href="/filter_company/{{$job->company}}" style="color: grey; font-size: 20px; font-weight: 500">{{$job->company}}</a>
                                
                                </div>


                                <div class="col-md-8">

                                <p style="padding-top:10px; padding-bottom: 10px; margin: 0; word-wrap: break-word; font-size: 17px;">{{\Illuminate\Support\Str::limit(strip_tags($job->short_description),700)}}</p>
                                @if(strlen($job->selling1)>0)<li style="font-size: 15px;">{{$job->selling1}}</li> @endif
                                 @if(strlen($job->selling2)>0)<li style="font-size: 15px;">{{$job->selling2}}</li> @endif
                                  @if(strlen($job->selling3)>0)<li style="font-size: 15px;">{{$job->selling3}}</li> @endif <br>

                                <small style="color: #777;">{{Categories::where('id', $job->category_id)->get()->first()? Categories::where('id', $job->category_id)->first()->title :''}} / {{SubCategories::where('id', $job->subcategory)->first()? SubCategories::where('id', $job->subcategory)->first()->title : ''}}</small> <br>
                                <?php
                                    $usergroup = false;
                                    if(Auth::user()){
                                    $usergroup = \App\UsersGroups::where('user_id',Auth::user()->id)->first();
                                    $status = \DB::table('savedjobs')->where('user_id',Auth::user()->id)->where('job_id',$job->id)->get();
                                    }   
                                 ?>
                                @if($usergroup)
                                    @if($usergroup->group_id == 2)
                                        @if($status)
                                        <p style="display: block; margin: 0; color: #639941;">Saved!</p>
                                        @else
                                        <a style="display: block; text-decoration: none;" href="/customer/savejob/{{$job->id}}"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                        @endif
                                    @endif
                                @else
                                <a style="display: block; text-decoration: none;" href="register-user"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                @endif
                                @if(strlen($job->featured_image)>0)
                                <img class="featured-logo" style="max-width: 200px;max-height: 70px; margin-bottom: 5px;" src="{{$job->featured_image}}">
                                @endif

                            </div>
                            <div class="col-md-4">
                            @if($job->posts_packages == 2)
                                <ul class="list-group" style="max-width: 300px; padding-top:0px; padding-bottom: 0px; background-color: white;" >
                                    <li class="list-group-item" style="padding:3px 15px;font-size: 15px; color: black;">Location: <a href="https://www.google.com/maps/place/{{$job->joblocation}}">{{$job->joblocation}}</a></li>
                                    <li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Employment Hours: <span style="color: grey">{{ucfirst(str_replace("_", " ", $job->employment_type))}}</span></span></li>
                                    <li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Employment Status: <span style="color: grey">
                                        @if($job->employment_term == 'fixed_term')
                                        Fixed-Term / Contract
                                        @else
                                        {{ucfirst(str_replace("_", " ", $job->employment_term))}}
                                        @endif
                                    </span></span></li>
                                    @if($job->showsalary == "1") 
                                    @if($job->salarytype == "annual")<li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Annual Salary: <span style="color: grey">${{number_format($job->salary)}}</span></span></li>@endif
                                    @if($job->salarytype == "hourly")<li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Hourly Rate: <span style="color: grey">${{number_format($job->hrsalary, 2)}}</span></span></li>@endif
                                    @if($job->paycycle != "no")<li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Pay Frequency: <span style="color: grey">{{ucfirst($job->paycycle)}}</span></span></li>@endif
                                    @if($job->paycycle == "no")<li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Pay Frequency: <span style="color: grey">Not Specified</span></span></li>@endif
                                    @endif
                                    <li style="padding:3px 15px;font-size: 15px; color: black;" class="list-group-item"><span class="color-black">Advertiser: <span style="color: grey"> @foreach($users as $user) @if($job->author_id == $user->id) @if($user->advertiser_type == 'private_advertiser') Private Advertiser @endif @if($user->advertiser_type == 'recruitment_agency') Recruitment Agency @endif  @endif @endforeach </span></span></li>
                                    <li class="list-group-item">
                                    <?php 
                                      $category =  \App\Categories::find($job->category);
                                      $sub_category =  \App\SubCategories::find($job->subcategory);
                                    ?>
                                    @if($category && $sub_category)
                                    <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                    @else
                                    <a href="/{{$job->slug}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>

                                    @endif
                                </ul>
                            @endif
                            
                            </div>
                        
                    </div>
                    @endif
                    @if( $job->posts_packages == 3)
                    <div class="row helo" style="margin:0;  border: 1px solid lightgrey !important; background-color: white !important;">
                        <div class="col-md-4 col-md-push-8 text-right ">
                                    <small style="color: grey; padding-right: 15px; ">Posted {{$job->created_at->diffForHumans()}}</small>
                                </div>
                            <div class="col-md-8 col-md-pull-4">
                                <?php 
                                      $category =  \App\Categories::find($job->category);
                                      $sub_category =  \App\SubCategories::find($job->subcategory);
                                    ?>
                                    @if($category && $sub_category)
                                    <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>
                                    @else
                                    <a href="/{{$job->slug}}" style="font-size: 28px; padding-bottom: 10px;" class="featured-heading">{{$job->title}}</a>

                                    @endif
                                        @if($job->work_from_home == 1) 
                                        @if($business_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$business_info->id!!}"><img src="{{$business_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$business_info->id!!}">
                                                {!!$business_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 2) 
                                        @if($agreement_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$agreement_info->id!!}"><img src="{{$agreement_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$agreement_info->id!!}">
                                                {!!$agreement_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 
                                        @if($job->work_from_home == 3) 
                                        @if($home_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$home_info->id!!}"><img src="{{$home_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$home_info->id!!}">
                                                {!!$home_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif
                                        @endif 


                                        @if($job->parentsoption == 1) 
                                        @if($standard_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$standard_info->id!!}"><img src="{{$standard_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$standard_info->id!!}">
                                                {!!$standard_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 2) 
                                        @if($exten_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$exten_info->id!!}"><img src="{{$exten_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$exten_info->id!!}">
                                                {!!$exten_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif 
                                        @endif
                                        @if($job->parentsoption == 3) 
                                        @if($parent_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$parent_info->id!!}"><img src="{{$parent_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$parent_info->id!!}">
                                                {!!$parent_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 4) 
                                        @if($after_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$after_info->id!!}"><img src="{{$after_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$after_info->id!!}">
                                                {!!$after_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 5) 
                                        @if($night_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$night_info->id!!}"><img src="{{$night_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$night_info->id!!}">
                                                {!!$night_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                        @if($job->parentsoption == 6) 
                                        @if($rotno_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotno_info->id!!}"><img src="{{$rotno_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"> </span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotno_info->id!!}">
                                                {!!$rotno_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif   
                                        @endif
                                        @if($job->parentsoption == 7)  
                                        @if($rotyes_info->value_txt) <span class="tooltips" data-tooltip-content="#tooltip_content{!!$rotyes_info->id!!}"><img src="{{$rotyes_info_img->value_string}}" style="padding-left: 7px; margin-top: -15px; width: 35px;"></span>
                                        <div class="tooltip_templates">
                                            <span id="tooltip_content{!!$rotyes_info->id!!}">
                                                {!!$rotyes_info->value_txt!!}
                                            </span>
                                        </div>
                                        @endif  
                                        @endif
                                 <br>
                                <a href="/filter_company/{{$job->company}}" style="color: grey; font-size: 20px; font-weight: 500">{{$job->company}}</a>
                                
                                </div>


                                <div class="col-md-8 standard">

                                <p style="padding-top:5px; margin: 0; word-wrap: break-word;">{{\Illuminate\Support\Str::limit(strip_tags($job->short_description),700)}}</p>
                                <div class="standardline" style="color: #777; font-size: 85%; position: absolute; bottom: 10px;">                                  
                                {{Categories::where('id', $job->category_id)->get()->first()? Categories::where('id', $job->category_id)->first()->title :''}} / {{SubCategories::where('id', $job->subcategory)->first()? SubCategories::where('id', $job->subcategory)->first()->title : ''}}
                                <?php
                                    $usergroup = false;
                                    if(Auth::user()){
                                    $usergroup = \App\UsersGroups::where('user_id',Auth::user()->id)->first();
                                    $status = \DB::table('savedjobs')->where('user_id',Auth::user()->id)->where('job_id',$job->id)->get();
                                    }   
                                 ?>
                                @if($usergroup)
                                    @if($usergroup->group_id == 2)
                                        @if($status)
                                        <p style="display: block; margin: 0; color: #639941;">Saved!</p>
                                        @else
                                        <a style="display: block; text-decoration: none;" href="/customer/savejob/{{$job->id}}"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                        @endif
                                    @endif
                                @else
                                <a style="display: block; text-decoration: none;" href="/register-user"><i class="fa fa-download fa-lg" aria-hidden="true"></i>  Save Job</a>
                                @endif
                                </div>
                                </div>
                            <div class="col-md-4">
                            @if($job->posts_packages == 3)
                                <ul class="list-group" style="max-width: 300px; background-color: white; padding-bottom: 0px;" >
                                    <li class="list-group-item" style="padding: 0px 15px; font-size: 15px; color: black;">Location: <a href="https://www.google.com/maps/place/{{$job->joblocation}}">{{$job->joblocation}}</a></li>
                                    <li class="list-group-item" style="padding: 0px 15px;"><span class="color-black">Employment Hours: <span style="color: grey">{{ucfirst(str_replace("_", " ", $job->employment_type))}}</span></span></li>
                                    <li class="list-group-item" style="padding: 0px 15px;"><span class="color-black">Employment Status: <span style="color: grey">
                                        @if($job->employment_term == 'fixed_term')
                                        Fixed-Term / Contract
                                        @else
                                        {{ucfirst(str_replace("_", " ", $job->employment_term))}}
                                        @endif
                                    </span></span></li>
                                    @if($job->showsalary == "1") 
                                    <li class="list-group-item" style="padding: 0px 15px;"><span class="color-black">Annual Salary: <span style="color: grey">${{number_format($job->salary)}}</span></span></li>
                                    @endif
                                    <li class="list-group-item" style="padding-top: 2px;">
                                    <?php 
                                      $category =  \App\Categories::find($job->category);
                                      $sub_category =  \App\SubCategories::find($job->subcategory);
                                    ?>
                                    @if($category && $sub_category)
                                    <a href="/callcentrejobs/{{$category->slug}}/{{$sub_category->slug}}/{{Str::slug($job->state)}}/{{$job->id}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>
                                    @else
                                    <a href="/{{$job->slug}}" class="btn btn-theme btn-default orange" style="width: 100%; font-size: 15px; border-radius: 10px !important;">View More & Apply</a></li>

                                    @endif
                                </ul>
                            @endif
                            
                            </div>
                        
                    </div>
                    @endif
                    <!-- item list -->
                    @endforeach
                    @endif
                </div><!-- end box list -->
                <!-- pagination -->
                <nav>
                    @if(sizeof($jobs)>0)
                    <ul class="pagination pagination-theme  no-margin pull-right">
                        {!! $jobs->appends(Input::except('page'))->render() !!}
                    </ul>
                    @endif
                </nav>

                @foreach($ads as $ad)
                @if($ad->position=='below_page')
                {!! $ad->code !!}
                @endif
                @endforeach
            </div>
            <div class="modal fade" tabindex="-1" id="myModal" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            @include('admin.layouts.notify')
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Create new Job Alert</h4>
                        </div>
                        <div class="modal-body">
                            <form action="/customer/subscriptions/user" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group ">
                                    <div class="select-style">
                                        <label>Main Category</label>
                                        <select class="form-control" style="" name="category" id="category" required></select>
                                    </div>
                                </div> 
                                <div class="form-group ">
                                    <div class="select-style">
                                        <label>Sub Category</label>
                                        <p id="notenough" class="text-center hidden" style="font-size: 14px;text-align: center;"><small><i class="fa fa-info-circle" aria-hidden="true"></i> Select 1 sub category. If you would like to select more, simply create another job alert.</small></p>
                                        <select class="form-control" style="" name="subcategory" id="subcategory" required>
                                            <option value="-1">Choose Sub Category</option>
                                            @foreach($categories as $category)
                                            @foreach($category->subcategory as $subcategory)
                                            <option value="{{$subcategory->title}}">{{$subcategory->title}}</option>
                                            @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="searchour" id="searchour" value="yes">
                                        Search By Hourly Rates
                                    </label>
                                </div>
                                <div class="salrwapperannual">
                                    <div class="col-md-6">
                                        <label>Min</label>
                                        <select class="form-control" style="margin-left:-15px;" name="wage_min" id="salstart">
                                            <option selected="selected" value="0" data-reactid="$0">$0</option>
                                            <option value="30000" data-reactid="$30000">$30k</option>
                                            <option value="40000" data-reactid="$40000">$40k</option>
                                            <option value="50000" data-reactid="$50000">$50k</option>
                                            <option value="60000" data-reactid="$60000">$60k</option>
                                            <option value="70000" data-reactid="$70000">$70k</option>
                                            <option value="80000" data-reactid="$80000">$80k</option>
                                            <option value="100000" data-reactid="$100000">$100k</option>
                                            <option value="120000" data-reactid="$120000">$120k</option>
                                            <option value="150000" data-reactid="$150000">$150k</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Max</label>
                                        <select class="form-control" style="margin-left:-15px;" name="wage_max" id="salend">
                                            <option value="0" data-reactid="$0">$0</option>
                                            <option value="30000" data-reactid="$30000">$30k</option>
                                            <option value="40000" data-reactid="$40000">$40k</option>
                                            <option value="50000" data-reactid="$50000">$50k</option>
                                            <option value="60000" data-reactid="$60000">$60k</option>
                                            <option value="70000" data-reactid="$70000">$70k</option>
                                            <option value="80000" data-reactid="$80000">$80k</option>
                                            <option value="100000" data-reactid="$100000">$100k</option>
                                            <option value="120000" data-reactid="$120000">$120k</option>
                                            <option value="150000" data-reactid="$150000">$150k</option>
                                            <option value="200000" selected="selected" data-reactid="$200000">$200k+</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="hourlywapperannual hidden">
                                    <div class="col-md-6">
                                        <label>Min Hourly Rate</label>
                                        <select class="form-control"  style="margin-left:-15px;" name="hrsalstart" id="salstart">
                                            <option selected="selected" value="0" data-reactid="$0">$0</option>
                                            <option value="15" data-reactid="15"">$15</option>
                                            <option value="20" data-reactid="25">$20</option>
                                            <option value="25" data-reactid="35">$25</option>
                                            <option value="30" data-reactid="40">$30</option>
                                            <option value="35" data-reactid="50">$35</option>
                                            <option value="40" data-reactid="60">$40</option>
                                            <option value="50" data-reactid="70+">$50</option>
                                            <option value="60" data-reactid="70+">$60</option>
                                            <option value="70" data-reactid="70+">$80</option>
                                            <option value="100" data-reactid="70+">$100</option>
                                            <option value="100+" data-reactid="70+">$100+</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Max Hourly Rate</label>
                                        <select class="form-control"  style="margin-left:-15px;" name="hrsalend" id="salend">
                                            <option selected="selected" value="0" data-reactid="$0">$0</option>
                                            <option value="15" data-reactid="15">$15</option>
                                            <option value="20" data-reactid="25">$20</option>
                                            <option value="25" data-reactid="35">$25</option>
                                            <option value="30" data-reactid="40">$30</option>
                                            <option value="35" data-reactid="50">$35</option>
                                            <option value="40" data-reactid="60">$40</option>
                                            <option value="50" data-reactid="70+">$50</option>
                                            <option value="60" data-reactid="70+">$60</option>
                                            <option value="70" data-reactid="70+">$80</option>
                                            <option value="100" data-reactid="70+">$100</option>
                                            <option value="100+" data-reactid="70+">$100+</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group"  >
                                    <label>Pay Cycle</label>
                                    <select id="pay_cycle" class="form-control" name="pay_cycle">
                                        <option value="">Dont care, as long as I get paid!</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="fortnightly">Fortnightly</option>
                                        <option value="monthly">Monthly</option>
                                    </select>
                                </div>
                                <div class="form-group"  >
                                    <label>Employment Type</label>
                                    <select id="employment_type" name="employment_type" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="part_time">{{trans('messages.post_employment_type_part_time')}}</option>
                                        <option value="full_time">{{trans('messages.post_employment_type_full_time')}}</option>
                                        <option value="casual">{{trans('messages.post_employment_type_casual')}}</option>
                                    </select>
                                </div>
                                <div class="form-group"  >
                                    <label>Employment Status</label>
                                    <select id="employment_term" name="employment_term" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="permanent">{{trans('messages.post_employment_permanent')}}</option>
                                        <option value="fixed_term">Fixed-Term / Contract</option>
                                        <option value="temp">{{trans('messages.post_employment_temp')}}</option>
                                    </select>
                                </div>
                                <div class="form-group"  >
                                    <label>Incentive Structure</label>
                                    <select id="incentivestructure" class="form-control" name="incentivestructure">
                                        <option value="">Please Select</option>
                                        <option value="any">All</option>
                                        <option value="fixed">Base + Super</option>
                                        <option value="basecommbonus">Base + Super + R&R/Bonus</option>
                                        <option value="basecomm">Base + Super + Commissions</option>
                                        <option value="commonly">Commissions Only</option>
                                    </select>
                                </div>

                                <label>
                                    <input type="hidden" name="work_from_home" value="0">
                                    <input type="checkbox"
                                           name="work_from_home"
                                           value="1">
                                    Only display work from home jobs
                                </label>

                                <div class="form-group">
                                    <label for="joblocation" class="">Location & Distance</label>
                                    <div class="form-inline">
                                        <div class="input-group col-md-8">
                                            <small>Location</small>
                                            <input id="joblocation" class="form-control geocomplete" type="text" name="joblocation" placeholder="Enter address, suburb or state"  required/>
                                        </div>
                                        <div class="input-group col-md-3">
                                            <small>Radius (km)</small>
                                            <input id="radius" class="form-control" placeholder="Radius.." value="25" type="number" name="radius" required/>
                                        </div>
                                    </div>

                                    <input id="lat" class="form-control geocomplete" type="hidden" name="lat" />
                                    <input id="lng" class="form-control geocomplete" type="hidden" name="lng" />
                                    <input id="city" class="form-control geocomplete" type="hidden" name="city" />
                                    <input id="postcode" class="form-control geocomplete" type="hidden" name="postcode" />
                                </div>

                                <input type="hidden" id="currStatus">

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Create Alert</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!--
                <div class="col-md-3">
                {# _@include('filter-form') #}
                </div>
                -->

                <script>
                    function loadModal(id) {
                        // alert("hello");
                        $('#myModal').modal('toggle');
                        $('#currStatus').val(id);
                    }
                    function hi() {
                        $('#sort').val($("#changeb").val());
                        //console.log($('#sort').val());
                        $('#formsub').submit();
                    }

                    $("#changeb").on("change", hi);

                    $(document).ready(function () {
                        $('#sort').val($("#changeb").val());
                    })
                </script>
            </div>
        </div>
    </div>
    <script src="/js/tooltipster.bundle.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
                $('.tooltips').tooltipster({
                    contentCloning: true,
                    theme: 'tooltipster-shadow',
                    side: 'right',
                    maxWidth: '765'
                });
    });
    </script>
    @endsection
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            z-index: 2;
            color: #fff!important;
            cursor: default;
            background-color: #639941!important;
            border-color: #639941!important;
        }

        .pagination>li>a, .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #639941!important;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }
    </style>