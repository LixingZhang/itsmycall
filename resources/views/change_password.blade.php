@extends('layouts.master')


@section('content')

    <div class="bg-color1">
        <div class="container">
            <div class="col-md-3 col-sm-3">
                @if(Auth::user()->isPoster())
                <div class="block-section text-center ">
                    <?php
                       $posts = \App\Posts::where('author_id',Auth::user()->id)->orderBy('id', 'desc')->first();
                     ?>
                    @if(Auth::user()->advertiser_type == 'private_advertiser')
                        @if(Auth::user()->avatar)
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                        @elseif($posts && $posts->featured_image)
                        <img style="max-width: 150px;" src="{{$posts->featured_image}}" class="img-rounded" alt="">
                        @endif
                    @endif
                    @if(Auth::user()->advertiser_type == 'recruitment_agency')
                        <img style="max-width: 150px;" src="{{Auth::user()->avatar}}" class="img-rounded" alt="">
                    @endif
                    <div class="white-space-20"></div>
                    <h4>{{Auth::user()->name}}</h4>
                    <div class="white-space-20"></div>
                    <ul class="list-unstyled">
                        <li><a href="/poster/edit-poster"> Edit Profile </a></li>
                        <li><a href="/poster/applicants">View All Applicants</a></li>
                        <li><a href="/poster">View Active Jobs</a></li>
                        <li><a href="/poster/inactive"> Inactive Jobs</a></li>
                        <li><a href="/poster/expired"> Expired Jobs</a></li>
                    </ul>
                    <div class="white-space-20"></div>
                    <a href="/poster/job_post" class="btn btn-primary btn-block" style="padding: 9px 12px;">Advertise a Job</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/buy_credits" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">Buy Credits</a>
                    <div class="white-space-20"></div>
                    <a href="/poster/transaction" class="btn btn-info btn-block" style="background-color:#639941!important; color: white; border:0;">Transactions</a>
                    <div class="white-space-20"></div>
                    <a href="http://itsmycall.zendesk.com/hc/en-us/categories/115000436728-Job-Advertisers-Support"
                       target="_blank"
                       class="btn btn-info btn-block">View Pricing and FAQ's</a>
                </div>
                @endif
                @if(Auth::user()->isCustomer())
                <div class="block-section text-center">
                    <img src="{{Auth::user()->avatar}}" class="img-rounded" alt="" style="max-width: 150px;">
                    <div class="white-space-20"></div>
                    <h4>Hi {{Auth::user()->name}}</h4>
                    <div class="white-space-20"></div>
                    <ul class="list-unstyled">
                        <li><a href="/customer"> My Account </a></li>
                        <li><a href="/customer/edit-user"> Edit Profile </a></li>
                    </ul>
                    <div class="white-space-20"></div>
                    <a href="/customer/job_alerts" class="btn btn-info btn-block" style="background-color:#ff7200; color: white; border:0;">Job Alerts</a>
                    <div class="white-space-20"></div>
                    <a href="/customer/savedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Saved Jobs</a>
                    <div class="white-space-20"></div>
                    <a href="/customer/appliedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Applied Jobs</a>
                    <div class="white-space-20"></div>
                    <a href="/customer/connect-program" class="btn btn-info btn-block" style=" border:0;">Connect Program</a>
                </div>
                @endif   
            </div>
            <div class="col-md-9 col-sm-9">
                <!-- Block side right -->
                <div class="block-section box-side-account">
                    <h3 class="no-margin-top">Change Password</h3>
                    <hr/>
                    <div class="row">
                        @include('admin.layouts.notify')
                        <div class="col-md-7">
                            @if(Auth::user()->isPoster())
                            <form action="/poster/change_password" method="post">
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" name="old_password" class="form-control">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Re-type New Password</label>
                                    <input type="password" name="password_confirmation" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-theme btn-t-primary">Change Password</button>
                                </div>
                            </form>
                            @endif
                            @if(Auth::user()->isCustomer())
                            <form action="/customer/change_password" method="post">
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" name="old_password" class="form-control">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Re-type New Password</label>
                                    <input type="password" name="password_confirmation" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-theme btn-t-primary">Change Password</button>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div><!-- end Block side right -->
            </div>

        </div>
    </div>
    </div>

@endsection