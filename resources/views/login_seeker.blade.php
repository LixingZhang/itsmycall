@extends('layouts.master')

@section('extra_css')
@if( ! empty($seo_keywords))
    <meta name="keywords" content="{{$seo_keywords->value_txt}}">
@endif
@if( ! empty($seo_description))
    <meta name="description" content="{{$seo_description->value_txt}}">
@endif
@endsection

@section('google_adwards')
    {!! $google_adwards_tracking_code->value_txt !!}
@endsection

@section('facebook_pixel')
    {!! $facebook_pixel_tracking_code->value_txt !!}
@endsection


@section('content')
<div class="block-section ">
    <div class="container" style="padding-bottom:70px">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <?php $seeker_login = \App\Settings::where('column_key','seeker_login')->first(); ?>
                    @if($seeker_login) {!!$seeker_login->value_txt!!} @endif
                </div>
            </div>
        </div>
    </div>


</div>

@endsection