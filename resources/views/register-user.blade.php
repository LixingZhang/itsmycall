@extends('layouts.master')

@section('extra_js')
   {!!$settings_general->register_user_conversion!!}
@endsection

@section('content')


<div class="block-section" style="padding-bottom:130px">
    <div class="container">
        <div class="panel panel-lg">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        {!!$settings_general->register_user!!}
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <h4 style="font-weight: bold;font-size: 17px;padding-bottom: 8px;" >Register with:</h4>
                        <!-- Add font awesome icons -->
                        <a href="/connect/facebook" style="padding-right: 7px;"><img src="/register_fb.png"></a>
                        <a href="/connect/twitter" style="padding-right: 7px;"><img src="/register_twitter.png"></a>
                        <a href="/connect/linkedin" style="padding-right: 7px;"><img src="/register_linkedin.png"></a>
                        <a href="/connect/google" style="padding-right: 7px;"><img src="/register_google.png"></a>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="font-weight: bold; font-size: 17px;margin-top: 0;" >Register with Email:</h4>
                        <p>Just fill in the details below to get started:</p>
                        
                        @include('admin.layouts.notify')

                        <!-- form login -->
                        <form action="/register-user" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="type">    
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="firstname" required class="form-control" placeholder="Your Name" value="<?php if (isset($_GET['fn'])) {
                                            echo $_GET['fn'];
                                        } ?>">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="lastname" required class="form-control" placeholder="Your Name" value="<?php if (isset($_GET['ln'])) {
                                            echo $_GET['ln'];
                                        } ?>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" required class="form-control" placeholder="Your Email" value="<?php if (isset($_GET['e'])) {
                                            echo $_GET['e'];
                                        } ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label required" required">State you live in</label>
                                <select id="state" name="state" class="form-control" required>
                                    <option @if(old('state') == "" ) selected @endif value="">Select State</option>
                                    <option @if(old('state') == "Australian Capital Territory" ) selected @endif value="Australian Capital Territory">Australian Capital Territory</option>
                                    <option @if(old('state') == "New South Wales" ) selected @endif value="New South Wales">New South Wales</option>
                                    <option @if(old('state') == "Northern Territory" ) selected @endif value="Northern Territory">Northern Territory</option>
                                    <option @if(old('state') == "Queensland" ) selected @endif value="Queensland">Queensland</option>
                                    <option @if(old('state') == "South Australia" ) selected @endif value="South Australia">South Australia</option>
                                    <option @if(old('state') == "Tasmania" ) selected @endif value="Tasmania">Tasmania</option>
                                    <option @if(old('state') == "Victoria" ) selected @endif value="Victoria">Victoria</option>
                                    <option @if(old('state') == "Western Australia" ) selected @endif value="Western Australia">Western Australia</option>
                                    <option @if(old('state') == "I don't live in Australia" ) selected @endif value="I don't live in Australia">I don't live in Australia</option>
                                </select>
                            </div>	
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" required class="form-control" placeholder="Your Password">
                                <small>Password must be at least 6 characters long</small>
                            </div>
                            <div class="form-group">
                                <label>Re-type Password</label>
                                <input type="password" required name="password_confirmation" class="form-control" placeholder="Re-type Your Password">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="terms_and_conditions"  value="yes" placeholder="Enter Code" class="required" required> <label> I agree to the <a target="_blank" href="/terms-conditions">Terms and Conditions</a>, <a target="_blank" href="/privacy">Privacy Policy</a> and the <a target="_blank" href="/privacy-collection-notice">Collection Notice</a></label>
                            </div>
                            <div class="white-space-10"></div>
                            <div class="form-group no-margin">
                                <button class="btn btn-theme btn-lg btn-t-primary btn-block">Register</button>
                            </div>
                        </form><!-- form login -->

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection