@extends('layouts.master')


@section('content')
    <div class="bg-color1">
        <div class="container">
            <div class="col-md-3 col-sm-3">

                <div class="block-section text-center">
                    <img src="{{Auth::user()->avatar}}" class="img-rounded" alt="" style="max-width: 150px;">
                    <div class="white-space-20"></div>
                    <h4>Hi {{Auth::user()->name}}</h4>
                    <div class="white-space-20"></div>
                    <ul class="list-unstyled">
                        <li><a href="/customer"> My Account </a></li>
                        <li><a href="/customer/change_password"> Change Password</a></li>
                    </ul>
                    <div class="white-space-20"></div>
                    <a href="/customer/job_alerts" class="btn btn-info btn-block" style="background-color:#ff7200; color: white; border:0;">Job Alerts</a>
                    <div class="white-space-20"></div>
                    <a href="/customer/savedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Saved Jobs</a>
                    <div class="white-space-20"></div>
                    <a href="/customer/appliedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Applied Jobs</a>
                    <div class="white-space-20"></div>
                    <a href="/customer/connect-program" class="btn btn-info btn-block" style=" border:0;">Connect Program</a>
                </div>
            </div>
            <div class="col-md-9 col-sm-9">
                <!-- Block side right -->
                <div class="block-section box-side-account">
                    <h3 class="no-margin-top">Edit Profile</h3>
                    <hr/>
                    <div class="row">
                        @include('admin.layouts.notify')
                        <div class="col-md-10">
                            <!-- form login -->
                                <form action="/customer/editsave" method="POST" enctype="multipart/form-data">

                                    {!! csrf_field() !!}
                                <div class="form-group">
                                <label for="Avatar">Upload New Avatar</label>
                                <div class="input-group">

                                    <span class="input-group-btn">
                                        <span class="btn btn-primary btn-file" style="padding-top: 9px;">
                                            Add Avavtar  
                                            <input type="file" name="avatar">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control form-flat" readonly>
                                </div>
                                <div class="loader hidden">Uploading Avatar <img style="width: 25px;" src="/Spinner.gif" alt="Loading" title="Loading" /></div>
                                <div class="done hidden"><i class="fa fa-check" aria-hidden="true"></i> Your Avatar has been uploaded Successfully!</div>

                            </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" required name="first-name" value="{{ $user->first_name }}"
                                               class="form-control" placeholder="First Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" required name="last-name" value="{{ $user->last_name }}" class="form-control"
                                               placeholder="Last Name">
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" disabled="disabled" name="email" value="{{ $user->email }}" class="form-control"
                                               placeholder="Your Email">
                                    </div>


                                    <div class="form-group">
                                        <label>State</label>
                                          <select id="state" name="state" class="form-control" required>
                                                <option @if($user->state == "" ) selected @endif value="">Select State</option>
                                                <option @if($user->state == "Australian Capital Territory" ) selected @endif value="Australian Capital Territory">Australian Capital Territory</option>
                                                <option @if($user->state == "New South Wales" ) selected @endif value="New South Wales">New South Wales</option>
                                                <option @if($user->state == "Northern Territory" ) selected @endif value="Northern Territory">Northern Territory</option>
                                                <option @if($user->state == "Queensland" ) selected @endif value="Queensland">Queensland</option>
                                                <option @if($user->state == "South Australia" ) selected @endif value="South Australia">South Australia</option>
                                                <option @if($user->state == "Tasmania" ) selected @endif value="Tasmania">Tasmania</option>
                                                <option @if($user->state == "Victoria" ) selected @endif value="Victoria">Victoria</option>
                                                <option @if($user->state == "Western Australia" ) selected @endif value="Western Australia">Western Australia</option>
                                                <option @if($user->state == "I don't live in Australia" ) selected @endif value="I don't live in Australia">I don't live in Australia</option>
                                            </select>
                                    </div>

                                    <div class="form-group no-margin">
                                        <button class="btn btn-theme btn-lg btn-t-primary btn-block">Save</button>
                                    </div>
                                </form><!-- form login -->
                        </div>
                    </div>
                </div><!-- end Block side right -->
            </div>

        </div>
    </div>
<script type="text/javascript">
$('INPUT[type="file"]').change(function () {
    var ext = this.value.match(/\.(.+)$/)[1];
    if(this.files[0].size > 3000000){
        alert('This Image size should not be greater than 3MB.');
        this.value = ''; 
    }else{
    if($(this).val().length > 0){
        $( ".loader" ).removeClass('hidden');
        setTimeout(function () { 
            $( ".loader" ).addClass('hidden');
            $( ".done" ).removeClass('hidden');
         }, 6000);
    }
    }
});
</script>
@endsection

@section('extra_js')
    <script>
      $(function ()
      {
        $('#is_aus_contact_member').on('change', function ()
        {
          if ($('#is_aus_contact_member').val() == 'Yes') {
            $('#aus_contact_member_no').show()
            $('#aus_contact_member_no0').show()
            $('#aus_contact_member_no').focus()
            $('#aus_contact_member_no01').hide()
          } else {
            $('#aus_contact_member_no').val('')
            $('#aus_contact_member_no').hide()
            $('#aus_contact_member_no0').hide()
            $('#aus_contact_member_no01').show()
          }
        })
      })
    </script>
@endsection