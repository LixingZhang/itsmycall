@extends('admin.layouts.master')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/en-au.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/css/bootstrap-datetimepicker.css" />
<script type="text/javascript">
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker({format: 'Y-M-D'});
        $('#datetimepicker2').datetimepicker({format: 'Y-M-D'});
    });
</script>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="/">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/reports">Reports</a>
            </li>
        </ul>

    </div>
<?php setlocale(LC_MONETARY, 'en_US.UTF-8');?>
    <h3 class="page-title" style="padding-bottom: 20px;">
        @if($date_begin || $date_fin)
        <?php 
        $date_1 = new \Carbon\Carbon($date_begin);
        $date_2 = new \Carbon\Carbon($date_fin);
        ?>
        Reports from {{$date_1->toFormattedDateString()}} to {{$date_2->toFormattedDateString()}}:
        @else
        Reports:
        @endif
    </h3>
        <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Select Dates
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/reports/get" id="form-username" method="get"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="code" class="col-md-3 control-label">Date Start</label>

                        <div class="col-md-8">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="date_begin" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        </div>

                        <div class="form-group">
                        <label for="code" class="col-md-3 control-label">Date End</label>

                        <div class="col-md-8">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' name="date_end" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




    @if($continue)
    <h3>Active Status:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($active_jobs)
                                {{$active_jobs}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($active_pulse)
                                {{$active_pulse}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Active Pulse News Letter Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                
                                @if($active_facebook)
                                {{$active_facebook}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                 Active Facebook Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($active_linked)
                                {{$active_linked}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                 Active Linkedin Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                
                                @if($active_alerts)
                                {{$active_alerts}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                 Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                
                                @if($active_seekers)
                                {{$active_seekers}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                 Active Job Seekers
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        <h3>Totals (From Respective Dates):</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($total_job)
                                {{$total_job}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Jobs 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($total_job_advertiser)
                                {{$total_job_advertiser}}
                                @else
                                nil
                                @endif

                            </div>
                            <div class="desc">
                               Total Job Advertisers
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                
                                @if($total_standard)
                                {{$total_standard}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Inbound Program – Standard 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                 
                                @if($total_maximum)
                                {{$total_maximum}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Inbound Program – Maximum 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($total_job_seekers)
                                {{$total_job_seekers}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Job Seekers
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <h3>Performance:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat yellow-casablanca">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                
                                @if($total_job_views)
                                {{$total_job_views}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Job Views
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat yellow-casablanca">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($total_job_applicants)
                                {{$total_job_applicants}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Job Applicants
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat yellow-casablanca">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($total_job_filled)
                                {{$total_job_filled}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Jobs Filled
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        <h3>Revenue:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($revenue_jobs)
                                {{$revenue_jobs}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Job Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                               
                                @if($revenue_boost)
                                {{$revenue_boost}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Boost Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($revenue_packages)
                                {{$revenue_packages}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Value Packages Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($total_revenue)
                                {{money_format('%.2n',$total_revenue)}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Revenue
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($welcome_revenue)
                                {{money_format('%.2n',$welcome_revenue)}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                                Total Welcome Bonus Claimed
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                @if($refund_revenue)
                                {{money_format('%.2n',$refund_revenue)}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                               Total Refunds
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                
                                @if($bonus_revenue)
                                {{money_format('%.2n',$bonus_revenue)}}
                                @else
                                nil
                                @endif
                            </div>
                            <div class="desc">
                               Total Bonus Credits
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <h3>Top Five:</h3>
        <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Job (With most Applicants)</th>
                            <th>Applicants</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($top5applicants) > 0)
                                @foreach($top5applicants as $job)
                                    <tr>
                                        <td>{{$job->job_id}}</td>
                                        <td>
                                        <?php
                                        $post = \App\Posts::where('id',$job->job_id)->first();
                                         ?>
                                        @if($post)
                                        {{$post->title}}
                                        @else
                                        {{$job->job_id}}
                                        @endif
                                        </td>
                                        <td>{{$job->occurrences}}</td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td>nil</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Registration Code</th>
                            <th>Usage</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($top5ref) > 0)
                                @foreach($top5ref as $ref)
                                    <tr>
                                        <td>{{$ref->ref_id}}</td>
                                        <td>
                                        <?php
                                        $referralcode = \App\ReferralCode::where('id',$ref->ref_id)->first();
                                         ?>
                                         @if($referralcode)
                                        {{$referralcode->name}}
                                        @else
                                        {{$ref->ref_id}}
                                        @endif
                                        </td>
                                        <td>{{$ref->occurrences}}</td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td>nil</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Advertiser</th>
                            <th>Jobs</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($top5advertiser) > 0)
                                @foreach($top5advertiser as $user)
                                    <tr>
                                        <td>{{$user->author_id}}</td>
                                        <td>
                                        <?php
                                        $username = \App\Users::where('id',$user->author_id)->first();
                                         ?>
                                        @if($username)
                                        {{$username->email}}
                                        @else
                                        {{$user->author_id}}
                                        @endif
                                        </td>
                                        <td>{{$user->occurrences}}</td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td>nil</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Viewed Jobs</th>
                            <th>Views</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($top5views) > 0)
                                @foreach($top5views as $job)
                                <tr>
                                    <td>{{$job->id}}</td>
                                    <td>{{$job->title}}</td>
                                    <td>{{$job->views}}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td>nil</td>
                                    <td>nil</td>
                                    <td>nil</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
            </div>
    @endif

@stop