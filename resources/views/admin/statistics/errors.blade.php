@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Errors
        <small>Manage Errors</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/statistics/errors">Errors</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Errors
                    </div>
                    <div class="actions">
                        <a href="/admin/posts/errors/create" class="btn red">
                            <i class="fa fa-plus"></i> Create new Error </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th> ID</th>
                            <th> Error Name</th>
                            <th> Error Description </th>
                            <th> Page Title </th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($errors as $error)
                            <tr>
                            <td>{{$error->id}}</td>
                            <td>{{$error->name}}</td>
                             <td>{!!$error->description!!}</td>
                              <td>{{$error->pagetitle}}</td>
                               <td><a href="/admin/posts/errors/edit/{{$error->id}}"
                                       class="btn btn-warning btn-sm">Edit</a>
                                </td></td>
                                <td><a href="/admin/posts/errors/delete/{{$error->id}}"
                                   class="btn btn-danger btn-sm">Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop