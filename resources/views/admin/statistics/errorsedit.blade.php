@extends('admin.layouts.master')
@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop
@section('content')
<script src="/assets/plugins/redactor/plugins/imagemanager.js"></script>
<script src="/assets/plugins/redactor/redactor.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#description').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
    });
</script>
    <h3 class="page-title">
        Errors
        <small>Edit Error</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/posts/errorsall">Errors</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/posts/errors/edit/{{$error->id}}">Edit</a>
            </li>

        </ul>
    </div>

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Edit Error
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/posts/errors/update" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">Error Name</label>
                            <input type="hidden" value="{{$error->id}}" name="id">
                            <div class="col-sm-8">
                                <input id="title" class="form-control" type="text" name="title"
                                       placeholder="{{trans('messages.enter_category_title')}}"
                                       value="{{$error->name}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea id="description" class="description" name="description" class="form-control">{{$error->description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="page_title"
                                   class="col-sm-3 control-label">Page Title</label>

                            <div class="col-sm-8">
                                <textarea id="page_title" class="form-control" maxlength="60" name="page_title"
                                          placeholder="Enter Page Title">{{$error->pagetitle}}</textarea>
                            </div>
                        </div>



                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop