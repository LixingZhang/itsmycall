@extends('admin.layouts.master')

@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="/">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin">{{trans('messages.dashboard')}}</a>
            </li>
        </ul>

    </div>
<?php setlocale(LC_MONETARY, 'en_US.UTF-8');?>
    <h3 class="page-title">
        {{trans('messages.dashboard')}}
        <small>{{trans('messages.reports_statistics')}}</small>
    </h3>

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-users"></i>{{trans('messages.manage_dashboard')}}
                    </div>
                </div>
                <div class="portlet-body">


                    @include('admin.layouts.notify')

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="core-box">
                                <div class="heading">
                                    <i class="clip-database circle-icon circle-dark-blue"></i>

                                    <h2>{{trans('messages.manage_users_settings')}}</h2>
                                </div>
                                <div class="content">
                                    {{trans('messages.manage_users_settings_desc')}}
                                </div>

                                <br>

                                <div class="btn-group btn-group-justified">
                                    @if(\App\Users::hasPermission("users.add"))
                                        <a class="btn btn-primary" href="/admin/users/create">
                                            {{trans('messages.new_user')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                    @if(\App\Users::hasPermission("users.view"))
                                        <a class="btn btn-primary" href="/admin/users/all">
                                            {{trans('messages.all_users')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif
                                </div>
                                <br/>

                                <div class="btn-group btn-group-justified">
                                    @if(\App\Users::hasPermission("settings.view"))
                                        <a class="btn btn-success" href="/admin/settings">
                                            {{trans('messages.settings')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                    @if(\App\Users::hasPermission("statistics.view"))
                                        <a class="btn btn-success" href="/admin/statistics">
                                            {{trans('messages.analytics')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                    @if(\App\Users::hasPermission("ad_sections.view"))
                                        <a class="btn btn-success" href="/admin/ads">
                                            {{trans('messages.ads')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="core-box">
                                <div class="heading">
                                    <i class="clip-folder circle-icon circle-dark-blue"></i>

                                    <h2>{{trans('messages.content_management')}}</h2>
                                </div>
                                <div class="content">
                                    {{trans('messages.content_management_desc')}}
                                </div>

                                </br>

                                <div class="btn-group btn-group-justified">
                                    @if(\App\Users::hasPermission("posts.add"))
                                        <a class="btn btn-primary" href="/admin/posts/create">
                                            {{trans('messages.create_new_post')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                    @if(\App\Users::hasPermission("posts.view"))
                                        <a class="btn btn-primary" href="/admin/posts">
                                            {{trans('messages.all_posts')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif
                                </div>
                                <br/>

                                <div class="btn-group btn-group-justified">
                                    @if(\App\Users::hasPermission("categories.add"))
                                        <a class="btn btn-success" href="/admin/categories/create">
                                            {{trans('messages.new_category')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                    @if(\App\Users::hasPermission("categories.view"))
                                        <a class="btn btn-success" href="/admin/categories">
                                            {{trans('messages.categories')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                </div>
                                <br/>

                                <div class="btn-group btn-group-justified">
                                    @if(\App\Users::hasPermission("sources.add"))
                                        <a class="btn btn-default" href="/admin/sources/create">
                                            {{trans('messages.new_category')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif

                                    @if(\App\Users::hasPermission("sources.view"))
                                        <a class="btn btn-default" href="/admin/sources">
                                            {{trans('messages.all_sources')}} <i class="clip-arrow-right-2"></i>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<h3>Value Pack Purchases</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <?php 
                                   $pack_count = \DB::table('package_info')->where('payment','Invoice')->count();
                                ?>
                                {{$pack_count}}
                            </div>
                            <div class="desc">
                                Total Invoice Value Pack Purchased
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Value Pack Purchased</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Send Invoice</td>
                                <?php 
                                   $sipack = \DB::table('package_info')->where('payment','Invoice')->whereNULL('status')->count();
                                ?>
                                <td>{{$sipack}}</td>
                            </tr>
                            <tr>
                                <td>Awaiting payment</td>
                                <?php 
                                   $appack = \DB::table('package_info')->where('payment','Invoice')->where('status','Awaiting Payment')->count();
                                ?>
                                <td>{{$appack}}</td>
                            </tr>
                            <tr>
                                <td>Overdue</td>
                                <?php 
                                   $opack = \DB::table('package_info')->where('payment','Invoice')->where('status','Overdue')->count();
                                ?>
                                <td>{{$opack}}</td>
                            </tr>
                            <tr>
                                <td>Paid</td>
                                <?php 
                                   $ppack = \DB::table('package_info')->where('payment','Invoice')->where('status','Paid')->count();
                                ?>
                                <td>{{$ppack}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    </div>
<h3>Job Status Outcomes:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$posts_count}}
                            </div>
                            <div class="desc">
                                Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Total Jobs Posted</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Active - Job will be visible to applicants (this is the default status after job is advertised)</td>
                                <?php 
                                   $apost = \App\Posts::where('status','active')->where('expired_at','>',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$apost}}</td>
                            </tr>
                            <tr>
                                <td>Hide - Will remove the job from the live website however it can still edited and re-listed within the 30 day period</td>
                                <?php 
                                   $hpost = \App\Posts::where('status','remove')->where('expired_at','>',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$hpost}}</td>
                            </tr>
                            <tr>
                                <td>Success - Job has been offered to a candidate. This will remove the job advertisement.</td>
                                <?php 
                                   $fpost = \App\Posts::where('status','filled')->where('expired_at','>',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$fpost}}</td>
                            </tr>
                            <tr>
                                <td>Closed - Withdrawn - The job has been withdrawn and is no longer available.</td>
                                <?php 
                                   $cwpost = \App\Posts::where('status','closed_withdrawn')->where('expired_at','>',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$cwpost}}</td>
                            </tr>
                            <tr>
                                <td>Closed - Filled Elsewhere - The job was filled outside of ItsMyCall.</td>
                                <?php 
                                   $cfpost = \App\Posts::where('status','closed_filled')->where('expired_at','>',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$cfpost}}</td>
                            </tr>
                            <tr>
                                <td>Closed - Poor Response - The job had a poor response/lack of quality candidates.</td>
                                <?php 
                                   $crpost = \App\Posts::where('status','closed_response')->where('expired_at','>',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$crpost}}</td>
                            </tr>
                            <tr>
                                <td>Expired</td>
                                <?php 
                                   $expost = \App\Posts::where('expired_at','<',\Carbon\Carbon::now())->count();
                                ?>
                                <td>{{$expost}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    </div>
    <h3><strong>By State:</strong></h3>
    <h3>Australian Capital Territory</h3>
    <?php 
        $act_active_jobs = \App\Posts::where('status','active')->where('state','Australian Capital Territory')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $act_total_jobs = \App\Posts::where('state','Australian Capital Territory')->count();
        $act_total_jobalerts = \App\Subscription::where('state','Australian Capital Territory')->where('active',1)->count();
        $user_calc_act = \App\Users::where('state','Australian Capital Territory')->get();
        $user_pass_act = [];
        foreach ($user_calc_act as $post) {
            $user_pass_act[] = $post->id;
        }
        $transaction_revenue_act = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_act)->get();
        $total_revenue_act = 0;
        foreach ($transaction_revenue_act as $transaction) {
            $total_revenue_act = $total_revenue_act+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$act_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$act_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$act_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_act)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>New South Wales</h3>
    <?php 
        $nsw_active_jobs = \App\Posts::where('status','active')->where('state','New South Wales')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $nsw_total_jobs = \App\Posts::where('state','New South Wales')->count();
        $nsw_total_jobalerts = \App\Subscription::where('state','New South Wales')->where('active',1)->count();
        $user_calc_nsw = \App\Users::where('state','New South Wales')->get();
        $user_pass_nsw = [];
        foreach ($user_calc_nsw as $post) {
            $user_pass_nsw[] = $post->id;
        }
        $transaction_revenue_nsw = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_nsw)->get();
        $total_revenue_nsw = 0;
        foreach ($transaction_revenue_nsw as $transaction) {
            $total_revenue_nsw = $total_revenue_nsw+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$nsw_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$nsw_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$nsw_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_nsw)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>Northern Territory</h3>
    <?php 
        $nt_active_jobs = \App\Posts::where('status','active')->where('state','Northern Territory')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $nt_total_jobs = \App\Posts::where('state','Northern Territory')->count();
        $nt_total_jobalerts = \App\Subscription::where('state','Northern Territory')->where('active',1)->count();
        $user_calc_nt = \App\Users::where('state','Northern Territory')->get();
        $user_pass_nt = [];
        foreach ($user_calc_nt as $post) {
            $user_pass_nt[] = $post->id;
        }
        $transaction_revenue_nt = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_nt)->get();
        $total_revenue_nt = 0;
        foreach ($transaction_revenue_nt as $transaction) {
            $total_revenue_nt = $total_revenue_nt+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$nt_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$nt_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$nt_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_nt)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>Queensland</h3>
    <?php 
        $ql_active_jobs = \App\Posts::where('status','active')->where('state','Queensland')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $ql_total_jobs = \App\Posts::where('state','Queensland')->count();
        $ql_total_jobalerts = \App\Subscription::where('state','Queensland')->where('active',1)->count();
        $user_calc_ql = \App\Users::where('state','Queensland')->get();
        $user_pass_ql = [];
        foreach ($user_calc_ql as $post) {
            $user_pass_ql[] = $post->id;
        }
        $transaction_revenue_ql = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_ql)->get();
        $total_revenue_ql = 0;
        foreach ($transaction_revenue_nt as $transaction) {
            $total_revenue_ql = $total_revenue_ql+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$ql_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$ql_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$ql_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_ql)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>South Australia</h3>
    <?php 
        $sa_active_jobs = \App\Posts::where('status','active')->where('state','South Australia')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $sa_total_jobs = \App\Posts::where('state','South Australia')->count();
        $sa_total_jobalerts = \App\Subscription::where('state','South Australia')->where('active',1)->count();
        $user_calc_sa = \App\Users::where('state','South Australia')->get();
        $user_pass_sa = [];
        foreach ($user_calc_sa as $post) {
            $user_pass_sa[] = $post->id;
        }
        $transaction_revenue_sa = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_sa)->get();
        $total_revenue_sa = 0;
        foreach ($transaction_revenue_sa as $transaction) {
            $total_revenue_sa = $total_revenue_sa+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$sa_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$sa_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$sa_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_sa)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>Tasmania</h3>
    <?php 
        $tas_active_jobs = \App\Posts::where('status','active')->where('state','Tasmania')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $tas_total_jobs = \App\Posts::where('state','Tasmania')->count();
        $tas_total_jobalerts = \App\Subscription::where('state','Tasmania')->where('active',1)->count();
        $user_calc_tas = \App\Users::where('state','Tasmania')->get();
        $user_pass_tas = [];
        foreach ($user_calc_tas as $post) {
            $user_pass_tas[] = $post->id;
        }
        $transaction_revenue_tas = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_tas)->get();
        $total_revenue_tas = 0;
        foreach ($transaction_revenue_tas as $transaction) {
            $total_revenue_tas = $total_revenue_tas+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$tas_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$tas_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$tas_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_tas)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>Victoria</h3>
    <?php 
        $vic_active_jobs = \App\Posts::where('status','active')->where('state','Victoria')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $vic_total_jobs = \App\Posts::where('state','Victoria')->count();
        $vic_total_jobalerts = \App\Subscription::where('state','Victoria')->where('active',1)->count();
        $user_calc_vic = \App\Users::where('state','Victoria')->get();
        $user_pass_vic = [];
        foreach ($user_calc_vic as $post) {
            $user_pass_vic[] = $post->id;
        }
        $transaction_revenue_vic = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_vic)->get();
        $total_revenue_vic = 0;
        foreach ($transaction_revenue_vic as $transaction) {
            $total_revenue_vic = $total_revenue_vic+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$vic_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$vic_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$vic_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_vic)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>Western Australia</h3>
    <?php 
        $wa_active_jobs = \App\Posts::where('status','active')->where('state','Western Australia')->where('expired_at','>',\Carbon\Carbon::now())->count();
        $wa_total_jobs = \App\Posts::where('state','Western Australia')->count();
        $wa_total_jobalerts = \App\Subscription::where('state','Western Australia')->where('active',1)->count();
        $user_calc_wa = \App\Users::where('state','Western Australia')->get();
        $user_pass_wa = [];
        foreach ($user_calc_wa as $post) {
            $user_pass_wa[] = $post->id;
        }
        $transaction_revenue_wa = \DB::table('transactions')->where('payment','!=','Credits')->where('payment','!=','')->whereIn('user_id', $user_pass_wa)->get();
        $total_revenue_wa = 0;
        foreach ($transaction_revenue_wa as $transaction) {
            $total_revenue_wa = $total_revenue_wa+$transaction->totalnogst;
        }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$wa_active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$wa_total_jobalerts}}
                            </div>
                            <div class="desc">
                                Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$wa_total_jobs}}
                            </div>
                            <div class="desc">
                                 Total Jobs Posted
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue_wa)}}
                            </div>
                            <div class="desc">
                                 Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <hr>
    <h3>Active Status:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_jobs}}
                            </div>
                            <div class="desc">
                                Active Jobs
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_alerts}}
                            </div>
                            <div class="desc">
                                 Active Job Alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_seekers}}
                            </div>
                            <div class="desc">
                                 Active Job Seekers
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_advertisers}}
                            </div>
                            <div class="desc">
                                 Active Job Advertisers
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_auscontact}}
                            </div>
                            <div class="desc">
                                 Active Auscontact Users
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_standard}}
                            </div>
                            <div class="desc">
                                Active Connect Program – Standard 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_maximum}}
                            </div>
                            <div class="desc">
                                Active Connect Program – Maximum 
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <h3>Boost Status:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">

               <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$pending_pulse}}
                            </div>
                            <div class="desc">
                                Pending Newsletter Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$pending_facebook}}
                            </div>
                            <div class="desc">
                                 Pending Facebook Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$pending_linked}}
                            </div>
                            <div class="desc">
                                 Pending Linkedin Boost
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_pulse}}
                            </div>
                            <div class="desc">
                                Active Newsletter Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_facebook}}
                            </div>
                            <div class="desc">
                                 Active Facebook Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$active_linked}}
                            </div>
                            <div class="desc">
                                 Active Linkedin Boost
                            </div>
                        </div>
                    </div>
                </div>

               <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$completed_pulse}}
                            </div>
                            <div class="desc">
                                Completed Newsletter Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$completed_facebook}}
                            </div>
                            <div class="desc">
                                 Completed Facebook Boost
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$completed_linked}}
                            </div>
                            <div class="desc">
                                 Completed Linkedin Boost
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        <h3>Totals (to date):</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_job}}
                            </div>
                            <div class="desc">
                                Total Jobs 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_job_advertiser}}
                            </div>
                            <div class="desc">
                               Total Job Advertisers
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_auscontact}}
                            </div>
                            <div class="desc">
                               Total Auscontact Users
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_standard}}
                            </div>
                            <div class="desc">
                                Total Connect Program – Standard 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_maximum}}
                            </div>
                            <div class="desc">
                                Total Connect Program – Maximum 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green-haze">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_job_seekers}}
                            </div>
                            <div class="desc">
                                Total Job Seekers
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <h3>Performance:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat yellow-casablanca">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_job_views}}
                            </div>
                            <div class="desc">
                                Total Job Views
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat yellow-casablanca">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_job_applicants}}
                            </div>
                            <div class="desc">
                                Total Job Applicants
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat yellow-casablanca">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$total_job_filled}}
                            </div>
                            <div class="desc">
                                Total Jobs Filled
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        <h3>Revenue:</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$revenue_jobs}}
                            </div>
                            <div class="desc">
                                Total Job Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$revenue_boost}}
                            </div>
                            <div class="desc">
                                Total Boost Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$revenue_screening}}
                            </div>
                            <div class="desc">
                                Total Screening Questions Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{$revenue_packages}}
                            </div>
                            <div class="desc">
                                Total Value Packages Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$total_revenue)}}
                            </div>
                            <div class="desc">
                                Total Revenue pre GST(10%)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$welcome_revenue)}}
                            </div>
                            <div class="desc">
                                Total Welcome Bonus Claimed
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{money_format('%.2n',$bonus_revenue)}}
                            </div>
                            <div class="desc">
                               Total Bonus Credits Allocated
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <h3>Top Five:</h3>
        <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Job (With most Applicants)</th>
                            <th>Applicants</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($top5applicants as $job)
                            <tr>
                                <td>{{$job->job_id}}</td>
                                <td>
                                <?php
                                $post = \App\Posts::where('id',$job->job_id)->first();
                                 ?>
                                @if($post)
                                {{$post->title}}
                                @else
                                {{$job->job_id}}
                                @endif
                                </td>
                                <td>{{$job->occurrences}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Registration Code</th>
                            <th>Usage</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($top5ref as $ref)
                            <tr>
                                <td>{{$ref->ref_id}}</td>
                                <td>
                                <?php
                                $referralcode = \App\ReferralCode::where('id',$ref->ref_id)->first();
                                 ?>
                                 @if($referralcode)
                                {{$referralcode->name}}
                                @else
                                {{$ref->ref_id}}
                                @endif
                                </td>
                                <td>{{$ref->occurrences}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Advertiser</th>
                            <th>Jobs</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($top5advertiser as $user)
                            <tr>
                                <td>{{$user->author_id}}</td>
                                <td>
                                <?php
                                $username = \App\Users::where('id',$user->author_id)->first();
                                 ?>
                                @if($username)
                                {{$username->email}}
                                @else
                                {{$user->author_id}}
                                @endif
                                </td>
                                <td>{{$user->occurrences}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Viewed Jobs</th>
                            <th>Views</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($top5views as $job)
                            <tr>
                                <td>{{$job->id}}</td>
                                <td>{{$job->title}}</td>
                                <td>{{$job->views}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
            </div>

@stop