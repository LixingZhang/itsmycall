@extends('admin.layouts.master')
<?php setlocale(LC_MONETARY, 'en_US.UTF-8');?>
@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
        $('#datetimepicker1').datetimepicker({format: 'Y-M-D'});
        $('#datetimepicker2').datetimepicker({format: 'Y-M-D'});
        });
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 3,
                    filter_default_label: 'Select User ID',
                    filter_match_mode: 'exact'

                },
                {
                    column_number: 4,
                    filter_default_label: 'Select Username',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 5,
                    filter_default_label: 'Select User Email',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 26,
                    filter_default_label: 'Select Reason',
                    filter_match_mode: 'exact'
                }
            ]);
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@stop

@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <h3 class="page-title">
        Auscontact Members Transactions from {{$date_begin}} to {{$date_fin}}
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/transactions/auscontact">Aucontact Transactions</a>
            </li>

        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('admin.layouts.notify')
        </div> 
    </div>
    <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            {{$trans_jobs}}
                            </div>
                            <div class="desc">
                                Total Job Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            {{$revenue_boost}}
                            </div>
                            <div class="desc">
                                Total Boost Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            {{$trans_package}}
                            </div>
                            <div class="desc">
                                Total Value Packages Purchased
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            {{$commission}}%
                            </div>
                            <div class="desc">
                                Auscontact Commission Rate
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            ${{number_format($total_revenue, 2, '.', '')}}
                            </div>
                            <div class="desc">
                                Total Revenue Pre GST
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            ${{number_format($total_revenue*($commission/100), 2, '.', '')}}
                            </div>
                            <div class="desc">
                                Total Commission Due
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-puzzle"></i>Auscontact Commision %
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <form action="/admin/settings/update_auscontact" id="form-username" method="get"
                          class="form-horizontal form-bordered">
                            <div class="form-group">
                                <?php $aus_contact_discount = \App\Settings::where('column_key','aus_contact_discount')->first(); ?>
                                        <label for="aus_contact_discount"
                                               class="col-sm-3 control-label">Auscontact Commision %</label>

                                        <div class="col-sm-8">
                                            <input id="aus_contact_discount" class="form-control" type="number"
                                                   name="aus_contact_discount"
                                                   placeholder="In percentage" step="any" @if($aus_contact_discount) value="{{$aus_contact_discount->value_string}}" @endif/>
                                        </div>
                            </div>
                            <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box blue-madison">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Select Dates
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/transactions/search/auscontact" id="form-username" method="get"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                        <label for="code" class="col-md-3 control-label">Date Start</label>

                        <div class="col-md-8">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="date_begin" class="form-control" value="{{$date_begin}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        </div>

                        <div class="form-group">
                        <label for="code" class="col-md-3 control-label">Date End</label>

                        <div class="col-md-8">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' name="date_end" class="form-control" value="{{$date_fin}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Transactions
                    </div>
                </div>

                <div class="portlet-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Job Id</th>
                            <th>Job Title</th>
                            <th>User Id</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Billing Reference</th>
                            <th>Level</th>
                            <th>Screening Questions</th>
                            <th>Boosts</th>
                            <th>Packages</th>
                            <th>Date of Purchase</th>
                            <th>Time of Purchase</th>
                            <th>Payment Type</th>
                            <th>Standard Price</th>
                            <th>Discount</th>
                            <th>Total Amount (Pre GST)</th>
                            <th>Credits Used</th>
                            <th>Balance Payable</th>
                            <th>GST Paid</th>
                            <th>Total Paid</th>
                            <th>Credit Card Fees</th>
                            <th>Total Amount Invoiced</th>
                            <th>Bonus Credits</th>
                            <th>Welcome Bonus</th>
                            <th>Credit Amount</th>
                            <th>Reason Code</th>
                            <th>Current Balance</th>
                            <th>User Details</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $transaction)
                            @if($transaction->package_id)
                            <?php $details = app\Packages::find($transaction->package_id); ?>
                            <?php   $discount = 0;
                                    $bonuscredits = 0;
                                    if($transaction->package_price){
                                        $discount = $transaction->package_price + $transaction->package_price * $transaction->package_discount / 100;
                                        $bonuscredits = $transaction->package_price * $transaction->package_discount / 100;
                                    }else{
                                        $discount = $details->price + $details->price * $details->discount_percent / 100;
                                        $bonuscredits = $details->price * $details->discount_percent / 100;
                                    }
                                 ?>
                        <tr>
                            <?php
                                $job = 'nil';
                                $job_id = 'nil';
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                        $job_id  = $post->id;
                                    }
                                }
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td><a href="/admin/posts/edit/{{$job_id}}">{{$job_id}}</a></td>
                            <td> 
                                 {{$job}}
                                 </td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td> @if($transaction->listing_type) {{$transaction->listing_type}} @else nil @endif</td>
                            <td> @if($transaction->questions) {{$transaction->questions}} @else nil @endif</td>
                            <td> @if(empty($transaction->upgrades)) nil @else {!! $transaction->upgrades !!} @endif </td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                            <td>@if($transaction->total) Credits @else nil @endif </td>
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount +$transaction->refdiscup+$transaction->coupon_disc_up) - $transaction->creditsremaing)}}
                            </td>
                            <td>@if($transaction->coupon_disc_up > 0 || $transaction->refdiscup > 0)
                                 @if($transaction->ref_used)
                                    @if($transaction->refdiscup > 0)
                                    Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br>  {{money_format('%.2n',$transaction->refdiscup)}}
                                    <br>
                                     <br>
                                    @endif
                                 @endif
                                 @if($transaction->coupon_used) 
                                    @if($transaction->coupon_disc_up > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc_up)}} 
                                     @endif
                                 @endif
                            @else
                            nil
                            @endif
                            </td> 
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}}</td>
                            <td>-{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}} </td>
                            <td> nil
                             </td>
                            <td>{{money_format('%.2n',($transaction->creditsin + $discount) - $transaction->creditsremaing)}} </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> @if(empty($transaction->creditsused) && empty($transaction->package)) {{money_format('%.2n',$transaction->creditsin)}} @else {{ money_format('%.2n',$transaction->creditsremaing )}} @endif </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                        <tr> <td>{{$transaction->id}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> {{ $transaction->package }} </td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                            <td>@if($transaction->total) {{$transaction->payment}} @else nil @endif </td>

                            <td>@if($transaction->coupon_used) {{ money_format('%.2n',$transaction->totalnogst+$transaction->coupon_disc+$transaction->refdisc )}} @else {{money_format('%.2n',$transaction->totalnogst+$transaction->refdisc)}} @endif </td>
                            <td>@if($transaction->coupon_disc > 0 || $transaction->refdisc > 0) 
                                     @if($transaction->refdisc > 0)
                                        Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br> {{money_format('%.2n',$transaction->refdisc)}}
                                        <br>
                                     <br>
                                     @endif
                                     @if($transaction->coupon_used && $transaction->coupon_disc > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc)}} 
                                     @endif
                                 @else
                                    nil
                                @endif
                            </td> 
                            <td>{{ money_format('%.2n',$transaction->totalnogst)}}</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>{{ money_format('%.2n',$transaction->gst_paid )}}</td>
                            <td> {{money_format('%.2n',$transaction->totalnogst + $transaction->gst_paid)}} </td>
                            <td> @if($transaction->creditfee)
                                    {{money_format('%.2n',$transaction->creditfee)}}
                                    @else nil
                                @endif

                             </td>
                            <td> {{money_format('%.2n',$transaction->totalnogst +$transaction->gst_paid+ $transaction->creditfee)}} </td>
                            <td>{{money_format('%.2n',$bonuscredits)}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>
                                {{money_format('%.2n',$discount)}}
                            </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                    @elseif($transaction->free_upgrade)
                        <tr> 
                            <?php
                                $job = 'nil';
                                $job_id = 'nil';
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                        $job_id  = $post->id;
                                    }
                                }
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td><a href="/admin/posts/edit/{{$job_id}}">{{$job_id}}</a></td>
                            <td> 
                                 {{$job}}
                                 </td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>Screening Questions {{ money_format('%.2n', $transaction->free_upgrade_price)}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Free Upgrade</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>
                            nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade}}</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                    @elseif($transaction->free_upgrade_package)
                        <tr> 
                            <?php
                                $job = 'nil';
                                $job_id = 'nil';
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                        $job_id  = $post->id;
                                    }
                                }
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td><a href="/admin/posts/edit/{{$job_id}}">{{$job_id}}</a></td>
                            <td> 
                                 {{$job}}
                            </td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade_price}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Free Upgrade</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>
                            nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade_package}}</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                    @elseif($transaction->free_upgrade_boost)
                        <tr> 
                            <?php
                                $job = 'nil';
                                $job_id = 'nil';
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                        $job_id  = $post->id;
                                    }
                                }
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td><a href="/admin/posts/edit/{{$job_id}}">{{$job_id}}</a></td>
                            <td> 
                                 {{$job}}
                            </td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade_price}}</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Free Upgrade</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>
                            nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$transaction->free_upgrade_boost}}</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                    @elseif($transaction->reason)
                        <tr> 
                            <?php
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Credits</td>
                            <td>nil</td>
                            <td>nil
                            </td> 
                            <td>nil</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>
                            nil</td>
                            <td>nil</td>
                            <td>{{ money_format('%.2n', $transaction->creditsgiven)}}</td>
                            <td>{{$transaction->reason}}</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                    @elseif($transaction->welcome_credits)
                        <tr> 
                            <?php
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Credits</td>
                            <td>{{money_format('%.2n',$transaction->welcome_credits)}}</td>
                            <td>nil
                            </td> 
                            <td>{{ money_format('%.2n', $transaction->welcome_credits)}}</td>
                            <td>-</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil
                             </td>
                            <td>nil</td>
                            <td>
                            nil</td>
                            <td>{{money_format('%.2n', $transaction->welcome_credits)}}</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> {{money_format('%.2n', $transaction->creditsremaing )}} </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>

                    @elseif($transaction->partial_credits == 1)
                        <tr> 
                            <?php
                                $job = 'nil';
                                $job_id = 'nil';
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                        $job_id  = $post->id;
                                    }
                                }
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td><a href="/admin/posts/edit/{{$job_id}}">{{$job_id}}</a></td>
                            <td> 
                                 {{$job}}
                                 </td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td> @if($transaction->listing_type) {{$transaction->listing_type}} @else nil @endif</td>
                            <td> @if($transaction->questions) {{$transaction->questions}} @else nil @endif</td>
                            <td> @if(empty($transaction->upgrades)) nil @else {!! $transaction->upgrades !!} @endif </td>
                            <td> @if(empty($transaction->package)) nil @else {{ $transaction->package }} @endif </td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>Credit Card & Credits</td>
                            <td>@if($transaction->coupon_used) {{ money_format('%.2n',$transaction->totalnogst+$transaction->coupon_disc+$transaction->refdisc+$transaction->refdiscup+$transaction->creditsin) }} @else {{money_format('%.2n',$transaction->totalnogst+$transaction->refdisc+$transaction->refdiscup+$transaction->creditsin)}} @endif </td>
                            <td>@if($transaction->coupon_disc > 0 || $transaction->refdisc > 0 || $transaction->refdiscup > 0) 
                                     @if($transaction->ref_used)
                                        @if($transaction->refdisc > 0 || $transaction->refdiscup > 0)
                                        Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br>  {{money_format('%.2n',$transaction->refdisc+$transaction->refdiscup)}}
                                        <br>
                                     <br>
                                        @endif
                                     @endif
                                     @if($transaction->coupon_used && $transaction->coupon_disc > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc)}} 
                                     @endif
                                 @else
                                    nil
                                @endif
                            </td>
                            <td>{{ money_format('%.2n', $transaction->totalnogst+$transaction->creditsin)}}</td>
                            <td> @if(empty($transaction->creditsused)) - @else -{{money_format('%.2n',$transaction->creditsin-$transaction->creditsremaing)}} @endif </td>
                            <td>{{ money_format('%.2n', $transaction->totalnogst)}}</td>
                            <td>{{money_format('%.2n', $transaction->gst_paid)}}</td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid)}} </td>
                            <td> @if($transaction->creditfee)
                                    {{money_format('%.2n', $transaction->creditfee)}}
                                    @else nil
                                @endif

                             </td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid+$transaction->creditfee)}} </td>
                            <td>
                            <?php $details = app\Packages::find($transaction->packageid); ?>
                            <?php  
                                    $bonuscredits = 0;
                                    if($transaction->package_price){
                                        $bonuscredits = $transaction->package_price * $transaction->package_discount / 100;
                                    }else{
                                        if($details){
                                            $bonuscredits = $details->price * $details->discount_percent / 100;
                                        }
                                    }
                                 ?>
                            @if($bonuscredits > 0) {{money_format('%.2n',$bonuscredits)}} @else nil @endif </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> @if(empty($transaction->creditsused) && empty($transaction->package)) {{money_format('%.2n',$transaction->creditsin)}} @else {{money_format('%.2n', $transaction->creditsremaing )}} @endif </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                    @else
                        <tr> 
                            <?php
                                $job = 'nil';
                                $job_id = 'nil';
                                $username = 'nil';
                                $user_id = 'nil';
                                $user_email = 'nil';
                                foreach ($posts as $post) {
                                    if ($post->id == $transaction->job_id) {
                                        $job = $post->title;
                                        $job_id  = $post->id;
                                    }
                                }
                                foreach ($users as $user) {
                                    if ($user->id == $transaction->user_id) {
                                        $username = $user->name;
                                        $user_id  = $user->id;
                                        $user_email  = $user->email;
                                    }
                                }
                                 ?> 
                            <td>{{$transaction->id}}</td>
                            <td><a href="/admin/posts/edit/{{$job_id}}">{{$job_id}}</a></td>
                            <td> 
                                 {{$job}}
                                 </td>
                            <td>{{$user_id}}</td>
                            <td>{{$username}}</td>
                            <td>{{$user_email}}</td>
                            <td>@if($transaction->billing_reference) {{$transaction->billing_reference}} @else nil @endif</td>
                            <td> @if($transaction->listing_type) {{$transaction->listing_type}} @else nil @endif</td>
                            <td> @if($transaction->questions) {{$transaction->questions}} @else nil @endif</td>
                            <td> @if(empty($transaction->upgrades)) nil @else {!! $transaction->upgrades !!} @endif </td>
                            <td> @if(empty($transaction->package)) nil @else {{ $transaction->package }} @endif </td>
                            <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->created_at);
                                    $print = $date->format('d M Y');
                                    $print2 = $date->format('H:i:s'); ?>
                                <td>@if($print)
                                    <span class="hidden">{{\Carbon\Carbon::parse($transaction->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$print}} 
                                    @endif</td>
                            <td>@if($print2)
                                        {{$print2}} 
                                @endif
                            </td>
                           <td>@if($transaction->total) {{$transaction->payment}} @else nil @endif </td>
                            <td>@if($transaction->coupon_used) {{ money_format('%.2n',$transaction->totalnogst+$transaction->coupon_disc+$transaction->refdisc+$transaction->refdiscup) }} @else {{money_format('%.2n',$transaction->totalnogst+$transaction->refdisc+$transaction->refdiscup)}} @endif </td>
                            <td>@if($transaction->coupon_disc > 0 || $transaction->refdisc > 0 || $transaction->refdiscup > 0) 
                                     @if($transaction->ref_used)
                                        @if($transaction->refdisc > 0 || $transaction->refdiscup > 0)
                                        Registration Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#refdetails{{$transaction->id}}">{{$transaction->ref_used}}</a> <br>  {{money_format('%.2n',$transaction->refdisc+$transaction->refdiscup)}}
                                        <br>
                                     <br>
                                        @endif
                                     @endif
                                     @if($transaction->coupon_used && $transaction->coupon_disc > 0)
                                         Coupon Code:<br> <a style="text-decoration: none;" data-toggle="modal" data-target="#coupondetail{{$transaction->id}}">{{$transaction->coupon_used}}</a> <br> {{money_format('%.2n',$transaction->coupon_disc)}} 
                                     @endif
                                 @else
                                    nil
                                @endif
                            </td> 
                            <td>{{ money_format('%.2n', $transaction->totalnogst)}}</td>
                            <td> @if(empty($transaction->creditsused)) - @else -{{money_format('%.2n',$transaction->creditsin-$transaction->creditsremaing)}} @endif </td>
                            <td>nil</td>
                            <td>{{money_format('%.2n', $transaction->gst_paid)}}</td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid)}} </td>
                            <td> @if($transaction->creditfee)
                                    {{money_format('%.2n', $transaction->creditfee)}}
                                    @else nil
                                @endif

                             </td>
                            <td> {{money_format('%.2n', $transaction->totalnogst+$transaction->gst_paid+$transaction->creditfee)}} </td>
                            <td>
                            <?php $details = app\Packages::find($transaction->packageid); ?>
                            <?php  
                                    $bonuscredits = 0;
                                    if($transaction->package_price){
                                        $bonuscredits = $transaction->package_price * $transaction->package_discount / 100;
                                    }else{
                                        if($details){
                                            $bonuscredits = $details->price * $details->discount_percent / 100;
                                        }
                                    }
                                 ?>
                            @if($bonuscredits > 0) {{money_format('%.2n',$bonuscredits)}} @else nil @endif </td>
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            <td> @if(empty($transaction->creditsused) && empty($transaction->package)) {{money_format('%.2n',$transaction->creditsin)}} @else {{money_format('%.2n', $transaction->creditsremaing )}} @endif </td>
                            <td><a href="/admin/users/edit/{{$transaction->user_id}}" class="btn btn-primary">View Details</a></td>
                        </tr>
                        @endif
                        <!-- Modal -->
                          <div class="modal fade" id="coupondetail{{$transaction->id}}" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Coupon Description:</h4>
                                </div>
                                <?php 
                                    $couponcode = App\Coupons::where('coupon_code',$transaction->coupon_used)->first();
                                ?>
                                <div class="modal-body">
                                  <p>@if($couponcode) {!!$couponcode->description!!} @endif</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          <!-- Modal -->
                          <div class="modal fade" id="refdetails{{$transaction->id}}" role="dialog">
                            <div class="modal-dialog">
                            
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Registration Code Description:</h4>
                                </div>
                                <?php 
                                    $refcode = App\ReferralCode::where('referral_code',$transaction->ref_used)->first();
                                ?>
                                <div class="modal-body">
                                  <p>@if($refcode) {!!$refcode->description!!} @endif</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@stop