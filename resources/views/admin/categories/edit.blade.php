@extends('admin.layouts.master')

@section('content')

    <h3 class="page-title">
        {{trans('messages.categories')}}
        <small>{{trans('messages.manage_categories')}}</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/categories">{{trans('messages.categories')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/categories/edit/{{$category->id}}">{{trans('messages.update_category')}}
                    - {{$category->title}}</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>{{trans('messages.edit_category')}} - {{$category->title}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/categories/update" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="id" value="{{$category->id}}"/>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">Category Title</label>

                            <div class="col-sm-8">
                                <input id="title" class="form-control" type="text" name="title"
                                       placeholder="{{trans('messages.enter_category_title')}}"
                                       value="{{old('title',$category->title)}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="page_title"
                                   class="col-sm-3 control-label">Page Title</label>

                            <div class="col-sm-8">
                                <textarea id="page_title" class="form-control" maxlength="60" name="page_title"
                                          placeholder="Enter Page Title">{{old('page_title',$category->page_title)}}</textarea>
                                <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                        <span class = "current-value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="seo_keywords"
                                   class="col-sm-3 control-label">{{trans('messages.seo_keywords')}}</label>

                            <div class="col-sm-8">
                                <textarea id="seo_keywords" class="form-control" maxlength="160" name="seo_keywords"
                                          placeholder="{{trans('messages.enter_seo_keywords')}}">{{old('seo_keywords',$category->seo_keywords)}}</textarea>
                                <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                        <span class = "current-value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="seo_description"
                                   class="col-sm-3 control-label">{{trans('messages.seo_description')}}</label>

                            <div class="col-sm-8">
                                <textarea id="seo_description" maxlength="160" class="form-control" name="seo_description"
                                          placeholder="{{trans('messages.enter_seo_description')}}">{{old('seo_description',$category->seo_description)}}</textarea>
                                <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                        <span class = "current-value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('textarea').keyup(function() {
                var box = $(this).val();
                var limit = $(this).attr('maxlength');
                var main = box.length * 100;
                var value = (main / limit);
                var count = 0 + value;
                var reverse_count = 100 - value;
                if(box.length >= 0){
                    $(this).siblings().children().css('width', count + '%');
                    //$('.current-value').text(count + '%');
                    $('.count').text(reverse_count);
                    
                    if (count >= 60 && count < 101){
                        $(this).siblings().children().removeClass('progress-bar-danger').addClass('progress-bar-warning');
                    }
                    if(count >= 0 && count < 60){
                        $(this).siblings().children().removeClass('progress-bar-danger');
                        $(this).siblings().children().removeClass('progress-bar-warning');         
                        $(this).siblings().children().addClass('progress-bar-success')
                    }
                    
                }
                return false;
            });
        });
    </script>
@stop