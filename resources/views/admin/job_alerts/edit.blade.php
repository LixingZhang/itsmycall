@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#description').redactor({
                imageUpload: '/admin/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        {{trans('messages.ads_section')}}
        <small>{{trans('messages.manage_ads')}}</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/post_type">Post Type</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/post_type/edit/{{$ad->id}}">Edit Post Type</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Edit Post Type
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/post_types/update" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="id" value="{{$ad->id}}"/>
						
						<div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Title</label>

                            <div class="col-sm-8">
                                <input type="text" id="name" name="title" class="form-control" value="{{old('title',$ad->title)}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Post Type Name</label>

                            <div class="col-sm-8">
                                <input type="text" id="name" name="name" class="form-control" value="{{old('name',$ad->name)}}">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Xero Code</label>

                            <div class="col-sm-8">
                                <input type="text" id="xero_code" name="xero_code" class="form-control" value="{{old('xero_code',$ad->xero_code)}}">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Price</label>

                            <div class="col-sm-8">
                                <input type="text" id="price" name="price" class="form-control" value="{{old('price',$ad->price)}}">
                            </div>
                        </div>

						
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <textarea id="description" name="description" class="form-control" value="{{old('description',$ad->description)}}">{{old('description',$ad->description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop