@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/natural.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
             yadcf.init(myTable,[
              
                {
                     column_number: 3,
                     filter_default_label: 'Select Status',
                     filter_match_mode: 'exact'

                 },
                
            ])
        })
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Job Alerts
        <small>Manage Job Alerts</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/Upgrades">Job Alerts</a>
            </li>

        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Job Alerts
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>User</th>
                            <th>User Deleted</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Radius</th>
                            <th>Location</th>
                            <th>Salary Type</th>
                            <th>Low Salary Range</th>
                            <th>High Salary Range</th>
                            <th>Pay Cycle</th>
                            <th>Employment Type</th>
                            <th>Employment Status</th>
                            <th>Work Location</th>
                            <th>Shift Guide</th>
                            <th>Incentive Structure</th>
                            <th>Advertiser Type</th>
                            <th>Date Added</th>
                            <th>Date Deleted</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($job_alerts as $job_alert)
                            <tr>
                                <td> {{$job_alert->id}} </td>
                                <td> <a href="/admin/users/{{$job_alert->user_id->id}}" >{{$job_alert->user_id->name}}</a> </td>
                                <td>{{$job_alert->user_id->deleted ? 'Yes' : 'No'}}</td>
                                <td> @if($job_alert->active == '1')
                                        <?php print "Active"; ?>
                                     @endif
                                     @if($job_alert->active == '0')
                                        <?php print "Inactive"; ?>
                                     @endif
                                </td>
                                <td>{{$job_alert->user_id->email}}</td>
                                <td> {{$job_alert->category_id->title}} </td>
                                <td> @foreach($job_alert->sub_category_id as $job_sub_category) {{$job_sub_category->title}}, @endforeach</td>
                                <td> {{$job_alert->radius}} </td>
                                <td> {{$job_alert->location}} </td>
                                <td> @if($job_alert->salary_type == 'yes')
                                        <?php print "Hourly"; ?>
                                     @endif
                                     @if($job_alert->salary_type == '')
                                        <?php print "Annually"; ?>
                                     @endif
                                </td>
                                <td> @if($job_alert->salary_type == 'yes')
                                        ${{$job_alert->hourly_start}}
                                     @endif
                                     @if($job_alert->salary_type == '')
                                        ${{$job_alert->wage_min}}
                                     @endif 
                                 </td>
                                <td> @if($job_alert->salary_type == 'yes')
                                        ${{$job_alert->hourly_end}}
                                     @endif
                                     @if($job_alert->salary_type == '')
                                        ${{$job_alert->wage_max}}
                                     @endif 
                                </td>
                                <td> @if($job_alert->pay_cycle == '')
                                        nil
                                     @endif 
                                     @if($job_alert->pay_cycle != '')
                                        {{ucwords(str_replace("_", " ", implode(', ', json_decode($job_alert->pay_cycle, true))))}}
                                     @endif 
                                </td>
                                <td> {{$job_alert->employment_type}} </td>
                                <td> {{$job_alert->employment_term}} </td>
                                <td> 
                                <?php 
                                $jsonString = $job_alert->work_from_home;
                                $keys = json_decode($jsonString);
                                 ?>
                                @if($keys && is_array($keys))
                                @foreach($keys as $key)
                                    <ul>
                                        @if($key == 1)
                                        <li> At business address</li>
                                        @endif
                                        @if($key == 2)
                                        <li> By mutual agreement</li>
                                        @endif
                                        @if($key == 3) 
                                        <li> Work from home </li>
                                        @endif
                                    </ul>
                                @endforeach
                                @else
                                nil
                                @endif
                                </td>
                                <td> 
                                <?php 
                                $jsonString2 = $job_alert->parentsoption;
                                $keys2 = json_decode($jsonString2);
                                 ?>
                                @if($keys2 && is_array($keys2))
                                @foreach($keys2 as $key)
                                    <ul>
                                        @if($key == 1)
                                        <li> Standard (9am-5pm Mon to Fri)</li>
                                        @endif
                                        @if($key == 2)
                                        <li>Extended (8am-8pm Mon to Fri)</li>
                                        @endif
                                        @if($key == 3) 
                                        <li>Parent Friendly (9am-3pm Mon to Fri) </li>
                                        @endif
                                        @if($key == 4) 
                                        <li>Afternoon (12pm-12am Mon to Fri)</li>
                                        @endif
                                        @if($key == 5) 
                                        <li>Night Shift </li>
                                        @endif
                                        @if($key == 6) 
                                        <li>Rotating shifts - no weekends</li>
                                        @endif
                                        @if($key == 7) 
                                        <li>Rotating shifts - including weekend work</li>
                                        @endif
                                    </ul>
                                @endforeach
                                @else
                                nil
                                @endif
                                </td>
                                <td> {{$job_alert->incentivestructure}}</td>
                                <td> @if($job_alert->advertiser_type == '')
                                        Private Advertiser, Recruitment Agency
                                     @endif 
                                     @if($job_alert->advertiser_type != '')
                                        {{$job_alert->advertiser_type}}
                                     @endif 
                                </td>
                                <?php 
                                    $date = 0;
                                    $delete = 0;
                                    $print = 0;
                                    $dp = 0;
                                    $time = 0;
                                    $dtime = 0;
                                if($job_alert->created_at) {
                                    $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$job_alert->created_at);
                                    $print = $date->format('d M Y');
                                    $time = $date->format("H:i A");
                                } 
                                if($job_alert->deleted_at) {
                                    $delete = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$job_alert->deleted_at);
                                    $dp = $delete->format('d M Y');
                                    $dtime = $delete->format("H:i A");
                                } 
                                 ?>
                                <td> @if($print) <span class="hidden">{{\Carbon\Carbon::parse($job_alert->created_at)->format('YYYY/mm/dd')}}</span> {{$print}} - {{$time}} @endif </td>
                                <td> @if($dp) <span class="hidden">{{\Carbon\Carbon::parse($job_alert->deleted_at)->format('YYYY/mm/dd')}}</span> {{$dp}} - {{$dtime}} @endif </td>
                                <td><a href="/admin/job_alerts/delete/{{$job_alert->id}}"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_ad')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4><i class="fa fa-exclamation-triangle"></i>{{trans('messages.delete_ad_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal"> {{trans('messages.cancel')}} </button>
                                    <a class="btn btn-danger btn-ok"> {{trans('messages.delete')}} </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop