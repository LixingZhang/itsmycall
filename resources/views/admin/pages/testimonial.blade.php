@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('textarea[name="testimonial-html[]"]').each(function () {
            $(this).redactor({
                imageUpload: '/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 100, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });
        $('.testimonials').on('change', 'input[type="file"]', function () {
            var input = this;
            var image = $(this).closest('.testimonial-image').find('img');

            var reader = new FileReader();
            reader.onload = function (e) {
                image.attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        });

        $('.testimonials').on('click', '.testimonial-delete', function (e) {
            var button = $(this);
            e.preventDefault();
            if (!confirm('Are you sure you want to delete?')) {
                return;
            }
            var id = button.data('id');
            if (!id) {
                button.closest('.testimonial').remove();
                return;
            }
            $.ajax({
                method: 'DELETE',
                url: '/admin/settings/delete_testimonial',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}"
                }
            }).done(function () {
                button.closest('.testimonial').remove();
            }).fail(function () {
                console.error('Failed to delete testimonial.');
            });
        });

        $('.testimonial-add').on('click', function (e) {
            e.preventDefault();

            var html = ['<div class="testimonial">',
                          '<input type="hidden" name="testimonial-ids[]">',
                          '<div class="form-group">',
                            '<label class="col-sm-3 control-label">Testimonial Order</label>',
                            '<div class="col-sm-8">',
                              '<input class="form-control" type="number" name="testimonial-orders[]" min="1">',
                            '</div>',
                          '</div>',
                          '<div class="form-group testimonial-image">',
                            '<label class="col-sm-3 control-label">Testimonial Image</label>',
                            '<div class="col-sm-8">',
                              '<div class="row">',
                                '<div class="col-sm-3">',
                                  '<img src="" alt="placeholder image" style="width: 100%">',
                                '</div>',
                                '<div class="col-sm-9">',
                                  '<input class="form-control" name="testimonial-images[]" type="file">',
                                '</div>',
                              '</div>',
                            '</div>',
                          '</div>',
                          '<div class="form-group">',
                            '<label class="col-sm-3 control-label">Testimonial HTML</label>',
                            '<div class="col-sm-8">',
                              '<textarea name="testimonial-html[]"></textarea>',
                            '</div>',
                          '</div>',
                          '<div class="form-group">',
                            '<div class="col-sm-offset-3 col-sm-8">',
                              '<button class="btn btn-danger testimonial-delete" data-id="">Delete</button>',
                            '</div>',
                          '</div>',
                          '<hr>',
                        '</div>'].join('');
            $('.testimonials').append(html);
            $('textarea[name="testimonial-html[]"]:last').redactor({
                imageUpload: '/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 100, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });

    </script>
@stop

@section('content')

    <h3 class="page-title">
        Testimonials
        <small>Create Testimonial</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/pages">Campaign Page</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/testimonial">Testimonials</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>Create Testimonial
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                                <form action="/admin/settings/update_testimonials" enctype="multipart/form-data" method="post" class="form-horizontal form-bordered">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Testimonial Interval (seconds)</label>
                                        <div class="col-sm-1">
                                            <input class="form-control" type="number" name="testimonial-interval" value="{{old('testimonial-interval',$welcome->testimonial_interval)}}" min="1">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="testimonials">
                                        @foreach ($testimonials as $key => $testimonial)
                                            <div class="testimonial">
                                                <input type="hidden" name="testimonial-ids[]" value="{{ $testimonial->id }}">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Testimonial Order</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="number" name="testimonial-orders[]" value="{{ $testimonial->order }}" min="1">
                                                    </div>
                                                </div>
                                                <div class="form-group testimonial-image">
                                                    <label class="col-sm-3 control-label">Testimonial Image</label>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <img src="/uploads/images/{{ $testimonial->image }}" style="width: 100%">
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input class="form-control" name="testimonial-images[]" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Testimonial HTML</label>
                                                    <div class="col-sm-8">
                                                        <textarea name="testimonial-html[]">
                                                            {!! $testimonial->html !!}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-8">
                                                        <button class="btn btn-danger testimonial-delete" data-id="{{ $testimonial->id }}">Delete</button>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" class="btn green testimonial-add"><i class="fa fa-plus"></i>
                                                    Add
                                                </button>
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
@stop