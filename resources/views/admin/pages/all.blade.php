@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Campaign Pages
        <small>Manage Campaign Pages</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/pages">Campaign Pages</a>
            </li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box green-meadow">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>All Campaign Pages
                    </div>
                    <div class="actions">
                        <a href="/admin/pages/testimonial" class="btn green">
                            Testimonials</a>
                        <a href="/admin/pages/welcome" class="btn green">
                            Default Page</a>
                        <a href="/admin/pages/create" class="btn red">
                            <i class="fa fa-plus"></i> {{trans('messages.create_new_page')}} </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>URL</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Duplicate</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td> {{$page->id}} </td>
                                <td><a target="_blank" href="/welcome/{{$page->url}}">{{$page->url}}</a> </td>
                                <td>@if($page->status == 1) Active @else Inactive @endif</td>
                                <td><a href="/admin/pages/edit/{{$page->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a></td>
                                <td><a href="/admin/pages/duplicate/{{$page->id}}"
                                       class="btn btn-danger btn-sm">Duplicate</a></td>
                                <td><a href="/admin/pages/delete/{{$page->id}}"
                                       class="btn btn-danger btn-sm">Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_page')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4><i class="fa fa-exclamation-triangle"></i>{{trans('messages.delete_page_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop