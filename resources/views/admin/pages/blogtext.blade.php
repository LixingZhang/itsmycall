@extends('admin.layouts.master')
@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop
@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('#blog_text').redactor({
                imageUpload: '/admin/pages/redactor',
                imageManagerJson: '/admin/pages/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 300, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
    </script>
@stop
@section('content')
    <h3 class="page-title">
        Blog Page
        <small>Manage /blogs Page Text</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/blogs">Blogs</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/blogtext">Blog Page</a>
            </li>

        </ul>
    </div>

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Blog Page
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/settings/blog_text" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group">
                                        <label for="front_page_logo_text"
                                               class="col-sm-3 control-label">Blog Page</label>
                                        <?php $blog_text = \App\Settings::where('column_key','blog_text')->first(); ?>
                                        <div class="col-sm-8">
                                            <textarea id="blog_text" class="form-control" name="blog_text"
                                                      placeholder="Blog Page Text">@if($blog_text) {{$blog_text->value_txt}} @endif </textarea>
                                        </div>
                                    </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop