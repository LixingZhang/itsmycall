@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Blogs
        <small>Manage Blogs</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/pages/blogs">Blogs</a>
            </li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box green-meadow">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>All Blogs
                    </div>
                    <div class="actions">
                        <a href="/admin/pages/blogs/create" class="btn red">
                            <i class="fa fa-plus"></i> Create New Blog </a>
                        <a href="/admin/pages/blogs/categories" class="btn red">
                            <i class="fa fa-plus"></i> Categories </a>
                        <a href="/admin/pages/blogs/subcategories" class="btn red">
                            <i class="fa fa-plus"></i> Sub Categories </a>
                        <a href="/admin/pages/blogtext" class="btn red">
                            <i class="fa fa-plus"></i> Blog Page </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>URL</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Preview</th>
                            <th>Preview Image</th>
                            <th>Page Title</th>
                            <th>Published On</th>
                            <th>Updated On</th>
                            <th>Views</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($blogs as $blog)
                            <tr>
                                <?php $parent = \DB::table('blogcategories')->where('id',$blog->parent_category)->first(); ?>
                                <?php $sub = \DB::table('blogsubcategories')->where('id',$blog->sub_category)->first(); ?>
                                <td>{{$blog->id}}</td>
                                <td><a target="_blank" href="/blogs/{{$parent->slug}}/{{$sub->slug}}/{{$blog->slug}}">http://itsmycall.com.au/blogs/{{$parent->slug}}/{{$sub->slug}}/{{$blog->slug}}</a></td>
                                <td>{{$blog->title}}</td>
                                <td>{{$parent->title}}</td>
                                <td>{{$sub->title}}</td>
                                <td>{!!$blog->preview!!}</td>
                                <td><img width="100" src="{!!$blog->image!!}"></td>
                                <td>{!!$blog->page_title!!}</td>
                                <td>{{\Carbon\Carbon::parse($blog->created_at)->format('d M Y H:i:s')}}</td>
                                <td>{{\Carbon\Carbon::parse($blog->updated_at)->format('d M Y H:i:s')}}</td>
                                <td>{{$blog->views}}</td>
                                <td><a href="/admin/pages/blogs/edit/{{$blog->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                    </td>
                                <td><a href="/admin/pages/blogs/delete/{{$blog->id}}" class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_page')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4><i class="fa fa-exclamation-triangle"></i>{{trans('messages.delete_page_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop