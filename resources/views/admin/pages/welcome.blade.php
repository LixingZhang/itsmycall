@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('#welcome-section-one').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-six').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-seven').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-eight').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-two').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-three').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-four').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-five').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

    </script>
@stop

@section('content')

    <h3 class="page-title">
        Default Page
        <small>Edit Default Page</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/pages">Campaign Page</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/welcome">Default Page</a>
            </li>

        </ul>
    </div>
    <p><strong>Note: </strong> To track Advertise Job Now / Register Page click out, you have to put onclick="register()" in the html link tag</p>
    <p><strong>Example for Advertisers: </strong>&lta class="btn btn-primary" style="height: auto; padding: 1px 20px !important;" href="https://itsmycall.com.au/register-poster?ref=welcome" onclick="register()"&gtAdvertise Job Now&lt/a&gt</p>
    <p><strong>Example for Seekers: </strong>&lta class="btn btn-primary" style="height: auto; padding: 1px 20px !important;" href="https://itsmycall.com.au/register-user?ref=welcome" onclick="register()"&gtSearch for Jobs Now&lt/a&gt</p>

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>Edit Default Page
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                                <form action="/admin/settings/update_welcome" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for landing on page</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code" class="form-control" name="welcome-google-adwards-tracking-code"
                                                  placeholder="">{{old('welcome-google-adwards-tracking-code',$welcome->google_adwards_tracking_code)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for landing on page</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code" class="form-control" name="welcome-facebook-pixel-tracking-code"
                                                  placeholder="">{{old('welcome-facebook-pixel-tracking-code',$welcome->facebook_pixel_tracking_code)}}</textarea>
                                        </div>
                                    </div>
                                    <?php 
                                    $googledownload = \App\Settings::where('category','welcome')->where('column_key','google_adwards_tracking_code-download')->first();
                                    $pixeldownload = \App\Settings::where('category','welcome')->where('column_key','facebook_pixel_tracking_code-download')->first();
                                    ?>
                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for Download Job Advertisers Guide</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code-download" class="form-control" name="welcome-google-adwards-tracking-code-download" placeholder="">@if($googledownload) {!! $googledownload->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for Download Job Advertisers Guide</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code-download" class="form-control" name="welcome-facebook-pixel-tracking-code-download" placeholder="">@if($pixeldownload) {!! $pixeldownload->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>
                                    <?php 
                                    $googleregister = \App\Settings::where('category','welcome')->where('column_key','google_adwards_tracking_code-register')->first();
                                    $pixelregister = \App\Settings::where('category','welcome')->where('column_key','facebook_pixel_tracking_code-register')->first();
                                    $sectionsix = \App\Settings::where('category','welcome')->where('column_key','section_six')->first();
                                    $sectioneight = \App\Settings::where('category','welcome')->where('column_key','section_eight')->first();
                                    $sectionseven = \App\Settings::where('category','welcome')->where('column_key','section_seven')->first();
                                    ?>
                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for Register Page for Job Advertiser</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code-register" class="form-control" name="welcome-google-adwards-tracking-code-register" placeholder="">@if($googleregister) {!! $googleregister->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for Register Page for Job Advertiser</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code-register" class="form-control" name="welcome-facebook-pixel-tracking-code-register" placeholder="">@if($pixelregister) {!! $pixelregister->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Section One</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-one" class="form-control" name="welcome-section-one"
                                                  placeholder="">{{old('welcome-section-one',$welcome->section_one)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-two" class="col-sm-3 control-label">Section Two</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-two" class="form-control" name="welcome-section-two"
                                                  placeholder="">{{old('welcome-section-two',$welcome->section_two)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-three" class="col-sm-3 control-label">Section Three</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-three" class="form-control" name="welcome-section-three"
                                                  placeholder="">{{old('welcome-section-three',$welcome->section_three)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-four" class="col-sm-3 control-label">Section Four</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-four" class="form-control" name="welcome-section-four"
                                                  placeholder="">{{old('welcome-section-four',$welcome->section_four)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-five" class="col-sm-3 control-label">Section Five</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-five" class="form-control" name="welcome-section-five"
                                                  placeholder="">{{old('welcome-section-five',$welcome->section_five)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-six" class="col-sm-3 control-label">Section Six</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-six" class="form-control" name="welcome-section-six"
                                                  placeholder="">@if($sectionsix) {!! $sectionsix->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-seven" class="col-sm-3 control-label">Section Seven</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-seven" class="form-control" name="welcome-section-seven"
                                                  placeholder="">@if($sectionseven) {!! $sectionseven->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-eight" class="col-sm-3 control-label">Section Eight</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-eight" class="form-control" name="welcome-section-eight"
                                                  placeholder="">@if($sectioneight) {!! $sectioneight->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
@stop