@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('#section-one').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-two').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-three').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#quality').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-four').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-five').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#section-six').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#section-seven').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#section-eight').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

    </script>
@stop

@section('content')

    <h3 class="page-title">
        Campaign Pages
        <small>Edit Campaign Page</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/pages">Campaign Page</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/">Edit</a>
            </li>

        </ul>
    </div>
    <p><strong>Note: </strong> To track Advertise Job Now / Register Page click out, you have to put onclick="register()" in the html link tag</p>
    <p><strong>Example for Advertisers: </strong>&lta class="btn btn-primary" style="height: auto;    padding: 1px 20px !important;" href="https://itsmycall.com.au/register-poster?ref=welcome" onclick="register()"&gtAdvertise Job Now&lt/a&gt</p>
    <p><strong>Example for Seekers: </strong>&lta class="btn btn-primary" style="height: auto; padding: 1px 20px !important;" href="https://itsmycall.com.au/register-user?ref=welcome" onclick="register()"&gtSearch for Jobs Now&lt/a&gt</p>

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>Edit Page
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                                <form action="/admin/pages/update" id="form-username" method="post"
                                      class="form-horizontal hello form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
   
                                    <div class="form-group">
                                        <label for="is_active" class="col-sm-3 control-label">Active</label>
                                        <div class="col-sm-8">
                                            <input type="checkbox" @if($page->status == 1) checked="checked" @endif id="is_active" name="is_active" class="form-control" value="1">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for landing on page</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code" class="form-control" name="welcome-google-adwards-tracking-code"
                                                  placeholder="">{{$page->google}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for landing on page</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code" class="form-control" name="welcome-facebook-pixel-tracking-code"
                                                  placeholder="">{{$page->facebook}}</textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for Download Job Advertisers Guide</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code-download" class="form-control" name="google_download" placeholder="">{{$page->google_download}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for Download Job Advertisers Guide</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code-download" class="form-control" name="facebook_download" placeholder="">{{$page->facebook_download}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for Register Page for Job Advertiser</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code-register" class="form-control" name="google_register" placeholder="">{{$page->google_register}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for Register Page for Job Advertiser</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code-register" class="form-control" name="facebook_register" placeholder="">{{$page->facebook_register}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">URL</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="url" value="{{$page->url}}">
                                            <input type="hidden" class="form-control" name="id" value="{{$page->id}}">
                                        <p><strong>Note: </strong>The URL should be simple text such as "test" which will automatically be converted into SiteURL/welcome/test</p>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="about_page_title" type="text" class="form-control" maxlength="60" name="page_title" value="{{$page->page_title}}">
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="about-seokeywords" type="text" class="form-control" maxlength="60" name="seokeywords" value="{{$page->seokeywords}}">
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="about-seodescription" type="text" class="form-control" maxlength="160" name="seodescription" value="{{$page->seodescription}}">
                                        </div>
                                    </div>

                                   <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Video</label>
                                        <div class="col-sm-8">
                                            @if($page->video)
                                            <p> <strong>Current Video: </strong><a target="_blank" href="https://www.youtube.com/watch?v={{$page->video}}">https://www.youtube.com/watch?v={{$page->video}}</a></p>
                                            <input id="status" class="form-control" type="text" name="video" value="https://www.youtube.com/watch?v={{$page->video}}" style="color: black;" />
                                            @else
                                            <input id="status" class="form-control" type="text" name="video" value="" style="color: black;" />
                                            @endif
                                            <p class="errorvideo hidden" style="color: red;">Please enter a valid video URL</p>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Auto Play Video</label>
                                        <div class="col-sm-8">
                                            <input id="autoplay" class="form-control" type="checkbox" value="1" name="autoplay" style="color: black;" @if($page->autoplay == 1) checked="checked" @endif  />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Section One</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-one" class="form-control" name="section1"
                                                  placeholder="">{{$page->section1}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-two" class="col-sm-3 control-label">Section Two</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-two" class="form-control" name="section2"
                                                  placeholder="">{{$page->section2}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-three" class="col-sm-3 control-label">Section Three</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-three" class="form-control" name="section3"
                                                  placeholder="">{{$page->section3}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-four" class="col-sm-3 control-label">Section Four</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-four" class="form-control" name="section4"
                                                  placeholder="">{{$page->section4}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-five" class="col-sm-3 control-label">Section Five</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-five" class="form-control" name="section5"
                                                  placeholder="">{{$page->section5}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-six" class="col-sm-3 control-label">Section Six</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-six" class="form-control" name="section6"
                                                  placeholder="">{{$page->section6}}
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-seven" class="col-sm-3 control-label">Section Seven</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-seven" class="form-control" name="section7"
                                                  placeholder="">{{$page->section7}}
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-eight" class="col-sm-3 control-label">Section Eight</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-eight" class="form-control" name="section8"
                                                  placeholder="">{{$page->section8}}
                                                  </textarea>
                                        </div>
                                    </div>

                                    <h2 style="padding-left: 15px;">Select Testimonials:</h2>
                                    @foreach($testimonials as $testimonial)
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <?php
                                                $testimonialcheck = [];
                                                if(count(json_decode($page->testimonial)) > 0){
                                                    $testimonialcheck = json_decode($page->testimonial);
                                                }
                                             ?>
                                            <input type="checkbox" name="testimonial[]" value="{{$testimonial->id}}" @foreach($testimonialcheck as $check) @if($testimonial->id == $check) checked="checked" @endif @endforeach>
                                        </div>
                                        <div class="col-sm-2"><img style="width: 100%" src="/uploads/images/{{$testimonial->image}}"></div>
                                        <div class="col-sm-9">
                                            <p>{!!$testimonial->html!!}</p>
                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <a onclick="hello()" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
    };
    var myId;
    function hello() {
        var myUrl = $('#status').val();
        if(myUrl.length > 0){
            myId = getId(myUrl);
            $('#status').val(myId);
            if(myId == 'error'){
                $('.errorvideo').removeClass('hidden');
            }else{
                $('.hello').submit()
            }
        }else{
           $('.hello').submit()
        }
    };


</script>
@stop