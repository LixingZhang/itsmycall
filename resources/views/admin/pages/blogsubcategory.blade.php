@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Blog Sub Categories
        <small>Manage Blog Sub Categories</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/blogs">Blogs</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/blogs/subcategories">Blog SubCategories</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Blog Sub Categories
                    </div>
                    <div class="actions">
                        <a href="/admin/pages/blogs/subcategories/create" class="btn red">
                            <i class="fa fa-plus"></i> Create New Sub Category </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th> Sub Category Name</th>
                            <th> Parent Category</th>
                            <th> {{trans('messages.seo_keywords')}} </th>
                            <th> {{trans('messages.seo_description')}} </th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($subcategories as $category)
                            <tr>
                                <td>{{$category->title}}</td>
                                <?php $parent = \DB::table('blogcategories')->where('id',$category->parent_category)->first(); ?>
                                <td>{{$parent->title}}</td>
                                <td>{{$category->seo_keywords}}</td>
                                <td>{{$category->seo_description}}</td>
                                <td><a href="/admin/pages/blogs/subcategories/edit/{{$category->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                    </td>
                                <td><a href="/admin/pages/blogs/subcategories/delete/{{$category->id}}" class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_category')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_category_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop