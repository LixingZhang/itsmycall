@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('#description').redactor({
                imageUpload: '/admin/pages/redactor',
                imageManagerJson: '/admin/pages/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 300, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        $('#share').redactor({
                imageUpload: '/admin/pages/redactor',
                imageManagerJson: '/admin/pages/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 300, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Create Blog
        <small>Manage Create Blog</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/blogs">Blogs</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/blogs/create">Create Blog</a>
            </li>

        </ul>
    </div>

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Create New Blog
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/pages/blogs/create" id="form-username" method="post" enctype="multipart/form-data"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="parent_category" class="col-sm-3 control-label">Select Category</label>

                            <div class="col-sm-8">
                                <select id="parent_category" class="form-control" name="parent_category" required="required">
                                    <option value="">Please select a Category</option>
                                    @foreach($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sub_category" class="col-sm-3 control-label">Select Sub Category</label>

                            <div class="col-sm-8">
                                <select id="sub_category" class="form-control" name="sub_category"  required="required">
                                        <option value="">Please select a Sub Category</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">Blog Title</label>

                            <div class="col-sm-8">
                                <input id="title" class="form-control" type="text" name="title" required="required"
                                       placeholder="Enter Blog Title"
                                       value="{{old('title')}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">Blog Preview Image</label>

                            <div class="col-sm-8">
                                <p>The URL of the image for your object. It should be at least 600x315 pixels, but 1200x630 or larger is preferred (up to 5MB). Stay close to a 1.91:1 aspect ratio to avoid cropping.</p>
                                <input id="image" class="form-control" type="file" name="image" required="required"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">Blog Preview</label>

                            <div class="col-sm-8">
                                <textarea id="preview" class="form-control" name="preview" placeholder="Enter Blog Preview"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="share" class="col-sm-3 control-label">Blog Share Links</label>

                            <div class="col-sm-8">
                                <textarea id="share" class="form-control" name="share" placeholder="Enter Share Links"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">Blog Page</label>

                            <div class="col-sm-8">
                                <textarea id="description" class="form-control" name="description" placeholder="Enter Blog Page"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="page_title"
                                   class="col-sm-3 control-label">Page Title</label>

                            <div class="col-sm-8">
                                <textarea id="page_title" class="form-control" maxlength="60" name="page_title"
                                          placeholder="Enter Page Title"></textarea>
                                <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                        <span class = "current-value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="seo_keywords"
                                   class="col-sm-3 control-label">{{trans('messages.seo_keywords')}}</label>

                            <div class="col-sm-8">
                                <textarea id="seo_keywords" class="form-control" name="seo_keywords"
                                          placeholder="{{trans('messages.enter_seo_keywords')}}">{{old('seo_keywords')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="seo_description"
                                   class="col-sm-3 control-label">{{trans('messages.seo_description')}}</label>

                            <div class="col-sm-8">
                                <textarea id="seo_description" class="form-control" name="seo_description"
                                          placeholder="{{trans('messages.enter_seo_description')}}">{{old('seo_description')}}</textarea>
                            </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
            $('#parent_category').on('change', function () {
            $.ajax({
                url: "/api/get_blogs_sub_categories_by_category/" + $('#parent_category').val(),
                success: function (sub_categories) {
                    var $sub_category_select = $('#sub_category');
                    $sub_category_select.find('option').remove();

                    $.each(sub_categories, function (key, value) {
                        $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>');
                    });
                },
                error: function (response) {
                }
            });
        });
</script>
@stop