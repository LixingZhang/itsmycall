@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('#section-one').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-two').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-three').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#quality').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-four').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#section-five').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#section-six').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#section-seven').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#section-eight').redactor({
            imageUpload: '/admin/pages/redactor',
            imageManagerJson: '/admin/pages/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 500, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

    </script>
@stop

@section('content')

    <h3 class="page-title">
        Campaign Pages
        <small>Create Campaign Page</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/pages">Campaign Page</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/pages/create">Create</a>
            </li>

        </ul>
    </div>
    <p><strong>Note: </strong> To track Advertise Job Now / Register Page click out, you have to put onclick="register()" in the html link tag</p>
    <p><strong>Example for Advertisers: </strong>&lta class="btn btn-primary" style="height: auto;    padding: 1px 20px !important;" href="https://itsmycall.com.au/register-poster?ref=welcome" onclick="register()"&gtAdvertise Job Now&lt/a&gt</p>
    <p><strong>Example for Seekers: </strong>&lta class="btn btn-primary" style="height: auto; padding: 1px 20px !important;" href="https://itsmycall.com.au/register-user?ref=welcome" onclick="register()"&gtSearch for Jobs Now&lt/a&gt</p>

    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>{{trans('messages.create_new_page')}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                                <form action="/admin/pages/create" id="form-username" method="post"
                                      class="form-horizontal hello form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="is_active" class="col-sm-3 control-label">Active</label>
                                        <div class="col-sm-8">
                                            <input type="checkbox" id="is_active" name="is_active" checked="checked" class="form-control" value="1">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for landing on page</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code" class="form-control" name="welcome-google-adwards-tracking-code"
                                                  placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for landing on page</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code" class="form-control" name="welcome-facebook-pixel-tracking-code"
                                                  placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for Download Job Advertisers Guide</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code-download" class="form-control" name="google_download" placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for Download Job Advertisers Guide</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code-download" class="form-control" name="facebook_download" placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code for Register Page for Job Advertiser</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code-register" class="form-control" name="google_register" placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code for Register Page for Job Advertiser</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code-register" class="form-control" name="facebook_register" placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="about_page_title" type="text" class="form-control" maxlength="60" name="page_title" value="">
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="about-seokeywords" type="text" class="form-control" maxlength="60" name="seokeywords" value="">
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="about-seodescription" type="text" class="form-control" maxlength="160" name="seodescription" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">URL</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="url">
                                        <p><strong>Note: </strong>The URL should be simple text such as "test" which will automatically be converted into SiteURL/welcome/test</p>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Video</label>
                                        <div class="col-sm-8">
                                            <input id="status" class="form-control" type="text" name="video" style="color: black;" />
                                            <p class="errorvideo hidden" style="color: red;">Please enter a valid video URL</p>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Auto Play Video</label>
                                        <div class="col-sm-8">
                                            <input id="autoplay" class="form-control" type="checkbox" name="autoplay" style="color: black;" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Section One</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-one" class="form-control" name="section1"
                                                  placeholder="">
                                                    <div class="col-sm-6">
                                                        <img src="http://dev.itsmycall.com.au/uploads/images/59cfef308e4c1_file.jpeg" style="width: 100%; margin-top: 20px;">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <h3>Finally a new solution!</h3>
                                                        <p>Finding call centre staff has never been easy and with the current employment market and the increasing complexity of call centre work its never been tougher! That why we’ve set out to provide a new alternative. On ItsMyCall:
                                                        </p>
                                                        <ul>
                                                            <li>We have over 59 job categories from agents to executive roles so you can target the exact call centre skills you need. </li>
                                                            <li>We’ve got the biggest audience of contact centre professionals in Australia</li>
                                                            <li>Our prices start from an amazing $14 with our launch special.</li>
                                                        </ul>
                                                    </div>
                                                  </textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-two" class="col-sm-3 control-label">Section Two</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-two" class="form-control" name="section2"
                                                  placeholder="">
                                                    <div class="col-sm-12">
                                                        <h2>Great Prices!</h2>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <img src="http://dev.itsmycall.com.au/uploads/images/59cff3c2e99e6_file.jpeg" style="width: 100%; max-width:300px; margin-top: 5px;">
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <p>Being niche and without the large overheads of the bigger players we are able to offer some great pricing to stretch your recruiting dollar further! With our current launch specials our pricing is:
                                                        </p>
                                                        <ul>
                                                            <li>Standard Job Ad – $20.65 (normally $59)</li>
                                                            <li>Premium Job Ad – $29.75 (normally $85)</li>
                                                            <li>Featured Job Ad – $38.15 (normally $109)</li>
                                                        </ul>
                                                    </div>
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-three" class="col-sm-3 control-label">Section Three</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-three" class="form-control" name="section3"
                                                  placeholder="">
                                                        <div class="col-sm-6">
                                                            <h2>BOOST your Job Ad!</h2>
                                                            <p>With Australia’s largest audience of Contact Centre Professionals wouldn’t it be great if you could put your Job Ad directly in front of them? Well you can with one of our Boosts!!!
                                                            </p>
                                                            <p>When you place your Job Ad simply choose:
                                                            </p>
                                                            <ul>
                                                                <li>A LinkedIn Post (to over 5k senior contact centre professionals) is just $35</li>
                                                                <li>A Facebook Post (to over 13k contact centre agents) is just $35</li>
                                                                <li>A Newsletter Post (to over 1k senior contact centre professionals) is just $12.50</li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <img src="http://dev.itsmycall.com.au/uploads/images/59cff4c0ee113_file.jpeg" style="width: 100%; margin-top: 20px;">
                                                        </div>
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-four" class="col-sm-3 control-label">Section Four</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-four" class="form-control" name="section4"
                                                  placeholder="">
                                                    <div class="col-sm-6 pull-right">
                                                        <h2>Target your exact role</h2>
                                                        <p>The call centre of today is more complex than ever. We understand the diversity and nuances of the different skills and jobs that are required to be successful and that’s why we’ve ensured we’ve got the key categories you need to target the exact skills you require saving both you, and the applicant time For example:
                                                        </p>
                                                        <ul>
                                                            <li><strong>Agents</strong> – Inbound, outbound, sales, customer service, collections and more</li>
                                                            <li><strong>Workforce Optimisation</strong> – forecasters, planners, schedulers and more</li>
                                                            <li><strong>ICT</strong> – Dialler, Network and CRM Administrators, desktop support and more</li>
                                                            <li><strong>Specialist Roles</strong> including reporting, analysts, L&amp;D, coaches, quality and more</li>
                                                            <li><strong>Team Leader</strong>, Management and Executive roles</li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <img src="http://dev.itsmycall.com.au/uploads/images/59cff524969c8_file.jpeg" style="width: 100%; margin-top: 20px;">
                                                    </div>
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-five" class="col-sm-3 control-label">Section Five</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-five" class="form-control" name="section5"
                                                  placeholder="">
                                                        <div class="Advertise">
                                                            <div class="Advertise__text">
                                                                <h4>Advertise your first role in minutes!</h4>
                                                                <p>It takes just seconds to open an account and you’ll have your first job advertised in just minutes.  We’ve made it super easy to advertise a job, purchase Boosts or even purchase one of our Value Packs to save even more if you advertise jobs frequently.
                                                                </p>
                                                            </div>
                                                            <div class="Advertise__button text-center">
                                                                <a class="btn btn-primary" style="height: auto;" href="https://itsmycall.com.au/register-poster?ref=welcome" onclick="register()">Advertise Job Now</a>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="Contact" style="margin-bottom: 20px;" "="">
                                                            <div class="Contact__text">
                                                                <h4>Still got more questions?</h4>
                                                                <p>If you’d like to learn more about advertising jobs on ItsMyCall our Australian based support team are here to help:
                                                                </p>
                                                                <ul>
                                                                    <li>Contact us on 1300 ITSMYCALL (1300 487 692) or via our live chat service between 8.30am and 5.30pm AEST Monday to Friday.</li>
                                                                    <li>Download our free Job Advertisers Guide that contains all our key information to get you started.</li>
                                                                    <li>Visit the <a href="https://itsmycall.zendesk.com/hc/en-us">ItsMyCall Support Centre</a> with all of our Frequently Asked Questions, How to Guides and more.</li>
                                                                </ul>
                                                            </div>
                                                            <div class="Contact__button text-center">
                                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height: auto;">
                                                                Download Job Advertisers Guide
                                                                </button>
                                                            </div>
                                                        </div>
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-six" class="col-sm-3 control-label">Section Six</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-six" class="form-control" name="section6"
                                                  placeholder="">
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-seven" class="col-sm-3 control-label">Section Seven</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-seven" class="form-control" name="section7"
                                                  placeholder="">
                                                  </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-eight" class="col-sm-3 control-label">Section Eight</label>
                                        <div class="col-sm-8">
                                            <textarea id="section-eight" class="form-control" name="section8"
                                                  placeholder="">
                                                  </textarea>
                                        </div>
                                    </div>


                                    <h2 style="padding-left: 15px;">Select Testimonials:</h2>
                                    @foreach($testimonials as $testimonial)
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <input type="checkbox" name="testimonial[]" value="{{$testimonial->id}}">
                                        </div>
                                        <div class="col-sm-2"><img style="width: 100%" src="/uploads/images/{{$testimonial->image}}"></div>
                                        <div class="col-sm-9">
                                            <p>{!!$testimonial->html!!}</p>
                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <a onclick="hello()" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
    };
    var myId;
    function hello() {
        var myUrl = $('#status').val();
        if(myUrl.length > 0){
            myId = getId(myUrl);
            $('#status').val(myId);
            if(myId == 'error'){
                $('.errorvideo').removeClass('hidden');
            }else{
                $('.hello').submit()
            }
        }else{
           $('.hello').submit()
        }
    };


</script>
@stop