@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#description').redactor({
                imageUpload: false,
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Email Templates
        <small>Manage Templates</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/email/all">Email Templates</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                @foreach($categories as $category) <a href="/admin/email/edit/{{$category->id}}">Email Template Edit</a> @endforeach
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Edit Template
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">

                @foreach($categories as $category)
                    <form enctype="multipart/form-data" action="/admin/email/update" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="id" value="{{$category->id}}"/>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Email Template Name</label>

                            <div class="col-sm-8">
                                <input type="text" id="name" name="category_name" required class="form-control" value="{{old('name',$category->name)}}">
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Email Template Description</label>

                            <div class="col-sm-8">
                                <textarea id="description" name="description" class="form-control" required value="{{old('description',$category->description)}}">{{old('description',$category->description)}}</textarea>
                            </div>
                        </div>
						
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop