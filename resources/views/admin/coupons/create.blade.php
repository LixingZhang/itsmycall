@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/en-au.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/css/bootstrap-datetimepicker.css" />
<script src="/assets/plugins/redactor/plugins/imagemanager.js"></script>
<script src="/assets/plugins/redactor/redactor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker({format: 'Y-MM-DD'});
        $('#datetimepicker2').datetimepicker({format: 'Y-MM-DD'});
        $('#description').redactor({
            imageUpload: '/admin/redactor',
            imageManagerJson: '/admin/redactor/images.json',
            plugins: ['imagemanager'],
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
    });
</script>
@stop

@section('content')

<h3 class="page-title">
    {{trans('messages.ads_section')}}
    <small>{{trans('messages.manage_ads')}}</small>
</h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/coupons">Coupons</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/coupons/create/">Create Coupon</a>
        </li>

    </ul>
</div>

<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-frame"></i>Create New Coupon
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form action="/admin/coupons/create" id="form-username" method="post"
                      class="form-horizontal form-bordered">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Coupon Name</label>

                        <div class="col-sm-8">
                            <input type="text" id="package_name" name="name" class="form-control" value="">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Coupon Code</label>

                        <div class="col-sm-8">
                            <input type="text" id="coupon_code" name="coupon_code" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Coupon Description</label>

                        <div class="col-sm-8">
                            <textarea id="description" name="description" class="form-control" value=""></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Date Begin</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="date_begin" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Date End</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' name="date_end" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Job Level Discount Percentage:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="job_discount_rate" name="job_discount_rate" class="form-control" value="0">
                            <p class="job_perc hidden"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Boost Discount Percentage:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="boost_discount_rate" name="boost_discount_rate" class="form-control" value="0">
                            <p class="boost_perc hidden"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Value Packs Discount Percentage:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="packages_discount_rate" name="packages_discount_rate" class="form-control" value="0">
                            <p class="packages_perc hidden"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Screening Questions Discount Percentage:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="question_discount_rate" name="question_discount_rate" class="form-control" value="0">
                            <p class="question_perc hidden"></p>
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Exclusive</label>
                        <div class="col-sm-8">
                            <select id="partialgo" onchange="partialcheck()" name="exclusive" class="form-control" value="">
                                <option value="yes" >Yes (No Registration Codes will apply)</option>
                                <option value="no">No (Any Registration Code can be used with it)</option>
                                <option value="partial">Partial (Allow custom Registration code exclusive to this Coupon Code only)</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group hidden" id="partialcheck">
                        <label for="code" class="col-sm-3 control-label">Registration Code</label>
                        <div class="col-sm-8">
                            <select name="referral" class="form-control" value="">
                            @foreach($referrals as $referral)
                                <option value="{{$referral->referral_code}}">{{$referral->referral_code}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="is_active" class="col-sm-3 control-label">Active</label>
                        <div class="col-sm-8">
                            <input type="checkbox" id="is_active" name="is_active" class="form-control" value="1">
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}} </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<script>
    $( "#job_discount_rate" ).keyup(function() {
        $( ".job_perc" ).removeClass('hidden');
        var var1 = $( "#job_discount_rate" ).val();
        $( ".job_perc" ).html('Job Level Discount Percentage: '+ Number(var1).toFixed(2)+'%');
    });
    $( "#question_discount_rate" ).keyup(function() {
        $( ".question_perc" ).removeClass('hidden');
        var var1 = $( "#question_discount_rate" ).val();
        $( ".question_perc" ).html('Screening Questions Discount Percentage: '+ Number(var1).toFixed(2)+'%');
    });
    $( "#boost_discount_rate" ).keyup(function() {
        $( ".boost_perc" ).removeClass('hidden');
        var var1 = $( "#boost_discount_rate" ).val();
        $( ".boost_perc" ).html('Boost Discount Percentage: '+ Number(var1).toFixed(2)+'%');
    });
    $( "#packages_discount_rate" ).keyup(function() {
        $( ".packages_perc" ).removeClass('hidden');
        var var1 = $( "#packages_discount_rate" ).val();
        $( ".packages_perc" ).html('Value Packs Discount Percentage: '+ Number(var1).toFixed(2)+'%');
    });
function partialcheck() {
    var partial = document.getElementById("partialgo").value;
    if(partial == 'partial'){
        document.getElementById("partialcheck").classList.remove("hidden");
    }else{
       document.getElementById("partialcheck").classList.add("hidden"); 
    }
    
}
</script>
@stop