@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/en-au.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/css/bootstrap-datetimepicker.css" />
<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
<script src="/assets/plugins/redactor/redactor.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
         $('#datetimepicker1').datetimepicker({format: 'Y-MM-DD' });
         $('#datetimepicker2').datetimepicker({format: 'Y-MM-DD' });
        $('#description').redactor({
            imageUpload: '/admin/redactor',
            imageManagerJson: '/admin/redactor/images.json',
            plugins: ['imagemanager'],
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
    });
</script>
@stop

@section('content')


<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/coupons">Coupons</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/coupons/edit/{{$ad->id}}">Edit Coupon</a>
        </li>

    </ul>
</div>


<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-frame"></i>Edit Coupon
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form action="/admin/coupons/update" id="form-username" method="post"
                      class="form-horizontal form-bordered">

                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>

                        <div class="col-sm-4">
                            @include('admin.layouts.notify')
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="id" value="{{$ad->id}}"/>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Coupon Name</label>

                        <div class="col-sm-8">
                            <input type="text" id="package_name" name="name" class="form-control" value="{{old('name',$ad->name)}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Coupon Code</label>

                        <div class="col-sm-8">
                            <input type="text" id="coupon_code" name="coupon_code" class="form-control" value="{{old('coupon_code',$ad->coupon_code)}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Coupon Description</label>

                        <div class="col-sm-8">
                            <textarea id="description" name="description" class="form-control" value="">{{old('description',$ad->description)}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Date Begin</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="date_begin" value="{{old('date_begin',$ad->date_begin)}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Date End</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' class="form-control" name="date_end" value="{{old('date_end',$ad->date_end)}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Job Level Discount Rate:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="job_discount_rate" name="job_discount_rate" class="form-control" value="{{$ad->job_discount_rate}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Boost Discount Rate:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="boost_discount_rate" name="boost_discount_rate" class="form-control" value="{{$ad->boost_discount_rate}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Value Packs Discount Rate:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="packages_discount_rate" name="packages_discount_rate" class="form-control" value="{{$ad->packages_discount_rate}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Screening Questions Discount Rate:</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="packages_discount_rate" name="question_discount_rate" class="form-control" value="{{$ad->question_discount_rate}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="exclusive" class="col-sm-3 control-label">Exclusive</label>
                        <div class="col-sm-8">
                            <select name="exclusive" id="partialgo" onchange="partialcheck()" class="form-control" value="{{old('exclusive',$ad->exclusive)}}">
                                <option value="yes" <?php if($ad->exclusive == 'yes') { echo 'selected="selected"'; } ?>>Yes (No Registration Codes will apply)</option>
                                <option value="no" <?php if($ad->exclusive == 'no') { echo 'selected="selected"'; } ?>>No (Any Registration Code can be used with it)</option>
                                <option value="partial" <?php if($ad->exclusive == 'partial') { echo 'selected="selected"'; } ?>>Partial (Allow custom Registration code exclusive to this Coupon Code only)</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group <?php if($ad->exclusive == 'partial') { }else{ echo 'hidden'; } ?>" id="partialcheck">
                        <label for="code" class="col-sm-3 control-label">Registration Code</label>
                        <div class="col-sm-8">
                            <select name="referral" class="form-control" value="">
                            @foreach($referrals as $referral)
                                <option value="{{$referral->referral_code}}" <?php if($ad->referral_code == $referral->referral_code) { echo 'selected="selected"'; } ?>>{{$referral->referral_code}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="is_active" class="col-sm-3 control-label">Active</label>
                        <div class="col-sm-8">
                            <input type="checkbox" id="is_active" name="is_active" class="form-control" {{!!$ad->is_active ? 'checked' : ''}} value="1">
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}} </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<script>
function partialcheck() {
    var partial = document.getElementById("partialgo").value;
    if(partial == 'partial'){
        document.getElementById("partialcheck").classList.remove("hidden");
    }else{
       document.getElementById("partialcheck").classList.add("hidden"); 
    }
    
}
</script>
@stop