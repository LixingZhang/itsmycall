@extends('admin.layouts.master')


@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable').DataTable({
                responsive: true
            });
             yadcf.init(myTable,[
                {
                    column_number: 9,
                    column_data_type: 'html',
                    html_data_type: 'text',
                    filter_default_label: 'Select Validity',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 10,
                    column_data_type: 'html',
                    html_data_type: 'text',
                    filter_default_label: 'Select Status',
                    filter_match_mode: 'exact'
                }
            ]);
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Coupons
        <small>Manage Coupons</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/coupons">All Coupons</a>
            </li>

        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Coupons
                    </div>
                    <div class="actions">
                        <a href="/admin/coupons/create" class="btn red">
                            <i class="fa fa-plus"></i> Create New Coupon </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover " id="datatable">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>Description</th>
                            <th>Job Level Discount Rate</th>
                            <th>Boosts Discount Rate</th>
                            <th>Value Packs Discount Rate</th>
                            <th>Screening Questions Discount Rate</th>
                            <th>Coupon Code</th>
                            <th>Date Begin</th>
                            <th>Date End</th>
                            <th>Validity</th>
                            <th>Status</th>
                            <th>Used Count</th>
                            <th>Exclusive</th>
                            <th>Partial Registration Code</th>
                            <th>Job Posts</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                            <tr>
                                <td> {{$ad->id}} </td>
                                <td> {!!$ad->description!!} </td>
                                <td> @if($ad->job_discount_rate){{$ad->job_discount_rate}} @else N/A @endif </td>
                                <td> @if($ad->boost_discount_rate){{$ad->boost_discount_rate}} @else N/A @endif </td>
                                <td> @if($ad->packages_discount_rate){{$ad->packages_discount_rate}} @else N/A @endif </td>
                                <td> @if($ad->question_discount_rate){{$ad->question_discount_rate}} @else N/A @endif </td>
                                <td> {{$ad->coupon_code}} </td>
                                <td>
                                <?php
                                $start = new \Carbon\Carbon($ad->date_begin);
                                $end = new \Carbon\Carbon($ad->date_end);
                                $date_begin = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start);
                                $date_end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $end);
                                $expires = new \Carbon\Carbon($ad->date_end);
                                $now = \Carbon\Carbon::now();
                                $difference = ($expires->diffForHumans());
                                ?>
                                <span class="hidden">{{\Carbon\Carbon::parse($ad->date_begin)->format('YYYY/mm/dd')}}</span>
                                {{$date_begin->format('d M Y')}} </td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($ad->date_end)->format('YYYY/mm/dd')}}</span> {{$date_end->format('d M Y')}} </td>
                                <td><label class="label {{$ad->getStatus() === \App\Coupons::STATUS_VALID ? 'label-success' : 'label-danger'}}"><strong>{{$ad->getStatus()}}</strong></label></td>
                                <td> {{$ad->is_active ? 'Active' : 'Inactive'}}</td>
                                <td> {{$ad->getUsedCount()}}</td>
                                <td> {{$ad->exclusive}} </td>
                                <td> @if($ad->referral_code) {{$ad->referral_code}} @else nil @endif </td>
                                <td><a href="/admin/posts/withcoupon/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">View Posts</a>
                                </td>
                                <td><a href="/admin/coupons/edit/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                </td>
                                <td><a data-href="/admin/coupons/delete/{{$ad->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_ad')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4><i class="fa fa-exclamation-triangle"></i>Delete Coupon?
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal"> {{trans('messages.cancel')}} </button>
                                    <a class="btn btn-danger btn-ok"> {{trans('messages.delete')}} </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop