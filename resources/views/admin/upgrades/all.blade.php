@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Boosts
        <small>Manage Boosts</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/Upgrades">All Boosts</a>
            </li>

        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Boosts
                    </div>
                    <div class="actions">
                        <a href="/admin/upgrades/create" class="btn red">
                            <i class="fa fa-plus"></i> Create New Boost </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>Boost name</th>
                            <th>Image</th>
                            <th>Price</th>
                            <th>Type</th>
                            <th>Number of boost per purchase</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                            <tr>
                                <td> {{$ad->id}} </td>
                                <td> {{$ad->name}} </td>
                                <?php $image = \DB::table('upgradeimages')->where('upgrade_id',$ad->id)->first(); ?>
                                <td>
                                    @if($image)
                                        <img src="{{$image->image}}" width="100">
                                    @endif
                                </td>
								<td>${{number_format($ad->price, 2, '.', '')}} </td>
                                <td> {{$ad->type}} </td>
                                <td> {{$ad->boost_number}} </td>
                                <td> {!!$ad->description!!} </td>
                                <td> {{$ad->status ? 'Active' : 'Inactive'}} </td>
                                
                                <td><a href="/admin/upgrades/edit/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                </td>
                                <td><a data-href="/admin/upgrades/delete/{{$ad->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_ad')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4><i class="fa fa-exclamation-triangle"></i>{{trans('messages.delete_ad_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal"> {{trans('messages.cancel')}} </button>
                                    <a class="btn btn-danger btn-ok"> {{trans('messages.delete')}} </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop