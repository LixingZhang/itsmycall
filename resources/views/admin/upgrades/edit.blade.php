@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#description').redactor({
                imageUpload: '/admin/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Edit Boost
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/upgrades">Boost</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/upgrades/edit/{{$ad->id}}">Edit Boost</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Edit Boost
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form enctype="multipart/form-data" action="/admin/upgrades/update" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="id" value="{{$ad->id}}"/>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Uploaded Images</label>

                            <div class="col-sm-8">
                            @foreach($images as $img)
                            <div class="col-sm-2" style="margin-bottom:5px; padding: 0;">
                            <a style="top: 1%; margin-left: 1px; position: absolute;" class="btn btn-danger btn-xs" href="/admin/upgrades/deleteimage/{{$img->id}}">X</a>
                                <a href="{{$img->image}}"><img style="width:100px; height:60px; " src="{{$img->image}}"></a>
                            </div>
                            @endforeach
                            @if(count($images) == 0)
                            <p style="margin-top: 7px;">No Images Uploaded!</p>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Boost Name</label>

                            <div class="col-sm-8">
                                <input type="text" id="name" name="name" class="form-control" value="{{old('name',$ad->name)}}">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Price</label>

                            <div class="col-sm-8">
                                <input type="number" step="any" id="price" name="price" class="form-control" value="{{old('price',$ad->price)}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Allow number of boost per purchase</label>

                            <div class="col-sm-8">
                                <input type="number" id="boost_number" name="boost_number" class="form-control" value="{{old('price',$ad->boost_number)}}">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Type</label>

                            <div class="col-sm-8">
                                <input type="text" id="type" name="type" class="form-control" value="{{old('type',$ad->type)}}">
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <textarea id="description" name="description" class="form-control" value="{{old('description',$ad->description)}}">{{old('description',$ad->description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="images" class="col-sm-3 control-label">Include example images on this boost?</label>
                            <div class="col-sm-8">
                                <input style="margin-top: 7px;" id="userimg" name="usrimg[]" type="file" multiple/><br />
                            </div>
                        </div>
						<div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Status</label>
							
                            <div class="col-sm-8">
                                <select id="status" class="form-control" name="status">
                                    <option @if($ad->status == 1) selected="selected" @endif value="1">Active</option>
                                    <option @if($ad->status == 0) selected="selected" @endif value="0">InActive</option>
                                </select>
                            </div>
                        </div>
						
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop