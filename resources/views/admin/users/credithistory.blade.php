@extends('admin.layouts.master')

@section('extra_js')
     <script type="text/javascript">
            $(document).ready(function () {
                var myTable = $('#datatable_advanced').DataTable({
                    responsive: true,
                    ordering: true,
                    order: [ 2, 'desc' ],
                });
            yadcf.init(myTable,[
                {
                    column_number: 4,
                    filter_default_label: 'Select User ID',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 6,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 5,
                    filter_default_label: 'Select User Name',
                    filter_match_mode: 'exact',

                },
            ])
            });
    </script>
@endsection

@section('content')

    <h3 class="page-title">
        Credits History
        <small>All Credits History</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">{{trans('messages.users')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/creditshistory">Credits History</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>Credits History
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Credits</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>User ID</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($activities as $activity)
                            <tr>
                                <td>${{number_format($activity->credits, 2, '.', '')}}</td>
                                <td>{{$activity->reason}}</td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($activity->date)->format('YYYY/mm/dd H:i:s')}}</span> {{\Carbon\Carbon::parse($activity->date)->format('d M Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($activity->date)->format('H:i:s')}}</td>
                                <?php 
                                    $user = \App\Users::find($activity->user_id);
                                ?>
                                <td>
                                    @if($user)
                                        {{$user->id}}
                                    @endif
                                </td>
                                <td>
                                    @if($user)
                                        {{$user->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user)
                                        {{$user->email}}
                                    @endif
                                </td>
                                <td>{{$activity->description}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop