@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 2,
                    filter_default_label: 'Select Job Title',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 3,
                    filter_default_label: 'Select Company',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 4,
                    filter_default_label: 'Select Applicant',
                    filter_match_mode: 'exact'
                }
            ]);
        })
    </script>
@stop
@section('content')

    <h3 class="page-title">
        {{trans('messages.applicants')}}
        <small>{{trans('messages.manage_applicants')}}</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/applicants">{{trans('messages.applicants')}}</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>{{trans('messages.all_applicants')}}
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Job Poster ID</th>
                            <th>Job Title</th>
                            <th>Job Company</th>
                            <th>Applicant Name</th>
                            <th>Email</th>
                            <th>Resume</th>
                            <th>Cover Note</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Reason1</th>
                            <th>Reason2</th>
                            <th>Reason3</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($applicants as $applicant)
                            <tr>
                                <td> {{$applicant->id}} </td>
                                <td> <a href="/admin/users/{{$applicant->author_id}}">{{$applicant->author_id}} </a> </td>
                                <td> {{$applicant->title}} </td>
                                <td>{{$applicant->user()->getResults() ? $applicant->user()->getResults()->company : ''}}</td>
                                <td> {{$applicant->name}} </td>
                                <td> {{$applicant->email}} </td>
                                <td><a href="{{$applicant->resume}}" target="_blank">{{basename($applicant->resume)}}</a></td>
                                <td>{{$applicant->cover_note}}</td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($applicant->date_created)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($applicant->date_created)->format('d M Y H:i:s')}}</td>
                                <td>{{$applicant->status}}</td>
                                <td>{{$applicant->reason1}}</td>
                                <td>{{$applicant->reason2}}</td>
                                <td>{{$applicant->reason3}}</td>
                                <td><a class="btn btn-danger" href="/admin/users/applicant/{{$applicant->id}}/delete">Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop