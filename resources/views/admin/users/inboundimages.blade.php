@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        })

    </script>
@endsection

@section('content')

    <h3 class="page-title">
        Connect Images
        <small>Manage Connect Images</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="admin/users/inboundimages">Connect Images</a>
            </li>

        </ul>

    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>All Connect Images
                    </div>
                    <div class="actions">
                         <form action="/admin/users/inboundimages/createvid" enctype="multipart/form-data" id="hello" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input id="status" type="text" name="url" style="color: black;" />
                        <button type="submit" id="myBtn" class="btn purple"><i
                                class="fa fa-check"></i> Add Video</button>
                        <a href="/admin/users/inboundimages/create" class="btn red">
                            <i class="fa fa-plus"></i> Add Image</a>
                        </form>

                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Video</th>
                            <th>URL</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($images as $image)
                            @if($image->video)
                            <tr>
                                <td> {{$image->id}} </td>
                                <td> </td>
                                <td>@if($image->video) <iframe width="100%" height="215" src="//www.youtube.com/embed/{{$image->video}}" frameborder="0" allowfullscreen></iframe> @endif</td>
                                <td style="word-wrap: break-word; max-width: 400px;"></td>
                                <td><a class="btn btn-danger" href="/admin/users/inboundimages/{{$image->id}}">Delete</a></td>
                            </tr>
                            @else
                            <tr>
                                <td> {{$image->id}} </td>
                                <td> <img style="max-width: 200px;" src="{{$image->image}}"> </td>
                                <td></td>
                                <td style="word-wrap: break-word; max-width: 400px;"><a href="{{$image->url}}">{{$image->url}}</a></td>
                                <td><a class="btn btn-danger" href="/admin/users/inboundimages/{{$image->id}}">Delete</a></td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

var myId;
$("#hello").submit(function(e){
   var myUrl = $('#status').val();
    myId = getId(myUrl);
    
    $('#status').val(myId);
    if(myId == 'error'){
        e.preventDefault();
    }else{
        this.form.submit()
    }
  });


</script>
@stop