@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
             yadcf.init(myTable,[
                {
                    column_number: 7,
                    column_data_type: 'html',
                    html_data_type: 'text',
                    filter_default_label: 'Select Type',
                    filter_match_mode: 'exact'
                }
            ]);
        });
    </script>
@stop


@section('content')

    <h3 class="page-title">
        {{trans('messages.users')}}
        <small>{{trans('messages.manage_users')}}</small>
    </h3>

    <h4 class="page-title">
        <?php if($message != "") { echo $message; } ?>
    </h4>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">{{trans('messages.users')}}</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>{{trans('messages.all_users')}}
                    </div>
                    <div class="actions">
                        <a href="/admin/users/create" class="btn red">
                            <i class="fa fa-plus"></i> {{trans('messages.create_new_user')}} </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{trans('messages.name')}}</th>
                            <th>State</th>
                            <th>{{trans('messages.email')}}</th>
                            <th>Credits</th>
                            <th>Aus Contact Number</th>
                            <th>{{trans('messages.created_on')}}</th>
                            <th>{{trans('messages.type')}}</th>
                            <th>{{trans('messages.advertiser_type')}}</th>
                            <th>Standard</th>
                            <th>Premium</th>
                            <th>Featured</th>
                            <th>Facebook</th>
                            <th>LinkedIn</th>
                            <th>Newsletter</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td> {{$user->id}} </td>
                                <td> {{$user->name}} </td>
                                <td> {{$user->state}} </td>
                                <td> {{$user->email}} </td>
                                <td> {{$user->credits}} </td>
                                <td> {{$user->aus_contact_member_no}} </td>
                                <td> @if($user->created_at) <span class="hidden">{{\Carbon\Carbon::parse($user->created_at)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($user->created_at)->format('d M Y H:i:s')}} @endif </td>
                                <td>@if($user->type->name) <label class="label label-info"><b> {{strtoupper($user->type->name)}}</b> @endif</label>
                                </td>
                                <td><label class="label label-primary">{{$user->advertiser_type ? \App\Users::getAdvertiserTypes()[$user->advertiser_type] : ''}}</label></td>
                                <td>{{$user->getPostsPackagesPurchasedCount(\App\PostsPackages::PACKAGE_STANDARD_ID)}}</td>
                                <td>{{$user->getPostsPackagesPurchasedCount(\App\PostsPackages::PACKAGE_PREMIUM_ID)}}</td>
                                <td>{{$user->getPostsPackagesPurchasedCount(\App\PostsPackages::PACKAGE_FEATURED_ID)}}</td>
                                <?php 
                                    $posts = App\Posts::where('author_id',$user->id)->get();
                                    $sumfacebook = 0;
                                    if(count($posts) > 0){
                                        foreach ($posts as $post) {
                                            $counts = App\PostUpgrades::where('upgrade_id',2)->where('post_id',$post->id)->get();
                                            if(count($counts) > 0){
                                              foreach ($counts as $count) {
                                               $sumfacebook = $sumfacebook + $count->count;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>{{$sumfacebook}}</td>
                                <?php 
                                    $posts = App\Posts::where('author_id',$user->id)->get();
                                    $sumlinkedin = 0;
                                    if(count($posts) > 0){
                                        foreach ($posts as $post) {
                                            $counts = App\PostUpgrades::where('upgrade_id',4)->where('post_id',$post->id)->get();
                                            if(count($counts) > 0){
                                              foreach ($counts as $count) {
                                               $sumlinkedin = $sumlinkedin + $count->count;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>{{$sumlinkedin}}</td>
                                 <?php 
                                    $posts = App\Posts::where('author_id',$user->id)->get();
                                    $sumpulse = 0;
                                    if(count($posts) > 0){
                                        foreach ($posts as $post) {
                                            $counts = App\PostUpgrades::where('upgrade_id',1)->where('post_id',$post->id)->get();
                                            if(count($counts) > 0){
                                              foreach ($counts as $count) {
                                               $sumpulse = $sumpulse + $count->count;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>{{$sumpulse}}</td>
                                <td><a href="/admin/users/edit/{{$user->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a></td>
                                <td><a data-href="/admin/users/delete/{{$user->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop