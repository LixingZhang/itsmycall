@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 4,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 2,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact'
                }
            ])
        })

    </script>
@endsection

@section('content')

    <h3 class="page-title">
        Transfer History
        <small>All Transfer Balance History</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">{{trans('messages.users')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/transferhistory">Transfer History</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>Transfer History
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>From Name</th>
                            <th>From Email</th>
                            <th>To Name</th>
                            <th>To Email</th>
                            <th>Credits Transferred</th>
                            <th>Transaction ID</th>
                            <th>Description</th>
                            <th>Date Transferred</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transfers as $transfer)
                            <tr>
                                <td> {{$transfer->id}} </td>
                                <?php
                                       $user_from = \App\Users::where('id',$transfer->from_id)->first();
                                ?>
                                <td>@if(!empty($user_from)) <a href="/admin/users/edit/{{$user_from->id}}">{{$transfer->fromname}}</a> @else {{$transfer->fromname}} @endif </td>
                                <td> {{$transfer->fromemail}} </td>
                                <td>
                                    <?php
                                       $user_to = \App\Users::where('id',$transfer->to_id)->first();
                                    ?>
                                    @if(!empty($user_to)) <a href="/admin/users/edit/{{$user_to->id}}">{{$transfer->toname}}</a> @else {{$transfer->toname}} @endif
                                </td>
                                <td> {{$transfer->toemail}} </td>
                                <td> ${{number_format($transfer->credits, 2, '.', '')}} </td>
                                <td> {{$transfer->trans_id}} </td>
                                <td> {{$transfer->description}} </td>
                                <td> 
                                    <?php
                                        $created_at = new \Carbon\Carbon($transfer->date);
                                        $posted = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $created_at);
                                    ?>
                                    <span class="hidden">{{$created_at->format('YYYY/mm/dd')}} </span>
                                    {{$posted->format('d M Y H:i:s')}}
                                 </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop