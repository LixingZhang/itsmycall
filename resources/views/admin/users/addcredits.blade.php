@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@stop

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


    <script type="text/javascript">
        $('.date-picker').datepicker({
            orientation: "left",
            autoclose: true
        });
    </script>

    <script type="text/javascript">
    $(".js-example-basic-multiple").select2({
      placeholder: "Select a User",
      allowClear: true,
    });
    $(".basic-multiple").select2({
      placeholder: "Select a Reason",
      allowClear: true,
    });
    </script>
     <script type="text/javascript">
            $(document).ready(function () {
                var myTable = $('#datatable_advanced').DataTable({
                    responsive: true,
                    ordering: true,
                    order: [ 2, 'desc' ],
                });
            yadcf.init(myTable,[
                {
                    column_number: 4,
                    filter_default_label: 'Select User ID',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 6,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 5,
                    filter_default_label: 'Select User Name',
                    filter_match_mode: 'exact',

                },
            ])
            });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Add Credits
        <small>Allocate Credits to Users</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">{{trans('messages.users')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/credits">Add Credits</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Add Credits To User
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">

                    @include('admin.layouts.notify')
                    <form action="/admin/users/credits" enctype="multipart/form-data" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Select User</label>

                            <div class="col-sm-8">
                                <select class="js-example-basic-multiple" style="width: 100%;" name="user">
                                 @foreach($users as $user)
                                  <option @if(old('user') == $user->id) selected="selected" @endif value="{{$user->id}}">({{$user->id}}) {{$user->email}} ({{$user->name}})</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="reason" class="col-sm-3 control-label">Select Reason</label>

                            <div class="col-sm-8">
                                <select class="basic-multiple" style="width: 100%;" name="reason">
                                  <option @if(old('reason') == 'Refund') selected="selected" @endif value="Refund">Refund</option>
                                  <option @if(old('reason') == 'Bonus') selected="selected" @endif value="Bonus">Bonus</option>
                                  <option @if(old('reason') == 'Invoice payment') selected="selected" @endif value="Invoice payment">Invoice payment</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="credits" class="col-sm-3 control-label">Credit amount</label>

                            <div class="col-sm-8">
                                <input id="credits" class="form-control" type="number" name="credits"  onchange="setTwoNumberDecimal(this)" min="0" step="0.25" value="0.00" placeholder="Add Credit amount"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <textarea cols="80" rows="8" name="description">{{old('description')}}</textarea>
                            </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<div class="row">
                <div class="col-md-12">
                    <h2>Credits History:</h2>
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Credits</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>User ID</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($activities as $activity)
                            <tr>
                                <td>${{number_format($activity->credits, 2, '.', '')}}</td>
                                <td>{{$activity->reason}}</td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($activity->date)->format('YYYY/mm/dd H:i:s')}}</span> {{\Carbon\Carbon::parse($activity->date)->format('d M Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($activity->date)->format('H:i:s')}}</td>
                                <?php 
                                    $user = \App\Users::find($activity->user_id);
                                ?>
                                <td>
                                    @if($user)
                                        {{$user->id}}
                                    @endif
                                </td>
                                <td>
                                    @if($user)
                                        {{$user->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user)
                                        {{$user->email}}
                                    @endif
                                </td>
                                <td>{{$activity->description}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
</div>
<script type="text/javascript">
function setTwoNumberDecimal(event) {
    $('#credits').val(parseFloat(Math.round(event.value * 100) / 100).toFixed(2));
}

</script>
@stop