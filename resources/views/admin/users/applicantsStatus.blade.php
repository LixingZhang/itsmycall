@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 3,
                    filter_default_label: 'Select Deleted',
                    filter_match_mode: 'exact'
                }
            ])
        })

    </script>
@endsection

@section('content')

    <h3 class="page-title">
        Applicants Status
        <small>{{trans('messages.manage_applicants')}}</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/applicants">{{trans('messages.applicants')}}</a>
            </li>
        </ul>
    </div>
    <p>Please note that order number '0' will appear as the default status.</p>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>{{trans('messages.all_applicants')}}
                    </div>
                    <div class="actions">
                        <a href="/admin/users/applicants_status/view" class="btn red">
                            <i class="fa fa-plus"></i> Create New Status</a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Status Name</th>
                            <th>Order</th>
                            <th>Status</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($applicantsStatus as $status)
                            <tr>
                                <td> {{$status->id}} </td>
                                <td> {{$status->status}} </td>
                                <td> {{$status->order}} </td>
                                <td> {{$status->deleted ? 'Deleted' : 'Active'}} </td>
                                <td><a class="btn btn-primary" href="/admin/users/applicants_status/{{$status->id}}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop