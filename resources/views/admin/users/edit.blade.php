@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" type="text/css"
      href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
@stop

@section('extra_js')
<script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
    $('.date-picker').datepicker({
        orientation: "left",
        autoclose: true
    });
</script>
 <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('.datatable1').DataTable({
                responsive: true,
                ordering: true,
                order: [ 1, 'desc' ],
            });
        });
</script>
     <script type="text/javascript">
            $(document).ready(function () {
                var myTable = $('.datatable2').DataTable({
                    responsive: true,
                    ordering: true,
                    order: [ 2, 'desc' ],
                });
            });
    </script>
@stop

@section('content')

<h3 class="page-title">
    Edit User: {{$user->email}} ({{$user->id}})
</h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/users">Users</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/users/edit/{{$user->id}}">{{trans('messages.edit')}} {{$user->name}} ({{$user->id}})</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-12">
        @include('admin.layouts.notify')
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$user->name}} Activity Log
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatable1" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Log</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>By</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($activities as $activity)
                            <tr>
                                <td>{{$activity->description}}</td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($activity->created_at)->format('YYYY/mm/dd H:i:s')}}</span> {{\Carbon\Carbon::parse($activity->created_at)->format('d M Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($activity->created_at)->format('H:i:s')}}</td>
                                @if($activity->admin == 1)
                                <td><span class="label label-info">Admin</span></td>
                                @else
                                <td><span class="label label-primary">User</span></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                   </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$user->name}} Change Log
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <a href="/admin/posts/userchangelog/{{$user->id}}" class="btn btn-primary">View Change Log</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$user->name}} Transactions
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <a href="/admin/transactions/{{$user->id}}" class="btn btn-primary">View Transactions</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>Add Credits to {{$user->name}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <form action="/admin/users/creditsuser" enctype="multipart/form-data" method="post"
                          class="form-horizontal form-bordered">

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="user" value="{{$user->id}}">

                        <div class="form-group">
                            <label for="reason" class="col-sm-3 control-label">Select Reason</label>

                            <div class="col-sm-8">
                                <select class="basic-multiple" style="width: 100%;" name="reason">
                                  <option @if(old('reason') == 'Refund') selected="selected" @endif value="Refund">Refund</option>
                                  <option @if(old('reason') == 'Bonus') selected="selected" @endif value="Bonus">Bonus</option>
                                  <option @if(old('reason') == 'Invoice payment') selected="selected" @endif value="Invoice payment">Invoice payment</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="credits" class="col-sm-3 control-label">Credit amount</label>

                            <div class="col-sm-8">
                                <input id="credits" class="form-control" type="number" name="credits"  onchange="setTwoNumberDecimal(this)" min="0" step="0.25" value="0.00" placeholder="Add Credit amount"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <textarea cols="80" rows="8" name="description">{{old('description')}}</textarea>
                            </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$user->name}} Credits History
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatable2" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Credits</th>
                            <th>Reason</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $history)
                            <tr>
                                <td>${{number_format($history->credits, 2, '.', '')}}</td>
                                <td>{{$history->reason}}</td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($history->date)->format('YYYY/mm/dd H:i:s')}}</span> {{\Carbon\Carbon::parse($history->date)->format('d M Y')}}</td>
                                <td>{{\Carbon\Carbon::parse($history->date)->format('H:i:s')}}</td>
                                <td>{{$history->description}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$user->name}} Welcome Credits Details
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatable2" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Welcome Bonus Allocated</th>
                            <th>Welcome Bonus Used</th>
                            <th>Welcome Bonus Remaining</th>
                            <th>Expiry Date</th>
                            <th>Registration Code Used</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>@if($welcomecred) ${{number_format($welcomecred, 2, '.', '')}} @else nil @endif </td>
                                @if(is_null($user->welcome_remaining))
                                <td>0.00</td>
                                @else
                                <td>@if($welcomecred) ${{number_format($welcomecred-$user->welcome_remaining, 2, '.', '')}} @else nil @endif</td>
                                @endif
                                <td>@if($user->welcome_remaining) ${{number_format($user->welcome_remaining, 2, '.', '')}} @else nil @endif</td>
                                <td>@if($referral) {{\Carbon\Carbon::parse($referral->bonus_end)->format('d M Y')}} @else nil @endif</td>
                                <td>@if($referral) {{$referral->referral_code}} @else nil @endif</td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$user->name}} Credit Details
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatable2" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Bonus Credits Allocated</th>
                            <th>Refund Credits Allocated</th>
                            <th>Transfer Credits Allocated</th>
                            <th>Invoice payment Credits Allocated</th>
                            <th>Total Credits Allocated</th>
                            <th>Credits Used</th>
                            <th>Credits Remaining</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>@if($bonuscred_bonus) ${{number_format($bonuscred_bonus, 2, '.', '')}} @else nil @endif </td>
                                <td>@if($bonuscred_refund) ${{number_format($bonuscred_refund, 2, '.', '')}} @else nil @endif</td>
                                <td>@if($bonuscred_transfer) ${{number_format($bonuscred_transfer, 2, '.', '')}} @else nil @endif</td>
                                <td>@if($bonuscred_invoice) ${{number_format($bonuscred_invoice, 2, '.', '')}} @else nil @endif</td>
                                <td>@if($bonuscred_transfer || $bonuscred_refund || $bonuscred_bonus) ${{number_format($bonuscred_transfer+$bonuscred_refund+$bonuscred_bonus, 2, '.', '')}} @else nil @endif</td>
                                @if(is_null($user->bonusremaining))
                                <td>0.00</td>
                                @else
                                <td>@if($bonuscred_transfer || $bonuscred_refund || $bonuscred_bonus) ${{number_format(($bonuscred_transfer+$bonuscred_refund+$bonuscred_bonus)-$user->bonusremaining, 2, '.', '')}} @else nil @endif</td>
                                @endif
                                <td>@if($user->bonusremaining) ${{number_format($user->bonusremaining, 2, '.', '')}} @else nil @endif</td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{trans('messages.edit')}} - {{$user->name}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">


                <form action="/admin/users/update" enctype="multipart/form-data" method="post"
                      class="form-horizontal form-bordered">


                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="id" value="{{$user->id}}"/>

                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label"><b>Credit Balance</b></label>

                        <div class="col-sm-8">
                            <input id="name" class="form-control" type="text" name="" disabled="disabled" placeholder="{{trans('messages.name')}}" value="${{old('credits',$user->credits)}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Auscontact Membership Number</label>
                            <div class="col-sm-8">
                                <input type="text" value="{{ $user->aus_contact_member_no }}" class="form-control" max="10">
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Invoice term</label>

                        <div class="col-sm-8">
                            <input type="number" class="form-control" name="invoice_term" @if($user->invoice_terms) value="{{$user->invoice_terms}}" @endif>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">{{trans('messages.name')}}</label>

                        <div class="col-sm-8">
                            <input id="name" class="form-control" type="text" name="name"
                                   placeholder="{{trans('messages.name')}}" value="{{old('name',$user->name)}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">{{trans('messages.email')}}</label>

                        <div class="col-sm-8">
                            <input id="email" class="form-control" type="email" name="email"
                                   placeholder="{{trans('messages.enter_email')}}"
                                   value="{{old('email',$user->email)}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">{{trans('messages.password')}}</label>

                        <div class="col-sm-8">
                            <input id="password" class="form-control" type="password" name="password"
                                   placeholder="{{trans('messages.enter_password')}}"/>
                            <span class="help-block">
                                {{trans('messages.leave_it_blank')}} </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation"
                               class="col-sm-3 control-label">{{trans('messages.confirm_password')}}</label>

                        <div class="col-sm-8">
                            <input id="password_confirmation" class="form-control" type="password"
                                   name="password_confirmation"
                                   placeholder="Confirm password"/>
                            <span class="help-block">
                                {{trans('messages.leave_it_blank')}} </span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="avatar" class="col-sm-3 control-label">User Avatar</label>

                        <div class="col-sm-8">
                            <input id="avatar" class="form-control" type="file" name="avatar"/>
                            <input type="hidden" name="old_avatar" value="{{$user->avatar}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <img style="width:100px;" src="{{$user->avatar}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="dob" class="col-sm-3 control-label">{{trans('messages.date_of_birth')}}</label>

                        <div class="col-sm-8">
                            <input id="dob" class="form-control date-picker" type="text" name="dob"
                                   value="{{$user->birthday}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="bio" class="col-sm-3 control-label">{{trans('messages.bio')}}</label>

                        <div class="col-sm-8">
                            <textarea id="bio" class="form-control" name="bio">{{$user->bio}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="gender" class="col-sm-3 control-label">{{trans('messages.gender')}}</label>

                        <div class="col-sm-8">
                            <select id="gender" class="form-control" name="gender">
                                <option {{$user->gender == \App\Users::GENDER_MALE ? "selected":""}} value="{{\App\Users::GENDER_MALE}}">
                                    {{trans('messages.male')}}
                                </option>
                                <option {{$user->gender == \App\Users::GENDER_FEMALE? "selected":""}} value="{{\App\Users::GENDER_FEMALE}}">
                                    {{trans('messages.female')}}
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mobile_no"
                               class="col-sm-3 control-label">{{trans('messages.mobile_no')}}</label>

                        <div class="col-sm-8">
                            <input id="mobile_no" class="form-control" type="text" name="mobile_no"
                                   value="{{$user->mobile_no}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fb_url"
                               class="col-sm-3 control-label">{{trans('messages.facebook_url')}}</label>

                        <div class="col-sm-8">
                            <input id="fb_url" class="form-control" type="text" name="fb_url"
                                   placeholder="{{trans('messages.enter_facebook_url')}}"
                                   value="{{old('fb_url',$user->fb_url)}}"/>
                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fb_page_url"
                               class="col-sm-3 control-label">{{trans('messages.facebook_page_url')}}</label>

                        <div class="col-sm-8">
                            <input id="fb_page_url" class="form-control" type="text" name="fb_page_url"
                                   placeholder="{{trans('messages.enter_facebook_page_url')}}"
                                   value="{{old('fb_page_url',$user->fb_page_url)}}"/>
                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="website_url"
                               class="col-sm-3 control-label">{{trans('messages.website_url')}}</label>

                        <div class="col-sm-8">
                            <input id="website_url" class="form-control" type="text" name="website_url"
                                   placeholder="{{trans('messages.website_url')}}"
                                   value="{{old('website_url',$user->website_url)}}"/>
                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="twitter_url"
                               class="col-sm-3 control-label">{{trans('messages.twitter_url')}}</label>

                        <div class="col-sm-8">
                            <input id="twitter_url" class="form-control" type="text" name="twitter_url"
                                   placeholder="{{trans('messages.enter_twitter_url')}}"
                                   value="{{old('twitter_url',$user->twitter_url)}}"/>
                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="google_plus_url"
                               class="col-sm-3 control-label">{{trans('messages.google_plus_url')}}</label>

                        <div class="col-sm-8">
                            <input id="google_plus_url" class="form-control" type="text" name="google_plus_url"
                                   placeholder="{{trans('messages.enter_google_plus_url')}}"
                                   value="{{old('google_plus_url',$user->google_plus_url)}}"/>
                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="country" class="col-sm-3 control-label">{{trans('messages.country')}}</label>

                        <div class="col-sm-8">
                            <select id="country" name="country" class="form-control">
                                @foreach($countries as $country)
                                <option {{$country->id == $user->country ? "selected":""}} value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="type" class="col-sm-3 control-label">{{trans('messages.type')}}</label>

                        <div class="col-sm-8">
                            <select id="type" class="form-control" name="type">
                                @foreach($groups as $group)
                                <option {{$group->id == $user->group->group_id ? "selected":""}} value="{{$group->id}}">{{ucfirst($group->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="advertiser_type" class="col-sm-3 control-label">{{trans('messages.advertiser_type')}}</label>
                            <div class="col-sm-8">
                                <select name="advertiser_type" id="advertiser_type" class="form-control">
                                    <option value="">Please Select</option>
                                    @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                        <option value="{{$type}}" {{$user->advertiser_type === $type || old('advertiser_type') === $type ? 'selected': ''}}>{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>

                    <div class="form-group">

                        <div class="col-md-offset-3 col-md-8">
                            <label>
                                <input name="activate" type="checkbox" {{$user->activated == 1 ? "checked":""}}>
                                {{trans('messages.activate')}} </label>
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="col-md-offset-3 col-md-8">
                            <label>
                                <input name="deleted" type="checkbox" {{$user->deleted == 1 ? "checked":""}}>
                                Deleted </label>
                        </div>

                    </div>

                    <hr>

                    <h2 class="orangeudnerline">Billing Details</h2>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Billing Company</label>
                            <div class="col-sm-8">
                            <input type="text" name="billing_business"  value="{{ $user->billing_business }}" class="form-control" placeholder="">
                        </div>
                            </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Billing Phone Number</label>
                            <div class="col-sm-8">
                            <input type="text" name="billing_number"  value="{{ $user->billing_number }}" class="form-control" placeholder="">
                        </div>
                            </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ABN</label>
                            <div class="col-sm-8">
                            <input type="text" name="billing_abn"  value="{{ $user->billing_abn }}" class="form-control" placeholder="">
                        </div>
                            </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Billing Address</label>
                            <div class="col-sm-8">
                            <input type="text" name="billing_address"  value="{{ $user->billing_address }}" class="form-control" placeholder="">
                        </div>
                            </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Billing Email</label>
                            <div class="col-sm-8">
                            <input type="text" name="billing_email"  value="{{ $user->billing_email }}" class="form-control" placeholder="">
                        </div>
                        </div>
                        <div class="white-space-10"></div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function setTwoNumberDecimal(event) {
    $('#credits').val(parseFloat(Math.round(event.value * 100) / 100).toFixed(2));
}
</script>
@stop