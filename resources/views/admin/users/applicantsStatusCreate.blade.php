@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
@stop

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $('.date-picker').datepicker({
            orientation: "left",
            autoclose: true
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Applicants Status
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
    </div>
    <p>Please note that order number '0' will appear as the default status.</p>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>{{trans('messages.edit')}}
                    </div>
                    <div class="tools">
                        <a href="javascript;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/users/applicants_status/create" enctype="multipart/form-data" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="status" class="col-sm-3 control-label">Status</label>

                            <div class="col-sm-8">
                                <input id="status" class="form-control" type="text" name="status" value="{{old('status')}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="order" class="col-sm-3 control-label">Order</label>

                            <div class="col-sm-8">
                                <input id="order" class="form-control" type="text" name="order" value=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="deleted" class="col-sm-3 control-label">Deleted</label>

                            <div class="col-sm-8">
                                <select name="deleted" id="deleted" class="form-control">
                                    <option value="0">Active</option>
                                    <option value="1">Deleted</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop