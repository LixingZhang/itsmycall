@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
      $(document).ready(function () {
        var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        yadcf.init(myTable,[
          {
            column_number: 3,
            column_data_type: 'html',
            html_data_type: 'text',
            filter_default_label: 'Select Type',
          }
        ]);
      })
    </script>
@stop
@section('content')

    <h3 class="page-title">
        {{trans('messages.inbound')}}
        <small>{{trans('messages.manage_inbound')}}</small>
    </h3>

    <h4 class="page-title">
        {{ $message ?: '' }}
    </h4>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/connect">{{trans('messages.inbound')}}</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>{{trans('messages.all_inbound_users')}}
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>User ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Profile Type</th>
                            <th>Join Date</th>
                            <th>Join Time</th>
                            <th>{{trans('messages.email')}}</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>State</th>
                            <th>Availability</th>
                            <th>{{trans('messages.category')}}</th>
                            <th>{{trans('messages.sub_category')}} 1</th>
                            <th>{{trans('messages.sub_category')}} 2</th>
                            <th>{{trans('messages.sub_category')}} 3</th>
                            <th>Current Job Title</th>
                            <th>Incentive Structure</th>
                            <th>Employment Type</th>
                            <th>Employment Status</th>
                            <th>Time In Role</th>
                            <th>Current/former Employer</th>
                            <th>Min Salary</th>
                            <th>Max Salary</th>
                            <th>Work Location</th>
                            <th>Shift Guide</th>
                            <th>CV</th>
                            <th width="500">Career Ambitions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td> {{$user->id}} </td>
                                <td> {{$user->first_name}} </td>
                                <td> {{$user->last_name}} </td>
                                <td>
                                    <label class="label label-info"><b>@if($user->profile_type == 'maximum')Advanced @else Basic @endif</b></label>
                                </td>
                                <td><span class="hidden">{{$user->updated_at->format('YYYY/mm/dd')}}</span> {{$user->updated_at->format('d M Y')}}</td>
                                <td>{{$user->updated_at->format('H:i:s')}}</td>
                                <td> {{$user->email}} </td>
                                <td>{{$user->mobile_no}}</td>
                                <td>{{$user->city}}</td>
                                <td> {{$user->state}} </td>
                                <td> {{$user->availability}} </td>
                                <td> {{\App\Categories::getCategoryById($user->category)}} </td>
                                <td> {{$user->getSubcategories(0)}} </td>
                                <td> {{$user->getSubcategories(1)}} </td>
                                <td> {{$user->getSubcategories(2)}} </td>
                                <td> {{$user->currentposition}} </td>
                                <td> @if($user->incentivestructure) {!!ucwords(str_replace("_", " ", implode('<br><br> ', json_decode($user->incentivestructure, true))))!!} @else nil @endif</td>
                                <td> {{$user->employment_type}} </td>
                                <td> {{$user->employment_term}} </td>
                                <td>{{$user->timeinrole}}</td>
                                <td> {{$user->lastwork}} </td>
                                <td>${{number_format($user->wage_min)}}</td>
                                <td>{{$user->wage_max == '200000+' ? '$200,000+': '$' . number_format($user->wage_max)}}</td>
                                <td> 
                                <?php 
                                $jsonString = $user->workfromhome;
                                $keys = json_decode($jsonString);
                                 ?>
                                @if($keys && is_array($keys))
                                @foreach($keys as $key)
                                    <ul>
                                        @if($key == 1)
                                        <li> At business address</li>
                                        @endif
                                        @if($key == 2)
                                        <li> By mutual agreement</li>
                                        @endif
                                        @if($key == 3) 
                                        <li> Work from home </li>
                                        @endif
                                    </ul>
                                @endforeach
                                @else
                                nil
                                @endif
                                </td>
                                <td> 
                                <?php 
                                $jsonString2 = $user->parentsoption;
                                $keys2 = json_decode($jsonString2);
                                 ?>
                                @if($keys2 && is_array($keys2))
                                @foreach($keys2 as $key)
                                    <ul>
                                        @if($key == 1)
                                        <li> Standard (9am-5pm Mon to Fri)</li>
                                        @endif
                                        @if($key == 2)
                                        <li>Extended (8am-8pm Mon to Fri)</li>
                                        @endif
                                        @if($key == 3) 
                                        <li>Parent Friendly (9am-3pm Mon to Fri) </li>
                                        @endif
                                        @if($key == 4) 
                                        <li>Afternoon (12pm-12am Mon to Fri)</li>
                                        @endif
                                        @if($key == 5) 
                                        <li>Night Shift </li>
                                        @endif
                                        @if($key == 6) 
                                        <li>Rotating shifts - no weekends</li>
                                        @endif
                                        @if($key == 7) 
                                        <li>Rotating shifts - including weekend work</li>
                                        @endif
                                    </ul>
                                @endforeach
                                @else
                                nil
                                @endif
                                </td>
                                <td><a href="{{$user->resume}}" target="_blank">{{$user->resume}}</a></td>
                                <td>{{$user->ambitions}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop