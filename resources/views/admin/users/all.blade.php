@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true,
                orderClasses: false,
            });
            yadcf.init(myTable,[
                {
                    column_number: 8,
                    filter_default_label: 'Select Type',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 9,
                    filter_default_label: 'Select Advertiser Type',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 10,
                    filter_default_label: 'Select Deleted',
                    filter_match_mode: 'exact'
                },
                {
                    column_number: 1,
                    filter_default_label: 'Select User',
                    filter_match_mode: 'exact'
                },
            ])
      })
    </script>
@stop


@section('content')

    <h3 class="page-title">
        {{trans('messages.users')}}
        <small>{{trans('messages.manage_users')}}</small>
    </h3>

    <h4 class="page-title">
        <?php if($message != "") { echo $message; } ?>
    </h4>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">{{trans('messages.users')}}</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>{{trans('messages.all_users')}}
                    </div>
                    <div class="actions" style="padding-right: 10px; ">
                        <a href="/admin/users/credits" class="btn blue">
                            <i class="fa fa-plus"></i> Add Credits </a>
                    </div>
                    <div class="actions" style="padding-right: 10px; ">
                        <a href="/admin/users/transferbalance" class="btn green">
                             Transfer Balance </a>
                    </div>
                    <div class="actions" style="padding-right: 10px; ">
                        <a href="/admin/users/transferhistory" class="btn green">
                             Transfer History </a>
                    </div>
                    <div class="actions" style="padding-right: 10px; ">
                        <a href="/admin/users/creditshistory" class="btn yellow">
                             Credits History </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                    <?php setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>
                                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Aus Contact User</th>
                            <th>{{trans('messages.name')}}</th>
                            <th>State</th>
                            <th>{{trans('messages.email')}}</th>
                            <th>Aus Contact Membership Number</th>
                            <th>Credits</th>
                            <th>{{trans('messages.created_on')}}</th>
                            <th>{{trans('messages.type')}}</th>
                            <th>{{trans('messages.advertiser_type')}}</th>
                            <th>Deleted</th>
                            <th>Deleted At</th>
                            <th>Standard</th>
                            <th>Premium</th>
                            <th>Featured</th>
                            <th>Facebook</th>
                            <th>LinkedIn</th>
                            <th>Newsletter</th>
                            <th>Registration Code</th>
                            <th>Last Login</th>
                            <th>Welcome Bonus</th>
                            <th>Credit Amount</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td> {{$user->id}} </td>
                                <td>{{$user->aus_contact_member_no ? 'Yes' : 'No'}}</td>
                                <td> {{$user->name}} </td>
                                <td> {{$user->state}} </td>
                                <td> {{$user->email}} </td>
                                <td>{{$user->aus_contact_member_no ? $user->aus_contact_member_no : 'nil'}}</td>
                                <td> ${{number_format($user->credits, 2, '.', '')}} </td>
                                <td>@if($user->created_at) <span class="hidden">{{\Carbon\Carbon::parse($user->created_at)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($user->created_at)->format('d M Y H:i:s')}} @endif </td>
                                 <td>@if($user->type->name) @if($user->type->name == 'customer') Job Seeker @elseif($user->type->name == 'admin') Admin @else Advertiser @endif @endif
                                </td>
                                <td>{{$user->advertiser_type ? \App\Users::getAdvertiserTypes()[$user->advertiser_type] : ''}}</td>
                                <td>@if($user->deleted) {{$user->deleted ? 'Yes' : 'No'}} @endif</td>
                                <td>@if($user->deleted_at) <span class="hidden">{{\Carbon\Carbon::parse($user->deleted_at)->format('YYYY/mm/dd')}}</span> {{$user->deleted_at ? \Carbon\Carbon::parse($user->deleted_at)->format('d M Y H:i:s') : ''}} @endif</td>
                                <td>{{$user->getPostsPackagesPurchasedCount(\App\PostsPackages::PACKAGE_STANDARD_ID)}}</td>
                                <td>{{$user->getPostsPackagesPurchasedCount(\App\PostsPackages::PACKAGE_PREMIUM_ID)}}</td>
                                <td>{{$user->getPostsPackagesPurchasedCount(\App\PostsPackages::PACKAGE_FEATURED_ID)}}</td>
                                <?php 
                                    $posts = App\Posts::where('author_id',$user->id)->get();
                                    $sumfacebook = 0;
                                    if(count($posts) > 0){
                                        foreach ($posts as $post) {
                                            $counts = App\PostUpgrades::where('upgrade_id',2)->where('post_id',$post->id)->get();
                                            if(count($counts) > 0){
                                              foreach ($counts as $count) {
                                               $sumfacebook = $sumfacebook + $count->count;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>{{$sumfacebook}}</td>
                                <?php 
                                    $posts = App\Posts::where('author_id',$user->id)->get();
                                    $sumlinkedin = 0;
                                    if(count($posts) > 0){
                                        foreach ($posts as $post) {
                                            $counts = App\PostUpgrades::where('upgrade_id',4)->where('post_id',$post->id)->get();
                                            if(count($counts) > 0){
                                              foreach ($counts as $count) {
                                               $sumlinkedin = $sumlinkedin + $count->count;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>{{$sumlinkedin}}</td>
                                 <?php 
                                    $posts = App\Posts::where('author_id',$user->id)->get();
                                    $sumpulse = 0;
                                    if(count($posts) > 0){
                                        foreach ($posts as $post) {
                                            $counts = App\PostUpgrades::where('upgrade_id',1)->where('post_id',$post->id)->get();
                                            if(count($counts) > 0){
                                              foreach ($counts as $count) {
                                               $sumpulse = $sumpulse + $count->count;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>{{$sumpulse}}</td>
                                <td>
                                    <?php
                                        $referral = DB::table('referralcode')->where('id',$user->ref_id)->first();
                                     ?>
                                     @if($referral) {{$referral->referral_code}} @else nil @endif
                                </td>
                                <td>
                                    <?php
                                        if ($user->lastlogin) {
                                        $expires = new \Carbon\Carbon($user->lastlogin);
                                        $date = $expires->toFormattedDateString();
                                        $now = \Carbon\Carbon::now();
                                        $difference = ($expires->diffForHumans());
                                        }
                                     ?>
                                     @if($user->lastlogin)
                                     <span class="hidden">{{\Carbon\Carbon::parse($user->lastlogin)->format('YYYY/mm/dd H:i:s')}}</span>
                                     {{$difference}}
                                     @else
                                     <span class="hidden">0</span>
                                     Not Logged In
                                     @endif
                                </td>
                                <td>
                                    <?php
                                        $trans = App\Transactions::where('user_id',$user->id)->get();
                                        $sum = 0;
                                        foreach ($trans as $tran) {
                                          $sum = $sum+$tran->welcome_credits;
                                        }
                                     ?>
                                     {{money_format('%.2n',$sum)}}
                                </td>
                                <td>
                                    <?php
                                        $trans = App\Transactions::where('user_id',$user->id)->get();
                                        $sumc = 0;
                                        foreach ($trans as $tran) {
                                          $sumc = $sumc+$tran->creditsgiven;
                                        }
                                     ?>
                                     {{money_format('%.2n',$sumc)}}
                                </td>
                                <td><a href="/admin/users/edit/{{$user->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a></td>
                                <td><a data-href="/admin/users/delete/{{$user->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_user')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_user_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop