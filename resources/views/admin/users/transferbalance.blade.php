@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@stop

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


    <script type="text/javascript">
        $('.date-picker').datepicker({
            orientation: "left",
            autoclose: true
        });
    </script>

    <script type="text/javascript">
    $(".js-example-basic-multiple").select2({
      placeholder: "Select a User",
      allowClear: true,
    });
    $(".basic-multiple").select2({
      placeholder: "Select a Reason",
      allowClear: true,
    });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Transfer Balance
        <small>Transfer Balance from one user to another</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">{{trans('messages.users')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/transferbalance">Transfer Balance</a>
            </li>
        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Transfer Balance to another user
                    </div>
                    <div class="actions" style="padding-right: 10px; ">
                        <a href="/admin/users/transferhistory" class="btn blue">
                             Transfer History </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/users/transferbalance" enctype="multipart/form-data" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">From</label>

                            <div class="col-sm-8">
                                <select class="js-example-basic-multiple" style="width: 100%;" name="from">
                                 @foreach($users as $user)
                                  <option value="{{$user->id}}">({{$user->id}}) {{$user->email}} ({{$user->name}}) Credits = {{number_format($user->credits, 2, '.', '')}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">To</label>

                            <div class="col-sm-8">
                                <select class="js-example-basic-multiple" style="width: 100%;" name="to">
                                 @foreach($users as $user)
                                  <option value="{{$user->id}}">({{$user->id}}) {{$user->email}} ({{$user->name}}) Credits = {{number_format($user->credits, 2, '.', '')}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user" class="col-sm-3 control-label">Transfer Description</label>

                            <div class="col-sm-8">
                                <textarea cols="80" rows="8" name="description"></textarea>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> Transfer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop