@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
    <script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#description').redactor({
                imageUpload: '/admin/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Edit Value Pack
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/packages">Value Packs</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/packages/edit/{{$ad->id}}">Value Packs</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Edit Value Pack
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/packages/update" id="form-username" method="post"
                          class="form-horizontal form-bordered">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="id" value="{{$ad->id}}"/>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Value Pack Deleted</label>

                            <div class="col-sm-8">
                                <input type="checkbox" id="status" name="status" class="form-control" value="1" @if($ad->status == 1) checked="checked" @endif >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Value Pack Name</label>

                            <div class="col-sm-8">
                                <input type="text" id="package_name" name="package_name" class="form-control" value="{{old('package_name',$ad->package_name)}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Value Pack Description</label>

                            <div class="col-sm-8">
                                <p>The price text at the start of description will automatically be added. Example: Spend $350.00 <br> <i class="fa fa-warning"></i> Make sure that there are no paragraph tags in description.</p>
                                <textarea id="description" name="description" class="form-control" value="">{{old('description',$ad->description)}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Price</label>

                            <div class="col-sm-8">
                                <input type="text" id="price" name="price" class="form-control" value="{{old('price',$ad->price)}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Bonus Percentage</label>
                            <div class="col-sm-6">
                                <input type="text" id="discount_percent" name="discount_percent" class="form-control" value="{{old('discount_percent',$ad->discount_percent)}}">
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-primary" onclick="partialcheck();">Calculate</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Total Value</label>
                            <div class="col-sm-8">
                                <input type="text" disabled="disabled" id="totalvalue" name="" class="form-control" value="${{ number_format($ad->price * ($ad->discount_percent + 100)/100, 0 ,'.', ',') }}">
                                <span style="color: red" id="error" class="hidden">Please fill out the Price and Bonus Percentage Field</span>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
<script type="text/javascript">
    function partialcheck() {
    var discount = document.getElementById("discount_percent").value;
    var price = parseInt(document.getElementById("price").value);
    if(discount > 0 && price > 0){
        var totalvalue = Number(price + (price * discount/100));
        document.getElementById('totalvalue').value = '$'+totalvalue;
        document.getElementById("error").classList.add("hidden"); 
    }else if(discount == 0 && price > 0){
       document.getElementById('totalvalue').value = '$'+price;
       document.getElementById("error").classList.add("hidden"); 
    }else{
       document.getElementById("error").classList.remove("hidden");
    }
    
}
</script>
@stop