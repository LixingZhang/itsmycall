@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Value Packs
        <small>Manage Value Packs</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/packages">All Value Packs</a>
            </li>

        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Value Packs
                    </div>
                    <div class="actions">
                        <a href="/admin/packages/create" class="btn red">
                            <i class="fa fa-plus"></i> Create New Value Pack </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Bonus Percentage</th>
                            <th>Total Value</th>
                            <th>Status</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                            <tr>
                                <td> {{$ad->id}} </td>
                                <td> {{$ad->package_name}} </td>
                                <td> {!!$ad->description!!} </td>
                                <td>  ${{number_format($ad->price, 0 ,'.', ',')}} </td>
                                <td> @if($ad->discount_percent > 0) {{ number_format($ad->discount_percent, 0 ,'.', ',') }}% @else N/A @endif </td>
                                <td> @if($ad->discount_percent) ${{ number_format($ad->price * ($ad->discount_percent + 100)/100, 0 ,'.', ',') }} @else ${{$ad->price}} @endif  </td>
                                <td> @if($ad->status == 1) Deleted @else Active @endif  </td>
                                <td><a href="/admin/packages/edit/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                </td>
                                <td><a data-href="/admin/packages/delete/{{$ad->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_ad')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4><i class="fa fa-exclamation-triangle"></i>{{trans('messages.delete_ad_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal"> {{trans('messages.cancel')}} </button>
                                    <a class="btn btn-danger btn-ok"> {{trans('messages.delete')}} </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop