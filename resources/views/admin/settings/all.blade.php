@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
<script src="/assets/plugins/redactor/redactor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('#development_message').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });



        $('#business_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#home_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#agreement_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });



        $('#standard_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#exten_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#parent_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#after_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#night_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#rotno_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#rotyes_info').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });


        $('#tab_company_logo').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_company_name').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_title').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_preview').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_main').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_selling').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });



        $('#tab_video').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_photos').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });



        $('#tab_location').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_state').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_category').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_subcategory').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_salary').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_estsalary').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_hrsalinfo').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_annualsalinfo').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_freq').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_showsal').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_emptype').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_empstatus').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_workloc').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_shiftguide').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_incentive').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });


        $('#tab_applic').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_secemail').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#tab_url').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 200, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });



        $('#privacy').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#connect_program').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#max_text').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#stan_text').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#question_text').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#front_page_logo_text').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#posts_select_question_text').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#terms').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });


        $('#front').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#front_page_video').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#homestatus').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#faq').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#default_login').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#seeker_login').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#advertiser_login').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#seeker_login_verify').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#advertiser_login_verify').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#connect').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });


        $('#privacycollection').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#referral').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-one').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-two').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-three').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#quality').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-four').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#welcome-section-five').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('textarea[name="testimonial-html[]"]').each(function () {
            $(this).redactor({
                imageUpload: '/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 100, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });

        $('#adterms').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
        $('#about').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#contact').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#reguser').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#regposter').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#live_js').redactor({
            imageUpload: '/admin/settings/redactor',
            imageManagerJson: '/admin/settings/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });

        $('#regtextposter, #regtextposter, #regtextuser, #posts_purchase_text, #posts_boosts_text').redactor({
            imageUpload: '/admin/redactor',
            imageManagerJson: '/admin/redactor/images.json',
            plugins: ['imagemanager'],
            minHeight: 300, // pixels
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
              _token: "{{csrf_token()}}"
            }
        });

        $('#{{\App\Posts::COMMENT_DISQUS}}_div').hide();

        $('#comment_system').on('change', function () {
            $selected = $('#comment_system option:selected').val();


            if ($selected == "{{\App\Posts::COMMENT_FACEBOOK}}") {
                $('#{{\App\Posts::COMMENT_FACEBOOK}}_div').show();

                $('#{{\App\Posts::COMMENT_DISQUS}}_div').hide();

            }

            if ($selected == "{{\App\Posts::COMMENT_DISQUS}}") {
                $('#{{\App\Posts::COMMENT_DISQUS}}_div').show();

                $('#{{\App\Posts::COMMENT_FACEBOOK}}_div').hide();

            }

        });

        $('#comment_system').trigger('change');

        $('.testimonials').on('change', 'input[type="file"]', function () {
            var input = this;
            var image = $(this).closest('.testimonial-image').find('img');

            var reader = new FileReader();
            reader.onload = function (e) {
                image.attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        });

        $('.testimonials').on('click', '.testimonial-delete', function (e) {
            var button = $(this);
            e.preventDefault();
            if (!confirm('Are you sure you want to delete?')) {
                return;
            }
            var id = button.data('id');
            if (!id) {
                button.closest('.testimonial').remove();
                return;
            }
            $.ajax({
                method: 'DELETE',
                url: '/admin/settings/delete_testimonial',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}"
                }
            }).done(function () {
                button.closest('.testimonial').remove();
            }).fail(function () {
                console.error('Failed to delete testimonial.');
            });
        });

        $('.testimonial-add').on('click', function (e) {
            e.preventDefault();

            var html = ['<div class="testimonial">',
                          '<input type="hidden" name="testimonial-ids[]">',
                          '<div class="form-group">',
                            '<label class="col-sm-3 control-label">Testimonial Order</label>',
                            '<div class="col-sm-8">',
                              '<input class="form-control" type="number" name="testimonial-orders[]" min="1">',
                            '</div>',
                          '</div>',
                          '<div class="form-group testimonial-image">',
                            '<label class="col-sm-3 control-label">Testimonial Image</label>',
                            '<div class="col-sm-8">',
                              '<div class="row">',
                                '<div class="col-sm-3">',
                                  '<img src="" alt="placeholder image" style="width: 100%">',
                                '</div>',
                                '<div class="col-sm-9">',
                                  '<input class="form-control" name="testimonial-images[]" type="file">',
                                '</div>',
                              '</div>',
                            '</div>',
                          '</div>',
                          '<div class="form-group">',
                            '<label class="col-sm-3 control-label">Testimonial HTML</label>',
                            '<div class="col-sm-8">',
                              '<textarea name="testimonial-html[]"></textarea>',
                            '</div>',
                          '</div>',
                          '<div class="form-group">',
                            '<div class="col-sm-offset-3 col-sm-8">',
                              '<button class="btn btn-danger testimonial-delete" data-id="">Delete</button>',
                            '</div>',
                          '</div>',
                          '<hr>',
                        '</div>'].join('');
            $('.testimonials').append(html);
            $('textarea[name="testimonial-html[]"]:last').redactor({
                imageUpload: '/redactor',
                imageManagerJson: '/admin/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 100, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
        });
    });
</script>
@stop

@section('content')

<h3 class="page-title">
    {{trans('messages.settings')}}
    <small>{{trans('messages.manage_settings')}}
    </small>
</h3>


<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/settings">{{trans('messages.settings')}}</a>
        </li>
    </ul>
</div>

<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings"></i>{{trans('messages.change_settings')}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">

                @include('admin.layouts.notify')

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_general" data-toggle="tab">
                            {{trans('messages.general')}} </a>
                    </li>
                    <li>
                        <a href="#tab_homestatus" data-toggle="tab">
                            Home Page Status </a>
                    </li>
                    <li>
                        <a href="#tab_development" data-toggle="tab">
                            Development Mode </a>
                    </li>
                    <li>
                        <a href="#tab_advertising_info" data-toggle="tab">
                            Job Advertising Info Tabs </a>
                    </li>
                    <li>
                        <a href="#wl_sg_info" data-toggle="tab">
                            Work Location and Shift Guide Info </a>
                    </li>
                    <li>
                        <a href="#tab_login" data-toggle="tab">
                            Login Page </a>
                    </li>
                    <li>
                        <a href="#tab_seo" data-toggle="tab">
                            {{trans('messages.seo')}} </a>
                    </li>

                    <li>
                        <a href="#tab_social" data-toggle="tab">
                            {{trans('messages.social')}} </a>
                    </li>
                    <li>
                        <a href="#tab_about" data-toggle="tab">
                            About Page </a>
                    </li>
                    <li>
                        <a href="#tab_contact" data-toggle="tab">
                            Contact Page </a>
                    </li>
                    <li>
                        <a href="#tab_connect" data-toggle="tab">
                            Connect Program </a>
                    </li>
                    <li>
                        <a href="#tab_faq" data-toggle="tab">
                            FAQ </a>
                    </li>
                    <li>
                        <a href="#tab_quality" data-toggle="tab">
                            Search Quality Candidates </a>
                    </li>
                    <li>
                        <a href="#tab_referral" data-toggle="tab">
                            Referral Page </a>
                    </li>
                    <li>
                        <a href="#tab_terms" data-toggle="tab">
                            Website Terms of Use </a>
                    </li>
                    <li>
                        <a href="#tab_adterms" data-toggle="tab">
                            Advertiser Terms and Conditions </a>
                    </li>
                    <li>
                        <a href="#tab_privacy" data-toggle="tab">
                            Privacy Policy </a>
                    </li>
                    <li>
                        <a href="#tab_privacy_collection" data-toggle="tab">
                            Privacy Collection Policy </a>
                    </li>
                    <li>
                        <a href="#tab_custom_js" data-toggle="tab">
                            {{trans('messages.custom_js')}} </a>
                    </li>
                    <li>
                        <a href="#tab_live_js" data-toggle="tab">
                            Live Chat Javascript  </a>
                    </li>

                    <li>
                        <a href="#tab_custom_css" data-toggle="tab">
                            {{trans('messages.custom_css')}} </a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane fade active in" id="tab_general">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_general" id="form-username" method="post"
                                      class="form-horizontal form-bordered" enctype="multipart/form-data">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group">
                                        <label for="site_url"
                                               class="col-sm-3 control-label">{{trans('messages.site_url')}}</label>

                                        <div class="col-sm-8">
                                            <input id="site_url" class="form-control" type="text" name="site_url"
                                                   placeholder="{{URL::to('/')}}"
                                                   value="{{old('site_url',$general->site_url)}}"/>
                                            <span class="help-block"> {{trans('messages.site_url_should_start_with_etc')}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="site_title"
                                               class="col-sm-3 control-label">{{trans('messages.site_title')}}</label>

                                        <div class="col-sm-8">
                                            <input id="site_title" maxlenght="60" class="form-control" type="text"
                                                   name="site_title"
                                                   placeholder="{{trans('messages.enter_site_title')}}"
                                                   value="{{old('site_title',$general->site_title)}}"/>
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label">Value Packs default invoice term</label>

                                    <div class="col-sm-8">
                                        <?php $current_term = \App\Settings::where('column_key','invoice_terms')->first();
                                              $invoice_first_warning = \App\Settings::where('column_key','invoice_first_warning')->first();
                                              $invoice_first_warning_text = \App\Settings::where('column_key','invoice_first_warning_text')->first();
                                              $invoice_second_warning = \App\Settings::where('column_key','invoice_second_warning')->first();
                                              $invoice_second_warning_text = \App\Settings::where('column_key','invoice_second_warning_text')->first();
                                         ?>
                                        <input type="number" class="form-control" name="invoice_terms" @if($current_term) value="{{$current_term->value_string}}" @endif>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label">Value Pack Invoice First warning</label>

                                    <div class="col-sm-8">
                                        <small style="color: red;">Appears when Entered days are remaining for Invoice Term to end.</small>
                                        <input type="number" class="form-control" name="invoice_first_warning" @if($invoice_first_warning) value="{{$invoice_first_warning->value_string}}" @endif>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label">Value Pack Invoice First warning Text</label>

                                    <div class="col-sm-8">
                                        <small style="color: red;">The warning text should start after "Please ensure payment is received by due date"</small>
                                        <textarea id="invoice_first_warning_text" rows="6" cols="50" class="form-control" name="invoice_first_warning_text"> @if($invoice_first_warning_text) {{$invoice_first_warning_text->value_txt}} @endif </textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label">Value Pack Invoice Second warning</label>

                                    <div class="col-sm-8">
                                        <small style="color: red;">Appears when Entered days are remaining for Invoice Term to end</small>
                                        <input type="number" class="form-control" name="invoice_second_warning" @if($invoice_second_warning) value="{{$invoice_second_warning->value_string}}" @endif>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code" class="col-sm-3 control-label">Value Pack Invoice Second warning Text</label>

                                    <div class="col-sm-8">
                                        <small style="color: red;">The warning text should start after "Please Note: Your invoice is due for payment in remaining days. "</small>
                                        <textarea id="invoice_second_warning_text" rows="6" cols="50" class="form-control" name="invoice_second_warning_text"> @if($invoice_second_warning_text) {{$invoice_second_warning_text->value_txt}} @endif </textarea>
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <label for="analytics_code"
                                               class="col-sm-3 control-label">{{trans('messages.google_analytics_code')}}</label>

                                        <div class="col-sm-8">
                                            <textarea id="analytics_code" rows="6" cols="50" class="form-control" name="analytics_code"
                                                      placeholder="{{trans('messages.enter_google_analytics_code')}}">{{old('analytics_code',$general->analytics_code)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="analytics_code"
                                               class="col-sm-3 control-label">Facebook Events Code</label>
                                        <?php
                                       $desc =  \App\Settings::where('category','general')->where('column_key','facebook_pixel_tracking_code')->first();
                                       ?>
                                        <div class="col-sm-8">
                                            <textarea id="newfacebook" rows="6" cols="50" class="form-control" name="newfacebook"
                                                      placeholder="Facebook Pixel Code">@if(!empty($desc)) {{ $desc->value_txt }} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="mailchimp_form"
                                               class="col-sm-3 control-label">{{trans('messages.mailchimp_signup_form')}}</label>

                                        <div class="col-sm-8">
                                            <textarea id="mailchimp_form" class="form-control" name="mailchimp_form"
                                                      placeholder="{{trans('messages.enter_mailchimp_signup_form_code')}} ">{{old('mailchimp_form',$general->mailchimp_form)}}</textarea>
                                            <span class="help-block"> {{trans('messages.know_more_abt_mailchimp')}}
                                                <a
                                                    href="http://kb.mailchimp.com/lists/signup-forms/add-a-signup-form-to-your-website">{{trans('messages.here')}}</a></span>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label for="logo_120"
                                               class="col-sm-3 control-label">{{trans('messages.logo_120_120')}}</label>

                                        <div class="col-sm-8">
                                            <input id="logo_120" class="form-control" name="logo_120" type="file"/>
                                        </div>
                                    </div>

                                    @if(strlen($general->logo_120) > 0)
                                    <div class="form-group">
                                        <label for="logo_120"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="logo_120_value"
                                                   value="{{$general->logo_120}}"/>
                                        <div class="col-sm-8">
                                            <img src="{{$general->logo_120}}"/>
                                        </div>
                                    </div>
                                    @endif


                                    <div class="form-group">
                                        <label for="favicon"
                                               class="col-sm-3 control-label">{{trans('messages.upload_favicon')}}</label>

                                        <div class="col-sm-8">
                                            <input type="hidden" name="favicon_value"
                                                   value="{{$general->favicon}}"/>
                                            <input id="favicon" class="form-control" name="favicon" type="file"/>
                                        </div>
                                    </div>

                                    @if(strlen($general->favicon)>0)
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <img src="{{$general->favicon}}"/>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <label>
                                                <input {{($general->generate_sitemap == 1)?'checked':''}}
                                                name="generate_sitemap"
                                                type="checkbox"> {{trans('messages.generate_sitemap')}}
                                            </label>
                                            <span class="help-block"> {{trans('messages.generate_sitemap_help')}} {{URL::to('/').'/sitemap.xml'}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <label>
                                                <input {{$general->generate_rss_feeds == 1?'checked':''}}
                                                name="generate_rss_feeds"
                                                type="checkbox"> {{trans('messages.generate_rss_feeds')}}
                                            </label>
                                            <span class="help-block"> {{trans('messages.generate_rss_feeds_help')}} {{URL::to('/').'/rss.xml'}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="card_fee"
                                               class="col-sm-3 control-label">Card Fee %</label>

                                        <div class="col-sm-8">
                                            <input id="youtube_channel_url" class="form-control" type="text"
                                                   name="card_fee"
                                                   placeholder="In percentage"
                                                   value="{{old('card_fee',$general->card_fee)}}"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="front_page_logo_text"
                                               class="col-sm-3 control-label">Front Page Logo Text</label>
                                        <?php $front_page_logo_text = \App\Settings::where('column_key','front_page_logo_text')->first(); ?>
                                        <div class="col-sm-8">
                                            <textarea id="front_page_logo_text" class="form-control" name="front_page_logo_text"
                                                      placeholder="Front Page Logo Text">@if($front_page_logo_text) {{$front_page_logo_text->value_txt}} @endif </textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="front_page_text"
                                               class="col-sm-3 control-label">Front Page Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="front" class="form-control" name="front_page_text"
                                                      placeholder="">{{old('front_page_text',$general->front_page_text)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="front_page_text"
                                               class="col-sm-3 control-label">Front Page Video</label>

                                        <div class="col-sm-8">
                                            <small><strong>Sample Code:</strong> &ltiframe width="100%" height="400" src="https://www.youtube.com/embed/D-E6pt5V8ds" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen="">
                                            &lt/iframe&gt <br> <strong>Width should remain 100% and height can be adjusted as needed.</strong></small>
                                            <textarea id="front_page_video" class="form-control" name="front_page_video"
                                                      placeholder="">{{$general->front_page_video}}</textarea>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="general-conversion"
                                               class="col-sm-3 control-label">General Conversion Code</label>

                                        <div class="col-sm-8">
                                            <textarea id="general-conversion" class="form-control" name="general_conversion"
                                                      placeholder="">{{old('general_conversion',$general->general_conversion)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="reguser"
                                               class="col-sm-3 control-label">User Registration Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="reguser" class="form-control" name="register_user"
                                                      placeholder="">{{old('register_user',$general->register_user)}}</textarea>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label for="reguser-conversion"
                                               class="col-sm-3 control-label">User Registration Conversion Code</label>

                                        <div class="col-sm-8">
                                            <textarea id="reguser-conversion" class="form-control" name="register_user_conversion"
                                                      placeholder="">{{old('register_user_conversion',$general->register_user_conversion)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="after-reguser-conversion"
                                               class="col-sm-3 control-label">After User Registration Conversion Code</label>

                                        <div class="col-sm-8">
                                            <textarea id="after-reguser-conversion" class="form-control" name="after_register_user_conversion"
                                                      placeholder="">{{old('after_register_user_conversion',$general->after_register_user_conversion)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="regposter"
                                               class="col-sm-3 control-label">Poster Registration Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="regposter" class="form-control" name="register_poster"
                                                      placeholder="">{{old('register_poster',$general->register_poster)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="regposter-conversion"
                                               class="col-sm-3 control-label">Poster Registration Conversion Code</label>

                                        <div class="col-sm-8">
                                            <textarea id="regposter-conversion" class="form-control" name="register_poster_conversion"
                                                      placeholder="">{{old('register_poster_conversion',$general->register_poster_conversion)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="after-regposter-conversion"
                                               class="col-sm-3 control-label">After Poster Registration Conversion Code</label>

                                        <div class="col-sm-8">
                                            <textarea id="after-regposter-conversion" class="form-control" name="after_register_poster_conversion"
                                                      placeholder="">{{old('after_register_poster_conversion',$general->after_register_poster_conversion)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="regtextuser"
                                               class="col-sm-3 control-label">User Registration Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="regtextuser" class="form-control" name="registration_text_user"
                                                      placeholder="">{{old('registration_text_user',$general->registration_text_user)}}</textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="regtextposter"
                                               class="col-sm-3 control-label">Poster Registration Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="regtextposter" class="form-control" name="registration_text_poster"
                                                      placeholder="">{{old('registration_text_poster', $general->registration_text_poster)}}</textarea>
                                        </div>
                                    </div>
                                    <?php 
                                        $maxtext =  \App\Settings::where('column_key','max_text')->first();
                                        $stantext =  \App\Settings::where('column_key','stan_text')->first();
                                        $connecttext =  \App\Settings::where('column_key','connect_program')->first();
                                    ?>
                                    <div class="form-group">
                                        <label for="connect_program"
                                               class="col-sm-3 control-label">Connect Program Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="connect_program" class="form-control" name="connect_program"
                                                      placeholder="Connect Program Text">@if($connecttext) {{$connecttext->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="max_text"
                                               class="col-sm-3 control-label">Advanced Profile Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="max_text" class="form-control" name="max_text"
                                                      placeholder="Advanced Profile Text">@if($maxtext) {{$maxtext->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="stan_text"
                                               class="col-sm-3 control-label">Basic Profile Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="stan_text" class="form-control" name="stan_text"
                                                      placeholder="Basic Profile Text">@if($stantext) {{$stantext->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="posts_purchase_text"
                                               class="col-sm-3 control-label">Value Pack Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="posts_purchase_text" class="form-control" name="posts_purchase_text"
                                                      placeholder="">{{old('posts_purchase_text', $general->posts_purchase_text)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="posts_purchase_text"
                                               class="col-sm-3 control-label">Optional Screening Questions Text</label>
                                    <?php 
                                        $posts_question_text =  \App\Settings::where('column_key','posts_question_text')->first();
                                    ?>
                                        <div class="col-sm-8">
                                            <textarea id="question_text" class="form-control" name="posts_question_text"
                                                      placeholder="">@if($posts_question_text) {{$posts_question_text->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="posts_purchase_text"
                                               class="col-sm-3 control-label">Select Screening Questions Text</label>
                                    <?php 
                                        $posts_select_question_text =  \App\Settings::where('column_key','posts_select_question_text')->first();
                                    ?>
                                        <div class="col-sm-8">
                                            <textarea id="posts_select_question_text" class="form-control" name="posts_select_question_text"
                                                      placeholder="">@if($posts_select_question_text) {{$posts_select_question_text->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="posts_boosts_text"
                                               class="col-sm-3 control-label">Posts Boosts Text</label>

                                        <div class="col-sm-8">
                                            <textarea id="posts_boosts_text" class="form-control" name="posts_boosts_text"
                                                      placeholder="">{{old('posts_boosts_text', $general->posts_boosts_text)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_seo">
                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_seo" id="form-username" method="post"
                                      class="form-horizontal form-bordered" enctype="multipart/form-data">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group progress-field">
                                        <label for="seo_keywords"
                                               class="col-sm-3 control-label">{{trans('messages.seo_keywords')}}</label>

                                        <div class="col-sm-8">
                                            <textarea id="seo_keywords" class="form-control" maxlength="160" name="seo_keywords"
                                                      placeholder="{{trans('messages.enter_seo_keywords')}}">{{old('seo_keywords',$seo->seo_keywords)}}</textarea>
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="seo_description"
                                               class="col-sm-3 control-label">{{trans('messages.seo_description')}}</label>

                                        <div class="col-sm-8">
                                            <textarea id="seo_description" class="form-control" maxlength="160" name="seo_description"
                                                      placeholder="{{trans('messages.enter_seo_description')}}">{{old('seo_description',$seo->seo_description)}}</textarea>
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="google_verify"
                                               class="col-sm-3 control-label">{{trans('messages.google_webmaster_domain_verify')}}</label>

                                        <div class="col-sm-8">
                                            <input id="google_verify" class="form-control" type="text"
                                                   name="google_verify"
                                                   placeholder="{{trans('messages.google_webmaster_domain_verify_holder')}}"
                                                   value="{{old('google_verify',$seo->google_verify)}}"/>
                                            <span class="help-block"> {{trans('messages.google_webmaster_domain_verify_help')}}</span>
                                            <label class="label label-success">&#x3C;meta name=&#x22;google-site-verification&#x22;
                                                content=&#x22;QsHIQMfsdaassq1kr8irG33KS7LoaJhZY8XLTdAQ7PA&#x22; /&#x3E;</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="bing_verify"
                                               class="col-sm-3 control-label">{{trans('messages.bing_webmaster_domain_verify')}}</label>

                                        <div class="col-sm-8">
                                            <input id="bing_verify" class="form-control" type="text"
                                                   name="bing_verify"
                                                   placeholder="{{trans('messages.bing_webmaster_domain_verify_holder')}}"
                                                   value="{{old('bing_verify',$seo->bing_verify)}}"/>
                                            <span class="help-block"> {{trans('messages.bing_webmaster_domain_verify_help')}}</span>
                                            <label class="label label-success">&#x3C;meta name=&#x22;msvalidate.01&#x22;
                                                content=&#x22;5A3A378F55B7518E3733ffS784711DC0&#x22; /&#x3E;</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="fb_app_id"
                                               class="col-sm-3 control-label">FACEBOOK APP ID</label>

                                        <div class="col-sm-8">
                                            <input id="fb_app_id" class="form-control" type="text"
                                                   name="fb_app_id"
                                                   placeholder="Paste your meta og description tag here"
                                                   value="{{old('fb_app_id', $seo->fb_app_id)}}"/>
                                            <span class="help-block">Paste your meta tag here, it will look something like below</span>
                                            <label class="label label-success">&#x3C;meta property=&#x22;fb:app_id&#x22;
                                                content=&#x22;ItsMyCall - Australia's own call centre jobs website&#x22; /&#x3E;</label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="og_url"
                                               class="col-sm-3 control-label">Default Seo URL</label>

                                        <div class="col-sm-8">
                                            <input id="og_url" class="form-control" type="text"
                                                   name="og_url"
                                                   placeholder="Paste your meta og url tag here"
                                                   value="{{old('bing_verify', $seo->og_url)}}"/>
                                            <span class="help-block">Paste your meta tag here, it will look something like below</span>
                                            <label class="label label-success">&#x3C;meta property=&#x22;og:url&#x22;
                                                content=&#x22;http://www.your-domain.com/&#x22; /&#x3E;</label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="og_image"
                                               class="col-sm-3 control-label">Default Seo Image</label>

                                        <div class="col-sm-8">
                                            <p style="padding-top: 10px;">The URL of the image for your object. It should be at least 600x315 pixels, but 1200x630 or larger is preferred (up to 5MB). Stay close to a 1.91:1 aspect ratio to avoid cropping.</p>
                                            <input type="hidden" name="og_image_value"
                                                   value="{{$seo->og_image}}"/>
                                            <input id="og_image" class="form-control" name="og_image" type="file"/>
                                        </div>
                                    </div>

                                    @if(strlen($seo->og_image)>0)
                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-8">
                                                <img src="{{$seo->og_image}}" width="300"/>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="og_url"
                                               class="col-sm-3 control-label">Default Seo Title</label>

                                        <div class="col-sm-8">
                                            <input id="og_title" class="form-control" type="text"
                                                   name="og_title"
                                                   placeholder="Paste your meta og title tag here"
                                                   value="{{old('bing_verify', $seo->og_title)}}"/>
                                            <span class="help-block">Paste your meta tag here, it will look something like below</span>
                                            <label class="label label-success">&#x3C;meta property=&#x22;og:title&#x22;
                                                content=&#x22;ItsMyCall - Australia's own call centre jobs website&#x22; /&#x3E;</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="og_description"
                                               class="col-sm-3 control-label">Default Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="og_description" class="form-control" type="text"
                                                   name="og_description"
                                                   placeholder="Paste your meta og description tag here"
                                                   value="{{old('og_description', $seo->og_description)}}"/>
                                            <span class="help-block">Paste your meta tag here, it will look something like below</span>
                                            <label class="label label-success">&#x3C;meta property=&#x22;og:description&#x22;
                                                content=&#x22;Search for all contact centre and call centre jobs in Australia from agent to executive roles with over 59 jobs types!&#x22; /&#x3E;</label>
                                        </div>
                                    </div>

                                    <?php
                                    $tweet_text = \App\Settings::where('column_key','tweet_text')->first();
                                    ?>

                                    <div class="form-group">
                                        <label for="tweet_text"
                                               class="col-sm-3 control-label">Default Tweet Text</label>

                                        <div class="col-sm-8">
                                            <input class="form-control" placeholder="Default Tweet Text" type="text" name="tweet_text" value="@if(!empty($tweet_text)){{ $tweet_text->value_txt }} @endif"/>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="default_job_image"
                                               class="col-sm-3 control-label">Default Seo Job Image</label>

                                        <div class="col-sm-8">
                                            <p style="padding-top: 10px;">The URL of the image for your object. It should be at least 600x315 pixels, but 1200x630 or larger is preferred (up to 5MB). Stay close to a 1.91:1 aspect ratio to avoid cropping.</p>
                                            <input type="hidden" name="default_job_image_value"
                                                   value="{{$seo->default_job_image}}"/>
                                            <input id="default_job_image" class="form-control" name="default_job_image" type="file"/>
                                        </div>
                                    </div>

                                    @if(strlen($seo->default_job_image) > 0)
                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-md-8">
                                                <img src="{{$seo->default_job_image}}" width="300"/>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab_homestatus">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/homestatus" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="Display"
                                               class="col-sm-3 control-label">Display</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <input type="checkbox" name="homestatuson" value="1" @if(\App\Settings::where('column_key','homestatuson')->first()->value_txt) checked="checked" @endif >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="status"
                                               class="col-sm-3 control-label">Status</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="homestatus" class="form-control" name="homestatus" placeholder="Home Page Status"> @if(\App\Settings::where('column_key','homestatus')->first()) {!!\App\Settings::where('column_key','homestatus')->first()->value_txt!!} @endif </textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_development">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/development" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="Display"
                                               class="col-sm-3 control-label">Activate</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">   <select name="status_development">
                                                <option @if(\App\Settings::where('column_key','status_development')->first() && \App\Settings::where('column_key','status_development')->first()->value_txt == 1) selected="selected" @endif value="1">Yes</option>
                                                <option @if(\App\Settings::where('column_key','status_development')->first() && \App\Settings::where('column_key','status_development')->first()->value_txt == 0) selected="selected" @endif value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="status"
                                               class="col-sm-3 control-label">Message</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="development_message" class="form-control" name="development_message" placeholder="Development Mode Message"> @if(\App\Settings::where('column_key','development_message')->first()) {!!\App\Settings::where('column_key','development_message')->first()->value_txt!!} @endif </textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>


                    <div class="tab-pane fade" id="tab_advertising_info">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/advetisingtabs" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <h3>Advertisement Details:</h3>
                                        </div>
                                    </div>
                                    <?php 
                                        $tab_company_logo = \App\Settings::where('column_key','tab_company_logo')->first();
                                        $tab_company_name = \App\Settings::where('column_key','tab_company_name')->first();
                                        $tab_title = \App\Settings::where('column_key','tab_title')->first();
                                        $tab_preview = \App\Settings::where('column_key','tab_preview')->first();
                                        $tab_main = \App\Settings::where('column_key','tab_main')->first();
                                        $tab_selling = \App\Settings::where('column_key','tab_selling')->first();

                                        $tab_video = \App\Settings::where('column_key','tab_video')->first();
                                        $tab_photos = \App\Settings::where('column_key','tab_photos')->first();

                                        $tab_location = \App\Settings::where('column_key','tab_location')->first();
                                        $tab_state = \App\Settings::where('column_key','tab_state')->first();
                                        $tab_category = \App\Settings::where('column_key','tab_category')->first();
                                        $tab_subcategory = \App\Settings::where('column_key','tab_subcategory')->first();
                                        $tab_salary = \App\Settings::where('column_key','tab_salary')->first();
                                        $tab_estsalary = \App\Settings::where('column_key','tab_estsalary')->first();
                                        $tab_hrsalinfo = \App\Settings::where('column_key','tab_hrsalinfo')->first();
                                        $tab_annualsalinfo = \App\Settings::where('column_key','tab_annualsalinfo')->first();
                                        $tab_incentive = \App\Settings::where('column_key','tab_incentive')->first();
                                        $tab_freq = \App\Settings::where('column_key','tab_freq')->first();
                                        $tab_showsal = \App\Settings::where('column_key','tab_showsal')->first();
                                        $tab_emptype = \App\Settings::where('column_key','tab_emptype')->first();
                                        $tab_empstatus = \App\Settings::where('column_key','tab_empstatus')->first();
                                        $tab_workloc = \App\Settings::where('column_key','tab_workloc')->first();
                                        $tab_shiftguide = \App\Settings::where('column_key','tab_shiftguide')->first();

                                        $tab_applic = \App\Settings::where('column_key','tab_applic')->first();
                                        $tab_secemail = \App\Settings::where('column_key','tab_secemail')->first();
                                        $tab_url = \App\Settings::where('column_key','tab_url')->first();

                                    ?>

                                    <div class="form-group">
                                        <label for="tab_company_logo"
                                               class="col-sm-3 control-label">Company Logo to display Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_company_logo" class="form-control" name="tab_company_logo" placeholder="Company Logo to display Info">@if($tab_company_logo) {{$tab_company_logo->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_company_name"
                                               class="col-sm-3 control-label">Company Name to display Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_company_name" class="form-control" name="tab_company_name" placeholder="Company Name to display Info">@if($tab_company_name) {{$tab_company_name->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_title"
                                               class="col-sm-3 control-label">Job Title Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_title" class="form-control" name="tab_title" placeholder="Job Title Info">@if($tab_title) {{$tab_title->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_preview"
                                               class="col-sm-3 control-label">Preview Description Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_preview" class="form-control" name="tab_preview" placeholder="Preview Description Info">@if($tab_preview) {{$tab_preview->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_main"
                                               class="col-sm-3 control-label">Main Description Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_main" class="form-control" name="tab_main" placeholder="Main Description Info">@if($tab_main) {{$tab_main->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_selling"
                                               class="col-sm-3 control-label">Selling Point Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_selling" class="form-control" name="tab_selling" placeholder="Selling Point Info">@if($tab_selling) {{$tab_selling->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <h3>Video & Graphics:</h3>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_video"
                                               class="col-sm-3 control-label">Include a video Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_video" class="form-control" name="tab_video" placeholder="Include a video Info">@if($tab_video) {{$tab_video->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_photos"
                                               class="col-sm-3 control-label">Job Photos to display Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_photos" class="form-control" name="tab_photos" placeholder="Job Photos to display Info">@if($tab_photos) {{$tab_photos->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <h3>Job Details:</h3>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_location"
                                               class="col-sm-3 control-label">Location Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_location" class="form-control" name="tab_location" placeholder="Job Location Info">@if($tab_location) {{$tab_location->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_state"
                                               class="col-sm-3 control-label">State Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_state" class="form-control" name="tab_state" placeholder="Job state Info">@if($tab_state) {{$tab_state->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_category"
                                               class="col-sm-3 control-label">Job Category Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_category" class="form-control" name="tab_category" placeholder="Job Category Info">@if($tab_category) {{$tab_category->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_subcategory"
                                               class="col-sm-3 control-label">Job Sub Category Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_subcategory" class="form-control" name="tab_subcategory" placeholder="Job Sub Category Info">@if($tab_subcategory) {{$tab_subcategory->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_salary"
                                               class="col-sm-3 control-label">Salary Type Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_salary" class="form-control" name="tab_salary" placeholder="Salary Type Info">@if($tab_salary) {{$tab_salary->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_estsalary"
                                               class="col-sm-3 control-label">Estimated Salary Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_estsalary" class="form-control" name="tab_estsalary" placeholder="Estimated Salary Info">@if($tab_estsalary) {{$tab_estsalary->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_hrsalinfo"
                                               class="col-sm-3 control-label">Do you want this job to appear in hourly salary searches as well? Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_hrsalinfo" class="form-control" name="tab_hrsalinfo" placeholder="Estimated Salary Info">@if($tab_hrsalinfo) {{$tab_hrsalinfo->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_annualsalinfo"
                                               class="col-sm-3 control-label">Do you want this job to appear in annual salary searches as well? Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_annualsalinfo" class="form-control" name="tab_annualsalinfo" placeholder="Estimated Salary Info">@if($tab_annualsalinfo) {{$tab_annualsalinfo->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_estsalary"
                                               class="col-sm-3 control-label">Estimated Salary Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_estsalary" class="form-control" name="tab_estsalary" placeholder="Estimated Salary Info">@if($tab_estsalary) {{$tab_estsalary->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_incentive"
                                               class="col-sm-3 control-label">Incentive Structure Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_incentive" class="form-control" name="tab_incentive" placeholder="Incentive Structure Info">@if($tab_incentive) {{$tab_incentive->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_freq"
                                               class="col-sm-3 control-label">Pay Frequency Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_freq" class="form-control" name="tab_freq" placeholder="Pay Frequency Info">@if($tab_freq) {{$tab_freq->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_showsal"
                                               class="col-sm-3 control-label">Show salary on listing Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_showsal" class="form-control" name="tab_showsal" placeholder="Show salary on listing Info">@if($tab_showsal) {{$tab_showsal->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_emptype"
                                               class="col-sm-3 control-label">Employment Type Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_emptype" class="form-control" name="tab_emptype" placeholder="Employment Type Info">@if($tab_emptype) {{$tab_emptype->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_empstatus"
                                               class="col-sm-3 control-label">Employment Status Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_empstatus" class="form-control" name="tab_empstatus" placeholder="Employment Status Info">@if($tab_empstatus) {{$tab_empstatus->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_workloc"
                                               class="col-sm-3 control-label">Work Location Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_workloc" class="form-control" name="tab_workloc" placeholder="Work Location Info">@if($tab_workloc) {{$tab_workloc->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_shiftguide"
                                               class="col-sm-3 control-label">Shift Guide Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_shiftguide" class="form-control" name="tab_shiftguide" placeholder="Shift Guide Info">@if($tab_shiftguide) {{$tab_shiftguide->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <h3>Application Submission Information:</h3>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_applic"
                                               class="col-sm-3 control-label">How would you like to receive applications Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_applic" class="form-control" name="tab_applic" placeholder="How would you like to receive applications Info">@if($tab_applic) {{$tab_applic->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_secemail"
                                               class="col-sm-3 control-label">If required, enter a second email address you'd like us to send Job Application notifications to Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_secemail" class="form-control" name="tab_secemail" placeholder="If required, enter a second email address you'd like us to send Job Application notifications to Info">@if($tab_secemail) {{$tab_secemail->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tab_url"
                                               class="col-sm-3 control-label">Please enter the URL in which your applicants will be redirected to Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="tab_url" class="form-control" name="tab_url" placeholder="Please enter the URL in which your applicants will be redirected to Info">@if($tab_url) {{$tab_url->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="wl_sg_info">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/iconsinfo" id="form-username" method="post"
                                      class="form-horizontal form-bordered" enctype="multipart/form-data">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <h3>Work Location Details:</h3>
                                        </div>
                                    </div>
                                    <?php 
                                        $business_info = \App\Settings::where('column_key','business_info')->first();
                                        $home_info = \App\Settings::where('column_key','home_info')->first();
                                        $agreement_info = \App\Settings::where('column_key','agreement_info')->first();


                                        $standard_info = \App\Settings::where('column_key','standard_info')->first();
                                        $exten_info = \App\Settings::where('column_key','exten_info')->first();
                                        $parent_info = \App\Settings::where('column_key','parent_info')->first();
                                        $after_info = \App\Settings::where('column_key','after_info')->first();
                                        $night_info = \App\Settings::where('column_key','night_info')->first();
                                        $rotno_info = \App\Settings::where('column_key','rotno_info')->first();
                                        $rotyes_info = \App\Settings::where('column_key','rotyes_info')->first();



                                        $business_info_img = \App\Settings::where('column_key','business_info_img')->first();
                                        $home_info_img = \App\Settings::where('column_key','home_info_img')->first();
                                        $agreement_info_img = \App\Settings::where('column_key','agreement_info_img')->first();


                                        $standard_info_img = \App\Settings::where('column_key','standard_info_img')->first();
                                        $exten_info_img = \App\Settings::where('column_key','exten_info_img')->first();
                                        $parent_info_img = \App\Settings::where('column_key','parent_info_img')->first();
                                        $after_info_img = \App\Settings::where('column_key','after_info_img')->first();
                                        $night_info_img = \App\Settings::where('column_key','night_info_img')->first();
                                        $rotno_info_img = \App\Settings::where('column_key','rotno_info_img')->first();
                                        $rotyes_info_img = \App\Settings::where('column_key','rotyes_info_img')->first();

                                    ?>
                                    <div class="form-group">
                                        <label for="business_info"
                                               class="col-sm-3 control-label">At Business Address Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="business_info" class="form-control" name="business_info" placeholder="At Business Address Info">@if($business_info) {{$business_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="business_info_img"
                                               class="col-sm-3 control-label">At Business Address Image</label>

                                        <div class="col-sm-8">
                                            <input id="business_info_img" class="form-control" name="business_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($business_info_img)
                                    <div class="form-group">
                                        <label for="business_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="business_info_img_value"
                                                   value="{{$business_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$business_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="agreement_info"
                                               class="col-sm-3 control-label">By Mutual Agreement Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="agreement_info" class="form-control" name="agreement_info" placeholder="By Mutual Agreement Info">@if($agreement_info) {{$agreement_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="agreement_info_img"
                                               class="col-sm-3 control-label">By Mutual Agreement Image</label>

                                        <div class="col-sm-8">
                                            <input id="agreement_info_img" class="form-control" name="agreement_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($agreement_info_img)
                                    <div class="form-group">
                                        <label for="agreement_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="agreement_info_img_value"
                                                   value="{{$agreement_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$agreement_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="home_info"
                                               class="col-sm-3 control-label">Work from home Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="home_info" class="form-control" name="home_info" placeholder="Work from home Info">@if($home_info) {{$home_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="home_info_img"
                                               class="col-sm-3 control-label">Work from home Image</label>

                                        <div class="col-sm-8">
                                            <input id="home_info_img" class="form-control" name="home_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($home_info_img)
                                    <div class="form-group">
                                        <label for="home_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="home_info_img_value"
                                                   value="{{$home_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$home_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif





                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <h3>Shift Guide Details:</h3>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="standard_info"
                                               class="col-sm-3 control-label">Standard Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="standard_info" class="form-control" name="standard_info" placeholder="Standard Info">@if($standard_info) {{$standard_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="standard_info_img"
                                               class="col-sm-3 control-label">Standard Info Image</label>

                                        <div class="col-sm-8">
                                            <input id="standard_info_img" class="form-control" name="standard_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($standard_info_img)
                                    <div class="form-group">
                                        <label for="standard_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="standard_info_img_value"
                                                   value="{{$standard_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$standard_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="exten_info"
                                               class="col-sm-3 control-label">Extended Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="exten_info" class="form-control" name="exten_info" placeholder="Extended Info">@if($exten_info) {{$exten_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exten_info_img"
                                               class="col-sm-3 control-label">Extended Info Image</label>

                                        <div class="col-sm-8">
                                            <input id="exten_info_img" class="form-control" name="exten_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($exten_info_img)
                                    <div class="form-group">
                                        <label for="exten_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="exten_info_img_value"
                                                   value="{{$exten_info_img->value_string}}"/>

                                        <div class="col-sm-8">
                                            <img width="50" src="{{$exten_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif                                    

                                    <div class="form-group">
                                        <label for="parent_info"
                                               class="col-sm-3 control-label">Parent Friendly Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="parent_info" class="form-control" name="parent_info" placeholder="Parent Friendly Info">@if($parent_info) {{$parent_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="parent_info_img"
                                               class="col-sm-3 control-label">Parent Friendly Image</label>

                                        <div class="col-sm-8">
                                            <input id="parent_info_img" class="form-control" name="parent_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($parent_info_img)
                                    <div class="form-group">
                                        <label for="parent_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="parent_info_img_value"
                                                   value="{{$parent_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$parent_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif                                    

                                    <div class="form-group">
                                        <label for="after_info"
                                               class="col-sm-3 control-label">Afternoon Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="after_info" class="form-control" name="after_info" placeholder="Afternoon Info">@if($after_info) {{$after_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="after_info_img"
                                               class="col-sm-3 control-label">Afternoon Info Image</label>

                                        <div class="col-sm-8">
                                            <input id="after_info_img" class="form-control" name="after_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($after_info_img)
                                    <div class="form-group">
                                        <label for="after_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="after_info_img_value"
                                                   value="{{$after_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$after_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif 

                                    <div class="form-group">
                                        <label for="night_info"
                                               class="col-sm-3 control-label">Night Shift Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="night_info" class="form-control" name="night_info" placeholder="Night Shift Info">@if($night_info) {{$night_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="night_info_img"
                                               class="col-sm-3 control-label">Night Shift Image</label>

                                        <div class="col-sm-8">
                                            <input id="night_info_img" class="form-control" name="night_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($night_info_img)
                                    <div class="form-group">
                                        <label for="night_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="night_info_img_value"
                                                   value="{{$night_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$night_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif                                    

                                    <div class="form-group">
                                        <label for="rotno_info"
                                               class="col-sm-3 control-label">Rotating Shift No-Weekends Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="rotno_info" class="form-control" name="rotno_info" placeholder="Rotating Shift No-Weekends Info">@if($rotno_info) {{$rotno_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="rotno_info_img"
                                               class="col-sm-3 control-label">Rotating Shift No-Weekends Image</label>

                                        <div class="col-sm-8">
                                            <input id="rotno_info_img" class="form-control" name="rotno_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($rotno_info_img)
                                    <div class="form-group">
                                        <label for="rotno_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="rotno_info_img_value"
                                                   value="{{$rotno_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$rotno_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif                                    

                                    <div class="form-group">
                                        <label for="rotyes_info"
                                               class="col-sm-3 control-label">Rotating Shift Including-Weekends Info</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="rotyes_info" class="form-control" name="rotyes_info" placeholder="Rotating Shift Including-Weekends Info">@if($rotyes_info) {{$rotyes_info->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="rotyes_info_img"
                                               class="col-sm-3 control-label">Rotating Shift Including-Weekends Image</label>

                                        <div class="col-sm-8">
                                            <input id="rotyes_info_img" class="form-control" name="rotyes_info_img" type="file"/>
                                        </div>
                                    </div>

                                    @if($rotyes_info_img)
                                    <div class="form-group">
                                        <label for="rotyes_info_img"
                                               class="col-sm-3 control-label"></label>
                                        <input type="hidden" name="rotyes_info_img_value"
                                                   value="{{$rotyes_info_img->value_string}}"/>
                                        <div class="col-sm-8">
                                            <img width="50" src="{{$rotyes_info_img->value_string}}"/>
                                        </div>
                                    </div>
                                    @endif                                    

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_login">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/loginpage" id="form-username" method="post"
                                      class="form-horizontal form-bordered" novalidate>

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <?php 
                                        $default_login = \App\Settings::where('column_key','default_login')->first();
                                        $default_login_page_title = \App\Settings::where('column_key','default_login_page_title')->first();
                                        $default_login_seokeywords = \App\Settings::where('column_key','default_login-seokeywords')->first();
                                        $default_login_seodescription = \App\Settings::where('column_key','default_login-seodescription')->first();
                                        $default_login_google = \App\Settings::where('column_key','default_login_google')->first();
                                        $default_login_facebook = \App\Settings::where('column_key','default_login_facebook')->first();

                                        $seeker_login = \App\Settings::where('column_key','seeker_login')->first();
                                        $seeker_login_page_title = \App\Settings::where('column_key','seeker_login_page_title')->first();
                                        $seeker_login_seokeywords = \App\Settings::where('column_key','seeker_login_seokeywords')->first();
                                        $seeker_login_seodescription = \App\Settings::where('column_key','seeker_login_seodescription')->first();
                                        $seeker_login_google = \App\Settings::where('column_key','seeker_login_google')->first();
                                        $seeker_login_facebook = \App\Settings::where('column_key','seeker_login_facebook')->first();


                                        $advertiser_login = \App\Settings::where('column_key','advertiser_login')->first();
                                        $advertiser_login_page_title = \App\Settings::where('column_key','advertiser_login_page_title')->first();
                                        $advertiser_login_seokeywords = \App\Settings::where('column_key','advertiser_login_seokeywords')->first();
                                        $advertiser_login_seodescription = \App\Settings::where('column_key','advertiser_login_seodescription')->first();
                                        $advertiser_login_google = \App\Settings::where('column_key','advertiser_login_google')->first();
                                        $advertiser_login_facebook = \App\Settings::where('column_key','advertiser_login_facebook')->first();



                                        $seeker_login_verify = \App\Settings::where('column_key','seeker_login_verify')->first();
                                        $seeker_login_verify_page_title = \App\Settings::where('column_key','seeker_login_verify_page_title')->first();
                                        $seeker_login_verify_seokeywords = \App\Settings::where('column_key','seeker_login_verify_seokeywords')->first();
                                        $seeker_login_verify_seodescription = \App\Settings::where('column_key','seeker_login_verify_seodescription')->first();
                                        $seeker_login_verify_google = \App\Settings::where('column_key','seeker_login_verify_google')->first();
                                        $seeker_login_verify_facebook = \App\Settings::where('column_key','seeker_login_verify_facebook')->first();



                                        $advertiser_login_verify = \App\Settings::where('column_key','advertiser_login_verify')->first();
                                        $advertiser_login_verify_page_title = \App\Settings::where('column_key','advertiser_login_verify_page_title')->first();
                                        $advertiser_login_verify_seokeywords = \App\Settings::where('column_key','advertiser_login_verify_seokeywords')->first();
                                        $advertiser_login_verify_seodescription = \App\Settings::where('column_key','advertiser_login_verify_seodescription')->first();
                                        $advertiser_login_verify_google = \App\Settings::where('column_key','advertiser_login_verify_google')->first();
                                        $advertiser_login_verify_facebook = \App\Settings::where('column_key','advertiser_login_verify_facebook')->first();
                                    ?>

                                    <div class="form-group">
                                        <label for="default_login_page_title"
                                               class="col-sm-3 control-label">Default Login Page Title</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <input id="default_login_page_title" type="text" class="form-control" maxlength="60" name="default_login_page_title" value="@if($default_login_page_title) {{$default_login_page_title->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="default_login-seokeywords"
                                               class="col-sm-3 control-label">Default Login Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="default_login-seokeywords" type="text" class="form-control" maxlength="60" name="default_login-seokeywords" value="@if($default_login_seokeywords) {{$default_login_seokeywords->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="default_login-seodescription"
                                               class="col-sm-3 control-label">Default Login Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="default_login-seodescription" type="text" class="form-control" maxlength="160" name="default_login-seodescription" value="@if($default_login_seodescription) {{$default_login_seodescription->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="default_login_google" class="col-sm-3 control-label">Google AdWords Tracking Code for Default Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="default_login_google" class="form-control" name="default_login_google" placeholder="">@if($default_login_google) {!! $default_login_google->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="default_login_facebook" class="col-sm-3 control-label">Facebook Events Code for Default Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="default_login_facebook" class="form-control" name="default_login_facebook" placeholder="">@if($default_login_facebook) {!! $default_login_facebook->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label for="status"
                                               class="col-sm-3 control-label">Job Seeker Login Text</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="seeker_login" class="form-control" name="seeker_login" placeholder="Job Seeker Login Text">@if($seeker_login) {{$seeker_login->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_page_title"
                                               class="col-sm-3 control-label">Job Seeker Login Page Title</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <input id="seeker_login_page_title" type="text" class="form-control" maxlength="60" name="seeker_login_page_title" value="@if($seeker_login_page_title) {{$seeker_login_page_title->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_seokeywords"
                                               class="col-sm-3 control-label">Job Seeker Login Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="seeker_login_seokeywords" type="text" class="form-control" maxlength="60" name="seeker_login_seokeywords" value="@if($seeker_login_seokeywords) {{$seeker_login_seokeywords->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_seodescription"
                                               class="col-sm-3 control-label">Job Seeker Login Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="seeker_login_seodescription" type="text" class="form-control" maxlength="160" name="seeker_login_seodescription" value="@if($seeker_login_seodescription) {{$seeker_login_seodescription->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_google" class="col-sm-3 control-label">Google AdWords Tracking Code for Job Seeker Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="seeker_login_google" class="form-control" name="seeker_login_google" placeholder="">@if($seeker_login_google) {!! $seeker_login_google->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_facebook" class="col-sm-3 control-label">Facebook Events Code for Job Seeker Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="seeker_login_facebook" class="form-control" name="seeker_login_facebook" placeholder="">@if($seeker_login_facebook) {!! $seeker_login_facebook->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label for="status"
                                               class="col-sm-3 control-label">Job Advertiser Login Text</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="advertiser_login" class="form-control" name="advertiser_login" placeholder="Job Advertiser Login Text">@if($advertiser_login) {{$advertiser_login->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_page_title"
                                               class="col-sm-3 control-label">Job Advertiser Login Page Title</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <input id="advertiser_login_page_title" type="text" class="form-control" maxlength="60" name="advertiser_login_page_title" value="@if($advertiser_login_page_title) {{$advertiser_login_page_title->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_seokeywords"
                                               class="col-sm-3 control-label">Job Advertiser Login Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="advertiser_login_seokeywords" type="text" class="form-control" maxlength="60" name="advertiser_login_seokeywords" value="@if($advertiser_login_seokeywords) {{$advertiser_login_seokeywords->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_seodescription"
                                               class="col-sm-3 control-label">Job Advertiser Login Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="advertiser_login_seodescription" type="text" class="form-control" maxlength="160" name="advertiser_login_seodescription" value="@if($advertiser_login_seodescription) {{$advertiser_login_seodescription->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_google" class="col-sm-3 control-label">Google AdWords Tracking Code for Job Advertiser Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="advertiser_login_google" class="form-control" name="advertiser_login_google" placeholder="">@if($advertiser_login_google) {!! $advertiser_login_google->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_facebook" class="col-sm-3 control-label">Facebook Events Code for Job Advertiser Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="advertiser_login_facebook" class="form-control" name="advertiser_login_facebook" placeholder="">@if($advertiser_login_facebook) {!! $advertiser_login_facebook->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label for="status"
                                               class="col-sm-3 control-label">Job Seeker After Verification Login Text</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="seeker_login_verify" class="form-control" name="seeker_login_verify" placeholder="Job Seeker After Verification Login Text">@if($seeker_login_verify) {{$seeker_login_verify->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_verify_page_title"
                                               class="col-sm-3 control-label">Job Seeker After Verification Login Page Title</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <input id="seeker_login_verify_page_title" type="text" class="form-control" maxlength="60" name="seeker_login_verify_page_title" value="@if($seeker_login_verify_page_title) {{$seeker_login_verify_page_title->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_verify_seokeywords"
                                               class="col-sm-3 control-label">Job Seeker After Verification Login Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="seeker_login_verify_seokeywords" type="text" class="form-control" maxlength="60" name="seeker_login_verify_seokeywords" value="@if($seeker_login_verify_seokeywords) {{$seeker_login_verify_seokeywords->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_verify_seodescription"
                                               class="col-sm-3 control-label">Job Seeker After Verification Login Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="seeker_login_verify_seodescription" type="text" class="form-control" maxlength="160" name="seeker_login_verify_seodescription" value="@if($seeker_login_verify_seodescription) {{$seeker_login_verify_seodescription->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_verify_google" class="col-sm-3 control-label">Google AdWords Tracking Code for Job Seeker After Verification Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="seeker_login_verify_google" class="form-control" name="seeker_login_verify_google" placeholder="">@if($seeker_login_verify_google) {!! $seeker_login_verify_google->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="seeker_login_verify_facebook" class="col-sm-3 control-label">Facebook Events Code for Job Seeker After Verification Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="seeker_login_verify_facebook" class="form-control" name="seeker_login_verify_facebook" placeholder="">@if($seeker_login_verify_facebook) {!! $seeker_login_verify_facebook->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label for="status"
                                               class="col-sm-3 control-label">Job Advertiser After Verification Login Text</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <textarea id="advertiser_login_verify" class="form-control" name="advertiser_login_verify" placeholder="Job Advertiser After Verification Login Text">@if($advertiser_login_verify) {{$advertiser_login_verify->value_txt}} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_verify_page_title"
                                               class="col-sm-3 control-label">Job Advertiser After Verification Login Page Title</label>

                                        <div class="col-sm-8" style="padding-top: 10px;">
                                            <input id="advertiser_login_verify_page_title" type="text" class="form-control" maxlength="60" name="advertiser_login_verify_page_title" value="@if($advertiser_login_verify_page_title) {{$advertiser_login_verify_page_title->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_verify_seokeywords"
                                               class="col-sm-3 control-label">Job Advertiser After Verification Login Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="advertiser_login_verify_seokeywords" type="text" class="form-control" maxlength="60" name="advertiser_login_verify_seokeywords" value="@if($advertiser_login_verify_seokeywords) {{$advertiser_login_verify_seokeywords->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_verify_seodescription"
                                               class="col-sm-3 control-label">Job Advertiser After Verification Login Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="advertiser_login_verify_seodescription" type="text" class="form-control" maxlength="160" name="advertiser_login_verify_seodescription" value="@if($advertiser_login_verify_seodescription) {{$advertiser_login_verify_seodescription->value_txt}} @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_verify_google" class="col-sm-3 control-label">Google AdWords Tracking Code for Job Advertiser After Verification Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="advertiser_login_verify_google" class="form-control" name="advertiser_login_verify_google" placeholder="">@if($advertiser_login_verify_google) {!! $advertiser_login_verify_google->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="advertiser_login_verify_facebook" class="col-sm-3 control-label">Facebook Events Code for Job Advertiser After Verification Login</label>
                                        <div class="col-sm-8">
                                            <textarea id="advertiser_login_verify_facebook" class="form-control" name="advertiser_login_verify_facebook" placeholder="">@if($advertiser_login_verify_facebook) {!! $advertiser_login_verify_facebook->value_txt !!} @endif</textarea>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_social">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_social" id="form-username" method="post"
                                      class="form-horizontal form-bordered" enctype="multipart/form-data">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group">
                                        <label for="fb_page_url"
                                               class="col-sm-3 control-label">{{trans('messages.facebook_page_url')}}</label>

                                        <div class="col-sm-8">
                                            <input id="fb_page_url" class="form-control" type="text"
                                                   name="fb_page_url"
                                                   placeholder="{{trans('messages.enter_facebook_page_url')}}"
                                                   value="{{old('fb_page_url',$social->fb_page_url)}}"/>
                                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="twitter_url"
                                               class="col-sm-3 control-label">{{trans('messages.twitter_url')}}</label>

                                        <div class="col-sm-8">
                                            <input id="twitter_url" class="form-control" type="text"
                                                   name="twitter_url"
                                                   placeholder="{{trans('messages.enter_twitter_url')}}"
                                                   value="{{old('twitter_url',$social->twitter_url)}}"/>
                                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="linkedin_page_url"
                                               class="col-sm-3 control-label">LinkedIn</label>

                                        <div class="col-sm-8">
                                            <input id="google_plus_page_url" class="form-control" type="text"
                                                   name="linkedin_page_url"
                                                   placeholder="Linkedin"
                                                   value="{{old('linkedin_page_url',$social->linkedin_page_url)}}"/>
                                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}}</span>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="youtube_channel_url"
                                               class="col-sm-3 control-label">{{trans('messages.youtube_channel_url')}}</label>

                                        <div class="col-sm-8">
                                            <input id="youtube_channel_url" class="form-control" type="text"
                                                   name="youtube_channel_url"
                                                   placeholder="{{trans('messages.enter_youtube_channel_url')}}"
                                                   value="{{old('youtube_channel_url',$social->youtube_channel_url)}}"/>
                                            <span class="help-block"> {{trans('messages.url_should_start_with_etc')}} </span>
                                        </div>
                                    </div>


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_about">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_about" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="about"
                                               class="col-sm-3 control-label">About Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="about" class="form-control" name="about"
                                                      placeholder="">{{old('terms',$general->about)}}</textarea>
                                        </div>
                                    </div>

                                   <div class="form-group progress-field">
                                        <label for="about_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="about_page_title" type="text" class="form-control" maxlength="60" name="about_page_title" value="{{old('about_page_title',$seo->about_page_title)}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="about-seokeywords" type="text" class="form-control" maxlength="60" name="about-seokeywords" value="{{old('about_seo_keywords',$seo->about_seo_keywords)}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="about-seodescription" type="text" class="form-control" maxlength="160" name="about-seodescription" value="{{old('about_seo_description',$seo->about_seo_description)}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_faq">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_faq" id="form-username" method="post"
                                      class="form-horizontal form-bordered">
                                    <?php
                                       $desc =  \App\Settings::where('column_key','faq')->first();
                                       $faq_seo_description =  \App\Settings::where('column_key','faq_seo_description')->first();
                                       $faq_seo_keywords =  \App\Settings::where('column_key','faq_seo_keywords')->first();
                                       $faq_page_title =  \App\Settings::where('column_key','faq_page_title')->first();
                                     ?>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="about"
                                               class="col-sm-3 control-label">FAQ Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="faq" class="form-control" name="faq" placeholder="">@if($desc) {!!$desc->value_txt!!} @endif</textarea>
                                        </div>
                                    </div>

                                   <div class="form-group progress-field">
                                        <label for="about_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="about_page_title" type="text" class="form-control" maxlength="60" name="faq_page_title" value="@if($faq_page_title) {!!$faq_page_title->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="about-seokeywords" type="text" class="form-control" maxlength="60" name="faq-seokeywords" value="@if($faq_seo_keywords) {!!$faq_seo_keywords->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="about-seodescription" type="text" class="form-control" maxlength="160" name="faq-seodescription" value="@if($faq_seo_description) {!!$faq_seo_description->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_quality">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_quality" id="form-username" method="post"
                                      class="form-horizontal form-bordered">
                                    <?php
                                       $desc3 =  \App\Settings::where('column_key','quality')->first();
                                       $quality_seo_description =  \App\Settings::where('column_key','quality_seo_description')->first();
                                       $quality_seo_keywords =  \App\Settings::where('column_key','quality_seo_keywords')->first();
                                       $quality_page_title =  \App\Settings::where('column_key','quality_page_title')->first();
                                     ?>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="about"
                                               class="col-sm-3 control-label">Search Quality Candidate Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="quality" class="form-control" name="quality" placeholder="">@if($desc3) {!!$desc3->value_txt!!} @endif</textarea>
                                        </div>
                                    </div>

                                   <div class="form-group progress-field">
                                        <label for="about_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="about_page_title" type="text" class="form-control" maxlength="60" name="quality_page_title" value="@if($quality_page_title) {!!$quality_page_title->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="about-seokeywords" type="text" class="form-control" maxlength="60" name="quality-seokeywords" value="@if($quality_seo_keywords) {!!$quality_seo_keywords->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="about-seodescription" type="text" class="form-control" maxlength="160" name="quality-seodescription" value="@if($quality_seo_description) {!!$quality_seo_description->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                <div class="tab-pane fade" id="tab_connect">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_connect" id="form-username" method="post"
                                      class="form-horizontal form-bordered">
                                    <?php
                                       $desc2 =  \App\Settings::where('column_key','connect')->first();
                                       $connect_seo_description =  \App\Settings::where('column_key','connect_seo_description')->first();
                                       $connect_seo_keywords =  \App\Settings::where('column_key','connect_seo_keywords')->first();
                                       $connect_page_title =  \App\Settings::where('column_key','connect_page_title')->first();
                                     ?>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="about"
                                               class="col-sm-3 control-label">Connect Program Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="connect" class="form-control" name="connect" placeholder="">@if($desc2) {!!$desc2->value_txt!!} @endif</textarea>
                                        </div>
                                    </div>

                                   <div class="form-group progress-field">
                                        <label for="about_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="connect_page_title" type="text" class="form-control" maxlength="60" name="connect_page_title" value="@if($connect_page_title) {!!$connect_page_title->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="about-seokeywords" type="text" class="form-control" maxlength="60" name="connect-seokeywords" value=" @if($connect_seo_keywords) {!!$connect_seo_keywords->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="about-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="about-seodescription" type="text" class="form-control" maxlength="160" name="connect-seodescription" value="@if($connect_seo_description) {!!$connect_seo_description->value_string!!} @endif">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>


                    <div class="tab-pane fade" id="tab_contact">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_contact" id="form-username" method="post"
                                      class="form-horizontal form-bordered" novalidate="novalidate">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="contactus"
                                               class="col-sm-3 control-label">Contact Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="contact" class="form-control" name="contactus"
                                                      placeholder="">{{old('contactus',$general->contactus)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="contact_page_title"
                                               class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="contact_page_title" type="text" maxlength="60" class="form-control" name="contact_page_title" value="{{old('contact_page_title',$seo->contact_page_title)}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="contact-seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="contact-seokeywords" type="text" maxlength="60" class="form-control" name="contact-seokeywords" value="{{old('contact_seo_keywords',$seo->contact_seo_keywords)}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="contact-seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="contact-seodescription" type="text" maxlength="160" class="form-control" name="contact-seodescription" value="{{old('contact_seo_description',$seo->contact_seo_description)}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact-recaptcha-site-key"
                                               class="col-sm-3 control-label">reCAPTCHA Site Key</label>

                                        <div class="col-sm-8">
                                            <input id="contact-recaptcha-site-key" type="text" maxlength="160" class="form-control" name="contact-recaptcha-site-key" value="{{old('contact_recaptcha_site_key', $general->contact_recaptcha_site_key)}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact-recaptcha-secret-key"
                                               class="col-sm-3 control-label">reCAPTCHA Secret Key</label>

                                        <div class="col-sm-8">
                                            <input id="contact-recaptcha-secret-key" type="text" maxlength="160" class="form-control" name="contact-recaptcha-secret-key" value="{{old('contact_recaptcha_secret_key', $general->contact_recaptcha_secret_key)}}">
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_referral">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_referral" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="about"
                                               class="col-sm-3 control-label">Referral Page</label>

                                        <div class="col-sm-8">
                                            <textarea id="referral" class="form-control" name="referral"
                                                      placeholder="">{{old('referral',$general->referral)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_welcome">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_welcome" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google Adwards Tracking Code</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-google-adwards-tracking-code" class="form-control" name="welcome-google-adwards-tracking-code"
                                                  placeholder="">{{old('welcome-google-adwards-tracking-code',$welcome->google_adwards_tracking_code)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-facebook-pixel-tracking-code" class="form-control" name="welcome-facebook-pixel-tracking-code"
                                                  placeholder="">{{old('welcome-facebook-pixel-tracking-code',$welcome->facebook_pixel_tracking_code)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-one" class="col-sm-3 control-label">Section One</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-one" class="form-control" name="welcome-section-one"
                                                  placeholder="">{{old('welcome-section-one',$welcome->section_one)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-section-two" class="col-sm-3 control-label">Section Two</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-two" class="form-control" name="welcome-section-two"
                                                  placeholder="">{{old('welcome-section-two',$welcome->section_two)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-three" class="col-sm-3 control-label">Section Three</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-three" class="form-control" name="welcome-section-three"
                                                  placeholder="">{{old('welcome-section-three',$welcome->section_three)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-four" class="col-sm-3 control-label">Section Four</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-four" class="form-control" name="welcome-section-four"
                                                  placeholder="">{{old('welcome-section-four',$welcome->section_four)}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="welcome-section-five" class="col-sm-3 control-label">Section Five</label>
                                        <div class="col-sm-8">
                                            <textarea id="welcome-section-five" class="form-control" name="welcome-section-five"
                                                  placeholder="">{{old('welcome-section-five',$welcome->section_five)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_testimonials">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="/admin/settings/update_testimonials" enctype="multipart/form-data" method="post" class="form-horizontal form-bordered">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Testimonial Interval (seconds)</label>
                                        <div class="col-sm-1">
                                            <input class="form-control" type="number" name="testimonial-interval" value="{{old('testimonial-interval',$welcome->testimonial_interval)}}" min="1">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="testimonials">
                                        @foreach ($testimonials as $key => $testimonial)
                                            <div class="testimonial">
                                                <input type="hidden" name="testimonial-ids[]" value="{{ $testimonial->id }}">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Testimonial Order</label>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" type="number" name="testimonial-orders[]" value="{{ $testimonial->order }}" min="1">
                                                    </div>
                                                </div>
                                                <div class="form-group testimonial-image">
                                                    <label class="col-sm-3 control-label">Testimonial Image</label>
                                                    <div class="col-sm-8">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <img src="/uploads/images/{{ $testimonial->image }}" style="width: 100%">
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <input class="form-control" name="testimonial-images[]" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Testimonial HTML</label>
                                                    <div class="col-sm-8">
                                                        <textarea name="testimonial-html[]">
                                                            {!! $testimonial->html !!}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-8">
                                                        <button class="btn btn-danger testimonial-delete" data-id="{{ $testimonial->id }}">Delete</button>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="button" class="btn green testimonial-add"><i class="fa fa-plus"></i>
                                                    Add
                                                </button>
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab_terms">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_terms" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="privacy"
                                               class="col-sm-3 control-label">Website Terms of Use</label>

                                        <div class="col-sm-8">
                                            <textarea id="terms" class="form-control" name="terms"
                                                      placeholder="">{{old('terms',$general->terms)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>


                    <div class="tab-pane fade" id="tab_adterms">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_adterms" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="privacy"
                                               class="col-sm-3 control-label">Advertiser Terms & Conditions</label>

                                        <div class="col-sm-8">
                                            <textarea id="adterms" class="form-control" name="adterms"
                                                      placeholder="">{{old('terms',$general->adterms)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_privacy">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_privacy" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="privacy"
                                               class="col-sm-3 control-label">Privacy Policy</label>

                                        <div class="col-sm-8">
                                            <textarea id="privacy" class="form-control" name="privacy"
                                                      placeholder="">{{old('privacy',$general->privacy)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>


                    <div class="tab-pane fade" id="tab_privacy_collection">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_privacy_collection" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="privacycollection"
                                               class="col-sm-3 control-label">Privacy Collection Policy</label>

                                        <div class="col-sm-8">
                                            <textarea id="privacycollection" class="form-control" name="privacycollection"
                                                      placeholder="">{{old('privacy',$general->privacycollection)}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_custom_js">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_custom_js" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group">
                                        <label for="custom_js"
                                               class="col-sm-3 control-label">{{trans('messages.custom_js_code')}}</label>

                                        <div class="col-sm-8">
                                            <textarea rows="10" cols="10" id="custom_js" class="form-control"
                                                      name="custom_js"
                                                      placeholder="{{trans('messages.custom_js_code')}}">{{$custom_js->custom_js}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                <div class="tab-pane fade" id="tab_live_js">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_live_js" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group">
                                        <label for="custom_js"
                                               class="col-sm-3 control-label">{{trans('messages.custom_js_code')}}</label>

                                        <div class="col-sm-8">
                                            <textarea rows="10" cols="10" id="" class="form-control"
                                                      name="live_js"
                                                      placeholder="Custom Live Chat Javascript"><?php
                                                                    $desc = \App\Settings::where('category','live_js')->first();
                                                                 ?>
                                                    @if($desc)
                                                  {!!$desc->value_txt!!}  
                                                    @endif            
                                                 </textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="tab_custom_css">

                        <div class="row">
                            <div class="col-md-12">

                                <form action="/admin/settings/update_custom_css" id="form-username" method="post"
                                      class="form-horizontal form-bordered">

                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                    <div class="form-group">
                                        <label for="custom_css"
                                               class="col-sm-3 control-label">{{trans('messages.custom_css_code')}}</label>

                                        <div class="col-sm-8">
                                            <textarea rows="10" cols="10" id="custom_css" class="form-control"
                                                      name="custom_css"
                                                      placeholder="{{trans('messages.custom_css_code')}}">{{$custom_css->custom_css}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                    {{trans('messages.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('.progress-field textarea').keyup(function() {
                var box = $(this).val();
                var limit = $(this).attr('maxlength');
                var main = box.length * 100;
                var value = (main / limit);
                var count = 0 + value;
                var reverse_count = 100 - value;
                if(box.length >= 0){
                    $(this).siblings().children().css('width', count + '%');
                    //$('.current-value').text(count + '%');
                    $('.count').text(reverse_count);

                    if (count >= 60 && count < 101){
                        $(this).siblings().children().removeClass('progress-bar-danger').addClass('progress-bar-warning');
                    }
                    if(count >= 0 && count < 60){
                        $(this).siblings().children().removeClass('progress-bar-danger');
                        $(this).siblings().children().removeClass('progress-bar-warning');
                        $(this).siblings().children().addClass('progress-bar-success')
                    }

                }
                return false;
            });
            $('.progress-field input').keyup(function() {
                var box = $(this).val();
                var limit = $(this).attr('maxlength');
                var main = box.length * 100;
                var value = (main / limit);
                var count = 0 + value;
                var reverse_count = 100 - value;
                if(box.length >= 0){
                    $(this).siblings().children().css('width', count + '%');
                    //$('.current-value').text(count + '%');
                    $('.count').text(reverse_count);

                    if (count >= 60 && count < 101){
                        $(this).siblings().children().removeClass('progress-bar-danger').addClass('progress-bar-warning');
                    }
                    if(count >= 0 && count < 60){
                        $(this).siblings().children().removeClass('progress-bar-danger');
                        $(this).siblings().children().removeClass('progress-bar-warning');
                        $(this).siblings().children().addClass('progress-bar-success')
                    }

                }
                return false;
            });
        });
    </script>
@stop
