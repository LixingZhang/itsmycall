@extends('admin.layouts.master')

@section('extra_js')
<script type="text/javascript">
    $(document).ready(function () {
        var myTable = $('#datatable_advanced').DataTable({
                responsive: true,
                order: [ 0, 'desc' ]
            });
        yadcf.init(myTable,[
            {
                column_number: 3,
                filter_default_label: 'Select Deleted',
                filter_match_mode: 'exact'

            },
            {
                column_number: 2,
                filter_default_label: 'Select Email',
                filter_match_mode: 'exact'

            },
            {
                column_number: 4,
                filter_default_label: 'Select Company',
                filter_match_mode: 'exact'

            },
            {
                column_number: 10,
                filter_default_label: 'Select Status',
                filter_match_mode: 'exact'

            }
        ])
    })
</script>
@stop

@section('content')

<h3 class="page-title">
    Purchased Boosts
    <small>View Purchased Boosts</small>
</h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/packages">Purchased Boosts</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>Purchased Boosts
                </div>
            </div>

            <div class="portlet-body">

                @include('admin.layouts.notify')
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                    <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>User name</th>
                            <th>User email</th>
                            <th>User Deleted</th>
                            <th>Company Name</th>
                            <th>Job</th>
                            <th>Job Level</th>
                            <th>Logo</th>
                            <th>Job Location</th>
                            <th>Job Published On</th>
                            <th>Boost Status</th>
                            <th>Boost Details (Name - Purchased Boost Date / Time - Status - Social Media Platform)</th>
                            <th>View Boost List</th>
                            <th>Add Boosts</th>
                            {{--<th>Edit</th>--}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                            <?php 
                                  $user = \App\Users::find($post->author_id);
                                  $joblevel = \App\PostsPackages::where('id',$post->posts_packages)->first();
                             ?>
                            <tr>
                                <td>{{$post->id}}</td>
                                <td><a href="/admin/users/edit/{{$user->id}}">{{$user->name}}</a></td>
                                <td>{{$user->email}}</td>
                                <td> @if($user->deleted == 1) Deleted @else Active @endif </td>
                                <td> {{$post->company}} </td>
                                <td> <a href="/admin/posts/edit/{{$post->id}}">{{$post->title}}</a> </td>
                                <td>
                                @if($joblevel)
                                {{$joblevel->name}}
                                @else
                                nil
                                @endif
                                </td>
                                <td> <img style="max-width: 100px;" src="{{$post->featured_image}}"> </td>
                                <td> {{$post->joblocation}} </td>
                                <td> <span class="hidden">{{\Carbon\Carbon::parse($post->created_at)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($post->created_at)->format('d M Y H:i:s')}} </td>
                                <?php $boosts = \App\PostUpgradesLists::where('post_id',$post->id)->get(); 
                                      $active = \App\PostUpgradesLists::where('post_id',$post->id)->where('post_status','not_actioned')->count(); 
                                      $inprogress = \App\PostUpgradesLists::where('post_id',$post->id)->where('post_status','in_progress')->count(); 
                                      $completed = \App\PostUpgradesLists::where('post_id',$post->id)->where('post_status','completed')->count(); 
                                ?>
                                <td>
                                  @if($active > 0)
                                  Active
                                  @elseif($inprogress > 0)
                                  InProgress
                                  @else
                                  Completed
                                  @endif
                                </td>
                                <td>
                                    @foreach($boosts as $boost)
                                        <?php $upgrade = \App\Upgrades::find($boost->boost_id);$post_upgrade = \App\PostUpgrades::find($boost->post_upgrades_id); ?>
                                        @if($upgrade)
                                        <p style="width: 650px;">{{$upgrade->name}} - <span class="label label-warning label-sm">@if($post_upgrade->is_purchased == 1) ${{number_format($boost->boost_cost, 2, '.', '')}}@else Free @endif</span> - <span class="label label-info label-sm">{{\Carbon\Carbon::parse($post_upgrade->created_at)->format('d M Y H:i:s')}}</span> - <span class="label label-danger label-sm">{{\App\PostUpgradesLists::$status[$boost->post_status]}}</span> - <span class="label label-default label-sm">{{$boost->post_edition}}</span></p>
                                        @endif
                                    @endforeach
                                </td>
                                <td><a href="/admin/boost_manage/list/{{$post->id}}" class="btn btn-primary btn-sm">{{trans('messages.view')}}</a></td>
                                <td><a href="/admin/boost_manage/list/add/{{$post->id}}" class="btn btn-primary btn-sm">Create Boosts</a></td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
@stop