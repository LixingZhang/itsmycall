@extends('admin.layouts.master')


@section('content')
    <?php
        $job = \DB::table('posts')->where('id',$id)->first();
    ?>
    <h3 class="page-title">
        Create Boost for Job: @if($job) {{$job->title}} ({{$job->id}}) @endif
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/boost_manage/all">Purchased Boosts</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/boost_manage/list/add/{{$id}}">Create Boost</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Create Boost
                    </div>
                    <div class="tools">
                        <a href="javascript:" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/boost_manage/list/add/{{$id}}" method="post"
                          class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div class="form-group">
                            <label for="upgrade_id" class="col-sm-3 control-label">Boost Type</label>

                            <div class="col-sm-8">
                                <select id="upgrade_id" class="form-control" name="upgrade_id">
                                    @foreach($upgrades as $upgrade)
                                        <option value="{{$upgrade->id}}" {{old('upgrade_id') == $upgrade->id ? 'selected' : ''}}>{{$upgrade->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="count" class="col-sm-3 control-label">Insert Number of free Boosts</label>

                            <div class="col-sm-8">
                                <input id="count" class="form-control" type="number" name="count"
                                       placeholder="Number of free Boosts" value="{{old('count')}}"/>
                            </div>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> Create Boosts</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop