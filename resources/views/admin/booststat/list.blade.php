@extends('admin.layouts.master')


@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true,
                order: [ 0, 'desc' ]
            });
            yadcf.init(myTable,[
                {
                    column_number: 1,
                    filter_default_label: 'Select Type',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 15,
                    filter_default_label: 'Select Status',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 9,
                    filter_default_label: 'Select Platform',
                    filter_match_mode: 'exact',

                }
            ])
        });
    </script>
@stop

@section('content')
    <?php
            $job = \DB::table('posts')->where('id',$id)->first();
    ?>
    <h3 class="page-title">
        Boost List for Job: @if($job) {{$job->title}} ({{$job->id}}) @endif
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/boost_manage/all">Purchased Boost</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
               <a href="/admin/boost_manage/list/{{$id}}">Boost List For Job:@if($job) {{$job->id}} @endif</a>
            </li>

        </ul>
    </div>
    <label class="label label-danger label-md" style="background: rgb(255, 179, 194);
        color: black;
        display: block;
        max-width: 200px;
        margin-bottom: 10px;">If Boost has status Not Actioned</label>
    <label class="label label-danger label-md" style="background: lightyellow;
        color: black;
        display: block;
        max-width: 200px;
        margin-bottom: 10px;">If Boost has status In Progress</label>
    <label class="label label-danger label-md" style="background: #b6ffb6;
        color: black;
        display: block;
        max-width: 200px;
        margin-bottom: 10px;">If Boost has status Completed</label>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Boosts List
                    </div>
                    @if($job)
                        <div class="actions">
                        <a href="/admin/posts/edit/{{$job->id}}" class="btn red">
                            <i class="fa fa-edit"></i> Edit Job </a>
                        </div>
                    @endif
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>Boost Type</th>
                            <th>Job</th>
                            <th>Job Level</th>
                            <th>Company</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Purchased Boost Date / Time</th>
                            <th>Boost Posted Date / Time</th>
                            <th>Social Media Platform</th>
                            <th>Credits Used for Purchase</th>
                            <th>$ received for Boost</th>
                            <th>$ spent on Boost</th>
                            <th>Boost Views</th>
                            <th>Boost Clicks</th>
                            <th>Boost Status</th>
                            <th>Feedback to Job Advertiser</th>
                            <th>Internal comments only</th>
                            <th>Facebook Post Image</th>
                            <th>LinkedIn Post Image</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($postsUpgradeLists as $list)
                            <?php $boostname = \DB::table('upgrades')->where('id',$list->boost_id)->first();
                                $post_upgrade = \App\PostUpgrades::where('id',$list->post_upgrades_id)->first();
                             ?>
                            <tr @if($list->post_status == 'completed') style="background: #b6ffb6;" @elseif($list->post_status == 'in_progress') style="background: lightyellow;" @else style="background: rgb(255, 179, 194);" @endif>
                                <td> {{$list->id}} </td>
                                <td> @if($boostname) {{$boostname->name}} @endif </td>
                                <td><a href="/admin/posts/edit/{{$list->getJob()->id}}">{{$list->getJob()->title}}</a></td>
                                <td>{{$list->getPostPackage()->title}}</td>
                                <td>{{$list->getJob()->company}}</td>
                                <td><a href="/admin/users/edit/{{$list->getUser()->id}}">{{$list->getUser()->name}}</a></td>
                                <td>{{$list->getUser()->email}}</td>
                                <td> @if($post_upgrade) <span class="hidden">{{\Carbon\Carbon::parse($post_upgrade->created_at)->format('YYYY/mm/dd')}}</span>
                                 {{\Carbon\Carbon::parse($post_upgrade->created_at)->format('d M Y  H:i:s')}} @endif </td>
                                <td> <span class="hidden">{{\Carbon\Carbon::parse($list->post_datetime)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($list->post_datetime)->format('d M Y H:i:s')}} </td>
                                <td>{{$list->post_edition}}</td>
                                <td>{{$post_upgrade->isCreditsUsed()}}</td>
                                <td>
                                    <?php
                                      $pricetotal = \App\PostUpgrades::where('id',$list->post_upgrades_id)->first();
                                     ?>
                                     @if($pricetotal) 
                                        @if($pricetotal->is_purchased == 1) 
                                           ${{number_format($list->boost_cost, 2, '.', '')}}
                                        @else 
                                        Free 
                                        @endif
                                     @endif

                                </td>
                                <td> ${{$list->post_cost}}</td>
                                <td> {{$list->post_views}}</td>
                                <td> {{$list->post_clicks}} </td>
                                <td> {{\App\PostUpgradesLists::$status[$list->post_status]}}</td>
                                <td>{{$list->post_comment}}</td>
                                <td>{{$list->internal_comment}}</td>
                                @if($post_upgrade->upgrade_id == 2)
                                    <td>
                                        @if(!empty($list->boost_image))
                                            <img src="{{$list->boost_image}}" style="max-width: 100px;" />
                                        @else
                                        nil
                                        @endif
                                    </td>
                                    <td>
                                        nil
                                    </td>
                                @elseif($post_upgrade->upgrade_id == 4)
                                    <td>
                                        nil
                                    </td>
                                    <td>
                                        @if(!empty($list->boost_image))
                                            <img src="{{$list->boost_image}}" style="max-width: 100px;" />
                                        @else
                                        nil
                                        @endif
                                    </td>
                                @else
                                    <td>
                                        nil
                                    </td>
                                    <td>
                                        nil
                                    </td>
                                @endif
                                <td><a href="/admin/boost_manage/list/delete/{{$list->id}}/{{$list->post_id}}"
                                       class="btn btn-danger btn-sm">Delete</a></td>
                                <td><a href="/admin/boost_manage/list/edit/{{$list->id}}"
                                       class="btn btn-info btn-sm">{{trans('messages.edit')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@stop