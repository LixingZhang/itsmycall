@extends('admin.layouts.master')


@section('content')

    <h3 class="page-title">
        <?php
            $post_upgrade = DB::table('posts_upgrades')->where('id',$postsUpgradeList->post_upgrades_id)->first();
            $boostname = NULL;
            if($post_upgrade){
                $boostname = \DB::table('upgrades')->where('id',$post_upgrade->upgrade_id)->first();
            }
            $job = \DB::table('posts')->where('id',$postsUpgradeList->post_id)->first();
        ?>
        Edit @if($boostname) {{$boostname->name}} @endif Boost for Job: @if($job) {{$job->title}} ({{$job->id}}) @endif
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/boost_manage/all">Purchased Boost</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/boost_manage/list/{{$job->id}}">Boost List For Job:@if($job) {{$job->id}} @endif</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/boost_manage/list/edit/{{$postsUpgradeList->id}}">Edit @if($boostname) {{$boostname->name}} @endif Boost for Job: @if($job) {{$job->id}} @endif</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Edit @if($boostname) {{$boostname->name}} @endif Boost List
                    </div>
                    @if($job)
                        <div class="actions">
                        <a href="/admin/posts/edit/{{$job->id}}" class="btn red">
                            <i class="fa fa-edit"></i> Edit Job </a>
                        </div>
                    @endif
                </div>

                <div class="portlet-body form">

                    <form action="/admin/boost_manage/list/edit/{{$postsUpgradeList->id}}" method="post"
                          class="form-horizontal form-bordered" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div class="form-group">
                            <label for="post_datetime" class="col-sm-3 control-label">Purchased Boost Date / Time</label>
                            <div class="col-sm-8">
                                <input id="" class="form-control" type="text" name=""
                                       placeholder="" @if($post_upgrade) value="{{\Carbon\Carbon::parse($post_upgrade->created_at)->format('d M Y  H:i:s')}}" @endif disabled="disabled" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="post_datetime" class="col-sm-3 control-label">Boost Posted Date / Time</label>

                            <div class="col-sm-8">
                                <input id="post_datetime " class="form-control datetimepicker" type="text" name="post_datetime"
                                       placeholder="{{config('app.datetime_format.php')}}" value="{{old('post_datetime', \Carbon\Carbon::parse($postsUpgradeList->post_datetime)->format(config('app.datetime_format.php')))}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post_cost" class="col-sm-3 control-label"><p class="packages_perc">$ Received for Boost</p></label>

                            <div class="col-sm-8">
                                <?php
                                      $pricetotal = \App\PostUpgrades::where('id',$postsUpgradeList->post_upgrades_id)->first();
                                     ?>
                                     @if($pricetotal) 
                                        @if($pricetotal->is_purchased == 1) 
                                            <input id="post_cost" class="form-control" type="text" name="post_cost" value="${{number_format($postsUpgradeList->boost_cost, 2, '.', '')}}" disabled="disabled"/> 
                                        @else 
                                            <input id="post_cost" class="form-control" type="text" name="post_cost" value="Free" disabled="disabled"/>
                                        @endif
                                     @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post_cost" class="col-sm-3 control-label"><p class="packages_perc">${{old('post_cost', $postsUpgradeList->post_cost)}} Spent on Boost</p></label>

                            <div class="col-sm-8">
                                <input id="post_cost" class="form-control" type="text" name="post_cost" value="{{old('post_cost', $postsUpgradeList->post_cost)}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post_views" class="col-sm-3 control-label">Boost Views</label>

                            <div class="col-sm-8">
                                <input id="post_views" class="form-control" type="text" name="post_views"
                                       value="{{old('post_views', $postsUpgradeList->post_views)}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post_clicks" class="col-sm-3 control-label">Boost Clicks</label>

                            <div class="col-sm-8">
                                <input id="post_clicks" class="form-control" type="text" name="post_clicks"
                                       value="{{old('post_clicks', $postsUpgradeList->post_clicks)}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post_status" class="col-sm-3 control-label">Boost Status</label>

                            <div class="col-sm-8">
                                <select id="post_status" class="form-control" name="post_status">
                                    @foreach(\App\PostUpgradesLists::$status as $status => $label)
                                        <option value="{{$status}}" {{old('post_status', $postsUpgradeList->post_status) == $status ? 'selected' : ''}}>{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if($post_upgrade)
                            @if($post_upgrade->upgrade_id == 1)
                                <div class="form-group">
                                    <label for="post_edition" class="col-sm-3 control-label">Social Media Platform</label>

                                    <div class="col-sm-8">
                                        <select id="post_edition" class="form-control" name="post_edition">
                                            @foreach(\App\PostUpgradesLists::$postEditionsPulse as $key => $value)
                                                <option value="{{$key}}" {{ stripos($postsUpgradeList->post_edition, $key) !== FALSE ? 'selected' : ''}}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @elseif($post_upgrade->upgrade_id == 2)
                                <div class="form-group">
                                    <label for="post_edition" class="col-sm-3 control-label">Social Media Platform</label>

                                    <div class="col-sm-8">
                                        <select id="post_edition" class="form-control" name="post_edition">
                                            @foreach(\App\PostUpgradesLists::$postEditionsFacebook as $key => $value)
                                                <option value="{{$key}}" {{ stripos($postsUpgradeList->post_edition, $key) !== FALSE ? 'selected' : ''}}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @elseif($post_upgrade->upgrade_id == 4)
                                <div class="form-group">
                                    <label for="post_edition" class="col-sm-3 control-label">Social Media Platform</label>

                                    <div class="col-sm-8">
                                        <select id="post_edition" class="form-control" name="post_edition">
                                            @foreach(\App\PostUpgradesLists::$postEditionsLinkedIn as $key => $value)
                                                <option value="{{$key}}" {{ stripos($postsUpgradeList->post_edition, $key) !== FALSE ? 'selected' : ''}}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="post_edition" class="col-sm-3 control-label">Social Media Platform</label>

                                    <div class="col-sm-8">
                                        <select id="post_edition" class="form-control" name="post_edition">
                                            @foreach(\App\PostUpgradesLists::$postEditions as $key => $value)
                                                <option value="{{$key}}" {{ stripos($postsUpgradeList->post_edition, $key) !== FALSE ? 'selected' : ''}}>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label for="post_edition" class="col-sm-3 control-label">Social Media Platform</label>

                                <div class="col-sm-8">
                                    <select id="post_edition" class="form-control" name="post_edition">
                                        @foreach(\App\PostUpgradesLists::$postEditions as $key => $value)
                                            <option value="{{$key}}" {{ stripos($postsUpgradeList->post_edition, $key) !== FALSE ? 'selected' : ''}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                        @if($post_upgrade)
                            @if($post_upgrade->upgrade_id == 1)
                                <div class="form-group">
                                    <label for="pulse_edition" class="col-sm-3 control-label">Pulse Edition Number</label>

                                    <div class="col-sm-8">
                                        <input id="pulse_edition" class="form-control" type="text" name="pulse_edition"
                                               value="{{stripos($postsUpgradeList->post_edition, 'The Pulse Newsletter') !== FALSE ? str_replace('The Pulse Newsletter #', '', $postsUpgradeList->post_edition) : ''}}"/>
                                    </div>
                                </div>
                            @elseif($post_upgrade->upgrade_id == 2)
                                <div class="form-group">
                                    <label for="boost_image"
                                           class="col-sm-3 control-label">Facebook Post Image</label>

                                    <div class="col-sm-8">
                                        <input type="hidden" name="boost_image_value"
                                               value="{{$postsUpgradeList->boost_image}}"/>
                                        <input id="boost_image" class="form-control" name="boost_image" type="file"/>
                                    </div>
                                </div>

                                @if(strlen($postsUpgradeList->boost_image) > 0)
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <img src="{{$postsUpgradeList->boost_image}}" width="300"/>
                                        </div>
                                    </div>
                                @endif
                            @elseif($post_upgrade->upgrade_id == 4)
                                <div class="form-group">
                                    <label for="boost_image"
                                           class="col-sm-3 control-label">LinkedIn Post Image</label>

                                    <div class="col-sm-8">
                                        <input type="hidden" name="boost_image_value"
                                               value="{{$postsUpgradeList->boost_image}}"/>
                                        <input id="boost_image" class="form-control" name="boost_image" type="file"/>
                                    </div>
                                </div>

                                @if(strlen($postsUpgradeList->boost_image) > 0)
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <img src="{{$postsUpgradeList->boost_image}}" width="300"/>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="form-group">
                                    <label for="pulse_edition" class="col-sm-3 control-label">Pulse Edition Number</label>

                                    <div class="col-sm-8">
                                        <input id="pulse_edition" class="form-control" type="text" name="pulse_edition"
                                               value="{{stripos($postsUpgradeList->post_edition, 'The Pulse Newsletter') !== FALSE ? str_replace('The Pulse Newsletter #', '', $postsUpgradeList->post_edition) : ''}}"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="boost_image"
                                           class="col-sm-3 control-label">Boost Image</label>

                                    <div class="col-sm-8">
                                        <input type="hidden" name="boost_image_value"
                                               value="{{$postsUpgradeList->boost_image}}"/>
                                        <input id="boost_image" class="form-control" name="boost_image" type="file"/>
                                    </div>
                                </div>

                                @if(strlen($postsUpgradeList->boost_image) > 0)
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <img src="{{$postsUpgradeList->boost_image}}" width="300"/>
                                        </div>
                                    </div>
                                @endif

                            @endif
                        @else
                                <div class="form-group">
                                    <label for="pulse_edition" class="col-sm-3 control-label">Pulse Edition Number</label>

                                    <div class="col-sm-8">
                                        <input id="pulse_edition" class="form-control" type="text" name="pulse_edition"
                                               value="{{stripos($postsUpgradeList->post_edition, 'The Pulse Newsletter') !== FALSE ? str_replace('The Pulse Newsletter #', '', $postsUpgradeList->post_edition) : ''}}"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="boost_image"
                                           class="col-sm-3 control-label">Boost Image</label>

                                    <div class="col-sm-8">
                                        <input type="hidden" name="boost_image_value"
                                               value="{{$postsUpgradeList->boost_image}}"/>
                                        <input id="boost_image" class="form-control" name="boost_image" type="file"/>
                                    </div>
                                </div>

                                @if(strlen($postsUpgradeList->boost_image) > 0)
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-8">
                                            <img src="{{$postsUpgradeList->boost_image}}" width="300"/>
                                        </div>
                                    </div>
                                @endif
                        @endif

                        <div class="form-group">
                            <label for="post_comment" class="col-sm-3 control-label">Feedback to Job Advertiser</label>

                            <div class="col-sm-8">
                                <textarea id="post_comment" class="form-control" name="post_comment">{{old('post_comment', $postsUpgradeList->post_comment)}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="internal_comment" class="col-sm-3 control-label">Internal comments only</label>

                            <div class="col-sm-8">
                                <textarea id="internal_comment" class="form-control" name="internal_comment">{{old('internal_comment', $postsUpgradeList->internal_comment)}}</textarea>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
<script type="text/javascript">
    $( "#post_cost" ).keyup(function() {
        var var1 = $( "#post_cost" ).val();
        $( ".packages_perc" ).html('$'+ Number(var1).toFixed(2)+ ' Spent on Boost');
    });
</script>
@stop