@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 9,
                    filter_default_label: 'Select Status',
                    filter_match_mode: 'exact'

                }
            ]);
        });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Packages Stats
        <small>View Packages Stats</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/packages">All Packages Stats</a>
            </li>

        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Packages Stats
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>User name</th>
                            <th>Company Name</th>
                            <th>Package name</th>
                            <th>Date of purchases</th>
                            <th>Date of expiry</th>
                            <th>Days left</th>
                            <th>Price</th>
                            <th>Credits Left</th>
                            <th>Status</th>
                            <th>Deleted</th>
                            <th>Actions</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                            <tr>
                                <td> {{$ad->id}} </td>
                                <td> {{$ad->user->name}} </td>
                                <td> {{$ad->user->company}} </td>
                                <td>@if(\App\Packages::find($ad->package_id)) {{\App\Packages::find($ad->package_id)->package_name}} @else nil @endif </td>
                                <td> {{\Carbon\Carbon::parse($ad->date_purchase)->format('d M Y H:i:s')}} </td>
                                <td> <span class="hidden">{{\Carbon\Carbon::parse($ad->expiry_date)->format('YYYY/mm/dd')}}</span> {{$ad->expiry_date->format('d M Y H:i:s')}} </td>
                                <td> {{$ad->days_left}} </td>
                                <td>@if(\App\Packages::find($ad->package_id)) {{\App\Packages::find($ad->package_id)->price}} @else nil @endif </td>
                                <td> {{$ad->user->credits}} </td>
                                <td> {{\App\PackagesStats::$status[$ad->status]}}</td>
                                <td> {{$ad->deleted ? 'Yes' : 'No'}}</td>
                                 <td><a href="/admin/packages_stats/edit/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                </td>
                                <td><a href="/admin/packages_stats/delete/{{$ad->id}}"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@stop