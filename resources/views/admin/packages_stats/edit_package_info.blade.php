@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/css/bootstrap-datetimepicker.css" />
@stop

@section('extra_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/en-au.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
<script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
     $(document).ready(function () {
            $('#datetimepicker').datetimepicker();
     });
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Edit Value Packs Purchase
        <small>Edit Value Packs Purchase</small>
    </h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/packages_stats/packageinfo">Value Packs Purchases</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/packages_stats/packageinfo/edit/{{$ad->id}}">Edit Value Packs Purchase</a>
        </li>

    </ul>
</div>


<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-frame"></i>Edit Value Packs Purchase
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">


                <form action="/admin/packages_stats/setstatus/{{$ad->id}}" id="form-username" method="post"
                      class="form-horizontal form-bordered">

                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>

                        <div class="col-sm-4">
                            @include('admin.layouts.notify')
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Payment Status</label>

                        <div class="col-sm-8">
                            <select id="status" name="status" class="form-control">
                                <option value="" @if(!$ad->status) selected="selected" @endif>Send Invoice</option>
                                <option value="Awaiting Payment" @if($ad->status == 'Awaiting Payment') selected="selected" @endif >Awaiting Payment</option>
                                <option value="Paid" @if($ad->status == 'Paid') selected="selected" @endif >Paid</option>
                                <option value="Overdue" @if($ad->status == 'Overdue') selected="selected" @endif >Overdue</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Invoice term</label>

                        <div class="col-sm-8">
                            <?php $current_term = \App\Users::find($ad->user_id);
                                  $fixed_term = \App\Settings::where('column_key','invoice_terms')->first(); ?>
                            @if($current_term && $current_term->invoice_terms)
                                <p>User Invoice Term: {{$current_term->invoice_terms}} days</p>
                                <input type="number" class="form-control" name="invoice" @if($ad->invoice_terms) value="{{$ad->invoice_terms}}" @else value="{{$current_term->invoice_terms}}" @endif>
                            @else
                                <input type="number" class="form-control" name="invoice" @if($ad->invoice_terms) value="{{$ad->invoice_terms}}" @elseif($fixed_term && $fixed_term->value_string) value="{{$fixed_term->value_string}}" @else value="7" @endif>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date" class="col-sm-3 control-label">Invoice due date/time</label>

                        <div class="col-sm-8">
                            <p style="padding-top: 7px;">
                                @if($ad->invoice_terms)
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($ad->invoice_terms);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @elseif($current_term && $current_term->invoice_terms)
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($current_term->invoice_terms);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @elseif($fixed_term && $fixed_term->value_string) 
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($fixed_term->value_string);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @else
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays(7);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="xero" class="col-sm-3 control-label">Xero ID</label>

                        <div class="col-sm-8">
                            <input id="xero" class="form-control" type="text" name="xero"
                                   placeholder="Xero ID" value="{{$ad->xero}}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="payment_time" class="col-sm-3 control-label">Payment Date/Time</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker'>
                                <input type='text' name="payment_time" class="form-control" value="{{$ad->payment_time}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}} </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
@stop