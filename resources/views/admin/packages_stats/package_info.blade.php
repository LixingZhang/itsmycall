@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 19,
                    filter_default_label: 'Select Expiry Status',
                    filter_match_mode: 'exact'

                },
                {
                    column_number: 1,
                    filter_default_label: 'Select Name',
                    filter_match_mode: 'exact'

                },
                {
                    column_number: 2,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact'

                },
                {
                    column_number: 4,
                    filter_default_label: 'Select Type',
                    filter_match_mode: 'exact'

                },
                {
                    column_number: 5,
                    filter_default_label: 'Select Payment Status',
                    filter_match_mode: 'exact'

                }
            ]);
        });
    </script>
@stop

@section('content')
    <?php setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>
    <h3 class="page-title">
        Value Packs Purchases
        <small>View Value Packs Purchases</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/packages">Value Packs Purchases</a>
            </li>

        </ul>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Value Packs Purchases
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>User name</th>
                            <th>Email</th>
                            <th>Value Pack</th>
                            <th>Payment Type</th>
                            <th>Payment Status</th>
                            <th>Invoice term</th>
                            <th>Invoice due date/time</th>
                            <th>Day/Hours until invoice is due</th>
                            <th>Total Amount (Pre GST)</th>
                            <th>GST Paid</th>
                            <th>Credit Card Fees</th>
                            <th>Total Paid</th>
                            <th>Credits Given</th>
                            <th>Credits Remaining</th>
                            <th>Credits Expired</th>
                            <th>Purchase Date</th>
                            <th>Days left till Expiry</th>
                            <th>Expiry Date</th>
                            <th>Expiry Status</th>
                            <th>Xero ID</th>
                            <th>Payment Date/Time</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                            @if($ad->payment == 'Invoice')
                            <tr>
                                <td>{{$ad->id}}</td>
                                <td>
                                    <?php
                                       $user =  \App\Users::where('id',$ad->user_id)->first();
                                    ?>
                                    @if($user)
                                        {{$user->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user)
                                        {{$user->email}}
                                    @endif
                                </td>
                                <td>
                                    {{$ad->name}}
                                </td>
                                <td>
                                    {{$ad->payment}}
                                </td>
                                <td>@if($ad->status) {{$ad->status}} @else Send Invoice  @endif
                                </td>
                                <?php $current_term = \App\Users::find($ad->user_id);
                                      $fixed_term = \App\Settings::where('column_key','invoice_terms')->first(); ?>
                                <td> @if($ad->invoice_terms) {{$ad->invoice_terms}} days @elseif($current_term && $current_term->invoice_terms) {{$current_term->invoice_terms}} days @elseif($fixed_term && $fixed_term->value_string) {{$fixed_term->value_string}} days @else 7 Days @endif</td>
                                <td>@if($ad->invoice_terms)
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($ad->invoice_terms);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @elseif($current_term && $current_term->invoice_terms)
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($current_term->invoice_terms);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @elseif($fixed_term && $fixed_term->value_string) 
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($fixed_term->value_string);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @else
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays(7);
                                     ?>
                                    {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                    @endif
                                </td>
                                <td>@if($ad->invoice_terms)
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($ad->invoice_terms);
                                        $remaining = trim($date_process->copy()->diffForHumans(\Carbon\Carbon::now()), "after");
                                        if(\Carbon\Carbon::now() > $date_process){
                                            $remaining = '0 days';
                                        }

                                     ?>
                                    {{$remaining}}
                                    @elseif($current_term && $current_term->invoice_terms)
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($current_term->invoice_terms);
                                        $remaining = trim($date_process->copy()->diffForHumans(\Carbon\Carbon::now()), "after");
                                        if(\Carbon\Carbon::now() > $date_process){
                                            $remaining = '0 days';
                                        }

                                     ?>
                                    {{$remaining}}
                                    @elseif($fixed_term && $fixed_term->value_string) 
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays($fixed_term->value_string);
                                        $remaining = trim($date_process->copy()->diffForHumans(\Carbon\Carbon::now()), "after");
                                        if(\Carbon\Carbon::now() > $date_process){
                                            $remaining = '0 days';
                                        }

                                     ?>
                                    {{$remaining}}
                                    @else
                                    <?php
                                        $start = new \Carbon\Carbon($ad->purchase_date);
                                        $date_process = $start->addDays(7);
                                        $remaining = trim($date_process->copy()->diffForHumans(\Carbon\Carbon::now()), "after");
                                        if(\Carbon\Carbon::now() > $date_process){
                                            $remaining = '0 days';
                                        }

                                     ?>
                                    {{$remaining}}
                                    @endif
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->price-$ad->gstpaid-$ad->creditfee)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->gstpaid)}}
                                </td>
                                <td>
                                    nil
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->price)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->creditsgiven)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->remaining)}}
                                </td>
                                <td>
                                    -{{money_format('%.2n',$ad->expired)}}
                                </td>
                                <td>
                                    <?php
                                        $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->purchase_date);
                                        $print = $date->format('d M Y H:i:s');
                                     ?>
                                     <span class="hidden">{{\Carbon\Carbon::parse($ad->purchase_date)->format('YYYY/mm/dd')}}</span>
                                     {{$print}}
                                </td>
                                <td>
                                    <?php
                                         $end = \Carbon\Carbon::now()->toDateString();
                                         $expires = new \Carbon\Carbon($ad->expiry_date);
                                         $date = $expires->toFormattedDateString();
                                         $now = \Carbon\Carbon::now();
                                         $difference = ($expires->diff($now)->days);
                                         if(\Carbon\Carbon::now() > $ad->expiry_date){
                                                    $difference = 0;
                                            }
                                     ?>
                                     {{$difference}} days
                                </td>
                                <td>
                                    <?php
                                        $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->expiry_date);
                                        $print2 = $date2->format('d M Y H:i:s');
                                     ?>
                                     <span class="hidden">{{\Carbon\Carbon::parse($ad->expiry_date)->format('YYYY/mm/dd')}}</span>
                                     {{$print2}}
                                </td>
                                <td>
                                   @if($ad->expired)
                                   Expired
                                   @else
                                   Active
                                   @endif
                                </td>
                                <td>@if($ad->xero) {{$ad->xero}} @else nil @endif </td>
                                <td>@if($ad->payment_time)
                                    <?php
                                        $payment_time = \Carbon\Carbon::createFromFormat('d/m/Y h:i A',$ad->payment_time);
                                        $print_time = $payment_time->format('d M Y H:i:s');
                                     ?>
                                     {{$print_time}}
                                 @else nil @endif</td>
                                <td>
                                    <a class="btn btn-info btn-xs" href="/admin/packages_stats/packageinfo/edit/{{$ad->id}}">Edit</a>
                                </td>
                            </tr>
                            @else
                            <tr>
                                <td>{{$ad->id}}</td>
                                <td>
                                    <?php
                                       $user =  \App\Users::where('id',$ad->user_id)->first();
                                    ?>
                                    @if($user)
                                        {{$user->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($user)
                                        {{$user->email}}
                                    @endif
                                </td>
                                <td>
                                    {{$ad->name}}
                                </td>
                                <td>
                                    {{$ad->payment}}
                                </td>
                                <td>Paid
                                </td>
                                <td>
                                    nil
                                </td>
                                <td>
                                    nil
                                </td>
                                <td>
                                    nil
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->price-$ad->gstpaid-$ad->creditfee)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->gstpaid)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->creditfee)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->price)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->creditsgiven)}}
                                </td>
                                <td>
                                    {{money_format('%.2n',$ad->remaining)}}
                                </td>
                                <td>
                                    -{{money_format('%.2n',$ad->expired)}}
                                </td>
                                <td>
                                    <?php
                                        $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->purchase_date);
                                        $print = $date->format('d M Y H:i:s');
                                     ?>
                                     <span class="hidden">{{\Carbon\Carbon::parse($ad->purchase_date)->format('YYYY/mm/dd')}}</span>
                                     {{$print}}
                                </td>
                                <td>
                                    <?php
                                         $end = \Carbon\Carbon::now()->toDateString();
                                         $expires = new \Carbon\Carbon($ad->expiry_date);
                                         $date = $expires->toFormattedDateString();
                                         $now = \Carbon\Carbon::now();
                                         $difference = ($expires->diff($now)->days);
                                         if(\Carbon\Carbon::now() > $ad->expiry_date){
                                                    $difference = 0;
                                            }
                                     ?>
                                     {{$difference}} days
                                </td>
                                <td>
                                    <?php
                                        $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->expiry_date);
                                        $print2 = $date2->format('d M Y H:i:s');
                                     ?>
                                     <span class="hidden">{{\Carbon\Carbon::parse($ad->expiry_date)->format('YYYY/mm/dd')}}</span>
                                     {{$print2}}
                                </td>
                                <td>
                                   @if($ad->expired)
                                   Expired
                                   @else
                                   Active
                                   @endif
                                </td>
                                <td>nil </td>
                                <td><span class="hidden">{{\Carbon\Carbon::parse($ad->purchase_date)->format('YYYY/mm/dd')}}</span>
                                     {{$print}}</td>
                                <td>
                                    nil
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@stop