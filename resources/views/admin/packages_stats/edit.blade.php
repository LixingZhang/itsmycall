@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/en-au.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/css/bootstrap-datetimepicker.css" />
<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
<script src="/assets/plugins/redactor/redactor.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
         $('#datetimepicker1').datetimepicker({format: 'Y/M/D' });
         $('#datetimepicker2').datetimepicker({format: 'Y/M/D' });
        $('#description').redactor({
            imageUpload: '/admin/redactor',
            imageManagerJson: '/admin/redactor/images.json',
            plugins: ['imagemanager'],
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
    });
</script>
@stop

@section('content')


<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/packages_stats">Package Stats</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/packages_stats/edit/{{$ad->id}}">Edit Packages Stats</a>
        </li>

    </ul>
</div>


<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-frame"></i>Edit Packages Stats
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">


                <form action="/admin/packages_stats/update" id="form-username" method="post"
                      class="form-horizontal form-bordered">

                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>

                        <div class="col-sm-4">
                            @include('admin.layouts.notify')
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="id" value="{{$ad->id}}"/>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Package Name</label>

                        <div class="col-sm-8">
                            {{$ad->package->package_name}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">User Name</label>

                        <div class="col-sm-8">
                            {{$ad->user->name}}
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Date Purchase</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="date_purchase" value="{{old('date_purchase',$ad->date_purchase)}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-3 control-label">Status</label>

                        <div class="col-sm-8">
                            <div class='input-group'>
                                <select name="status" id="status" class="form-control">
                                    @foreach (\App\PackagesStats::$status as $key => $status)
                                    <option value="{{$key}}">{{$status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="deleted" class="col-sm-3 control-label">Delete</label>

                        <div class="col-sm-8">
                            <div class='input-group'>
                                <select name="deleted" id="deleted" class="form-control">
                                        <option value="1" {{$ad->deleted ? 'selected' : ''}}>Yes</option>
                                        <option value="0" {{!$ad->deleted ? 'selected' : ''}}>No</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}} </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
@stop