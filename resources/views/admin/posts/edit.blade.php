@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('extra_js')
<script type="text/javascript" src="/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
<script src="/assets/plugins/redactor/redactor.js"></script>
<script type= "text/javascript" src = "/assets/js/countriesEdit.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true,
                order: [ 0, 'desc' ]
            });
            yadcf.init(myTable,[
                {
                    column_number: 1,
                    filter_default_label: 'Select Type',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 10,
                    filter_default_label: 'Select Status',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 4,
                    filter_default_label: 'Select Platform',
                    filter_match_mode: 'exact',

                }
            ])
        });
    </script>
<script type="text/javascript">
    $(document).ready(function () {
        var additional_upgrades_idx = 0;
        $('#add_upgrade_btn').click(function () {
            var upgrade_name = $("#upgrade_name option:selected").text();
            var upgrade_time = $("#upgrade_time option:selected").text();
            var upgrade_id = parseInt($('#upgrade_name').val());
            var upgrade_count = parseInt($('#upgrade_time').val());

            $('#additional-upgrades-wrapper').append(
                    $('<div>', {'class': 'col-sm-8', text: upgrade_name}),
                    $('<div>', {'class': 'col-sm-4', text: upgrade_time}),
                    $('<input>', {
                        'type': 'hidden',
                        'name': 'additional_upgrades[' + additional_upgrades_idx + '][upgrade_id]',
                        'value': upgrade_id
                    }),
                    $('<input>', {
                        'type': 'hidden',
                        'name': 'additional_upgrades[' + additional_upgrades_idx + '][upgrade_count]',
                        'value': upgrade_count
                    })
                    );
            additional_upgrades_idx++;
        });

        $('#tags').tagsinput();

        $('#description').redactor({
            imageUpload: '/admin/redactor',
            imageManagerJson: '/admin/redactor/images.json',
            plugins: ['imagemanager'],
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        }
        );

        var category_el = $('#category');
        var render_type_el = $('#render_type');

        category_el.on('change', function () {
            $.ajax({
                url: "/api/get_sub_categories_by_category/" + $('#category').val(),
                success: function (sub_categories) {

                    var $sub_category_select = $('#sub_category');
                    $sub_category_select.find('option').remove();

                    $.each(sub_categories, function (key, value) {
                        $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>');
                    });
                },
                error: function (response) {
                }
            });
        });

        render_type_el.on('change', function (ev) {
            var val = $(this).find('option:selected').val();

            if (val == "{{\App\Posts::RENDER_TYPE_TEXT}}") {
                $('#featured_image_div').hide();
                $('#image_parallax_div').hide();

                $('#gallery_image_div').hide();
                $('#featured_preview_div').hide();
                $('#video_div').hide();
                $('#video_parallax_div').hide();
            }

            if (val == "{{\App\Posts::RENDER_TYPE_IMAGE}}") {
                $('#featured_image_div').show();
                $('#image_parallax_div').show();

                $('#gallery_image_div').hide();
                $('#video_div').hide();
                $('#video_parallax_div').hide();
            }

            if (val == "{{\App\Posts::RENDER_TYPE_GALLERY}}") {
                $('#gallery_image_div').show();

                $('#featured_image_div').hide();
                $('#featured_preview_div').hide();
                $('#image_parallax_div').hide();
                $('#video_div').hide();
                $('#video_parallax_div').hide();
            }

            if (val == "{{\App\Posts::RENDER_TYPE_VIDEO}}") {
                $('#video_div').show();
                $('#video_parallax_div').show();
                $('#featured_preview_div').show();
                $('#featured_image_div').show();

                $('#gallery_image_div').hide();
                $('#image_parallax_div').hide();
            }

        });


        //category_el.trigger('change');
        render_type_el.trigger('change');
        populateCountries("country", "state");

    });
</script>

@stop

@section('content')
<style>
    .orangeudnerline {
        padding-left: 10px;
    }
</style>
<h3 class="page-title">
    <?php $advertiser =  \App\Users::find($post->author_id); ?>
    Edit Job Post by Advertiser: {{$advertiser->email}} ({{$advertiser->id}})
</h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/posts">{{trans('messages.posts')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/posts/edit/{{$post->id}}">{{trans('messages.edit_post')}} - {{$post->title}}</a>
        </li>

    </ul>
</div>
    <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                            {{\App\Views::where('job_id',$post->id)->count()}}
                            </div>
                            <div class="desc">
                                Views
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue-madison">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            @if($post->email_or_link)
                            <div class="number">
                            {{$post->out_count}}
                            </div>
                            <div class="desc">
                                Applicants Clicks Out
                            </div>
                            @else
                            <div class="number">
                            {{\App\Applicants::where('job_id',$post->id)->count()}}
                            </div>
                            <div class="desc">
                                Applicants
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$post->title}} Job Advertiser
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <a href="/admin/users/edit/{{$post->author_id}}" class="btn btn-primary">View Advertiser</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$post->title}} Change Log
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <a href="/admin/posts/postchangelog/{{$post->id}}" class="btn btn-primary">View Change Log</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>{{$post->title}} Transactions
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body">
                    <a href="/admin/transactions/post/{{$post->id}}" class="btn btn-primary">View Transactions</a>
            </div>
        </div>
    </div>
</div>
    <?php
            $job = \DB::table('posts')->where('id',$post->id)->first();
    ?>
    <label class="label label-danger label-md" style="background: rgb(255, 179, 194);
        color: black;
        display: block;
        max-width: 200px;
        margin-bottom: 10px;">If Boost has status Not Actioned</label>
    <label class="label label-danger label-md" style="background: lightyellow;
        color: black;
        display: block;
        max-width: 200px;
        margin-bottom: 10px;">If Boost has status In Progress</label>
    <label class="label label-danger label-md" style="background: #b6ffb6;
        color: black;
        display: block;
        max-width: 200px;
        margin-bottom: 10px;">If Boost has status Completed</label>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Boost List
                    </div>
                    @if($job)
                        <div class="actions">
                        <a class="btn btn-danger btn-sm" href="/admin/posts/boosts/add/{{$id}}">Create Boosts</a>
                        </div>
                    @endif
                </div>

                <div class="portlet-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>Boost Type</th>
                            <th>Purchased Boost Date / Time</th>
                            <th>Boost Posted Date / Time</th>
                            <th>Social Media Platform</th>
                            <th>Credits Used for Purchase</th>
                            <th>$ received for Boost</th>
                            <th>$ spent on Boost</th>
                            <th>Boost Views</th>
                            <th>Boost Clicks</th>
                            <th>Boost Status</th>
                            <th>Feedback to Job Advertiser</th>
                            <th>Internal comments only</th>
                            <th>Facebook Post Image</th>
                            <th>LinkedIn Post Image</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($postsUpgradeLists as $list)
                            <?php $boostname = \DB::table('upgrades')->where('id',$list->boost_id)->first();
                                $post_upgrade = \App\PostUpgrades::where('id',$list->post_upgrades_id)->first();
                             ?>
                            <tr @if($list->post_status == 'completed') style="background: #b6ffb6;" @elseif($list->post_status == 'in_progress') style="background: lightyellow;" @else style="background: rgb(255, 179, 194);" @endif>
                                <td> {{$list->id}} </td>
                                <td> @if($boostname) {{$boostname->name}} @endif </td>
                                <td> @if($post_upgrade) <span class="hidden">{{\Carbon\Carbon::parse($post_upgrade->created_at)->format('YYYY/mm/dd')}}</span>
                                 {{\Carbon\Carbon::parse($post_upgrade->created_at)->format('d M Y  H:i:s')}} @endif </td>
                                <td> <span class="hidden">{{\Carbon\Carbon::parse($list->post_datetime)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($list->post_datetime)->format('d M Y H:i:s')}} </td>
                                <td>{{$list->post_edition}}</td>
                                <td>{{$post_upgrade->isCreditsUsed()}}</td>
                                <td>
                                    <?php
                                      $pricetotal = \App\PostUpgrades::where('id',$list->post_upgrades_id)->first();
                                     ?>
                                     @if($pricetotal) 
                                        @if($pricetotal->is_purchased == 1) 
                                           ${{number_format($list->boost_cost, 2, '.', '')}}
                                        @else 
                                        Free 
                                        @endif
                                     @endif

                                </td>
                                <td> ${{$list->post_cost}}</td>
                                <td> {{$list->post_views}}</td>
                                <td> {{$list->post_clicks}} </td>
                                <td> {{\App\PostUpgradesLists::$status[$list->post_status]}}</td>
                                <td>{{$list->post_comment}}</td>
                                <td>{{$list->internal_comment}}</td>
                                @if($post_upgrade->upgrade_id == 2)
                                    <td>
                                        @if(!empty($list->boost_image))
                                            <img src="{{$list->boost_image}}" style="max-width: 100px;" />
                                        @else
                                        nil
                                        @endif
                                    </td>
                                    <td>
                                        nil
                                    </td>
                                @elseif($post_upgrade->upgrade_id == 4)
                                    <td>
                                        nil
                                    </td>
                                    <td>
                                        @if(!empty($list->boost_image))
                                            <img src="{{$list->boost_image}}" style="max-width: 100px;" />
                                        @else
                                        nil
                                        @endif
                                    </td>
                                @else
                                    <td>
                                        nil
                                    </td>
                                    <td>
                                        nil
                                    </td>
                                @endif
                                <td><a href="/admin/boost_manage/list/delete/{{$list->id}}/{{$list->post_id}}"
                                       class="btn btn-danger btn-sm">Delete</a></td>
                                <td><a href="/admin/boost_manage/list/edit/{{$list->id}}"
                                       class="btn btn-info btn-sm">{{trans('messages.edit')}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-docs"></i>{{trans('messages.edit_post')}} - {{$post->title}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">


                <form action="/admin/posts/update" id="form-username" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>

                        <div class="col-sm-8">
                           @include('admin.layouts.notify')
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="id" value="{{$post->id}}"/>


                    <h3 class='orangeudnerline' style="margin-bottom:25px">Job Status</h3>
                    <div class="form-group" style="">
                        <label for="status" class="col-sm-3 control-label">{{trans('messages.status')}}</label>
                        <div class="col-sm-8">
                            <select id="status" class="form-control" name="status">
                                @foreach(\App\Posts::getPositionStatuses() as $status => $label)
                                    <option value="{{$status}}" {{$post->status === $status ? 'selected' : ''}}>{{$label}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="expired_at" class="col-sm-3 control-label">{{trans('messages.expired_on')}}</label>

                        <div class="col-sm-8">
                            <input id="expired_at" class="form-control" type="datetime"
                                   name="expired_at" placeholder="Y-m-d H:i:s" value="{{$post->expired_at}}"/>
                        </div>
                    </div>

                    <div class="form-group" id="">
                        <label for="paid"
                               class="col-sm-3 control-label">Payment Status</label>
                        <div class="col-sm-8">
                            <select name="paid" id="paid" class="form-control">
                                <option {{$post->paid == 'yes' ? 'selected':''}} value="yes">Yes</option>
                                <option {{$post->paid == 'no' ? 'selected':''}} value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <h3 class='orangeudnerline' style="margin-bottom:25px">Advertisement Details</h3>
                    <div class="form-group">
                        <label for="company" class="col-sm-3 control-label required">{{trans('messages.company')}}</label>
                        <div class="col-sm-8">
                            <input id="company" data-class="company-name" class="form-control" type="text" name="company" 
                                   placeholder="{{trans('messages.enter_post_company')}}" value="{{$post->company}}"/>
                        </div>
                    </div>
                    <div class="form-group" id="featured_image_div">
                        <label for="featured_image" class="col-sm-3 control-label">{{trans('messages.company_logo')}}</label>
                        <div class="col-sm-8" >
                            @if($post->featured_image)
                            <img class="company-image" style="padding-bottom: 35px" src="{{$post->featured_image}}" width="150"/>
                            @else
                            No logo Uploaded
                            @endif
                            <input id="featured_image" style="padding-bottom: 45px" class="form-control" type="file" name="featured_image"/>
                        </div>
                    </div>

                    <div class="form-group" id="featured_image_div">
                        <label for="job_image" class="col-sm-3 control-label">Specific Job Image</label>
                        <div class="col-sm-8" >
                            <p>The URL of the image for your object. It should be at least 600x315 pixels, but 1200x630 or larger is preferred (up to 5MB). Stay close to a 1.91:1 aspect ratio to avoid cropping.</p>
                            @if($post->job_image)
                            <img class="company-image" style="padding-bottom: 35px" src="{{$post->job_image}}" width="150"/>
                            @else
                            No image Uploaded
                            @endif
                            <input id="job_image" style="padding-bottom: 45px" class="form-control" type="file" name="job_image"/>
                        </div>
                    </div>

                    <div class="form-group" id="featured_image_div">
                        <label for="job_image" class="col-sm-3 control-label">Job Photos to display<br><small>Job Photos are only available for Featured listings</small><br><small>Max 5 allowed!</small></label>
                        <div class="col-sm-8" >
                            @if($postimages)
                            @foreach($postimages as $image)
                            <a style="top: 12%;margin-left: 4px; position: absolute;" class="btn btn-danger btn-xs" href="/admin/posts/deleteimage/{{$image->id}}">X</a>
                            <a href="{{$image->jobimages}}"><img class="postimage" style="margin-left: 5px;" src="{{$image->jobimages}}" width="150"/></a>
                            @endforeach
                            @else
                            <p style="margin: 7px 0 0;">No Job Photos Uploaded!</p>
                            @endif
                            <input id="job_photos" style="padding-bottom: 45px;margin-top: 5px;" class="form-control" type="file" name="job_photos[]" multiple="multiple" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label required">{{trans('messages.title')}}</label>
                        <div class="col-sm-8">
                            <input id="title" data-class="job-title" class="form-control" type="text" name="title" 
                                   placeholder="{{trans('messages.enter_post_title')}}" value="{{$post->title}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="shortdescription" class="col-sm-3 control-label required">Preview Description</label>
                        <div class="col-sm-8" id="shortie">
                            <textarea id="shortdescription" class="form-control" name="shortdescription" maxlength="500">{{$post->short_description}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label required">{{trans('messages.description')}}</label>
                        <div class="col-sm-8">
                            <textarea id="description" class="form-control" name="description" maxlength="2500">{{$post->description}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Selling Point 1</label>
                        <div class="col-sm-8">
                            <textarea id="selling1" data-class="selling-point1" class="form-control" name="selling1">{{$post->selling1}}</textarea>
                            <small><span id ="charCounter1"></span></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Selling Point 2</label>
                        <div class="col-sm-8">
                            <textarea id="selling2" data-class="selling-point2" class="form-control" name="selling2">{{$post->selling2}}</textarea>
                            <small><span id ="charCounter2"></span></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Selling Point 3</label>
                        <div class="col-sm-8">
                            <textarea id="selling3" data-class="selling-point3" class="form-control" name="selling3">{{$post->selling3}}</textarea>
                            <small><span id ="charCounter3"></span></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="video_link" class="col-sm-3 control-label">Include a video on this listing?<br><small>Videos are only available for Featured listings</small></label>
                        <div class="col-sm-8">
                            <input onchange="checkVid()" id="video_link" class="form-control" type="text" name="video_link" 
                                   placeholder="Videos are only available for Featured listings" value="{{$post->video_link}}"/>
                            <small><span id ="vidOK">Please enter a valid Vimeo or Youtube URL</span></small>
                        </div>
                    </div>

                    <h3 class='orangeudnerline' style="margin-bottom:25px;  margin-top: 25px;">Job Details</h3>          
                    <div class="form-group" style="display:none">
                        <label for="country" class="col-sm-3 control-label">{{trans('messages.countries')}}</label>
                        <div class="col-sm-8">
                            <select id="country" name="country" class="form-control"></select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="joblocation" class="col-sm-3 control-label">Job Location</label>
                        <div class="col-sm-8">
                            <input id="joblocation" class="form-control geocomplete" type="text" value="{{$post->joblocation}}" name="joblocation" />
                            <input id="lat" class="form-control geocomplete" type="hidden" value="{{$post->lat}}" name="lat" />
                            <input id="lng" class="form-control geocomplete" type="hidden" value="{{$post->lng}}" name="lng" />
                            <input id="city" class="form-control geocomplete" type="hidden" value="{{$post->city}}" name="city" />
                            <input id="postcode" class="form-control geocomplete" type="hidden" value="{{$post->postcode}}" name="postcode" />
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="state" class="col-sm-3 control-label required">{{trans('messages.states')}}</label>
                        <div class="col-sm-8">
                            <select id="state" name="state" class="form-control">
                                <option @if($post->state == "") selected='selected' @endif value="">Select State</option><option value="Australian Capital Territory">Australian Capital Territory</option>
                                <option @if($post->state == "New South Wales") selected='selected' @endif value="New South Wales">New South Wales</option>
                                <option @if($post->state == "Northern Territory") selected='selected' @endif value="Northern Territory">Northern Territory</option>
                                <option @if($post->state == "Queensland") selected='selected' @endif value="Queensland">Queensland</option>
                                <option @if($post->state == "South Australia") selected='selected' @endif value="South Australia">South Australia</option>
                                <option @if($post->state == "Tasmania") selected='selected' @endif value="Tasmania">Tasmania</option>
                                <option @if($post->state == "Victoria") selected='selected' @endif value="Victoria">Victoria</option>
                                <option @if($post->state == "Western Australia") selected='selected' @endif value="Western Australia">Western Australia</option>
                            </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="category" class="col-sm-3 control-label">Job Category</label>
                        <div class="col-sm-8">
                            <select id="category" name="category" class="form-control">
                                @foreach($categories as $category)
                                <option value="{{$category->id}}" @if($category->id == $post->category) selected="selected" @endif>{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sub_category" class="col-sm-3 control-label">Secondary Category</label>
                        <div class="col-sm-8">

                            <select id="sub_category" name="sub_category" class="form-control">
                                @foreach($subcategories as $subcategory)
                                <?php if ($subcategory->parent_id == $post->category_id) { ?>
                                    <option value="{{$subcategory->id}}" @if($subcategory->id == $post->subcategory) selected="selected" @endif >{{$subcategory->title}}</option>
                                <?php } ?>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="salary_type" class="col-sm-3 control-label required">Salary Type</label>
                        <div class="col-sm-8">
                            <select id="salary_type" class="form-control" name="salarytype" onchange="changeSal()">
                                <option @if($post->salarytype == "annual") selected="selected" @endif value="annual">Annual Salary</option>
                                <option @if($post->salarytype == "hourly") selected="selected" @endif value="hourly">Hourly Salary</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group"  @if($post->salarytype != 'annual') style="display:none" @endif id="sal">
                         <label for="salary" class="col-sm-3 control-label">Annual Salary $</label>
                        <div class="col-sm-8">
                            <input id="salary" class="form-control" type="text"  value="{{$post->salary}}" name="salary" />
                        </div> 
                    </div>

                    <div class="form-group" id="hourlysal" @if($post->salarytype != 'hourly') style="display:none" @endif>
                         <label for="hrsalary" class="col-sm-3 control-label">Hourly Salary $</label>
                        <div class="col-sm-8">
                            <input id="hrsalary" class="form-control" type="text"  value="{{$post->hrsalary}}" name="hrsalary" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="salary_info" class="col-sm-3 control-label required">Incentive Structure</label>
                        <div class="col-sm-8">
                    <select id="incentivestructure" class="form-control" name="incentivestructure">
                            <option @if($post->incentivestructure == "fixed") selected='selected' @endif value="fixed">
                               Base + Super
                            </option>
                            <option @if($post->incentivestructure == "basecommbonus") selected='selected'
                                    @endif value="basecommbonus">Base + Super + R&R/Bonus
                            </option>
                            <option @if($post->incentivestructure == "basecomm") selected='selected'
                                    @endif value="basecomm">Base + Super + Commissions
                            </option>
                            <option @if($post->incentivestructure == "commonly") selected='selected'
                                    @endif value="commonly">Commissions Only
                            </option>
                    </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="pay_cycle" class="col-sm-3 control-label required">Pay Frequency</label>
                        <div class="col-sm-8">
                            <select id="pay_cycle" class="form-control" name="paycycle">
                                <option @if($post->paycycle == "weekly") selected='selected' @endif value="weekly">Weekly</option>
                                <option @if($post->paycycle == "fortnightly") selected='selected' @endif value="fortnightly">Fortnightly</option>
                                <option @if($post->paycycle == "monthly") selected='selected' @endif value="monthly">Monthly</option>
                                <option @if($post->paycycle == "no") selected='selected' @endif value="no">Not Disclosed</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Show salary on listing?</label>
                        <div class="col-sm-8">

                            <select id="showsalary" name="showsalary" class="form-control">
                                <option @if($post->showsalary == "0") selected='selected' @endif value="0">No</option>
                                <option @if($post->showsalary == "1") selected='selected' @endif value="1">Yes</option>
                            </select>

                        </div>
                    </div>



                    <div class="form-group">
                        <label for="employment_type" class="col-sm-3 control-label required">{{trans('messages.post_employment_type')}}</label>
                        <div class="col-sm-8">
                            <select id="employment_type" name="employment_type" class="form-control">
                                <option @if($post->employment_type == "") selected='selected' @endif value="">{{trans('messages.post_employment_select_type')}}</option>
                                <option @if($post->employment_type == "part_time") selected='selected' @endif value="{{\App\Posts::EMPLOYMENT_TYPE_PART_TIME}}">{{trans('messages.post_employment_type_part_time')}}</option>
                                <option @if($post->employment_type == "full_time") selected='selected' @endif value="{{\App\Posts::EMPLOYMENT_TYPE_FULL_TIME}}">{{trans('messages.post_employment_type_full_time')}}</option>
                                <option @if($post->employment_type == "casual") selected='selected' @endif value="{{\App\Posts::EMPLOYMENT_TYPE_CASUAL}}">{{trans('messages.post_employment_type_casual')}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="employment_term" class="col-sm-3 control-label required">{{trans('messages.post_employment_term')}}</label>
                        <div class="col-sm-8">
                            <select id="employment_term" name="employment_term" class="form-control">
                                <option @if($post->employment_term == "") selected='selected' @endif value="">{{trans('messages.post_employment_select_term')}}</option>
                                <option @if($post->employment_term == "permanent") selected='selected' @endif value="{{\App\Posts::EMPLOYMENT_TERM_PERMANENT}}">{{trans('messages.post_employment_permanent')}}</option>
                                <option @if($post->employment_term == "fixed_term") selected='selected' @endif value="{{\App\Posts::EMPLOYMENT_FIXED_TERM}}">{{trans('messages.post_employment_fixed_term')}}</option>
                                <option @if($post->employment_term == "temp") selected='selected' @endif value="{{\App\Posts::EMPLOYMENT_TEMP}}">{{trans('messages.post_employment_temp')}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                            <label for="workfromhome" class="col-sm-3 control-label required" required>Work Location</label>
                            <div class="col-md-8">
                            <select  name="work_from_home" class="form-control">
                                <option value="1" @if($post->work_from_home == 1) selected="selected" @endif >At business address</option>
                                <option value="2" @if($post->work_from_home == 2) selected="selected" @endif >By mutual agreement</option>
                                <option value="3" @if($post->work_from_home == 3) selected="selected" @endif >Work from home</option>
                            </select>
                            </div>

                    </div>
                    <div class="form-group">
                            <label for="workfromhome" class="col-sm-3 control-label required" required>Shift Guide</label>
                            <div class="col-md-8">
                            <select id="parentsoption" name="parentsoption" class="form-control">
                                <option value="1" @if($post->parentsoption == 1) selected="selected" @endif >Standard Business Hours (Shifts between hours of 9.00am and 5.00pm Mon to Fri)</option>
                                <option value="2" @if($post->parentsoption == 2) selected="selected" @endif >Extended Business Hours (Shifts between hours of 8.00am and 8.00pm Mon to Fri)</option>
                                <option value="3" @if($post->parentsoption == 3) selected="selected" @endif >Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only)</option>
                                <option value="4" @if($post->parentsoption == 4) selected="selected" @endif >Afternoon Shift (Shifts between hours of 12pm and 12am)</option>
                                <option value="5" @if($post->parentsoption == 5) selected="selected" @endif >Night Shifts</option>
                                <option value="6" @if($post->parentsoption == 6) selected="selected" @endif >Rotating Shifts - no weekends</option>
                                <option value="7" @if($post->parentsoption == 7) selected="selected" @endif >Rotating Shifts - including weekend work</option>
                            </select>
                            </div>

                    </div>
                    <h3 class='orangeudnerline' style="margin-bottom:25px; margin-top: 25px;">Application Submission Information</h3>   
                    <div class="form-group">
                        <label class="col-sm-3 control-label">How would you like to receive applications?</label>
                        <div class="col-sm-8">

                            <select id="wheretosend" name="application_method" onchange="showFields()"  class="form-control">
                                <option @if($post->person_email != '') selected='selected' @endif value="0">I want to receive applications via ItsMyCall</option>
                                <option @if($post->email_or_link != '') selected='selected' @endif value="1">Send applicants to a website or link to complete their application</option>
                            </select>

                        </div>
                    </div>
                <div class="field-wrap @if($post->email_or_link == '') @else hidden @endif" id="inside-itsmycall">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="padding-top: 0;">This is how you'll recieve job applications</label>
                        <ul class="col-sm-8" style="list-style-type: disc !important; margin-left: 15px; ">
                        <li>All applications and resumes will be stored in your account</li>
                        <li>You can manage the applicants status and send emails directly to applicants from ItsMyCall</li>
                        <li>You can still receive email alerts for each application and even send an alert to second email address if you choose to.</li>
                    </ul>
                    </div>
                    <div class="form-group">
                        <label for="person_email" style="padding-top: 0;" class="col-sm-3 control-label">Applications will be sent to your current email address</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" disabled="disabled" value="{{Auth::user()->email}}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="person_email" style="padding-top: 0;" class="col-sm-3 control-label">Send applications to another email address also? <a href="javascript:void(0)" data-toggle="tooltip" onclick="loadCoverModal('<b>Send applications to another email address also?: </b> This may be useful if you also want other Key Stakeholders to be notified of new applicants for this role')" title="Click for help" class="fa fa-info-circle" tabindex="-1"></a></label>
                        <div class="col-sm-8">
                            <input id="person_email" class="form-control" type="email" name="person_email"
                                   placeholder="Secondary Email" value="{{$post->person_email}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="person_email" class="col-sm-3 control-label">Activate free screening questions</label>
                        <div class="col-sm-8">
                            <input id="" class="form-control" type="checkbox" name="activate_screening" @if($post->selectquestions) checked="checked" @endif value="1"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="upgrades" class="col-sm-3 control-label">Screening Question Details</label>
                        <div class="col-sm-8">
                            <?php $trans_sc = \App\Transactions::where('job_id',$post->id)->where('questions','!=','')->get();
                                  $trans_sc_free = \App\Transactions::where('job_id',$post->id)->where('free_upgrade','!=','')->get();
                             ?>
                            @if(count($trans_sc) > 0)
                                @foreach($trans_sc as $key)
                                <p style="margin: 7px 0 0px;">{{\Carbon\Carbon::parse($key->created_at)->format('d M Y H:i:s')}} : {{$key->questions}} </p>
                                @endforeach
                            @elseif(count($trans_sc_free) > 0)
                                @foreach($trans_sc_free as $key)
                                <p style="margin: 7px 0 0px;">{{\Carbon\Carbon::parse($key->created_at)->format('d M Y H:i:s')}} : Screening Questions ${{number_format($key->free_upgrade_price, 2, '.', '')}} - Free Upgrade </p>
                                @endforeach
                            @else
                                <p style="margin: 7px 0 0px;">Not Purchased</p>
                            @endif
                        </div>
                    </div>
                    @if($post->selectquestions)
                   <h3 class='col-sm-12' style="margin-top: 30px;">Optional Screening Questions </h3>
                    <div class="col-sm-12">
                    <?php 
                        $posts_select_question_text =  \App\Settings::where('column_key','posts_select_question_text')->first();
                    ?>
                    @if($posts_select_question_text) {!!$posts_select_question_text->value_txt!!} @endif             
                    </div> 
                    @if($post->questions)
                    <?php
                       $questions = json_decode($post->questions);
                    ?>
                    <div class="form-group">
                        <label for="" style="padding-top: 0;" class="col-sm-3 control-label">Screening Questions selected for this job <a href="javascript:void(0)" data-toggle="tooltip" onclick="loadCoverModal('<b>Screening Questions selected for this job: </b> Screening Questions that you have already selected for this job and will be asked to applicants when they apply for this job')" title="Click for help" class="fa fa-info-circle" tabindex="-1"></a></label>
                        <div class="col-sm-8">
                            <ul>
                                @foreach($questions as $question)
                                <li>
                                    {{$question}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="col-sm-12" style="padding-top: 0;">
                             <div class="table-responsive countgreater">
                                <table class="table table-condensed" style="border: 1px solid lightgray; margin-bottom: 0;">
                                <thead>
                                <tr style="color: white;background-color: #068e00; ">
                                    <th style="padding: 10px;">Question   
                                    </th>
                                    <th style="padding: 10px;">Modify</th>
                                    <th style="padding: 10px;">Select</th>
                                </tr>
                                </thead>
                                <tbody class="added_quest">
                                    <tr>
                                        <td style="border-right: 0;"><h4 style="font-size: 16px; font-weight: 600;">Default Questions</h4></td>
                                        <td style="width: 160px;border-right: 0; border-left: 0;"></td>
                                        <td style="border-left: 0;"></td>
                                    </tr>
                                    <?php
                                        $questions = NULL;
                                        if($post->questions){
                                            $questions = json_decode($post->questions);
                                        }
                                     ?>
                                    @foreach($admin_questions as $question)
                                    <tr>
                                        <td style="border-right: 0;word-wrap:break-word;">{{$question->question}}</td>
                                        <td style="border-right: 0; border-left: 0;"></td>
                                        <td style="width: 160px;border-left: 0;">
                                          <div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;">
                                            <input id="checkbox{{$question->id}}" class="question" type="checkbox" value="{{$question->question}}" name="question[]" @if($questions) @foreach($questions as $key) @if($key == $question->question) checked="checked" @endif @endforeach @endif />
                                            <label for="checkbox{{$question->id}}" style="font-size: 14px;margin-bottom: 2px; font-weight: 700;">Select</label>
                                          </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="border-right: 0;"><h4 style="font-size: 16px; font-weight: 600;"><a style="text-decoration: none;" onclick="openall()">Your Questions <i style="    font-size: 15px;" class="fa fa-chevron-down" aria-hidden="true"></i></a></h4></td>
                                        <td style="width: 160px;border-right: 0; border-left: 0;"></td>
                                        <td style="border-left: 0;"><a data-toggle="modal" style="color: ; padding: 7px 12px;" class="btn btn-primary btn-sm" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> Add New Question</a></td>
                                    </tr>
                                    <?php
                                        $questions = NULL;
                                        if($post->questions){
                                            $questions = json_decode($post->questions);
                                        }
                                     ?>
                                    @foreach($user_questions as $question)
                                    <tr class="table_question{{$question->id}} open_question hidden">
                                        <td class="question_edit" style="border-right: 0;word-wrap:break-word;">{{$question->question}}</td>
                                        <td style="width: 160px;border-right: 0; border-left: 0;">
                                            <a class="btn-info btn btn-sm" style="text-decoration: none;" data-id="{{$question->id}}" data-question="{{$question->question}}" onclick="questionedit(this)">
                                            Edit
                                            </a>
                                            <a class="btn-danger btn btn-sm" style="text-decoration: none;" data-id="{{$question->id}}" onclick="questionremove(this)">
                                            Remove
                                            </a>
                                        </td>
                                            
                                        <td style="border-left: 0;">
                                          <div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;">
                                            <input id="checkbox{{$question->id}}" class="question" type="checkbox" value="{{$question->question}}" name="question[]"
                                            @if($questions) @foreach($questions as $key) @if($key == $question->question) checked="checked" @endif @endforeach @endif  />
                                            <label for="checkbox{{$question->id}}" style="font-size: 14px;margin-bottom: 2px; font-weight: 700;">Select</label>
                                          </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                             </div>
                        </div>
                    </div>
                    @endif
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-lg" style="margin: 155px auto !important;">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Create a Custom Question</h4>
                          </div>
                          <div class="modal-body">
                                <span class="question_success hidden"><i class="fa fa-check" aria-hidden="true"></i> Your Question has been added Successfully!</span>
                                <input type="text" id="question" class="form-control" style="height: 34px;" placeholder="Enter Question" onkeyup="countChar(this)" maxlength="100">
                                <span id="charNum2" style="color: #068e00; font-size: 13px; width:100%; clear:both; height: 8px;">&nbsp</span>
                          </div>
                          <div class="modal-footer">
                            <a class="btn btn-default btn-sm questiongo" onclick="questionadd()">Save</a>
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>
                </div>
                    <div class="field-wrap @if($post->email_or_link != '') @else hidden @endif" id="outside-itsmycall" >
                        <div class="form-group">
                            <label for="email_or_link" id="linker" class="col-sm-3 control-label">Please enter the URL in which your applicants will be redirected to <i data-toggle="tooltip" title="If you also have the job advertised on your own website or another jobs website (e.g. Seek) then just add the URL and when applicants click the apply button they will be taken directly to that location)" class="fa fa-info-circle"></i></label>
                            <div class="col-sm-8">
                                <input id="email_or_link" class="form-control" type="text" name="email_or_link" placeholder="Valid URL" value="{{$post->email_or_link}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="package" class="col-sm-3 control-label">Job Ad Level Details</label>

                        <div class="col-sm-8">
                                <?php
                                $level_purchased = \App\Transactions::where('job_id',$post->id)->where('listing_type','!=','')->get();
                                $level_given = \App\Transactions::where('job_id',$post->id)->where('free_upgrade_package','!=','')->get();
                                ?>
                                @if(count($level_purchased) > 0)
                                    @foreach($level_purchased as $package)
                                        <p style="margin: 7px 0 0px;">{{\Carbon\Carbon::parse($package->created_at)->format('d M Y H:i:s')}} : {{$package->listing_type}} - Purchased</p>
                                    @endforeach
                                    @if(count($level_given) > 0)
                                    @foreach($level_given as $package)
                                        <p style="margin: 7px 0 0px;">{{\Carbon\Carbon::parse($package->created_at)->format('d M Y H:i:s')}} : {{$package->free_upgrade_price}} - Free Upgrade</p>
                                    @endforeach
                                    @endif
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="package" class="col-sm-3 control-label">Upgrade Job Ad Level</label>

                        <div class="col-sm-8">
                            <select id="package" class="form-control" name="package">

                                @foreach($posts_packages as $package)
                                {{old('package', $post->package->id) == $package->id ? '<option value=""></option>' : ''}}
                                <option value="{{$package->id}}" {{old('package', $post->package->id) == $package->id ? 'selected' : ''}}>{{$package->title}} - ${{number_format($package->price, 2, '.', '')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}}</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" id="coverModal" role="dialog">
    <div class="modal-dialog" style="margin: 155px auto !important;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <p id="Note"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<input type="hidden" id="user_id" name="" value="{{$post->author_id}}">
<script>  
$(function () {
        //result.address_components[0]
        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            //console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    //console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    $('#stateselect option[value="' + value.long_name + '"]').prop('selected', true);
                    $("#stateselect").val(value.long_name).change();
                    $("#state").val(value.long_name);
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            //console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            //console.log(result.geometry.location.lng());
            //console.log(result);


        });
});
</script>
<script>
    function openall(){
        if($('.open_question').hasClass( "hidden" )){
           $('.open_question').removeClass('hidden');
        }else{
            $('.open_question').addClass('hidden');
        }

    }
    function questionremove(val) {
        var id = $(val).data('id');
        var data = {id: id};
        $.ajax({
        url: "/admin/posts/removequestion",
                data: data,
                headers:
        {
        'X-CSRF-Token': $('input[name="_token"]').val()
        },
                method: "post",
                success: function (response) {
                    $('.table_question'+id).remove();
                console.log(response);
                },
                error:  function (response) {
                console.log(response);
                }
        });
      };
    function questionedit(val) {
        var id = $(val).data('id');
        var question = $('.table_question'+ id +' .question_edit').text();
        $('.table_question'+ id +' .btn-info').attr('disabled', 'disabled');
        var data = {id: id};
        $('.table_question'+ id +' .question_edit').html('<input data-id="'+ id +'" data-question="'+ question +'" class="form-control edited_question" style="height: 34px; margin-bottom:5px;" placeholder="Edit Question" value="'+ question +'" maxlength="100" /> <a style="text-decoration: none;" data-id="'+ id +'" data-question="'+ question +'" class="btn-default btn btn-sm" onclick="editsave(this)">Save</a> <a style="text-decoration: none;" data-id="'+ id +'" data-question="'+ question +'" class="btn-default btn btn-sm" onclick="savecancel(this)">Cancel</a>')
      };
    function savecancel(val) {
        var id = $(val).data('id');
        var question = $(val).data('question');
        $('.table_question'+ id +' .question_edit').html(question)
        $('.table_question'+ id +' .btn-info').removeAttr('disabled');
      };
    function editsave(val) {
        var id = $(val).data('id');
        var question = $('.edited_question').val();
        $('.table_question'+ id +' .btn-info').removeAttr('disabled');
        if(!$('.edited_question').val()){
            var data = {id: id};
            $.ajax({
            url: "/admin/posts/removequestion",
                    data: data,
                    headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            },
                    method: "post",
                    success: function (response) {
                        $('.table_question'+id).remove();
                    console.log(response);
                    },
                    error:  function (response) {
                    console.log(response);
                    }
            }); 
        }else{
            var data = {id: id , question:question};
            $.ajax({
            url: "/admin/posts/edit_question",
                    data: data,
                    headers:
            {
            'X-CSRF-Token': $('input[name="_token"]').val()
            },
                    method: "post",
                    success: function (response) {
                        $('.table_question'+ id +' .question_edit').html(question)
                        $('.table_question'+ id +' .question').val(question)
                    console.log(response);
                    },
                    error:  function (response) {
                    console.log(response);
                    }
            });
        }
      };
        $(document).ready(function () {
           $(".question").change(function () {
              var maxAllowed = 5;
              var cnt = $(".question:checked").length;
              if (cnt > maxAllowed)
              {
                 $(this).prop("checked", "");
                 alert('You can only select a maximum of 5 screening questions.');
             }
          });
        });

    function countChar(val) {
        if($(val).val()){
            $('.questiongo').removeAttr('disabled')
            $('#question').css('background-color', 'initial');
        }
        $('#charNum2').removeClass('hidden');
        var maxLength = 100;
        var length = $(val).val().length;
        var length = maxLength-length;
        $('#charNum2').text('Remaining Characters: '+length+'/100');
      };
    function questionadd() {
    // alert("hello");
    if(!$('#question').val()){
        $('#question').css('border', '1px solid red');
        $('.questiongo').attr('disabled', 'disabled');
    }else{
        var question = $('#question').val();
        var userid = $('#user_id').val()
        var data = {question: question,user_id: userid};
        $.ajax({
        url: "/admin/posts/addquestion",
                data: data,
                headers:
        {
        'X-CSRF-Token': $('input[name="_token"]').val()
        },
                method: "post",
                success: function (response) {
                    $('.added_quest').append('<tr class="table_question'+ response +' open_question"><td class="question_edit" style="border-right: 0;word-wrap:break-word;">' + question + '</td><td style="width: 160px;border-right: 0; border-left: 0;"><a class="btn-info btn btn-sm" style="text-decoration: none;" data-question="'+ question +'" data-id="'+ response +'" onclick="questionedit(this)">Edit</a> <a style="text-decoration: none;" data-question="'+ question +'" data-id="'+ response +'" class="btn-danger btn btn-sm" onclick="questionremove(this)">Remove</a></td><td style="border-left: 0;"><div class="standout" style="width: 115px;padding: 0px;padding-top: 2px;margin: 0;border-radius: 4px !important;"><input id="checkbox'+ response +'" class="question" type="checkbox" value="'+ question +'" name="question[]"  /> <label for="checkbox'+ response +'" style="font-size: 14px; margin-bottom: 2px; font-weight:700;"> Select</label></div></td></tr>');
                    $('#question').val('')
                    $('.open_question').removeClass('hidden');
                    $('#charNum2').addClass('hidden');
                    $('#button_remove').addClass('hidden');
                    $('.question_success').removeClass('hidden');
                    $('.countgreater').removeClass('hidden');
                    $('#myModal').modal('hide');
                setTimeout(function () { 
                    $('.question_success').addClass('hidden');
                 }, 5000);
                console.log(response);
                },
                error:  function (response) {
                console.log(response);
                }
        });
    }
    }
<?php if (strlen($post->email_or_link) < 0) { ?>
        $("#onbtn").click();

<?php } else { ?>
        $("#inbtn").click();
<?php } ?>
    function loadCoverModal(id) {
        $('#coverModal').modal('toggle');
        $('#Note').html(id);
        }
    function showFields() {
        var val = $('#wheretosend').val();
        if (val == 0) {
            $('#inside-itsmycall').removeClass('hidden');
            $('#inside-itsmycall').show();
            $('#email_or_link').val('');
            $('#outside-itsmycall').addClass('hidden');
            $('#email_or_link').removeClass("required");
        } else {
            $('#outside-itsmycall').removeClass('hidden');
            $('#email_or_link').addClass("required");
            $('#inside-itsmycall').hide();
        }

    }

    function changeSal() {
        var val = $('#salary_type').val();
        if (val == 'hourly') {
            $('#hourlysal').show('slow');
            $('#sal').hide('slow');
            $('#hrsallab').addClass("required");
            $('#sallab').removeClass('yes');
        } else {

            $('#hourlysal').hide('slow');
            $('#sal').show('slow');
            $('#sallab').addClass("required");
            $('#hrsallab').removeClass('yes');
        }

    }
    var changer = false;
    function changeCat() {
        if (changer) {
            $.ajax({
                url: "/api/get_sub_categories_by_category/" + $('#category').val(),
                success: function (sub_categories) {

                    var $sub_category_select = $('#sub_category');
                    $sub_category_select.find('option').remove();

                    $.each(sub_categories, function (key, value) {
                        $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>');
                    });
                },
                error: function (response) {
                }
            });
        }
    }

</script>
@stop