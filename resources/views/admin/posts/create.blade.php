@extends('admin.layouts.master')

@section('extra_css')
    <link rel="stylesheet" href="/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
@stop

@section('content')

    <h3 class="page-title">
        {{trans('messages.posts')}}
        <small>{{trans('messages.manage_posts')}}</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/posts">{{trans('messages.posts')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/posts/create"> {{trans('messages.create_new_post')}} </a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs"></i>{{trans('messages.create_new_post')}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/posts/create" id="form-username" method="post"
                          class="form-horizontal form-bordered" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        
                        <div class="form-group" id="">
                            <label for="paid"
                                   class="col-sm-3 control-label">Payment Status</label>
                            <div class="col-sm-8"> 
                                <select name="paid" id="paid" class="form-control">
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                        
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="author_id" class="col-sm-3 control-label">Job Owner</label>
                            <div class="col-sm-8">
                                <select name="author_id" id="author_id" class="form-control">
                                    <option value="">Please Select</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}} ({{$user->company}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">{{trans('messages.company')}}</label>

                            <div class="col-sm-8">
                                <input id="company " class="form-control" type="text" name="company"
                                       placeholder="{{trans('messages.enter_post_company')}}" value="{{old('company')}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-3 control-label">{{trans('messages.title')}}</label>

                            <div class="col-sm-8">
                                <input id="title" class="form-control" type="text" name="title"
                                       placeholder="{{trans('messages.enter_post_title')}}" value="{{old('title')}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="short_description"
                                   class="col-sm-3 control-label">{{trans('messages.short_description')}}</label>

                            <div class="col-sm-8">
                                <textarea id="short_description" class="form-control" name="short_description"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description"
                                   class="col-sm-3 control-label">Long {{trans('messages.description')}}</label>

                            <div class="col-sm-8">
                                <textarea id="description" class="form-control" name="description"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selling1" class="col-sm-3 control-label">Selling Point 1</label>
                            <div class="col-sm-8">
                                <textarea id="selling1" data-class="selling-point1" class="form-control" name="selling1" maxlength="60" ></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selling2" class="col-sm-3 control-label">Selling Point 2</label>
                            <div class="col-sm-8">
                                <textarea id="selling2" data-class="selling-point2" class="form-control" name="selling2" maxlength="60"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selling3" class="col-sm-3 control-label">Selling Point 3</label>
                            <div class="col-sm-8">
                                <textarea id="selling3" data-class="selling-point3" class="form-control" name="selling3" maxlength="60"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="video_link" class="col-sm-3 control-label">Video Link</label>
                            <div class="col-sm-8">
                                <input  id="video_link" class="form-control" type="text" name="video_link"
                                       placeholder="{{trans('messages.enter_post_video_link')}}" value="http://"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="joblocation" class="col-sm-3 control-label required">Job Location</label>
                            <div class="col-sm-8">
                                <input id="joblocation" class="form-control geocomplete" type="text" name="joblocation" />
                                <input id="lat" class="form-control geocomplete" type="hidden" name="lat" />
                                <input id="lng" class="form-control geocomplete" type="hidden" name="lng" />
                                <input id="city" class="form-control geocomplete" type="hidden" name="city" />
                                <input id="postcode" class="form-control geocomplete" type="hidden" name="postcode" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="col-sm-3 control-label">{{trans('messages.countries')}}</label>
                            <div class="col-sm-8">
                                <select id="country" name="country" class="form-control">
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="state" class="col-sm-3 control-label">{{trans('messages.states')}}</label>
                            <div class="col-sm-8">
                                <select id="state" name="state" class="form-control">
                                </select>
                            </div>
                        </div>

                        @foreach($categories as $category)
                        <input type="hidden" name="category_id" value="{{$category->id}}"/>
                        @endforeach

                        <div class="form-group">
                            <label for="category" class="col-sm-3 control-label">{{trans('messages.category')}}</label>

                            <div class="col-sm-8">
                                <select id="category" name="category" class="form-control">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="sub_category"
                                   class="col-sm-3 control-label">{{trans('messages.sub_category')}}</label>
                            <div class="col-sm-8">
                                <select id="sub_category" name="sub_category" class="form-control">

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="salarytype" class="col-sm-3 control-label required">Salary Type</label>
                            <div class="col-sm-8">
                                <select id="salarytype" class="form-control" name="salarytype">
                                    <option value="">Please Select</option>
                                    <option value="annual">Annual Salary</option>
                                    <option value="hourly">Hourly Salary</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="annualsalary">
                            <label for="salary" class="col-sm-3 control-label">{{trans('messages.salary')}}</label>

                            <div class="col-sm-8">
                                <input id="salary" class="form-control" type="text" name="salary"
                                       placeholder="{{trans('messages.salary')}}" value="{{old('salary')}}"/>
                            </div>
                        </div>

                        <div class="form-group" id="hourlysal" style="display:none">
                            <label for="hrsalary" id='hrsallab' class="col-sm-3 control-label">Hourly Salary</label>
                            <div class="col-sm-8">
                                <input id="hrsalary" class="form-control" type="text" name="hrsalary" max="999.99" min="0"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="incentivestructure" class="col-sm-3 control-label required">Incentive Structure</label>
                            <div class="col-sm-8">
                                <select id="incentivestructure" class="form-control" name="incentivestructure">
                                    <option value="">Please Select</option>
                                    <option value="fixed">Base + Super</option>
                                    <option value="basecommbonus">Base + Super + R&R/Bonus</option>
                                    <option value="basecomm">Base + Super + Commissions</option>
                                    <option value="commonly">Commissions Only</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="paycycle" class="col-sm-3 control-label required" >Pay Frequency</label>
                            <div class="col-sm-8">
                                <select id="paycycle" class="form-control" name="paycycle">
                                    <option value="">Please Select</option>
                                    <option value="weekly">Weekly</option>
                                    <option value="fortnightly">Fortnightly</option>
                                    <option value="monthly">Monthly</option>
                                    <option value="no">Not Disclosed</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Show salary on listing?</label>
                            <div class="col-sm-8">
                                <select id="showsalary" name="showsalary"  class="form-control">
                                    <option value="">Please Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="employment_type" class="col-sm-3 control-label required">{{trans('messages.post_employment_type')}}</label>
                            <div class="col-sm-8">
                                <select id="emptype" name="employment_type" class="form-control">
                                    <option value="">Please Select</option>
                                    <option value="part_time">{{trans('messages.post_employment_type_part_time')}}</option>
                                    <option value="full_time">{{trans('messages.post_employment_type_full_time')}}</option>
                                    <option value="casual">{{trans('messages.post_employment_type_casual')}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="employment_term" class="col-sm-3 control-label required">{{trans('messages.post_employment_term')}}</label>
                            <div class="col-sm-8">
                                <select id="empterm" class="form-control" name="employment_term">
                                    <option value="">Please Select</option>
                                    <option value="permanent">{{trans('messages.post_employment_permanent')}}</option>
                                    <option value="fixed_term">Fixed-Term / Contract</option>
                                    <option value="temp">{{trans('messages.post_employment_temp')}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label required" for="workfromhome">Job Venue</label>
                            <div class="col-sm-8">
                                <select id="workfromhome" name="work_from_home" class="form-control">
                                    <option value="1">At business address</option>
                                    <option value="2">By mutual agreement</option>
                                    <option value="3">Work from home</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label required" for="parentsfilter">Shift Times</label>
                            <div class="col-sm-8">
                                <select id="parentsfiltervalue" name="parentsfilter" class="form-control">
                                    <option value="1">Standard Business Hours (Shifts between hours of 9.00am and 5.00pm Mon to Fri)</option>
                                    <option value="2">Extended Business Hours (Shifts between hours of 8.00am and 8.00pm Mon to Fri)</option>
                                    <option value="3">Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only)</option>
                                    <option value="4">Afternoon Shift (Shifts between hours of 12pm and 12am)</option>
                                    <option value="5">Night Shifts</option>
                                    <option value="6">Rotating Shifts - no weekends</option>
                                    <option value="7">Rotating Shifts - including weekend work</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="wheretosend">How would you like to receive applications?</label>
                            <div class="col-sm-8">
                                <select id="wheretosend" class="form-control" name="application_method">
                                    <option value="0" selected>I want to receive applications via ItsMyCall</option>
                                    <option value="1">Send applicants to a website or link to complete their application</option>
                                </select>
                            </div>
                        </div>
                        <div class="field-wrap" id="inside-itsmycall">
                            <div class="alert alert-success text-center">
                                <i class="fa fa-info-circle"></i>  Applications will be sent to {{Auth::user()->email}}. You can change this in your user preferences.
                            </div>
                            <div class="form-group">
                                <label for="person_email" class="col-sm-3 control-label">Send applications to another email address</label>
                                <div class="col-sm-8">
                                    <input id="person_email" class="form-control" type="email" name="person_email" placeholder="Secondary Email" value="{{old('person_email')}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="field-wrap" id="outside-itsmycall" style="display:none">
                            <div class="form-group">
                                <label for="email_or_link" id="linker" class="col-sm-3 control-label">Please enter the URL in which your applicants will be redirected to <a href="javascript:void(0)" data-toggle="tooltip" onclick="loadCoverModal('<b>Please enter the URL in which your applicants will be redirected to: </b> If you also have the job advertised on your own website or another jobs website (e.g. Seek) then just add the URL and when applicants click the apply button they will be taken directly to that location)')" title="Click for help" class="fa fa-info-circle"></a></label>
                                <div class="col-sm-8">
                                    <input id="email_or_link" class="form-control" type="text" name="email_or_link" placeholder="Enter valid URL (e.g. http://www.google.com)" value="{{old('email_or_link')}}"/>
                                </div>
                            </div>

                        </div>


                        <div class="form-group" id="featured_image_div">
                            <label for="featured_image"
                                   class="col-sm-3 control-label">{{trans('messages.company_logo')}}</label>

                            <div class="col-sm-8">
                                <input id="featured_image" class="form-control" type="file" name="featured_image"/>
                            </div>
                        </div>

                        <div class="form-group" id="featured_image_div">
                            <label for="job_image"
                                   class="col-sm-3 control-label">{{trans('messages.job_image')}}</label>

                            <div class="col-sm-8">
                                <input id="job_image" class="form-control" type="file" name="job_image"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="status" class="col-sm-3 control-label">{{trans('messages.status')}}</label>

                            <div class="col-sm-8">
                                <select id="status" class="form-control" name="status">
                                    @foreach(\App\Posts::getPositionStatuses() as $status => $label)
                                    <option value="{{$status}}">{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="expired_at" class="col-sm-3 control-label">{{trans('messages.expired_on')}}</label>

                            <div class="col-sm-8">
                                <input id="expired_at" class="form-control datetimepicker" type="datetime" name="expired_at" placeholder="{{config('app.datetime_format.php')}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="package" class="col-sm-3 control-label">{{trans('messages.post_package')}}</label>

                            <div class="col-sm-8">
                                <select id="package" class="form-control" name="package">
                                    @foreach($posts_packages as $package)
                                        <option value="{{$package->id}}">{{$package->title}} - ${{$package->price}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="upgrades" class="col-sm-3 control-label">{{trans('messages.post_upgrades')}}</label>--}}
                            {{--<div class="col-sm-8">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-sm-8">--}}
                                        {{--<label for="upgrade_name">{{trans('messages.post_upgrade_name_label')}}</label>--}}
                                        {{--<select class="form-control" id="upgrade_name">--}}
                                            {{--@foreach($upgrades as $upgrade)--}}
                                                {{--<option value="{{$upgrade->id}}">{{$upgrade->name}} - ${{$upgrade->price}} {{$upgrade->type}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<label for="upgrade_time">{{trans('messages.post_upgrade_time_label')}}</label>--}}
                                        {{--<div class="input-group">--}}
                                            {{--<select class="form-control" id="upgrade_time">--}}
                                                {{--<option value="1" selected>1 week</option>--}}
                                                {{--<option value="2">2 weeks</option>--}}
                                                {{--<option value="3">3 weeks</option>--}}
                                                {{--<option value="4">4 weeks</option>--}}
                                            {{--</select>--}}
                                            {{--<span class="input-group-btn">--}}
                                                {{--<button style="margin-left: 15px;" class="btn btn-default" type="button" id="add_upgrade_btn">{{trans('messages.post_upgrade_add_upgrade')}}</button>--}}
                                            {{--</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<hr/>--}}
                                {{--<div class="row" id="additional-upgrades-wrapper"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop