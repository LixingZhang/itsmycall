@extends('admin.layouts.master')
@section('extra_js')

<script type="text/javascript" src="/assets/js/countries.js"></script>
<script>populateCountries("country1", "state2");</script>
<script>

    var arCats = [];
    var arSubCats = [];
<?php foreach ($categories as $category) { ?>
        var oCatValKey = {
            id: "<?php echo $category->id ?>",
            title: "<?php echo $category->title ?>"
        };
        arCats.push(oCatValKey);

        var arSubCat = [];
    <?php foreach ($category->subcategory as $subcategory) { ?>
            var oSubCatValKey = {
                id: "<?php echo $subcategory->id ?>",
                title: "<?php echo $subcategory->title ?>"
            };
            arSubCat.push(oSubCatValKey);
    <?php } ?>

        arSubCats[oCatValKey.id] = arSubCat;
<?php } ?>
    function populateSubCats(categoryElementId, subcategoryElementId) {
        var select = document.getElementById(categoryElementId);
        var value = select.options[select.selectedIndex].value;

        var subcategoryElement = document.getElementById(subcategoryElementId);
        subcategoryElement.length = 0;
        subcategoryElement.options[0] = new Option('All Sub Categories', '-1');
        subcategoryElement.selectedIndex = 0;

        var arSubCat = arSubCats[value];
        for (var i = 0; i < arSubCat.length; i++) {
            var objSubCat = arSubCat[i];
            subcategoryElement.options[subcategoryElement.length] = new Option(objSubCat.title, objSubCat.id);
            if (subcategoryElement.getAttribute('value') == objSubCat.id) {
                subcategoryElement.value = subcategoryElement.getAttribute('value');
            }
        }
    }
    
    var selector = $('.panel-heading a[data-toggle="collapse"]');
            selector.on('click', function () {
                console.log("hekllo")
                console.log(this)
                var self = this;
                if ($(this).hasClass('collapsed')) {
                    $.each(selector, function (key, value) {
                        if (!$(value).hasClass('collapsed') && value != self) {
                            $(value).trigger('click');
                        }
                    });
                }
            });

    function populateMainCats(categoryElementId, subcategoryElementId) {
        var categoryElement = document.getElementById(categoryElementId);
        categoryElement.length = 0;
        categoryElement.options[0] = new Option('Select Category', '-1');
        categoryElement.selectedIndex = 0;

        for (var i = 0; i < arCats.length; i++) {
            var objCat = arCats[i];
            categoryElement.options[categoryElement.length] = new Option(objCat.title, objCat.id);
        }
        if (categoryElement.getAttribute('value')) {
            categoryElement.value = categoryElement.getAttribute('value');
        }

        // Assigned all countries. Now assign event listener for the states.
        if (subcategoryElementId) {
            categoryElement.onchange = function () {
                populateSubCats(categoryElementId, subcategoryElementId);
            };
            //populateSubCats(categoryElementId, subcategoryElementId);
        }
    }

    populateMainCats("category", "subcategory");


    $(function () {
        //result.address_components[0]
        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    console.log("Hello");
                    $("#state2").val(value.long_name).change();
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            console.log(result.geometry.location.lng());
            console.log(result);


        });

        $("#find").click(function () {
            $("#joblocation").trigger("geocode");
        });

        $('#find').keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                $("#joblocation").trigger("geocode");
            }
        });


        $("#examples a").click(function () {
            $("#joblocation").val($(this).text()).trigger("geocode");
            return false;
        });

    });
    $('#searchour').on('change', function () {
        if ($('#searchour').is(':checked')) {
            $('.salrwapperannual').addClass("hidden")
            $('.hourlywapperannual').removeClass("hidden")
        } else {
            $('.hourlywapperannual').addClass("hidden")
            $('.salrwapperannual').removeClass("hidden")
        }
    });


</script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });
    });

</script>

<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
    <script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $('#html_notes').redactor({
                imageUpload: '/admin/pages/redactor',
                imageManagerJson: '/admin/pages/redactor/images.json',
                plugins: ['imagemanager'],
                minHeight: 300, // pixels
                replaceDivs: false,
                convertDivs: false,
                uploadImageFields: {
                    _token: "{{csrf_token()}}"
                }
            });
    </script>
@stop
@section('extra_css')
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
<style type="text/css">
    .alert{
                margin-bottom: 3px !important;
        }
    h5{
        font-weight: 700;
        font-size: 13px;
    }
    .row-bor{
        border-right: 0px;
        min-height: ;
    }
    @media screen and (min-width: 995px) {
    .row-bor{
        border-right: 2px solid lightgray;
        min-height: 520px;
    }
    }
</style>
@stop
@section('content')

    <h3 class="page-title">
        Create Job Search Preset
        <small> Create Job Search Presets</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/posts/jobsearch">Job Search Presets</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/posts/jobsearch/create">Job Search Preset Create</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <form action="/admin/posts/jobsearch/edit/{{$preset->id}}" method="POST" role="form">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Create Job Search Presets
                    </div>
                    <div class="tools">
                        <a href="javascript:" class="collapse">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include('admin.layouts.notify')
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="result-filter">
                                    <input id="sort" class="form-control" name="sort" type="hidden" value="date"/>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="row row-bor">
                                                <div class="col-md-12">
                                                <H3 style="padding-bottom: 20px;">Job Details</H3>
                                                </div>
                                                <div class="col-md-12">
                                                <h5>
                                                <a href="#s_collapse_2" data-toggle="collapse">Job Category</a>
                                                </h5>
                                                <div class="form-group ">
                                                    <div class="select-style">
                                                        <select class="form-control category" name="category" id="category">
                                                            @foreach($categories as $category)
                                                            @if ($preset->category)
                                                            <option @if($category->id == $preset->category) selected=selected @endif value="{{$category->id}}" >{{$category->title}}</option>
                                                            @else 
                                                            <option value="{{$category->id}}" >{{$category->title}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        <div class="col-md-12 text-center" style="padding-bottom: 10px;">
                                                           <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i> 
                                                        </div>
                                                        
                                                    </div>
                                                    <h5>
                                                    <a href="#s_collapse_2" data-toggle="collapse">Sub Category</a>
                                                    </h5>
                                                    <div class="select-style">
                                                        <select class="form-control"
                                                                style="margin-top:15px;"
                                                                name="subcategory" id="subcategory">
                                                            <option value="-1">All Sub Categories</option>
                                                            @foreach($categories as $category)
                                                            @foreach($category->subcategory as $subcategory)
                                                            <option @if($preset->subcategory == $subcategory->id) selected=selected @endif value="{{$subcategory->title}}">{{$subcategory->title}}</option>
                                                            @endforeach
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                            <h5 class="font-bold margin-b-20 ">
                                                <a href="#s_collapse_2" data-toggle="collapse">{{trans('messages.post_employment_type')}}</a>
                                            </h5>
                                            <div>
                                                <label>
                                                    <input type="checkbox" name="employment_type[]" value="{{\App\Posts::EMPLOYMENT_TYPE_PART_TIME}}">
                                                    {{trans('messages.post_employment_type_part_time')}}
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="checkbox" name="employment_type[]" value="{{\App\Posts::EMPLOYMENT_TYPE_FULL_TIME}}">
                                                    {{trans('messages.post_employment_type_full_time')}}
                                                </label>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="font-bold margin-b-20">
                                            <a href="#s_collapse_3"
                                               data-toggle="collapse">{{trans('messages.post_employment_term')}}</a>
                                        </h5>
                                        <div>
                                            <label>
                                                <input type="checkbox" name="employment_term[]"
                                                       value="{{\App\Posts::EMPLOYMENT_TERM_PERMANENT}}">
                                                {{trans('messages.post_employment_permanent')}}
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input type="checkbox" name="employment_term[]"
                                                       value="{{\App\Posts::EMPLOYMENT_FIXED_TERM}}">
                                                Fixed-Term / Contract
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input type="checkbox" name="employment_term[]"
                                                       value="{{\App\Posts::EMPLOYMENT_TEMP}}">
                                                {{trans('messages.post_employment_temp')}}
                                            </label>
                                        </div>
                                    </div>
                                        <div class="col-md-12">
                                            <h5 class="font-bold">
                                                <a href="#s_collapse_2" data-toggle="collapse">Job Keywords</a>
                                            </h5>
                                            <div class="form-group" >
                                                <input type="text" name="title" class="form-control" placeholder="Keywords" value="{{$preset->title}}">
                                            </div>
                                        </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4" style="">
                                            <div class="row row-bor">
                                            <div class="col-md-12">
                                                    <H3 style="padding-bottom: 20px;">Job Specifics</H3>
                                                </div>
                                            <div class="col-md-12">
                                            <h5>
                                            <a href="#s_collapse_2" data-toggle="collapse">Job Location</a>
                                            </h5>

                                            <div class="form-group" style="margin-bottom: 0px;" >
                                                <div class="form-group" style="margin-bottom: 5px;">
                                                    <input id="joblocation" class="form-control geocomplete" type="text" name="joblocation" placeholder="Enter an address, suburb or state" value="" />
                                                    <input id="lat" class="form-control geocomplete" type="hidden" name="lat" value="" />
                                                    <input id="lng" class="form-control geocomplete" type="hidden" name="lng" />
                                                    <input id="city" class="form-control geocomplete" type="hidden" name="city" />
                                                    <input id="postcode" class="form-control geocomplete" type="hidden" name="postcode" />
                                                </div>
                                            </div>

                                            <div class="col-md-12 text-center" style="padding-bottom: 5px;">
                                                    <h5>OR</h5>
                                                </div>
                                            <div class="form-group">
                                                <div class="form-group" style="display:none">
                                                    <select  id="country1" name="country" class="form-control"></select>
                                                    <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i>
                                                </div>
                                                <div class="form-group">
                                                    <select margin-top:15px;" id="state2"
                                                            name="state" class="form-control">
                                                        <option value="">Select State</option>
                                                        <option value="Australian Capital Territory">Australian
                                                            Capital Territory
                                                        </option>
                                                        <option value="New South Wales">New South Wales</option>
                                                        <option value="Northern Territory">Northern Territory
                                                        </option>
                                                        <option value="Queensland">Queensland</option>
                                                        <option value="South Australia">South Australia</option>
                                                        <option value="Tasmania">Tasmania</option>
                                                        <option value="Victoria">Victoria</option>
                                                        <option value="Western Australia">Western Australia</option>
                                                    </select>
                                                </div>
                                            </div>
                                                    </div>
                                        <div class="">
                                        <div class="col-md-12">
                                             <h5 class="font-bold ">
                                                <a href="#s_collapse_2" data-toggle="collapse">Salary</a>
                                            </h5>
                                        </div>
                                        <div class="salrwapperannual">
                                            <div class="col-md-6">
                                                <small>Min</small>
                                                <select class="form-control"  name="salstart" id="salstart">
                                                    <option selected="selected" value="0" data-reactid="$0">$0</option>
                                                    <option value="30000" data-reactid="$30000">$30k</option>
                                                    <option value="40000" data-reactid="$40000">$40k</option>
                                                    <option value="50000" data-reactid="$50000">$50k</option>
                                                    <option value="60000" data-reactid="$60000">$60k</option>
                                                    <option value="70000" data-reactid="$70000">$70k</option>
                                                    <option value="80000" data-reactid="$80000">$80k</option>
                                                    <option value="100000" data-reactid="$100000">$100k</option>
                                                    <option value="120000" data-reactid="$120000">$120k</option>
                                                    <option value="150000" data-reactid="$150000">$150k</option>
                                                    <option value="175000" data-reactid="$175000">$175k</option>
                                                    <option value="200000" data-reactid="$200000">$200k</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <small>Max</small>
                                                <select class="form-control" name="salend" id="salend">
                                                    <option value="0" data-reactid="$0">$0</option>
                                                    <option value="30000" data-reactid="$30000">$30k</option>
                                                    <option value="40000" data-reactid="$40000">$40k</option>
                                                    <option value="50000" data-reactid="$50000">$50k</option>
                                                    <option value="60000" data-reactid="$60000">$60k</option>
                                                    <option value="70000" data-reactid="$70000">$70k</option>
                                                    <option value="80000" data-reactid="$80000">$80k</option>
                                                    <option value="100000" data-reactid="$100000">$100k</option>
                                                    <option value="120000" data-reactid="$120000">$120k</option>
                                                    <option value="150000" data-reactid="$150000">$150k</option>
                                                    <option value="250000" selected="selected" data-reactid="$250000">$200k+</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="hourlywapperannual hidden">
                                            <div class="col-md-6">
                                                <small>Min Hourly Rate</small>
                                                <select class="form-control" name="hrsalstart" id="salstart">
                                                    <option selected="selected" value="0" data-reactid="$0">$0</option>
                                                    <option value="15" data-reactid="15"">$15</option>
                                                    <option value="20" data-reactid="25">$20</option>
                                                    <option value="25" data-reactid="35">$25</option>
                                                    <option value="30" data-reactid="40">$30</option>
                                                    <option value="35" data-reactid="50">$35</option>
                                                    <option value="40" data-reactid="60">$40</option>
                                                    <option value="50" data-reactid="70+">$50</option>
                                                    <option value="60" data-reactid="70+">$60</option>
                                                    <option value="70" data-reactid="70+">$80</option>
                                                    <option value="100" data-reactid="70+">$100</option>
                                                    <option value="100" data-reactid="70+">$100+</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <small>Max Hourly Rate</small>
                                                <select class="form-control" name="hrsalend" id="salend">
                                                    <option value="0" data-reactid="$0">$0</option>
                                                    <option value="15" data-reactid="15"">$15</option>
                                                    <option value="20" data-reactid="25">$20</option>
                                                    <option value="25" data-reactid="35">$25</option>
                                                    <option value="30" data-reactid="40">$30</option>
                                                    <option value="35" data-reactid="50">$35</option>
                                                    <option value="40" data-reactid="60">$40</option>
                                                    <option value="50" data-reactid="70+">$50</option>
                                                    <option value="60" data-reactid="70+">$60</option>
                                                    <option value="70" data-reactid="70+">$80</option>
                                                    <option value="100" data-reactid="70+">$100</option>
                                                    <option value="100" selected="selected" data-reactid="70+">$100+</option>
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-md-12 text-center" style="padding-top: 20px;">
                                                <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="searchour" id="searchour" value="yes">
                                                Switch to Hourly Rates
                                            </label>
                                        </div>
                                            </div>
                                        <div class="col-md-12">
                                            <h5 class="font-bold" style="margin-top: 15px;">
                                                <a href="#s_collapse_2" data-toggle="collapse">Incentive Structure</a>
                                            </h5>
                                            <div class="form-group">
                                                <select id="incentivestructure" name="incentivestructure1" class="form-control">
                                                    <option value="">Any</option>
                                                    <option value="fixed">Base + Super</option>
                                                    <option value="basecommbonus">Base + Super + R&R/Bonus</option>
                                                    <option value="basecomm">Base + Super + Commissions</option>
                                                    <option value="commonly">Commissions Only</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row row-bor" style="border-right: 0px;">
                                                <div class="col-md-12">
                                                <H3 style="padding-bottom: 20px;">Job Preferences</H3>
                                                </div>
                                                <div class="col-md-12">
                                                    <h5 class="font-bold" >
                                                        <a href="#s_collapse_2" data-toggle="collapse">Shift Guide (between these hours)</a>
                                                    </h5>
                                                    <div class="form-group" style="margin-bottom: 10px;">
                                                        <select id="parentsfiltervalue" name="parentsoption" class="form-control selectpicker">
                                                            <option value=""> I'm open to any shift time</option>
                                                            <option value="1" data-content="<img src='/Standard Business Hours.png' style='max-width:25px;'>  Standard (9am-5pm Mon to Fri)">Standard (9am-5pm Mon to Fri)</option>
                                                            <option value="2" data-content="<img src='/Extended Business Hours.png' style='max-width:25px;'>  Extended (8am-8pm Mon to Fri)">Extended (8am-8pm Mon to Fri)</option>
                                                            <option value="3" data-content="<img src='/Parent Friendly.png' style='max-width:25px;'>  Parent Friendly (9am-3pm Mon to Fri)">Parent Friendly (9am-3pm Mon to Fri)</option>
                                                            <option value="4" data-content="<img src='/Afternoon Shift.png' style='max-width:25px;'>  Afternoon (12pm-12am Mon to Fri)">Afternoon (12pm-12am Mon to Fri)</option>
                                                            <option value="5" data-content="<img src='/Night Shift.png' style='max-width:25px;'>  Night Shift">Night Shift</option>
                                                            <option value="6" data-content="<img src='/Rotating Shifts no weekends.png' style='max-width:25px;'>  Rotating Shifts - no weekends">Rotating Shifts - no weekends</option>
                                                            <option value="7" data-content="<img src='/Rotating shifts - including weekend work.png' style='max-width:25px;'>  Rotating Shifts - including weekend work">Rotating Shifts - including weekend work</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <h5 class="font-bold " >
                                                        <a href="#s_collapse_2" data-toggle="collapse">Work Location</a>
                                                    </h5>
                                                    <div class="form-group">
                                                        <select id="workfromhome" name="work_from_home" class="form-control selectpicker">
                                                            <option value="">Happy to work anywhere</option>
                                                            <option value="1" data-content="<img src='/Work is only at the business address.png' style='max-width:25px;'>  At business address">At business address</option>
                                                            <option value="2" data-content="<img src='/There is potential to work from home.png' style='max-width:25px;'>  By mutual agreement"> By mutual agreement</option>
                                                            <option value="3" data-content="<img src='/Only display Work From Home Jobs.png' style='max-width:25px;'>  Work from home">Work from home</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                <div class="col-md-12">
                                                <h5 class="font-bold" >
                                                    <a href="#s_collapse_2" data-toggle="collapse">Pay Frequency</a>
                                                </h5>
                                                <div class="form-group">
                                                    <select id="pay_cycle" class="form-control selectpicker" name="pay_cycle">
                                                        <option value="">Dont care, as long as I get paid!</option>
                                                        <option value="weekly">Weekly</option>
                                                        <option value="fortnightly">Fortnightly</option>
                                                        <option value="monthly">Monthly</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <h5 class="font-bold">
                                                    <a href="" data-toggle="collapse">Job Advertiser Type</a>
                                                </h5>
                                                <select class="form-control" name="advertiser_type">
                                                    <option value="both">Both</option>
                                                    @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                                        <option value="{{$type}}" {{old('advertiser_type') == $type ? 'selected' : ''}}>{{$label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="portlet-body form">


                    <div class="form-horizontal form-bordered">

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"></label>

                                        <div class="col-sm-8">
                                           <h3>Selected Options</h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Job Details</label>

                                        <div class="col-sm-8">
                                           <strong>Job Category</strong> @foreach($categories as $category) @if ($preset->category == $category->id) = ( {{$category->title}} ) @endif @endforeach
                                           <br>
                                           <strong>Sub Category</strong> @foreach($categories as $category) @foreach($category->subcategory as $subcategory) @if ($preset->subcategory == $subcategory->id) = ( {{$subcategory->title}} ) @endif @endforeach @endforeach
                                           <br>
                                           <strong>Employment Type</strong>@if($preset->employment_type != 'null') = ( {{$preset->employment_type}} ) @else = ( nil ) @endif
                                           <br>
                                           <strong>Employment Status</strong> @if($preset->employment_term != 'null') = ( {{$preset->employment_term}} ) @else = ( nil ) @endif
                                           <br>
                                           <strong>Job Keywords</strong>@if($preset->title) = ( {{$preset->title}} ) @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Job Specifics</label>

                                        <div class="col-sm-8">
                                           <strong>Job Location</strong> @if($preset->location) ( {{$preset->location}} ) @else = (nil) @endif
                                           <br>
                                           <strong>Salary</strong> = (@if($preset->searchour)
                                                    ${{$preset->hrsalstart}} to @if($preset->hrsalend == 100) $100+ @else ${{$preset->hrsalend}} @endif Per Hour
                                                @else
                                                    ${{$preset->salstart}} to @if($preset->salend == 250000) $200k+ @else ${{$preset->salend}} @endif Annual
                                                @endif) 
                                           <br>
                                            <strong>Incentive Structure</strong>@if($preset->incentivestructure1) = ( @if($preset->incentivestructure1 == 'fixed')Base + Super @elseif($preset->incentivestructure1 == 'basecommbonus') Base + Super + R&R/Bonus @elseif($preset->incentivestructure1 == 'basecomm') Base + Super + Commissions @elseif($preset->incentivestructure1 == 'commonly') Commissions Only @endif) @else = (Any) @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label">Job Preferences</label>

                                        <div class="col-sm-8">
                                           <strong>Shift Guide</strong> @if($preset->parentsoption) = (@if($preset->parentsoption == 1) Standard Business Hours (shifts between hours of 9.00am and 5.00pm Mon to Fri)  @endif
                                            @if($preset->parentsoption == 2) Extended Business Hours (shifts between hours of 8.00am and 8.00pm Mon to Fri) @endif 
                                            @if($preset->parentsoption == 3) Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only) @endif
                                            @if($preset->parentsoption == 4) Afternoon Shift (shifts between hours of 12pm and 12am) @endif
                                            @if($preset->parentsoption == 5) Night Shift @endif
                                            @if($preset->parentsoption == 6) Rotating shifts - no weekends @endif
                                            @if($preset->parentsoption == 7) Rotating shifts - including weekend work @endif ) @else = ( I'm open to any shift time ) @endif
                                           <br>
                                           <strong>Work Location</strong>@if($preset->work_from_home) = ( @if($preset->work_from_home == 1) At business address     @endif
                                                @if($preset->work_from_home == 2) By mutual agreement @endif 
                                                @if($preset->work_from_home == 3) Work from home @endif ) @else = ( Happy to work anywhere ) @endif
                                           <br>
                                           <strong>Pay Frequency</strong>@if($preset->pay_cycle) = ( {{$preset->pay_cycle}} ) @else = ( Dont care, as long as I get paid! ) @endif
                                           <br>
                                           <strong>Job Advertiser Type</strong>@if($preset->advertiser_type) = ( {{$preset->advertiser_type}} ) @else Both @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label"></label>

                                        <div class="col-sm-8">
                                           <h3>Details</h3>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="url" class="col-sm-3 control-label">URL</label>

                                        <div class="col-sm-8">
                                            <input id="url" class="form-control" type="text" name="url"
                                                   placeholder="URL" value="{{$preset->url}}" required="required" />
                                            <p><strong>Note: </strong>The URL should be simple text such as "test" which will automatically be converted into SiteURL/callcentrejobs/test. If there is a second sub category it needs a ' - ' like callcentrejobs/manager-melbourne</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="is_active" class="col-sm-3 control-label">Active</label>
                                        <div class="col-sm-8">
                                            <input type="checkbox" id="is_active" name="is_active" checked="checked" class="form-control" value="1" @if($preset->status == 1) checked="checked @endif">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="welcome-google-adwards-tracking-code" class="col-sm-3 control-label">Google AdWords Tracking Code</label>
                                        <div class="col-sm-8">
                                            <textarea id="google-adwards-tracking-code" class="form-control" name="google-adwards-tracking-code"
                                                  placeholder="">{{$preset->google_adwards}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="facebook-pixel-tracking-code" class="col-sm-3 control-label">Facebook Events Code</label>
                                        <div class="col-sm-8">
                                            <textarea id="facebook-pixel-tracking-code" class="form-control" name="facebook-pixel-tracking-code"
                                                  placeholder="">{{$preset->facebook_pixel}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="seokeywords"
                                               class="col-sm-3 control-label">Seo Keywords</label>

                                        <div class="col-sm-8">
                                            <input id="seokeywords" type="text" class="form-control" maxlength="60" name="seokeywords" value="{{$preset->seokeywords}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="60" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="seodescription"
                                               class="col-sm-3 control-label">Seo Description</label>

                                        <div class="col-sm-8">
                                            <input id="seodescription" type="text" class="form-control" maxlength="160" name="seodescription" value="{{$preset->seodescription}}">
                                            <div class="progress" style="height: 15px;margin-bottom: 0;margin-top: 10px;">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="160" style="width: 0%;">
                                                    <span class = "current-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="page_title" class="col-sm-3 control-label">Page Title</label>

                                        <div class="col-sm-8">
                                            <input id="" class="form-control" type="text" name="page_title" value="{{$preset->pagetitle}}" placeholder="Page Title" />
                                        </div>
                                    </div>

                                    <div class="form-group progress-field">
                                        <label for="html_notes"
                                               class="col-sm-3 control-label">HTML Notes</label>

                                        <div class="col-sm-8">
                                            <textarea id="html_notes" class="form-control" name="html_notes"
                                                  placeholder="">{{$preset->html_notes}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="searchheading" class="col-sm-3 control-label">Search Heading</label>

                                        <div class="col-sm-8">
                                            <input id="searchheading" class="form-control" type="text" name="searchheading" value="{{$preset->heading}}" placeholder="Search Heading" />
                                        </div>
                                    </div>


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn purple"><i
                                                            class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
          </form>
        </div>
    </div>
@stop