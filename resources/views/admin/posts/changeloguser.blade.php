@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/natural.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true,
                order: [ 2, 'desc' ],
            });
            yadcf.init(myTable,[
                {
                    column_number: 6,
                    filter_default_label: 'Select Company',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 7,
                    filter_default_label: 'Select Level',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 2,
                    filter_default_label: 'Select Job',
                    column_data_type: 'html',
                    html_data_type: 'text',
                    filter_match_mode: 'exact',
                },
            ])
        })
    </script>
@stop

@section('content')

    <h3 class="page-title">
        {{$user->name}} Jobs Change Log
        <small>Manage Change Log</small>
    </h3>


    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users">Users</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/users/edit/{{$user->id}}">Edit - {{$user->name}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/posts/userchangelog/{{$user->id}}">Change Log - {{$user->name}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>Jobs Change Log
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>Modified By</th>
                            <th>Change Log</th>           
                            <th>Job ID</th>
                            <th>User</th>
                            <th>User Email</th>
                            <th>Job Title</th>
                            <th>{{trans('messages.company')}}</th>
                            <th>{{trans('messages.job_level')}}</th>
                            <th>Description</th>
                            <th>Short Description</th>
                            <th>Selling Points</th>
                            <th>Employment Type</th>
                            <th>Employment Status</th>
                            <th>Incentive Structure</th>
                            <th>Pay Cycle</th>
                            <th>Show Salary</th>
                            <th>Salary</th>
                            <th>Application Method</th>
                            <th>Location</th>
                            <th>State</th>
                            <th>Work Location</th>
                            <th>Shift Guide (between these hours)</th>
                            <th>Screening Questions Activation</th>
                            <th>Screening Questions</th>
                            <th>{{trans('messages.category')}}</th>
                            <th>{{trans('messages.sub_category')}}</th>
                            <th>{{trans('messages.status')}}</th>
                            <th>{{trans('messages.published_on')}}</th>
                            <th>Updated On</th>
                            <th>Expiry</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                            @if($post->old_post == 1)
                            <tr>
                                <?php
                                    $user = \App\Users::where('id',$post->author_id)->first();
                                 ?>
                                <td>
                                    @if($post->admin_change == NULL)
                                    <button type="button" class="btn btn-info">Advertiser</button>
                                    @else
                                    <button type="button" class="btn btn-warning">Admin</button>
                                    @endif
                                </td>
                                <td><button type="button" class="btn btn-danger">Old Job</button> </td>
                                <td>{{$post->post_id}}</td>
                                <td>
                                @if($user)
                                <a href="/admin/users/edit/{{$post->author_id}}">{{$user->name}}</a>
                                @else
                                nil
                                @endif
                                </td>
                                <td>
                                @if($user)
                                {{$user->email}} ({{$user->id}})
                                @else
                                nil
                                @endif
                                </td>
                                <td><a href="/admin/posts/edit/{{$post->post_id}}">{{$post->title}} ({{$post->post_id}})</a></td>
                                <td>{{$post->company}}</td>
                                <?php 
                                    $joblevel = \App\PostsPackages::where('id',$post->posts_packages)->first();
                                ?>
                                <td>
                                @if($joblevel)
                                {{$joblevel->name}}
                                @else
                                nil
                                @endif
                                </td>
                                <td>
                                    <!-- Trigger the modal with a button -->
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalh{{$post->id}}">View Description</button>
                                </td>

                                <!-- Modal -->
                                <div id="myModalh{{$post->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Job Description</h4>
                                      </div>
                                      <div class="modal-body">
                                        <p>{!!$post->description!!}</p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                                <td>{!!$post->short_description!!}</td>
                                <td>
                                    @if($post->selling1 || $post->selling2 || $post->selling3)
                                    <ul>
                                        <li>{!!$post->selling1!!}</li>
                                        <li>{!!$post->selling2!!}</li>
                                        <li>{!!$post->selling3!!}</li>
                                    </ul>
                                    @else
                                    nil
                                    @endif
                                </td>
                                <td>{{$post->employment_type}}</td>
                                <td>{{$post->employment_term}}</td>
                                <td>{{$post->incentivestructure}}</td>
                                <td>{{$post->paycycle}}</td>
                                <td>
                                    @if($post->showsalary == 1)
                                    Yes
                                    @else
                                    No
                                    @endif
                                </td>
                                <td>
                                    @if($post->salarytype == 'annual')
                                        ${{$post->salary}} Annual
                                    @else
                                        ${{$post->hrsalary}} Per Hour
                                    @endif
                                </td>
                                <td>
                                    @if($post->email_or_link)
                                        Outside ItsMyCall<br>
                                        <a target="_blank" href="{{$post->email_or_link}}">{{$post->email_or_link}}</a>
                                    @else
                                        Inside ItsMyCall
                                        <br>
                                        @if($post->person_email)
                                        Secondary Email:
                                        {{$post->person_email}}
                                        @else
                                        No Secondary Email Given!
                                        @endif
                                    @endif
                                </td>
                                <td>{{$post->joblocation}}</td>
                                <td>{{$post->state}}</td>
                                <td> 
                                @if($post->work_from_home == 1) At business address @endif
                                @if($post->work_from_home == 2) By mutual agreement @endif 
                                @if($post->work_from_home == 3) Work from home @endif
                                </td>
                                <td> 
                                @if($post->parentsoption == 1) Standard Business Hours (shifts between hours of 9.00am and 5.00pm Mon to Fri)  @endif
                                @if($post->parentsoption == 2) Extended Business Hours (shifts between hours of 8.00am and 8.00pm Mon to Fri) @endif 
                                @if($post->parentsoption == 3) Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only) @endif
                                @if($post->parentsoption == 4) Afternoon Shift (shifts between hours of 12pm and 12am) @endif
                                @if($post->parentsoption == 5) Night Shift @endif
                                @if($post->parentsoption == 6) Rotating shifts - no weekends @endif
                                @if($post->parentsoption == 7) Rotating shifts - including weekend work @endif
                                @if($post->email_or_link)
                                    <td>nil</td>
                                @else
                                    <td>@if($post->screening_activation == 1) Active @else NotActive @endif</td>
                                @endif
                                </td>
                                <?php 
                                    $questions = NULL;
                                    if($post->questions){
                                      $questions = json_decode($post->questions);  
                                    }
                                ?>
                                @if($post->email_or_link)
                                    <td>
                                      nil
                                    </td>
                                @else
                                    <td>
                                        @if($questions != NULL)
                                        <ul>
                                            @foreach($questions as $question)
                                            <li>{{$question}}</li>
                                            @endforeach
                                        </ul>
                                        @else 
                                        nil
                                        @endif
                                    </td>
                                @endif
                                <?php 
                                    $category = App\Categories::where('id',$post->category)->first();
                                ?>
                                <td>
                                @if($category)
                                {{$category->title}}
                                @else
                                nil
                                @endif
                                </td>
                                <?php 
                                    $subcategory = App\SubCategories::where('id',$post->subcategory)->first();
                                ?>
                                <td>
                                @if($subcategory)
                                {{$subcategory->title}}
                                @else
                                nil
                                @endif
                                </td>
                                <td>
                                    @if($post->status == 'closed_withdrawn') Closed - withdrawn - The job has been withdrawn and is no longer available. @endif
                                    @if($post->status == 'closed_filled') Closed - filled elsewhere The job was filled outside of ItsMyCall.   @endif
                                    @if($post->status == 'closed_response') Closed - poor response You'd like the job removed from our site as there has been a poor response/lack of quality candidates. @endif
                                    @if($post->status == 'remove') Hide - Will remove the job from the live website however it can still edited and re-listed within the 30 day period  @endif
                                    @if($post->status == 'filled') Success - Job has been offered to a candidate. This will remove the job advertisement.  @endif
                                    @if($post->status == 'active') Active - Job will be visible to applicants (this is the default status after job is advertised)  @endif
                                </td>
                                <td>
                                    <?php
                                        $postold = \App\Posts::where('id',$post->post_id)->first();
                                     ?>
                                    @if($postold)
                                        <?php
                                            $start = new \Carbon\Carbon($postold->created_at);
                                            $date_begin = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($postold->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$date_begin->format('d M Y H:i:s')}}
                                    @else
                                    nil
                                    @endif
                                </td>
                                <td>
                                    @if($post->updated_at)
                                        <?php
                                            $updated = new \Carbon\Carbon($post->updated_at);
                                            $updated_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $updated);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($post->updated_at)->format('YYYY/mm/dd')}}</span>
                                        {{$updated_at->format('d M Y H:i:s')}}
                                    @else
                                    nil
                                    @endif
                                </td>
                                <td>
                                    @if($post->expired_at)
                                        <?php
                                            $expiry = new \Carbon\Carbon($post->expired_at);
                                            $expired_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $expiry);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($post->expired_at)->format('YYYY/mm/dd')}}</span>
                                        {{$expired_at->format('d M Y')}}
                                    @else
                                    nil
                                    @endif
                                </td>
                            </tr>

                            @else
                            <tr>
                                <?php
                                    $post_old = \DB::table('changelog')->where('id',$post->id-1)->first();
                                    $user = \App\Users::where('id',$post->author_id)->first();
                                 ?>
                                <td>
                                    @if($post->admin_change == NULL)
                                    <button type="button" class="btn btn-info">Advertiser</button>
                                    @else
                                    <button type="button" class="btn btn-warning">Admin</button>
                                    @endif
                                </td>
                                <td><button type="button" class="btn btn-success">Updated Job</button> </td>
                                <td>{{$post->post_id}}</td>
                                <td>
                                @if($user)
                                <a href="/admin/users/edit/{{$post->author_id}}">{{$user->name}}</a>
                                @else
                                nil
                                @endif
                                </td>
                                <td>
                                @if($user)
                                {{$user->email}} ({{$user->id}})
                                @else
                                nil
                                @endif
                                </td>
                                @if($post->title != $post_old->title)
                                <td style="background: lightyellow;"><a href="/admin/posts/edit/{{$post->post_id}}">{{$post->title}} ({{$post->post_id}})</a></td>
                                @else
                                <td><a href="/admin/posts/edit/{{$post->post_id}}">{{$post->title}} ({{$post->post_id}})</a></td>
                                @endif
                                @if($post->company != $post_old->company)
                                <td style="background: lightyellow;">{{$post->company}}</a></td>
                                @else
                                <td>{{$post->company}}</td>
                                @endif
                                <?php 
                                    $joblevel = \App\PostsPackages::where('id',$post->posts_packages)->first();
                                ?>
                                @if($post->posts_packages != $post_old->posts_packages)
                                <td style="background: lightyellow;">
                                @if($joblevel)
                                {{$joblevel->name}}
                                @else
                                nil
                                @endif
                                </td>
                                @else
                                <td>
                                @if($joblevel)
                                {{$joblevel->name}}
                                @else
                                nil
                                @endif
                                </td>
                                @endif

                                @if($post->description != $post_old->description)
                                 <td style="background: lightyellow;"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalh{{$post->id}}">View Description</button></td>
                                 @else
                                 <td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalh{{$post->id}}">View Description</button></td>
                                 @endif


                                <!-- Modal -->
                                <div id="myModalh{{$post->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        @if($post->description != $post_old->description)
                                        <h4 class="modal-title">Job Description (Changed)</h4>
                                        @else
                                        <h4 class="modal-title">Job Description </h4>
                                        @endif
                                      </div>
                                      <div class="modal-body">
                                        <p>{!!$post->description!!}</p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                                 @if($post->short_description != $post_old->short_description)
                                 <td style="background: lightyellow;">{!!$post->short_description!!}</td>
                                 @else
                                 <td>{!!$post->short_description!!}</td>
                                 @endif

                                 @if($post->selling1 != $post_old->selling1 || $post->selling2 != $post_old->selling2 || $post->selling3 != $post_old->selling3)
                                  <td style="background: lightyellow;">
                                    @if($post->selling1 || $post->selling2 || $post->selling3)
                                    <ul>
                                        <li>{!!$post->selling1!!}</li>
                                        <li>{!!$post->selling2!!}</li>
                                        <li>{!!$post->selling3!!}</li>
                                    </ul>
                                    @else
                                    nil
                                    @endif
                                </td>
                                 @else
                                 <td>
                                    @if($post->selling1 || $post->selling2 || $post->selling3)
                                    <ul>
                                        <li>{!!$post->selling1!!}</li>
                                        <li>{!!$post->selling2!!}</li>
                                        <li>{!!$post->selling3!!}</li>
                                    </ul>
                                    @else
                                    nil
                                    @endif
                                </td>
                                 @endif

                                 @if($post->employment_type != $post_old->employment_type)
                                 <td style="background: lightyellow;">{{$post->employment_type}}</td>
                                 @else
                                 <td>{{$post->employment_type}}</td>
                                 @endif

                                 @if($post->employment_term != $post_old->employment_term)
                                 <td style="background: lightyellow;">{{$post->employment_term}}</td>
                                 @else
                                 <td>{{$post->employment_term}}</td>
                                 @endif

                                 @if($post->incentivestructure != $post_old->incentivestructure)
                                 <td style="background: lightyellow;">{{$post->incentivestructure}}</td>
                                 @else
                                 <td>{{$post->incentivestructure}}</td>
                                 @endif

                                 @if($post->paycycle != $post_old->paycycle)
                                 <td style="background: lightyellow;">{{$post->paycycle}}</td>
                                 @else
                                 <td>{{$post->paycycle}}</td>
                                 @endif

                                 @if($post->showsalary != $post_old->showsalary)
                                 <td style="background: lightyellow;">
                                    @if($post->showsalary == 1)
                                    Yes
                                    @else
                                    No
                                    @endif</td>
                                 @else
                                 <td>@if($post->showsalary == 1)
                                    Yes
                                    @else
                                    No
                                    @endif</td>
                                 @endif

                                 @if($post->salarytype != $post_old->salarytype || $post->salary != $post_old->salary)
                                 <td style="background: lightyellow;">
                                     @if($post->salarytype == 'annual')
                                        ${{$post->salary}} Annual
                                    @else
                                        ${{$post->hrsalary}} Per Hour
                                    @endif
                                 </td>
                                 @else
                                 <td>
                                     @if($post->salarytype == 'annual')
                                        ${{$post->salary}} Annual
                                    @else
                                        ${{$post->hrsalary}} Per Hour
                                    @endif
                                 </td>
                                 @endif

                                 @if($post->email_or_link != $post_old->email_or_link)
                                 <td style="background: lightyellow;">
                                    @if($post->email_or_link)
                                        Outside ItsMyCall<br>
                                        <a target="_blank" href="{{$post->email_or_link}}">{{$post->email_or_link}}</a>
                                    @else
                                        Inside ItsMyCall
                                        <br>
                                        @if($post->person_email)
                                        Secondary Email:
                                        {{$post->person_email}}
                                        @else
                                        No Secondary Email Given!
                                        @endif
                                    @endif
                                </td>
                                 @else
                                 <td>
                                    @if($post->email_or_link)
                                        Outside ItsMyCall<br>
                                        <a target="_blank" href="{{$post->email_or_link}}">{{$post->email_or_link}}</a>
                                    @else
                                        Inside ItsMyCall
                                        <br>
                                        @if($post->person_email)
                                        Secondary Email:
                                        {{$post->person_email}}
                                        @else
                                        No Secondary Email Given!
                                        @endif
                                    @endif
                                </td>
                                 @endif

                                 @if($post->joblocation != $post_old->joblocation)
                                 <td style="background: lightyellow;">{{$post->joblocation}}</td>
                                 @else
                                 <td>{{$post->joblocation}}</td>
                                 @endif

                                 @if($post->state != $post_old->state)
                                 <td style="background: lightyellow;">{{$post->state}}</td>
                                 @else
                                 <td>{{$post->state}}</td>
                                 @endif

                                 @if($post->work_from_home != $post_old->work_from_home)
                                 <td style="background: lightyellow;">
                                     @if($post->work_from_home == 1) Work is only at the business address @endif
                                    @if($post->work_from_home == 2) There is potential to Work From Home @endif 
                                    @if($post->work_from_home == 3) Only Display Work from Home jobs @endif
                                 </td>
                                 @else
                                 <td>
                                     @if($post->work_from_home == 1) Work is only at the business address @endif
                                    @if($post->work_from_home == 2) There is potential to Work From Home @endif 
                                    @if($post->work_from_home == 3) Only Display Work from Home jobs @endif
                                 </td>
                                 @endif

                                 @if($post->parentsoption != $post_old->parentsoption)
                                 <td style="background: lightyellow;">
                                     @if($post->parentsoption == 1) Standard Business Hours (shifts between hours of 9.00am and 5.00pm Mon to Fri)  @endif
                                    @if($post->parentsoption == 2) Extended Business Hours (shifts between hours of 8.00am and 8.00pm Mon to Fri) @endif 
                                    @if($post->parentsoption == 3) Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only) @endif
                                    @if($post->parentsoption == 4) Afternoon Shift (shifts between hours of 12pm and 12am) @endif
                                    @if($post->parentsoption == 5) Night Shift @endif
                                    @if($post->parentsoption == 6) Rotating shifts - no weekends @endif
                                    @if($post->parentsoption == 7) Rotating shifts - including weekend work @endif
                                 </td>
                                 @else
                                 <td>
                                    @if($post->parentsoption == 1) Standard Business Hours (shifts between hours of 9.00am and 5.00pm Mon to Fri)  @endif
                                    @if($post->parentsoption == 2) Extended Business Hours (shifts between hours of 8.00am and 8.00pm Mon to Fri) @endif 
                                    @if($post->parentsoption == 3) Parent Friendly (Shifts between hours of 9am and 3pm Mon to Fri only) @endif
                                    @if($post->parentsoption == 4) Afternoon Shift (shifts between hours of 12pm and 12am) @endif
                                    @if($post->parentsoption == 5) Night Shift @endif
                                    @if($post->parentsoption == 6) Rotating shifts - no weekends @endif
                                    @if($post->parentsoption == 7) Rotating shifts - including weekend work @endif
                                 </td>
                                 @endif

                                 @if($post->screening_activation != $post_old->screening_activation)
                                         @if($post->email_or_link)
                                            <td style="background: lightyellow;"> nil </td>
                                        @else
                                            <td style="background: lightyellow;">@if($post->screening_activation == 1) Active @else NotActive @endif</td>
                                        @endif
                                 @else
                                        @if($post->email_or_link)
                                            <td>nil </td>
                                        @else
                                            <td>@if($post->screening_activation == 1) Active @else NotActive @endif</td>
                                        @endif
                                 @endif

                                <?php 
                                    $questions = NULL;
                                    if($post->questions){
                                      $questions = json_decode($post->questions);  
                                    }
                                ?>
                                @if($post->questions != $post_old->questions)
                                 <td style="background: lightyellow;">
                                        @if($questions != NULL)
                                        <ul>
                                            @foreach($questions as $question)
                                            <li>{{$question}}</li>
                                            @endforeach
                                        </ul>
                                        @else 
                                        nil
                                        @endif
                                 </td>
                                 @else
                                 <td>
                                        @if($questions != NULL)
                                        <ul>
                                            @foreach($questions as $question)
                                            <li>{{$question}}</li>
                                            @endforeach
                                        </ul>
                                        @else 
                                        nil
                                        @endif
                                 </td>
                                 @endif
    
                                <?php 
                                    $category = App\Categories::where('id',$post->category)->first();
                                ?>
                                @if($post->category != $post_old->category)
                                 <td style="background: lightyellow;">
                                     @if($category)
                                    {{$category->title}}
                                    @else
                                    nil
                                    @endif
                                 </td>
                                 @else
                                 <td>
                                     @if($category)
                                    {{$category->title}}
                                    @else
                                    nil
                                    @endif
                                 </td>
                                 @endif
                                <?php 
                                    $subcategory = App\SubCategories::where('id',$post->subcategory)->first();
                                ?>
                                 @if($post->subcategory != $post_old->subcategory)
                                 <td style="background: lightyellow;">
                                    @if($subcategory)
                                    {{$subcategory->title}}
                                    @else
                                    nil
                                    @endif
                                 </td>
                                 @else
                                 <td>
                                    @if($subcategory)
                                    {{$subcategory->title}}
                                    @else
                                    nil
                                    @endif
                                 </td>
                                 @endif

                                 @if($post->status != $post_old->status)
                                 <td style="background: lightyellow;">
                                    @if($post->status == 'closed_withdrawn') Closed - withdrawn - The job has been withdrawn and is no longer available. @endif
                                    @if($post->status == 'closed_filled') Closed - filled elsewhere The job was filled outside of ItsMyCall.   @endif
                                    @if($post->status == 'closed_response') Closed - poor response You'd like the job removed from our site as there has been a poor response/lack of quality candidates. @endif
                                    @if($post->status == 'remove') Hide - Will remove the job from the live website however it can still edited and re-listed within the 30 day period  @endif
                                    @if($post->status == 'filled') Success - Job has been offered to a candidate. This will remove the job advertisement.  @endif
                                    @if($post->status == 'active') Active - Job will be visible to applicants (this is the default status after job is advertised)  @endif
                                 </td>
                                 @else
                                 <td>
                                    @if($post->status == 'closed_withdrawn') Closed - withdrawn - The job has been withdrawn and is no longer available. @endif
                                    @if($post->status == 'closed_filled') Closed - filled elsewhere The job was filled outside of ItsMyCall.   @endif
                                    @if($post->status == 'closed_response') Closed - poor response You'd like the job removed from our site as there has been a poor response/lack of quality candidates. @endif
                                    @if($post->status == 'remove') Hide - Will remove the job from the live website however it can still edited and re-listed within the 30 day period  @endif
                                    @if($post->status == 'filled') Success - Job has been offered to a candidate. This will remove the job advertisement.  @endif
                                    @if($post->status == 'active') Active - Job will be visible to applicants (this is the default status after job is advertised)  @endif
                                 </td>
                                 @endif

                                <td>
                                    <?php
                                        $postold = \App\Posts::where('id',$post->post_id)->first();
                                     ?>
                                    @if($postold)
                                        <?php
                                            $start = new \Carbon\Carbon($postold->created_at);
                                            $date_begin = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($postold->created_at)->format('YYYY/mm/dd')}}</span>
                                        {{$date_begin->format('d M Y H:i:s')}}
                                    @else
                                    nil
                                    @endif
                                </td>
                                <td>
                                    @if($post->updated_at)
                                        <?php
                                            $updated = new \Carbon\Carbon($post->updated_at);
                                            $updated_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $updated);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($post->updated_at)->format('YYYY/mm/dd')}}</span>
                                        {{$updated_at->format('d M Y H:i:s')}}
                                    @else
                                    nil
                                    @endif
                                </td>
                                @if($post->expired_at != $post_old->expired_at)
                                 <td style="background: lightyellow;">
                                     @if($post->expired_at)
                                        <?php
                                            $expiry = new \Carbon\Carbon($post->expired_at);
                                            $expired_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $expiry);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($post->expired_at)->format('YYYY/mm/dd')}}</span>
                                        {{$expired_at->format('d M Y')}}
                                    @else
                                    nil
                                    @endif
                                 </td>
                                 @else
                                 <td>
                                     @if($post->expired_at)
                                        <?php
                                            $expiry = new \Carbon\Carbon($post->expired_at);
                                            $expired_at = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $expiry);
                                        ?>
                                        <span class="hidden">{{\Carbon\Carbon::parse($post->expired_at)->format('YYYY/mm/dd')}}</span>
                                        {{$expired_at->format('d M Y')}}
                                    @else
                                    nil
                                    @endif
                                 </td>
                                 @endif
                            </tr>
                            @endif

                            @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_post')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_post_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
@stop