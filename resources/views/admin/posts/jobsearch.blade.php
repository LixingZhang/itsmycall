@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/natural.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
        })
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Job Search Presets
        <small>All Job Search Presets</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/posts/jobsearch">Job Search Presets</a>
            </li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Job Search Presets
                    </div>
                    <div class="actions">
                        <a href="/admin/posts/jobsearch/create" class="btn red">
                            <i class="fa fa-plus"></i> Create Job Search Preset </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>URL</th>
                                <th>Heading</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($presets as $preset)
                                <tr>
                                    <td>{{$preset->id}}</td>
                                    <td><a target="_blank" href="/callcentrejobs/{{$preset->url}}">{{$preset->url}}</a></td>
                                    <td>{{$preset->heading}}</td>
                                    <td>@if($preset->status) Active @else Inactive @endif</td>
                                    <td><a href="/admin/posts/jobsearch/edit/{{$preset->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                    </td>
                                    <td><a href="/admin/posts/jobsearch/delete/{{$preset->id}}" class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
@stop