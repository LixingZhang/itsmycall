@extends('admin.layouts.master')

@section('extra_js')
    <script type="text/javascript" src="/assets/plugins/natural.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 2,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 1,
                    filter_default_label: 'Select User',
                    filter_match_mode: 'exact',

                },
            ])
        })
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Job Poster Questions
        <small>All Job Poster Questions</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/posts">{{trans('messages.posts')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="/admin/posts/questions">Job Poster Questions</a>
            </li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Job Poster Questions
                    </div>
                    <div class="actions">
                        <a href="/admin/posts/questions/create" class="btn red">
                            <i class="fa fa-plus"></i> Create Job Poster Question </a>
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Question</th>
                            <th>Order</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($questions as $question)
                            <tr>
                                <td>{{$question->id}}</td>
                                <?php
                                    $user = \App\Users::find($question->user_id);
                                ?>
                                <td> @if($user) {{$user->name}} @else nil @endif </td>
                                <td> @if($user) {{$user->email}} @else nil @endif </td>
                                <td> {{$question->question}} </td>
                                <td> @if($user && $user->id == 1) {{$question->order}} @else nil @endif</td>
                                <td><a href="/admin/posts/questions/edit/{{$question->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                </td>
                                <td><a data-href="/admin/posts/questions/delete/{{$question->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    Delete Job Poster Question
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> Are you sure to Delete Job Poster Question?
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
@stop