@extends('admin.layouts.master')

@section('extra_js')
        <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 1,
                    filter_default_label: 'Select User',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 2,
                    filter_default_label: 'Select Email',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 6,
                    filter_default_label: 'Select Company',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 10,
                    filter_default_label: 'Select Level',
                    filter_match_mode: 'exact',

                },
                {
                    column_number: 20,
                    filter_default_label: 'Select Status',
                    filter_match_mode: 'exact',
                },
                {
                    column_number: 22,
                    filter_default_label: 'Select Validity',
                    column_data_type: 'html',
                    html_data_type: 'text',
                    filter_match_mode: 'exact',
                },
                {
                    column_number: 23,
                    filter_default_label: 'Select User Deleted',
                    filter_match_mode: 'exact'
                },
            ])
        })
    </script>
@stop

@section('content')

    <h3 class="page-title">
        Unpaid Job Posts
        <small>Jobs that are unpaid due to invoicing</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
                <i class="fa fa-angle-right"></i>
            </li>

            <li>
                <a href="/admin/unpaid">Unpaid Job Posts</a>
            </li>

        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-meadow">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-puzzle"></i>All Unpaid Job Posts
                    </div>
                </div>

                <div class="portlet-body">

                    @include('admin.layouts.notify')
<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                        <thead>
                        <tr>
                            <th>{{trans('messages.job_id')}}</th>
                            <th>{{trans('messages.user')}}</th>
                            <th>{{trans('messages.user_email')}}</th>
                            <th>{{trans('messages.featured_image')}}</th>
                            <th>{{trans('messages.title')}}</th>
                            <th>{{trans('messages.job_image')}}</th>
                            <th>{{trans('messages.company')}}</th>
                            <th>Work Location</th>
                            <th>Shift Guide</th>
                            <th>Screening Questions</th>
                            <th>{{trans('messages.job_level')}}</th>
                            <?php
                            $upgrades = \App\Upgrades::where('status',1)->get();
                            ?>
                            @foreach($upgrades as $upgrade)
                            <th>{{$upgrade->name}} Boost</th>
                            @endforeach
                            <th>{{trans('messages.location')}}</th>
                            <th>{{trans('messages.category')}}</th>
                            <th>{{trans('messages.sub_category')}}</th>
                            <th>{{trans('messages.applications')}}</th>
                            <th>{{trans('messages.views')}}</th>
                            <th>{{trans('messages.published_on')}}</th>
                            <th>{{trans('messages.status')}}</th>
                            <th>{{trans('messages.expired_on')}}</th>
                            <th>{{trans('messages.validity')}}</th>
                            <th>{{trans('messages.user_deleted')}}</th>
                            <th>{{trans('messages.user_deleted_date')}}</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($posts)) { ?>
                        @foreach($posts as $post)
                            <tr>
                                <td> {{$post->id}} </td>
                                <td> <a href="/admin/users/{{$post->user->id}}" >{{$post->user->name}}</a> </td>
                                <td>{{$post->user->email}}</td>
                                <td>

                                    @if($post->render_type == \App\Posts::RENDER_TYPE_TEXT)
                                        {{trans('messages.text_post')}}
                                    @else
                                        <a target="_blank" href="{{$post->featured_image}}"><img
                                                    src="{{$post->featured_image}}" style="width:100px;"/></a>
                                    @endif

                                </td>
                                <td><a href="/{{$post->slug}}" target="_blank">{{$post->title}}</a></td>
                                <td><img src="{{$post->job_image}}" style="width:100px;"/></td>
                                <td>{{$post->company}}</td>
                                <td>
                                    <?php
                                        $key1 = $post->work_from_home;
                                     ?>
                                     <ul>
                                        @if($key1 == 1)
                                        <li> At business address</li>
                                        @endif
                                        @if($key1 == 2)
                                        <li> By mutual agreement</li>
                                        @endif
                                        @if($key1 == 3) 
                                        <li> Work from home </li>
                                        @endif
                                    </ul> 
                                </td>
                                <td>
                                    <?php
                                        $key = $post->parentsoption;
                                     ?>
                                    <ul>
                                        @if($key == 1)
                                        <li> Standard (9am-5pm Mon to Fri)</li>
                                        @endif
                                        @if($key == 2)
                                        <li>Extended (8am-8pm Mon to Fri)</li>
                                        @endif
                                        @if($key == 3) 
                                        <li>Parent Friendly (9am-3pm Mon to Fri) </li>
                                        @endif
                                        @if($key == 4) 
                                        <li>Afternoon (12pm-12am Mon to Fri)</li>
                                        @endif
                                        @if($key == 5) 
                                        <li>Night Shift </li>
                                        @endif
                                        @if($key == 6) 
                                        <li>Rotating shifts - no weekends</li>
                                        @endif
                                        @if($key == 7) 
                                        <li>Rotating shifts - including weekend work</li>
                                        @endif
                                    </ul> 
                                </td>
                                <?php 
                                    $questions = NULL;
                                    if($post->questions){
                                      $questions = json_decode($post->questions);  
                                    }
                                ?>
                                <td>
                                    @if($questions != NULL)
                                    <ul>
                                        @foreach($questions as $question)
                                        <li>{{$question}}</li>
                                        @endforeach
                                    </ul>
                                    @else 
                                    nil
                                    @endif
                                </td>
                                <?php 
                                    $joblevel = \App\PostsPackages::where('id',$post->posts_packages)->first();
                                ?>
                                <td>
                                @if($joblevel)
                                {{$joblevel->name}}
                                @else
                                nil
                                @endif
                                </td>
                                <?php
                                $upgrades = \App\Upgrades::where('status',1)->get();
                                ?>
                                @foreach($upgrades as $upgrade)
                                <td>
                                    <?php
                                        $upgradecount = \App\PostUpgrades::where('post_id',$post->id)->where('upgrade_id',$upgrade->id)->first();
                                     ?>
                                     @if($upgradecount)
                                     {{$upgradecount->count}}
                                     @else
                                     nil
                                     @endif
                                </td>
                                @endforeach
                                <td>{{$post->joblocation}}</td>
                                <td> {{isset($post->category)?$post->category->title:'NO CATEGORY'}} </td>
                                <td> {{isset($post->sub_category)?$post->sub_category->title:'NO CATEGORY'}} </td>

                                <td> <?php if ($post->email_or_link != '') {
                                        echo $post->out_count . " (Clicks Out)";
                                    } else {
                                        echo $post->apps_count;
                                    } ?>
                                </td>
                                <td> {{$post->views}} </td>
                                <td> <span class="hidden">{{\Carbon\Carbon::parse($post->created_at)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($post->created_at)->format('d M Y H:i:s')}} </td>

                                <td>
                                    @if($post->status == 'closed_withdrawn') Closed - withdrawn - The job has been withdrawn and is no longer available. @endif
                                    @if($post->status == 'closed_filled') Closed - filled elsewhere The job was filled outside of ItsMyCall.   @endif
                                    @if($post->status == 'closed_response') Closed - poor response You'd like the job removed from our site as there has been a poor response/lack of quality candidates. @endif
                                    @if($post->status == 'remove') Hide - Will remove the job from the live website however it can still edited and re-listed within the 30 day period  @endif
                                    @if($post->status == 'filled') Success - Job has been offered to a candidate. This will remove the job advertisement.  @endif
                                    @if($post->status == 'active') Active - Job will be visible to applicants (this is the default status after job is advertised)  @endif
                                </td>

                                <td><span class="hidden">{{\Carbon\Carbon::parse($post->expired_at)->format('YYYY/mm/dd')}}</span> {{\Carbon\Carbon::parse($post->expired_at)->format('d M Y H:i:s')}}</td>

                                @if($post->expired_at >= \Carbon\Carbon::now())
                                    <td>
                                        <label class="label label-warning label-sm">Not Published</label>
                                    </td>
                                @else
                                    <td><label class="label label-danger label-sm">{{trans('messages.expired')}}</label>
                                    </td>
                                @endif
                                <td>{{$post->user->deleted ? 'Yes' : 'No'}}</td>
                                <td>{{$post->user->deleted_at ? \Carbon\Carbon::parse($post->user->deleted_at)->format('d M Y H:i:s') : ''}}</td>
                                <td><a href="/admin/posts/edit/{{$post->id}}"
                                       class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                                </td>
                                <td><a data-href="/admin/posts/delete/{{$post->id}}" data-toggle="modal"
                                       data-target="#confirm-delete"
                                       class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                            </tr>
                        @endforeach
                        <?php } ?>
                        </tbody>
                    </table>
</div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    {{trans('messages.delete_post')}}
                                </div>
                                <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                    <h4>
                                        <i class="fa fa-exclamation-triangle"></i> {{trans('messages.delete_post_desc')}}
                                    </h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">{{trans('messages.cancel')}}</button>
                                    <a class="btn btn-danger btn-ok">{{trans('messages.delete')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
@stop