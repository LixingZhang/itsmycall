@extends('admin.layouts.master')


@section('content')

    <h3 class="page-title">
        Edit Job Poster Question
        <small> Edit Job Poster Question</small>
    </h3>

    <div class="page-bar">
        <ul class="page-breadcrumb">

            <li>
                <a href="/admin">{{trans('messages.home')}}</a>
            </li>

        </ul>
    </div>


    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-frame"></i>Edit Job Poster Question
                    </div>
                    <div class="tools">
                        <a href="javascript:" class="collapse">
                        </a>
                    </div>
                </div>

                <div class="portlet-body form">


                    <form action="/admin/posts/questions/edit/{{$question->id}}" method="post"
                          class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-4">
                                @include('admin.layouts.notify')
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                        <div class="form-group">
                            <label for="Question" class="col-sm-3 control-label">Question</label>

                            <div class="col-sm-8">
                                <input id="count" class="form-control" type="text" name="question"
                                       placeholder="Question..." value="{{$question->question}}"/>
                            </div>
                        </div>
                        @if($question->user_id == 1)
                        <div class="form-group">
                            <label for="is_purchased" class="col-sm-3 control-label">Order</label>

                            <div class="col-sm-8">
                                <input id="order" class="form-control" type="number" name="order" value="{{$question->order}}" placeholder="Order..." />
                            </div>
                        </div>
                        @endif


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i> {{trans('messages.save')}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop