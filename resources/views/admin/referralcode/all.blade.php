@extends('admin.layouts.master')


@section('extra_js')
 <script type="text/javascript">
        $(document).ready(function () {
            var myTable = $('#datatable_advanced').DataTable({
                responsive: true
            });
            yadcf.init(myTable,[
                {
                    column_number: 9,
                    filter_default_label: 'Select Status',
                    filter_match_mode: 'exact'

                },
                {
                    column_number: 4,
                    filter_default_label: 'Select Yes/No',
                    filter_match_mode: 'exact'

                }
            ]);
        });
    </script>
@stop

@section('content')

<h3 class="page-title">
    Registration Code
    <small>Manage Registration Code</small>
</h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/referralcode">All Registration Code</a>
        </li>

    </ul>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-puzzle"></i>All Registration Code
                </div>
                <div class="actions">
                    <a href="/admin/referralcode/create" class="btn red">
                        <i class="fa fa-plus"></i> Create New Registration Code </a>
                </div>
            </div>

            <div class="portlet-body">

                @include('admin.layouts.notify')
<div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="datatable_advanced">
                    <thead>
                        <tr>
                            <th>{{trans('messages.id')}}</th>
                            <th>Registration Code</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>For Auscontact Members</th>
                            <th>Registration Code Start Date</th>
                            <th>Registration Code Expiry Date</th>
                            <th>Discount Pricing Expiry Date</th>
                            <th>Expiry</th>
                            <th>Status</th>
                            <th>Exclusive</th>
                            <th>Welcome Bonus</th>
                            <th>Welcome Bonus Expiry</th>
                            <th>Registration Code Expiry Alert</th>
                            <th>Screening Question Pricing</th>
                            @foreach($boosts as $boost)
                            <th>{{$boost->name}}</th>
                            @endforeach
                            @foreach($joblevels as $joblevel)
                            <th>{{$joblevel->name}}</th>
                            @endforeach
                            <th>Value Packs Discount Rate</th>
                            @foreach($packages as $package)
                            <th>{{$package->package_name}}</th>
                            @endforeach
                            <th>Registration Code Usage</th> 
                            <th>Users</th> 
                            <th>Job Posts</th>
                            <th>{{trans('messages.edit')}}</th>
                            <th>{{trans('messages.delete')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ads as $ad)
                        <tr>
                            <td> {{$ad->id}} </td>
                            <td> {{$ad->referral_code}} </td>
                            <td> {{$ad->name}} </td>
                            <td> {!!$ad->description!!} </td>
                            <td> @if(strpos($ad->referral_code, 'AUS_') !== false) Yes @else No @endif</td>
                            <td> 
                            <?php
                            $start = new \Carbon\Carbon($ad->date_begin);
                            $start1 = new \Carbon\Carbon($ad->campaign_end);
                            $end = new \Carbon\Carbon($ad->date_end);
                            $date_begin = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start);
                            $campaign_end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $start1);
                            $date_end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $end);
                            $expires = new \Carbon\Carbon($ad->date_end);
                            $now = \Carbon\Carbon::now();
                            $difference = ($expires->diffForHumans());
                            ?>
                            <span class="hidden">{{\Carbon\Carbon::parse($ad->date_begin)->format('YYYY/mm/dd')}}</span>
                            {{$date_begin->format('d M Y')}} </td>
                            <td>@if($ad->campaign_end) <span class="hidden">{{\Carbon\Carbon::parse($ad->campaign_end)->format('YYYY/mm/dd')}}</span> {{$campaign_end->format('d M Y')}} @else nil @endif </td>
                            <td><span class="hidden">{{\Carbon\Carbon::parse($ad->date_end)->format('YYYY/mm/dd')}}</span> {{$date_end->format('d M Y')}} </td>
                            <td>{{$difference}}</td>
                            <td>
                                <?php 
                                    $date = \Carbon\Carbon::now()->toDateString();
                                ?>
                                @if($ad->date_end >= $date)
                                Active
                                @else
                                Expired
                                @endif
                            </td>
                            <td> {{$ad->exclusive}} </td>
                            <td> @if($ad->credit_amount > 0) ${{$ad->credit_amount}} @else N/A @endif </td>
                            <td> @if($ad->credit_amount > 0 && $ad->bonus_end) 
                                    <?php
                                    $bonus_end = new \Carbon\Carbon($ad->bonus_end);
                                    $bonusdate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $bonus_end);
                                    ?>
                                <span class="hidden">{{\Carbon\Carbon::parse($ad->bonus_end)->format('YYYY/mm/dd')}}</span>
                                 {{$bonusdate->format('d M Y')}}
                                 @else
                                 nil 
                                 @endif</td>

                            <td>@if($ad->referral_alert == 1) Yes @else No @endif </td>
                            @if($ad->hello3($ad->id))
                            @foreach($ad->hello3($ad->id) as $question)
                            <td>${{$question->price}}</td>
                            @endforeach
                            @else
                            <td>nil</td>
                            @endif
                            @if($ad->hello($ad->id))
                            @foreach($ad->hello($ad->id) as $upgrade)
                            <td>${{$upgrade->price}}</td>
                            @endforeach
                            @else
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            @endif
                            @if($ad->hello1($ad->id))
                            @foreach($ad->hello1($ad->id) as $package)
                            <td>${{$package->price}}</td>
                            @endforeach
                            @else
                            <td>nil</td>
                            <td>nil</td>
                            <td>nil</td>
                            @endif
                            <td> @if($ad->discount_rate) {{$ad->discount_rate}}% @else N/A @endif</td>
                            @foreach($packages as $package)
                            <?php
                            $package_disc = $package->price*($ad->discount_rate/100);
                            ?>
                            @if($package_disc > 0)
                            <td>${{$package->price-$package_disc}}</td>
                            @else
                            <td>${{$package->price}}</td>
                            @endif
                            @endforeach
                            <td>
                                <?php
                                    $all = \App\Users::where('ref_id',$ad->id)->get();
                                    $count = count($all);
                                 ?>
                                 {{$count}}
                            </td>
                            <td><a href="/admin/users/withreferral/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">View Users</a>
                                </td>
                            <td><a href="/admin/posts/withreferral/{{$ad->id}}"
                                       class="btn btn-warning btn-sm">View Posts</a>
                                </td>
                            <td><a href="/admin/referralcode/edit/{{$ad->id}}"
                                   class="btn btn-warning btn-sm">{{trans('messages.edit')}}</a>
                            </td>
                            <td><a data-href="/admin/referralcode/delete/{{$ad->id}}" data-toggle="modal"
                                   data-target="#confirm-delete"
                                   class="btn btn-danger btn-sm">{{trans('messages.delete')}}</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
</div>
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                {{trans('messages.delete_ad')}}
                            </div>
                            <div class="modal-body" style="background-color:#FFB848; color:#ffffff;">
                                <h4><i class="fa fa-exclamation-triangle"></i>{{trans('messages.delete_ad_desc')}}
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal"> {{trans('messages.cancel')}} </button>
                                <a class="btn btn-danger btn-ok"> {{trans('messages.delete')}} </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop