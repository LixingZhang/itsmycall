@extends('admin.layouts.master')

@section('extra_css')
<link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/css/bootstrap-datetimepicker.css" />
@stop

@section('extra_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/en-au.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.45/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/plugins/redactor/plugins/imagemanager.js"></script>
<script src="/assets/plugins/redactor/redactor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.description').redactor({
                replaceDivs: false,
                convertDivs: false
            });
        });
        </script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker({format: 'Y-MM-DD'});
        $('#datetimepicker2').datetimepicker({format: 'Y-MM-DD'});
        $('#datetimepicker3').datetimepicker({format: 'Y-MM-DD'});
        $('#description').redactor({
            imageUpload: '/admin/redactor',
            imageManagerJson: '/admin/redactor/images.json',
            plugins: ['imagemanager'],
            replaceDivs: false,
            convertDivs: false,
            uploadImageFields: {
                _token: "{{csrf_token()}}"
            }
        });
    });
</script>
@stop

@section('content')

<h3 class="page-title">
    Create New Registration Code
</h3>

<div class="page-bar">
    <ul class="page-breadcrumb">

        <li>
            <a href="/admin">{{trans('messages.home')}}</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/referralcode">Registration Code</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/admin/referralcode/create/">Create Registration Code</a>
        </li>

    </ul>
</div>


<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-frame"></i>Create New Registration Code
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>

            <div class="portlet-body form">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
                <form action="/admin/referralcode/create" id="form-username" method="post"
                      class="form-horizontal form-bordered">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Registration Code Name</label>

                        <div class="col-sm-8">
                            <input type="text" id="package_name" name="name" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label"> For Auscontact Registration Code</label>

                        <div class="col-sm-8">
                            <input type="checkbox" class="form-control" id="for_auscontact">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Registration Code</label>

                        <div class="col-sm-8">
                            <input type="text" id="referral_code" name="referral_code" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Registration Code Description</label>

                        <div class="col-sm-8">
                            <textarea id="description" name="description" class="form-control" value=""></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Registration Code Start Date</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="date_begin" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Registration Code Expiry Date</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' name="campaign_end" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Discount Pricing Expiry Date</label>

                        <div class="col-sm-8">
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' name="date_end" class="form-control" value=""/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Exclusive</label>
                        <div class="col-sm-8">
                            <select name="exclusive" class="form-control">
                                <option value="yes">Yes <small>(NO Coupon Codes will apply)</small></option>
                                <option value="no">No <small>(ANY Coupon Code will apply)</small></option>
                                 <option value="partial">Partial <small>(Only selected Coupon Code will apply)</small></option>
                            </select>
                        </div>
                    </div>
                    <h2 style="margin-left: 25px;">Welcome Bonus:</h2>
                    <hr>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Credit Amount</label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="credit_amount" name="credit_amount" class="form-control" value="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Allow Registration Code <br> (Registration Code expiry alerts will appear)</label>
                        <div class="col-sm-8">
                            <input type="checkbox" name="referral_alert" class="form-control" value="1">
                        </div>
                    </div>
                    <h2 style="margin-left: 25px;">Screening Questions:</h2>
                    <hr>
                    @foreach($questions as $question)
                    <input type="hidden" value="{{$question->id}}" name="questionid">
                    <input type="hidden" value="{{$question->title}}" name="questiontitle">
                    <input type="hidden" value="{{$question->price}}" name="originalpricequestion">
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Price</label>
                        <div class="col-sm-8">
                            <input type="number" step="any" name="questionprice" class="form-control" value="{{$question->price}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="description" name="questiondescription" class="form-control">{!!$question->description!!}</textarea>
                        </div>
                    </div>
                    @endforeach
                    <h2 style="margin-left: 25px;">Value Packs:</h2>
                    <hr>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Discount Rate<br><small> (Applies to Value Packs)</small></label>
                        <div class="col-sm-8">
                            <input type="number" step="any"  id="discount_rate" name="discount_rate" class="form-control" value="0">
                        </div>
                    </div>
                    <h2 style="margin-left: 25px;">Boosts:</h2>
                    <hr>
                    @foreach($upgrades as $upgrade)
                    <h4 style="margin-left: 40px; margin-top: 43px;">{{$upgrade->name}}:</h4>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <input type="hidden" name="upgradeid[]" value="{{$upgrade->id}}">
                            <input type="hidden" name="upgradetype[]" value="{{$upgrade->type}}">
                            <input type="hidden" name="upgradename[]" value="{{$upgrade->name}}">
                            <input type="hidden" name="upgradestatus[]" value="{{$upgrade->status}}">
                            <input type="hidden" name="boost_number[]" value="{{$upgrade->boost_number}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Price</label>
                        <div class="col-sm-8">
                            <input type="number" step="any" name="upgradeprice[]" class="form-control" value="{{$upgrade->price}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="description" name="upgradedescription[]" class="form-control">{!!$upgrade->description!!}</textarea>
                        </div>
                    </div>
                    @endforeach
                    <h2 style="margin-left: 25px;">Job Levels:</h2><hr>
                    @foreach($packages as $package)
                    <h4 style="margin-left: 40px; margin-top: 43px;">{{$package->name}}:</h4>
                    <div class="form-group">
                        <div class="col-sm-8">
                        <input type="hidden" name="packageid[]" value="{{$package->id}}">
                        <input type="hidden" name="packagename[]" value="{{$package->name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Price</label>
                        <div class="col-sm-8">
                            <input type="number" step="any" name="packageprice[]" class="form-control" value="{{$package->price}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-8">
                            <input name="packagetitle[]" class="form-control" value="{{$package->title}}"></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="code" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="description" name="packagedescription[]" class="form-control">{!!$package->description!!}</textarea>
                        </div>
                    </div>
                    @endforeach
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn purple"><i
                                        class="fa fa-check"></i> {{trans('messages.save')}} </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<script type="text/javascript">
    $("#for_auscontact").change(function(){
        if ($('#for_auscontact').is(':checked')) {
            $('#referral_code').val('AUS_');
        }else{
            $('#referral_code').val('');
        }
    });
</script>
@stop