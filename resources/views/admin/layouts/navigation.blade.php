<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="sidebar-toggler-wrapper">
                <div class="sidebar-toggler"></div>
            </li>

            <li class="start {{sizeof(Request::segments())==1?'active':''}} ">
                <a href="/admin">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('messages.dashboard')}}</span>
                </a>
            </li>

            <li class="start {{(isset(Request::segments()[1]) && Request::segments()[1]=="reports")?'active':''}}">
                <a href="/admin/reports">
                    <i class="icon-settings"></i>
                    <span class="title">Reports</span>
                </a>
            </li>

            @if(\App\Users::hasPermission("categories.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="categories")?'active':''}}">
                    <a href="/admin/categories">
                        <i class="icon-puzzle"></i>
                        <span class="title">{{trans('messages.categories')}}</span>
                    </a>
                </li>
            @endif
            
            @if(\App\Users::hasPermission("settings.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="coupons")?'active':''}}">
                    <a href="/admin/coupons">
                        <i class="icon-settings"></i>
                        <span class="title">Coupons</span>
                    </a>
                </li>
            @endif
            
            @if(\App\Users::hasPermission("settings.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="referralcode")?'active':''}}">
                    <a href="/admin/referralcode">
                        <i class="icon-settings"></i>
                        <span class="title">Registration Codes</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("sub_categories.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="sub_categories")?'active':''}}">
                    <a href="/admin/sub_categories">
                        <i class="icon-note"></i>
                        <span class="title">{{trans('messages.sub_categories')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("sources.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="sources")?'active':''}}">
                    <a href="/admin/sources">
                        <i class="icon-feed"></i>
                        <span class="title">{{trans('messages.sources')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(count(Request::segments()) === 2 && Request::segments()[1]=="posts" ) ? 'active':''}}">
                    <a href="/admin/posts">
                        <i class="icon-notebook"></i>
                        <span class="title">All Job Posts</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2]=="jobsearch") ? 'active':''}}">
                    <a href="/admin/posts/jobsearch">
                        <i class="icon-notebook"></i>
                        <span class="title">Job Search Presets</span>
                    </a>
                </li>
            @endif
            
            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2]=="changelog") ? 'active':''}}">
                    <a href="/admin/posts/changelog">
                        <i class="icon-notebook"></i>
                        <span class="title">Change Log</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2]=="unpaid") ? 'active':''}}">
                    <a href="/admin/posts/unpaid">
                        <i class="icon-notebook"></i>
                        <span class="title">UnPaid Job Posts</span>
                    </a>
                </li>
            @endif
            

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="job_alerts") ? 'active':''}}">
                    <a href="/admin/job_alerts">
                        <i class="icon-notebook"></i>
                        <span class="title">Job Alerts</span>
                    </a>
                </li>
            @endif
            
            @if(\App\Users::hasPermission("statistics.view"))
                <li class="{{(count(Request::segments()) === 2 && Request::segments()[1] == "transactions") ? 'active' : ''}}">
                    <a href="/admin/transactions">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Transactions</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("statistics.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "auscontact") ? 'active' : ''}}">
                    <a href="/admin/transactions/auscontact">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Auscontact Transactions</span>
                    </a>
                </li>
            @endif



            @if(\App\Users::hasPermission("users.view"))
                <li class="{{(count(Request::segments()) === 2 && Request::segments()[1] == "users") ? 'active' : ''}}">
                    <a href="/admin/users">
                        <i class="icon-users"></i>
                        <span class="title">{{trans('messages.users')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("users.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "connect") ? 'active' : ''}}">
                    <a href="/admin/users/connect">
                        <i class="icon-users"></i>
                        <span class="title">{{trans('messages.inbound')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("users.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "inboundimages") ? 'active' : ''}}">
                    <a href="/admin/users/inboundimages">
                        <i class="icon-users"></i>
                        <span class="title">Connect Images</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("users.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "applicants") ? 'active' : ''}}">
                    <a href="/admin/users/applicants">
                        <i class="icon-users"></i>
                        <span class="title">{{trans('messages.applicants')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("users.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "applicants_status") ? 'active' : ''}}">
                    <a href="/admin/users/applicants_status">
                        <i class="icon-users"></i>
                        <span class="title">{{trans('messages.applicants_status')}}</span>
                    </a>
                </li>
            @endif

             @if(\App\Users::hasPermission("users.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "applied_status") ? 'active' : ''}}">
                    <a href="/admin/users/applied_status">
                        <i class="icon-users"></i>
                        <span class="title">Applied Job Status</span>
                    </a>
                </li>
            @endif
            
            
            @if(\App\Users::hasPermission("packages_stats.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="boost_manage")?'active':''}}">
                    <a href="/admin/boost_manage/all">
                        <i class="icon-share-alt"></i>
                        <span class="title">Purchased Boosts</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("packages_stats.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "packageinfo") ? 'active' : ''}}">
                    <a href="/admin/packages_stats/packageinfo">
                        <i class="icon-share-alt"></i>
                        <span class="title">Value Pack Purchases</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("ad_sections.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="ads")?'active':''}}">
                    <a href="/admin/ads">
                        <i class="icon-frame"></i>
                        <span class="title">{{trans('messages.ads_section')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("statistics.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="statistics")?'active':''}}">
                    <a href="/admin/statistics">
                        <i class="icon-bar-chart"></i>
                        <span class="title">{{trans('messages.statistics')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2] == "errorsall") ? 'active' : ''}}">
                    <a href="/admin/posts/errorsall">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Errors</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("crons.all"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="crons")?'active':''}}">
                    <a href="/admin/crons">
                        <i class="fa fa-cubes"></i>
                        <span class="title">{{trans('messages.cron_logs')}}</span>
                    </a>
                </li>
            @endif
			
			@if(\App\Users::hasPermission("crons.all"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="packages")?'active':''}}">
                    <a href="/admin/packages">
                        <i class="fa fa-archive"></i>
                        <span class="title">Value Packs</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="email")?'active':''}}">
                    <a href="/admin/email/all">
                        <i class="fa fa-archive"></i>
                        <span class="title">Email Templates</span>
                    </a>
                </li>
            @endif
			
			@if(\App\Users::hasPermission("crons.all"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="upgrades")?'active':''}}">
                    <a href="/admin/upgrades">
                        <i class="fa fa-archive"></i>
                        <span class="title">Boosts</span>
                    </a>
                </li>
            @endif
			
			@if(\App\Users::hasPermission("crons.all"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="post_types")?'active':''}}">
                    <a href="/admin/post_types">
                        <i class="fa fa-archive"></i>
                        <span class="title">Job Levels</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("posts.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2]=="questions_pricing") ? 'active':''}}">
                    <a href="/admin/posts/questions_pricing/all">
                        <i class="icon-notebook"></i>
                        <span class="title">Screening Questions Pricing</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("roles.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="roles")?'active':''}}">
                    <a href="/admin/roles">
                        <i class="icon-lock"></i>
                        <span class="title">{{trans('messages.user_roles')}}</span>
                    </a>
                </li>
            @endif
			
			@if(\App\Users::hasPermission("roles.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="companies")?'active':''}}">
                    <a href="/admin/companies">
                        <i class="icon-lock"></i>
                        <span class="title">{{trans('messages.user_companies')}}</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("settings.view"))
                <li class="{{(isset(Request::segments()[1]) && !isset(Request::segments()[2]) && Request::segments()[1]=="pages")?'active':''}}">
                    <a href="/admin/pages">
                        <i class="icon-settings"></i>
                        <span class="title">Campaign Pages</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("settings.view"))
                <li class="{{(isset(Request::segments()[2]) && Request::segments()[2]=="blogs") ? 'active':''}}">
                    <a href="/admin/pages/blogs">
                        <i class="icon-settings"></i>
                        <span class="title">Blogs</span>
                    </a>
                </li>
            @endif

            @if(\App\Users::hasPermission("settings.view"))
                <li class="{{(isset(Request::segments()[1]) && Request::segments()[1]=="settings")?'active':''}}">
                    <a href="/admin/settings">
                        <i class="icon-settings"></i>
                        <span class="title">{{trans('messages.settings')}}</span>
                    </a>
                </li>
            @endif

            <li>
                <a href="/logout">
                    <i class="icon-logout"></i>
                    <span class="title">{{trans('messages.logout')}}</span>
                </a>
            </li>

        </ul>
    </div>
</div>