<div class="page-footer">
    <div class="page-footer-inner">
        2017 &copy; <a href="https://itsmycall.com.au" title="" target="_blank">ItsMyCall - Australia's own call centre jobs website</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
