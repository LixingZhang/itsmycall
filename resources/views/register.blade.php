@extends('layouts.master')


@section('content')


<div class="block-section "   style="padding-bottom:130px">
    <div class="container">
        <div class="row">
            <div style="margin: 15px;">
                @include('admin.layouts.notify')
            </div>
            <a href="/register-user"> <div class="col-md-6 col-xs-12 text-center">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important;min-height:339px">
                            {!!$settings_general->registration_text_user!!}
                        </div>
                    </div>
                </div>
            </a>
            <a href="/register-poster"> <div class="col-md-6 col-xs-12 text-center">
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding:25px!important;min-height:339px">
                            {!! $settings_general->registration_text_poster !!}
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="row">
            <h3><a target="_blank" href="https://itsmycall.zendesk.com/hc/en-us">Looking for more information or need help? Visit our Support Centre</a></h3>
        </div>
        
    </div>
</div>
@endsection