@extends('layouts.master')

@section('extra_css')
  <link rel="stylesheet" href="/assets/plugins/redactor/redactor.css"/>
  <link href="/css/card-js.min.css" rel="stylesheet" type="text/css" />
	<style>
.com { color: #93a1a1; }
.lit { color: #195f91; }
.pun, .opn, .clo { color: #93a1a1; }
.fun { color: #dc322f; }
.str, .atv { color: #D14; }
.kwd, .prettyprint .tag { color: #1e347b; }
.typ, .atn, .dec, .var { color: teal; }
.pln { color: #48484c; }

.prettyprint {
  padding: 8px;
  background-color: #f7f7f9;
  border: 1px solid #e1e1e8;
}
.prettyprint.linenums {
  -webkit-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
     -moz-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
          box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
}

/* Specify class=linenums on a pre to get line numbering */
ol.linenums {
  margin: 0 0 0 33px; /* IE indents via margin-left */
}
ol.linenums li {
  padding-left: 12px;
  color: #bebec5;
  line-height: 20px;
  text-shadow: 0 1px 0 #fff;
}
input[type=radio] { display:none; } /* to hide the checkbox itself */
input[type=radio] + label:before {
  font-family: FontAwesome;
  display: inline-block;
}


input[type=radio],
input[type=checkbox] {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}
    
input[type=radio] ~ label:before,
input[type=checkbox] ~ label:before {
    font-family: FontAwesome;
    display: inline-block;
    content: "\f1db";
    letter-spacing: 10px;
    font-size: 23px;
    color: #FFF;
    width: 1.4em;
}

input[type=radio]:checked ~ label:before,
input[type=checkbox]:checked ~ label:before  {
    content: "\f00c";
    font-size: 23px;
    color: white;
    letter-spacing: 5px;
}
input[type=checkbox] ~ label:before {        
    content: "\f096";
}
input[type=checkbox]:checked ~ label:before {
    content: "\f046";        
    color: white;
}
input[type=radio]:focus ~ label:before,
input[type=checkbox]:focus ~ label:before,
input[type=radio]:focus ~ label,
input[type=checkbox]:focus ~ label
{                
    color: white;
}
label.required:after {
  content:"*";
  color:red;
}
.required-empty,
.required-empty:focus {
  background-color: #FFC0BF;
}
</style>
    @stop

@section('extra_js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.js"></script>


<script src="/assets/plugins/redactor/redactor.js"></script>
        <!-- Plugin -->
        <script src="/assets/plugins/redactor/plugins/counter.js"></script>
		<script src="/assets/plugins/redactor/plugins/imagemanager.js" data-cfasync='false'></script>
		<script src="/assets/plugins/redactor/plugins/limiter.js"></script>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('.modal').hide();
        $('#rootwizard').bootstrapWizard({
            onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#rootwizard .progress-bar').css({width: $percent + '%'});
            }
        });
        $("#inbtn").click();
    });
    
    $(document).ready(function () {        
        $("#paymentopt").on('change', function () {
            if ($('#paymentopt').val() == "creditc") {
                $('.card-details').removeClass("hidden");
                $('#invwarning').addClass("hidden");
                $('#cf89073789').val($('#cf89073789old').val());
                setTotalPrice();
            } else if ($('#paymentopt').val() == "eft") {
                $('.card-details').addClass("hidden");
                $('#invwarning').removeClass("hidden");
                $('#cf89073789').val(0);
                setTotalPrice();
            }
        });
        // get attachment image
//  $("#featured_image").on('change', function () {
//      var reader = new FileReader();
//
//      reader.onload = function (e) {
//          // get loaded data and render thumbnail.
//          $(".company-image").attr('src', e.target.result).css('max-width','110px');
//      };
//      // read the image file as a data URL.
//      reader.readAsDataURL(this.files[0]);
//  });

        /* Calculate Price on all steps */
        var PriceStandartJob = $('#step3_box #standard').data('price'); // From: Standard Listing in Step 3
        var PriceStep3 = 0;
        var PriceStep4 = 0;
        var PriceStep5 = 0;
        var TotalPrice = 0;
        var TotalPayable = 0;
        var BonusCredits = 0;
        var skipSelect;
        featuredproduct = false;
       
        $("#couponbtn").click(function () {
            //console.log("hello");
            var token = $('#token').val();
            var code = $('#coupon').val();
            
            $.ajax({
                url: "/poster/checkCouponpac?code=" + code + "&_token=" + token,
                type: "GET",
                success: function (result) {
                    result = JSON.parse(result);
                    //console.log(result.rate)
                    if (result.status == 'ok') {
                        console.log(result);
                        if($('.ref_apply').val() == 'yes'){
                            if($('.codeallow').val() == 'yes' && result.exclusive == 'no'){
                                if(result.exclusive == 'no') {
                                    $('#couponresponse').html("<span style='color:green'>" + result.reason + "</span>");
                                    $('#NhibunsniiMNSK992').val(result.rate);
                                    $('#NhibunsniiMNSK992ID').val(result.id);
                                    $('#ZKONONOSDN3223NSK992').val(result.exclusive);
                                    setTotalPrice(0);
                                }
                            }else{
                                $('#couponresponse').html("<span style='color:red'>" + ' Sorry, this Coupon Code is unable to be combined with your Referral Code.' + "</span>");
                                $('#NhibunsniiMNSK992').val(0);
                                $('#NhibunsniiMNSK992ID').val("");
                                $('#ZKONONOSDN3223NSK992').val(0);
                                $('#step6_Q').html("");
                                $('#step6_DISC').html('$0');
                                setTotalPrice(0);
                            }
                        }else{
                            $('#couponresponse').html("<span style='color:green'>" + result.reason + "</span>");
                            $('#NhibunsniiMNSK992').val(result.rate);
                            $('#NhibunsniiMNSK992ID').val(result.id);
                            $('#ZKONONOSDN3223NSK992').val(result.exclusive);
                            setTotalPrice(0);
                        }
                    } else {
                        $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                        $('#NhibunsniiMNSK992').val(0);
                        $('#NhibunsniiMNSK992ID').val("");
                        $('#ZKONONOSDN3223NSK992').val(0);
                        $('#step6_Q').html("");
                        $('#step6_DISC').html('$0');
                        setTotalPrice(0);
                    }
                }, error: function (result) {
                    result = JSON.parse(result);
                    $('#error').removeClass("hidden");
                    $('#error').html(result.responseText);
                    $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                }
            });
        });

        /* fill step 4 from step 3 */
        $('#step3_box [name=posts_packages]').on('change', function () {
            var _this = $(this).parent().parent().parent();

            PriceStep3 = parseFloat($(this).data('price'), 10) | 0;
            //console.log(PriceStep3);
            var title = _this.find('.title').html();
            var packagename = _this.find('.packagename').val();
            clogo = packagename.substr(0,4) == "Feat" || packagename.substr(0,4) == "Prem";
            featuredproduct = packagename.substr(0,4) == "Feat";
            var price = _this.find('.price').val();
            var description = _this.find('.package-description').text();
            preview_vid = $("#video_link").val();
            if (featuredproduct == true) {
                    $(".pager .next").removeClass("disabled");
            } 
            //console.log(packagename);
            
            //console.log(packagename);
            /* fill step5 */
            $('#step5_box .title').html(title);
            $('#step5_box .desc').html(title);
            $('#step5_box .price').html('$' + PriceStep3);
            /* fill step6 */
            $('.package_name').html(packagename + " Listing");
            $('.package_price').html('$' + price);
            $('#PostsPackages').val($(this).val());

            setTotalPrice();
        });

        $('[name=packages_stats]').on('change', function () {
                $(".next").css("display", "initial");
        });

        /* Fill step 6 on change additional services step 4 */
        $('#step4_box .additional-upgrade, #step4_box .additional-upgrade-count').on('change', function () {
            PriceStep4 = 0;
            $('#step5Upgrades').html('');
            var upgrades = new Array();
            var posts_upgrades = new Array();
            $('#step4_box .additional-upgrade').each(function () {
                if ($(this).is(':checked')) {
                    var id = $(this).data('id');
                    var _this = $(this).parent().parent().parent();
                    PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                    /* get upgrades to array */
                    upgrades[id] = {
                        'title': _this.find('.add-upgrade-title').html(),
                        'price': _this.find('.price').val(),
                        'weeks': _this.find('.additional-upgrade-count').val()
                    };
                    /* create value to POST */
                    posts_upgrades[id] = parseFloat($('#count_' + id).val(), 10);
                }
            });
            /* fill step6 */
            var eq = PriceStep4 + PriceStep3;
            PriceStep5 = 0;
            $('#step5_box .price').html('$' + eq);

            if (PriceStep4 > 0) {
                $('#step6_box .upgrades').removeClass('hidden');
                $('#step6_box .add-upgrade-title').html('$' + PriceStep4);
                $('#PostsUpgrades').val(JSON.stringify(posts_upgrades));

                /* fill Upgrades on step 5 */
                var upgradesHtml = '';
                $(".rr").remove();
                upgrades.forEach(function (e) {
                    //console.log(upgradesHtml + '' + e.title + ': ' + e.price + '<br>');
                    $('#step5Upgrades').append(e.weeks + 'x ' + e.title + " - $" + e.price * e.weeks + "<br>")
                    $('#ordertable').append('<tr class="rr"><td>' + e.title + '</td><td class="text-center">$' + e.price + '</span></td><td class="text-center">' + e.weeks + '</td><td class="text-right">$' + e.price * e.weeks + '</td></tr>');
                });
                //$('#step5Upgrades').html(upgradesHtml);
            } else {
                $(".rr").remove();
            }

            //console.log(PriceStep4);

            setTotalPrice();
            
        });

        /* Fill step 6 on change step 5 */
        $('#step5_box .package').on('change', function () {
            var id = $(this).data('id');

            PriceStep5 = parseFloat($(this).data('price'), 10) | 0;
            /* Use Bonus on Step 5 */
            BonusCredits = parseFloat($(this).data('bonus'), 10) | 0;

            var _this = $(this).parent().parent().parent();
            var title = _this.find('.add-package-title').html();
            title = title + ' ' + _this.find('.add-package-price').html();
            $(".asr").remove();
            /* fill step6 */
            $('#step6_box .add-package .add-package-title').html(title);
            if (_this.find('.add-package-title').html() != "Single Listing Only") {
                $('#ordertable').append('<tr class="asr"><td>' + _this.find('.add-package-title').html() + '<p style="font-size:10px">' + _this.find('.add-package-price').html() + '</p></td><td class="text-center">$' + _this.find('.priceBonus').val() + '</span></td><td class="text-center">1</td><td class="text-right">$' + $(this).data('price') + '</td></tr>');
            }
            if (PriceStep5 > 0) {
                $('#step6_box .add-package').removeClass('hidden');
                $('#PackagesStats').val($(this).val());
            } else {
                $('#step6_box .add-package').addClass('hidden');
                $('#PackagesStats').val(0);
            }

            setTotalPrice();
            var code = "{{$coupon_code}}";
            if (code.length > 3 ) {
                var token = $('#token').val();
                
                $.ajax({
                    url: "/poster/checkRegCoupon?code=" + code + "&_token=" + token,
                    type: "POST",
                    success: function (result) {
                        result = JSON.parse(result);
                        //console.log(result.rate)
                        if (result.status == 'ok') {
                            console.log(result);
                            if(result.exclusive == 'no') {
                                $('#couponresponse').html("<span style='color:green'>" + result.reason + "</span>");
                            }
                            if(result.exclusive == 'yes') {
                                $('#couponresponse').html("<span style='color:orange'>" + result.reason + "</span><br><small>This is an exclusive coupon. Other discounts do not apply.</small>");
                            }
                            $('#NhibunsniiMNSK992').val(result.rate);
                            $('#NhibunsniiMNSK992ID').val(result.id);
                            $('#ZKONONOSDN3223NSK992').val(result.exclusive);
                            setTotalPrice(0);
                        } else {
                            $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                        }
                    }, error: function (result) {
                        result = JSON.parse(result);
                        $('#error').removeClass("hidden");
                        $('#error').html(result.responseText);
                        $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                    }
                });
            }
        });
        
        var allowthrough = false;
        
        function setTotalPrice() {

            var credits = parseFloat($('#use_credits').data('credits'));
            console.log("SUPPCREDITS " + credits);
            var restToCredits = 0;
            var newprice = 0;

            var selectedPrice = PriceStep3 + PriceStep4;
            $('#assdsda23312312').val(0);
            $('#step5_box .package').each(function () {
                if ($(this).is(':checked')) {
                    var id = $(this).data('id');

                PriceStep5 = parseFloat($(this).data('price'), 10) | 0;
                /* Use Bonus on Step 5 */
                BonusCredits = parseFloat($(this).data('bonus'), 10) | 0;

                var _this = $(this).parent().parent().parent();

                var title = _this.find('.add-package-title').html();
                title = title + ' ' + _this.find('.add-package-price').html();
                $(".asr").remove();
                /* fill step6 */
                $('#step6_box .add-package .add-package-title').html(title);
                if (_this.find('.add-package-title').html() != "Single Listing Only") {
                    var ce = $(this).data('price') + $(this).data('bonus');
                    $('#assdsda23312312').val(ce);
                    $('#ordertable').append('<tr class="asr"><td>' + _this.find('.add-package-title').html() + '<p style="font-size:10px">' + _this.find('.add-package-price').html() + '</p></td><td class="text-center">$' + PriceStep5 + '</span></td><td class="text-center">1</td><td class="text-right">$' + $(this).data('price') + '</td></tr>');
                }
                if (PriceStep5 > 0) {
                    $('#step6_box .add-package').removeClass('hidden');
                    $('#PackagesStats').val($(this).val());
                } else {
                    $('#step6_box .add-package').addClass('hidden');
                    $('#PackagesStats').val(0);
                }

                }
            });
            TotalPrice = selectedPrice;
            // Purchase as part of one of our Listing Packages
            if (credits > 0) {
                console.log("in here");
                $(".hideifcred").addClass("hidden");
                $(".credwarning").removeClass("hidden");
                allowthrough = true;

            } else {
                console.log("in here2");
                $(".hideifcred").removeClass("hidden");
                $(".credwarning").addClass("hidden");
                $("#notenough").removeClass("hidden");

            }

            console.log("TotalPrice " + TotalPrice);
            console.log("Step5 " + PriceStep5);

            if (PriceStep5 > 0) { //greater than
                if (PriceStep5 >= selectedPrice) {
                    TotalPrice = PriceStep5;
                    restToCredits = TotalPrice - selectedPrice;
                    restToCredits = restToCredits + BonusCredits;
                    
                    console.log("in here3");
                    //$('.cremain').removeClass('hidden');
                    if (restToCredits < 0)
                        restToCredits = 0;
                    $('#step6_REMAINING').html("$" + restToCredits);
                    $('#ZZZnnusdnosnonjweoj').val(restToCredits);
                    newprice = TotalPrice;
                }
            } else {
                    newprice = TotalPrice;
                    $('.cremain').addClass('hidden');
                    $('#ZZZnnusdnosnonjweoj').val(credits);
                }
            console.log("TotalPriceAfter HERE" + TotalPrice);
            console.log("credits " + credits);
            console.log("restToCredits " + restToCredits);
            console.log("BonusCredits " + BonusCredits);
            
            //var newcredits = restToCredits + BonusCredits;
            /* Calculate Credits */
            var pricebeforecredits = TotalPrice;
            var pricebeforecredits_with_gst = pricebeforecredits + pricebeforecredits * 10 / 100;
            if ($('#use_credits').is(':checked')) {
                $('#uuuucMNNMNmmd').val("yes");
                console.log(credits);
                console.log("credits");
                console.log("pricebeforecredits_with_gst");
                console.log(pricebeforecredits_with_gst);
                console.log(credits <= pricebeforecredits_with_gst);
                if (credits <= pricebeforecredits) {
                    //console.log("supp");
                    console.log("in here4fdcdfdf");
                    //$('.creditsused').removeClass("hidden");
                    //$('#step6_CREDITSUSED').html('$-' + credits);
                    $('#ZZZnnusdnosnonjweoj').val(credits);
                    TotalPayable = pricebeforecredits;
                    
                    console.log("credits below")
                    console.log(credits)
                    newprice = pricebeforecredits - credits;
                    console.log("newprice below")
                    console.log(newprice)
                    //newprice = TotalPayable;
                    TotalPayable = newprice;
                    TotalPrice = newprice;
                    console.log("newprice below")
                    console.log(newprice)
                } else { // credits > TotalPrice
                    //console.log("supp1");
                    console.log("in here5");
                    credits = credits - TotalPrice + restToCredits;
                    console.log("5CREDITS" + credits)
                    $('#use_my_credits').html('$' + TotalPrice);
                    TotalPayable = 0;
                    newprice = 0;
                }
            } else {  //Not use credits and select Listing Packages
                //console.log("supp3");
                $('#uuuucMNNMNmmd').val("no");
                $('.creditsused').addClass("hidden");
                $('#step6_CREDITSUSED').html('$0');
                if (PriceStep5 > 0) {
                    credits = credits + restToCredits;
                }
                $('#ZZZnnusdnosnonjweoj').val(credits);
                TotalPayable = TotalPrice;
                //newprice = TotalPrice;
                    $('#selectpayment').removeClass('hidden');
                if ($('#paymentopt').val() == "creditc") {
                    $('.card-details').removeClass("hidden");
                    $('#invwarning').addClass("hidden");
                    $('#cf89073789').val($('#cf89073789old').val());
                } else if ($('#paymentopt').val() == "eft") {
                    $('.card-details').addClass("hidden");
                    $('#invwarning').removeClass("hidden");
                    $('#cf89073789').val(0);
                }
                $('#step6_REMAINING').html("$" + restToCredits);
                //$('#invwarning').removeClass('hidden');
                $('#selectpayment').show();
            }
            var pricebeforedisc = TotalPrice;
            //console.log("TotalPriceAfter " + TotalPrice);
            $('#step6_ITEMS').html("$" + pricebeforecredits);
            //console.log("newprice " + newprice);

            /* Payment details */
            if (newprice == 0) {
                console.log("in here6");
                $('#payment-form .card-details').hide();
                $('#selectpayment').hide();
                $('#cf89073789').val(0);
                $('#adssdar32rfdsfdsf').val("L");
                $('#invwarning').addClass('hidden');
                $('#jobpaymentitle').html("Submit Listing");
            } else {
                $('#payment-form .card-details').show();
                $('#jobpaymentitle').html("Payment");
                $('#adssdar32rfdsfdsf').val("R");
            }
            

            /* Credits */
            ////console.log(credits + ' ' + BonusCredits + ' ' + (credits + BonusCredits));
            $('#credits').val(credits + BonusCredits);
            $('#rest_credits').html(credits + BonusCredits);
            if ((credits + BonusCredits) > 0) {
                $('#step6_box .rest-credits').removeClass('hidden');
            } else {
                $('#step6_box .rest-credits').addClass('hidden');
            }
            $('#ZZZnnusdnosnonjweoj').val(credits);
            var ac09871280971 = $('#ac09871280971').val();
            var aus_member_discount = ($('#aus_contact_member_no').val() != '') ? (TotalPrice * ac09871280971 / 100).toFixed(2) : 0;
            var other_disc = ($('#rf09871280971').val() != 0) ? (TotalPrice * $('#rf09871280971').val() / 100).toFixed(2) : 0;
            //console.log("OTHERDISC");
            //console.log(other_disc);
            var nudisc = 0;
            
            $('#step6_aus_member_discount').html('$0');
            $('#step6_AC').html("");

            if ($('#ZKONONOSDN3223NSK992').val() == 'yes') { // coupon
                nudisc = $('#NhibunsniiMNSK992').val();
                aus_member_discount = 0;
                other_disc = 0;
                $('#step6_QOTHER').html("");
                $('#step6_DISCOTHER').html("$0");
            }
            if ($('#rf09871280971EXC').val() == 'yes') {
                nudisc = 0;
                aus_member_discount = 0;
//                other_disc = 0;
//                $('#step6_QOTHER').html("");
//                $('#step6_DISCOTHER').html("$-" + );
            }
            if ($('#aus_contact_member_no').val() != '') { // auscontact
                ///nudisc = 0;
                aus_member_discount = aus_member_discount;
                //other_disc = 0;
                $('#step6_aus_member_discount').html('$-' + aus_member_discount);
                $('#step6_AC').html($("#ac09871280971").val() + "%");
//               $('#step6_QOTHER').html("");
//                $('#step6_DISCOTHER').html("$-" + );
            } else if($('#ac09871280971EXC').val() != 'yes' && $('#aus_contact_member_no').val() != '') {
                nudisc = 0;
            }

            nudisc = $('#NhibunsniiMNSK992').val();
         
            if (other_disc != 0) {
                //console.log("hello");
                //$('#step6_QOTHERNAME').html($('#rf09871280971#NAME').val());
                $('#step6_QOTHER').html($('#rf09871280971').val() + "%");
                $('#step6_DISCOTHER').html("$-" + other_disc);
            } else {
                $('#step6_QOTHER').html("");
                $('#step6_DISCOTHER').html("$0");
            }
            
                TotalPrice = TotalPrice - aus_member_discount;
                TotalPrice = TotalPrice - other_disc;
                console.log(TotalPrice);
            if (nudisc != 0) { //coupon - coupon will always be applied last. 
                $('#step6_Q').html(nudisc + "%");
                     //other_disc = 0;
                    //console.log("NEW PRICE INSIDE NUDISC" + TotalPrice)
                    nudisc = (TotalPrice * nudisc / 100).toFixed(2);
                    $('#step6_DISC').html("$-" + nudisc);
                } else {
                   nudisc = 0;
                }
            
            
        
            TotalPrice = TotalPrice - nudisc;
                
            
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                
                var gst = (TotalPrice / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = parseFloat(TotalPrice);
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                $('#step6_SUB').html("$" + totalminusgst);
                //console.log("SETTINGSUB HERE2");
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice + ccfeetotal;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    //console.log("SETTINGSUB HERE1");
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                    $('#fintot').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
                
            
            var nucredits = parseFloat($('#use_credits').data('credits'), 10) | 0;
            var pricebeforecredits = pricebeforecredits + pricebeforecredits * 10 / 100;
            
            if ($('#use_credits').is(':checked')) {
                //console.log(credits);
                //console.log("CREDITS ABOVE");
            if (nucredits <= pricebeforecredits) {
                var pricebeforedisc = TotalPrice;
                //TotalPrice = TotalPrice - aus_member_discount;
                //TotalPrice = TotalPrice - other_disc;
                //TotalPrice = TotalPrice - nudisc;
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                
                var gst = (TotalPrice / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = parseFloat(TotalPrice);
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                
//                console.log("SETTINGSUB HERE3");
                
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    console.log("SETTINGSUB HERE3");
                    //console.log("totalplusgst");
                    //console.log(totalplusgst);
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    //console.log(ccfeetotal);
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                    $('#fintot').val(tots.toFixed(2));
                } else {
                    console.log("SETTINGSUB HERE4");
                    $('#step6_SUB').html("$" + totalminusgst.toFixed(2));
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
            } else { // credits > TotalPrice
                console.log("SETTINGSUB HERE5");
                var pricebeforedisc = TotalPrice;
                console.log("CHECKER ABOVE" + TotalPrice)
                console.log("CHECKER " + TotalPrice)
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                $('#fintot').val(0);
                var gst = (TotalPrice / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = parseFloat(TotalPrice);
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                var credos = pricebeforedisc + credits;
               
                $('#step6_SUB').html("$" + TotalPrice.toFixed(2));
                //console.log("SETTINGSUB HERE6");
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    //console.log("SETTINGSUB HERE9");
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                    $('#fintot').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
            }
        } else {
            //console.log("hello");
            var pricebeforedisc = TotalPrice;
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                //$('#fintot').val(0);
                var gst = (TotalPrice / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = pricebeforedisc - gst;
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                $('#step6_SUB').html("$" + TotalPrice.toFixed(2));
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#fintot').val(tots.toFixed(2));
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
        }
                
            
            
            return;
        };
    });
</script>
<script src="/js/card-js.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    Stripe.setPublishableKey("{{ env('STRIPE_PUBLISHABLE_KEY') }}");
</script>
<script>
    $(function () {
        //PAYMENT
        var $form = $('#payment-form');
        $form.submit(function (event) {
            if ($('#fintot').val() > 0 && $('#adssdar32rfdsfdsf').val() != "L" && $('#paymentopt').val() != "eft") {
                // Disable the submit button to prevent repeated clicks:
                $form.find('.submit').prop('disabled', true);

                // Request a token from Stripe:
                Stripe.card.createToken($form, stripeResponseHandler);

            } else {
                submitForm(false);
            }
            // Prevent the form from being submitted:
            return false;
        });

        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            //console.log("GOT RESPONSE");
            //console.log(response);
            if (response.error) { // Problem!
                //console.log(response.error);
                // Show the errors on the form:
                $('#error').html(response.error.message);
                $('#error').removeClass("hidden");
                $form.find('.submit').prop('disabled', false); // Re-enable submission
                return false;


            } else { // Token was created!

                // Get the token ID:
                var token = response.id;

                submitForm(token);

                return false;

                // Submit the form:
                // $form.get(0).submit();
            }
        }
        ;

        function submitForm(token) {
            var $form = $('#payment-form');
            // Insert the token ID into the form so it gets submitted to the server:
            $form.append($('<input type="hidden" id="stripeToken" name="stripeToken">').val(token));

            // Append Summ
            // var summ = parseFloat($('#step6_total_price').text(), 10) | 0;
            // $form.append($('<input type="hidden" id="summ" name="summ">').val(summ));

            //append fields from form on Step 2
            $form.append('<div class="hidden" id="submitted_form"></div>');
            var form = $("#form-username").clone();
            var form1 = $("#billing-form").clone();
            $('#submitted_form').html("");
            $('#submitted_form').append(form.find(":input"));
            $('#submitted_form').append(form1.find(":input"));
            //$(this).parents("submitted_form").ajaxForm(options);
            var _self = $('#payment-form'),
                _data = _self.serialize();

            $.ajax({
                url: _self.attr('action'),
                data: _data,
                method: _self.attr('method'),
                dataType: 'json',
                beforeSend: function (xhr) {
                    // $('#').loadingIndicator();
                    $('.modal').show();
                },
                success: function (response) {
                    //console.log(response);
                    if (response.status == 200) {
                        if($('#adssdar32rfdsfdsf').val() == "R" && $('#paymentopt').val() == "creditc"){
                            window.location.replace("/poster?creditcard");
                        }else{
                            window.location.replace("/poster?invoice");
                        }

                    } else {
                        $('.modal').hide();
                        $('#error').html(response.error);
                        $('#error').removeClass('hidden');
                        $form.find('.submit').prop('disabled', false); // Re-enable submission    
                    }
                },
                error: function (response) {
                    //console.log(response);
                    $('.modal').hide();
                    $('#error').html(response.error);
                    $('#error').removeClass('hidden');
                    $('#error').html(response.responseText);
                    //$('#output').html(response.responseText);
                    $form.find('.submit').prop('disabled', false);
                    return false;
                }
            });
            return true;
        }
        ;
    });

    $(function () {
        //result.address_components[0]
        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            //console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    //console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    $('#stateselect option[value="' + value.long_name + '"]').prop('selected', true);
                    $("#stateselect").val(value.long_name).change();
                    $("#state").val(value.long_name);
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            //console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            //console.log(result.geometry.location.lng());
            //console.log(result);


        });

        $("#billing_address").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            //console.log(result.address_components);

            results = result.address_components;

        });

        $("#find").click(function () {
            $("#billing_address").trigger("geocode");
        });

        $('#find').keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                $("#joblocation").trigger("geocode");
            }
        });


        $("#examples a").click(function () {
            $("#joblocation").val($(this).text()).trigger("geocode");
            return false;
        });

    });

</script>



@stop


@section('content')
<style>
.center-pills { display: inline-block; }
</style>
<div class="modal"><!-- Place at bottom of page --></div>
  <div class="bg-color1 block-section line-bottom">
    <div class="container">
	    <div id="rootwizard">
        
        <div class="navbar" style="display:none">
          <div class="navbar-inner" style="text-align: center;">
            <div class="container no-pad">
	            <ul class="center-pills">
                <li><a href="#tab5" data-toggle="tab" class="a-tab a-tab5 disabled">Credit Packages</a></li>
                <li><a href="#tab6" data-toggle="tab" class="a-tab a-tab5 disabled">Finalisation</a></li>
              </ul>
            </div>
      	  </div>
      	</div>
        
	      <div class="tab-content">
          
          <div class="tab-pane" id="tab5">
				@include('buy-credits.step5')
          </div>
    
		      <div class="tab-pane" id="tab6">
			      @include('buy-credits.step6')
	        </div>
		
		<ul class="pager wizard">
			<li class="previous first" style="display:none;"><a href="#">First</a></li>
			<li class="previous"><a href="#">Previous</a></li>
		  <li class="next" style="display: none;
          "><a href="#">Next</a></li>
		</ul>
	</div>	
</div>
		
    <div class="row"></div>
  </div>
</div>

@endsection