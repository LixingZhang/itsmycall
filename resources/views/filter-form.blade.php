<div class="panel panel-default">
  <div class="panel-heading">Search For Jobs</div>
  <div class="panel-body">

	<div class="result-filter">
	<form action="/job_list" method="POST" role="form">
		<div class="col-md-12">
			<div class="col-md-4">
				<h5 class="font-bold  margin-b-20" style="margin-left:-15px;"><a href="#s_collapse_2" data-toggle="collapse">Job Category</a></h5>
				<div class="form-group ">
					<select class="form-control" style="margin-left:-15px;" name="category" id="category" value="{{isset($filter['category']) ? $filter['category'] : ''}}">
					</select>
					<i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i>
				</div>
				<div class="form-group ">
					<select class="form-control" style="margin-left:-15px;margin-top:15px;" name="subcategory" id="subcategory" value="{{isset($filter['subcategory']) ? $filter['subcategory'] : ''}}">
						@foreach($categories as $category)
						<option value="-1">Sub Category</option>
						@foreach($category->subcategory as $subcategory)
						<option value="{{$subcategory->title}}">{{$subcategory->title}}</option>
						@endforeach
						@endforeach
					</select>
				</div>
			</div>

			<div class="col-md-4" style="">
				<h5 class="font-bold  margin-b-20" style="margin-left:-15px;"><a href="#s_collapse_2" data-toggle="collapse">Job Location</a></h5>
				<div class="form-group">
                                                    <div class="form-group" style="display:none">
                                                        <select style="margin-left:-15px;" id="country1" name="country" class="form-control"></select>
                                                        <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i>
                                                    </div>
                                                    <div class="form-group">
                                                        <select style="margin-left:-15px;margin-top:15px;" id="state2"
                                                                name="state" class="form-control">
                                                            <option value="">Select State</option>
                                                            <option value="Australian Capital Territory">Australian
                                                                Capital Territory
                                                            </option>
                                                            <option value="New South Wales">New South Wales</option>
                                                            <option value="Northern Territory">Northern Territory
                                                            </option>
                                                            <option value="Queensland">Queensland</option>
                                                            <option value="South Australia">South Australia</option>
                                                            <option value="Tasmania">Tasmania</option>
                                                            <option value="Victoria">Victoria</option>
                                                            <option value="Western Australia">Western Australia</option>
                                                        </select>
                                                    </div>
                                                </div>

				<div class="checkbox">
                                                    <label>
                                                        <input type="hidden" name="work_from_home" value="0">
                                                        <input type="checkbox"
                                                               name="work_from_home"
                                                               value="1"
                                                               @if (@$filter['work_from_home'] == 1) checked @endif
                                                        >
                                                        Only display work from home jobs
                                                    </label>
                                                </div>
			</div>
			<div class="col-md-4">
				<h5 class="font-bold  margin-b-20" style="margin-left:-15px;">
					<a href="#s_collapse_2" data-toggle="collapse">Job Keywords</a>
				</h5>
				<div class="form-group">
					<input style="margin-left:-15px;" type="text" name="title" class="form-control" placeholder="Keywords" value="{{isset($filter['title']) ? $filter['title'] : ''}}">
				</div>
			</div>
			
			
		</div>

		<div class="col-md-4 margin-b-20">
			<h5 class="font-bold margin-b-20 "><a href="#s_collapse_1" data-toggle="collapse">Salary Filter</a></h5>

			<div class="collapse in" id="s_collapse_1">
				<p>
					<input type="text" id="amount1" readonly style="border:0; color:#f6931f; font-weight:bold;">
					<input type="hidden" id="salstart" name="salstart">
					<input type="hidden" id="salend" name="salend">
				</p>

				<div id="slider-range1"></div>
				<div class="list-area" style="display:none">
					<ul class="list-unstyled">
						<li>
							<a href="/price_filter/1">0$-1000$</a>({{count(\App\Posts::whereBetween('salary', [0, 1000])->get())}}
							)
						</li>
						<li>
							<a href="/price_filter/2">1000$-5000$</a>({{count(\App\Posts::whereBetween('salary', [1000, 5000])->get())}}
							)
						</li>
						<li>
							<a href="/price_filter/3">5000$-10000$</a>({{count(\App\Posts::whereBetween('salary', [5000, 10000])->get())}}
							)
						</li>
						<li>
							<a href="/price_filter/4">10000$-20000$</a>
							({{count(\App\Posts::whereBetween('salary', [10000, 20000])->get())}})
						</li>
						<li>
							<a href="/price_filter/5">20000$ +</a>
							({{count(\App\Posts::where('salary','>',20000)->get())}})
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-4 margin-b-20">
			<h5 class="font-bold  margin-b-20">
				<a href="#s_collapse_2" data-toggle="collapse">{{trans('messages.post_employment_type')}}</a>
			</h5>
			<div class="checkbox">
				<label>
					<input type="checkbox"
						   name="employment_type[]"
						   value="{{\App\Posts::EMPLOYMENT_TYPE_PART_TIME}}"
						   @if (isset($filter['employment_type']) AND in_array(\App\Posts::EMPLOYMENT_TYPE_PART_TIME, $filter['employment_type'])) checked @endif
					>
					{{trans('messages.post_employment_type_part_time')}}
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox"
						   name="employment_type[]"
						   value="{{\App\Posts::EMPLOYMENT_TYPE_FULL_TIME}}"
						   @if (isset($filter['employment_type']) AND in_array(\App\Posts::EMPLOYMENT_TYPE_FULL_TIME, $filter['employment_type'])) checked @endif
					>
					{{trans('messages.post_employment_type_full_time')}}
				</label>
			</div>
			<div class="checkbox disabled">
				<label>
					<input type="checkbox"
						   name="employment_type[]"
						   value="{{\App\Posts::EMPLOYMENT_TYPE_CASUAL}}"
						   @if (isset($filter['employment_type']) AND in_array(\App\Posts::EMPLOYMENT_TYPE_CASUAL, $filter['employment_type'])) checked @endif
					>
					{{trans('messages.post_employment_type_casual')}}
				</label>
			</div>
      <div class="checkbox">
        <label></label>
      </div>
		</div>

		<div class="col-md-4 margin-b-20">
			<h5 class="font-bold  margin-b-20">
				<a href="#s_collapse_3" data-toggle="collapse">{{trans('messages.post_employment_term')}}</a>
			</h5>

			<div class="checkbox">
				<label>
					<input type="checkbox"
						   name="employment_term[]"
						   value="{{\App\Posts::EMPLOYMENT_TERM_PERMANENT}}"
						   @if (isset($filter['employment_term']) AND in_array(\App\Posts::EMPLOYMENT_TERM_PERMANENT, $filter['employment_term'])) checked @endif
					>
					{{trans('messages.post_employment_permanent')}}
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox"
						   name="employment_term[]"
						   value="{{\App\Posts::EMPLOYMENT_FIXED_TERM}}"
						   @if (isset($filter['employment_term']) AND in_array(\App\Posts::EMPLOYMENT_FIXED_TERM, $filter['employment_term'])) checked @endif
					>
					{{trans('messages.post_employment_fixed_term')}}
				</label>
			</div>
      <div class="checkbox">
				<label>
					<input type="checkbox"
						   name="employment_term[]"
						   value="{{\App\Posts::EMPLOYMENT_TEMP}}"
						   @if (isset($filter['employment_term']) AND in_array(\App\Posts::EMPLOYMENT_TEMP, $filter['employment_term'])) checked @endif
					>
					{{trans('messages.post_employment_temp')}}
				</label>
			</div>
		</div>

	<input type="hidden" name="_token" value="{{csrf_token()}}"/>

		<div class="col-md-4" style="padding-bottom: 15px;">
			<button type="submit" class="btn btn-primary btn-block" ><i class="fa fa-search"></i> Search</button>
		</div>

	</form>
	</div>
  </div>
</div>

{{--
<div class="panel panel-default">
  <div class="panel-heading">Browse By Category</div>
  <div class="panel-body">
	<div class="col-md-12">
		<div class="row multi-columns-row">
			@foreach($categories as $category)
			<div class="col-xs-12 col-sm-4 col-lg-4">

				<ul class="list-group" data-toggle="collapse" data-target="#main_cat_{{$category->id}}">
					<li class="list-group-item"><i class="fa fa-caret-down fa-lg"></i><a href="/category_filter/{{$category->id}}"> {{$category->title}}</a><span id = "{{$category->title}}" class="badge pull-right jcount">{{$category->count}}</span>
						@if(sizeof($category->subcategory)>0) 
						<ul style="position: absolute;width: 100%;margin-left: -15px;background:#F5F3F3;z-index: 1000;border:1px solid #cecece"
						id="main_cat_{{$category->id}}" class="collapse out">
						@foreach($category->subcategory as $subcategory)
						<li>
							<a href="/sub_category_filter/{{$subcategory->id}}"> {{$subcategory->title}} </a><span id="{{$subcategory->title}}" class="badge subcount">{{$subcategory->count}}</span>
						</li>
						@endforeach
						</ul>
						@endif
					</li>
				</ul>
			</div>
			@endforeach
		</div>
	</div>
  </div>
</div>
--}}