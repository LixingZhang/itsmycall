@extends('layouts.master')
@section('extra_js')
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });
        $('.dropdown-menu input').click(function(e) {
        e.stopPropagation(); //This will prevent the event from bubbling up and close the dropdown when you type/click on text boxes.
    });
    });

</script>
<script type="text/javascript" src="/assets/js/countries.js"></script>
<script>//populateCountries("country", "state");</script>
<script>//populateCountries("country1", "state2");</script>

<script>


    $(function () {
        //result.address_components[0]
        var checkbox = $('#searchour');

        $('#searchour').on('change', function () {
            if ($('#searchour').is(':checked')) {
                $('.salrwapperannual').addClass("hidden")
                $('.hourlywapperannual').removeClass("hidden")
            } else {
                $('.hourlywapperannual').addClass("hidden")
                $('.salrwapperannual').removeClass("hidden")
            }
        });

        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU"
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    console.log(value.long_name); // here you get the zip code
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    console.log("Hello");
                    $('#state2 option[value="' + value.long_name + '"]').attr('selected', 'selected');
                    $("#stateselectbill").val(value.long_name);
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            console.log(result.geometry.location.lng());
            console.log(result);


        });

        $("#find").click(function () {
            $("#joblocation").trigger("geocode");
        });

        $('#find').keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                $("#joblocation").trigger("geocode");
            }
        });


        $("#examples a").click(function () {
            $("#joblocation").val($(this).text()).trigger("geocode");
            return false;
        });

        var arCats = [];
        var arSubCats = [];
        @foreach ($categories as $category)
            var oCatValKey = {
                id: "{{ $category->id }}",
                title: "{!!$category->title!!} "
            };
            arCats.push(oCatValKey);

            var arSubCat = [];
            @foreach ($category->subcategory as $subcategory)
                var oSubCatValKey = {
                    id: "{{ $subcategory->id }}",
                    title: "{!!$subcategory->title!!}"
                };
                arSubCat.push(oSubCatValKey);
            @endforeach
              arSubCats[oCatValKey.id] = arSubCat;
        @endforeach

        function populateSubCats(categoryElementId, subcategoryElementId) {
            var select = document.getElementById(categoryElementId);
            var value = select.options[select.selectedIndex].value;

            var subcategoryElement = document.getElementById(subcategoryElementId);
            subcategoryElement.length = 0;
            //subcategoryElement.options[0] = new Option('Select Sub Category', '-1');


            var arSubCat = arSubCats[value];
            if (typeof arSubCat === 'undefined') {
              return $('#subcategory').multipleSelect();
            }

            for (var i = 0; i < arSubCat.length; i++) {
                var objSubCat = arSubCat[i];
                subcategoryElement.options[i] = new Option(objSubCat.title, objSubCat.id);

                if (subcategoryElement.getAttribute('value') === objSubCat.id) {
                    subcategoryElement.value = subcategoryElement.getAttribute('value');
                }
            }
            subcategoryElement.selectedIndex = -1
            $('#subcategory').multipleSelect({
              placeholder: "Select Sub Category..."
            });
        }

        function populateMainCats(categoryElementId, subcategoryElementId) {
            var categoryElement = document.getElementById(categoryElementId);
            categoryElement.length = 0;
            categoryElement.options[0] = new Option('Select Category', '-1');
            categoryElement.selectedIndex = 0;

            for (var i = 0; i < arCats.length; i++) {
                var objCat = arCats[i];
                categoryElement.options[categoryElement.length] = new Option(objCat.title, objCat.id);
            }
            if (categoryElement.getAttribute('value')) {
                categoryElement.value = categoryElement.getAttribute('value');
            }

            // Assigned all countries. Now assign event listener for the states.
            if (subcategoryElementId) {
                categoryElement.onchange = function () {
                    populateSubCats(categoryElementId, subcategoryElementId);
                };
                //populateSubCats(categoryElementId, subcategoryElementId);
            }
        }

        populateMainCats("category", "subcategory");
        //multiple select

        $('#subcategory').multipleSelect({
            placeholder: "Select Sub Category..."
        });
        $('#create-alert').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action')
            var errors = {};
            for (var k in errors) {                            
                            if(k == 'category'){
                                $('.categoryinput').css('border-color', '');
                                $('.categoryerror').css('visibility', 'hidden');
                            }
                            if(k == 'subcategory'){
                                $('.subcategoryinput').css('border-color', '');
                                $('.subcategoryerror').css('visibility', 'hidden');
                            }
                            if(k == 'hrsalstart'){
                                $('.hrrateinput').css('border-color', '');
                                $('.hrrateerror').css('visibility', 'hidden');
                            }
                            if(k == 'wage_min'){
                                $('.maxrateinput').css('border-color', '');
                                $('.maxrateerror').css('visibility', 'hidden');
                            }
                            if(k == 'pay_cycle'){
                                $('.paycycleinput').children().css('border-color', '');
                                $('.paycycleerror').css('visibility', 'hidden');
                            }
                            if(k == 'employment_type'){
                                $('.emptypeinput').css('border-color', '');
                                $('.emptypeerror').css('visibility', 'hidden');
                            }
                            if(k == 'employment_term'){
                                $('.empterminput').children().css('border-color', '');
                                $('.emptermerror').css('visibility', 'hidden');
                            }
                            if(k == 'incentivestructure'){
                                $('.incinput').children().css('border-color', '');
                                $('.incerror').css('visibility', 'hidden');
                            }
                            if(k == 'work_from_home'){
                                $('.wlinput').children().css('border-color', '');
                                $('.wlerror').css('visibility', 'hidden');
                            }
                            if(k == 'parentsoption'){
                                $('.sginput').children().css('border-color', '');
                                $('.sgerror').css('visibility', 'hidden');
                            }
                            if(k == 'joblocation'){
                                $('.locinput').css('border-color', '');
                                $('.locerror').css('visibility', 'hidden');
                            }
                            if(k == 'state'){
                                $('.locstinput').css('border-color', '');
                                $('.locsterror').css('visibility', 'hidden');
                            }
                            if(k == 'radius'){
                                $('.radinput').css('border-color', '');
                                $('.raderror').css('visibility', 'hidden');
                            }
            }
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                success: function (data) {
                    if (data.errors !== undefined) {
                        errors = data.errors;
                        var messages = [];
                        for(k in errors) {
                            if(k){
                                $('.fieldsrequired').css('visibility', 'visible');
                            }
                            if(k == 'category'){
                                $('.categoryinput').css('border-color', '#a94442');
                                $('.categoryerror').css('visibility', 'visible');
                            }
                            if(k == 'subcategory'){
                                $('.subcategoryinput').css('border-color', '#a94442');
                                $('.subcategoryerror').css('visibility', 'visible');
                            }
                            if(k == 'hrsalstart'){
                                $('.hrrateinput').css('border-color', '#a94442');
                                $('.hrrateerror').css('visibility', 'visible');
                            }
                            if(k == 'wage_min'){
                                $('.maxrateinput').css('border-color', '#a94442');
                                $('.maxrateerror').css('visibility', 'visible');
                            }
                            if(k == 'pay_cycle'){
                                $('.paycycleinput').children().css('border-color', '#a94442');
                                $('.paycycleerror').css('visibility', 'visible');
                            }
                            if(k == 'employment_type'){
                                $('.emptypeinput').css('border-color', '#a94442');
                                $('.emptypeerror').css('visibility', 'visible');
                            }
                            if(k == 'employment_term'){
                                $('.empterminput').children().css('border-color', '#a94442');
                                $('.emptermerror').css('visibility', 'visible');
                            }
                            if(k == 'incentivestructure'){
                                $('.incinput').children().css('border-color', '#a94442');
                                $('.incerror').css('visibility', 'visible');
                            }
                            if(k == 'work_from_home'){
                                $('.wlinput').children().css('border-color', '#a94442');
                                $('.wlerror').css('visibility', 'visible');
                            }
                            if(k == 'parentsoption'){
                                $('.sginput').children().css('border-color', '#a94442');
                                $('.sgerror').css('visibility', 'visible');
                            }
                            if(k == 'joblocation'){
                                $('.locinput').css('border-color', '#a94442');
                                $('.locerror').css('visibility', 'visible');
                            }
                            if(k == 'state'){
                                $('.locstinput').css('border-color', '#a94442');
                                $('.locsterror').css('visibility', 'visible');
                            }
                            if(k == 'radius'){
                                $('.radinput').css('border-color', '#a94442');
                                $('.raderror').css('visibility', 'visible');
                            }
                            messages.push('<li style="margin-left:10px;">' + errors[k] + '</li>')
                        }
                        $('#errors').html(messages.join('')).removeClass('hidden');
                    } else {
                        $('#errors').html('').addClass('hidden');
                        location.reload();
                    }
                }
            });
        });
    });


</script>
@stop
<script>
    function loadModal(id) {
        $('#myModal').modal('toggle');
        $('#currStatus').val(id);
    }
</script>
<style>
    .pac-container {
        /* put Google geocomplete list on top of Bootstrap modal */
        z-index: 9999;
    }
    .form-group {
    margin-bottom: 10px !important;
    }
</style>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@section('content')
<div class="bg-color1">
    <div class="container">
        <div class="col-md-3 col-sm-3">

            <div class="block-section text-center">
                <img src="{{Auth::user()->avatar}}" class="img-rounded" alt="" style="max-width: 150px;">
                <div class="white-space-20"></div>
                <h4>Hi {{Auth::user()->name}}</h4>
                <div class="white-space-20"></div>
                <ul class="list-unstyled">
                    <li><a href="/customer/edit-user"> Edit Profile </a></li>
                    <li><a href="/customer/change_password"> Change Password</a></li>
                </ul>
                <div class="white-space-20"></div>
                <a href="/customer/savedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Saved Jobs</a>
                <div class="white-space-20"></div>
                <a href="/customer/appliedjobs" class="btn btn-info btn-block" style="background-color:#639941; color: white; border:0;">View Applied Jobs</a>
                <div class="white-space-20"></div>
                <a href="/customer/connect-program" class="btn btn-info btn-block" style=" border:0;">Connect Program</a>
            </div>
        </div>

        <div class="col-md-9 col-sm-9">
            <div class="block-section box-side-account">
                <h3 class="no-margin-top">Find your next contact centre job with ItsMyCall</h3>
                @include('admin.layouts.notify')
                <p>Get jobs suited to your preferences delivered straight to your inbox. </p>
                <button type="button" class="btn btn-primary" onclick="loadModal()">Create Job Alert</button>
                <hr/>
                @if(sizeof($subscriptions)>0)
                <h3 class="no-margin-top">Your Active Job Alerts</h3>
                @endif
                @foreach($subscriptions as $item)
                    <div class="panel panel-default">
                        <div class="panel-body" style="padding: 25px!important">
                            <div class="row">
                                <div class="col-md-8">

                                    <h4 class="list-group-item-heading">{{$item->category->title}}</h4>
                                    @if (!empty($item->subcategoryTitles))
                                        <p class="list-group-item-text"><br><strong>Sub Category: </strong>{{implode(", ", $item->subcategoryTitles)}}</p>
                                    @endif
                                    <p class="list-group-item-text"><br><strong>Advertiser Type: </strong>
                                    <?php if ($item->advertiser_type == "private_advertiser") {
                                        print "Private Advertiser";
                                    } elseif ($item->advertiser_type == "recruitment_agency") {
                                        print "Recruitment Agency";
                                    } else { ?>
                                        {{ $item->advertiser_type ?: implode(',  ', \App\Users::getAdvertiserTypes())}}
                                    <?php } ?>
                                    </p>
                                    <p class="list-group-item-text <?php if ($item->salary_type == 'yes') { ?> hidden <?php } ?>"><br><strong>Wage Range: </strong> ${{number_format($item->wage_min)}} to ${{number_format($item->wage_max)}}</p>
                                    <p class="list-group-item-text <?php if ($item->salary_type == '') { ?> hidden <?php } ?>"><br><strong>Wage Range (Hourly): </strong> ${{$item->hourly_start}} to @if($item->hourly_end > 100) $100+ @else ${{$item->hourly_end}} @endif</p>
                                    <p class="list-group-item-text"><br><strong>Pay Cycle: </strong>
                                            <?php 
                                            $jsonString = $item->pay_cycle;
                                            $keys = json_decode($jsonString);
                                             ?>
                                            @if($keys)
                                                {{ucwords(str_replace("_", " ", implode(', ', json_decode($item->pay_cycle, true))))}}
                                            @else
                                            nil
                                            @endif
                                    <p class="list-group-item-text"><br><strong>Employment Type: </strong>
                                        {{ucfirst(str_replace("_", " ", $item->employment_type))}}
                                    </p>
                                    <p class="list-group-item-text"><br><strong>Employment Status: </strong>
                                        @if($item->employment_term == 'fixed_term')
                                        Fixed-Term / Contract
                                        @else
                                        {{ucwords(str_replace("_", " ", implode(', ', json_decode($item->employment_term, true))))}}
                                        @endif
                                    </p>
                                    <p class="list-group-item-text"><br><strong>
                                    Work Location: </strong><br>
                                        <?php 
                                            $jsonString = $item->work_from_home;
                                            $keys = json_decode($jsonString);
                                             ?>
                                            @if($keys)
                                            <ul>
                                            @foreach($keys as $key)
                                                    @if($key == 1)
                                                    <li> At business address</li>
                                                    @endif
                                                    @if($key == 2)
                                                    <li> By mutual agreement</li>
                                                    @endif
                                                    @if($key == 3) 
                                                    <li> Work from home </li>
                                                    @endif
                                                
                                            @endforeach
                                            </ul>
                                            @else
                                            nil
                                            @endif
                                    </p>
                                    <p class="list-group-item-text"><br><strong>
                                    Shift Guide (between these hours): </strong><br>
                                        <?php 
                                        $jsonString2 = $item->parentsoption;
                                        $keys2 = json_decode($jsonString2);
                                         ?>
                                        @if($keys2)
                                        <ul>
                                        @foreach($keys2 as $key)
                                            
                                                @if($key == 1)
                                                <li> Standard (9am-5pm Mon to Fri)</li>
                                                @endif
                                                @if($key == 2)
                                                <li>Extended (8am-8pm Mon to Fri)</li>
                                                @endif
                                                @if($key == 3) 
                                                <li>Parent Friendly (9am-3pm Mon to Fri) </li>
                                                @endif
                                                @if($key == 4) 
                                                <li>Afternoon (12pm-12am Mon to Fri)</li>
                                                @endif
                                                @if($key == 5) 
                                                <li>Night Shift </li>
                                                @endif
                                                @if($key == 6) 
                                                <li>Rotating shifts - no weekends</li>
                                                @endif
                                                @if($key == 7) 
                                                <li>Rotating shifts - including weekend work</li>
                                                @endif
                                            

                                        @endforeach
                                        </ul>
                                        @else
                                        nil
                                        @endif
                                    </p>
                                    <p class="list-group-item-text">
                                        <br><strong>Incentive Structure: </strong>
                                        @if(count($item->incentivestructure) == count(\App\Subscription::INCENTIVE_STRUCTURE))
                                        Any
                                        @else
                                        <!-- {{ implode(', ', $item->incentivestructureTitle) }} !-->
                                        <ul>
                                            @foreach ($item->incentivestructureTitle as $eachincentivestructureTitle)
                                                <li>{{$eachincentivestructureTitle}}</li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </p>
                                    <p class="list-group-item-text"><br><strong>Location Details: </strong> Within {{$item->radius}}kms of {{$item->location}}</p>
                                    <p class="list-group-item-text"><a href="/customer/deletesub/{{$item->id}}" style="margin-top:20px;" type="button" class="btn btn-theme btn-xs btn-default">Remove Job Alert</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
    <div class="modal fade" tabindex="-1" id="myModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create new Job Alert</h4>
                </div>
                <div class="modal-body">
                    <form action="/customer/subscriptions/user" method="post" novalidate id="create-alert">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <p class="fieldsrequired" style="color: #cc0000; margin: 0;display: block;visibility: hidden;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please fill in the required fields!</p>
                        <div class="form-group ">
                            <div class="select-style">
                                <label>Main Category</label>
                                <select class="form-control categoryinput" style="" name="category" id="category" required></select>
                                    <small class="categoryerror" style="color: #cc0000; margin: 0; display: block; visibility: hidden;">Please select the Main Job category</small>
                            </div>
                        </div> 
                        <div class="form-group ">
                            <div class="select-style">
                                <label>Sub Category</label>
                                <p id="notenough" class="text-center hidden" style="font-size: 14px;text-align: center;"><small><i class="fa fa-info-circle" aria-hidden="true"></i> Select 1 sub category. If you would like to select more, simply create another job alert.</small></p>
                                <select class="form-control subcategoryinput" style="" name="subcategory[]" id="subcategory" required multiple="multiple">
                                </select>
                                <small class="subcategoryerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please select the Job Sub category</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="select-style">
                                <label>{{trans('messages.advertiser_type')}}</label>
                                <select class="form-control" name="advertiser_type" id="advertiser_type">
                                    <option value="both">Both</option>
                                    @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                        <option value="{{$type}}" {{old('advertiser_type') == $type ? 'selected' : ''}}>{{$label}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="searchour" id="searchour" value="yes" {{old('searchhour') == 'yes' ? 'checked' : ''}}>
                                Search By Hourly Rates
                            </label>
                        </div>
                        <div class="salrwapperannual">
                            <div class="col-md-6">
                                <label>Min</label>
                                <select class="form-control maxrateinput" style="margin-left:-15px;" name="wage_min" id="wage_min">
                                    @foreach(\App\Subscription::WAGE_RANGE_MIN as $wage => $label)
                                        <option value="{{$wage}}" data-reactid="{{$wage}}" {{old('salstart') == $type ? 'selected' : ''}}>{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Max</label>
                                <select class="form-control maxrateinput" style="margin-left:-15px;" name="wage_max" id="wage_max">
                                    @foreach(\App\Subscription::WAGE_RANGE as $wage => $label)
                                    <option value="{{$wage}}" data-reactid="{{$wage}}" {{old('salend') == $type ? 'selected' : ''}}>{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <small class="maxrateerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Max wage must be larger than min wage</small>
                        </div>


                        <div class="hourlywapperannual hidden">
                            <div class="col-md-6">
                                <label>Min Hourly Rate</label>
                                <select class="form-control hrrateinput"  style="margin-left:-15px;" name="hrsalstart" id="salstart">
                                    @foreach(\App\Subscription::HOURLY_RATE as $rate => $label)
                                        <option value="{{$rate}}" data-reactid="{{$rate}}" {{old('salstart') === $rate ? 'selected' : ''}}>{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Max Hourly Rate</label>
                                <select class="form-control hrrateinput"  style="margin-left:-15px;" name="hrsalend" id="salend">
                                    @foreach(\App\Subscription::MAX_HOURLY_RATE as $rate => $label)
                                        <option value="{{$rate}}" data-reactid="{{$rate}}" {{old('salend') === $rate ? 'selected' : ''}}>{{$label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <small class="hrrateerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Max hourly rate must be larger than min hourly rate</small>
                        </div>
                        <div class="form-group">
                            <label>Pay Cycle</label>
                            <select name="pay_cycle[]" id="pay_cycle" class="form-control paycycleinput selectpicker" multiple title="Select Pay Cycle..." data-selected-text-format="count" data-actions-box="true">
                                <option value="weekly" {{old('pay_cycle') === 'weekly' ? 'selected' : ''}}>Weekly</option>
                                <option value="fortnightly" {{old('pay_cycle') === 'fortnightly' ? 'selected' : ''}}>Fortnightly</option>
                                <option value="monthly" {{old('pay_cycle') === 'monthly' ? 'selected' : ''}}>Monthly</option>
                            </select>
                                    <small  class="paycycleerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please confirm your preferred pay frequency.</small>
                        </div>
                        <div class="form-group"  >
                            <label>Employment Type</label>
                            <select id="employment_type" name="employment_type" class="form-control emptypeinput">
                                <option value="">Please Select</option>
                                <option value="part_time" {{old('employment_type') == 'part_time' ? 'selected': ''}}>{{trans('messages.post_employment_type_part_time')}}</option>
                                <option value="full_time" {{old('employment_type') == 'full_time' ? 'selected': ''}}>{{trans('messages.post_employment_type_full_time')}}</option>
                                <option value="casual" {{old('employment_type') == 'casual' ? 'selected': ''}}>{{trans('messages.post_employment_type_casual')}}</option>
                            </select>
                                    <small class="emptypeerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">The employment type field is required.</small>
                        </div>
                        <div class="form-group"  >
                            <label>Employment Status</label>
                            <select id="employment_term" class="form-control empterminput selectpicker" name="employment_term[]" multiple title="Select Employment Status..." data-selected-text-format="count" data-actions-box="true">
                                <option value="permanent">{{trans('messages.post_employment_permanent')}}</option>
                                <option value="fixed_term">Fixed-Term / Contract</option>
                                <option value="temp">{{trans('messages.post_employment_temp')}}</option>
                            </select>
                                    <small class="emptermerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please select your preferred Employment Status.</small>
                        </div>
                        <div class="form-group"  >
                            <label>Incentive Structure</label>
                            <select id="incentivestructure" name="incentivestructure[]" class="form-control selectpicker incinput" multiple title="Select Incentive Structure..." data-selected-text-format="count" data-actions-box="true">
                                <option value="fixed">Base + Super</option>
                                <option value="basecommbonus">Base + Super + R&R/Bonus</option>
                                <option value="basecomm">Base + Super + Commissions</option>
                                <option value="commonly">Commissions Only</option>
                            </select>
                                    <small class="incerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please select your preferred Incentive Structure.</small>
                        </div>

                        <div class="form-group">
                            <label>Work Location</label>
                            <select name="work_from_home[]" id="workfromhome" class="form-control selectpicker wlinput" data-selected-text-format="count" multiple title="Select a Work Location..." data-actions-box="true">
                                <option value="1" data-content="<img src='/Work is only at the business address.png' style='max-width:20px;'>  At business address">At business address</option>
                                <option value="2" data-content="<img src='/There is potential to work from home.png' style='max-width:20px;'>  By mutual agreement">By mutual agreement</option>
                                <option value="3" data-content="<img src='/Only display Work From Home Jobs.png' style='max-width:20px;'>  Work from home">Work from home</option>
                            </select>
                                    <small class="wlerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please select your preferred Work Location.</small>
                        </div>

                        <div class="form-group"  >
                            <label>Shift Guide (between these hours)</label>
                            <select name="parentsoption[]" id="parentsoption" class="form-control selectpicker sginput" data-selected-text-format="count" multiple title="Select Shift Time..." data-actions-box="true">
                                            <option value="1" data-content="<img src='/Standard Business Hours.png' style='max-width:20px;'>  Standard (9am-5pm Mon to Fri)">Standard (9am-5pm Mon to Fri)</option>
                                            <option value="2" data-content="<img src='/Extended Business Hours.png' style='max-width:20px;'>  Extended (8am-8pm Mon to Fri)">Extended (8am-8pm Mon to Fri)</option>
                                            <option value="3" data-content="<img src='/Parent Friendly.png' style='max-width:20px;'>  Parent Friendly (9am-3pm Mon to Fri)">Parent Friendly (9am-3pm Mon to Fri)</option>
                                            <option value="4" data-content="<img src='/Afternoon Shift.png' style='max-width:20px;'>  Afternoon (12pm-12am Mon to Fri)">Afternoon (12pm-12am Mon to Fri)</option>
                                            <option value="5" data-content="<img src='/Night Shift.png' style='max-width:20px;'>  Night Shift">Night Shift</option>
                                            <option value="6" data-content="<img src='/Rotating Shifts no weekends.png' style='max-width:20px;'>  Rotating Shifts - no weekends">Rotating Shifts - no weekends</option>
                                            <option value="7" data-content="<img src='/Rotating shifts - including weekend work.png' style='max-width:20px;'>  Rotating Shifts - including weekend work">Rotating Shifts - including weekend work</option>
                            </select>
                                    <small class="sgerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please select the shift times closest to your requirements.</small>
                        </div>

                        <div class="form-group">
                            <label for="joblocation" class="">Your location & distance preferences</label>
                            <div class="form-inline">
                                <div class="input-group col-md-12">
                                    <small>Location</small>
                                    <input id="joblocation" class="form-control geocomplete locinput" type="text" name="joblocation" placeholder="Please enter your address, suburb or state"  required value=""/>
                                    <small class="locerror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please confirm your location so we can find jobs near you.</small>
                                </div>
                                <div class="input-group col-md-12">
                                    <small>State</small>
                                        <select id="state2" name="state" class="form-control" required>
                                                <option value="">Select State</option>
                                                <option value="Australian Capital Territory">Australian Capital Territory</option>
                                                <option value="New South Wales">New South Wales</option>
                                                <option value="Northern Territory">Northern Territory</option>
                                                <option value="Queensland">Queensland</option>
                                                <option value="South Australia">South Australia</option>
                                                <option value="Tasmania">Tasmania</option>
                                                <option value="Victoria">Victoria</option>
                                                <option value="Western Australia">Western Australia</option>
                                        </select>
                                    <small class="locsterror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please confirm your state so we can find jobs near you.</small>
                                </div>
                                <div class="input-group col-md-12">
                                    <small>Radius (km)</small>
                                    <input id="radius" class="form-control radinput" placeholder="Radius.." value="{{old('radius', 25)}}" type="number" name="radius" required/>
                                    <small class="raderror" style="color: #cc0000; margin: 0;display: block; visibility: hidden;">Please confirm your radius so we can find jobs near you.</small>
                                </div>
                            </div>
                            <p class="fieldsrequired" style="color: #cc0000; margin: 0;display: block;visibility: hidden;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Please fill in the required fields!</p>
                            <input id="lat" class="form-control geocomplete" type="hidden" name="lat" />
                            <input id="lng" class="form-control geocomplete" type="hidden" name="lng" />
                            <input id="city" class="form-control geocomplete" type="hidden" name="city" />
                            <input id="postcode" class="form-control geocomplete" type="hidden" name="postcode" />
                        </div>

                        <input type="hidden" id="currStatus">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Create Alert</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</div>
@endsection