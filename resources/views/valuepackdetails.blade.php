@extends('layouts.master')
@section('extra_css')
    <!--BEGIN DATATABLE STYLES-->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/media/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="/assets/plugins/yadcf/jquery.dataTables.yadcf.css" />
    <!--END DATATABLE STYLES-->
@stop
@section('extra_js')
    <!-- BEGIN DATATABLE JS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jszip.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/assets/plugins/yadcf/jquery.dataTables.yadcf.js" type="text/javascript"></script>
    <!-- END DATATABLE JS -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Metronic.handleTables();
            $('#table').dataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        })
    </script>
@stop
<style>

    div#table_filter {
        float: right;
    }

    .pagination>li>a, .pagination>li>span {
        line-height: 1!important;
    }
    th{
        font-size: 13px;
        font-weight: 600;
        text-align: -webkit-center !important;
        border-right: 1px solid #ddd;
        border-top: 1px solid #ddd !important;
    }
    td{
        text-align: -webkit-center;
        font-size: 14px;
        font-weight: 400;
        border-right: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }
    

</style>
@section('content')

<div class="bg-color1">
    <div class="container">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="bg-color2 block-section-xs line-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">

                    </div>
                    <div class="col-sm-6">
                        <div class="text-right"><a href="/poster/transaction">&laquo; Go back</a></div>
                    </div>
                </div>
            </div>
        </div><!-- end link top -->

        <div class="col-md-12 col-sm-9">
            <div class="block-section">
            <?php setlocale(LC_MONETARY, 'en_US.UTF-8'); ?>
                @if(sizeof($ads)>0)
                <div class="col-xs-6">
                <h3 class="no-margin-top">Value Pack Details</h3>
                </div>
                @else
                <div class="col-xs-6">
                <h3 class="no-margin-top">No Value Pack Purchased</h3>
                </div>
                @endif
                <div class="col-xs-6 text-right">
                <a href="/poster/transaction" class="btn btn-info" style=" color: white; border:0;">Transaction History</a>    
                @if(Auth::user()->credits == 0)
                <a href="/poster/buy_credits" class="btn btn-info" style="background-color:#639941; color: white; border:0;">Buy More Credits</a>
                @endif
                </div>
            </div>
                <hr/>
                <div class="table-responsive table-striped table-hover">
                    <table class="table" id="table">
                        <thead>
                        <tr>
                            <th style="border-left: 1px solid #ddd;">Value Pack</th>
                            <th>Total Price</th>
                            <th>Payment Type</th>
                            <th>Payment Status</th>
                            <th>Credits Given</th>
                            <th>Credits Remaining</th>
                            <th>Credits Expired</th>
                            <th>Date of Purchase</th>
                            <th>Time of Purchase</th>
                            <th>Invoice Due Date/Time</th>
                            <th>Days/Hours until invoice is Due</th>
                            <th>Date Paid</th>
                            <th>Credits Expiry Date</th>
                            <th>Days left</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($ads as $ad)
                                <tr>
                                    <td style="border-left:1px solid #ddd;">
                                        {{$ad->name}}
                                    </td>
                                    <td>
                                        {{money_format('%.2n',$ad->price)}}
                                    </td>
                                    <td>
                                        {{$ad->payment}}
                                    </td>
                                    <td>
                                        @if($ad->payment == 'Invoice')
                                            @if($ad->status == 'Paid')
                                               Paid
                                            @elseif($ad->status == 'Overdue')
                                               Overdue
                                            @else
                                               Outstanding
                                            @endif

                                        @else
                                            Paid
                                        @endif
                                    </td>
                                    <td>
                                        {{money_format('%.2n',$ad->creditsgiven)}}
                                    </td>
                                    <td>
                                        {{money_format('%.2n',$ad->remaining)}}
                                    </td>
                                    <td>
                                        @if($ad->expired)
                                        -{{money_format('%.2n',$ad->expired)}}
                                        @else
                                        nil
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                            $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->purchase_date);
                                            $print = $date->format('d M Y');
                                         ?>
                                         {{$print}}
                                    </td>
                                    <td>
                                        <?php
                                            $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->purchase_date);
                                            $print = $date->format('H:i:s');
                                         ?>
                                         {{$print}}
                                    </td>
                                    <td>
                                      @if($ad->payment == 'Invoice')
                                        @if($ad->invoice_terms)
                                        <?php
                                            $start = new \Carbon\Carbon($ad->purchase_date);
                                            $date_process = $start->addDays($ad->invoice_terms);
                                         ?>
                                        {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                        @elseif($current_term && $current_term->invoice_terms)
                                        <?php
                                            $start = new \Carbon\Carbon($ad->purchase_date);
                                            $date_process = $start->addDays($current_term->invoice_terms);
                                         ?>
                                        {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                        @elseif($fixed_term && $fixed_term->value_string) 
                                        <?php
                                            $start = new \Carbon\Carbon($ad->purchase_date);
                                            $date_process = $start->addDays($fixed_term->value_string);
                                         ?>
                                        {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                        @else
                                        <?php
                                            $start = new \Carbon\Carbon($ad->purchase_date);
                                            $date_process = $start->addDays(7);
                                         ?>
                                        {{\Carbon\Carbon::parse($date_process)->format('d M Y H:i:s')}}
                                        @endif
                                       @else
                                         nil
                                      @endif
                                    </td>
                                    <td>
                                      @if($ad->payment == 'Invoice' && $ad->status != 'Paid')
                                        @if($ad->invoice_terms)
                                        <?php
                                            $start = new \Carbon\Carbon($ad->purchase_date);
                                            $date_process = $start->addDays($ad->invoice_terms);
                                            $remaining = trim($date_process->copy()->diffForHumans(\Carbon\Carbon::now()), "after");
                                            if(\Carbon\Carbon::now() > $date_process){
                                                $remaining = '0 days';
                                            }
                                         ?>
                                            @if($date_process > \Carbon\Carbon::now())
                                                {{$remaining}}
                                            @else
                                                0 days
                                            @endif
                                        @endif
                                       @else
                                         nil
                                      @endif
                                    </td>
                                    <td>
                                         @if($ad->payment == 'Invoice')
                                            @if($ad->payment_time)
                                                <?php
                                                    $payment_time = \Carbon\Carbon::createFromFormat('d/m/Y h:i A',$ad->payment_time);
                                                    $print_time = $payment_time->format('d M Y H:i:s');
                                                 ?>
                                                 {{$print_time}}
                                            @else
                                              nil 
                                            @endif
                                        @else
                                            {{\Carbon\Carbon::parse($ad->purchase_date)->format('d M Y H:i:s')}}
                                        @endif
                                    </td>

                                    <td>
                                        <?php
                                            $date2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$ad->expiry_date);
                                            $print2 = $date2->format('d M Y');
                                         ?>
                                         {{$print2}}
                                    </td>
                                    <td>
                                        <?php
                                             $end = \Carbon\Carbon::now()->toDateString();
                                             $expires = new \Carbon\Carbon($ad->expiry_date);
                                             $date = $expires->toFormattedDateString();
                                             $now = \Carbon\Carbon::now();
                                             $difference = ($expires->diff($now)->days);
                                             if(\Carbon\Carbon::now() > $ad->expiry_date){
                                                    $difference = 0;
                                                 }
                                         ?>
                                         {{$difference}} days
                                    </td>
                                    <td>
                                       @if($ad->expired)
                                       <strong>Expired</strong>
                                       @else
                                       <strong>Active</strong>
                                       @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>

@endsection