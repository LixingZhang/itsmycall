@extends('layouts.master')
@section('extra_css')
    <!--BEGIN DATATABLE STYLES-->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Scroller/css/scroller.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/media/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"
          href="/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="/assets/plugins/yadcf/jquery.dataTables.yadcf.css" />
    <!--END DATATABLE STYLES-->
@stop
@section('extra_js')
    <!-- BEGIN DATATABLE JS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript"
            src="/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jszip.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="/assets/plugins/yadcf/jquery.dataTables.yadcf.js" type="text/javascript"></script>
    <!-- END DATATABLE JS -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Metronic.handleTables();
            $('#table').dataTable({
                ordering: false,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            }).yadcf([
                {
                    column_number: 0,
                    filter_default_label: 'Select Job',
                    filter_match_mode: 'exact'

                }
            ]);
        })
    </script>
@stop
<style>

    div#table_filter {
        float: right;
    }

    .pagination>li>a, .pagination>li>span {
        line-height: 1!important;
    }
    .fa {
            line-height: 2.2!important;
    }
    

</style>
@section('content')

<div class="bg-color1">
    <div class="container">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <div class="bg-color2 block-section-xs line-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">

                    </div>
                    <div class="col-sm-6">
                        <div class="text-right"><a href="/poster/applicants">&laquo; Go back</a></div>
                    </div>
                </div>
            </div>
        </div><!-- end link top -->

        <div class="col-md-12 col-sm-9">
            <div class="block-section">
                @if(sizeof($pastemails)>0)
                <h3 class="no-margin-top">Sent Emails to @foreach($applicants as $applicant)  @if($applicant->id == $id) {{$applicant->email}} @endif @endforeach </h3>

                @else
                <h3 class="no-margin-top">No Emails sent</h3>
                @endif

                <hr/>
                 @include('admin.layouts.notify')
                <div class="table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Job Title</th>
                                <th>Candidate Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Template</th>
                                <th>Your Personal Feedback</th>
                                <th>Resend Email</th>
                                <th>View Email</th>
                            </tr>
                        </thead>
                        <tbody>
<?php $count = 0; ?>
                            @foreach($pastemails as $email)
                            <tr>
                                <th scope="row">@foreach($posts as $post) @if($post->id == $email->post_id) {{$post->title}} @endif @endforeach</th>
                                <td> @foreach($applicants as $applicant) @if($applicant->id == $email->applicant_id) {{$applicant->name}} @endif @endforeach </td>
                                <?php  $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $email->created_at);
                                    $print = $date->format('d M Y');
                                    $time = $date->format("H:i A");
                                     ?>
                                <td>@if($print)
                                        {{$print}} 
                                    @endif</td>
                                <td>@if($time)
                                        {{$time}} 
                                    @endif</td>
                                <td>@foreach($categories as $category) @if($category->id == $email->category_id) {{$category->name}} @endif @endforeach</td>
                                <td> {!!$email->feedback!!} </td> 
                                <td><a href="/poster/resendmail/{{$email->id}}" class="btn btn-theme btn-xs btn-default">Resend Email</a></td>
                                <td><a href="/poster/resendmailpreview/{{$email->id}}" class="btn btn-theme btn-xs btn-default">View Sent Email</a></td>
                            </tr>
<?php $count++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <!-- pagination -->
                <nav >
                    <ul class="pagination pagination-theme no-margin pull-right  ">
                        {!! $pastemails->render() !!}

                    </ul>
                </nav><!-- pagination -->

            </div>
        </div>

    </div>
</div>
</div>

@endsection