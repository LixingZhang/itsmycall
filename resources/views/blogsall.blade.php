@extends('layouts.master')
@section('extra_js')
@if($keywords)
<meta name="keywords" content="{{$keywords}}">
@endif
@if($seodescription)
<meta name="description" content="{{$seodescription}}">
@endif
@stop
@section('content')
<div class="bg-color2">
    <div class="container">


        <div class="row">
                <?php $blog_text = \App\Settings::where('column_key','blog_text')->first(); ?>
                @if($blog_text) {!!$blog_text->value_txt!!} @endif
                <div class="panel panel-default" style="margin-bottom: 45px">
                    <div class="panel-heading text-center" style="font-size: 22px;">Browse By Category</div>
                    <div class="panel-body">
                        <div class="col-md-12" style="padding-top: 25px;">
                            <div class="row multi-columns-row">

                                <!--<div class="panel-group" id="accordion">-->
                                @foreach($categories as $category)
                                <div class="col-xs-12 col-sm-4 col-lg-4">    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <?php $countcatgeory = \DB::table('blogs')->where('parent_category',$category->id)->count(); ?>
                                                <a data-toggle="collapse" class="collapsed smaller-mobile" data-parent="#accordion" href="#collapse{{$category->id}}">{{$category->title}} <span id="{{$category->title}}" class="badge pull-right jcount" style="background-color: #639941;">{{$countcatgeory}}</span></a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{$category->id}}" class="panel-collapse collapse">
                                            <ul class="list-group">
                                                <?php $subcategories = \DB::table('blogsubcategories')->where('parent_category',$category->id)->get(); ?>
                                                @foreach($subcategories as $subcategory)
                                                <?php $countsubcatgeory = \DB::table('blogs')->where('sub_category',$subcategory->id)->count(); ?>
                                                <li class="list-group-item">
                                                    <a href="/blogs/{{$category->slug}}/{{$subcategory->slug}}" >{{$subcategory->title}}</a><span id="{{$subcategory->title}}" class="badge subcount">{{$countsubcatgeory}}</span>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                                <!--</div>-->
                            </div>

                        </div>
                    </div>

                </div>
        </div>
    </div>
</div>
@endsection