@extends('layouts.master')


@section('content')
<style>
    a {
        text-decoration: none!important;
    }
</style>
<div class="container min-hight" style="padding-top:20px">
    <div class="row" >
        <div class="col-md-8  col-xs-12 col-md-offset-2  text-center">
            <?php $connecttext =  \App\Settings::where('column_key','connect_program')->first(); ?>
            @if($connecttext) {!!$connecttext->value_txt!!} @endif 
        </div>
        <h2></h2>
    </div>
    <div class="row" >
        <div class="col-md-12 col-sm-12"  style="padding-bottom:20px">
            <div class="col-md-4 @if(Auth::user()->profile_type != 'standard' && Auth::user()->profile_type != 'maximum') col-md-offset-2 @endif col-xs-12 text-center">
                <div class="panel panel-default" style="padding:25px">
                    <a href="/customer/connect/basic-profile">
                        @if (Auth::user()->profile_type == 'standard')
                           <p style="color:orange"><i class="fa fa-check green-icon faa-pulse" aria-hidden="true" style="color:orange"></i> Your selected profile <br> Click to edit</p> 
                        @else
                        <div style="min-height:57px">
                            <i class="fa fa-user fa-3x green-icon faa-pulse" aria-hidden="true"></i> 
                        </div>
                        @endif
                        <h2 class="text-center">Basic Profile</h2>
                        <p>Employers can view:
                        <ol class="text-left">
                            <li>Your name</li>
                            <li>Your location</li>
                            <li>Current position</li>
                            <li>Length of time in position</li>
                            <li>Your contact details (email and phone number)</li>
                            <li>Location details (address, suburb or state)</li>
                            <li>Availability</li>
                        </ol>
                        </p>	
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 text-center">
                <div class="panel panel-default" style="padding:25px!important">
                    <a href="/customer/connect/advanced-profile">
                        @if (Auth::user()->profile_type == 'maximum')
                        <p style="color:orange"><i class="fa fa-check green-icon faa-pulse" aria-hidden="true"></i> Your selected profile <br> Click to edit</p> 
                        @else
                        <div style="min-height:57px">
                            <i class="fa fa-user fa-3x green-icon faa-pulse" aria-hidden="true"></i> 
                        </div>
                        @endif
                        <h2 class="text-center">Advanced Profile</h2>
                        <p>Employers can view:
                        <ol class="text-left">
                            <li>Your name</li>
                            <li>Your location</li>
                            <li>Current position</li>
                            <li>Length of time in position</li>
                            <li>Your contact details (email and phone number)</li>
                            <li>Location details (address, suburb or state)</li>
                            <li>Availability</li>
                            <li>Your ambitions brief</li>
                            <li>Indicative salary expectations</li>
                            <li>Your CV</li>
                        </ol>
                        </p>		
                    </a>
                </div>
            </div>
            @if (Auth::user()->profile_type == 'standard' || Auth::user()->profile_type == 'maximum')
            <div class="col-md-4 col-xs-12 text-center">
                <div class="panel panel-default" style="padding:25px!important">
                    <a href="/customer/inbound/del">
                        <div style="min-height:57px">
                            <i class="fa fa-times fa-4x green-icon faa-pulse" aria-hidden="true"></i> 
                        </div>
                        <h2 class="text-center">Opt Out</h2>
                    </a>
                </div>
            </div>

            @endif

        </div>
    </div>
</div>

@endsection