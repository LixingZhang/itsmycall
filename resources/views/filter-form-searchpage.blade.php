<script>
    function toggler() {

        $('#filts').toggle("slow");

    }
</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
          style: 'btn-default',
        });
    });

</script>
<style type="text/css">
    h5{
        font-weight: 700;
        font-size: 13px;
    }
    .row-bor{
        border-right: 0px;
        min-height: ;
    }
    @media screen and (min-width: 995px) {
    .row-bor{
        border-right: 2px solid lightgray;
        min-height: 520px;
    }
    }
</style>
<?php 
$business_info = \App\Settings::where('column_key','business_info')->first();
$home_info = \App\Settings::where('column_key','home_info')->first();
$agreement_info = \App\Settings::where('column_key','agreement_info')->first();


$standard_info = \App\Settings::where('column_key','standard_info')->first();
$exten_info = \App\Settings::where('column_key','exten_info')->first();
$parent_info = \App\Settings::where('column_key','parent_info')->first();
$after_info = \App\Settings::where('column_key','after_info')->first();
$night_info = \App\Settings::where('column_key','night_info')->first();
$rotno_info = \App\Settings::where('column_key','rotno_info')->first();
$rotyes_info = \App\Settings::where('column_key','rotyes_info')->first();

$business_info_img = \App\Settings::where('column_key','business_info_img')->first();
$home_info_img = \App\Settings::where('column_key','home_info_img')->first();
$agreement_info_img = \App\Settings::where('column_key','agreement_info_img')->first();

$standard_info_img = \App\Settings::where('column_key','standard_info_img')->first();
$exten_info_img = \App\Settings::where('column_key','exten_info_img')->first();
$parent_info_img = \App\Settings::where('column_key','parent_info_img')->first();
$after_info_img = \App\Settings::where('column_key','after_info_img')->first();
$night_info_img = \App\Settings::where('column_key','night_info_img')->first();
$rotno_info_img = \App\Settings::where('column_key','rotno_info_img')->first();
$rotyes_info_img = \App\Settings::where('column_key','rotyes_info_img')->first();

?>
<div class="panel panel-default">
    <div class="panel-heading" style="text-align: center;"><span style="font-weight: 500; font-size: 25px; color: gray;">@if(!empty($filtercount)) Filters Applied ({{$filtercount}}) @else Search for Jobs  @endif</span> <span onclick="toggler()" class="pull-right"> Expand <i class="fa fa-chevron-down" aria-hidden="true"></i></span></div>
    <div class="panel-body" id="filts" style="display:none">

        <div class="result-filter">
            <form action="/job_list" id="formsub" method="GET" role="form">
                <input id="sort" class="form-control" name="sort" type="hidden" value="{{isset($filter['sort']) ? $filter['sort'] : ''}}"/>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="row row-bor">
                                <div class="col-md-12">
                                    <H3 style="padding-bottom: 20px;">Job Details</H3>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="font-bold  margin-b-20"><a href="#s_collapse_2" data-toggle="collapse">Job Category</a></h5>
                                    <div class="form-group ">
                                        <select class="form-control category" name="category" id="category" value="{{isset($filter['category']) ? $filter['category'] : ''}}">
                                            @foreach($categories as $category)
                                            @if (isset($filter['category']))
                                            <option @if($category->id == $filter['category']) selected=selected @endif value="{{$category->id}}" >{{$category->title}}</option>
                                            @else 
                                            <option value="{{$category->id}}" >{{$category->title}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <div class="col-md-12 text-center" style="padding-bottom: 15px;">
                                        <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <select class="form-control subcategory" margin-top:15px;" name="subcategory" id="subcategory" value="{{isset($filter['subcategory']) ? $filter['subcategory'] : ''}}">
                                            @foreach($categories as $category)
                                            <option value="-1">All Sub Categories</option>
                                            @foreach($category->subcategory as $subcategory)
                                            @if (isset($filter['category']))
                                            <option @if($subcategory->id == $filter['subcategory']) selected=selected @endif value="{{$subcategory->title}}">{{$subcategory->title}}</option>
                                            @else 
                                            <option value="{{$subcategory->id}}" >{{$subcategory->title}}</option>
                                            @endif
                                            @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-6">
                                <h5 class="margin-b-20">
                                    <a href="#s_collapse_2" data-toggle="collapse">{{trans('messages.post_employment_type')}}</a>
                                </h5>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="employment_type[]"
                                               value="{{\App\Posts::EMPLOYMENT_TYPE_PART_TIME}}"
                                               @if (isset($filter['employment_type']) AND in_array(\App\Posts::EMPLOYMENT_TYPE_PART_TIME, $filter['employment_type'])) checked @endif
                                               >
                                               {{trans('messages.post_employment_type_part_time')}}
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="employment_type[]"
                                               value="{{\App\Posts::EMPLOYMENT_TYPE_FULL_TIME}}"
                                               @if (isset($filter['employment_type']) AND in_array(\App\Posts::EMPLOYMENT_TYPE_FULL_TIME, $filter['employment_type'])) checked @endif
                                               >
                                               {{trans('messages.post_employment_type_full_time')}}
                                    </label>
                                </div>
                                </div>

                                <div class="col-md-6">
                                <h5 class="margin-b-20">
                                    <a href="#s_collapse_3" data-toggle="collapse">{{trans('messages.post_employment_term')}}</a>
                                </h5>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="employment_term[]"
                                               value="{{\App\Posts::EMPLOYMENT_TERM_PERMANENT}}"
                                               @if (isset($filter['employment_term']) AND in_array(\App\Posts::EMPLOYMENT_TERM_PERMANENT, $filter['employment_term'])) checked @endif
                                               >
                                               {{trans('messages.post_employment_permanent')}}
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="employment_term[]"
                                               value="{{\App\Posts::EMPLOYMENT_FIXED_TERM}}"
                                               @if (isset($filter['employment_term']) AND in_array(\App\Posts::EMPLOYMENT_FIXED_TERM, $filter['employment_term'])) checked @endif
                                               >
                                               Fixed-Term / Contract
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"
                                               name="employment_term[]"
                                               value="{{\App\Posts::EMPLOYMENT_TEMP}}"
                                               @if (isset($filter['employment_term']) AND in_array(\App\Posts::EMPLOYMENT_TEMP, $filter['employment_term'])) checked @endif
                                               >
                                               {{trans('messages.post_employment_temp')}}
                                    </label>
                                </div>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="font-bold  margin-b-20">
                                        <a href="#s_collapse_2" data-toggle="collapse">Job Keywords</a>
                                    </h5>
                                    <div class="form-group">
                                        <input type="text" name="title" class="form-control" placeholder="Keywords" value="{{isset($filter['title']) ? $filter['title'] : ''}}">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="row row-bor">
                            <div class="col-md-12">
                                <H3 style="padding-bottom: 20px;">Job Specifics</H3>
                            </div>
                            <div class="col-md-12">
                                <h5 class="font-bold  margin-b-20"><a href="#s_collapse_2" data-toggle="collapse">Job Location</a></h5>
                                <div class="form-group">
                                    <div class="form-group" style="display:none">
                                        <select id="country1" name="country" class="form-control"></select>
                                        <i style="color: #078d00;margin-top: 15px;" class="fa fa-long-arrow-down fa-lg"></i>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 5px;">
                                        <input  id="joblocation" value="{{isset($filter['location']) ? $filter['location'] : ''}}" class="form-control geocomplete" type="text" name="joblocation" placeholder="Enter an address, suburb or state" />
                                        <input id="lat" class="form-control geocomplete" value="{{isset($filter['lat']) ? $filter['lat'] : ''}}" type="hidden" name="lat" />
                                        <input id="lng" class="form-control geocomplete" value="{{isset($filter['lng']) ? $filter['lng'] : ''}}" type="hidden" name="lng" />
                                        <input id="city" class="form-control geocomplete" value="{{isset($filter['city']) ? $filter['city'] : ''}}" type="hidden" name="city" />
                                        <input id="postcode" class="form-control geocomplete" value="{{isset($filter['postcode']) ? $filter['postcode'] : ''}}" type="hidden" name="postcode" />
                                    </div>
                                    <div class="col-md-12 text-center" style="padding-bottom: 5px;">
                                        <h5>OR</h5>
                                    </div>
                                    <div class="form-group">
                                        <select id="state2"
                                                name="state" class="form-control">
                                            <option value="">Select State</option>
                                            <option value="Australian Capital Territory">Australian
                                                Capital Territory
                                            </option>
                                            <option value="New South Wales">New South Wales</option>
                                            <option value="Northern Territory">Northern Territory
                                            </option>
                                            <option value="Queensland">Queensland</option>
                                            <option value="South Australia">South Australia</option>
                                            <option value="Tasmania">Tasmania</option>
                                            <option value="Victoria">Victoria</option>
                                            <option value="Western Australia">Western Australia</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                                <div class="col-md-12">
                                    <h5 class="font-bold ">Salary</h5>
                                </div>
                                <div class="salrwapperannual">
                                <div class="col-md-6">
                                    <small>Min</small>
                                    <select class="form-control" name="salstart" id="salstart">
                                        <option selected="selected" value="0" data-reactid="$0">$0</option>
                                        <option value="30000" data-reactid="$30000">$30k</option>
                                        <option value="40000" data-reactid="$40000">$40k</option>
                                        <option value="50000" data-reactid="$50000">$50k</option>
                                        <option value="60000" data-reactid="$60000">$60k</option>
                                        <option value="70000" data-reactid="$70000">$70k</option>
                                        <option value="80000" data-reactid="$80000">$80k</option>
                                        <option value="100000" data-reactid="$100000">$100k</option>
                                        <option value="120000" data-reactid="$120000">$120k</option>
                                        <option value="150000" data-reactid="$150000">$150k</option>
                                        <option value="175000" data-reactid="$175000">$175k</option>
                                        <option value="200000" data-reactid="$200000">$200k</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <small>Max</small>
                                    <select class="form-control" name="salend" id="salend">
                                        <option value="0" data-reactid="$0">$0</option>
                                        <option value="30000" data-reactid="$30000">$30k</option>
                                        <option value="40000" data-reactid="$40000">$40k</option>
                                        <option value="50000" data-reactid="$50000">$50k</option>
                                        <option value="60000" data-reactid="$60000">$60k</option>
                                        <option value="70000" data-reactid="$70000">$70k</option>
                                        <option value="80000" data-reactid="$80000">$80k</option>
                                        <option value="100000" data-reactid="$100000">$100k</option>
                                        <option value="120000" data-reactid="$120000">$120k</option>
                                        <option value="150000" data-reactid="$150000">$150k</option>
                                        <option value="500000" selected="selected" data-reactid="$250000">$200k+</option>
                                    </select>
                                </div>
                            </div>

                            <div class="hourlywapperannual hidden">
                                <div class="col-md-6">
                                    <small>Min Hourly Rate</small>
                                    <select class="form-control"  name="hrsalstart" id="salstart">
                                        <option selected="selected" value="0" data-reactid="$0">$0</option>
                                        <option value="15" data-reactid="15"">$15</option>
                                        <option value="20" data-reactid="25">$20</option>
                                        <option value="25" data-reactid="35">$25</option>
                                        <option value="30" data-reactid="40">$30</option>
                                        <option value="35" data-reactid="50">$35</option>
                                        <option value="40" data-reactid="60">$40</option>
                                        <option value="50" data-reactid="70+">$50</option>
                                        <option value="60" data-reactid="70+">$60</option>
                                        <option value="70" data-reactid="70+">$80</option>
                                        <option value="100" data-reactid="70+">$100</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <small>Max Hourly Rate</small>
                                    <select class="form-control"  name="hrsalend" id="salend">
                                        <option value="0" data-reactid="$0">$0</option>
                                        <option value="15" data-reactid="15"">$15</option>
                                        <option value="20" data-reactid="25">$20</option>
                                        <option value="25" data-reactid="35">$25</option>
                                        <option value="30" data-reactid="40">$30</option>
                                        <option value="35" data-reactid="50">$35</option>
                                        <option value="40" data-reactid="60">$40</option>
                                        <option value="50" data-reactid="70+">$50</option>
                                        <option value="60" data-reactid="70+">$60</option>
                                        <option value="70" data-reactid="70+">$80</option>
                                        <option value="100" data-reactid="70+">$100</option>
                                        <option value="1000" data-reactid="70+" selected="selected">$100+</option>
                                    </select>
                                </div>
                            </div> 
                                <div class="col-md-12 text-center" style="padding-top: 20px;">
                                    <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="searchour" id="searchour" value="yes">
                                    Switch to Hourly Rates
                                </label>
                            </div>
                                </div>
                                    <div class="col-md-12">
                                    <h5 class="font-bold  margin-b-20" style="margin-top: 15px;">
                                        <a href="#s_collapse_2" data-toggle="collapse">Incentive Structure</a>
                                    </h5>
                                    <div class="form-group">
                                        <select id="incentivestructure" name="incentivestructure1" class="form-control">
                                            <option value="">Any</option>
                                            <option value="fixed" @if(Input::get('incentivestructure1') == 'fixed') selected="selected" @endif >Base + Super</option>
                                            <option value="basecommbonus" @if(Input::get('incentivestructure1') == 'basecommbonus') selected="selected" @endif >Base + Super + R&R/Bonus</option>
                                            <option value="basecomm" @if(Input::get('incentivestructure1') == 'basecomm') selected="selected" @endif >Base + Super + Commissions</option>
                                            <option value="commonly" @if(Input::get('incentivestructure1') == 'commonly') selected="selected" @endif >Commissions Only</option>
                                        </select>
                                    </div>
                                    </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                            <div class="row row-bor" style="border-right: 0px;">
                                <div class="col-md-12">
                                    <H3 style="padding-bottom: 20px;">Job Preferences</H3>
                                </div>
                                    <div class="col-md-12">
                                    <h5 class="font-bold" style="margin-bottom: 18px;">
                                        <a href="#s_collapse_2" data-toggle="collapse">Shift Guide (between these hours)</a>
                                    </h5>
                                    <div class="form-group" style="margin-bottom: 10px;">
                                        <select id="parentsfiltervalue" name="parentsoption" class="form-control selectpicker">
                                            <option value=""> I'm open to any shift time</option>
                                            <option value="1" @if(Input::get('parentsoption') == 1) selected="selected" @endif data-content="<img src='{{$standard_info_img->value_string}}' style='max-width:25px;'>  Standard (9am-5pm Mon to Fri)">Standard (9am-5pm Mon to Fri)</option>
                                            <option value="2" @if(Input::get('parentsoption') == 2) selected="selected" @endif data-content="<img src='{{$exten_info_img->value_string}}' style='max-width:25px;'>  Extended (8am-8pm Mon to Fri)">Extended (8am-8pm Mon to Fri)</option>
                                            <option value="3" @if(Input::get('parentsoption') == 3) selected="selected" @endif data-content="<img src='{{$parent_info_img->value_string}}' style='max-width:25px;'>  Parent Friendly (9am-3pm Mon to Fri)">Parent Friendly (9am-3pm Mon to Fri)</option>
                                            <option value="4" @if(Input::get('parentsoption') == 4) selected="selected" @endif data-content="<img src='{{$after_info_img->value_string}}' style='max-width:25px;'>  Afternoon (12pm-12am Mon to Fri)">Afternoon (12pm-12am Mon to Fri)</option>
                                            <option value="5" @if(Input::get('parentsoption') == 5) selected="selected" @endif data-content="<img src='{{$night_info_img->value_string}}' style='max-width:25px;'>  Night Shift">Night Shift</option>
                                            <option value="6" @if(Input::get('parentsoption') == 6) selected="selected" @endif data-content="<img src='{{$rotno_info_img->value_string}}' style='max-width:25px;'>  Rotating Shifts - no weekends">Rotating Shifts - no weekends</option>
                                            <option value="7" @if(Input::get('parentsoption') == 7) selected="selected" @endif data-content="<img src='{{$rotyes_info_img->value_string}}' style='max-width:25px;'>  Rotating Shifts - including weekend work">Rotating Shifts - including weekend work</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                    <h5 class="font-bold " >
                                        <a href="#s_collapse_2" data-toggle="collapse">Work Location</a>
                                    </h5>
                                    <div class="form-group">
                                        <select id="workfromhome" name="work_from_home" class="form-control selectpicker">
                                            <option value="">Happy to work anywhere</option>
                                            <option value="1" @if(Input::get('work_from_home') == 1) selected="selected" @endif data-content="<img src='{{$business_info_img->value_string}}' style='max-width:25px;'>  At business address">At business address</option>
                                            <option value="2" @if(Input::get('work_from_home') == 2) selected="selected" @endif data-content="<img src='{{$agreement_info_img->value_string}}' style='max-width:25px;'>  By mutual agreement">By mutual agreement</option>
                                            <option value="3" @if(Input::get('work_from_home') == 3) selected="selected" @endif data-content="<img src='{{$home_info_img->value_string}}' style='max-width:25px;'>  Work from home">Work from home</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                    <h5 class="font-bold" >
                                        <a href="#s_collapse_2" data-toggle="collapse">Pay Frequency</a>
                                    </h5>
                                    <div class="form-group">
                                        <select id="pay_cycle" class="form-control selectpicker" name="pay_cycle">
                                            <option value="">Dont care, as long as I get paid!</option>
                                            <option value="weekly" @if(Input::get('paycycle') == 'weekly') selected="selected" @endif>Weekly</option>
                                            <option value="fortnightly" @if(Input::get('paycycle') == 'fortnightly') selected="selected" @endif>Fortnightly</option>
                                            <option value="monthly" @if(Input::get('paycycle') == 'monthly') selected="selected" @endif>Monthly</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <h5 class="font-bold ">
                                            <a href="" data-toggle="collapse">Job Advertiser Type</a>
                                        </h5>
                                        <select class="form-control" name="advertiser_type">
                                            <option value="both">Both</option>
                                            @foreach(\App\Users::getAdvertiserTypes() as $type => $label)
                                                <option value="{{$type}}">{{$label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                            </div>
                    </div>


                </div>


                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                <div class="col-md-12" style="padding-bottom: 15px;padding-top: 30px;">
                    <div class="col-md-6 col-md-offset-3">
                    <button type="submit" class="btn btn-primary btn-block" style="font-weight: bold; font-size: 25px; height: auto;" ><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

{{--
<div class="panel panel-default">
  <div class="panel-heading">Browse By Category</div>
  <div class="panel-body">
	<div class="col-md-12">
		<div class="row multi-columns-row">
			@foreach($categories as $category)
			<div class="col-xs-12 col-sm-4 col-lg-4">

				<ul class="list-group" data-toggle="collapse" data-target="#main_cat_{{$category->id}}">
<li class="list-group-item"><i class="fa fa-caret-down fa-lg"></i><a href="/category_filter/{{$category->id}}"> {{$category->title}}</a><span id = "{{$category->title}}" class="badge pull-right jcount">{{$category->count}}</span>
    @if(sizeof($category->subcategory)>0) 
    <ul style="position: absolute;width: 100%;margin-left: -15px;background:#F5F3F3;z-index: 1000;border:1px solid #cecece"
        id="main_cat_{{$category->id}}" class="collapse out">
        @foreach($category->subcategory as $subcategory)
        <li>
            <a href="/sub_category_filter/{{$subcategory->id}}"> {{$subcategory->title}} </a><span id="{{$subcategory->title}}" class="badge subcount">{{$subcategory->count}}</span>
        </li>
        @endforeach
    </ul>
    @endif
</li>
</ul>
</div>
@endforeach
</div>
</div>
</div>
</div>
--}}
<script>
    $('#searchour').on('change', function () {
        if ($('#searchour').is(':checked')) {
            $('.salrwapperannual').addClass("hidden")
            $('.hourlywapperannual').removeClass("hidden")
        } else {
            $('.hourlywapperannual').addClass("hidden")
            $('.salrwapperannual').removeClass("hidden")
        }
    });
</script>