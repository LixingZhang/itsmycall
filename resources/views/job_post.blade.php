@extends('layouts.master')

@section('extra_css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">

    <link rel="stylesheet" href="/css/lightbox.min.css">
    <link rel="stylesheet" href="/css/tooltipster.bundle.min.css">
    <link rel="stylesheet" href="/css/tooltipster-sideTip-shadow.min.css">
    <link rel="stylesheet" href="/css/tooltipster-sideTip-light.min.css">
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link href="/css/card-js.min.css" rel="stylesheet" type="text/css" />
<style>
    .com { color: #93a1a1; }
    .lit { color: #195f91; }
    .pun, .opn, .clo { color: #93a1a1; }
    .fun { color: #dc322f; }
    .str, .atv { color: #D14; }
    .kwd, .prettyprint .tag { color: #1e347b; }
    .typ, .atn, .dec, .var { color: teal; }
    .pln { color: #48484c; }

    .prettyprint {
        padding: 8px;
        background-color: #f7f7f9;
        border: 1px solid #e1e1e8;
    }
    .prettyprint.linenums {
        -webkit-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
        -moz-box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
        box-shadow: inset 40px 0 0 #fbfbfc, inset 41px 0 0 #ececf0;
    }

    /* Specify class=linenums on a pre to get line numbering */
    ol.linenums {
        margin: 0 0 0 33px; /* IE indents via margin-left */
    }
    ol.linenums li {
        padding-left: 12px;
        color: #bebec5;
        line-height: 20px;
        text-shadow: 0 1px 0 #fff;
    }
    input[type=radio] { display:none; } /* to hide the checkbox itself */
    input[type=radio] + label:before {
        font-family: FontAwesome;
        display: inline-block;
    }


    input[type=radio],
    input[type=checkbox] {
        border: 0;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }

    input[type=radio] ~ label:before,
    input[type=checkbox] ~ label:before {
        font-family: FontAwesome;
        display: inline-block;
        content: "\f1db";
        letter-spacing: 10px;
        font-size: 23px;
        color: #FFF;
        width: 1.4em;
    }

    input[type=radio]:checked ~ label:before,
    input[type=checkbox]:checked ~ label:before  {
        content: "\f00c";
        font-size: 23px;
        color: white;
        letter-spacing: 5px;
    }
    input[type=checkbox] ~ label:before {        
        content: "\f096";
    }
    input[type=checkbox]:checked ~ label:before {
        content: "\f046";        
        color: white;
    }
    input[type=radio]:focus ~ label:before,
    input[type=checkbox]:focus ~ label:before,
    input[type=radio]:focus ~ label,
    input[type=checkbox]:focus ~ label
    {                
        color: white;
    }
    label.required:after {
        content:"*";
        color:red;
    }
    .required-empty,
    .required-empty:focus {
        background-color: #FFC0BF !important;
    }


    .error,
    .error:focus {
        background-color: #FFC0BF !important;
    }
</style>
@stop

@section('extra_js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

<style>
    .pager li>a:focus, .pager li>a:hover {
    text-decoration: none;
    background-color: #ff7200;
}
    </style>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('.modal').hide();
        $('#rootwizard').bootstrapWizard({
            onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#rootwizard .progress-bar').css({width: $percent + '%'});
            }
        });
        $("#inbtn").click();
    });


    $(document).ready(function () {
        $('#summernote').summernote({
          height: 400,  //set editable area's height
          disableDragAndDrop: true,
          toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
          ],
          callbacks: {
            onBlur: function() {
               var str =  $('#summernote').summernote('code');
                var res = [];
                res = str.match(/www/g);
                var com = [];
                com = str.match(/@/g);
                var matches = [];
                matches = str.match(/\bhttps?::\/\/\S+/gi);
                var matches2 = [];
                matches2 = str.match(/\b(http|https)?(:\/\/)?(\S*)\.(\w{2,4})(.*)/g);
                if(matches2 || matches || res || com){
                     $('.next').addClass('hidden');
                     $('.nextscreeningerror').addClass('hidden');
                     $('.nexterror').removeClass('hidden');
                }else{
                     $('.next').removeClass('hidden');
                     $('.nexterror').addClass('hidden');
                }
            }
          }
        });

        $('#selling1').keyup(function (e) {
            updateOutput(1);
        });

        $('#selling2').keyup(function (e) {
            updateOutput(2);
        });

        $('#selling3').keyup(function (e) {
            updateOutput(3);
        });

        $('#selling4').keyup(function (e) {
            updateOutput(4);
        });

        function updateOutput(id) {
            var sampleInput = $('#selling' + id + '').val(),
                    sampleInputLength = sampleInput.length;

            if (sampleInputLength >= 155) {
                sampleInput = sampleInput.substr(0, 150) + ".....";
            }
            if (id != 4) {
                $('#charCounter' + id + '').html('<span style="color:green">' + sampleInputLength + '</span> / 60 characters left.');

            } else {

                $('#charCounter' + id + '').html('<span style="color:green">' + sampleInputLength + '</span> / 200 characters left.');
            }
        }

        var category_el = $('#category');
        var render_type_el = $('#render_type');

        category_el.on('change', function () {
            $.ajax({
                url: "/api/get_sub_categories_by_category/" + $('#category').val(),
                success: function (sub_categories) {

                    var $sub_category_select = $('#sub_category');
                    $sub_category_select.find('option').remove();

                    $.each(sub_categories, function (key, value) {
                        $sub_category_select.append('<option value=' + value['id'] + '>' + value['title'] + '</option>');
                    });
                    $sub_category_select.trigger("change");
                },
                error: function (response) {
                }
            });
        });

        render_type_el.on('change', function (ev) {
            var val = $(this).find('option:selected').val();

            //console.log(val);

            if (val == "{{\App\Posts::RENDER_TYPE_TEXT}}") {
                $('#featured_image_div').hide();
                $('#image_parallax_div').hide();

                $('#gallery_image_div').hide();
                $('#video_div').hide();
                $('#video_parallax_div').hide();
            }

            if (val == "{{\App\Posts::RENDER_TYPE_IMAGE}}") {
                $('#featured_image_div').show();
                $('#image_parallax_div').show();

                $('#gallery_image_div').hide();
                $('#video_div').hide();
                $('#video_parallax_div').hide();
            }

            if (val == "{{\App\Posts::RENDER_TYPE_GALLERY}}") {
                $('#gallery_image_div').show();

                $('#featured_image_div').hide();
                $('#image_parallax_div').hide();
                $('#video_div').hide();
                $('#video_parallax_div').hide();
            }

            if (val == "{{\App\Posts::RENDER_TYPE_VIDEO}}") {
                $('#video_div').show();
                $('#video_parallax_div').show();
                $('#featured_image_div').show();

                $('#gallery_image_div').hide();
                //$('#featured_image_div').hide();
                $('#image_parallax_div').hide();
            }

        });

        category_el.trigger('change');
        render_type_el.trigger('change');



        /* fill step 3 from step 2 */
        // get input value & put it by data-class
        $('#tab2 input, #tab2 textarea').on('change', function () {
            var value = $(this).val();
            var className = $(this).data('class');
            if (value.length > 0) {
                $('.' + className).html(value).removeClass('hidden');
            } else {
                $('.' + className).html(value).addClass('hidden');
            }
        });
        // get text from editor
        $("#selling4").on('change', function () {
            $('.company-description').html(($(this).val()));
        });
        
        $("#paymentopt").on('change', function () {
            if ($('#paymentopt').val() == "creditc") {
                $('.card-details').removeClass("hidden");
                $('#invwarning').addClass("hidden");
                $('#cf89073789').val($('#cf89073789old').val());
                setTotalPrice();
            } else if ($('#paymentopt').val() == "eft") {
                $('.card-details').addClass("hidden");
                $('#invwarning').removeClass("hidden");
                $('#cf89073789').val(0);
                setTotalPrice();
            }
        });
        // get attachment image
//  $("#featured_image").on('change', function () {
//      var reader = new FileReader();
//
//      reader.onload = function (e) {
//          // get loaded data and render thumbnail.
//          $(".company-image").attr('src', e.target.result).css('max-width','110px');
//      };
//      // read the image file as a data URL.
//      reader.readAsDataURL(this.files[0]);
//  });

        /* Calculate Price on all steps */
        var PriceStandartJob = $('#step3_box #standard').data('price'); // From: Standard Listing in Step 3
        var PriceStep2 = 0;
        var PriceStep3 = 0;
        var PriceStep4 = 0;
        var PriceStep5 = 0;
        var TotalPrice = 0;
        var TotalPayable = 0;
        var BonusCredits = 0;
        var skipSelect;
        featuredproduct = false;
       
        $("#couponbtn").click(function () {
            //console.log("hello");
            var token = $('#token').val();
            var code = $('#coupon').val();
            
            $.ajax({
                url: "/poster/checkCoupon?code=" + code + "&_token=" + token,
                type: "POST",
                success: function (result) {
                    result = JSON.parse(result);
                    //console.log(result.rate)
                    if (result.status == 'ok') {
                        if($('.ref_apply').val() == 'yes'){
                            if($('.codeallow').val() == 'yes' && result.exclusive == 'no'){
                                console.log(result);
                                $('#couponresponse').html("<span style='color:green'>" + result.reason + "</span>");
                                $('#boost_discount_rate').val(result.boost_discount_rate);
                                $('#question_discount_rate').val(result.question_discount_rate);
                                $('#job_discount_rate').val(result.job_discount_rate);
                                $('#packages_discount_rate').val(result.packages_discount_rate);
                                $('#NhibunsniiMNSK992ID').val(result.id);
                                $('#ZKONONOSDN3223NSK992').val(result.exclusive);
                                $('.coup_name').html('Coupon Code: '+result.name);
                                $('.coup_expiry').html('Coupon Expiry: '+result.expiry);
                                setTotalPrice();
                            }
                            else{
                                $('#couponresponse').html("<span style='color:red'>" + ' Sorry, this Coupon Code is unable to be combined with your Referral Code.' + "</span>");
                                $('#boost_discount_rate').val(0);
                                $('#job_discount_rate').val(0);
                                $('#packages_discount_rate').val(0);
                                $('#question_discount_rate').val(0);
                                $('#NhibunsniiMNSK992ID').val("");
                                $('#ZKONONOSDN3223NSK992').val(0);
                                $('.coup_name').html('Coupon Code: '+result.name);
                                $('.coup_expiry').html('Coupon Expiry: '+result.expiry);
                                 setTotalPrice();
                            }
                        
                        }
                        else{
                                $('#couponresponse').html("<span style='color:green'>" + result.reason + "</span>");
                                $('#boost_discount_rate').val(result.boost_discount_rate);
                                $('#question_discount_rate').val(result.question_discount_rate);
                                $('#job_discount_rate').val(result.job_discount_rate);
                                $('#packages_discount_rate').val(result.packages_discount_rate);
                                $('#NhibunsniiMNSK992ID').val(result.id);
                                $('#ZKONONOSDN3223NSK992').val(result.exclusive);
                                $('.coup_name').html('Coupon Code: '+result.name);
                                $('.coup_expiry').html('Coupon Expiry: '+result.expiry);     
                                setTotalPrice();
                        }
                    } else {
                        $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                        $('#boost_discount_rate').val(0);
                        $('#job_discount_rate').val(0);
                        $('#packages_discount_rate').val(0);
                        $('#question_discount_rate').val(0);
                        $('#NhibunsniiMNSK992ID').val("");
                        $('#ZKONONOSDN3223NSK992').val(0);
                        $('.coup_name').html('');
                        $('.coup_expiry').html('');
                         setTotalPrice();
                    }
                }, error: function (result) {
                    result = JSON.parse(result);
                    $('#error').removeClass("hidden");
                    $('#error').html(result.responseText);
                    $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                }
            });
        });

        /* fill step 4 from step 3 */
        $('#step3_box [name=posts_packages]').on('change', function () {
            var _this = $(this).parent().parent().parent();
            PriceStep3 = 0;
            PriceStep3 = parseFloat($(this).data('price'), 10);
            //console.log(PriceStep3);
            var title = _this.find('.title').html();
            var packagename = _this.find('.packagename').val();
            $('.valuego').html(packagename);
            if(packagename != ''){
                $('.helo').prop('checked', false);
                PriceStep4 = 0;
                $(".rr").remove();
                $('#step5Upgrades').html('No Boost Selected');
                $('#PostsUpgrades').val('');

            }
            if(packagename.substr(0,4) == "Feat"){
                $('.display1').css('display','');
                $('.display2').css('display','none');
                 $('.display3').css('display','none');
            }
            else if(packagename.substr(0,4) == "Prem"){
                 $('.display1').css('display','none');
                 $('.display2').css('display','');
                 $('.display3').css('display','none');
            }
            else if(packagename.substr(0,4) == "Stan"){
                $('.display1').css('display','none');
                 $('.display2').css('display','none');
                 $('.display3').css('display','');
            }
            else{
                $('.display1').css('display','none');
                 $('.display2').css('display','none');
                 $('.display3').css('display','none');
            }
            clogo = packagename.substr(0,4) == "Feat" || packagename.substr(0,4) == "Prem";
            featuredproduct = packagename.substr(0,4) == "Feat";
            var price = _this.find('.price').val();
            var stanprice = _this.find('.price_stan').val();
            var description = _this.find('.package-description').text();
            preview_vid = $("#video_link").val();
            if (featuredproduct == true) {
                    $(".pager .next").removeClass("disabled");
            } else {
                featuredproduct == false;
                if(preview_vid !== null) {
                    if(preview_vid.length > 10) {
                        swal({
                            html: "We've noticed that you’ve added a video in your job description. Please note that video can only be included with our Featured Listings.</br>Please either select a Featured Listing or go back and remove the video to continue with a Premium or Standard Job Ad.",
                        })
                        $(".pager .next").addClass("disabled");
                    };
                };
                    
                if(matchs !== null) {
                    if (matchs.length > 0) {
                        swal({
                            html: "We've noticed that you’ve added photos in your job description. Please note that photos can only be included with our Featured Listings.</br>Please either select a Featured Listing or go back and remove the photo to continue with a Premium or Standard Job Ad.",                        
                        })
                        $(".pager .next").addClass("disabled");
                    };
                };

                if(preview_vid.length > 10 && matchs !== null) {
                    swal({
                        html: "We've noticed that you’ve added photos and a video in your job description. Please note that these can only be included with our Featured Listings. </br>Please either select a Featured Listing or go back and remove the photo and video to continue with a Premium or Standard Job Ad.",
                    })
                };

                if(matchs == null && preview_vid.length < 10) {
                    $(".pager .next").removeClass("disabled");
                }
            }
            //console.log(packagename);
            
            //console.log(packagename);
            /* fill step5 */
            $('#step5_box .title').html(title);
            $('#step5_box .desc').html(title);
            /* fill step6 */
            $('.package_name').html(packagename + " Listing");
            $('#PostsPackages').val($(this).val());
            setTotalPrice();
        });
        Number.prototype.countDecimals = function () {
            if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
            return this.toString().split(".")[1].length || 0; 
        }

        /* Fill step 6 on change additional services step 4 */
        $('#step4_box .additional-upgrade, #step4_box .additional-upgrade-count').on('change', function () {
            //console.log(PriceStep4);

            setTotalPrice();
            
        });

        /* Fill step 6 on change questions step 2 */
        $('#step2_box .questiontotal').on('change', function () {
            if($('#step2_box .questiontotal').is(':checked')){
                $('.questiondisplay').removeClass('hidden');
            }else{
                $('.questiondisplay').addClass('hidden');
            }
            setTotalPrice();
        });

        /* Fill step 6 on change step 5 */
        $('#step5_box .package').on('change', function () {
            var id = $(this).data('id');
            if (!$('#tab5 [name=packages_stats]:checked').val()) {
                    $('.card-details').removeClass("hidden");
                    $('#invwarning').addClass("hidden");
                    $('#cf89073789').val($('#cf89073789old').val());
                    $('#paymentopt').addClass('hidden');
                    $('#removecredit').removeClass('hidden');
                     $('.creditoption').attr("selected");
                     $('.invoiceoption').css('display', 'none');
                     $('.invoiceoption').removeAttr("selected");
            } 
            else if($('#tab5 [name=packages_stats]:checked').val() == 'single') {
                    $('.card-details').removeClass("hidden");
                    $('#invwarning').addClass("hidden");
                    $('#cf89073789').val($('#cf89073789old').val());
                    $('#paymentopt').addClass('hidden');
                    $('#removecredit').removeClass('hidden');
                     $('.creditoption').attr("selected");
                    $('.invoiceoption').css('display', 'none');
                    $('.invoiceoption').removeAttr("selected");
            }
            else{  
                    $('.card-details').removeClass("hidden");
                    $('#invwarning').addClass("hidden");
                    $('#paymentopt').removeClass('hidden');
                    $('#removecredit').addClass('hidden');
                    $('.invoiceoption').css('display', '');
                    $('#cf89073789').val($('#cf89073789old').val());
                    $('.creditoption').attr("selected");
                    $('.invoiceoption').removeAttr("selected");
            }
            PriceStep5 = parseFloat($(this).data('price'), 10) | 0;
            /* Use Bonus on Step 5 */
            BonusCredits = parseFloat($(this).data('bonus'), 10) | 0;

            var _this = $(this).parent().parent().parent();

            var title = _this.find('.add-package-title').html();
            title = title + ' ' + Number(_this.find('.add-package-price').html()).toFixed(2);
            $(".asr").remove();
            /* fill step6 */
            if (PriceStep5 > 0) {
                $('#step6_box .add-package').removeClass('hidden');
                $('#PackagesStats').val($(this).val());  
            } 
            else{
                $('#step6_box .add-package').addClass('hidden');
                $('#PackagesStats').val(0);
            }

            $('#step6_box .add-package .add-package-title').html(title);
            setTotalPrice();
            var code = "{{$coupon_code}}";
            if (code.length > 3 ) {
                var token = $('#token').val();
                
                $.ajax({
                    url: "/poster/checkRegCoupon?code=" + code + "&_token=" + token,
                    type: "POST",
                    success: function (result) {
                        result = JSON.parse(result);
                        //console.log(result.rate)
                        if (result.status == 'ok') {
                            console.log(result);
                            if(result.exclusive == 'no') {
                                $('#couponresponse').html("<span style='color:green'>" + result.reason + "</span>");
                            }
                            if(result.exclusive == 'yes') {
                                $('#couponresponse').html("<span style='color:orange'>" + result.reason + "</span><br><small>This is an exclusive coupon. Referral discounts donot apply.</small>");
                            }
                            $('#NhibunsniiMNSK992ID').val(result.id);
                            $('#ZKONONOSDN3223NSK992').val(result.exclusive);
                            setTotalPrice(0);
                            couponcheck2 = 1;
                        } else {
                            $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                        }
                    }, error: function (result) {
                        result = JSON.parse(result);
                        $('#error').removeClass("hidden");
                        $('#error').html(result.responseText);
                        $('#couponresponse').html("<span style='color:red'>" + result.reason + "</span>");
                    }
                });
            }
        });

        /* Use my credits */
        $('#use_credits').on('change', function () {
            var credits = 0;
            if (this.checked) {
                credits = $(this).data('credits');
                $('#credits').val(credits);
                console.log("HERECREDITS" + credits)
                $('#step6_box .use-credits').removeClass('hidden');
                $('#payment-form .card-details').show();
                $('#jobpaymentitle').html("Payment");
            } else {
                $('#credits').val(0);
                $('#step6_box .use-credits').addClass('hidden');
            }
            setTotalPrice();
        });
        
        var allowthrough = false;
        
        function setTotalPrice() {
                                $('#step2_box .questiontotal').each(function () {
                                    if ($(this).is(':checked')) {
                                        PriceStep2 = 0;
                                        var price = Number($(this).data('price'));
                                        var stanprice = Number($(this).data('pricestan'));
                                        PriceStep2 = Number($(this).data('price'));
                                        $('.questions_final').removeClass('hidden');
                                        if($('#question_discount_rate').val() > 0 && $('.codeallow').val() == 'no' ){
                                        if($('#question_discount_rate').val() > 0){
                                            $('.ref_disc_perc_question').html('N/A');
                                            $('.coup_disc_perc_question').html(Number($('#question_discount_rate').val()).toFixed(2) + '%');
                                            $itemcoup = PriceStep2 * $('#question_discount_rate').val()/100;
                                            $('.question_price').html('$' + Number(PriceStep2-$itemcoup).toFixed(2));
                                            $('.question_price_go').html('$' + Number(PriceStep2-$itemcoup).toFixed(2));
                                            $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                        }
                                        }
                                        else if($('#question_discount_rate').val() == 0 && stanprice-price > 0){
                                            $itemref = stanprice-PriceStep2;
                                            $itemrefper = $itemref/stanprice*100;
                                            $('.ref_disc_perc_question').html(Number($itemrefper).toFixed(2) + '%');
                                            $('.coup_disc_perc_question').html('N/A');
                                            $('.question_price').html('$' + Number(PriceStep2).toFixed(2));
                                            $('.question_price_go').html('$' + Number(PriceStep2).toFixed(2));
                                            $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                        }
                                        else if($('.codeallow').val() == 'yes' && $('#ZKONONOSDN3223NSK992').val() == 'no'){
                                                if(stanprice-price > 0){
                                                    if($('#question_discount_rate').val() > 0){
                                                        $itemref = stanprice-price;
                                                        $itemrefper = $itemref/stanprice*100;
                                                        $('.ref_disc_perc_question').html(Number($itemrefper).toFixed(2) + '%');
                                                        $('.coup_disc_perc_question').html(Number($('#question_discount_rate').val()).toFixed(2)+'%');
                                                        $('.question_price').html('$' + Number(price-(price*$('#question_discount_rate').val()/100)).toFixed(2));
                                                        $('.question_price_go').html('$' + Number(price-(price*$('#question_discount_rate').val()/100)).toFixed(2));
                                                        $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                    } else{
                                                        $itemref = stanprice-price;
                                                        $itemrefper = $itemref/stanprice*100;
                                                        $('.ref_disc_perc_question').html(Number($itemrefper).toFixed(2) + '%');
                                                        $('.coup_disc_perc_question').html('N/A');
                                                        $('.question_price').html('$' + Number(price).toFixed(2));
                                                        $('.question_price_go').html('$' + Number(price).toFixed(2));
                                                        $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                    }
                                                }
                                                else{
                                                    if($('#question_discount_rate').val() > 0){
                                                        $('.ref_disc_perc_question').html('N/A');
                                                        $('.coup_disc_perc_question').html(Number($('#question_discount_rate').val()).toFixed(2)+'%');
                                                        $coup_go = price-(price*$('#question_discount_rate').val()/100);
                                                        $('.question_price').html('$' + Number($coup_go).toFixed(2));
                                                        $('.question_price_go').html('$' + Number($coup_go).toFixed(2));
                                                        $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                    } else{
                                                        $('.ref_disc_perc_question').html('N/A');
                                                        $('.coup_disc_perc_question').html('N/A');
                                                        $('.question_price').html('$' + Number(price).toFixed(2));
                                                        $('.question_price_go').html('N/A');
                                                        $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                            }
                                                }
                                        }
                                        else{
                                                $('.ref_disc_perc_question').html('N/A');
                                                $('.coup_disc_perc_question').html('N/A');
                                                $('.question_price').html('$' + Number(price).toFixed(2));
                                                $('.question_price_go').html('N/A');
                                                $('.question_price_stan').html('$' + Number(stanprice).toFixed(2));
                                        }
                                    }else{
                                        PriceStep2 = 0;
                                        $('.questions_final').addClass('hidden');
                                        $('#step2Questions').html('Screening Questions not purchased');
                                    };
                                    });
                                    if(PriceStep2 > 0){
                                        $('#step2Questions').html('');
                                        $('#step2Questions').append('Screening Questions' + " - $" + Number(PriceStep2).toFixed(2) + "<br>");
                                    }

                                    PriceStep4 = 0;
                                    $('#step3_box [name=posts_packages]').each(function (){
                                    if ($(this).is(':checked')) {
                                        PriceStep3 = 0;
                                        var _this = $(this).parent().parent().parent();
                                        var price = _this.find('.price').val();
                                        var stanprice = _this.find('.price_stan').val();
                                        PriceStep3 = parseFloat($(this).data('price'), 10);
                                        if($('#job_discount_rate').val() > 0 && $('.codeallow').val() == 'no' ){
                                        if($('#job_discount_rate').val() > 0){
                                            $('.ref_disc_perc').html('N/A');
                                            $('.coup_disc_perc').html(Number($('#job_discount_rate').val()).toFixed(2) + '%');
                                            $itemcoup = PriceStep3 * $('#job_discount_rate').val()/100;
                                            $('.package_price').html('$' + Number(PriceStep3-$itemcoup).toFixed(2));
                                            $('.package_price_go').html('$' + Number(PriceStep3-$itemcoup).toFixed(2));
                                            $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                        }
                                        }
                                        else if($('#job_discount_rate').val() == 0 && stanprice-price > 0){
                                            $itemref = stanprice-PriceStep3;
                                            $itemrefper = $itemref/stanprice*100;
                                            $('.ref_disc_perc').html(Number($itemrefper).toFixed(2) + '%');
                                            $('.coup_disc_perc').html('N/A');
                                            $('.package_price').html('$' + Number(PriceStep3).toFixed(2));
                                            $('.package_price_go').html('$' + Number(PriceStep3).toFixed(2));
                                            $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                        }
                                        else if($('.codeallow').val() == 'yes' && $('#ZKONONOSDN3223NSK992').val() == 'no'){
                                                if(stanprice-price > 0){
                                                    if($('#job_discount_rate').val() > 0){
                                                        $itemref = stanprice-price;
                                                        $itemrefper = $itemref/stanprice*100;
                                                        $('.ref_disc_perc').html(Number($itemrefper).toFixed(2) + '%');
                                                        $('.coup_disc_perc').html(Number($('#job_discount_rate').val()).toFixed(2)+'%');
                                                        $('.package_price').html('$' + Number(price-(price*$('#job_discount_rate').val()/100)).toFixed(2));
                                                        $('.package_price_go').html('$' + Number(price-(price*$('#job_discount_rate').val()/100)).toFixed(2));
                                                        $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                    } else{
                                                        $itemref = stanprice-price;
                                                        $itemrefper = $itemref/stanprice*100;
                                                        $('.ref_disc_perc').html(Number($itemrefper).toFixed(2) + '%');
                                                        $('.coup_disc_perc').html('N/A');
                                                        $('.package_price').html('$' + Number(price).toFixed(2));
                                                        $('.package_price_go').html('$' + Number(price).toFixed(2));
                                                        $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                    }
                                                }
                                                else{
                                                    if($('#job_discount_rate').val() > 0){
                                                        $('.ref_disc_perc').html('N/A');
                                                        $('.coup_disc_perc').html(Number($('#job_discount_rate').val()).toFixed(2)+'%');
                                                        $coup_go = price-(price*$('#job_discount_rate').val()/100);
                                                        $('.package_price').html('$' + Number($coup_go).toFixed(2));
                                                        $('.package_price_go').html('$' + Number($coup_go).toFixed(2));
                                                        $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                    } else{
                                                        $('.ref_disc_perc').html('N/A');
                                                        $('.coup_disc_perc').html('N/A');
                                                        $('.package_price').html('$' + Number(price).toFixed(2));
                                                        $('.package_price_go').html('N/A');
                                                        $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                                            }
                                                }
                                        }
                                        else{
                                                $('.ref_disc_perc').html('N/A');
                                                $('.coup_disc_perc').html('N/A');
                                                $('.package_price').html('$' + Number(price).toFixed(2));
                                                $('.package_price_go').html('N/A');
                                                $('.package_price_stan').html('$' + Number(stanprice).toFixed(2));
                                        }
                                    }
                                    });
                                $('#step5Upgrades').html('');
                                var upgrades = new Array();
                                var posts_upgrades = new Array();
                                PriceStep4 = 0;
                                $('#step4_box .additional-upgrade').each(function () {
                                    if ($(this).is(':checked')) {
                                        var id = $(this).data('id');
                                        var _this = $(this).parent().parent().parent();
                                        if($('#boost_discount_rate').val() > 0 && $('.codeallow').val() == 'no' ){
                                            if($('#boost_discount_rate').val() > 0){
                                                PriceStep4 += _this.find('.actualprice').val() * parseFloat($('#count_' + id).val(), 10);
                                                /* get upgrades to array */
                                                upgrades[id] = {
                                                    'standard_price': '$'+Number(_this.find('.actualprice').val()).toFixed(2),
                                                    'ref_disc': 'N/A',
                                                    'coup_disc': Number($('#boost_discount_rate').val()).toFixed(2)+'%',
                                                    'title': _this.find('.add-upgrade-title').html(),
                                                    'price': _this.find('.actualprice').val(),
                                                    'price_go':_this.find('.actualprice').val() - _this.find('.actualprice').val()*$('#boost_discount_rate').val()/100,
                                                    'price_go2':'$'+Number(_this.find('.actualprice').val() - _this.find('.actualprice').val()*$('#boost_discount_rate').val()/100).toFixed(2),
                                                    'weeks': _this.find('.additional-upgrade-count').val()
                                                };
                                            } 
                                        }
                                        else if($('#boost_discount_rate').val() == 0 && _this.find('.price_stan_up').val() - _this.find('.price').val() > 0){
                                            PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                                             /* get upgrades to array */
                                            upgrades[id] = {
                                                'standard_price':'$'+ Number(_this.find('.price_stan_up').val()).toFixed(2),
                                                'ref_disc': Number((_this.find('.price_stan_up').val() - _this.find('.price').val())/_this.find('.price_stan_up').val()*100).toFixed(2)+'%',
                                                'coup_disc': 'N/A',
                                                'title': _this.find('.add-upgrade-title').html(),
                                                'price': _this.find('.price').val(),
                                                'price_go':_this.find('.price').val(),
                                                'price_go2':'$'+Number(_this.find('.price').val()).toFixed(2),
                                                'weeks': _this.find('.additional-upgrade-count').val()
                                            };
                                        }
                                        else if($('.codeallow').val() == 'yes' && $('#ZKONONOSDN3223NSK992').val() == 'no'){
                                                if(_this.find('.price_stan_up').val() - _this.find('.price').val() > 0){
                                                    if($('#boost_discount_rate').val() > 0){
                                                        PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                                                         /* get upgrades to array */
                                                        upgrades[id] = {
                                                            'standard_price': '$'+Number(_this.find('.price_stan_up').val()).toFixed(2),
                                                            'ref_disc': Number((_this.find('.price_stan_up').val() - _this.find('.price').val())/_this.find('.price_stan_up').val()*100).toFixed(2)+'%',
                                                            'coup_disc': Number($('#boost_discount_rate').val()).toFixed(2)+'%',
                                                            'title': _this.find('.add-upgrade-title').html(),
                                                            'price': _this.find('.price').val(),
                                                            'price_go2':'$'+Number(_this.find('.price').val()-(_this.find('.price').val()*$('#boost_discount_rate').val()/100)).toFixed(2),
                                                            'price_go':_this.find('.price').val()-(_this.find('.price').val()*$('#boost_discount_rate').val()/100),
                                                            'weeks': _this.find('.additional-upgrade-count').val()
                                                        };
                                                    } else{
                                                         PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                                                         /* get upgrades to array */
                                                        upgrades[id] = {
                                                            'standard_price': '$'+Number(_this.find('.price_stan_up').val()).toFixed(2),
                                                            'ref_disc': Number((_this.find('.price_stan_up').val() - _this.find('.price').val())/_this.find('.price_stan_up').val()*100).toFixed(2)+'%',
                                                            'coup_disc': 'N/A',
                                                            'title': _this.find('.add-upgrade-title').html(),
                                                            'price': _this.find('.price').val(),
                                                            'price_go2':'$'+Number(_this.find('.price')).toFixed(2),
                                                            'price_go':_this.find('.price').val(),
                                                            'weeks': _this.find('.additional-upgrade-count').val()
                                                        };
                                                    }
                                                }
                                                else{
                                                    if($('#boost_discount_rate').val() > 0){
                                                        PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                                                         /* get upgrades to array */
                                                        upgrades[id] = {
                                                            'standard_price': '$'+Number(_this.find('.price_stan_up').val()).toFixed(2),
                                                            'ref_disc': 'N/A',
                                                            'coup_disc': Number($('#boost_discount_rate').val()).toFixed(2)+'%',
                                                            'title': _this.find('.add-upgrade-title').html(),
                                                            'price': _this.find('.price').val(),
                                                            'price_go2':'$'+Number(_this.find('.price').val()-(_this.find('.price').val()*($('#boost_discount_rate').val()/100))).toFixed(2),
                                                            'price_go':_this.find('.price').val()-(_this.find('.price').val()*($('#boost_discount_rate').val()/100)),
                                                            'weeks': _this.find('.additional-upgrade-count').val()
                                                        };
                                                    } else{
                                                        PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                                                        /* get upgrades to array */
                                                        upgrades[id] = {
                                                            'title': _this.find('.add-upgrade-title').html(),
                                                            'standard_price': '$'+Number(_this.find('.price').val()).toFixed(2),
                                                            'ref_disc': 'N/A',
                                                            'coup_disc': 'N/A',
                                                            'price': _this.find('.price').val(),
                                                            'price_go':_this.find('.price').val(),
                                                            'price_go2':'N/A',
                                                            'weeks': _this.find('.additional-upgrade-count').val()
                                                        };
                                                    }
                                                }                            
                                            }
                                        else{
                                            PriceStep4 += parseFloat($(this).data('price'), 10) * parseFloat($('#count_' + id).val(), 10);
                                            /* get upgrades to array */
                                            upgrades[id] = {
                                                'title': _this.find('.add-upgrade-title').html(),
                                                'standard_price': '$'+Number(_this.find('.price').val()).toFixed(2),
                                                'ref_disc': 'N/A',
                                                'coup_disc': 'N/A',
                                                'price': _this.find('.price').val(),
                                                'price_go':_this.find('.price').val(),
                                                'price_go2':'N/A',
                                                'weeks': _this.find('.additional-upgrade-count').val()
                                            };
                                        }
                                        /* create value to POST */
                                        posts_upgrades[id] = parseFloat($('#count_' + id).val(), 10);
                                    }
                                });
                                /* fill step6 */
                                if (PriceStep4 > 0) {
                                    $('#step6_box .upgrades').removeClass('hidden');
                                    $('#step6_box .add-upgrade-title').html('$' + Number(PriceStep4).toFixed(2));
                                    $('#PostsUpgrades').val(JSON.stringify(posts_upgrades));
                                    /* fill Upgrades on step 5 */
                                    var upgradesHtml = '';
                                    $(".rr").remove();
                                    upgrades.forEach(function (e) {

                                        $cal_price = e.price * e.weeks;
                                        $upgrade_price = Number($cal_price).toFixed(2);
                                        //console.log(upgradesHtml + '' + e.title + ': ' + e.price + '<br>');
                                        $('#step5Upgrades').append(e.weeks + 'x ' + e.title + " - $" + Number($upgrade_price).toFixed(2) + "<br>")
                                        $('#ordertable').append('<tr class="rr"><td>' + e.title + '</td><td class="text-center">' + e.standard_price + '</td><td class="text-center">' + e.ref_disc  + '</td><td class="text-center">' + e.coup_disc + '</td><td class="text-center">' + e.price_go2 + '</td><td class="text-center">' + e.weeks + '</td><td class="text-right">$' + Number(e.price_go*e.weeks).toFixed(2) + '</td></tr>');
                                    });
                                        //$('#step5Upgrades').html(upgradesHtml);
                                    } else {
                                        $('.helo').prop('checked', false);
                                        PriceStep4 = 0;
                                        $(".rr").remove();
                                        $('#step5Upgrades').html('No Boost Selected');
                                        $('#PostsUpgrades').val('');
                                    }
            $('#step5_box .package').each(function () {
                if ($(this).is(':checked')) {
                    var id = $(this).data('id');
                PriceStep5 = parseFloat($(this).data('price'), 10) | 0;
                /* Use Bonus on Step 5 */
                BonusCredits = parseFloat($(this).data('bonus'), 10) | 0;

                var _this = $(this).parent().parent().parent();

                var title = _this.find('.add-package-title').html();
                title = title + ' ' + Number(_this.find('.add-package-price').html()).toFixed(2);
                if(PriceStep5 > 0 && PriceStep3+PriceStep4+PriceStep2 > PriceStep5){
                swal({
                        html: "You cannot purchase this Value pack beacuse your Job Ad Total price is greater than Value Pack's bonus credits.",
                    })
                $('.notallow').prop('checked', false);
                $('#step6_box .add-package').addClass('hidden');
                $('#PackagesStats').val(0);
                PriceStep5 = 0;
                }
                if (PriceStep5 > 0) {
                                        if($('#packages_discount_rate').val() > 0 && $('.codeallow').val() == 'no' ){
                                        $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                        $pac_disc = '$'+ Number($(this).data('price')-($(this).data('price')*$('#packages_discount_rate').val()/100)).toFixed(2);
                                        $pac_disc2 = '$'+ Number($(this).data('price')-($(this).data('price')*$('#packages_discount_rate').val()/100)).toFixed(2);
                                        $pac_coup = Number($('#packages_discount_rate').val()).toFixed(2)+'%';
                                        $pac_ref = 'N/A';
                                        }
                                    else if($('#packages_discount_rate').val() == 0 && $('.ref_disc_go').val() > 0){
                                        $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                        $pac_disc = '$'+Number($(this).data('price')-($(this).data('price')*$('.ref_disc_go').val()/100)).toFixed(2);
                                        $pac_disc2 = '$'+Number($(this).data('price')-($(this).data('price')*$('.ref_disc_go').val()/100)).toFixed(2);
                                        $pac_coup = 'N/A';
                                        $pac_ref = Number($('.ref_disc_go').val()).toFixed(2)+'%';
                                    }
                                    else if($('.codeallow').val() == 'yes' && $('#ZKONONOSDN3223NSK992').val() == 'no'){
                                            if($('.ref_disc_go').val() > 0 && $('#packages_discount_rate').val() > 0){
                                                $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                                $ref_calc = $(this).data('price')-($(this).data('price')*$('.ref_disc_go').val()/100);
                                                $pac_disc = '$'+Number($ref_calc-($ref_calc*$('#packages_discount_rate').val()/100)).toFixed(2);
                                                $pac_disc2 = '$'+Number($ref_calc-($ref_calc*$('#packages_discount_rate').val()/100)).toFixed(2);
                                                $pac_coup = Number($('#packages_discount_rate').val()).toFixed(2)+'%';
                                                $pac_ref = Number($('.ref_disc_go').val()).toFixed(2)+'%';
                                            }
                                            else if($('.ref_disc_go').val() > 0 && $('#packages_discount_rate').val() == 0){
                                                $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                                $ref_calc = $(this).data('price')-($(this).data('price')*$('.ref_disc_go').val()/100);
                                                $pac_disc = '$'+Number($ref_calc).toFixed(2);
                                                $pac_disc2 = '$'+Number($ref_calc).toFixed(2);
                                                $pac_coup = 'N/A';
                                                $pac_ref = Number($('.ref_disc_go').val()).toFixed(2)+'%';
                                            }
                                            else if($('.ref_disc_go').val() == 0 && $('#packages_discount_rate').val() > 0){
                                                $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                                $pac_disc = '$'+ Number($(this).data('price')-($(this).data('price')*$('#packages_discount_rate').val()/100)).toFixed(2);
                                                $pac_disc2 = '$'+ Number($(this).data('price')-($(this).data('price')*$('#packages_discount_rate').val()/100)).toFixed(2);
                                                $pac_coup = Number($('#packages_discount_rate').val()).toFixed(2)+'%';
                                                $pac_ref = 'N/A';
                                            }
                                            else if($('.ref_disc_go').val() == 0 && $('#packages_discount_rate').val() == 0){
                                                $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                                $pac_disc = '$'+Number($(this).data('price')).toFixed(2);
                                                $pac_disc2 = 'N/A';
                                                $pac_coup = 'N/A';
                                                $pac_ref = 'N/A';
                                            }                            
                                        }
                                    else{
                                        $pac_price = '$'+Number($(this).data('price')).toFixed(2);
                                        $pac_disc = '$'+Number($(this).data('price')).toFixed(2);
                                        $pac_disc2 = 'N/A';
                                        $pac_coup = 'N/A';
                                        $pac_ref = 'N/A';
                                    }
                    $(".asr").remove();
                    /* fill step6 */
                    $('#step6_box .add-package .add-package-title').html(title);
                    if (_this.find('.add-package-title').html() != "Single Listing Only") {
                        var ce = $(this).data('price') + $(this).data('bonus');
                        $('#assdsda23312312').val(ce);
                        $('#ordertable').append('<tr class="asr"><td>' + _this.find('.add-package-title').html() + '<p style="font-size:10px">' +_this.find('.add-package-price').html() + '</p></td><td class="text-center">' + $pac_price + '</td><td class="text-center">'+ $pac_ref +'</td><td class="text-center">' + $pac_coup + '</td><td class="text-center">' + $pac_disc2 +'</td><td class="text-center">1</td><td class="text-right">' + $pac_disc + '</td></tr>');
                    }
                    $('#step6_box .add-package').removeClass('hidden');
                    $('#PackagesStats').val($(this).val());
                } else {
                    $('#step6_box .add-package').addClass('hidden');
                    $('#PackagesStats').val(0);
                    PriceStep5 = 0;
                }

                }
            });
            var credits = parseFloat($('#use_credits').data('credits'));
            console.log("SUPPCREDITS " + credits);
            var restToCredits = 0;
            var newprice = 0;
            var nudiscboost = $('#boost_discount_rate').val();
            if($('#boost_discount_rate').val() > 0 && PriceStep4 > 0){
                nudiscboost = (PriceStep4 * nudiscboost / 100).toFixed(2);
                PriceStep4 = PriceStep4 - nudiscboost;
            }
            var nudiscquestions = $('#question_discount_rate').val();
            if($('#question_discount_rate').val() > 0 && PriceStep2 > 0){
                nudiscquestions = (PriceStep2 * nudiscquestions / 100).toFixed(2);
                PriceStep2 = PriceStep2 - nudiscquestions;
            }   
            var nudiscjob = $('#job_discount_rate').val();
            if(nudiscjob != 0){
                nudiscjob = (PriceStep3 * nudiscjob / 100).toFixed(2);
                PriceStep3 = PriceStep3 - nudiscjob;
            }
            var selectedPrice = PriceStep3 + PriceStep4 + PriceStep2;
            $('#assdsda23312312').val(0);
            TotalPrice = selectedPrice;
            // Purchase as part of one of our Listing Packages
            if (credits > 0) {
                console.log("in here");
                $(".hideifcred").addClass("hidden");
                $(".hideifcred2").removeClass("hidden");
                $(".credwarning").removeClass("hidden");
                allowthrough = true;

            } else {
                console.log("in here2");
                $(".hideifcred").removeClass("hidden");
                $(".hideifcred2").addClass("hidden");
                $(".credwarning").addClass("hidden");
                $("#notenough").removeClass("hidden");

            }
            console.log("TotalPrice " + TotalPrice);
            console.log("Step5 " + PriceStep5);
            if (PriceStep5 > 0) { //greater than
                    $('#step5_box .price').html('$' + Number(PriceStep3+PriceStep4+PriceStep2).toFixed(2));
                    $('#step5_box .select').html('Select $' + Number(PriceStep3+PriceStep4+PriceStep2).toFixed(2));
                    console.log("in here3");
                    if(PriceStep4 == 0){
                        $('#step5Upgrades').html('No Boost Selected');
                    }
                    if(PriceStep2 == 0){
                        $('#step2Questions').html('Screening Questions not purchased');
                    } 
                    var nudiscpac = $('#packages_discount_rate').val();
                    if($('#packages_discount_rate').val() > 0 && PriceStep5 > 0){
                        nudiscpac = (PriceStep5 * nudiscpac / 100).toFixed(2);
                        PriceStep5 = PriceStep5 - nudiscpac;
                    }               
                if (PriceStep5 >= selectedPrice) {
                    TotalPrice = PriceStep5;
                    restToCredits = TotalPrice - selectedPrice;
                    restToCredits = restToCredits + BonusCredits;
                    //$('.cremain').removeClass('hidden');
                    if (restToCredits < 0)
                        restToCredits = 0;
                    $('#step6_REMAINING').html("$" + restToCredits);
                    $('#ZZZnnusdnosnonjweoj').val(restToCredits);
                    newprice = TotalPrice;
                }
            } else {
                    $('#step5_box .price').html('$' + Number(PriceStep3+PriceStep4+PriceStep2).toFixed(2));
                    $('#step5_box .select').html('Select $' + Number(PriceStep3+PriceStep4+PriceStep2).toFixed(2)); 
                    if(PriceStep4 == 0){
                        $('#step5Upgrades').html('No Boost Selected');
                    }
                    if(PriceStep2 == 0){
                        $('#step2Questions').html('Screening Questions not purchased');
                    } 
                    $('.cremain').addClass('hidden');
                    $('#ZZZnnusdnosnonjweoj').val(credits);
                    newprice = TotalPrice;
                }
            console.log("TotalPriceAfter HERE" + TotalPrice);
            console.log("credits " + credits);
            console.log("restToCredits " + restToCredits);
            console.log("BonusCredits " + BonusCredits);
            //var newcredits = restToCredits + BonusCredits;
            /* Calculate Credits */
            var pricebeforecredits = TotalPrice;
            var pricebeforecredits_with_gst = pricebeforecredits + pricebeforecredits * 10 / 100;
            if ($('#use_credits').is(':checked')) {
                $('#uuuucMNNMNmmd').val("yes");
                console.log(credits);
                console.log("credits");
                console.log("pricebeforecredits_with_gst");
                console.log(pricebeforecredits_with_gst);
                console.log(credits <= pricebeforecredits_with_gst);
                if (credits <= pricebeforecredits) {
                    //console.log("supp");
                    console.log("in here4fdcdfdf");
                    //$('.creditsused').removeClass("hidden");
                    //$('#step6_CREDITSUSED').html('$-' + credits);
                    $('#ZZZnnusdnosnonjweoj').val(credits);
                    TotalPayable = pricebeforecredits;
                    console.log("credits below")
                    console.log(credits)
                    newprice = pricebeforecredits;
                    console.log("newprice below")
                    console.log(newprice)
                    //newprice = TotalPayable;
                    TotalPayable = newprice;
                    TotalPrice = newprice;
                    console.log("newprice below")
                    console.log(newprice)
                } else { // credits > TotalPrice
                    //console.log("supp1");
                    console.log("in here5");
                    credits = credits - TotalPrice + restToCredits;
                    console.log("5CREDITS" + credits)
                    $('#use_my_credits').html('$' + TotalPrice);
                    TotalPayable = 0;
                    newprice = 0;
                }
            } else {  //Not use credits and select Listing Packages
                //console.log("supp3");
                $('#uuuucMNNMNmmd').val("no");
                $('.creditsused').addClass("hidden");
                $('#step6_CREDITSUSED').html('$0');
                if (PriceStep5 > 0) {
                    credits = credits + restToCredits;
                }
                $('#ZZZnnusdnosnonjweoj').val(credits);
                TotalPayable = TotalPrice;
                //newprice = TotalPrice;
                    $('#selectpayment').removeClass('hidden');
                if ($('#paymentopt').val() == "creditc") {
                    $('.card-details').removeClass("hidden");
                    $('#invwarning').addClass("hidden");
                    $('#cf89073789').val($('#cf89073789old').val());
                } else if ($('#paymentopt').val() == "eft") {
                    $('.card-details').addClass("hidden");
                    $('#invwarning').removeClass("hidden");
                    $('#cf89073789').val(0);
                    $('#cf89073789old').val(0);
                }
                $('#step6_REMAINING').html("$" + restToCredits);
                //$('#invwarning').removeClass('hidden');
                $('#selectpayment').show();
            }
            var pricebeforedisc = TotalPrice;
            //console.log("TotalPriceAfter " + TotalPrice);
            $('#step6_ITEMS').html("$" + Number(pricebeforecredits).toFixed(2));
            //console.log("newprice " + newprice);
            /* Payment details */
            if (newprice == 0) {
                console.log("in here6");
                $('#payment-form .card-details').hide();
                $('#selectpayment').hide();
                $('#cf89073789').val(0);
                $('#adssdar32rfdsfdsf').val("L");
                $('#invwarning').addClass('hidden');
                $('#jobpaymentitle').html("Submit Listing");
            } else {
                $('#selectpayment').show();
                $('#payment-form .card-details').show();
                $('#jobpaymentitle').html("Payment");
                $('#adssdar32rfdsfdsf').val("R");
            }
            

            /* Credits */
            ////console.log(credits + ' ' + BonusCredits + ' ' + (credits + BonusCredits));
            $('#credits').val(credits + BonusCredits);
            $('#rest_credits').html(credits + BonusCredits);
            if ((credits + BonusCredits) > 0) {
                $('#step6_box .rest-credits').removeClass('hidden');
            } else {
                $('#step6_box .rest-credits').addClass('hidden');
            }
            $('#ZZZnnusdnosnonjweoj').val(credits);
            var ac09871280971 = $('#ac09871280971').val();
            var aus_member_discount = ($('#aus_contact_member_no').val() != '') ? (TotalPrice * ac09871280971 / 100).toFixed(2) : 0;
            if(PriceStep5 > 0){
              var other_disc = ($('#rf09871280971').val() != 0) ? (TotalPrice * $('#rf09871280971').val() / 100).toFixed(2) : 0;  
              }else{
                  var other_disc = 0;
              }
            //console.log("OTHERDISC");
            //console.log(other_disc);
            var nudisc = 0;
            $('#step6_aus_member_discount').html('$0');
            $('#step6_AC').html("");

            if ($('#ZKONONOSDN3223NSK992').val() == 'yes') { // coupon
                nudisc = 0;
                aus_member_discount = 0;
                other_disc = 0;
                $('#step6_QOTHER').html("");
                $('#step6_DISCOTHER').html("$0");
            }
            if ($('#rf09871280971EXC').val() == 'yes') {
                nudisc = 0;
                aus_member_discount = 0;
//                other_disc = 0;
//                $('#step6_QOTHER').html("");
//                $('#step6_DISCOTHER').html("$-" + );
            }
            if ($('#aus_contact_member_no').val() != '') { // auscontact
                ///nudisc = 0;
                aus_member_discount = aus_member_discount;
                //other_disc = 0;
                $('#step6_aus_member_discount').html('$-' + aus_member_discount);
                $('#step6_AC').html($("#ac09871280971").val() + "%");
//               $('#step6_QOTHER').html("");
//                $('#step6_DISCOTHER').html("$-" + );
            } else if($('#ac09871280971EXC').val() != 'yes' && $('#aus_contact_member_no').val() != '') {
                nudisc = 0;
            }

            nudisc = 0;
            if (other_disc != 0) {
                //console.log("hello");
                //$('#step6_QOTHERNAME').html($('#rf09871280971#NAME').val());
                $('#step6_QOTHER').html($('#rf09871280971').val() + "%");
                $('#step6_DISCOTHER').html("$-" + other_disc);
            } else {
                $('#step6_QOTHER').html("");
                $('#step6_DISCOTHER').html("$0");
            }
                TotalPrice = TotalPrice - aus_member_discount;
                TotalPrice = TotalPrice - other_disc;
                console.log(TotalPrice);
            if (nudisc != 0) { //coupon - coupon will always be applied last. 
                $('#step6_Q').html(nudisc + "%");
                     //other_disc = 0;
                    //console.log("NEW PRICE INSIDE NUDISC" + TotalPrice)
                    nudisc = (TotalPrice * nudisc / 100).toFixed(2);
                    TotalPrice = TotalPrice - nudisc;
                    $('#step6_DISC').html("$-" + nudisc);
                } else {
                   nudisc = 0;
                }
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                if(TotalPrice == 0){
                    $('.billing_hidden').css('display','none');
                    $('.submitzero').addClass('hidden');
                    $('.totalzero').addClass('hidden');
                    $('.panelzero').css('border-color','white');
                }
                else{
                    $('.billing_hidden').css('display','');
                    $('.submitzero').removeClass('hidden');
                    $('.totalzero').removeClass('hidden');
                    $('.panelzero').css('border-color','#ddd');
                }
                    var gst = (TotalPrice / 10).toFixed(2);
                    $('#step6_GST').html("$" + gst);
                    var totalminusgst = parseFloat(TotalPrice);
                    var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                $('#step6_SUB').html("$" + totalminusgst);
                //console.log("SETTINGSUB HERE2");
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice + ccfeetotal;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    //console.log("SETTINGSUB HERE1");
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                    $('#fintot').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
                
            
            var nucredits = parseFloat($('#use_credits').data('credits'), 10) | 0;
            var pricebeforecredits = TotalPrice;
            if ($('#use_credits').is(':checked')) {
                //console.log(credits);
                //console.log("CREDITS ABOVE");
            if (nucredits > 0 && nucredits < pricebeforecredits) {
                //TotalPrice = TotalPrice - aus_member_discount;
                //TotalPrice = TotalPrice - other_disc;
                //TotalPrice = TotalPrice - nudisc;
                $('.partial_credits').html('<i class="fa fa-info-circle" aria-hidden="true"></i> You already have remaining credit,  just click next and the balance will be payable on checkout.');
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                var gst = ((TotalPrice-credits) / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = parseFloat(TotalPrice);
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                var pricebeforedisc = TotalPrice;
                $('.partialcred').removeClass('hidden');
                $('.availcred').removeClass('hidden');
                $('.payableend').removeClass('hidden');
                $('.creditsend').removeClass('hidden');
                $('.totalpriceend').addClass('hidden');
                var ccfee = $('#cf89073789old').val();
//                console.log("SETTINGSUB HERE3");
                if (ccfee != 0) {
                    console.log("SETTINGSUB HERE3");
                    //console.log("totalplusgst");
                    //console.log(totalplusgst);
                    var ccfeetotal = (totalplusgst-credits).toFixed(2) * ccfee / 100;
                    //console.log(ccfeetotal);
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#step6_cred').html('$' + (tots-credits).toFixed(2));
                    $('#availcred').html('$' + (tots-credits).toFixed(2));
                    $('#step6_credit').html('-$' + (credits).toFixed(2));
                    $('#step6_payable').html('$' + (totalminusgst-credits).toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                    $('#fintot').val(tots.toFixed(2));
                } else {
                    console.log("SETTINGSUB HERE4");
                    $('#step6_SUB').html("$" + totalminusgst.toFixed(2));
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#step6_cred').html('$' + (totalplusgst-credits).toFixed(2));
                    $('#availcred').html('$' + (tots-credits).toFixed(2));
                    $('#step6_credit').html('-$' + (credits).toFixed(2));
                    $('#step6_payable').html('$' + (totalminusgst-credits).toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
            } else if(nucredits > 0 && nucredits >= pricebeforecredits) { // credits > TotalPrice
                console.log("SETTINGSUB HERE5");
                $('.partial_credits').html('<i class="fa fa-info-circle" aria-hidden="true"></i> You already have remaining credits left so please just click the "Next" button below to proceed to checkout.');
                var pricebeforedisc = TotalPrice;
                $('.partialcred').addClass('hidden');
                $('.availcred').addClass('hidden');
                $('.payableend').addClass('hidden');
                $('.creditsend').addClass('hidden');
                $('.totalpriceend').removeClass('hidden');
                console.log("CHECKER ABOVE" + TotalPrice)
                console.log("CHECKER " + TotalPrice)
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                $('#fintot').val(0);
                var gst = Number(0).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = parseFloat(TotalPrice);
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                $('#payment-form .card-details').hide();
                $('#selectpayment').hide();
                $('#adssdar32rfdsfdsf').val("L");
                $('#invwarning').addClass('hidden');
                $('#jobpaymentitle').html("Submit Listing");
                var credos = pricebeforedisc + credits;
               
                $('#step6_SUB').html("$" + TotalPrice.toFixed(2));
                //console.log("SETTINGSUB HERE6");
                var ccfee = 0;
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    //console.log("SETTINGSUB HERE9");
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                    $('#fintot').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
            }else{
                var pricebeforedisc = TotalPrice;
                $('.partial_credits').html('<i class="fa fa-info-circle" aria-hidden="true"></i> You have remaining credits so please just click the "next" button below');
                $('.partialcred').addClass('hidden');
                $('.availcred').addClass('hidden');
                $('.totalpriceend').removeClass('hidden');
                $('.payableend').addClass('hidden');
                $('.creditsend').addClass('hidden');
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                //$('#fintot').val(0);
                var gst = (TotalPrice / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = pricebeforedisc - gst;
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                $('#step6_SUB').html("$" + TotalPrice.toFixed(2));
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#fintot').val(tots.toFixed(2));
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
            }
        } else {
            //console.log("hello");
                var pricebeforedisc = TotalPrice;
                $('#step6_total_price').html("$" + TotalPrice.toFixed(2));
                $('#TotalPrice').val(newprice);
                $('.partial_credits').html('<i class="fa fa-info-circle" aria-hidden="true"></i> You have remaining credits so please just click the "next" button below');
                $('.partialcred').addClass('hidden');
                $('.availcred').addClass('hidden');
                $('.totalpriceend').removeClass('hidden');
                $('.payableend').addClass('hidden');
                $('.creditsend').addClass('hidden');
                //$('#fintot').val(0);
                var gst = (TotalPrice / 10).toFixed(2);
                $('#step6_GST').html("$" + gst);
                var totalminusgst = pricebeforedisc - gst;
                var totalplusgst = parseFloat(TotalPrice) + parseFloat(gst);
                //$('#step6_ITEMS').html("$" + pricebeforedisc);
                $('#step6_SUB').html("$" + TotalPrice.toFixed(2));
                var ccfee = $('#cf89073789').val();
                if (ccfee != 0) {
                    var ccfeetotal = totalplusgst.toFixed(2) * ccfee / 100;
                    var subbie = TotalPrice;
                    $('#step6_SUB').html("$" + subbie.toFixed(2));
                    $('.ccrow').removeClass("hidden");
                    $('#step6_CCFEE').html("$" + ccfeetotal.toFixed(2));
                    var tots = totalplusgst + ccfeetotal;
                    $('#fintot').val(tots.toFixed(2));
                    $('#step6_total_price').html("$" + tots.toFixed(2));
                    $('#TotalPrice').val(tots.toFixed(2));
                } else {
                    $('#step6_total_price').html("$" + totalplusgst.toFixed(2));
                    $('#fintot').val(totalplusgst.toFixed(2));
                    $('#TotalPrice').val(totalplusgst.toFixed(2));
                    $('.ccrow').addClass("hidden");
                }
        }
            
            
            return;
        }
        ;

        // disable inactive tabs
        $('.a-tab').on('click', function (e) {
            if ($(this).hasClass('disabled')) {
                return false;
            }
        });

        $('ul.pager').on('click', function () {
            var id = $('.tab-pane.active').first().attr('id');
            if(id == 'tab3'){
                $('.a-tab4').addClass('disabled');
                $('.a-tab5').addClass('disabled');
                $('.a-tab6').addClass('disabled');
                $('.a-tab7').addClass('disabled');
            }
            if(id == 'tab2'){
                $('.a-tab3').addClass('disabled');
                $('.a-tab4').addClass('disabled');
                $('.a-tab5').addClass('disabled');
                $('.a-tab6').addClass('disabled');
                $('.a-tab7').addClass('disabled');
            }
            
        });

        $('.a-tab2').on('click', function () {
                $('.a-tab3').addClass('disabled');
                $('.a-tab4').addClass('disabled');
                $('.a-tab5').addClass('disabled');
                $('.a-tab6').addClass('disabled');
                $('.a-tab7').addClass('disabled');
        });

        $('.a-tab3').on('click', function () {
                $('.a-tab4').addClass('disabled');
                $('.a-tab5').addClass('disabled');
                $('.a-tab6').addClass('disabled');
                $('.a-tab7').addClass('disabled');
        });

        $('.a-tab3').on('click', function () {
            setTimeout(
            function() 
            { 
                productdescription = $("#dropzone-img .dz-image").html();
                if (productdescription == null) {
                    productdescription = "Not found";
                }
                matchs = productdescription.match(/<img/gi);
                preview_vid = $("#video_link").val();
                if(featuredproduct == false) {
                    if(preview_vid.length > 10) {
                        $(".pager .next").addClass("disabled");
                    };
                };
                if(matchs !== null) {
                    if (matchs.length < 1) {
                        if(preview_vid.length < 10) {
                            $(".pager .next").removeClass("disabled");
                        };
                    } else {
                        if(featuredproduct == false) {
                            $(".pager .next").addClass("disabled");
                        };
                    };
                } else {
                    if(preview_vid.length < 10) {
                        $(".pager .next").removeClass("disabled");
                    }
                };
            if (featuredproduct == true) {
            } else {
                if($('#step3_box [name=posts_packages]').is(':checked')) {
                if(preview_vid.length > 10) {
                    swal({
                        html: "We've noticed that you’ve added a video in your job description. Please note that video can only be included with our Featured Listings.</br>Please either select a Featured Listing or go back and remove the video to continue with a Premium or Standard Job Ad.",
                    })
                };

                if(matchs !== null) {
                    if (matchs.length > 0) {
                        swal({
                            html: "We've noticed that you’ve added photos in your job description. Please note that photos can only be included with our Featured Listings.</br>Please either select a Featured Listing or go back and remove the photo to continue with a Premium or Standard Job Ad.",                        
                        })
                    };
                };

                if(preview_vid.length > 10 && matchs !== null) {
                    swal({
                        html: "We've noticed that you’ve added photos and a video in your job description. Please note that these can only be included with our Featured Listings. </br>Please either select a Featured Listing or go back and remove the photo and video to continue with a Premium or Standard Job Ad.",
                    })
                };
            }
            }

            }, 1000);
        });
        // by clicking to 'next' button enable the following tab
        $('ul.pager li.next a').on('click', function () {
            var id = $('.tab-pane.active').first().attr('id'); // id of 'old' tab. Example: tab2
            var enabled = true;
            var display = false;
            var focused = false;

            if ($('#application_method').val() === '1') {
                if($('#email_or_link').val() == ''){
                    $('.url_required').css('display', '');
                    display = true;
                }else{
                    $('.url_required').css('display', 'none');
                }
            }
            if($('#company').val() == ''){
                    $('.comp_name_required').css('display', '');
                    display = true;
            }else{
                    $('.comp_name_required').css('display', 'none');
            }
            if($('#title').val() == ''){
                    $('.title_required').css('display', '');
                    display = true;
            }else{
                    $('.title_required').css('display', 'none');
            }
            if($('#selling4').val() == ''){
                    $('.preview_required').css('display', '');
                    display = true;
            }else{
                    $('.preview_required').css('display', 'none');
            }
            if($('#summernote').summernote('isEmpty')){
                    $('.description_required').css('display', '');
                    display = true;
            }else{
                    $('.description_required').css('display', 'none');
            }
            if($('#joblocation').val() == ''){
                    $('.location_required').css('display', '');
                    display = true;
            }else{
                    $('.location_required').css('display', 'none');
            }
            if($('#stateselect').val() == ''){
                    $('.state_required').css('display', '');
                    display = true;
            }else{
                    $('.state_required').css('display', 'none');
            }
            if($('#salary_type1').val() == ''){
                    $('.sal_type_required').css('display', '');
                    display = true;
            }else{
                    $('.sal_type_required').css('display', 'none');
            }
            if ($('#salary_type1').val() === 'annual') {
                $('.sal_hr_required').css('display', 'none');
                $('.select_hourly_required').css('display', 'none');
                $('.enter_hourly_required').css('display', 'none');
                $('.enter_annual_required_check').css('display', 'none');
                if($('#annual_ask_select').val() == ''){
                    $('.select_annual_required').css('display', '');
                    $('.enter_annual_required').css('display', 'none');
                    display = true;
                }else{
                    $('.select_annual_required').css('display', 'none');
                    if($('#annual_ask_select').val() == 'yes'){
                        if($('#annual_ask_salary_enter').val() == ''){
                            $('.enter_annual_required').css('display', '');
                            display = true;
                        }else{
                            $('.enter_annual_required').css('display', 'none');
                        }
                    }else{
                            $('.enter_annual_required').css('display', 'none');
                    }
                }
                if($('#salary').val() == ''){
                    $('.sal_est_required').css('display', '');
                    display = true;
                }else{
                    $('.sal_est_required').css('display', 'none');
                }
            }else if($('#salary_type1').val() === 'hourly'){
                $('.sal_annual_required').css('display', 'none');
                $('.enter_annual_required').css('display', 'none');
                $('.select_annual_required').css('display', 'none');

                if($('#hourly_ask_select').val() == ''){
                    $('.select_hourly_required').css('display', '');
                    $('.enter_hourly_required').css('display', 'none');
                    $('.enter_annual_required_check').css('display', 'none');
                    display = true;
                }else{
                    $('.select_hourly_required').css('display', 'none');
                    if($('#hourly_ask_select').val() == 'yes'){
                        var val = parseFloat($('#hourly_ask_salary_enter').val().replace(/,/g, ''));
                        if($('#hourly_ask_salary_enter').val() == ''){
                            $('.enter_hourly_required').css('display', '');
                            display = true;
                        }else{
                            $('.enter_hourly_required').css('display', 'none');
                        } 

                        if(val > 500000){
                            $('.enter_annual_required_check').css('display', '');
                            display = true;
                            enabled = false;
                        }else{
                            $('.enter_annual_required_check').css('display', 'none');
                        }
                    }else{
                            $('.enter_hourly_required').css('display', 'none');
                            $('.enter_annual_required_check').css('display', 'none');
                    }
                }
                if($('#hrsalary').val() == ''){
                    $('.sal_est_required').css('display', '');
                    display = true;
                }else{
                    $('.sal_est_required').css('display', 'none');
                }
            }else if($('#salary_type1').val() === ''){
                    $('.enter_annual_required').css('display', 'none');
                    $('.select_annual_required').css('display', 'none');
                    $('.sal_hr_required').css('display', 'none');
                    $('.sal_annual_required').css('display', 'none');
                    $('.sal_est_required').css('display', '');
                    $('.select_hourly_required').css('display', 'none');
                    $('.enter_hourly_required').css('display', 'none');
                    $('.enter_annual_required_check').css('display', 'none');
                    display = true;
            }
            if($('#incentivestructure1').val() == ''){
                    $('.incent_required').css('display', '');
                    display = true;
            }else{
                    $('.incent_required').css('display', 'none');
            }
            if($('#pay_cycle1').val() == ''){
                    $('.freq_required').css('display', '');
                    display = true;
            }else{
                    $('.freq_required').css('display', 'none');
            }
            if($('#showsalary1').val() == ''){
                    $('.showsal_required').css('display', '');
                    display = true;
            }else{
                    $('.showsal_required').css('display', 'none');
            }
            if($('#emptype').val() == ''){
                    $('.emptype_required').css('display', '');
                    display = true;
            }else{
                    $('.emptype_required').css('display', 'none');
            }
            if($('#empterm').val() == ''){
                    $('.empstatus_required').css('display', '');
                    display = true;
            }else{
                    $('.empstatus_required').css('display', 'none');
            }
            if($('#workfromhome').val() == ''){
                    $('.workloc_required').css('display', '');
                    display = true;
            }else{
                    $('.workloc_required').css('display', 'none');
            }
            if($('#parentsfiltervalue').val() == ''){
                    $('.shiftguide_required').css('display', '');
                    display = true;
            }else{
                    $('.shiftguide_required').css('display', 'none');
            }
            if($('#wheretosend').val() == ''){
                    $('.recappl_required').css('display', '');
                    display = true;
            }else{
                    $('.recappl_required').css('display', 'none');
            }


            if ($('#application_method').val() === '1') {
                var link = $('#email_or_link').val();
                if (!/^http[s]?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g.test(link)) {
                  enabled = false;
                  $('#email_or_link').parent().parent().addClass('has-error');
                } else {
                  $('#email_or_link').parent().parent().removeClass('has-error');
                }
            }
            if ($('#application_method').val() === '0') {
                if ($('#questionpricing').val() == '') {
                  $('#questionpricing').parent().parent().addClass('has-error');
                  enabled = false;
                } else {
                  $('#questionpricing').parent().parent().removeClass('has-error');
                }
            }
            if($(".questiontotal:checked").length > 0){
                    if ($(".question:checked").length == 0) {
                        enabled = false;
                    swal({
                                html: "You haven't selected any screening questions!<br> You can select a maximum of 5 - just make sure you tick the 'select' box next to each one.",
                            })
                    }
            }
            $('#' + id + ' label.required').each(function () {
                var el = $(this).next().find('input').first();
                if (el.val() == "") {
                    el.addClass('required-empty');
                    enabled = false;
                } else {
                    el.removeClass('required-empty');
                }

                var se = $(this).next().find('select').first();
                if (se && se.val() == "") {
                    se.addClass('required-empty');
                    enabled = false;
                } else {
                    se.removeClass('required-empty');
                }
            });

            var hrsalary = parseFloat($('#hrsalary').val().replace(/,/g, ''));
            var max = parseFloat($('#hrsalary').attr('max'))
            var min = parseFloat($('#hrsalary').attr('min'))
            if (hrsalary) {
                if (hrsalary > max || hrsalary < min) {
                    $('#hrsalary').next('p.text-danger').show();
                    $('#hrsalary').addClass('required-empty');
                    if ($('#salary_type1').val() === 'hourly') {
                        $('.sal_hr_required').css('display', '');
                        display = true;
                    }
                    enabled = false;
                } else {
                    $('#hrsalary').next('p.text-danger').hide();
                    $('.sal_hr_required').css('display', 'none');
                    $('#hrsalary').removeClass('required-empty');
                }
            }

            if ($('#summernote').summernote('isEmpty')) {
                enabled = false;
                $('.note-editable').addClass('required-empty');
            }else{
                $('.note-editable').removeClass('required-empty');
            }
            if ($('#selling4').val() == '') {
                enabled = false;
                $('#selling4').addClass('required-empty');
            }else{
                $('#selling4').removeClass('required-empty');
            }


            var salary = parseFloat($('#salary').val().replace(/,/g, ''));
            max = parseFloat($('#salary').attr('max'))
            min = parseFloat($('#salary').attr('min'))
            if (salary) {
                if(salary > max || salary < min) {
                    $('#salary').next('p.text-danger').show();
                    $('#salary').addClass('required-empty');
                    if ($('#salary_type1').val() === 'annual') {
                        $('.sal_annual_required').css('display', '');
                        display = true;
                    }
                    enabled = false;
                } else {
                    $('#salary').next('p.text-danger').hide();
                    $('.sal_annual_required').css('display', 'none');
                    $('#salary').removeClass('required-empty');
                }
            }
            if(display == true){
                 $('.required_errors').css('display', '');
                 $('.scrollto').css('display', '');
                 $('html, body').animate({
                        scrollTop: $('#tab2 .scrollto').offset().top
                    }, 600);
            }else{
                 $('.scrollto').css('display', 'none');
                 $('.required_errors').css('display', 'none');
            }
            if (enabled === false) {
                return false;                
            }

            if (id == 'tab2') {
                setTimeout(
            function() 
            {
                if( $("#dropzone-img .dz-image").html() !== null ) {
                    productdescription = $("#dropzone-img .dz-image").html();
                    if (productdescription == null) {
                        productdescription = "Not found";
                    }
                    matchs = productdescription.match(/<img/gi);
                } else {
                    matchs = '';
                }
                preview_vid = $("#video_link").val();
                if(featuredproduct == false) {
                    if(preview_vid.length > 10) {
                        $(".pager .next").addClass("disabled");
                    };
                };
                if(matchs !== null) {
                    if (matchs.length < 1) {
                        if(preview_vid.length < 10) {
                            $(".pager .next").removeClass("disabled");
                        };
                    } else {
                        if(featuredproduct == false) {
                            $(".pager .next").addClass("disabled");
                        };
                    };
                } else {
                    if(preview_vid.length < 10) {
                        $(".pager .next").removeClass("disabled");
                    }
                };
            if($(".questiontotal:checked").length > 0){
                    if ($(".question:checked").length == 0) {
                    swal({
                                html: "You haven't selected any screening questions!<br> You can select a maximum of 5 - just make sure you tick the 'select' box next to each one.",
                            })
                    }
            }
            if (featuredproduct == true) {
                if(matchs.length > 5) {
                    swal({
                        html: "We've noticed that you've added more than 5 photos in your job description. Please note that only 5 photos are allowed. </br>Please remove the extra photos from the Job Description",
                    })
                };
            } else {
                if($('#step3_box [name=posts_packages]').is(':checked')) {

                    if(preview_vid !== null) {
                        if(preview_vid.length > 10) {
                            swal({
                                html: "We've noticed that you’ve added a video in your job description. Please note that video can only be included with our Featured Listings.</br>Please either select a Featured Listing or go back and remove the video to continue with a Premium or Standard Job Ad.",
                            })
                        };
                    }

                    if(matchs !== null) {
                        if (matchs.length > 0) {
                            swal({
                                html: "We've noticed that you’ve added photos in your job description. Please note that photos can only be included with our Featured Listings.</br>Please either select a Featured Listing or go back and remove the photo to continue with a Premium or Standard Job Ad.",                        
                            })
                        };
                    };

                    if(matchs !== null && preview_vid !== null) {
                        if(preview_vid.length > 10 && matchs.length > 0) {
                            swal({
                                html: "We've noticed that you’ve added photos and a video in your job description. Please note that these can only be included with our Featured Listings. </br>Please either select a Featured Listing or go back and remove the photo and video to continue with a Premium or Standard Job Ad.",
                            })
                        };
                    };
                }
            }
            }, 1000);
            }

            // Listing Options required
            if (id == 'tab3') {
                if (!$('#tab3 [name=posts_packages]:checked').val()) {
                    $('#tab3 .alert').removeClass('hidden');
                    $('html, body').animate({
                        scrollTop: $('#tab3 .alert').offset().top
                    }, 200);
                    return false;
                } else {
                    $('#tab3 .alert').addClass('hidden');
                }
            }
            //console.log(id);
            if (id == 'tab5') {
                if (!$('#tab5 [name=packages_stats]:checked').val() && !allowthrough) {
                    //console.log("hello2");
                    $('#tab5 #err1').removeClass('hidden');
                    $('html, body').animate({
                        scrollTop: $('#tab5 #err1').offset().top
                    }, 200);
                    return false;
                } else {
                    $('#tab5 #err1').addClass('hidden');
                    $("#box_selected").trigger("change");
                    setTotalPrice();
                }
            }
            if (id == 'tab6') {
                var img_thumb = jQuery(".image-picker option").size();
                if(img_thumb < 1) {
                } else {
                    clogo = false;
                }
                if(clogo == true) {
                   swal({
                        html: "We've noticed that you haven't attached a company logo. If you would like to include your logo go back to step one (<a class='goto-step1'>click here</a>) to upload one. If you do not wish to include a logo just click OK to continue.",
                    })
                }
                $('.goto-step1').on('click', function () {
                    $(".a-tab2").click();
                    $(".swal2-confirm").click();
                    $(window).scrollTop(0);
                });
            }
         
            $('input, select').on('blur', function (e) {
                var label = $(this).parent().parent().find('label').first()
                if (label.hasClass('required')) {
                    if ($(this).val() === '') {
                        $(this).addClass('required-empty')
                    } else {
                        $(this).removeClass('required-empty')
                    }
                }
            })

            $('ul.nav.nav-pills .a-' + id).parent().next().find('a').removeClass('disabled')
        });
    });
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    Stripe.setPublishableKey("{{ env('STRIPE_PUBLISHABLE_KEY') }}");
</script>
<script>
    $(function () {
        //PAYMENT
        var $form = $('#payment-form');
        $form.submit(function (event) {
            if ($('#fintot').val() > 0 && $('#adssdar32rfdsfdsf').val() != "L" && $('#paymentopt').val() != "eft") {
                // Disable the submit button to prevent repeated clicks:
                $form.find('.submit').prop('disabled', true);

                // Request a token from Stripe:
                Stripe.card.createToken($form, stripeResponseHandler);

            } else {
                submitForm(false);
            }
            // Prevent the form from being submitted:
            return false;
        });

        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            //console.log("GOT RESPONSE");
            //console.log(response);
            if (response.error) { // Problem!
                //console.log(response.error);
                // Show the errors on the form:
                $('#error').html(response.error.message);
                $('#error').removeClass("hidden");
                $form.find('.submit').prop('disabled', false); // Re-enable submission
                return false;


            } else { // Token was created!

                // Get the token ID:
                var token = response.id;

                submitForm(token);

                return false;

                // Submit the form:
                // $form.get(0).submit();
            }
        }
        ;

        function submitForm(token) {
            var $form = $('#payment-form');
            // Insert the token ID into the form so it gets submitted to the server:
            $form.append($('<input type="hidden" id="stripeToken" name="stripeToken">').val(token));

            // Append Summ
            // var summ = parseFloat($('#step6_total_price').text(), 10) | 0;
            // $form.append($('<input type="hidden" id="summ" name="summ">').val(summ));

            //append fields from form on Step 2
            $form.append('<div class="hidden" id="submitted_form"></div>');
            var form = $("#form-username").clone();
            var form1 = $("#billing-form").clone();
            $('#submitted_form').html("");
            $('#submitted_form').append(form.find(":input"));
            $('#submitted_form').append(form1.find(":input"));
            //$(this).parents("submitted_form").ajaxForm(options);
            var _self = $('#payment-form'),
                _data = _self.serialize();

            $.ajax({
                url: _self.attr('action'),
                data: _data,
                method: _self.attr('method'),
                dataType: 'json',
                beforeSend: function (xhr) {
                    // $('#').loadingIndicator();
                    $('.modal').show();
                },
                success: function (response) {
                    //console.log(response);
                    if (response.status == 200) {
                        window.location.replace("/poster");

                    } else {
                        $('.modal').hide();
                        $('#error').html(response.error);
                        $('#error').removeClass('hidden');
                        $form.find('.submit').prop('disabled', false); // Re-enable submission    
                    }
                },
                error: function (response) {
                    //console.log(response);
                    $('.modal').hide();
                    $('#error').html(response.error);
                    $('#error').removeClass('hidden');
                    $('#error').html(response.responseText);
                    //$('#output').html(response.responseText);
                    $form.find('.submit').prop('disabled', false);
                    return false;
                }
            });
            return true;
        }
        ;
    });

    function youtube_parser(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

    function checkVid() {
        if ($('#video_link').val().indexOf('youtu') > -1 || $('#video_link').val().indexOf('vimeo.com') > -1) {
            var url = $('#video_link').val();
            var a = youtube_parser(url);
            var b = '';
            if (a) {
               b = "http://i3.ytimg.com/vi/" + a +  "/1.jpg";
               $('#video_embed img').attr('src', b);
                $('#video_embed').show();
            }
            
            $('#vidOK').html('<span style="color:green">Valid video URL</span>');
        } else {
            $('#video_embed').hide();
            if ($('#video_link').val() == "") {
                $('#vidOK').html('Please enter a valid Vimeo or Youtube URL');
            } else {
                $('#vidOK').html('<span style="color:red">Not a valid video URL</span>');
            }
        }
    }
    
    function limitText(limitField) {
        limitNum = 200;
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        }
    }


    $('#hrsalary').on('keydown', function (e) {
        // tab, esc, enter
     
        if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
                // Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                e.preventDefault();

                // backspace & del
                if ($.inArray(e.keyCode, [8, 46]) !== -1) {
                    $(this).val('');
                    return;
                }

                var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
                var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

                var value = $(this).val();
                var clean = value.replace(/\./g, '').replace(/,/g, '').replace(/^0+/, '');

                var charCode = String.fromCharCode(e.keyCode);
                //console.log(charCode);
                var p = $.inArray(charCode, a);
                var g = $.inArray(charCode, n);

                if (p !== -1)
                {
                    value = clean + n[p];

                    if (value.length == 2)
                        value = '0' + value;
                    if (value.length == 1)
                        value = '00' + value;

                    var formatted = '';
                    for (var i = 0; i < value.length; i++)
                    {
                        var sep = '';
                        if (i == 2)
                            sep = '.';
                        if (i > 3 && (i + 1) % 3 == 0)
                            sep = ',';
                        formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
                    }

                    $(this).val(formatted);
                }

                if (g !== -1)
                {
                    //console.log("WORK GOD DAMMIT");
                    value = clean + n[g];

                    if (value.length == 2)
                        value = '0' + value;
                    if (value.length == 1)
                        value = '00' + value;

                    var formatted = '';
                    for (var i = 0; i < value.length; i++)
                    {
                        var sep = '';
                        if (i == 2)
                            sep = '.';
                        if (i > 3 && (i + 1) % 3 == 0)
                            sep = ',';
                        formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
                    }

                    $(this).val(formatted);
                }

                return;

            });


    function getoembed(url1) {

        $.ajax({
            url: 'http://query.yahooapis.com/v1/public/yql',
            data: {
                q: "select * from json where url ='http://www.youtube.com/oembed?url='" + url1 + "",
                format: "json"
            },
            dataType: "jsonp",
            success: function (data) {

                alert(JSON.stringify(data));


            },
            error: function (result) {
                alert("Sorry no data found.");
            }
        });


    }

    $('#salary').on('keydown', function (e) {
        // tab, esc, enter
        if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
                // Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    return;
                }

                e.preventDefault();

                // backspace & del
                if ($.inArray(e.keyCode, [8, 46]) !== -1) {
                    $(this).val('');
                    return;
                }

                var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
                var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

                var value = $(this).val();
                var clean = value.replace(/\./g, '').replace(/,/g, '').replace(/^0+/, '');

                var charCode = String.fromCharCode(e.keyCode);
                //console.log(charCode);
                var p = $.inArray(charCode, a);
                var g = $.inArray(charCode, n);

                if (p !== -1)
                {
                    value = clean + n[p];

                    if (value.length == 2)
                        value = '0' + value;
                    if (value.length == 1)
                        value = '00' + value;

                    var formatted = '';
                    for (var i = 0; i < value.length; i++)
                    {
                        var sep = '';
                        if (i == 2)
                            sep = '.';
                        if (i > 3 && (i + 1) % 3 == 0)
                            sep = ',';
                        formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
                    }

                    $(this).val(formatted);
                }

                if (g !== -1)
                {
                    //console.log("WORK GOD DAMMIT");
                    value = clean + n[g];

                    if (value.length == 2)
                        value = '0' + value;
                    if (value.length == 1)
                        value = '00' + value;

                    var formatted = '';
                    for (var i = 0; i < value.length; i++)
                    {
                        var sep = '';
                        if (i == 2)
                            sep = '.';
                        if (i > 3 && (i + 1) % 3 == 0)
                            sep = ',';
                        formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
                    }

                    $(this).val(formatted);
                }

                return;

            });

    $(function () {
        //result.address_components[0]
        $("#joblocation").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            //console.log(result.address_components);

            results = result.address_components;

            $.each(results, function (index, value) {
                if (value.types == 'postal_code') {
                    $("#postcode").val(value.long_name);
                    //console.log(value.long_name); // here you get the zip code  
                } else if (value.types[0] == 'locality') {
                    $("#city").val(value.long_name);
                } else if (value.types[0] == 'administrative_area_level_1') {
                    $('#stateselect option[value="' + value.long_name + '"]').prop('selected', true);
                    $("#stateselect").val(value.long_name).change();
                    $("#state").val(value.long_name);
                } else if (value.types[0] == 'country') {
                    $("#country").val(value.long_name);
                }
            });

            //$("#postcode").val(result.address_components[5]['long_name']);
            //console.log(result.geometry.location.lat());
            $("#lat").val(result.geometry.location.lat());
            $("#lng").val(result.geometry.location.lng());
            //console.log(result.geometry.location.lng());
            //console.log(result);


        });

        $("#billing_address").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo",
            country: "AU",
        }).bind("geocode:result", function (event, result) {
            geo_pickup = true;
            //console.log(result.address_components);

            results = result.address_components;

        });

        $("#find").click(function () {
            $("#billing_address").trigger("geocode");
        });

        $('#find').keypress(function (e) {
            if (e.which == 13) {//Enter key pressed
                $("#joblocation").trigger("geocode");
            }
        });


        $("#examples a").click(function () {
            $("#joblocation").val($(this).text()).trigger("geocode");
            return false;
        });

    });

</script>



@stop


@section('content')
<style>
    .center-pills { display: inline-block; }
</style>
<div class="modal" style="display:block;"><!-- Place at bottom of page --></div>
<div class="bg-color1 block-section line-bottom">
    <div class="container">
        <div id="rootwizard">

            <div class="navbar">
                <div class="navbar-inner" style="text-align: center;">
                    <div class="container no-pad">
                        <ul class="center-pills">
                            <li><a href="#tab2" data-toggle="tab" class="a-tab a-tab2">Job Details</a></li>
                            <li><a href="#tab3" data-toggle="tab" class="a-tab a-tab3 disabled">Choose Level</a></li>
                            <li><a href="#tab4" data-toggle="tab" class="a-tab a-tab4 disabled">Boosts</a></li>
                            <li><a href="#tab5" data-toggle="tab" class="a-tab a-tab5 disabled">Value Packs</a></li>
                            <li><a href="#tab6" data-toggle="tab" class="a-tab a-tab6 disabled">Preview</a></li>
                            <li><a href="#tab7" data-toggle="tab" class="a-tab a-tab7 disabled">Checkout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="tab-content">

                <div class="tab-pane" id="tab2">
                    @include('post-job.step2')
                </div>

                <div class="tab-pane" id="tab3">
                    @include('post-job.step3')
                </div>

                <div class="tab-pane" id="tab4">
                    @include('post-job.step4')
                </div>

                <div class="tab-pane" id="tab5">
                    @include('post-job.step5')
                </div>

                <div class="tab-pane" id="tab6">
                    @include('post-job.step6')
                </div>

                <div class="tab-pane" id="tab7">
                    @include('post-job.step7')
                </div>
                <ul class="pager wizard">
                    <li class="previous first" style="display:none;"><a href="#">First</a></li>
                    <li class="previous"><a href="#">Previous</a></li>
                    <li class="next last" style="display:none;"><a href="#">Last</a></li>
                    <li class="nexterror hidden" onclick="viewurl()" style="float: right;"><a >Next</a></li>
                    <li class="nextscreeningerror hidden" onclick="viewscreeningurl()" style="float: right;"><a >Next</a></li>
                    <li class="next"><a href="#">Next</a></li>
                </ul>
            </div>	
        </div>

        <div class="row"></div>
    </div>
</div>
<script type="text/javascript">
        function viewurl(){
            swal({
                    html: "External website links, email addresses and the symbols @, www, http and https cannot be included in the Main Description text area.<br><br>Please remove to continue!"
                })
        }
        function viewscreeningurl(){
            swal({
                    html: "You haven't selected any screening questions!<br> You can select a maximum of 5 - just make sure you tick the 'select' box next to each one.",
                })
        }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
<script src="/js/highlight.js"></script>
<script src="/js/card-js.min.js"></script>
<script src="/js/tooltipster.bundle.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/lightbox.js"></script>
<script type="text/javascript"></script>
<script type="text/javascript">
    $( ".go_jobdetail" ).click(function() {
      $( ".a-tab2" ).click();
    });
    $( ".go_joblevel" ).click(function() {
      $( ".a-tab3" ).click();
    });
</script>
@endsection