<?php

return [

    'mailgun' => [
        'domain' => 'mg.itsmycall.com.au',
        'secret' => 'key-b388fe1bd45b4f463d928ff1a214ffb2',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => App\Users::class,
        'secret'   => env('STRIPE_API_SECRET'),
        'key' => env("STRIPE_PUBLISHABLE_KEY"),
    ],

    'facebook' => [
        'client_id'     => env('FACEBOOK_ID'),
        'client_secret' => env('FACEBOOK_SECRET'),
        'redirect'      => env('FACEBOOK_URL'),
    ],

    'twitter' => [
        'client_id'     => env('TWITTER_ID'),
        'client_secret' => env('TWITTER_SECRET'),
        'redirect'      => env('TWITTER_URL'),
    ],

    'google' => [ 
        'client_id' => env ( 'G+_CLIENT_ID' ),
        'client_secret' => env ( 'G+_CLIENT_SECRET' ),
        'redirect' => env ( 'G+_REDIRECT' ) 
    ],

    'linkedin' => [ 
        'client_id' => env ( 'linkedin_ID' ),
        'client_secret' => env ( 'linkedin_SECRET' ),
        'redirect' => env ( 'linkedin_REDIRECT' ) 
    ],


];
