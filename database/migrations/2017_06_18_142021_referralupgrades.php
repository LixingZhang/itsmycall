<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Referralupgrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('referralupgrades', function (Blueprint $table) {
            $table->increments('idtable');
            $table->string('name');
            $table->string('description');
            $table->string('price');
            $table->string('type');
            $table->integer('referral_id');
            $table->integer('status');
            $table->integer('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('referralupgrades');
    }
}
