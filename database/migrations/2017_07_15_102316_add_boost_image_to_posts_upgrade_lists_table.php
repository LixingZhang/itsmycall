<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoostImageToPostsUpgradeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_upgrade_lists', function (Blueprint $table) {
            $table->string('boost_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_upgrade_lists', function (Blueprint $table) {
            $table->dropColumn('boost_image');
        });
    }
}
