<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPostsUpgradeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_upgrade_lists', function ($table) {
            $table->integer('post_id')->nullable();
            $table->integer('ispurchased')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('posts_upgrade_lists', function ($table) {
            $table->dropColumn('post_id');
             $table->dropColumn('ispurchased');
        });
    }
}
