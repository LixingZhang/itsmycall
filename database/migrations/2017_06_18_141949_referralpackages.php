<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Referralpackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('referralpackages', function (Blueprint $table) {
            $table->increments('idtable');
            $table->string('title');
            $table->string('description');
            $table->string('price');
            $table->string('name');
            $table->integer('referral_id');
            $table->integer('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('referralpackages');
    }
}
