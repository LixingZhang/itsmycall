<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('billing_postal_code')->nullable();
            $table->string('billing_route')->nullable();
            $table->string('billing_street')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_administrative_area')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('billing_city');
            $table->dropColumn('billing_postal_code');
            $table->dropColumn('billing_route');
            $table->dropColumn('billing_street');
            $table->dropColumn('billing_city');
            $table->dropColumn('billing_administrative_area');
        });
    }
}
