<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePastEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('past_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->integer('applicant_id');
            $table->integer('post_id');
            $table->integer('author_id');
            $table->integer('category_id');
            $table->text('feedback')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('past_emails');
    }
}
