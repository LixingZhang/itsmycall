<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInternalCommentsToPostsUpgradeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts_upgrade_lists', function (Blueprint $table) {
            $table->text('internal_comment')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_upgrade_lists', function (Blueprint $table) {
            $table->dropColumn('internal_comment');
        });
    }
}
