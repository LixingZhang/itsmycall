<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('users')->insert([
			[
				'id'                    => '1',
				'inboundprogram'        => '0',
				'profile_type'          => null,
				'currentposition'       => null,
				'timeinrole'            => null,
				'address'               => null,
				'postcode'              => null,
				'state'                 => null,
				'name'                  => 'Admin',
				'first_name'            => '',
				'last_name'             => '',
				'slug'                  => 'admin',
				'email'                 => 'admin@mail.com',
				'password'              => '$2y$10$AycgqPwM79GBFFaoBvkCKOWeKYg5N1o5XD6dHaCWpkAOBKxLGVy16',
				'remember_token'        => 'i7kmJT4vtJzyNu6yGVVqeV6Ihwi56Y039ataw3vrNBaDY7QEPBCYV0DKS1VP',
				'created_at'            => '2015-09-09 11:52:02',
				'updated_at'            => '2017-04-13 20:54:19',
				'avatar'                => 'http://itsmycall.dev/uploads/5723f88aadfd9_file.png',
				'birthday'              => '',
				'bio'                   => '',
				'gender'                => 'male',
				'mobile_no'             => '',
				'country'               => '15',
				'timezone'              => '',
				'reset_password_code'   => '',
				'reset_requested_on'    => '0000-00-00 00:00:00',
				'activated'             => '1',
				'activation_code'       => '',
				'activated_at'          => '2016-11-29 01:00:21',
				'fb_url'                => '',
				'fb_page_url'           => '',
				'website_url'           => '',
				'twitter_url'           => '',
				'google_plus_url'       => '',
				'resume'                => '',
				'stripe_active'         => '1',
				'stripe_id'             => 'cus_6y5GDYnPVYd3es',
				'stripe_subscription'   => 'sub_6y5GW2tPWXPgJv',
				'stripe_plan'           => '1',
				'last_four'             => '4242',
				'trial_ends_at'         => null,
				'subscription_ends_at'  => null,
				'company_admin'         => '0',
				'credits'               => '0.00',
				'aus_contact_member_no' => null,
				'company'               => '',
				'billing_business'      => '',
				'billing_number'        => '',
				'billing_abn'           => '',
				'billing_address'       => '',
				'billing_email'         => '',
				'billing_ref'           => '',
				'ref_id'                => '',
				'city'                  => null,
				'ambitions'             => null,
				'expected_salary'       => null,
				'phone'                 => null,
				'availability'          => '',
				'category'              => '0',
				'subcategory'           => '0',
				'lastwork'              => null,
				'employment_term'       => null,
				'employment_type'       => null,
				'wage_min'              => null,
				'wage_max'              => null
			]
		]);
	}
}
