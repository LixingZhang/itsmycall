<?php

use Illuminate\Database\Seeder;

class UsersGroupsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('users_groups')->insert([
			[
				'id'         => '1',
				'user_id'    => '1',
				'group_id'   => '1',
				'created_at' => '2015-09-09 11:52:02',
				'updated_at' => '2015-09-09 11:52:02'
			],
		]);
	}
}
