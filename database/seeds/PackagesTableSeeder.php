<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('packages')->insert([
			[
				'id'               => '1',
				'package_name'     => 'Get Started',
				'jobs_included'    => null,
				'price'            => '1500',
				'created_at'       => '2017-03-09 18:06:42',
				'updated_at'       => '2017-03-09 18:06:42',
				'description'      => '<p>If you\'ve got around 10 jobs* you are going to advertise in the next six months our <strong>Get Started package</strong> is a great way to save. Simply purchase the Get Started package for $1,500 and get an <strong>additional $225 for free</strong>! So you\'ll have a total of <strong>$1,725</strong> to spend on either Standard, Premium or Featured jobs as well as any of our available <a href="https://itsmycall.zendesk.com/hc/en-us/articles/115002063687-What-are-upgrades-and-how-much-do-they-cost-" target="_blank">upgrades</a>. </p><p>*10 jobs based on a Regular Job Advertisement with no upgrades<br></p><p>Please note:</p><ul><li>You have six months to use your Job Package credits </li><li>You can purchase additional packs if you need them at any time</li><li>Price excludes GST</li></ul><p><br></p>',
				'discount_percent' => '15'
			],
			[
				'id'               => '2',
				'package_name'     => 'Regular Advertiser',
				'jobs_included'    => null,
				'price'            => '3000',
				'created_at'       => '2017-03-09 18:05:45',
				'updated_at'       => '2017-03-09 18:05:45',
				'description'      => '<p>If you\'re planning on advertising around 20 jobs in the next six months our <strong>Regular Advertiser package</strong> is a great way to save money! Simply purchase the Regular Advertiser package for $3,000 and get an additional $600 for free! So you\'ll have a total of <strong>$3,600</strong> to spend on either Standard, Premium or Featured jobs as well as any of our <a href="https://itsmycall.zendesk.com/hc/en-us/articles/115002063687-What-are-upgrades-and-how-much-do-they-cost-" target="_blank">upgrades</a>. </p><p>Please note:</p><ul><li>You have six months to use your package credits</li><li>You can purchase additional packs if you need them at any time</li><li>Price excludes GST</li></ul><p>
*20 jobs based on a Regular Job Advertisement with no upgrades</p>',
				'discount_percent' => '20'
			],
			[
				'id'               => '3',
				'package_name'     => 'Frequent Advertiser ',
				'jobs_included'    => null,
				'price'            => '5000',
				'created_at'       => '2017-03-09 18:04:45',
				'updated_at'       => '2017-03-09 18:04:45',
				'description'      => '<p>If you\'re planning on advertising around 35 jobs* in the next six months our <strong>Frequent Advertiser package</strong> is a great way to save money! Simply purchase the Frequent Advertiser package for $5,000 and get an <strong>additional $1,500 for free</strong>! So you\'ll have a total of <strong>$6,500</strong> to spend on either Standard, Premium or Featured jobs as well as any of our available <a href="https://itsmycall.zendesk.com/hc/en-us/articles/115002063687-What-are-upgrades-and-how-much-do-they-cost-" target="_blank">upgrades</a>. </p><p>Please note:</p><ul><li>You have six months to use your package credits commencing from the date you post your first role.</li><li>You can purchase additional packs if you need them at any time</li><li>Price excludes GST</li></ul><p>*35 jobs based on a Regular Job Advertisement with no upgrades</p>',
				'discount_percent' => '30'
			],
			[
				'id'               => '4',
				'package_name'     => 'Power Advertiser',
				'jobs_included'    => null,
				'price'            => '10000',
				'created_at'       => '2017-03-09 18:06:03',
				'updated_at'       => '2017-03-09 18:06:03',
				'description'      => '<p>If you\'re planning on advertising around 80 jobs in the next six months our <strong>Power Advertiser packag</strong><strong>e</strong> is a great way to save money! Simply purchase the Power Advertiser package for $10,000 and get an <strong>additional $5,000 for free</strong>! So you\'ll have a total of <strong>$15,000</strong> to spend on either Standard, Premium or Featured jobs as well as any of our available <a href="https://itsmycall.zendesk.com/hc/en-us/articles/115002063687-What-are-upgrades-and-how-much-do-they-cost-" target="_blank">upgrades.</a></p><p>*80 jobs based on a Regular Job Advertisement with no upgrades<br></p><p>Please note:</p><ul><li>You have six months to use your package credits commencing from the date you post your first role.</li><li>You can purchase additional packs if you need them at any time</li><li>Price excludes GST</li></ul>',
				'discount_percent' => '50'
			]
		]);
	}
}
