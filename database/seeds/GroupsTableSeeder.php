<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('groups')->insert([
			[
				'id'          => '1',
				'name'        => 'admin',
				'permissions' => '',
				'created_at'  => '2015-09-09 11:52:01',
				'updated_at'  => '2015-09-09 11:52:01'
			],
			[
				'id'          => '2',
				'name'        => 'customer',
				'permissions' => '',
				'created_at'  => '2016-05-08 19:30:36',
				'updated_at'  => '2016-05-08 19:30:36'
			],
			[
				'id'          => '3',
				'name'        => 'company_admin',
				'permissions' => '',
				'created_at'  => '2016-05-08 19:30:36',
				'updated_at'  => '2016-05-08 19:30:36'
			],
			[
				'id'          => '4',
				'name'        => 'poster',
				'permissions' => 'yes',
				'created_at'  => '2016-05-08 19:30:36',
				'updated_at'  => '2016-05-08 19:30:36'
			]
		]);
	}
}
