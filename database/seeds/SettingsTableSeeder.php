<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(
          [
            [
              'id'               => '48',
              'category'         => 'general',
              'column_key'       => 'registration_text_user',
              'value_string'     => '',
              'value_txt'        => '',
              'value_check'      => '',
              'created_at'       => '2017-04-23 11:23:42',
              'updated_at'       => '2017-04-17 21:37:00',
              'show_big_sharing' => '',
            ],
            [
              'id'               => '49',
              'category'         => 'general',
              'column_key'       => 'registration_text_poster',
              'value_string'     => '',
              'value_txt'        => '',
              'value_check'      => '',
              'created_at'       => '2017-04-23 11:23:42',
              'updated_at'       => '2017-04-17 21:37:00',
              'show_big_sharing' => '',
            ],
          ]
        );
    }
}
